package com.augiq.external.source.bi.cognos;

public class CatalogColumn {
	
	private String columnName;
	private String businessColumnName;
	private String columnDataType;
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getBusinessColumnName() {
		return businessColumnName;
	}
	public void setBusinessColumnName(String businessColumnName) {
		this.businessColumnName = businessColumnName;
	}
	public String getColumnDataType() {
		return columnDataType;
	}
	public void setColumnDataType(String columnDataType) {
		this.columnDataType = columnDataType;
	}
}

/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.entity.model.configuration.bean.Join;

public class CognosModelData {
	private String packageName;
	private ExtractLayerData extractLayerData;
	private Map<String,DataModelLayerData> dataModelLayerDatas;
	private List<String> dataModelNames;
	private Map<String,Table> physicalTableMap;
	private List<Join> joinList;
	private List<String> dbSchemaList;
	private SymenticLayerOutput symenticLayerOutput;
	private Map<String,String> expressionMap;
	private CognosReportData cognosReportData;
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public List<String> getDataModelName() {
		return dataModelNames;
	}
	public void setDataModelName(List<String> dataModelNames) {
		this.dataModelNames = dataModelNames;
	}
	public List<Join> getJoinList() {
		return joinList;
	}
	public void setJoinList(List<Join> joinList) {
		this.joinList = joinList;
	}
	public Map<String,Table> getPhysicalTableMap() {
		return physicalTableMap;
	}
	public void setPhysicalTableMap(Map<String,Table> physicalTableMap) {
		this.physicalTableMap = physicalTableMap;
	}
	public List<String> getDbSchemaList() {
		return dbSchemaList;
	}
	public void setDbSchemaList(List<String> dbSchemaList) {
		this.dbSchemaList = dbSchemaList;
	}
	public ExtractLayerData getExtractLayerData() {
		return extractLayerData;
	}
	public void setExtractLayerData(ExtractLayerData extractLayerData) {
		this.extractLayerData = extractLayerData;
	}
	public Map<String,DataModelLayerData> getDataModelLayerDatas() {
		return dataModelLayerDatas;
	}
	public void setDataModelLayerDatas(Map<String,DataModelLayerData> dataModelLayerDatas) {
		this.dataModelLayerDatas = dataModelLayerDatas;
	}
	public SymenticLayerOutput getSymenticLayerOutput() {
		return symenticLayerOutput;
	}
	public void setSymenticLayerOutput(SymenticLayerOutput symenticLayerOutput) {
		this.symenticLayerOutput = symenticLayerOutput;
	}
	public Map<String,String> getExpressionMap() {
		return expressionMap;
	}
	public void setExpressionMap(Map<String,String> expressionMap) {
		this.expressionMap = expressionMap;
	}
	public List<String> getDataModelNames() {
		return dataModelNames;
	}
	public void setDataModelNames(List<String> dataModelNames) {
		this.dataModelNames = dataModelNames;
	}
	public CognosReportData getCognosReportData() {
		return cognosReportData;
	}
	public void setCognosReportData(CognosReportData cognosReportData) {
		this.cognosReportData = cognosReportData;
	}
}

/**
 * 
 */
package com.augiq.external.source.bi.cognos;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import com.augmentiq.maxiq.entity.model.configuration.bean.Join;


public class CognosModelExtractor {
	public Map<String,String> convertedExpressionMap=new HashMap<>();
	public Map<String,String> builtExpressionMap=new HashMap<>();

	public CognosModelData extractModel(String modelPath, String cognosXmlDownloadPath, CognosReportData cognosReportData){
		XmlParser xmlParser=null;
		XPathExpression<Element> xpath =null;
		ParserObject modelPo=null;
		CognosModelData cognosModelData=null;
		String packageName=null;
		SymenticLayerOutput symenticLayerOutput=null;
		CommonDataModel commonDataModel=null;
		ExtractLayerData extractLayerData=null;
		try{
			cognosModelData=new CognosModelData();
			cognosModelData.setCognosReportData(cognosReportData);
			packageName=modelPath.split("/package\\[@name='")[modelPath.split("/package\\[@name='").length-1].replace("']", "");
			System.out.println("packageName------------------------"+packageName);
			cognosModelData.setPackageName(packageName);
			xmlParser=new XmlParser();
			modelPo=xmlParser.getXmlParserObject(cognosXmlDownloadPath + "/Model" + packageName+".xml");
			symenticLayerOutput=new SymenticLayerOutput();
			commonDataModel=new CommonDataModel();
			commonDataModel.setReportName(cognosReportData.getCommonDataModelReports());
			symenticLayerOutput.setCommonDataModel(commonDataModel);
			symenticLayerOutput.setPackageName(packageName);
			symenticLayerOutput.setDataModelLayer(new HashMap<String,DataModelLayer>());
			cognosModelData.setSymenticLayerOutput(symenticLayerOutput);
			extractLayerData=new ExtractLayerData();
			extractLayerData.setModifiedTableMap(new HashMap<String,String>());
			cognosModelData.setExtractLayerData(extractLayerData);
			cognosModelData.setDataModelLayerDatas(new HashMap<String,DataModelLayerData>());
			cognosModelData.setDbSchemaList(extractDatasources(xmlParser, xpath, modelPo));
			cognosModelData=extractQuerySubject(xmlParser, xpath, modelPo, cognosReportData, cognosModelData);
			cognosModelData=extractFilters(xmlParser, xpath, modelPo, cognosReportData, cognosModelData);
			cognosModelData=extractJoins(xmlParser, xpath, modelPo, cognosModelData);
			cognosModelData.setExpressionMap(convertedExpressionMap);
			System.out.println("COgnos Model extraction is done for package "+packageName);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			xmlParser=null;
			xpath =null;
			modelPo=null;
			packageName=null;
			symenticLayerOutput=null;
			commonDataModel=null;
			extractLayerData=null;
		}
		return cognosModelData;
	}

	private CognosModelData extractQuerySubject(XmlParser xmlParser,XPathExpression<Element> xpath,ParserObject modelPo,CognosReportData cognosReportData, CognosModelData cognosModelData){
		Element shortcutElement=null,querySubjectElement=null;
		String refObj=null,targetType=null, namespace=null,queryColumnRef=null;
		DataModelLayerData dataModelLayerData=null;
		DataModelLayer dataModelLayerOutput=null;
		Map<String,List<String>> tableMap=null;
		Map<String,String> qlikAliasTablesMap=null;
		try{
			for(String dataModelName:cognosReportData.getQueryMap().keySet()){
				dataModelLayerData=new DataModelLayerData();
				dataModelLayerData.setDataModelName(dataModelName);
				qlikAliasTablesMap=new HashMap<>();
				dataModelLayerData.setQlikAliasTablesMap(qlikAliasTablesMap);
				dataModelLayerOutput=new DataModelLayer();
				dataModelLayerOutput.setDerivedColumn(new ArrayList<DerivedColumn>());
				dataModelLayerOutput.setMeasure(new ArrayList<Measures>());
				dataModelLayerOutput.setModelFilter(new ArrayList<ModelFilter>());
				cognosModelData.getSymenticLayerOutput().getDataModelLayer().put(dataModelName, dataModelLayerOutput);
				cognosModelData.getDataModelLayerDatas().put(dataModelName, dataModelLayerData);
				for (String namespaceShortcut : cognosReportData.getQueryMap().get(dataModelName).keySet()) {
					xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:shortcut[ns:name='"+namespaceShortcut+"']")), Filters .element(),null,modelPo.getNs());
					shortcutElement=xpath.evaluateFirst(modelPo.getDocument());
					if(shortcutElement!=null){
						refObj=shortcutElement.getChildText("refobj", modelPo.getNs()).replace("[", "").replace("]", "");
						targetType=shortcutElement.getChildText("targetType", modelPo.getNs());
						if(targetType.equals("namespace")){
							namespace=refObj;
							tableMap=cognosReportData.getQueryMap().get(dataModelName).get(namespaceShortcut);
							for (String tableName : tableMap.keySet()) {
								xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+namespace+"']//ns:shortcut[ns:name='"+tableName.trim()+"']")), Filters .element(),null,modelPo.getNs());
								shortcutElement=xpath.evaluateFirst(modelPo.getDocument());
								if(shortcutElement==null){
									xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+namespace+"']//ns:querySubject[ns:name='"+tableName+"']")), Filters .element(),null,modelPo.getNs());	
									querySubjectElement=xpath.evaluateFirst(modelPo.getDocument());
									if(null!=querySubjectElement){
										for (String columnName : tableMap.get(tableName)) {
											queryColumnRef="["+namespaceShortcut+"].["+tableName+"].["+columnName+"]";
											cognosModelData= extractQueryItem(xmlParser, xpath, modelPo, namespace,querySubjectElement.getChildText("name", modelPo.getNs()), columnName,cognosModelData,columnName,null,dataModelName,null,queryColumnRef,null);
										}
									}else{
										System.out.println("Table is not available in data model with name-----"+tableName);
									}
								}else{
									refObj=shortcutElement.getChildText("refobj", modelPo.getNs()).replace("[", "").replace("]", "");
									targetType=shortcutElement.getChildText("targetType", modelPo.getNs());
									if(targetType.equals("querySubject")){
										xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+refObj.split("\\.")[0]+"']//ns:querySubject[ns:name='"+refObj.split("\\.")[1]+"']")), Filters .element(),null,modelPo.getNs());	
										querySubjectElement=xpath.evaluateFirst(modelPo.getDocument());
										if(null!=querySubjectElement){
											for (String columnName : tableMap.get(tableName)) {
												queryColumnRef="["+namespaceShortcut+"].["+tableName+"].["+columnName+"]";
												cognosModelData= extractQueryItem(xmlParser, xpath, modelPo, refObj.split("\\.")[0],querySubjectElement.getChildText("name", modelPo.getNs()), columnName,cognosModelData,columnName,null, dataModelName,null,queryColumnRef,null);
											}
										}else{
											System.out.println("Table is not available in data model with name-----"+tableName);
										}
									}
								}
							}
						}else if(targetType.equals("querySubject")){

						}
					}else{
						namespace=namespaceShortcut;
						tableMap=cognosReportData.getQueryMap().get(dataModelName).get(namespaceShortcut);
						for (String tableName : tableMap.keySet()) {
							xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+namespace+"']//ns:shortcut[ns:name='"+tableName.trim()+"']")), Filters .element(),null,modelPo.getNs());
							shortcutElement=xpath.evaluateFirst(modelPo.getDocument());
							if(shortcutElement==null){
								xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+namespace+"']//ns:querySubject[ns:name='"+tableName+"']")), Filters .element(),null,modelPo.getNs());	
								querySubjectElement=xpath.evaluateFirst(modelPo.getDocument());
								if(null!=querySubjectElement){
									for (String columnName : tableMap.get(tableName)) {
										queryColumnRef="["+namespaceShortcut+"].["+tableName+"].["+columnName+"]";
										cognosModelData= extractQueryItem(xmlParser, xpath, modelPo, namespace,querySubjectElement.getChildText("name", modelPo.getNs()), columnName,cognosModelData,columnName,null,dataModelName,null,queryColumnRef,null);
									}
								}else{
									System.out.println("Table is not available in data model with name-----"+tableName);
								}
							}else{
								refObj=shortcutElement.getChildText("refobj", modelPo.getNs()).replace("[", "").replace("]", "");
								targetType=shortcutElement.getChildText("targetType", modelPo.getNs());
								if(targetType.equals("querySubject")){
									xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+refObj.split("\\.")[0]+"']//ns:querySubject[ns:name='"+refObj.split("\\.")[1]+"']")), Filters .element(),null,modelPo.getNs());	
									querySubjectElement=xpath.evaluateFirst(modelPo.getDocument());
									if(null!=querySubjectElement){
										for (String columnName : tableMap.get(tableName)) {
											queryColumnRef="["+namespaceShortcut+"].["+tableName+"].["+columnName+"]";
											cognosModelData= extractQueryItem(xmlParser, xpath, modelPo, refObj.split("\\.")[0],querySubjectElement.getChildText("name", modelPo.getNs()), columnName,cognosModelData,columnName,null, dataModelName,null,queryColumnRef,null);
										}
									}else{
										System.out.println("Table is not available in data model with name-----"+tableName);
									}
								}
							}
						}
					}
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			shortcutElement=null;
			querySubjectElement=null;
			refObj=null;
			targetType=null;
			namespace=null;
			queryColumnRef=null;
			dataModelLayerData=null;
			dataModelLayerOutput=null;
			tableMap=null;
		}
		return cognosModelData;
	}

	private CognosModelData extractQueryItem(XmlParser xmlParser,XPathExpression<Element> xpath,ParserObject modelPo,String namespace,String tableName,String columnName,CognosModelData cognosModelData,String columnAliasName, String tableAliasShortcut,String dataModelName,String colExpression,String queryColumnRef, List<String> whereClauseListDataModel){
		Element queryItemElement=null, sql=null,parentElemnt=null;
		String columnExpression=null,expression=null,tempValue=null,expression2=null,column=null,table=null,nameSpace=null,origExpression=null;
		Map<String, Table> physicalTableMap=null;
		ExpressionBuilder expressionBuilder=null;
		Map<String, Table> DataModelTableMap=null;
		String [] splitArray=null;
		Measures measure=null;
		DerivedColumn derivedColumn=null;
		Column columnNew=null;
		String physicalTable=null,nativeSql=null;
		List<String> whereClauseListExtract=null;
		List<String> queryColumnRefList=null;
		DataModelLayerData dataModelLayerData=null;
		try{
			expressionBuilder=new ExpressionBuilder();
			physicalTableMap=cognosModelData.getPhysicalTableMap();
			xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+namespace+"']//ns:querySubject[ns:name='"+tableName+"']//ns:queryItem[ns:name='"+columnName+"']")), Filters .element(),null,modelPo.getNs());	
			queryItemElement=xpath.evaluateFirst(modelPo.getDocument());
			if(queryItemElement!=null){
				measure=checkForMeasure(queryItemElement, modelPo);
				if(null!=measure&&!cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getMeasure().contains(measure)){
					cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getMeasure().add(measure)	;
				}
				if(queryItemElement.getChild("expression", modelPo.getNs())!=null){
					origExpression=buildExpression(queryItemElement, modelPo);
					if(!convertedExpressionMap.containsKey(origExpression)){
						expression=expressionBuilder.buildExpression(origExpression);
					}else{
						expression=convertedExpressionMap.get(origExpression);	
					}
					tempValue="";
					expression2=expression;
					int count1=0,count2=0;
					if(queryItemElement.getChild("expression", modelPo.getNs()).getChildren("refobjViaShortcut", modelPo.getNs()).size()>0){
						for (Element expr : queryItemElement.getChild("expression", modelPo.getNs()).getChildren("refobjViaShortcut", modelPo.getNs())) {
							columnExpression=expr.getChildren("refobj",modelPo.getNs()).get(1).getText();
							tableAliasShortcut=expr.getChildren("refobj",modelPo.getNs()).get(0).getText();
							if(!tableAliasShortcut.equals(tempValue)){
								tempValue=tableAliasShortcut;
								count1++;
							}
							count2++;
							if(columnExpression.startsWith("[")&&columnExpression.endsWith("]")){
								splitArray=columnExpression.split("].\\[");
								if(splitArray.length==3){
									nameSpace=splitArray[0].substring(1).trim();
									table=splitArray[1].trim();
									column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
									if(!table.equals(tableName)){
										parentElemnt=getQuerySubjectParent(queryItemElement);
										if(parentElemnt.getChild("definition", modelPo.getNs()).getChild("modelQuery", modelPo.getNs()).getChild("filters", modelPo.getNs())!=null){
											whereClauseListDataModel=new ArrayList<String>();
											for (Element filterElement : parentElemnt.getChild("definition", modelPo.getNs()).getChild("modelQuery", modelPo.getNs()).getChild("filters", modelPo.getNs()).getChildren("filterDefinition", modelPo.getNs())) {
												whereClauseListDataModel.add(extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement, dataModelName, tableName));
											}
										}
									}
									expression=expression.replace(columnExpression, column.contains(" ")?"\""+column+"\"":column);
									expression2=expression2.replace(columnExpression, "##"+tableAliasShortcut.split("]\\.")[1].substring(0, tableAliasShortcut.split("]\\.")[1].length()-1)+"."+column+"]##");
									if(queryItemElement.getChild("expression", modelPo.getNs()).getChildren("refobjViaShortcut", modelPo.getNs()).size()==1){
										expression=expression.replace("[", "").replace("]", "");
										cognosModelData=extractQueryItem(xmlParser, xpath, modelPo, nameSpace,table, column,cognosModelData,columnAliasName,tableAliasShortcut, dataModelName,expression,queryColumnRef,whereClauseListDataModel);
									}else{
										cognosModelData=extractQueryItem(xmlParser, xpath, modelPo, nameSpace,table, column,cognosModelData,column,tableAliasShortcut, dataModelName,null,queryColumnRef,whereClauseListDataModel);
									}
								}
							}
						}
					} if(queryItemElement.getChild("expression", modelPo.getNs()).getChildren("refobj", modelPo.getNs()).size()>0){
						for (Element expr : queryItemElement.getChild("expression", modelPo.getNs()).getChildren("refobj", modelPo.getNs())) {
							columnExpression=expr.getText();
							if(columnExpression.startsWith("[")&&columnExpression.endsWith("]")){
								splitArray=columnExpression.split("].\\[");
								if(splitArray.length==3){
									tableAliasShortcut=splitArray[0].trim()+"].["+splitArray[1].trim()+"]";
									if(!tableAliasShortcut.equals(tempValue)){
										tempValue=tableAliasShortcut;
										count1++;
									}
									count2++;
									nameSpace=splitArray[0].substring(1).trim();
									table=splitArray[1].trim();
									column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
									if(!table.equals(tableName)){
										parentElemnt=getQuerySubjectParent(queryItemElement);
										if(parentElemnt.getChild("definition", modelPo.getNs()).getChild("modelQuery", modelPo.getNs()).getChild("filters", modelPo.getNs())!=null){
											whereClauseListDataModel=new ArrayList<String>();
											for (Element filterElement : parentElemnt.getChild("definition", modelPo.getNs()).getChild("modelQuery", modelPo.getNs()).getChild("filters", modelPo.getNs()).getChildren("filterDefinition", modelPo.getNs())) {
												whereClauseListDataModel.add(extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement, dataModelName, tableName));
											}
										}
									}
									expression=expression.replace(columnExpression, column.contains(" ")?"\""+column+"\"":column);
									expression2=expression2.replace(columnExpression, "##"+tableAliasShortcut.split("]\\.")[1].substring(0, tableAliasShortcut.split("]\\.")[1].length()-1)+"."+column+"]##");
									if(queryItemElement.getChild("expression", modelPo.getNs()).getChildren("refobj", modelPo.getNs()).size()==1){
										expression=expression.replace("[", "").replace("]", "");
										cognosModelData=extractQueryItem(xmlParser, xpath, modelPo, nameSpace,table, column,cognosModelData,columnAliasName,tableAliasShortcut, dataModelName,expression,queryColumnRef,whereClauseListDataModel);
									}else{
										cognosModelData=extractQueryItem(xmlParser, xpath, modelPo,nameSpace, table, column,cognosModelData,column,tableAliasShortcut, dataModelName,null,queryColumnRef,whereClauseListDataModel);
									}
								}
							}
						}  
					}
					convertedExpressionMap.put(origExpression, expression);
					if(count1>1){
						derivedColumn=new DerivedColumn();
						System.out.println(" Derived Column Name-----------------------"+columnName);
						System.out.println(" Derived Column Expression-----------------------"+expression);
						derivedColumn.setColumnName(columnName);
						derivedColumn.setExpression(expression2);
						derivedColumn.setQueryColumnRef(queryColumnRef);
						if(!cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getDerivedColumn().contains(derivedColumn)){
							cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getDerivedColumn().add(derivedColumn);
						}
					}else if(count2>1){
						columnNew=new Column();
						columnNew.setColumnName(columnName);
						columnNew.setColumnExpression(expression);
						queryColumnRefList=new ArrayList<>();
						queryColumnRefList.add(queryColumnRef);
						columnNew.setQueryColumnRef(queryColumnRefList);
						tableName=tempValue.split("]\\.\\[")[1].substring(0, tempValue.split("]\\.\\[")[1].length()-1);
						if(cognosModelData.getDataModelLayerDatas().get(dataModelName).getTables().get(tableName)!=null){
							cognosModelData.getDataModelLayerDatas().get(dataModelName).getTables().get(tableName).getColumnMap().put(columnName, columnNew);
						}else{
							tableName=cognosModelData.getExtractLayerData().getModifiedTableMap().get(tableName);
							if(cognosModelData.getDataModelLayerDatas().get(dataModelName).getTables().get(tableName)!=null){
								cognosModelData.getDataModelLayerDatas().get(dataModelName).getTables().get(tableName).getColumnMap().put(columnName, columnNew);
							}else{
								derivedColumn=new DerivedColumn();
								System.out.println(" Derived Column Name-----------------------"+columnName);
								System.out.println(" Derived Column Expression-----------------------"+expression);
								derivedColumn.setColumnName(columnName);
								derivedColumn.setExpression(expression2);
								if(!cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getDerivedColumn().contains(derivedColumn)){
									cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getDerivedColumn().add(derivedColumn);
								}
							}
						}
					}
				}else{
					sql=queryItemElement.getParentElement().getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs());
					if(null==sql.getChild("table", modelPo.getNs())){
						if(!sql.getAttributeValue("type").equals("passThrough")){
							if(sql.getTextTrim().startsWith("{")){
								physicalTable=queryItemElement.getParentElement().getChildText("name", modelPo.getNs()).trim(); 
								nativeSql=sql.getText().trim();
								nativeSql=nativeSql.substring(1, nativeSql.length()-2);
							}else{
								physicalTable=queryItemElement.getParentElement().getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim(); 
							}
						}
					}else{
						physicalTable=sql.getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
					}
					if(null!=physicalTable){
						if(!physicalTable.trim().equals(queryItemElement.getParentElement().getChildText("name", modelPo.getNs()).trim())){
							cognosModelData.getExtractLayerData().getModifiedTableMap().put(queryItemElement.getParentElement().getChildText("name", modelPo.getNs()).trim(), physicalTable);
						}
						measure=checkForMeasure(queryItemElement, modelPo);
						if(null!=measure&&!cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getMeasure().contains(measure)){
							cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getMeasure().add(measure)	;
						}
						if(cognosModelData.getExtractLayerData().getTables()!=null){
							physicalTableMap=cognosModelData.getExtractLayerData().getTables();
						}else{
							physicalTableMap=new HashMap<>();
						}
						if(!(physicalTableMap.containsKey(physicalTable)&&physicalTableMap.get(physicalTable).getWhereClauseList()!=null)){
							whereClauseListExtract=new ArrayList<String>();
							if(queryItemElement.getParentElement().getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs())!=null){
								for (Element filterElement  : queryItemElement.getParentElement().getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs()).getChildren("filterDefinition", modelPo.getNs())) {
									whereClauseListExtract.add(extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement, dataModelName, null));
								}
							}
						}
						physicalTableMap=addColumnToPhysicaltableMap(physicalTableMap, physicalTable, nativeSql, columnName,whereClauseListExtract);
						if(!cognosModelData.getDataModelLayerDatas().containsKey(dataModelName)){
							dataModelLayerData=new DataModelLayerData();
							dataModelLayerData.setDataModelName(dataModelName);
							cognosModelData.getDataModelLayerDatas().put(dataModelName, dataModelLayerData);
						}
						if(cognosModelData.getDataModelLayerDatas().get(dataModelName).getTables()!=null){
							DataModelTableMap=cognosModelData.getDataModelLayerDatas().get(dataModelName).getTables();
						}else{
							DataModelTableMap=new HashMap<>();
						}
						DataModelTableMap=addColumnToDataModeltableMap(DataModelTableMap, physicalTable, tableAliasShortcut, columnName, columnAliasName, queryColumnRef, colExpression,whereClauseListDataModel);
						cognosModelData.getExtractLayerData().setTables(physicalTableMap);
						cognosModelData.getDataModelLayerDatas().get(dataModelName).setTables(DataModelTableMap);
						
						Set<String> key = cognosModelData.getCognosReportData().getCatalogQueryMap().keySet();
						for (String keyName : key) {
							Map<String, Map<String, List<CatalogColumn>>> namespaceMap = cognosModelData.getCognosReportData().getCatalogQueryMap().get(keyName);
							for (String namespaceName : namespaceMap.keySet()) {
								Map<String, List<CatalogColumn>> tableMap = namespaceMap.get(namespaceName);
								for (String tableNameReportData : tableMap.keySet()) {
									List<CatalogColumn> catalogColumnList = tableMap.get(tableNameReportData);
									for (CatalogColumn catalogColumn : catalogColumnList) {
										if (columnAliasName.equals(catalogColumn.getColumnName())) {
											cognosModelData.getExtractLayerData().getTables().get(tableName).getColumnMap().get(columnName).setColumnBusinessName(columnAliasName);
											System.out.println("Column Name :" + columnName + " ::Column Business Name: " + catalogColumn.getBusinessColumnName());
										}
									}
								}
							}
						}
						
						
					}
				}	
			}else{
				System.out.println("column with name "+columnName+" is not available in the table with name "+tableName);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			queryColumnRefList=null;
			dataModelLayerData=null;
			expressionBuilder=null;
			measure=null;
			queryItemElement=null;
			columnExpression=null;
			physicalTableMap=null;
			sql=null;
			expression=null;
			tempValue=null;
			expression2=null;
			column=null;
			table=null;
			nameSpace=null;
			DataModelTableMap=null;
			splitArray=null;
			measure=null;
			derivedColumn=null;
			columnNew=null;
			physicalTable=null;
			nativeSql=null;
		}
		return cognosModelData;
	}

	public CognosModelData extractJoins(XmlParser xmlParser,XPathExpression<Element> xpath,ParserObject modelPo,CognosModelData cognosModelData){
		List<Element> relationshipList=null,refobjViaShortcut=null;
		List<Join> joinList=null;
		Map<String,Table> physicalTableMap=null,newTablesExtract=null,newTablesDataModel=null;
		Join join=null,joinCopy=null;
		DataModelLayerData dataModelLayerData=null;
		String left=null,right=null,tableName=null,columnName=null,tableAliasName=null;
		List<String> leftColumns=null,rightColumns=null;
		String [] splitArray=null;
		Element tableElement=null, queryItemElement=null;
		Table table=null;
		Column column=null;
		List<String> whereClauseList=null;
		Map<String,String> modifiedTableMap=null;
		String tableNameModified=null;
		try{
			modifiedTableMap=new HashMap<>();
			physicalTableMap=cognosModelData.getExtractLayerData().getTables();
			joinList=new ArrayList<>();
			xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:relationship")), Filters .element(),null,modelPo.getNs());	
			relationshipList=xpath.evaluate(modelPo.getDocument());
			newTablesExtract=new HashMap<>();
			for (String dataModelName : cognosModelData.getDataModelLayerDatas().keySet()) {
				newTablesDataModel=new HashMap<>();
				dataModelLayerData=cognosModelData.getDataModelLayerDatas().get(dataModelName);
				if(dataModelLayerData.getTables()!=null && dataModelLayerData.getTables().size()>0) {
					joinList=new ArrayList<>();
					for (Element relation : relationshipList) {
						String joinName = relation.getChildText("name", modelPo.getNs());
						if(relation.getChild("expression", modelPo.getNs()).getChildren("refobjViaShortcut", modelPo.getNs()).size()>0){
							/*************************************Logic for database layer join starts here*******************************************************/
							left=relation.getChild("left",modelPo.getNs()).getChildText("refobj",modelPo.getNs());
							right=relation.getChild("right",modelPo.getNs()).getChildText("refobj",modelPo.getNs());
							boolean leftFlag=false,rightFlag=false;
							for (Table table1 : dataModelLayerData.getTables().values()) {
								if(table1.getAliasNameShortcut().equals(left)){
									leftFlag=true;
									break;
								}
							}
							for (Table table1 : dataModelLayerData.getTables().values()) {
								if(table1.getAliasNameShortcut().equals(right)){
									rightFlag=true;
									break;
								}
							}
							if(leftFlag || rightFlag){
								join=new Join();
								refobjViaShortcut=relation.getChild("expression", modelPo.getNs()).getChildren("refobjViaShortcut", modelPo.getNs());
								leftColumns=new ArrayList<>();
								rightColumns=new ArrayList<>();
								int count=1;
								for (Element element : refobjViaShortcut) {
									splitArray=element.getChildren("refobj", modelPo.getNs()).get(1).getText().split("].\\[");
									tableName=splitArray[1].trim();
									columnName=splitArray[2].substring(0,splitArray[2].length()-1).trim();
									tableAliasName=element.getChildren("refobj", modelPo.getNs()).get(0).getText().split("].\\[")[1].replace("]", "");
									if(count==1){
										if(!leftFlag){
											xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+tableName+"']")), Filters .element(),null,modelPo.getNs());	
											tableElement=xpath.evaluateFirst(modelPo.getDocument());
											if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
											}else{
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
											}
											table=new Table();
											table.setTableName(tableAliasName);
											table.setPhysicalTableName(tableName);
											table.setAliasNameShortcut(left);
											newTablesDataModel.put(tableAliasName, table);
											newTablesDataModel.put(tableAliasName, table);
										}
										join.setLeftTable(tableAliasName);
									}else if(count==2){
										if(!rightFlag){
											xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+tableName+"']")), Filters .element(),null,modelPo.getNs());	
											tableElement=xpath.evaluateFirst(modelPo.getDocument());
											if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
											}else{
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
											}
											table=new Table();
											table.setTableName(tableAliasName);
											table.setPhysicalTableName(tableName);
											table.setAliasNameShortcut(right);
											newTablesDataModel.put(tableAliasName, table);
										}
										join.setRightTable(tableAliasName);
									}
									if(join.getLeftTable().equals(tableAliasName)){
										if(!leftColumns.contains(columnName))
											leftColumns.add(columnName);
									}else{
										if(!rightColumns.contains(columnName))
											rightColumns.add(columnName);
									}
									count++;
									if(physicalTableMap.containsKey(tableName)){
										if(!physicalTableMap.get(tableName).getColumnMap().containsKey(columnName)){
											column=new Column();
											column.setColumnName(columnName);
											physicalTableMap.get(tableName).getColumnMap().put(columnName, column);
										}
									}else if(modifiedTableMap.containsKey(tableName)){
										if(physicalTableMap.containsKey(modifiedTableMap.get(tableName))){
											if(!physicalTableMap.get(modifiedTableMap.get(tableName)).getColumnMap().containsKey(columnName)){
												column=new Column();
												column.setColumnName(columnName);
												physicalTableMap.get(modifiedTableMap.get(tableName)).getColumnMap().put(columnName, column);
											}
										}else{
											if(!(newTablesExtract.containsKey(tableName)&&newTablesExtract.get(tableName).getWhereClauseList()!=null)){
												whereClauseList=new ArrayList<String>();
												if(tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs())!=null){
													for (Element filterElement  : tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs()).getChildren("filterDefinition", modelPo.getNs())) {
														whereClauseList.add(extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement, dataModelName, null));
													}
												}
											}
											newTablesExtract=addColumnToPhysicaltableMap(newTablesExtract, tableName, null, columnName,whereClauseList);
										}
									}else{

										tableNameModified=tableName;
										xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+splitArray[1]+"']")), Filters .element(),null,modelPo.getNs());	
										tableElement=xpath.evaluateFirst(modelPo.getDocument());
										if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
											tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
										}else{
											tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
										}
										modifiedTableMap.put(tableNameModified, tableName);
										if(physicalTableMap.containsKey(tableName)){
											if(!physicalTableMap.get(tableName).getColumnMap().containsKey(columnName)){
												column=new Column();
												column.setColumnName(columnName);
												physicalTableMap.get(tableName).getColumnMap().put(columnName, column);
											}
										}else{
											if(!(newTablesExtract.containsKey(tableName)&&newTablesExtract.get(tableName).getWhereClauseList()!=null)){
												whereClauseList=new ArrayList<String>();
												if(tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs())!=null){
													for (Element filterElement  : tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs()).getChildren("filterDefinition", modelPo.getNs())) {
														whereClauseList.add(extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement, dataModelName, null));
													}
												}
											}
											newTablesExtract=addColumnToPhysicaltableMap(newTablesExtract, tableName, null, columnName,whereClauseList);
										}
									}
								}
								if(!join.getLeftTable().equals(join.getRightTable())){
									join.setRightColumn(rightColumns);
									join.setLeftColumn(leftColumns);
//									join.setKeyAlias(join.getLeftTable()+"_"+join.getRightTable()+"_Key");
									join.setKeyAlias(joinName);
									if(!joinList.contains(join))
										joinList.add(join);
									if(leftFlag && dataModelLayerData.getTables().get(join.getLeftTable()).getQlikAliasCount()>0){
										for (int i = 1; i <=dataModelLayerData.getTables().get(join.getLeftTable()).getQlikAliasCount(); i++) {
											joinCopy=join.clone();
											joinCopy.setLeftTable(joinCopy.getLeftTable()+"_Copy"+i);
											joinCopy.setKeyAlias(joinName);
											if(!joinList.contains(joinCopy))
												joinList.add(joinCopy);
										}
									}
									if(rightFlag && dataModelLayerData.getTables().get(join.getRightTable()).getQlikAliasCount()>0){
										for (int i = 1; i <=dataModelLayerData.getTables().get(join.getRightTable()).getQlikAliasCount(); i++) {
											joinCopy=join.clone();
											joinCopy.setRightTable(joinCopy.getRightTable()+"_Copy"+i);
											joinCopy.setKeyAlias(joinName);
											if(!joinList.contains(joinCopy))
												joinList.add(joinCopy);
										}
									}
								}
							}
							/*************************************Logic for database layer join ends here*******************************************************/

						}else{
							/******Logic for joins in business layer starts here**********/
							left=relation.getChild("left",modelPo.getNs()).getChildText("refobj",modelPo.getNs()).split("]\\.\\[")[1].replace("]","");
							right=relation.getChild("right",modelPo.getNs()).getChildText("refobj",modelPo.getNs()).split("]\\.\\[")[1].replace("]","");
							boolean leftFlag=false,rightFlag=false;
							if(dataModelLayerData.getTables().containsKey(left)){
								leftFlag=true;
							}
							if(dataModelLayerData.getTables().containsKey(right)){
								rightFlag=true;
							}
							if(leftFlag || rightFlag){
								join=new Join();
								refobjViaShortcut=relation.getChild("expression", modelPo.getNs()).getChildren("refobj", modelPo.getNs());
								leftColumns=new ArrayList<>();
								rightColumns=new ArrayList<>();
								int count=1;
								for (Element element : refobjViaShortcut) {
									splitArray=element.getText().split("].\\[");
									tableName=splitArray[1].trim();
									columnName=splitArray[2].substring(0,splitArray[2].length()-1).trim();
									tableAliasName=tableName;
									if(count==1){
										if(!leftFlag){
											xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+tableName+"']//ns:queryItem[ns:name='"+columnName+"']")), Filters .element(),null,modelPo.getNs());	
											queryItemElement=xpath.evaluateFirst(modelPo.getDocument());
											if(null!=queryItemElement.getChild("expression", modelPo.getNs())){
												splitArray=buildExpression(queryItemElement, modelPo).trim().split("].\\[");
												tableName=splitArray[1].trim();
												columnName=splitArray[2].substring(0,splitArray[2].length()-1).trim();	
											}
											xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+tableName+"']")), Filters .element(),null,modelPo.getNs());	
											tableElement=xpath.evaluateFirst(modelPo.getDocument());
											if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
											}else{
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
											}
											table=new Table();
											table.setTableName(tableAliasName);
											table.setPhysicalTableName(tableName);
											table.setAliasNameShortcut(left);
											newTablesDataModel.put(tableAliasName, table);
											newTablesDataModel.put(tableAliasName, table);
										}
										join.setLeftTable(tableAliasName);
									}else if(count==2){
										if(!rightFlag){
											xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+tableName+"']//ns:queryItem[ns:name='"+columnName+"']")), Filters .element(),null,modelPo.getNs());	
											queryItemElement=xpath.evaluateFirst(modelPo.getDocument());
											if(null!=queryItemElement.getChild("expression", modelPo.getNs())){
												splitArray=buildExpression(queryItemElement, modelPo).trim().split("].\\[");
												tableName=splitArray[1].trim();
												columnName=splitArray[2].substring(0,splitArray[2].length()-1).trim();	
											}
											xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+tableName+"']")), Filters .element(),null,modelPo.getNs());	
											tableElement=xpath.evaluateFirst(modelPo.getDocument());
											if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
											}else{
												tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
											}
											table=new Table();
											table.setTableName(tableAliasName);
											table.setPhysicalTableName(tableName);
											table.setAliasNameShortcut(right);
											newTablesDataModel.put(tableAliasName, table);
										}
										join.setRightTable(tableAliasName);
									}
									if(join.getLeftTable().equals(tableAliasName)){
										if(!leftColumns.contains(columnName))
											leftColumns.add(columnName);
									}else{
										if(!rightColumns.contains(columnName))
											rightColumns.add(columnName);
									}
									count++;
									if(physicalTableMap.containsKey(tableName)){
										if(!physicalTableMap.get(tableName).getColumnMap().containsKey(columnName)){
											column=new Column();
											column.setColumnName(columnName);
											physicalTableMap.get(tableName).getColumnMap().put(columnName, column);
										}
									}else if(modifiedTableMap.containsKey(tableName)){
										if(physicalTableMap.containsKey(modifiedTableMap.get(tableName))){
											if(!physicalTableMap.get(modifiedTableMap.get(tableName)).getColumnMap().containsKey(columnName)){
												column=new Column();
												column.setColumnName(columnName);
												physicalTableMap.get(modifiedTableMap.get(tableName)).getColumnMap().put(columnName, column);
											}
										}else{
											if(!(newTablesExtract.containsKey(tableName)&&newTablesExtract.get(tableName).getWhereClauseList()!=null)){
												whereClauseList=new ArrayList<String>();
												if(tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs())!=null){
													for (Element filterElement  : tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs()).getChildren("filterDefinition", modelPo.getNs())) {
														whereClauseList.add(extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement, dataModelName, null));
													}
												}
											}
											newTablesExtract=addColumnToPhysicaltableMap(newTablesExtract, tableName, null, columnName,whereClauseList);
										}
									}else{

										tableNameModified=tableName;
										xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+splitArray[0].replace("[","")+"']//ns:querySubject[ns:name='"+splitArray[1]+"']")), Filters .element(),null,modelPo.getNs());	
										tableElement=xpath.evaluateFirst(modelPo.getDocument());
										if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
											tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
										}else{
											tableName=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
										}
										modifiedTableMap.put(tableNameModified, tableName);
										if(physicalTableMap.containsKey(tableName)){
											if(!physicalTableMap.get(tableName).getColumnMap().containsKey(columnName)){
												column=new Column();
												column.setColumnName(columnName);
												physicalTableMap.get(tableName).getColumnMap().put(columnName, column);
											}
										}else{
											if(!(newTablesExtract.containsKey(tableName)&&newTablesExtract.get(tableName).getWhereClauseList()!=null)){
												whereClauseList=new ArrayList<String>();
												if(tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs())!=null){
													for (Element filterElement  : tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("filters", modelPo.getNs()).getChildren("filterDefinition", modelPo.getNs())) {
														whereClauseList.add(extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement, dataModelName, null));
													}
												}
											}
											newTablesExtract=addColumnToPhysicaltableMap(newTablesExtract, tableName, null, columnName,whereClauseList);
										}
									}
								}
								if(!join.getLeftTable().equals(join.getRightTable())){
									join.setRightColumn(rightColumns);
									join.setLeftColumn(leftColumns);
									join.setKeyAlias(joinName);
									if(!joinList.contains(join))
										joinList.add(join);
									if(leftFlag && dataModelLayerData.getTables().get(join.getLeftTable()).getQlikAliasCount()>0){
										for (int i = 1; i <=dataModelLayerData.getTables().get(join.getLeftTable()).getQlikAliasCount(); i++) {
											joinCopy=join.clone();
											joinCopy.setLeftTable(joinCopy.getLeftTable()+"_Copy"+i);
											joinCopy.setKeyAlias(joinName);
											if(!joinList.contains(joinCopy))
												joinList.add(joinCopy);
										}
									}
									if(rightFlag && dataModelLayerData.getTables().get(join.getRightTable()).getQlikAliasCount()>0){
										for (int i = 1; i <=dataModelLayerData.getTables().get(join.getRightTable()).getQlikAliasCount(); i++) {
											joinCopy=join.clone();
											joinCopy.setRightTable(joinCopy.getRightTable()+"_Copy"+i);
											joinCopy.setKeyAlias(joinName);
											if(!joinList.contains(joinCopy))
												joinList.add(joinCopy);
										}
									}
								}
							}
							/*************************Logic for business layer join ends here*********************************************/
						}
					}
				}
				if(newTablesDataModel!=null && newTablesDataModel.size()>0)
					dataModelLayerData.getTables().putAll(newTablesDataModel);
				dataModelLayerData.setJoinList(joinList);
				cognosModelData.getDataModelLayerDatas().put(dataModelName, dataModelLayerData);
			}
			if(newTablesExtract!=null && newTablesExtract.size()>0){
				for (String tableName1 : newTablesExtract.keySet()) {
					if(!physicalTableMap.containsKey(tableName1))
						physicalTableMap.put(tableName1,newTablesExtract.get(tableName1));
				}
			}
			cognosModelData.getExtractLayerData().setTables(physicalTableMap);
		}catch(Exception e){
			e.printStackTrace(); 
		}finally{
			queryItemElement=null;
			tableNameModified=null;
			relationshipList=null;
			refobjViaShortcut=null;
			joinList=null;
			physicalTableMap=null;
			newTablesExtract=null;
			newTablesDataModel=null;
			join=null;
			joinCopy=null;
			dataModelLayerData=null;
			left=null;
			right=null;
			tableName=null;
			columnName=null;
			tableAliasName=null;
			leftColumns=null;
			rightColumns=null;
			whereClauseList=null;
			splitArray=null;
			tableElement=null;
			table=null;
			column=null;
		}
		return cognosModelData;
	}

	private CognosModelData extractFilters(XmlParser xmlParser,XPathExpression<Element> xpath, ParserObject modelPo,CognosReportData cognosReportData, CognosModelData cognosModelData) {
		Element namespaceElement=null,shortcutElement=null,filterElement=null;
		ExpressionBuilder expressionBuilder=null;
		List<ModelFilter> modelFilters=null;
		ModelFilter modelFilter=null;
		Map<String,Map<String, List<String>>> filterReferences=null;
		String refObj=null,targetType=null,namespace=null,expression=null;
		try{
			expressionBuilder=new ExpressionBuilder();
			filterReferences=cognosReportData.getFilterReferences();
			for(String dataModelName :filterReferences .keySet()){
				modelFilters=cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getModelFilter();
				for (String namespaceRef :filterReferences.get(dataModelName).keySet()) {
					xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:shortcut[ns:name='"+namespaceRef+"']")), Filters .element(),null,modelPo.getNs());
					shortcutElement=xpath.evaluateFirst(modelPo.getDocument());
					if(shortcutElement!=null){
						refObj=shortcutElement.getChildText("refobj", modelPo.getNs()).replace("[", "").replace("]", "");
						targetType=shortcutElement.getChildText("targetType", modelPo.getNs());
						if(targetType.equals("namespace")){
							xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+refObj+"']")), Filters .element(),null,modelPo.getNs());
							namespaceElement=xpath.evaluateFirst(modelPo.getDocument());
							namespace=namespaceElement.getChildText("name", modelPo.getNs());
							for (String filterName : filterReferences.get(dataModelName).get(namespaceRef)) {
								xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+namespace+"']//ns:shortcut[ns:name='"+filterName+"']")), Filters .element(),null,modelPo.getNs());
								shortcutElement=xpath.evaluateFirst(modelPo.getDocument());
								if(shortcutElement==null){
									xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+namespace+"']//ns:filter[ns:name='"+filterName+"']")), Filters .element(),null,modelPo.getNs());	
									filterElement=xpath.evaluateFirst(modelPo.getDocument());
									if(null!=filterElement){
										cognosModelData=extractFilterExpression(xmlParser, xpath, modelPo, cognosModelData,filterElement, dataModelName);
									}else{
									}
								}else{
									refObj=shortcutElement.getChildText("refobj", modelPo.getNs()).replace("[", "").replace("]", "");
									targetType=shortcutElement.getChildText("targetType", modelPo.getNs());
									if(targetType.equals("filter")){
										xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+refObj.split("\\.")[0]+"']//ns:filter[ns:name='"+refObj.split("\\.")[1]+"']")), Filters .element(),null,modelPo.getNs());	
										filterElement=xpath.evaluateFirst(modelPo.getDocument());
										expression=buildExpression(filterElement, modelPo);
										filterName=shortcutElement.getChildText("name", modelPo.getNs());
										if(!convertedExpressionMap.containsKey(expression)){
											String key=expression;
											expression=expressionBuilder.buildExpression(expression);
											convertedExpressionMap.put(key, expression);
										}else{
											expression=convertedExpressionMap.get(expression);	
										}
										modelFilter=new ModelFilter();
										modelFilter.setFilterName(filterName);
										modelFilter.setExpression(filterElement.getChildText("name", modelPo.getNs()));
										if(!modelFilters.contains(modelFilter)){
											modelFilters.add(modelFilter);
										}
										if(null!=filterElement){
											cognosModelData=extractFilterExpression(xmlParser, xpath, modelPo, cognosModelData,filterElement, dataModelName);
										}
									}
								}
							}
						}
					}
				}
				cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).setModelFilter(modelFilters);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			namespaceElement=null;
			shortcutElement=null;
			filterElement=null;
			expressionBuilder=null;
			modelFilters=null;
			modelFilter=null;
			filterReferences=null;
			refObj=null;
			targetType=null;
			namespace=null;
			expression=null;
		}
		return cognosModelData;
	}
	public CognosModelData extractFilterExpression(XmlParser xmlParser,XPathExpression<Element> xpath,ParserObject modelPo,CognosModelData cognosModelData, Element filterElement,String dataModelName){
		ExpressionBuilder expressionBuilder=null;
		List<ModelFilter> modelFilters=null;
		ModelFilter modelFilter=null;
		String expression=null,filterName=null,columnExpression=null,tableAliasShortcut=null,refObj=null,columnAlias=null,nameSpace=null,column=null,table=null;
		List<String> tableAliasShortcuts=null;
		String [] splitArray=null;
		Element filterElement2=null;
		try{
			modelFilters=cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).getModelFilter();
			expressionBuilder=new ExpressionBuilder();
			if(null!=filterElement.getChild("expression",modelPo.getNs())){
				expression=buildExpression(filterElement, modelPo);
				filterName=filterElement.getChildText("name", modelPo.getNs());
				if(!convertedExpressionMap.containsKey(expression)){
					String key=expression;
					expression=expressionBuilder.buildExpression(expression);
					convertedExpressionMap.put(key, expression);
				}else{
					expression=convertedExpressionMap.get(expression);	
				}
				tableAliasShortcuts=new ArrayList<>();
				for (Element expr : filterElement.getChild("expression", modelPo.getNs()).getChildren()) {
					if(expr.getName().equals("refobjViaShortcut")){
						columnExpression=expr.getChildren("refobj",modelPo.getNs()).get(1).getText();
						tableAliasShortcut=expr.getChildren("refobj",modelPo.getNs()).get(0).getText();
						if(!tableAliasShortcuts.contains(tableAliasShortcut)){
							tableAliasShortcuts.add(tableAliasShortcut);
						}
						if(columnExpression.startsWith("[")&&columnExpression.endsWith("]")){
							splitArray=columnExpression.split("].\\[");
							if(splitArray.length==3){
								nameSpace=splitArray[0].substring(1).trim();
								table=splitArray[1].trim();
								column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
								cognosModelData=extractQueryItem(xmlParser, xpath, modelPo, nameSpace,table, column,cognosModelData,column,tableAliasShortcut, dataModelName,null,columnExpression,null);
								columnAlias="##"+tableAliasShortcut.split("]\\.")[1].substring(0, tableAliasShortcut.split("]\\.")[1].length()-1)+"."+column+"]##";
								expression=expression.replace(columnExpression, columnAlias.contains(" ")?"\""+columnAlias+"\"":columnAlias);
							}
						}
					}else if(expr.getName().equals("refobj")){
						refObj=expr.getText();
						expression=expression.replace(refObj, refObj.split("].\\[")[1].substring(0,refObj.split("].\\[")[1].length()-1).trim());
						xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+refObj.split("]\\.\\[")[0].replace("[", "")+"']//ns:filter[ns:name='"+refObj.split("]\\.\\[")[1].replace("]", "")+"']")), Filters .element(),null,modelPo.getNs());	
						filterElement2=xpath.evaluateFirst(modelPo.getDocument());
						cognosModelData=extractFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement2, dataModelName);
						expression=expression.replace("[", "").replace("]","");
					}	
				}
				modelFilter=new ModelFilter();
				modelFilter.setFilterName(filterName);
				modelFilter.setExpression(expression);
				if(!modelFilters.contains(modelFilter)){
					modelFilters.add(modelFilter);
				}
				cognosModelData.getSymenticLayerOutput().getDataModelLayer().get(dataModelName).setModelFilter(modelFilters);
			}	
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			expressionBuilder=null;
			modelFilters=null;
			modelFilter=null;
			expression=null;
			filterName=null;
			columnExpression=null;
			tableAliasShortcut=null;
			refObj=null;
			columnAlias=null;
			nameSpace=null;
			column=null;
			table=null;
			tableAliasShortcuts=null;
			splitArray=null;
			filterElement2=null;
		}
		return cognosModelData;
	}
	public String extractTableFilterExpression(XmlParser xmlParser,XPathExpression<Element> xpath,ParserObject modelPo,CognosModelData cognosModelData, Element filterElement,String dataModelName,String derivedTableName){
		ExpressionBuilder expressionBuilder=null;
		String expression=null,columnExpression=null,tableAliasShortcut=null,refObj=null,nameSpace=null,column=null,table=null,origExpression=null;
		String [] splitArray=null;
		Element filterElement2=null,tableElement=null;
		try{
			expressionBuilder=new ExpressionBuilder();
			if(null!=filterElement.getChild("expression",modelPo.getNs())){
				origExpression=buildExpression(filterElement, modelPo);
				if(derivedTableName!=null){
					if(!convertedExpressionMap.containsKey(origExpression)){
						expression=expressionBuilder.buildExpression(origExpression);
					}else{
						expression=convertedExpressionMap.get(origExpression);	
					}
				}
				for (Element expr : filterElement.getChild("expression", modelPo.getNs()).getChildren()) {
					if(expr.getName().equals("refobjViaShortcut")){
						columnExpression=expr.getChildren("refobj",modelPo.getNs()).get(1).getText();
						tableAliasShortcut=expr.getChildren("refobj",modelPo.getNs()).get(0).getText();
						splitArray=columnExpression.split("].\\[");
						if(splitArray.length==3){
							nameSpace=splitArray[0].substring(1).trim();
							table=splitArray[1].trim();
							column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
							xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+nameSpace+"']//ns:querySubject[ns:name='"+table+"']")), Filters .element(),null,modelPo.getNs());	
							tableElement=xpath.evaluateFirst(modelPo.getDocument());
							if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
								table=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
							}else{
								table=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
							}
							addColumnToPhysicaltableMap(cognosModelData.getExtractLayerData().getTables(), table, null, column, null);
							expression=expression.replace(columnExpression, "\""+column+"\"" );
							if(derivedTableName!=null){
								expression=tableAliasShortcut+"##"+expression;
							}
						}else if(splitArray.length==2){
							expression=expression.replace(columnExpression, columnExpression.split("].\\[")[1].substring(0,columnExpression.split("].\\[")[1].length()-1).trim());
							xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+columnExpression.split("]\\.\\[")[0].replace("[", "")+"']//ns:filter[ns:name='"+columnExpression.split("]\\.\\[")[1].replace("]", "")+"']")), Filters .element(),null,modelPo.getNs());	
							filterElement2=xpath.evaluateFirst(modelPo.getDocument());
							expression=extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement2, dataModelName,derivedTableName);
						}
					}else if(expr.getName().equals("refobj")){
						refObj=expr.getText();
						splitArray=refObj.split("].\\[");
						if(splitArray.length==3){
							nameSpace=splitArray[0].substring(1).trim();
							table=splitArray[1].trim();
							column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
							xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+nameSpace+"']//ns:querySubject[ns:name='"+table+"']")), Filters .element(),null,modelPo.getNs());	
							tableElement=xpath.evaluateFirst(modelPo.getDocument());
							if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
								table=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
							}else{
								table=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
							}
							addColumnToPhysicaltableMap(cognosModelData.getExtractLayerData().getTables(), table, null, column, null);
							expression=expression.replace(refObj, "\""+column+"\"");
							if(derivedTableName!=null){
								expression=tableAliasShortcut+"##"+expression;
							}
						}else if(splitArray.length==2){
							expression=expression.replace(refObj, refObj.split("].\\[")[1].substring(0,refObj.split("].\\[")[1].length()-1).trim());
							xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+refObj.split("]\\.\\[")[0].replace("[", "")+"']//ns:filter[ns:name='"+refObj.split("]\\.\\[")[1].replace("]", "")+"']")), Filters .element(),null,modelPo.getNs());	
							filterElement2=xpath.evaluateFirst(modelPo.getDocument());
							expression=extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement2, dataModelName,derivedTableName);
						}
					}	
				}

			}else if(filterElement.getChild("refobj", modelPo.getNs())!=null){
				expression=refObj=filterElement.getChild("refobj", modelPo.getNs()).getText();
				splitArray=refObj.split("].\\[");
				if(splitArray.length==3){
					nameSpace=splitArray[0].substring(1).trim();
					table=splitArray[1].trim();
					column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
					xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+nameSpace+"']//ns:querySubject[ns:name='"+table+"']")), Filters .element(),null,modelPo.getNs());	
					tableElement=xpath.evaluateFirst(modelPo.getDocument());
					if(null==tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChild("table", modelPo.getNs())){
						table=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChildText("sql", modelPo.getNs()).split("]\\.")[1].split(" ")[0].trim();
					}else{
						table=tableElement.getChild("definition", modelPo.getNs()).getChild("dbQuery", modelPo.getNs()).getChild("sql", modelPo.getNs()).getChildText("table", modelPo.getNs()).split("\\.")[1].split(" ")[0].trim();	
					}
					addColumnToPhysicaltableMap(cognosModelData.getExtractLayerData().getTables(), table, null, column, null);
					expression=expression.replace(refObj, "\""+column+"\"");
					if(derivedTableName!=null){
						expression=tableAliasShortcut+"##"+expression;
					}
				}else if(splitArray.length==2){
					expression=expression.replace(refObj, refObj.split("].\\[")[1].substring(0,refObj.split("].\\[")[1].length()-1).trim());
					xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:namespace[ns:name='"+refObj.split("]\\.\\[")[0].replace("[", "")+"']//ns:filter[ns:name='"+refObj.split("]\\.\\[")[1].replace("]", "")+"']")), Filters .element(),null,modelPo.getNs());	
					filterElement2=xpath.evaluateFirst(modelPo.getDocument());
					expression=extractTableFilterExpression(xmlParser, xpath, modelPo, cognosModelData, filterElement2, dataModelName,derivedTableName);
				}
			}	
			convertedExpressionMap.put(origExpression, expression);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			expressionBuilder=null;
			columnExpression=null;
			tableAliasShortcut=null;
			refObj=null;
			nameSpace=null;
			column=null;
			table=null;
			splitArray=null;
			filterElement2=null;
		}
		return expression;
	}
	public List<String> extractDatasources(XmlParser xmlParser,XPathExpression<Element> xpath,ParserObject modelPo){
		List<String> dbSchemas=null;
		List<Element> dataSources=null;
		try{
			dbSchemas=new ArrayList<>();
			xpath=XPathFactory.instance().compile((xmlParser.getExpression(modelPo,"//ns:project//ns:dataSources/ns:dataSource")), Filters .element(),null,modelPo.getNs());	
			dataSources=xpath.evaluate(modelPo.getDocument());
			for (Element dataSource : dataSources) {
				String dataSourceName = dataSource.getChildText("cmDataSource", modelPo.getNs());
				if (!"".equals(dataSourceName)) {
					dbSchemas.add(dataSourceName);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			dataSources=null;
		}
		return dbSchemas;
	}

	public String buildExpression(Element element, ParserObject modelPo){
		String expression=null,namespace=null;
		Element refobj=null;
		List<String> refobjText=null;
		String splitArray[]=null,  splitArrayFinal[]=null; 
		List<Element> refObjectViaShortcutList=null,refobjList=null;
		try{  
			namespace=modelPo.getNs().getURI().toString();
			if(element.getChild("expression", modelPo.getNs()).getChild("refobjViaShortcut", modelPo.getNs())!=null){
				expression=element.getChild("expression", modelPo.getNs()).getContent().toString().replace(namespace, "").replace("Namespace:", "").replaceAll("\n", " ");
				refObjectViaShortcutList=element.getChild("expression", modelPo.getNs()).getChildren("refobjViaShortcut", modelPo.getNs());
				refobjText=new ArrayList<String>();
				for (Element refobjViaShortcut :refObjectViaShortcutList ){
					refobj=refobjViaShortcut.getChildren("refobj", modelPo.getNs()).get(1);
					refobjText.add(refobj.getValue());
				}
				expression=expression.replace("Element: ", "");
				for (String refObj: refobjText) {
					expression=expression.replaceFirst("\\[\\<refobjViaShortcut \\[ ]/>]", refObj);
				}
			}if(element.getChild("expression", modelPo.getNs()).getChildren("refobj", modelPo.getNs()).size()>0){
				if(expression==null){
					expression=element.getChild("expression", modelPo.getNs()).getContent().toString().replace(namespace, "").replace("Namespace:", "").replaceAll("\n", " ");    	
				}
				refobjList=element.getChild("expression", modelPo.getNs()).getChildren("refobj", modelPo.getNs());
				refobjText=new ArrayList<String>();
				for(Element txt:refobjList){
					refobjText.add(txt.getValue());
				}
				expression=expression.replace("Element: ", "");
				for (String refObj: refobjText) {
					expression=expression.replaceFirst("\\[\\<refobj \\[ ]/>]", refObj);  
				}
			} else if(element.getChildText("expression", modelPo.getNs()) != null) {
				if(expression==null){
					expression=element.getChildText("expression", modelPo.getNs()).toString().replace(namespace, "").replace("Namespace:", "").replaceAll("\n", " ");    	
				}
			}
			
			if(!builtExpressionMap.containsKey(expression)){
				String key=expression;
				expression=expression.replaceAll("\\[+", "[").replaceAll("]+", "]").replace("Text:", "").replaceAll(" +", " ");
				splitArray=expression.split("], \\[");
				splitArrayFinal=splitArray;
				if(splitArray.length>1){
					expression="";
					for (int i = 0; i < splitArray.length; i++) {
						if(!splitArray[i].contains("].[")){
							splitArrayFinal[i]=splitArray[i].replace("[", "").replace("]", "");
						}else{
							if(splitArray[i].startsWith("[")){
								if(StringUtils.countMatches(splitArray[i], "[")-StringUtils.countMatches(splitArray[i], "]")==2){
									splitArrayFinal[i]=splitArray[i].replaceFirst("\\[", "")+"]";
								}else{
									splitArrayFinal[i]=splitArray[i]+"]";	
								}
							}else{
								if(splitArray[i].endsWith("]")){
									splitArrayFinal[i]="["+splitArray[i];
								}else{
									if(StringUtils.countMatches(splitArray[i], "[")==StringUtils.countMatches(splitArray[i], "]")){
										splitArrayFinal[i]="["+splitArray[i]+"]";
									}else{
										splitArrayFinal[i]=splitArray[i]+"]";
									}
								}
							}
						}
						expression=expression+splitArrayFinal[i];
					}
				}
				builtExpressionMap.put(key, expression);
			}else{
				expression=builtExpressionMap.get(expression);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			namespace=null;
			refobj=null;
			splitArray=null;
			splitArrayFinal=null; 
			refObjectViaShortcutList=null;
			refobjList=null;
			refobjText=null;
		}
		System.out.println("Expression---------------------------------"+expression);
		return expression;
	}
	private Measures checkForMeasure(Element queryItemElement,ParserObject modelPo){
		Measures measure =null;
		String numberFormat=null;
		try{
			if(null!=queryItemElement.getChild("usage", modelPo.getNs())){
				if(queryItemElement.getChildText("usage", modelPo.getNs()).equals("fact")){
					measure = new Measures();
					measure.setColumnName(queryItemElement.getChildText("name", modelPo.getNs()));
					if(queryItemElement.getChildText("datatype", modelPo.getNs()).equals("decimal")){
						numberFormat=new String("num([columnName],'#,##0.00)");
					}else if(queryItemElement.getChildText("datatype", modelPo.getNs()).equals("int32")){
						numberFormat=new String("num([columnName],'#,##0)");
					}else if(queryItemElement.getChildText("datatype", modelPo.getNs()).equals("float64")){
						numberFormat=new String("num([columnName],'#,##0.0)");
					}
					measure.setNumberFormat(numberFormat);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			numberFormat=null;
		}
		return measure;
	}
	private Map<String, Table> addColumnToPhysicaltableMap(Map<String, Table> physicalTableMap,String physicalTable,String nativeSql,String columnName, List<String> whereClauseList){
		Column column=null;
		Table table=null;
		Map<String,Column> columnMap=null;
		try{
			if(physicalTableMap==null){
				physicalTableMap=new HashMap<>();
			}
			if(physicalTableMap.containsKey(physicalTable)){
				if(!physicalTableMap.get(physicalTable).getColumnMap().containsKey(columnName)){
					column=new Column();
					column.setColumnName(columnName);
					physicalTableMap.get(physicalTable).getColumnMap().put(columnName, column);
				}
			}else{
				table=new Table();
				table.setTableName(physicalTable);
				if(whereClauseList!=null&&whereClauseList.size()>0)
					table.setWhereClauseList(whereClauseList);
				columnMap=new HashMap<>();
				column=new Column();
				column.setColumnName(columnName);
				if(null!=nativeSql){
					table.setNativeSql(nativeSql);
				}
				columnMap.put(columnName, column);
				table.setColumnMap(columnMap);
				physicalTableMap.put(physicalTable, table);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			column=null;
			table=null;
			columnMap=null;
		}
		return physicalTableMap;
	}
	private Element getQuerySubjectParent(Element element){
		try{
			if(element.getName().equals("queryItemFolder")||element.getName().equals("queryItem")){
				element=getQuerySubjectParent(element.getParentElement());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return element;
	}
	private Map<String, Table> addColumnToDataModeltableMap(Map<String, Table> DataModelTableMap,String physicalTable,String tableAliasShortcut,String columnName,String columnAliasName,String queryColumnRef,String colExpression, List<String> whereClauseListDataModel){
		Column column=null;
		Table table=null;
		String tableAliasName=null;
		List<String> queryColumnRefList=null,whereClauseList=null;
		Map<String,Column> columnMap=null;
		try{
			if(tableAliasShortcut==null){
				tableAliasShortcut=tableAliasName=physicalTable;	
			}else{
				tableAliasName=tableAliasShortcut.split("]\\.\\[")[1].substring(0, tableAliasShortcut.split("]\\.\\[")[1].length()-1);
			}
			if(whereClauseListDataModel!=null && whereClauseListDataModel.size()>0){
				whereClauseList=new ArrayList<>();
				for (String whereClause : whereClauseListDataModel) {
					if(whereClause.split("##")[0].equals(tableAliasShortcut)){
						whereClauseList.add(whereClause.split("##")[1]);
					}
				}
			}
			if(DataModelTableMap.containsKey(tableAliasName)){
				if(whereClauseList!=null&&whereClauseList.size()>0){
					if(DataModelTableMap.get(tableAliasName).getWhereClauseList()!=null&&DataModelTableMap.get(tableAliasName).getWhereClauseList().containsAll(whereClauseList)&&DataModelTableMap.get(tableAliasName).getWhereClauseList().size()==whereClauseList.size()){
						if(!DataModelTableMap.get(tableAliasName).getColumnMap().containsKey(columnAliasName)){
							column=new Column();
							column.setColumnName(columnAliasName);
							queryColumnRefList=new ArrayList<>();
							queryColumnRefList.add(queryColumnRef);
							column.setQueryColumnRef(queryColumnRefList);
							if(null==colExpression){
								colExpression=columnName.contains(" ")?"\""+columnName+"\"":columnName;
							}
							column.setColumnExpression(colExpression);;
							DataModelTableMap.get(tableAliasName).getColumnMap().put(columnAliasName, column);
						}else{
							column=DataModelTableMap.get(tableAliasName).getColumnMap().get(columnAliasName);
							if(!column.getQueryColumnRef().contains(queryColumnRef)){
								column.getQueryColumnRef().add(queryColumnRef);	
							}
							DataModelTableMap.get(tableAliasName).getColumnMap().put(columnAliasName, column);
						}
					}else{
						boolean columnAdded=false;
						for (int aliasCount=1;aliasCount <=DataModelTableMap.get(tableAliasName).getQlikAliasCount();aliasCount++) {
							if(DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getWhereClauseList()!=null&&DataModelTableMap.get(tableAliasName+"Copy"+aliasCount).getWhereClauseList().containsAll(whereClauseList)&&DataModelTableMap.get(tableAliasName+"Copy"+aliasCount).getWhereClauseList().size()==whereClauseList.size()){
								if(!DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().containsKey(columnAliasName)){
									column=new Column();
									column.setColumnName(columnAliasName);
									queryColumnRefList=new ArrayList<>();
									queryColumnRefList.add(queryColumnRef);
									column.setQueryColumnRef(queryColumnRefList);
									if(null==colExpression){
										colExpression=columnName.contains(" ")?"\""+columnName+"\"":columnName;
									}
									column.setColumnExpression(colExpression);;
									DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().put(columnAliasName, column);
								}else{
									column=DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().get(columnAliasName);
									if(!column.getQueryColumnRef().contains(queryColumnRef)){
										column.getQueryColumnRef().add(queryColumnRef);	
									}
									DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().put(columnAliasName, column);
								}
								columnAdded=true;
								break;
							}
						}
						if(!columnAdded){
							DataModelTableMap.get(tableAliasName).setQlikAliasCount(DataModelTableMap.get(tableAliasName).getQlikAliasCount()+1);
							tableAliasName=tableAliasName+"_Copy"+(DataModelTableMap.get(tableAliasName).getQlikAliasCount());
							table=new Table();
							table.setTableName(tableAliasName);
							table.setAliasNameShortcut(tableAliasShortcut);
							table.setPhysicalTableName(physicalTable);
							if(whereClauseList!=null && whereClauseList.size()>0){
								table.setWhereClauseList(whereClauseList);
							}
							columnMap=new HashMap<>();
							column=new Column();
							column.setColumnName(columnAliasName);
							if(null==colExpression){
								colExpression=columnName.contains(" ")?"\""+columnName+"\"":columnName;
							}
							column.setColumnExpression(colExpression);
							queryColumnRefList=new ArrayList<>();
							queryColumnRefList.add(queryColumnRef);
							column.setQueryColumnRef(queryColumnRefList);
							columnMap.put(columnName, column);
							table.setColumnMap(columnMap);
							DataModelTableMap.put(tableAliasName, table);
						}
					}
				}else{
					if(DataModelTableMap.get(tableAliasName).getWhereClauseList()!=null){

						boolean columnAdded=false;
						for (int aliasCount=1;aliasCount <=DataModelTableMap.get(tableAliasName).getQlikAliasCount();aliasCount++) {
							if(DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getWhereClauseList()!=null&&DataModelTableMap.get(tableAliasName+"Copy"+aliasCount).getWhereClauseList().containsAll(whereClauseList)&&DataModelTableMap.get(tableAliasName+"Copy"+aliasCount).getWhereClauseList().size()==whereClauseList.size()){
								if(!DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().containsKey(columnAliasName)){
									column=new Column();
									column.setColumnName(columnAliasName);
									queryColumnRefList=new ArrayList<>();
									queryColumnRefList.add(queryColumnRef);
									column.setQueryColumnRef(queryColumnRefList);
									if(null==colExpression){
										colExpression=columnName.contains(" ")?"\""+columnName+"\"":columnName;
									}
									column.setColumnExpression(colExpression);;
									DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().put(columnAliasName, column);
								}else{
									column=DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().get(columnAliasName);
									if(!column.getQueryColumnRef().contains(queryColumnRef)){
										column.getQueryColumnRef().add(queryColumnRef);	
									}
									DataModelTableMap.get(tableAliasName+"_Copy"+aliasCount).getColumnMap().put(columnAliasName, column);
								}
								columnAdded=true;
								break;
							}
						}
						if(!columnAdded){
							DataModelTableMap.get(tableAliasName).setQlikAliasCount(DataModelTableMap.get(tableAliasName).getQlikAliasCount()+1);
							tableAliasName=tableAliasName+"_Copy"+(DataModelTableMap.get(tableAliasName).getQlikAliasCount());
							table=new Table();
							table.setTableName(tableAliasName);
							table.setAliasNameShortcut(tableAliasShortcut);
							table.setPhysicalTableName(physicalTable);
							if(whereClauseList!=null && whereClauseList.size()>0){
								table.setWhereClauseList(whereClauseList);
							}
							columnMap=new HashMap<>();
							column=new Column();
							column.setColumnName(columnAliasName);
							if(null==colExpression){
								colExpression=columnName.contains(" ")?"\""+columnName+"\"":columnName;
							}
							column.setColumnExpression(colExpression);
							queryColumnRefList=new ArrayList<>();
							queryColumnRefList.add(queryColumnRef);
							column.setQueryColumnRef(queryColumnRefList);
							columnMap.put(columnName, column);
							table.setColumnMap(columnMap);
							DataModelTableMap.put(tableAliasName, table);
						}

					}else{
						if(!DataModelTableMap.get(tableAliasName).getColumnMap().containsKey(columnAliasName)){
							column=new Column();
							column.setColumnName(columnAliasName);
							queryColumnRefList=new ArrayList<>();
							queryColumnRefList.add(queryColumnRef);
							column.setQueryColumnRef(queryColumnRefList);
							if(null==colExpression){
								colExpression=columnName.contains(" ")?"\""+columnName+"\"":columnName;
							}
							column.setColumnExpression(colExpression);;
							DataModelTableMap.get(tableAliasName).getColumnMap().put(columnAliasName, column);
						}else{
							column=DataModelTableMap.get(tableAliasName).getColumnMap().get(columnAliasName);
							if(!column.getQueryColumnRef().contains(queryColumnRef)){
								column.getQueryColumnRef().add(queryColumnRef);	
							}
							DataModelTableMap.get(tableAliasName).getColumnMap().put(columnAliasName, column);
						}
					}
				}
			}else{
				table=new Table();
				table.setTableName(tableAliasName);
				table.setAliasNameShortcut(tableAliasShortcut);
				table.setPhysicalTableName(physicalTable);
				if(whereClauseList!=null && whereClauseList.size()>0){
					table.setWhereClauseList(whereClauseList);
				}
				columnMap=new HashMap<>();
				column=new Column();
				column.setColumnName(columnAliasName);
				if(null==colExpression){
					colExpression=columnName.contains(" ")?"\""+columnName+"\"":columnName;
				}
				column.setColumnExpression(colExpression);
				queryColumnRefList=new ArrayList<>();
				queryColumnRefList.add(queryColumnRef);
				column.setQueryColumnRef(queryColumnRefList);
				columnMap.put(columnAliasName, column);
				table.setColumnMap(columnMap);
				DataModelTableMap.put(tableAliasName, table);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			column=null;
			table=null;
			tableAliasName=null;
			queryColumnRefList=null;
			columnMap=null;
		}
		return DataModelTableMap;
	}
}

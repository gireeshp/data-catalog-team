/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;
import java.util.Map;

public class CognosReportData {

	private String packageName;
	private Map<String, Map<String, Map<String, List<String>>>> queryMap;
	private List<String> commonDataModelReports;
	private Map<String, Map<String, List<String>>> filterReferences;
	private Map<String,Map<String,Map<String,List<CatalogColumn>>>> catalogQueryMap;

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public Map<String, Map<String, Map<String, List<String>>>> getQueryMap() {
		return queryMap;
	}

	public void setQueryMap(Map<String, Map<String, Map<String, List<String>>>> queryMap) {
		this.queryMap = queryMap;
	}

	public List<String> getCommonDataModelReports() {
		return commonDataModelReports;
	}

	public void setCommonDataModelReports(List<String> commonDataModelReports) {
		this.commonDataModelReports = commonDataModelReports;
	}

	public Map<String, Map<String, List<String>>> getFilterReferences() {
		return filterReferences;
	}

	public void setFilterReferences(Map<String, Map<String, List<String>>> filterReferences) {
		this.filterReferences = filterReferences;
	}

	public Map<String, Map<String, Map<String, List<CatalogColumn>>>> getCatalogQueryMap() {
		return catalogQueryMap;
	}

	public void setCatalogQueryMap(Map<String, Map<String, Map<String, List<CatalogColumn>>>> catalogQueryMap) {
		this.catalogQueryMap = catalogQueryMap;
	}

	
}

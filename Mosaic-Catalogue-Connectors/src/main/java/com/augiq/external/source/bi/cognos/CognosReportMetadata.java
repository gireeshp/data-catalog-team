package com.augiq.external.source.bi.cognos;

import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.entity.model.configuration.bean.CognosReportPublishColumn;
import com.augmentiq.maxiq.entity.model.configuration.bean.Join;
import com.augmentiq.maxiq.entity.model.configuration.bean.ReportPublishData;

public class CognosReportMetadata {

	private String reportName;
	private String reportSearchPath;
	private String reportPackageName;
	private Map<String, List<String>> reportTableinfo;
	private List<Join> join;
	private List<String> dataSourceName;
	private Map<String, List<ColumnBusinessName>> reportBusinessTableColumnName;
	private Map<String, List<CognosReportPublishColumn>> reportTableBusinessColInfo;
	
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getReportSearchPath() {
		return reportSearchPath;
	}
	public void setReportSearchPath(String reportSearchPath) {
		this.reportSearchPath = reportSearchPath;
	}
	public String getReportPackageName() {
		return reportPackageName;
	}
	public void setReportPackageName(String reportPackageName) {
		this.reportPackageName = reportPackageName;
	}
	public Map<String, List<String>> getReportTableinfo() {
		return reportTableinfo;
	}
	public void setReportTableinfo(Map<String, List<String>> reportTableinfo) {
		this.reportTableinfo = reportTableinfo;
	}
	public List<Join> getJoin() {
		return join;
	}
	public void setJoin(List<Join> join) {
		this.join = join;
	}
	public List<String> getDataSourceName() {
		return dataSourceName;
	}
	public void setDataSourceName(List<String> dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
	public Map<String, List<ColumnBusinessName>> getReportBusinessTableColumnName() {
		return reportBusinessTableColumnName;
	}
	public void setReportBusinessTableColumnName(Map<String, List<ColumnBusinessName>> reportBusinessTableColumnName) {
		this.reportBusinessTableColumnName = reportBusinessTableColumnName;
	}
	public Map<String, List<CognosReportPublishColumn>> getReportTableBusinessColInfo() {
		return reportTableBusinessColInfo;
	}
	public void setReportTableBusinessColInfo(Map<String, List<CognosReportPublishColumn>> reportTableBusinessColInfo) {
		this.reportTableBusinessColInfo = reportTableBusinessColInfo;
	}
	
	
	
}

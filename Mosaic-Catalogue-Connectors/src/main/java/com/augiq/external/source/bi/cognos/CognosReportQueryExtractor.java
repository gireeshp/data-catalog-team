/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.math.NumberUtils;
import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;




public class CognosReportQueryExtractor {
	public CognosReportData extractQueriesFromReport(String packageName,String reportName, String cognosXmlDownloadPath){
		XmlParser xmlParser=null;
		XPathExpression<Element> xpath =null;
		CognosReportData cognosReportData=null;
		ParserObject reportPo=null;
		Map<String,Map<String,Map<String,List<String>>>> queryMap=null;
		Map<String,Map<String,List<String>>> filterReferences=null;
		Map<String,Map<String,Map<String,List<CatalogColumn>>>> catalogQueryMap=null;
		Element reportElement=null;
		List<String> commonDataModelReports=null;
		try{
			cognosReportData=new CognosReportData();
			queryMap=new HashMap<>();
			filterReferences=new HashMap<>();
			catalogQueryMap=new HashMap<>();
			commonDataModelReports=new ArrayList<>();
			cognosReportData.setQueryMap(queryMap);
			cognosReportData.setFilterReferences(filterReferences);
			cognosReportData.setCatalogQueryMap(catalogQueryMap);
				System.out.println("reportName------------------------"+reportName);
				xmlParser=new XmlParser();
				reportPo=xmlParser.getXmlParserObject(cognosXmlDownloadPath + "/Report"+reportName+".xml");
				reportElement=reportPo.getRootElement();
				xpath=XPathFactory.instance().compile((xmlParser.getExpression(reportPo,"//ns:report//ns:query//ns:joinOperation|//ns:report//ns:query//ns:queryOperation|//ns:report//ns:query//ns:sqlText")), Filters .element(),null,reportPo.getNs());
				if(xpath.evaluate(reportPo.getDocument()).size()!=0){
					cognosReportData=extractQueries(reportPo, reportElement, cognosReportData, reportName);
				}else{
					cognosReportData=extractQueries(reportPo, reportElement, cognosReportData, "Centralized");
					commonDataModelReports.add(reportName);
				}
			cognosReportData.setPackageName(packageName);
			cognosReportData.setCommonDataModelReports(commonDataModelReports);
			
			ReportXMLExtractor reportXMLExtractor = new ReportXMLExtractor();
			cognosReportData = reportXMLExtractor.extractReportPages(reportPo, xpath,cognosReportData);
			
			Set<String> key = cognosReportData.getCatalogQueryMap().keySet();

			for (String keyName : key) {
				Map<String, Map<String, List<CatalogColumn>>> namespaceMap = cognosReportData
						.getCatalogQueryMap().get(keyName);
				for (String namespaceName : namespaceMap.keySet()) {
					Map<String, List<CatalogColumn>> tableMap = namespaceMap.get(namespaceName);
					for (String tableName : tableMap.keySet()) {
						List<CatalogColumn> catalogColumnList = tableMap.get(tableName);
						for (CatalogColumn catalogColumn : catalogColumnList) {
							System.out.println("Column Name :" + catalogColumn.getColumnName() + " ::Column Business Name: " + catalogColumn.getBusinessColumnName());
						}
					}
				}
			}
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			xmlParser=null;
			xpath =null;
			reportPo=null;
			queryMap=null;
			filterReferences=null;
			reportElement=null;
			commonDataModelReports=null;
			reportName=null;
		}
		return cognosReportData;
	}
	public CognosReportData  extractQueries(ParserObject reportPo,Element reportElement,CognosReportData cognosReportData,String dataModelName){
		Map<String,Map<String,Map<String,List<String>>>> queriesMap=null;
		Map<String,Map<String,Map<String,List<CatalogColumn>>>> catalogQueriesMap=null;
		Map<String,Map<String,List<String>>> filtersReferences=null,queryMap=null;
		Map<String,Map<String,List<CatalogColumn>>> catalogQueryMap = null;
		Map<String,List<String>> filterReferences=null;
		List<Element> dataItems=null,filterElement =null,queries=null,elementList=null;
		Element joinElement=null;
		String [] splitArray=null;
		String filterExpression=null,dataItemExpression=null;
		List<String> exprList=null,queriesToIgnoreList=null;
		CatalogColumn catalogColumn = null;
		try{
			queriesMap=cognosReportData.getQueryMap();
			filtersReferences=cognosReportData.getFilterReferences();
			catalogQueriesMap = cognosReportData.getCatalogQueryMap();
			if(queriesMap.containsKey(dataModelName)){
				queryMap=queriesMap.get(dataModelName);
			}else{
				queryMap=new HashMap<>();
			}
			if(filtersReferences.containsKey(dataModelName)){
				filterReferences=filtersReferences.get(dataModelName);
			}else{
				filterReferences=new HashMap<>();
			}
			if (catalogQueriesMap.containsKey(dataModelName)) {
				catalogQueryMap = catalogQueriesMap.get(dataModelName);
			} else {
				catalogQueryMap = new HashMap<>();
			}
			queriesToIgnoreList=new ArrayList<>();
			if (null != reportElement.getChild("queries",reportPo.getNs()) ) {
				queries=reportElement.getChild("queries",reportPo.getNs()).getChildren("query",reportPo.getNs());
//			}
			if(!dataModelName.equals("Centralized")){
				for(Element qr:queries){
					if(null!=qr.getChild("source", reportPo.getNs()).getChild("joinOperation", reportPo.getNs())){
						queriesToIgnoreList.add(qr.getAttributeValue("name"));
						joinElement=qr.getChild("source", reportPo.getNs()).getChild("joinOperation", reportPo.getNs()).getChild("joinOperands", reportPo.getNs());
						if(joinElement.getChildren("joinOperand Cardinality", reportPo.getNs()).size()!=0){
							elementList=joinElement.getChildren("joinOperand Cardinality", reportPo.getNs());
						}else{
							elementList=joinElement.getChildren("joinOperand", reportPo.getNs());
						}
						for (Element element : elementList) {
							queriesToIgnoreList.add(element.getChild("queryRef",reportPo.getNs()).getAttributeValue("refQuery"));
						}
					}else if(null!=qr.getChild("source", reportPo.getNs()).getChild("queryOperation", reportPo.getNs())){
						queriesToIgnoreList.add(qr.getAttributeValue("name", reportPo.getNs()));
						elementList=qr.getChild("source", reportPo.getNs()).getChild("queryOperation", reportPo.getNs()).getChild("queryRefs", reportPo.getNs()).getChildren("queryRef", reportPo.getNs());
						for (Element element : elementList) {
							queriesToIgnoreList.add(element.getAttributeValue("refQuery"));
						}	
					}else if(null!=qr.getChild("source", reportPo.getNs()).getChild("sqlQuery", reportPo.getNs())){
						if(null!=qr.getChild("source", reportPo.getNs()).getChild("sqlQuery", reportPo.getNs()).getChild("sqlText", reportPo.getNs())){
							queriesToIgnoreList.add(qr.getAttributeValue("name"));	
						}
					}
				}
			}
			for(Element qr:queries){
				if(!queriesToIgnoreList.contains(qr.getAttributeValue("name"))){
					if(null!=qr.getChild("selection", reportPo.getNs())){
						if (null!=qr.getChild("selection", reportPo.getNs()).getChildren("dataItem", reportPo.getNs())) {
							dataItems = qr.getChild("selection", reportPo.getNs()).getChildren("dataItem", reportPo.getNs());
							for (Element di : dataItems) {
								catalogColumn = new CatalogColumn();
								dataItemExpression=di.getChildText("expression", reportPo.getNs()).replaceAll("\n\t\t\t\t\t\t", " ");
//								catalogColumn.setBusinessColumnName(di.getAttributeValue("name"));
								exprList=extractColumnsFromExpression(dataItemExpression);
								for(String expr : exprList){
									splitArray=expr.split("].\\[");
									if(splitArray.length==3){
										queryMap=AddColumnToQueryMap(queryMap, splitArray);
										catalogQueryMap = addColumnToCatalogQueryMap(catalogQueryMap, splitArray, catalogColumn);
									}else if(splitArray.length==2){
										filterReferences=AddFilterTOFilterReferences(filterReferences, splitArray);
									}
								}
							}
						}
					}
					if (null!=qr.getChild("detailFilters", reportPo.getNs())){
						if(null!=qr.getChild("detailFilters", reportPo.getNs()).getChildren("detailFilter", reportPo.getNs())) {
							filterElement = qr.getChild("detailFilters", reportPo.getNs()).getChildren("detailFilter", reportPo.getNs());
							for (Element fl : filterElement) {
								filterExpression=fl.getChildText("filterExpression", reportPo.getNs());
								exprList=extractColumnsFromExpression(filterExpression);
								for (String expr : exprList) {
									expr=expr.replaceAll("\n\t\t\t\t\t\t", " ");
									splitArray=expr.split("].\\[");
									if(splitArray.length==3){
										queryMap=AddColumnToQueryMap(queryMap, splitArray);
									}else if(splitArray.length==2){
										filterReferences=AddFilterTOFilterReferences(filterReferences, splitArray);
									}
								}
							}
						}
					}
				}
			}
		}
			queriesMap.put(dataModelName, queryMap);
			catalogQueriesMap.put(dataModelName, catalogQueryMap);
			filtersReferences.put(dataModelName, filterReferences);
			cognosReportData.setQueryMap(queriesMap);
			cognosReportData.setCatalogQueryMap(catalogQueriesMap);
			cognosReportData.setFilterReferences(filtersReferences);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			queriesMap=null;
			filtersReferences=null;
			queryMap=null;
			filterReferences=null;
			dataItems=null;
			filterElement =null;
			queries=null;
			elementList=null;;
			joinElement=null;
			splitArray=null;
			filterExpression=null;
			dataItemExpression=null;
			exprList=null;
			queriesToIgnoreList=null;
		}
		return cognosReportData;
	}
	public List<String> extractColumnsFromExpression(String function){
		List<String> list=null;
		Pattern pattern =null;
		Matcher matcher =null;
		try{
			list=new ArrayList<>();
			pattern = Pattern.compile("(\\[[^\\[\\]]*\\]){3}|(\\[[^\\[\\]]*\\]){2}");
			function=function.replaceAll("]\\.\\[", "]\\[");
			matcher = pattern.matcher(function);
			while (matcher.find()){
				if(!list.contains(matcher.group(0))){
					list.add(matcher.group(0).replaceAll("]\\[", "]\\.["));
				}
			}	
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			pattern =null;
			matcher =null;
		}
		return list;
	}

	private Map<String, Map<String, List<String>>>  AddColumnToQueryMap(Map<String, Map<String, List<String>>>  queryMap,String[] splitArray){
		String namespace=null,column=null,table=null;
		Map<String,List<String>> tableMap=null;
		try{
			namespace=splitArray[0].replaceFirst("\\[", "");
			table=splitArray[1].trim();
			column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
			if(queryMap.containsKey(namespace)){
				tableMap = queryMap.get(namespace);
				if(tableMap.containsKey(table)){
					if(!tableMap.get(table).contains(column)){
						tableMap.get(table).add(column);
					}
				}else{
					tableMap.put(table, new ArrayList<String>());
					tableMap.get(table).add(column);
				}
			}else{
				queryMap.put(namespace, new HashMap<String,List<String>>());
				queryMap.get(namespace).put(table, new ArrayList<String>());
				queryMap.get(namespace).get(table).add(column);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			namespace=null;
			column=null;
			table=null;
			tableMap=null;
		}
		return queryMap;
	}
	
	private Map<String, Map<String, List<CatalogColumn>>>  addColumnToCatalogQueryMap(Map<String, Map<String, List<CatalogColumn>>>  catalogQueryMap,String[] splitArray, CatalogColumn catalogColumn){
		String namespace=null,column=null,table=null;
		Map<String,List<CatalogColumn>> tableMap=null;
		boolean flag = false;
		try{
			namespace=splitArray[0].replaceFirst("\\[", "");
			table=splitArray[1].trim();
			column=splitArray[2].substring(0,splitArray[2].length()-1).trim();
			if(catalogQueryMap.containsKey(namespace)){
				tableMap = catalogQueryMap.get(namespace);
				if(tableMap.containsKey(table)){
					/*if(!tableMap.get(table).contains(column)){
						tableMap.get(table).add(column);
					}*/
					for (CatalogColumn catalogColumnItem : tableMap.get(table)) {
						if (catalogColumnItem.getColumnName().equals(column)) {
							flag = true;
							break;
						}
					}
					if (!flag) {
						catalogColumn.setColumnName(column);
						tableMap.get(table).add(catalogColumn);
					}
				}else{
					tableMap.put(table, new ArrayList<CatalogColumn>());
					catalogColumn.setColumnName(column);
					tableMap.get(table).add(catalogColumn);
				}
			}else{
				catalogQueryMap.put(namespace, new HashMap<String,List<CatalogColumn>>());
				catalogQueryMap.get(namespace).put(table,new ArrayList<CatalogColumn>());
				catalogColumn.setColumnName(column);
				catalogQueryMap.get(namespace).get(table).add(catalogColumn);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			namespace=null;
			column=null;
			table=null;
			tableMap=null;
		}
		return catalogQueryMap;
	}
	
	private Map<String, List<String>>  AddFilterTOFilterReferences(Map<String, List<String>>  filterReferences,String[] splitArray){
		String namespace=null,filter=null;
		List<String> filters=null;
		try{
			namespace=splitArray[0].replaceFirst("\\[", "");
			filter=splitArray[1].substring(0,splitArray[1].length()-1);
			if(filterReferences.containsKey(namespace)){
				filters=filterReferences.get(namespace);
				if(!filters.contains(filter)){
					filters.add(filter);
				}
			}else{
				filterReferences.put(namespace, new ArrayList<String>());
				filterReferences.get(namespace).add(filter);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			namespace=null;
			filter=null;
			filters=null;
		}
		return filterReferences;
	}
	
	/*private void extractReportColumnBusinessName(ParserObject reportPo, XPathExpression<Element> xpath, CognosReportData) {
		
	}*/
}

/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.axis.client.Stub;
import javax.xml.namespace.QName;
import com.cognos.developer.schemas.bibus._3.*;
import com.cognos.developer.schemas.bibus._3.Analysis;
import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.cognos.developer.schemas.bibus._3.BiBusHeader;
import com.cognos.developer.schemas.bibus._3.ContentManagerService_PortType;
import com.cognos.developer.schemas.bibus._3.ContentManagerService_ServiceLocator;
import com.cognos.developer.schemas.bibus._3.InteractiveReport;
import com.cognos.developer.schemas.bibus._3.Model;
import com.cognos.developer.schemas.bibus._3.PropEnum;
import com.cognos.developer.schemas.bibus._3.Query;
import com.cognos.developer.schemas.bibus._3.QueryOptions;
import com.cognos.developer.schemas.bibus._3.Report;
import com.cognos.developer.schemas.bibus._3.SearchPathMultipleObject;
import com.cognos.developer.schemas.bibus._3.Sort;
import com.cognos.org.apache.axis.message.SOAPHeaderElement;


public class CognosXmlDownloader {
	static final Logger LOGGER = Logger.getLogger(CognosXmlDownloader.class);
	private ContentManagerService_PortType cmService = null;

	public void downloadReportXml(String path, String reportName, String cognosXmlDownloadPath, String dispatcherURL) {
		String reportPath = path;
		ContentManagerService_ServiceLocator cmServiceLocator = null;
		PropEnum props[] = null;
		URL url = null;
		SearchPathMultipleObject spMulti = null;
		BaseClass bc[] = null;
		String tempXml = "";
		try {
			cmServiceLocator = new ContentManagerService_ServiceLocator();
			props = new PropEnum[] { PropEnum.searchPath, PropEnum.defaultName, PropEnum.specification };
			url = new URL(dispatcherURL);
			cmService = cmServiceLocator.getcontentManagerService(url);
			LOGGER.info("connected to cognos server at" + dispatcherURL);
			System.out.println("connected to cognos server at" + dispatcherURL);
			// logon("ADMIN","administrator","newuser123");
			/* extract and write report xml (repot) */
			spMulti = new SearchPathMultipleObject(reportPath);
			bc = cmService.query(spMulti, props, new Sort[] {}, new QueryOptions());

			if (bc != null) {
				for (int i = 0; i < bc.length; i++) {
					try {
						System.out.println("@@@@@@@@@>>>>>>>>>>>>>>>" + bc[i]);
						System.out.println(bc[i].getClass().getName());
						if (bc[i].getClass().getName().contains("Report")
								&& !bc[i].getClass().getName().contains("InteractiveReport")) {
							tempXml = (((Report) bc[i]).getSpecification().getValue());
						} else if (bc[i].getClass().getName().contains("Analysis")) {
							tempXml = (((Analysis) bc[i]).getSpecification().getValue());
						} else if (bc[i].getClass().getName().contains("Query")) {
							tempXml = (((Query) bc[i]).getSpecification().getValue());
						} else if (bc[i].getClass().getName().contains("InteractiveReport")) {
							tempXml = (((InteractiveReport) bc[i]).getSpecification().getValue());
						}

						File modelFile = writeModelData(cognosXmlDownloadPath + "/Report" + reportName + ".xml",
								tempXml);
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.error("Exception :: " + e);
					}
				}
			}

			// symenticObject.setSymenticName((pPath.substring(index+16)).split("'")[0]);

			LOGGER.info("created report.xml in " + cognosXmlDownloadPath);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			bc = null;
			spMulti = null;
			cmService = null;
			url = null;
			props = null;
			cmServiceLocator = null;
		}
	}

	/**
	 * This method is used to extracts symentic xml files from cognos server and
	 * writes in C://birt folder
	 * 
	 * @param packagePath
	 *            This is the first paramter for package path
	 *            (/content/package[@name='EmployeeSummary'])
	 * @param reportPath
	 *            This is the second parameter for report path
	 *            (/report[@name='EmployeeNew']). will be appended to path path
	 * @return void
	 */
	public void downloadModelXml(String packagePath, String packageName, String cognosXmlDownloadPath,
			String dispatcherURL) {
		ParserObject reportPo = null;
		ContentManagerService_ServiceLocator cmServiceLocator = null;
		PropEnum props[] = null;
		URL url = null;
		SearchPathMultipleObject spMulti = null;
		BaseClass bc[] = null;
		String tempXml = "";
		try {
			cmServiceLocator = new ContentManagerService_ServiceLocator();
			props = new PropEnum[] { PropEnum.searchPath, PropEnum.model };
			url = new URL(dispatcherURL);
			cmService = cmServiceLocator.getcontentManagerService(url);
			LOGGER.info("connected to cognos server at" + dispatcherURL);
			System.out.println("connected to cognos server at" + dispatcherURL);
			// logon("ADS", "hbhanda", "Jnj@1985");
			spMulti = new SearchPathMultipleObject(packagePath);
			bc = cmService.query(spMulti, props, new Sort[] {}, new QueryOptions());
			System.out.println();
			if (bc != null) {
				for (int i = 0; i < bc.length; i++) {
					try {
						tempXml = ((Model) bc[i]).getModel().getValue();
						File modelFile = writeModelData(cognosXmlDownloadPath + "/Model" + packageName + ".xml",
								tempXml);
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.error("Exception :: " + e);
						System.out.println();
					}
				}
			}
			LOGGER.info("created model.xml in " + cognosXmlDownloadPath);
			System.out.println("created model.xml in " + cognosXmlDownloadPath);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			bc = null;
			spMulti = null;
			cmService = null;
			url = null;
			props = null;
			cmServiceLocator = null;
		}
	}

	/**
	 * This method is used to write symentic and report xml files
	 * 
	 * @param fileName
	 *            This is the first paramter for xml fileName
	 * @param tempXml
	 *            This is the second parameter with xml value in String format
	 * @return File
	 */
	private File writeModelData(String fileName, String tempXml) {
		File file = null;
		try {
			file = new File(fileName);
			FileOutputStream fos = new FileOutputStream(file);
			ByteArrayInputStream bais = new ByteArrayInputStream(tempXml.getBytes("UTF-8"));
			while (bais.available() > 0) {
				fos.write(bais.read());
			}
			;
			fos.flush();
			fos.close();
		} catch (Exception e) {
			LOGGER.error("Exception : " + e);
		} finally {

		}
		return file;
	}

	public void logon(String nameSpaceID, String userName, String password) {
		StringBuffer credentialXML = new StringBuffer();

		credentialXML.append("<credential>");
		credentialXML.append("<namespace>").append(nameSpaceID).append("</namespace>");
		credentialXML.append("<username>").append(userName).append("</username>");
		credentialXML.append("<password>").append(password).append("</password>");
		credentialXML.append("</credential>");

		String encodedCredentials = credentialXML.toString();
		XmlEncodedXML xmlCredentials = new XmlEncodedXML();
		xmlCredentials.set_value(encodedCredentials);

		try {
			cmService.logon(xmlCredentials, null);

			getSetHeaders();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void getSetHeaders() {
		String BiBus_NS = "http://developer.cognos.com/schemas/bibus/3/";
		String BiBus_H = "biBusHeader";

		BiBusHeader CMbibus = null;

		org.apache.axis.message.SOAPHeaderElement temp = ((Stub) cmService).getResponseHeader(BiBus_NS, BiBus_H);

		try {
			CMbibus = (BiBusHeader) temp.getValueAsType(new QName(BiBus_NS, BiBus_H));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (CMbibus != null) {
			((Stub) cmService).setHeader(BiBus_NS, BiBus_H, CMbibus);
		}
	}

	/*
	 * public String getPackagePathAndDownloadReportXml(String path,Map<String,
	 * String> constantMap,ReportObject reportObject) { ReportXMLExtractor
	 * reportXMLExtractor=new ReportXMLExtractor(); downloadReportXml(path,
	 * constantMap, reportObject); System.out.println(); return
	 * reportXMLExtractor.getModelPathFromReport(reportObject.getReportXmlFile()
	 * ); }
	 */
}

/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;

public class Column {
	private String columnName;
	private String columnExpression;
	private List<String> queryColumnRef;
	private String columnBusinessName;

	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnExpression() {
		return columnExpression;
	}
	public void setColumnExpression(String columnExpression) {
		this.columnExpression = columnExpression;
	}
	public List<String> getQueryColumnRef() {
		return queryColumnRef;
	}
	public void setQueryColumnRef(List<String> queryColumnRef) {
		this.queryColumnRef = queryColumnRef;
	}
	public String getColumnBusinessName() {
		return columnBusinessName;
	}
	public void setColumnBusinessName(String columnBusinessName) {
		this.columnBusinessName = columnBusinessName;
	}

}

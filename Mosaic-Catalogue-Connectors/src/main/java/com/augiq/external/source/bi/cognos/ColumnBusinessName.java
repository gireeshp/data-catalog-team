package com.augiq.external.source.bi.cognos;

public class ColumnBusinessName {
	
	private String columnName;
	private String columnBusinessName;
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnBusinessName() {
		return columnBusinessName;
	}
	public void setColumnBusinessName(String columnBusinessName) {
		this.columnBusinessName = columnBusinessName;
	}

}

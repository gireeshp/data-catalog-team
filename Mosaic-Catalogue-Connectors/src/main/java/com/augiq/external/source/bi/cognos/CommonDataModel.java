package com.augiq.external.source.bi.cognos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class CommonDataModel {
	private List<String> reportName;
	@XmlElementWrapper(name = "reportNames")
	@XmlElement(name = "reportName")
	public List<String> getReportName() {
		return reportName;
	}
	public void setReportName(List<String> reportName) {
		this.reportName = reportName;
	}
}

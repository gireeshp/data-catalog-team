/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class DataModelLayer {
	private String dataModelName;
	private List<DerivedColumn>  derivedColumn;
	private List<QualifiedColumn>  qualifiedColumn;
	private List<Measures>  measure;
	private List<ModelFilter> modelFilter;

	@XmlElementWrapper(name = "derivedColumns")
	@XmlElement(name = "derivedColumn")
	public List<DerivedColumn> getDerivedColumn() {
		return derivedColumn;
	}
	public void setDerivedColumn(List<DerivedColumn> derivedColumn) {
		this.derivedColumn = derivedColumn;
	}
	@XmlElementWrapper(name = "modelFilters")
	@XmlElement(name = "modelFilter")
	public List<ModelFilter> getModelFilter() {
		return modelFilter;
	}
	public void setModelFilter(List<ModelFilter> modelFilter) {
		this.modelFilter = modelFilter;
	}
	@XmlElementWrapper(name = "measures")
	@XmlElement(name = "measure")
	public List<Measures> getMeasure() {
		return measure;
	}
	public void setMeasure(List<Measures> measure) {
		this.measure = measure;
	}
	@XmlElementWrapper(name = "qualifiedColumns")
	@XmlElement(name = "qualifiedColumn")
	public List<QualifiedColumn> getQualifiedColumn() {
		return qualifiedColumn;
	}
	public void setQualifiedColumn(List<QualifiedColumn> qualifiedColumn) {
		this.qualifiedColumn = qualifiedColumn;
	}
	public String getDataModelName() {
		return dataModelName;
	}
	public void setDataModelName(String dataModelName) {
		this.dataModelName = dataModelName;
	}
}

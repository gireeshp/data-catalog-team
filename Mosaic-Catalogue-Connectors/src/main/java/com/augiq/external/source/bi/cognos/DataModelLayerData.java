/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.entity.model.configuration.bean.Join;

public class DataModelLayerData {
	private String dataModelName;
	private Map<String,Table> tables;
	private List<Join> joinList;
	private Map<String,String> qlikAliasTablesMap;
	public String getDataModelName() {
		return dataModelName;
	}
	public void setDataModelName(String dataModelName) {
		this.dataModelName = dataModelName;
	}
	public Map<String, Table> getTables() {
		return tables;
	}
	public void setTables(Map<String, Table> tables) {
		this.tables = tables;
	}
	public List<Join> getJoinList() {
		return joinList;
	}
	public void setJoinList(List<Join> joinList) {
		this.joinList = joinList;
	}
	public Map<String,String> getQlikAliasTablesMap() {
		return qlikAliasTablesMap;
	}
	public void setQlikAliasTablesMap(Map<String,String> qlikAliasTablesMap) {
		this.qlikAliasTablesMap = qlikAliasTablesMap;
	}

}

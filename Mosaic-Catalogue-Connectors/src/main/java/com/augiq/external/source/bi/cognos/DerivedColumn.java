package com.augiq.external.source.bi.cognos;

public class DerivedColumn {
	private String columnName;
	private String expression;
	private String queryColumnRef;

	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public boolean equals(Object o) {

		if (o == this) return true;
		if (!(o instanceof DerivedColumn)) {
			return false;
		}

		DerivedColumn derivedColumn = (DerivedColumn) o;

		return derivedColumn.columnName.equals(columnName) &&
				derivedColumn.expression.equals(expression);
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + columnName.hashCode();
		result = 31 * result + expression.hashCode();
		return result;
	}
	public String getQueryColumnRef() {
		return queryColumnRef;
	}
	public void setQueryColumnRef(String queryColumnRef) {
		this.queryColumnRef = queryColumnRef;
	}
}

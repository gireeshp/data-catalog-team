package com.augiq.external.source.bi.cognos;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class ExpressionBuilder {

	public static void main(String[] args) {
		String expression="case when [PLW Planning - Versioned].[Activity Type - DS STUDY LEVEL FLAG - 1] then 'YES' else 'NO' end";
		expression = "case   when ([data.Attributes].[Stage Gate Ph1] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE])       or [data.Attributes].[Stage Gate Ph2] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE])       or [data.Attributes].[Stage Gate Ph3] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE])       or [data.Attributes].[Stage Gate File] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]))     then (1)   when ([data.Attributes].[PMDA] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]))     then (2)   when ([data.Attributes].[CTN] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]))     then (3)   when ([data.Attributes].[Study Phase]='I' and [data.Time].[DAY DATE] between _first_of_month([data.Attributes].[FPI]) and _first_of_month([data.Attributes].[LPO]))     then (4)   when ([data.Attributes].[Study Phase] in ('II', 'IIA', 'IIB') and [data.Time].[DAY DATE] between _first_of_month([data.Attributes].[FPI]) and _first_of_month([data.Attributes].[LPO]))     then (5)   when ([data.Attributes].[Study Phase]='III' and [data.Time].[DAY DATE] between _first_of_month([data.Attributes].[FPI]) and _first_of_month([data.Attributes].[LPO]))     then (6)   when ([data.Attributes].[Launch] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]))     then (10)  when (  [data.Attributes].[JSRB decision to Phase 4 (Planned)] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]) )     then (12) when ([data.Attributes].[Study Phase]='IV' and [data.Time].[DAY DATE] between _first_of_month([data.Attributes].[FPI]) and _first_of_month([data.Attributes].[LPO]))     then (7) when ([data.Attributes].[Japan NDA (HIV) (Planned)] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]))     then (8)   when ([data.Attributes].[Approval] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]))     then (9) when ([data.Attributes].[NDA] between [data.Time].[DAY DATE] and last_day([data.Time].[DAY DATE]))     then (11)   else (0) end";
		expression = "if(Count([Business_Model_QS].[Product].[Category] for [Lines])&lt;10)	then(&#39;POOR&#39;) else(&#39;GOOD&#39;)";

		ExpressionBuilder expressionBuilder=new ExpressionBuilder();
		System.out.println("EXpression=============>"+expression);
		expression=expressionBuilder.buildExpression(expression);
		System.out.println("Converted EXpression===>"+expression);
	}

	public String buildExpression(String expression){
		String convertedExp = expression;
		convertedExp = convertUpperCase(convertedExp);
		if(convertedExp.contains("Replace")){
			convertedExp = convertReplace(convertedExp);
		}
		if(convertedExp.contains("substring")){
			convertedExp = convertedExp.replaceAll("\\bsubstring\\b", "Mid");
		}
		if(convertedExp.contains("||")){
			convertedExp = convertedExp.replaceAll("\\|\\|", "&");
		}
		if(convertedExp.contains("+")){
			convertedExp = convertedExp.replace("+", "&");
		}
		if(isContain(convertedExp, "IF")){
			convertedExp = convertIf(convertedExp);
		}
		if(convertedExp.contains("coalesce") || convertedExp.contains("COALESCE") || convertedExp.contains("ISNULL")){
			convertedExp = convertCoalesce(convertedExp);
		}
		if(convertedExp.contains("CASE")){
			convertedExp = convertCase(convertedExp);
		}
		if(convertedExp.contains("EXTRACT")){
			convertedExp = convertExtract(convertedExp);
		}
		if(convertedExp.contains("IS NULL") || convertedExp.contains("IS NOT NULL")){
			convertedExp = convertIsNull(convertedExp);
		}
		if(convertedExp.contains("_add_years") || convertedExp.contains("_add_YEARs")){
			convertedExp = convertAddYears(convertedExp);
		}
		if(convertedExp.contains("_add_days")){
			convertedExp = convertAddDays(convertedExp);
		}
		if(convertedExp.contains("_add_months")){
			convertedExp = convertAddMonths(convertedExp);
		}
		if(convertedExp.contains("_first_of_month")){
			convertedExp = convertFirstOfMonth(convertedExp);
		}
		if(convertedExp.contains("_last_of_month") || convertedExp.contains("last_day")){
			convertedExp = convertLastOfMonth(convertedExp);
		}
		if(convertedExp.contains("CURRENT_DATE") || convertedExp.contains("CURRENT DATE")){
			convertedExp = convertCurrentDay(convertedExp);
		}
		if(convertedExp.contains("NVL")){
			convertedExp = convertNvl(convertedExp);
		}
		//searching for cast( instead of cast
		//to avoid forecasts column
		if(convertedExp.contains("CAST(") || convertedExp.contains("CAST (")){
			convertedExp = convertCast(convertedExp);
		}
		if(convertedExp.contains(" in ")){
			convertedExp = convertInToMatch(convertedExp);
		}
		if(convertedExp.contains(" for ")){
			convertedExp = convertForToAggr(convertedExp);
		}
		if(convertedExp.contains("CONCAT")){
			convertedExp = convertConcat(convertedExp);
		}
		if(convertedExp.contains("{date}")){
			convertedExp = convertedExp.replaceAll("\\{date\\}", "date");
		}
		if(convertedExp.contains("greatest")){
			convertedExp = convertedExp.replaceAll("\\bgreatest\\b", "RangeMax");
		}
		if(convertedExp.contains(";")){
			convertedExp = convertedExp.replaceAll(";", "");
		}
		if(convertedExp.contains("_days_between")){
			convertedExp = convertDaysBetween(convertedExp);
		}
		if(convertedExp.contains("between")){
			convertedExp = convertBetween(convertedExp);
		}
		if(convertedExp.contains("abs")){
			convertedExp = convertedExp.replaceAll("\\babs\\b", "fabs");
		}
		return convertedExp;
	}

	public boolean isContain(String source, String subItem){
		String pattern = "\\b"+subItem+"\\b";
		Pattern p=Pattern.compile(pattern);
		Matcher m=p.matcher(source);
		return m.find();
	}

	public String convertDaysBetween(String expression){
		StringBuilder convertedExpression = new StringBuilder();
		String[] daysBetweenArray = expression.split("_days_between");
		for (int i = 0; i < daysBetweenArray.length; i++) {
			String daysBetween = daysBetweenArray[i];
			if(!daysBetween.isEmpty()){
				if(daysBetween.trim().startsWith("(")){
					String daysBetweenTemp = daysBetween.substring(daysBetween.indexOf("(") + 1, daysBetween.lastIndexOf(")"));
					List<String> daysBetweenColumns = extractSeparateColumns(daysBetweenTemp);
					if(daysBetweenColumns != null && !daysBetweenColumns.isEmpty() && daysBetweenColumns.size() > 0){
						if(daysBetweenColumns.size() == 2){
							daysBetween = "Interval(" + daysBetweenColumns.get(0) + "-" + daysBetweenColumns.get(1) + ", 'DD')";
						}
					}
				}else if(i != 0){
					convertedExpression.append("_days_between");
				}
				convertedExpression.append(daysBetween);
			}
		}
		return convertedExpression.toString();
	}

	public String convertFirstOfMonth(String expression) {
		String convertedExpression = expression;
		try{
			if(convertedExpression.contains("_first_of_month")){
				convertedExpression=convertedExpression.replaceAll("_first_of_month", "MonthStart");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedExpression;
	}
	public String convertLastOfMonth(String expression) {
		String convertedExpression = expression;
		try{
			if(convertedExpression.contains("_last_of_month")){
				convertedExpression=convertedExpression.replaceAll("_last_of_month", "MonthEnd");
			}
			if(convertedExpression.contains("last_day")){
				convertedExpression=convertedExpression.replaceAll("last_day", "MonthEnd");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedExpression;
	}
	public String convertCurrentDay(String expression) {
		String convertedExpression = expression;
		try{
			if(convertedExpression.contains("CURRENT_DATE")){
				convertedExpression=convertedExpression.replaceAll("CURRENT_DATE", "Today()");
				convertedExpression = convertedExpression.replaceAll("\\[Today\\(\\)\\]", "[CURRENT_DATE]");
			}
			if(convertedExpression.contains("CURRENT DATE")){
				convertedExpression=convertedExpression.replaceAll("CURRENT DATE", "Today()");
				convertedExpression = convertedExpression.replaceAll("\\[Today\\(\\)\\]", "[CURRENT DATE]");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedExpression;
	}
	public String convertAddYears(String expression) {
		String convertedExpression = expression;
		try{
			if(convertedExpression.contains("_add_years")){
				convertedExpression = convertedExpression.replaceAll("_add_years", "AddYears");
			}
			if(convertedExpression.contains("_add_YEARs")){
				convertedExpression = convertedExpression.replaceAll("_add_YEARs", "AddYears");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedExpression;
	}

	public String convertAddDays(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] addDaysArray = expression.split("_add_days");
		for (int i = 0; i < addDaysArray.length; i++) {
			String addDays = addDaysArray[i];
			if(!addDays.isEmpty()){
				if(addDays.trim().startsWith("(")){
					addDays = addDays.trim();
					String temp = addDays.substring(addDays.indexOf("(") + 1, addDays.indexOf(")"));
					if(temp.contains("(")){
						Integer openCount =StringUtils.countMatches(temp, "(");
						Integer replacableIndex = StringUtils.ordinalIndexOf(addDays, ")", openCount + 1);
						addDays = addDays.substring(0, replacableIndex).replace(",", "+") + addDays.substring(replacableIndex + 1);
					}else{
						addDays = addDays.replace(",", "+");
						addDays = addDays.replaceFirst("\\)", "");
					}
					addDays = addDays.replaceFirst("\\(", "");
				}
				convertedExpression.append(addDays.trim());
			}
		}
		return convertedExpression.toString();
	}

	public String convertAddMonths(String expression) {
		String convertedExpression = expression;
		try{
			if(convertedExpression.contains("_add_months")){
				convertedExpression=convertedExpression.replaceAll("_add_months", "AddMonths");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedExpression;
	}

	public String convertReplace(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] replaceArray = expression.split("Replace");
		for (int i = 0; i < replaceArray.length; i++) {
			String replace = replaceArray[i];
			if(!replace.isEmpty()){
				if(replace.trim().startsWith("(")){
					replace = replace.trim();
					String inReplace = replace.substring(replace.indexOf("(") + 1, replace.indexOf(")"));
					String replaceAfter = replace.substring(replace.indexOf(")"));
					if(inReplace.contains("(")){
						Integer openCount =StringUtils.countMatches(inReplace, "(");
						Integer replacableIndex = StringUtils.ordinalIndexOf(replace, ")", openCount + 1);
						inReplace = replace.substring(replace.indexOf("(") + 1, replacableIndex);
						replaceAfter = replace.substring(replacableIndex);
					}
					inReplace = "(" + inReplace;
					inReplace = inReplace.replaceAll("NULL", "NULL()");
					replace = inReplace + replaceAfter;
				}
				if(i != 0){
					convertedExpression.append("Replace");
				}
				convertedExpression.append(replace.trim());
			}
		}
		return convertedExpression.toString();
	}

	public String convertConcat(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] concatArray = expression.split("CONCAT");
		for (int i = 0; i < concatArray.length; i++) {
			String concat = concatArray[i];
			if(!concat.isEmpty()){
				if(concat.trim().startsWith("(")){
					concat = concat.trim();
					String temp = concat.substring(concat.indexOf("(") + 1, concat.lastIndexOf(")"));
					if(temp.contains("(")){
						Integer openCount =StringUtils.countMatches(temp, "(");
						Integer replacableIndex = StringUtils.ordinalIndexOf(concat, ")", openCount + 1);
						concat = concat.substring(0, replacableIndex).replaceAll(",", "&") + concat.substring(replacableIndex + 1);
					}else{
						concat = concat.replaceAll(",", "&");
						concat = concat.replaceFirst("\\)", "");
					}
					concat = concat.replaceFirst("\\(", "");
				}
				convertedExpression.append(concat.trim());
			}
		}
		return convertedExpression.toString();
	}

	public String convertNvl(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		expression = expression.replaceAll("\\bNVL2\\b", "NVL");
		String[] nvlArray = expression.split("NVL");
		for (int i = 0; i < nvlArray.length; i++) {
			String nvl = nvlArray[i];
			if(!nvl.isEmpty()){
				if(nvl.trim().startsWith("(")){
					nvl = nvl.trim();
					if(nvl.contains("],")){
						nvl = nvl.replaceFirst("\\],", "])=0,");
					}else if(nvl.contains("] ,")){
						nvl = nvl.replaceFirst("\\] ,", "])=0,");
					}else{
						nvl = nvl.replaceFirst("\\]\\)", "])=0");
					}
					convertedExpression.append("if(IsNull");
				}
				convertedExpression.append(nvl.trim());
			}
		}
		return convertedExpression.toString();
	}

	public String convertBetween(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		try{
			String[] betweenArray = expression.split("between");
			String[] modifiedArray = new String[betweenArray.length];
			Boolean previousBetween = Boolean.FALSE;
			for (int i = 1; i < betweenArray.length; i++) {
				String betweenBefore = betweenArray[i-1];
				if(previousBetween){
					betweenBefore = modifiedArray[i-1];
				}
				String betweenAfter = betweenArray[i];
				if(!betweenAfter.isEmpty()){
					String replaceBetween = "between";
					if(betweenAfter.contains(" and ")){
						String column = "";
						if(betweenBefore.contains("or ")){
							column = betweenBefore.substring(betweenBefore.indexOf("or ") + 3).trim();
						}else if(betweenBefore.contains(" and ")){
							column = betweenBefore.substring(betweenBefore.lastIndexOf(" and ") + 4).trim();
						}else{
							String columnTemp = betweenBefore.substring(betweenBefore.lastIndexOf("("));
							Integer closedCountTemp = StringUtils.countMatches(columnTemp, ")");
							Integer openCount = StringUtils.countMatches(betweenBefore, "(");
							column = betweenBefore.substring(StringUtils.ordinalIndexOf(betweenBefore, "(", openCount - closedCountTemp) + 1).trim();
							if(StringUtils.countMatches(column, "[") != StringUtils.countMatches(column, "]")){
								List<String> columnsList = new CognosReportQueryExtractor().extractColumnsFromExpression(betweenBefore);
								if(columnsList != null && !columnsList.isEmpty() && columnsList.size() > 0){
									column = columnsList.get(columnsList.size() - 1);
								}
							}
						}
						String columnBefore = betweenAfter.substring(0, betweenAfter.indexOf(" and ")).trim();
						String columnAfter = "";
						if(betweenAfter.contains(" or ")){
							columnAfter = betweenAfter.substring(betweenAfter.indexOf(" and ") + 5, betweenAfter.indexOf(" or ")).trim();
							betweenAfter = betweenAfter.substring(betweenAfter.indexOf(" or ")).trim();
						}else{
							columnAfter = betweenAfter.substring(betweenAfter.indexOf(" and ") + 5, betweenAfter.indexOf(",")).trim();
							betweenAfter = betweenAfter.substring(betweenAfter.indexOf(",")).trim();
						}
						replaceBetween = ">=" + columnBefore + " and " + column + "<=" + columnAfter;

					}
					modifiedArray[i-1] = betweenBefore + replaceBetween;
					if(!replaceBetween.equalsIgnoreCase(" between ")){
						previousBetween = Boolean.TRUE;
						modifiedArray[i] = betweenAfter;
					}else{
						previousBetween = Boolean.FALSE;
					}
					if(i == betweenArray.length - 1){
						modifiedArray[i] = betweenAfter;
					}
				}
			}
			for(String s : modifiedArray) {
				convertedExpression.append(s);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedExpression.toString();
	}

	public String convertCast(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] castArray = expression.split("CAST");
		for (int i = 0; i < castArray.length; i++) {
			String cast = castArray[i];
			if(!cast.isEmpty()){
				if(cast.trim().startsWith("(") && (cast.contains(" as ") || cast.contains(","))){
					Integer castTypeBeginIndex = 0;
					if(cast.contains(" as ")){
						castTypeBeginIndex = cast.indexOf(" as ") + 4;
					}else{
						castTypeBeginIndex = cast.indexOf(",") + 1;
					}
					String asBefore = cast.substring(0, castTypeBeginIndex);
					Integer closedCountAsBefore = StringUtils.countMatches(asBefore, ")");
					String asAfter = cast.substring(castTypeBeginIndex, cast.indexOf(")", castTypeBeginIndex));
					Integer openCountAsAfter = StringUtils.countMatches(asAfter, "(");
					Integer castTypeEndIndex = StringUtils.ordinalIndexOf(cast, ")", openCountAsAfter + closedCountAsBefore + 1);
					String castTemp = cast.substring(castTypeBeginIndex, castTypeEndIndex);
					castTemp = castTemp.replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)");
					if(castTemp.contains("DECIMAL")){
						Integer decimalCount;
						try {
							decimalCount = Integer.valueOf(castTemp.substring(castTemp.indexOf(",") + 1, castTemp.indexOf("\\)")));
						} catch (Exception e) {
							decimalCount = 0;
						}
						cast = cast.replaceAll(" as " + castTemp, ",#,##0.0" + new String(new char[decimalCount - 1]).replace("\0", "0"));
						cast = cast.replaceAll("," + castTemp, ",#,##0.0" + new String(new char[decimalCount - 1]).replace("\0", "0"));
					}else{
						cast = cast.replaceAll(" as " + castTemp, "");
						cast = cast.replaceAll("," + castTemp, "");
					}
					if(castTemp.contains("VARCHAR")){
						castTemp = "Text";
					}
					if(castTemp.contains("INTEGER")){
						castTemp = "Num";
					}
					if(castTemp.contains("DECIMAL")){
						castTemp = "Num";
					}
					convertedExpression.append(castTemp.substring(0, 1).toUpperCase() + castTemp.substring(1).toLowerCase());
				}
				convertedExpression.append(cast);
			}
		}
		return convertedExpression.toString();
	}

	public String convertForToTotal(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] forArray = expression.split(" for ");
		String[] modifiedForArray = new String[forArray.length];
		Boolean previousFor = Boolean.FALSE;
		for (int i = 1; i < forArray.length; i++) {
			String forBefore = forArray[i-1];
			if(previousFor){
				forBefore = modifiedForArray[i-1];
			}
			String forAfter = forArray[i];
			String replaceFor = " for ";
			String forBeforeCol = "";
			String forAfterCol = "";
			String beforeTemp = forBefore.substring(forBefore.lastIndexOf("(") + 1);
			Integer beforeIndex = StringUtils.ordinalIndexOf(forBefore, "(", StringUtils.countMatches(forBefore, "(") - StringUtils.countMatches(beforeTemp, ")") );
			forBeforeCol = forBefore.substring(beforeIndex + 1);
			forBefore = forBefore.substring(0, beforeIndex + 1);

			String afterTemp = forAfter.substring(0, forAfter.indexOf(")"));
			Integer afterIndex = StringUtils.ordinalIndexOf(forAfter, ")", StringUtils.countMatches(afterTemp, "(") + 1 );
			forAfterCol = forAfter.substring(0, afterIndex);
			forAfter = forAfter.substring(afterIndex);
			replaceFor = "total<" + forAfterCol + ">" + forBeforeCol;
			modifiedForArray[i-1] = forBefore + replaceFor;
			if(!replaceFor.equalsIgnoreCase(" for ")){
				previousFor = Boolean.TRUE;
				modifiedForArray[i] = forAfter;
			}else{
				previousFor = Boolean.FALSE;
			}
			if(i == forArray.length - 1){
				modifiedForArray[i] = forAfter;
			}
		}
		for(String s : modifiedForArray) {
			convertedExpression.append(s);
		}
		return convertedExpression.toString();
	}

	public String convertForToAggr(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] forArray = expression.split(" for ");
		String[] modifiedForArray = new String[forArray.length];
		Boolean previousFor = Boolean.FALSE;
		for (int i = 1; i < forArray.length; i++) {
			String forBefore = forArray[i-1];
			if(previousFor){
				forBefore = modifiedForArray[i-1];
			}
			String forAfter = forArray[i];
			String replaceFor = " for ";
			String forBeforeCol = "";
			String forAfterCol = "";
			String beforeTemp = forBefore.substring(forBefore.lastIndexOf("(") + 1);
			Integer beforeIndex = StringUtils.ordinalIndexOf(forBefore, "(", StringUtils.countMatches(forBefore, "(") - StringUtils.countMatches(beforeTemp, ")") - 1);
			forBeforeCol = forBefore.substring(beforeIndex + 1);
			forBefore = forBefore.substring(0, beforeIndex + 1);

			String afterTemp = forAfter.substring(0, forAfter.indexOf(")"));
			Integer afterIndex = StringUtils.ordinalIndexOf(forAfter, ")", StringUtils.countMatches(afterTemp, "(") + 1 );
			forAfterCol = forAfter.substring(0, afterIndex);
			forAfter = forAfter.substring(afterIndex);
			replaceFor = "Aggr(" + forBeforeCol + ")," + forAfterCol;
			modifiedForArray[i-1] = forBefore + replaceFor;
			if(!replaceFor.equalsIgnoreCase(" for ")){
				previousFor = Boolean.TRUE;
				modifiedForArray[i] = forAfter;
			}else{
				previousFor = Boolean.FALSE;
			}
			if(i == forArray.length - 1){
				modifiedForArray[i] = forAfter;
			}
		}
		for(String s : modifiedForArray) {
			convertedExpression.append(s);
		}
		return convertedExpression.toString();
	}

	public String convertInToMatch(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] inArray = expression.split(" in ");
		String[] modifiedInArray = new String[inArray.length];
		Boolean previousIn = Boolean.FALSE;
		for (int i = 1; i < inArray.length; i++) {
			String inBefore = inArray[i-1];
			if(previousIn){
				inBefore = modifiedInArray[i-1];
			}
			String inAfter = inArray[i];
			String replaceIn = " in ";
			if(inAfter.trim().startsWith("(")){
				String inColumnName = "";
				String inList = "";
				if(inBefore.contains("(["))
					inColumnName = inBefore.substring(inBefore.lastIndexOf("([") + 1);
				else if(inBefore.contains("('")){
					inColumnName = inBefore.substring(inBefore.lastIndexOf("('") + 1);
				}
				String inAfterTemp = inAfter.substring(inAfter.indexOf("(") + 1, inAfter.indexOf(")"));
				Integer openCount =StringUtils.countMatches(inAfterTemp, "(");
				inList = inAfter.substring(inAfter.indexOf("(") + 1, StringUtils.ordinalIndexOf(inAfter, ")", openCount + 1));
				if(!inColumnName.contains(".")){
					inColumnName = inColumnName.replaceFirst("\\[", "").replaceFirst("\\]", ""); 
				}
				if(inBefore.contains("(["))
					inBefore = inBefore.substring(0, inBefore.lastIndexOf("([") + 1);
				else if(inBefore.contains("('")){
					inBefore = inBefore.substring(0, inBefore.lastIndexOf("('") + 1);
				}
				inAfter  = inAfter.substring(StringUtils.ordinalIndexOf(inAfter, ")", openCount + 1) + 1);
				replaceIn = "Match(" + inColumnName + "," + inList + ")";
			}
			modifiedInArray[i-1] = inBefore + replaceIn;
			if(!replaceIn.equalsIgnoreCase(" in ")){
				previousIn = Boolean.TRUE;
				modifiedInArray[i] = inAfter;
			}else{
				previousIn = Boolean.FALSE;
			}
			if(i == inArray.length - 1){
				modifiedInArray[i] = inAfter;
			}
		}
		for(String s : modifiedInArray) {
			convertedExpression.append(s);
		}
		return convertedExpression.toString();
	}

	public String convertExtract(String expression) {
		StringBuilder convertedExpression = new StringBuilder();
		String[] extractArray = expression.split("EXTRACT");
		for (int i = 0; i < extractArray.length; i++) {
			String extract = extractArray[i];
			if(!extract.isEmpty()){
				if(extract.trim().startsWith("(")){
					String temp = extract.substring(extract.indexOf("(") + 1, extract.indexOf(")"));
					if(temp.contains("(") && !extract.contains(")].")){
						Integer openCount =StringUtils.countMatches(temp, "(");
						Integer replacableIndex = StringUtils.ordinalIndexOf(extract, ")", openCount + 1);
						extract = extract.substring(0, replacableIndex) + extract.substring(replacableIndex + 1);
					}
					if(extract.contains(",") && !extract.contains(",(")){
						extract = extract.replaceFirst(",", ",(");
					}
					extract = extract.replaceFirst("\\(", "");
					extract = extract.substring(0, 1).toUpperCase() + extract.substring(1, extract.indexOf(",")).toLowerCase() + extract.substring(extract.indexOf(",")+1);
				}
				convertedExpression.append(extract);
			}
		}
		return convertedExpression.toString();
	}
	public String convertIsNull(String expression){
		String convertedExpression=expression;
		try{
			if(convertedExpression.contains("] IS NULL")){
				int idx1 = convertedExpression.indexOf("] IS NULL");
				int lastSpace = convertedExpression.lastIndexOf("[", idx1);
				String isNull=convertedExpression.substring(lastSpace, idx1+1);
				convertedExpression=convertedExpression.replace(isNull+" IS NULL", " IsNull("+isNull+")=-1");
			}if(convertedExpression.contains("] IS NOT NULL")){
				int idx1 = convertedExpression.indexOf("] IS NOT NULL");
				int lastSpace = convertedExpression.lastIndexOf("[", idx1);
				String isNull=convertedExpression.substring(lastSpace, idx1+1);
				convertedExpression=convertedExpression.replace(isNull+" IS NOT NULL", " IsNull("+isNull+")=0");
			}

			if(convertedExpression.contains(") IS NULL")){
				int idx1 = convertedExpression.indexOf(") IS NULL");
				int lastSpace = convertedExpression.lastIndexOf("(", idx1);
				String isNullTemp=convertedExpression.substring(lastSpace, idx1);
				String isNull = "";
				if(isNullTemp.contains(")")){
					Integer closedCount = StringUtils.countMatches(isNullTemp, ")");
					Integer openCount = StringUtils.countMatches(convertedExpression.substring(0,lastSpace), "(");
					isNull = convertedExpression.substring(StringUtils.ordinalIndexOf(convertedExpression, "(", openCount - closedCount + 1), idx1 + 1);
				}else{
					isNull = isNullTemp + ")";
				}
				convertedExpression=convertedExpression.replace(isNull+" IS NULL", " IsNull("+isNull+")=-1");
			}if(convertedExpression.contains(")  IS NULL")){
				int idx1 = convertedExpression.indexOf(") IS NULL");
				int lastSpace = convertedExpression.lastIndexOf("(", idx1);
				String isNullTemp=convertedExpression.substring(lastSpace, idx1);
				String isNull = "";
				if(isNullTemp.contains(")")){
					Integer closedCount = StringUtils.countMatches(isNullTemp, ")");
					Integer openCount = StringUtils.countMatches(convertedExpression.substring(0,lastSpace), "(");
					isNull = convertedExpression.substring(StringUtils.ordinalIndexOf(convertedExpression, "(", openCount - closedCount + 1), idx1 + 1);
				}else{
					isNull = isNullTemp + ")";
				}
				convertedExpression=convertedExpression.replace(isNull+" IS NULL", " IsNull("+isNull+")=-1");
			}if(convertedExpression.contains(") IS NOT NULL")){
				int idx1 = convertedExpression.indexOf(") IS NOT NULL");
				int lastSpace = convertedExpression.lastIndexOf("(", idx1);
				String isNullTemp=convertedExpression.substring(lastSpace, idx1);
				String isNull = "";
				if(isNullTemp.contains(")")){
					Integer closedCount = StringUtils.countMatches(isNullTemp, ")");
					Integer openCount = StringUtils.countMatches(convertedExpression.substring(0,lastSpace), "(");
					isNull = convertedExpression.substring(StringUtils.ordinalIndexOf(convertedExpression, "(", openCount - closedCount + 1), idx1 + 1);
				}else{
					isNull = isNullTemp + ")";
				}
				convertedExpression=convertedExpression.replace(isNull+" IS NOT NULL", " IsNull("+isNull+")=0");
			}if(convertedExpression.contains(")  IS NOT NULL")){
				int idx1 = convertedExpression.indexOf(")  IS NOT NULL");
				int lastSpace = convertedExpression.lastIndexOf("(", idx1);
				String isNullTemp=convertedExpression.substring(lastSpace, idx1);
				String isNull = "";
				if(isNullTemp.contains(")")){
					Integer closedCount = StringUtils.countMatches(isNullTemp, ")");
					Integer openCount = StringUtils.countMatches(convertedExpression.substring(0,lastSpace), "(");
					isNull = convertedExpression.substring(StringUtils.ordinalIndexOf(convertedExpression, "(", openCount - closedCount + 1), idx1 + 1);
				}else{
					isNull = isNullTemp + ")";
				}
				convertedExpression=convertedExpression.replace(isNull+"  IS NOT NULL", " IsNull("+isNull+")=0");
			}


		}catch(Exception e){
			e.printStackTrace();
		}
		if(convertedExpression.contains("IS NULL") || convertedExpression.contains("IS NOT NULL")){
			return convertIsNull(convertedExpression);
		}else{
			return convertedExpression;
		}
	}
	public String convertIf(String ifStatement){
		StringBuilder ifToIf = null;
		String convertedExpression="";
		try{
			ifToIf=new StringBuilder();
			String conditions[] = ifStatement.split("THEN|ELSE");
			if(conditions.length>0){
				String ifcond=conditions[0];
				ifToIf.append(ifcond).insert(ifToIf.toString().length()-2, ","+conditions[1]+","+conditions[2]);	
			}
			convertedExpression=ifToIf.toString();
			if (convertedExpression.contains(",(NULL)") || convertedExpression.contains(", (NULL)")) {
				convertedExpression=convertedExpression.replaceAll(",\\(NULL\\)", "").replaceAll(", \\(NULL\\)", "");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedExpression;
	}
	public String convertCase(String caseStatement){
		StringBuilder caseToIf = null;
		String convertedExpression="";
		try{
			caseToIf =new StringBuilder();
			/* Cast statement code */
			caseStatement=caseStatement.replaceAll("total", "").replaceAll("Total", "");

			String when = caseStatement.substring(5, caseStatement.indexOf("END"));
			String conditions[] = when.split("WHEN");
			int count = 0;
			int islast=0;
			for (String condition : conditions) {
				if (!condition.trim().equals("")) {
					if ( islast==conditions.length-1 && !condition.contains("ELSE")) {
						caseToIf.append("if(" + condition.replace("THEN", ",") + ")");
					}
					else if (!condition.contains("ELSE")) {
						caseToIf.append("if(" + condition.replace("THEN", ",") + ",");
					}
					else{
						caseToIf.append("if(" + (condition.split("ELSE")[0].trim()).replace("THEN", ",") + ",");
						caseToIf.append((condition.split("ELSE")[1].trim()));
						for (int i = 0; i <= count; i++) {
							caseToIf.append(")");
						}
					}
					count++;
				}
				islast++;
			}
			caseToIf.append((caseStatement.substring(caseStatement.indexOf("END") + 3)));


		}catch(Exception e){
			e.printStackTrace();
		}finally{

		}
		convertedExpression=caseToIf.toString();
		if ((convertedExpression.contains(",NULL") || convertedExpression.contains(", NULL")) && !convertedExpression.contains("NULL()")) {
			convertedExpression=convertedExpression.replaceAll(",NULL", "").replaceAll(", NULL", "");
		}
		return convertedExpression;
		/* Cast statement code */
	}

	public String convertCoalesce(String expression){
		StringBuilder convertedExpression = new StringBuilder();
		expression = expression.replaceAll("\\bcoalesce\\b", "COALESCE");
		expression = expression.replaceAll("\\bISNULL\\b", "COALESCE");
		String[] coalesceArray = expression.split("COALESCE");
		for (int i = 0; i < coalesceArray.length; i++) {
			String coalesce = coalesceArray[i];
			if(!coalesce.isEmpty()){
				if(coalesce.trim().startsWith("(")){
					String coalesceTemp = coalesce.substring(coalesce.indexOf("(") + 1, coalesce.lastIndexOf(")"));
					List<String> coalesceColumns = extractSeparateColumns(coalesceTemp);
					if(coalesceColumns != null && !coalesceColumns.isEmpty() && coalesceColumns.size() > 0){
						if(coalesceColumns.size() > 2){
							coalesceTemp = "CASE ";
							for (int j = 0; j < coalesceColumns.size(); j++)  {
								String coalesceFirst = coalesceColumns.get(j);
								String coalesceFirstColumn = "";
								try{
									coalesceFirstColumn = new CognosReportQueryExtractor().extractColumnsFromExpression(coalesceFirst).get(0);
								}catch (Exception e) {
									coalesceFirstColumn = coalesceFirst;
								}
								if(j != coalesceColumns.size() - 1){
									coalesceTemp = coalesceTemp + "WHEN ((" + coalesceFirst + ") IS NOT NULL) THEN" + coalesceFirstColumn;
								}else{
									coalesceTemp = coalesceTemp + "ELSE" + coalesceFirst;
								}
							}
							coalesceTemp = coalesceTemp + "END ";
							coalesceTemp = coalesceTemp + coalesce.substring(coalesce.lastIndexOf(")") + 1);
							coalesce = coalesceTemp;
						}else{
							String coalesceFirst = coalesceColumns.get(0);
							String coalesceFirstColumn = "";
							try{
								coalesceFirstColumn = new CognosReportQueryExtractor().extractColumnsFromExpression(coalesceFirst).get(0);
							}catch (Exception e) {
								coalesceFirstColumn = coalesceFirst;
							}
							String coalesceLast = coalesceColumns.get(1);
							String coalesceAfterLast = coalesce.substring(coalesce.lastIndexOf(")") + 1);
							coalesce = "if(IsNull(" + coalesceFirst + ")=0" + "," + coalesceFirstColumn + "," + coalesceLast + ")" + coalesceAfterLast;
						}
					}
				}
				convertedExpression.append(coalesce);
			}
		}
		return convertedExpression.toString();
	}

	public List<String> extractSeparateColumns(String coalesce) {
		List<String> separateColumns = new ArrayList<String>();
		String[] splitCommaArray = coalesce.split(",");
		for (int i = 0; i < splitCommaArray.length; i++) {
			String columnTemp = splitCommaArray[i];
			if(columnTemp.contains("(")){
				String column = "";
				Integer openCount = StringUtils.countMatches(columnTemp, "(");
				Integer closedCount = StringUtils.countMatches(columnTemp, ")");
				while (openCount > closedCount) {
					column = column + splitCommaArray[i] + ",";
					i = i + 1;
					openCount = openCount + StringUtils.countMatches(splitCommaArray[i], "(");
					closedCount = closedCount + StringUtils.countMatches(splitCommaArray[i], ")");
				}
				if(openCount == closedCount){
					column = column + splitCommaArray[i];
				}
				separateColumns.add(column);
			}else{
				separateColumns.add(columnTemp);
			}
		}
		return separateColumns;
	}

	public String convertUpperCase(String expression){
		expression=expression.replaceAll("\\bEnd\\b", "END").replaceAll("\\bend\\b", "END")
				.replaceAll("\\bWhen\\b", "WHEN").replaceAll("\\bwhen\\b", "WHEN")
				.replaceAll("\\bCase\\b", "CASE").replaceAll("\\bcase\\b", "CASE")
				.replaceAll("\\bThen\\b", "THEN").replaceAll("\\bthen\\b", "THEN")
				.replaceAll("\\bElse\\b", "ELSE").replaceAll("\\belse\\b", "ELSE");
		expression=expression.replaceAll("\\bNull\\b", "NULL").replaceAll("\\bnull\\b", "NULL");
		expression=expression.replaceAll("\\bisNull\\b", "ISNULL").replaceAll("\\bisnull\\b", "ISNULL")
				.replaceAll("\\bIsNull\\b", "ISNULL").replaceAll("\\bIsnull\\b", "ISNULL");
		expression=expression.replaceAll("\\bIf\\b", "IF").replaceAll("\\bif\\b", "IF")
				.replaceAll("\\bis\\b", "IS").replaceAll("\\bIs\\b", "IS");
		expression=expression.replaceAll("\\bnot\\b", "NOT").replaceAll("\\bNot\\b", "NOT");
		expression=expression.replaceAll("\\bcurrent_date\\b", "CURRENT_DATE");
		expression=expression.replaceAll("\\bcurrent date\\b", "CURRENT DATE").replaceAll("\\bCurrent date\\b", "CURRENT DATE")
				.replaceAll("\\bCurrent Date\\b", "CURRENT DATE");
		expression=expression.replaceAll("\\bnvl\\b", "NVL").replaceAll("\\bNvl\\b", "NVL");
		expression=expression.replaceAll("\\bextract\\b", "EXTRACT").replaceAll("\\bExtract\\b", "EXTRACT");
		expression=expression.replaceAll("\\bcast\\b", "CAST").replaceAll("\\bCast\\b", "CAST");
		expression=expression.replaceAll("\\bvarchar\\b", "VARCHAR").replaceAll("\\bVarchar\\b", "VARCHAR");
		expression=expression.replaceAll("\\binteger\\b", "INTEGER").replaceAll("\\bInteger\\b", "INTEGER");
		expression=expression.replaceAll("\\bdecimal\\b", "DECIMAL").replaceAll("\\bDecimal\\b", "DECIMAL");
		expression=expression.replaceAll("\\boreplace\\b", "Replace").replaceAll("\\breplace\\b", "Replace").replaceAll("\\bREPLACE\\b", "Replace"); //need "Replace" in camel case
		expression=expression.replaceAll("\\bconcat\\b", "CONCAT").replaceAll("\\bConcat\\b", "CONCAT");
		return expression;
	}
}

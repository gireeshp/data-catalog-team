/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.Map;

public class ExtractLayerData {
	private String packageName;
	private Map<String,Table> tables;
	private Map<String,String> modifiedTableMap;
	private String comment;

	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public Map<String, Table> getTables() {
		return tables;
	}
	public void setTables(Map<String, Table> tables) {
		this.tables = tables;
	}
	public Map<String,String> getModifiedTableMap() {
		return modifiedTableMap;
	}
	public void setModifiedTableMap(Map<String,String> modifiedTableMap) {
		this.modifiedTableMap = modifiedTableMap;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

}

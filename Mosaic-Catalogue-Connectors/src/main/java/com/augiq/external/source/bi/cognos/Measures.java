package com.augiq.external.source.bi.cognos;

public class Measures {
	private String columnName;
	private String numberFormat;

	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getNumberFormat() {
		return numberFormat;
	}
	public void setNumberFormat(String numberFormat) {
		this.numberFormat = numberFormat;
	}
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof Measures)) {
			return false;
		}
		Measures derivedColumn = (Measures) o;
		return derivedColumn.columnName.equals(columnName);
	}   	

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + columnName.hashCode();
		return result;
	}
}

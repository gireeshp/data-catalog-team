/**
 * 
 */
package com.augiq.external.source.bi.cognos;

public class ModelFilter {
	private String filterName;
	private String expression;
	public String getFilterName() {
		return filterName;
	}
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	public boolean equals(Object o) {
		if (o == this) return true;
		if (!(o instanceof ModelFilter)) {
			return false;
		}
		ModelFilter filter = (ModelFilter) o;
		return filter.filterName.equals(filterName) &&
				filter.expression.equals(expression);
	}
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + filterName.hashCode();
		result = 31 * result + expression.hashCode();
		return result;
	}
}

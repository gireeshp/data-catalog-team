/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;

/**
 * 
 *
 */
public class Page {
	private String pageName;
	
	private List<Object> reportElements;
	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return pageName;
	}
	/**
	 * @param pageName the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	/**
	 * @return the reportElemnts
	 */
	public List<Object> getReportElements() {
		return reportElements;
	}
	/**
	 * @param reportElemnts the reportElemnts to set
	 */
	public void setReportElements(List<Object> reportElements) {
		this.reportElements = reportElements;
	}
	
	
}

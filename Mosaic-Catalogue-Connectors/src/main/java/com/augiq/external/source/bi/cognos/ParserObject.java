package com.augiq.external.source.bi.cognos;

import java.io.File;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

public class ParserObject {
	private File inputFile;
	private SAXBuilder saxBuilder;
	private Document document;
	private Element rootElement;
	private Namespace ns;
	private boolean defaultPrifix;
	public boolean isDefaultPrifix() {
		return defaultPrifix;
	}
	public void setDefaultPrifix(boolean defaultPrifix) {
		this.defaultPrifix = defaultPrifix;
	}
	public File getInputFile() {
		return inputFile;
	}
	public void setInputFile(File inputFile) {
		this.inputFile = inputFile;
	}
	public SAXBuilder getSaxBuilder() {
		return saxBuilder;
	}
	public void setSaxBuilder(SAXBuilder saxBuilder) {
		this.saxBuilder = saxBuilder;
	}
	public Document getDocument() {
		return document;
	}
	public void setDocument(Document document) {
		this.document = document;
	}
	public Element getRootElement() {
		return rootElement;
	}
	public void setRootElement(Element rootElement) {
		this.rootElement = rootElement;
	}
	public Namespace getNs() {
		return ns;
	}
	public void setNs(Namespace ns) {
		this.ns = ns;
	}
}

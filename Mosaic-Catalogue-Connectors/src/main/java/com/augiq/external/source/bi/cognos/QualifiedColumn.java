package com.augiq.external.source.bi.cognos;

public class QualifiedColumn {
	private String queryColumnRef;
	private String columnName;
	private String qualifiedColumnName;
	public String getQueryColumnRef() {
		return queryColumnRef;
	}
	public void setQueryColumnRef(String queryColumnRef) {
		this.queryColumnRef = queryColumnRef;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getQualifiedColumnName() {
		return qualifiedColumnName;
	}
	public void setQualifiedColumnName(String qualifiedColumnName) {
		this.qualifiedColumnName = qualifiedColumnName;
	}
}

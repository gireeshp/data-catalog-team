/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;

/**
 *
 */
public class ReportColumn {

	 private String label;
	 private String refDataItem;
	 private Boolean isConditionalColor;
	 private String backgroundColor;
	 private String numberFormatPattern;
	 private String staticValue;
	 private List<String> masterListColumns;
	 
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the refDataItem
	 */
	public String getRefDataItem() {
		return refDataItem;
	}
	/**
	 * @param refDataItem the refDataItem to set
	 */
	public void setRefDataItem(String refDataItem) {
		this.refDataItem = refDataItem;
	}
	public Boolean getIsConditionalColor() {
		return isConditionalColor;
	}
	public void setIsConditionalColor(Boolean isConditionalColor) {
		this.isConditionalColor = isConditionalColor;
	}
	/**
	 * @return the backgroundColor
	 */
	public String getBackgroundColor() {
		return backgroundColor;
	}
	/**
	 * @param backgroundColor the backgroundColor to set
	 */
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	public String getNumberFormatPattern() {
		return numberFormatPattern;
	}
	public void setNumberFormatPattern(String numberFormatPattern) {
		this.numberFormatPattern = numberFormatPattern;
	}
	public String getStaticValue() {
		return staticValue;
	}
	public void setStaticValue(String staticValue) {
		this.staticValue = staticValue;
	}
	public List<String> getMasterListColumns() {
		return masterListColumns;
	}
	public void setMasterListColumns(List<String> masterListColumns) {
		this.masterListColumns = masterListColumns;
	}
	 
}

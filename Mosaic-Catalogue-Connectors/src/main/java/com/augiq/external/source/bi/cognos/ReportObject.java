package com.augiq.external.source.bi.cognos;

public class ReportObject {
	
	private String reportXmlFile;
	
	private String cognosXmlPath;
	private String reportPath;
	private String reportId;
	private String reportName;
	private String reportPackage;
	private String reportPackagePath;
	
	
	public String getReportPackagePath() {
		return reportPackagePath;
	}
	public void setReportPackagePath(String reportPackagePath) {
		this.reportPackagePath = reportPackagePath;
	}
	public String getReportPackage() {
		return reportPackage;
	}
	public void setReportPackage(String reportPackage) {
		this.reportPackage = reportPackage;
	}
	public String getReportXmlFile() {
		return reportXmlFile;
	}
	public void setReportXmlFile(String reportXmlFile) {
		this.reportXmlFile = reportXmlFile;
	}
	
	public String getCognosXmlPath() {
		return cognosXmlPath;
	}
	public void setCognosXmlPath(String cognosXmlPath) {
		this.cognosXmlPath = cognosXmlPath;
	}
	public String getReportPath() {
		return reportPath;
	}
	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	

}

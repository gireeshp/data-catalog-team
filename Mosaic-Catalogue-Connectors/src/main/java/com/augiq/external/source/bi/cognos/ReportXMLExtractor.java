package com.augiq.external.source.bi.cognos;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.xpath.XPathExpression;

public class ReportXMLExtractor {
	public static void main(String[] args) {
		ReportXMLExtractor reportXMLExtractor = new ReportXMLExtractor();

	}

	public CognosReportData extractReportPages(ParserObject reportPo, XPathExpression<Element> xpath,
			CognosReportData cognosReportData) {
		Element element = null;
		List<Element> pages = null;
		List<Element> pageElements = null;
		XmlParser xp = null;
		try {
			xp = new XmlParser();
			element = reportPo.getRootElement();
			/* Iterate over all pages in a report */
			pages = xp.getXmlElementList(reportPo, "//ns:reportPages//ns:page");
			if (pages != null && pages.size() != 0) {
				for (Element pg : pages) {
					String pageName = pg.getAttributeValue("name");
					pageElements = xp.getXmlElementList(reportPo, "//ns:reportPages/ns:page[@name='"
							+ pageName + "']/ns:pageBody/ns:contents/node()");
					if (null != pageElements && !(pageElements.size() > 0)) {
						pageElements = xp.getXmlElementList(reportPo,
								"//ns:reportPages/ns:pageSet/ns:pageGroups/ns:pageGroup/ns:footerPages/ns:page[@name='"
										+ pageName + "']/ns:pageBody/ns:contents/node()");
					}
					if (pageElements != null && pageElements.size() != 0) {
						for (Element pgElement : pageElements) {
							String elementType = pgElement.getName();
							switch (elementType) {
							case ("list"):
								cognosReportData = extractList(pgElement, xpath, xp, reportPo, cognosReportData);
								break;

							default:
								break;
							}
						}
					} else {
						pageElements = xp.getXmlElementList(reportPo,
								"//ns:reportPages/ns:pageSet/ns:detailPages/ns:page[@name='" + pageName
										+ "']/ns:pageBody/ns:contents/node()");
						if (pageElements != null && pageElements.size() != 0) {
							for (Element pgElement : pageElements) {
								String elementType = pgElement.getName();
								switch (elementType) {
								case ("list"):
									cognosReportData = extractList(pgElement, xpath, xp, reportPo, cognosReportData);
									break;

								default:
									break;
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cognosReportData;

	}

	public CognosReportData extractList(Element pgElement, XPathExpression<Element> xpath, XmlParser xp,
			ParserObject reportPo, CognosReportData cognosReportData) {
		List<Element> listColumn = null;
		try {
			if (pgElement.getChild("listColumns", reportPo.getNs()) != null) {
				listColumn = pgElement.getChild("listColumns", reportPo.getNs()).getChildren("listColumn",
						reportPo.getNs());
				for (Element col : listColumn) {
					List<Element> columnElements = col.getChild("listColumnBody", reportPo.getNs())
							.getChild("contents", reportPo.getNs()).getChildren();
					for (Element columnElement : columnElements) {
						String elementType = columnElement.getName();
						switch (elementType) {
						case ("list"):
							cognosReportData = extractList(columnElement, xpath, xp, reportPo, cognosReportData);
							break;
						case ("textItem"):
							String refDataItem = null;
							String label = null;
							if (null != col.getChild("listColumnBody", reportPo.getNs())
									.getChild("contents", reportPo.getNs()).getChild("textItem", reportPo.getNs())
									.getChild("dataSource", reportPo.getNs())
									&& null != col.getChild("listColumnBody", reportPo.getNs())
											.getChild("contents", reportPo.getNs())
											.getChild("textItem", reportPo.getNs())
											.getChild("dataSource", reportPo.getNs())
											.getChild("dataItemValue", reportPo.getNs())) {
								refDataItem = col.getChild("listColumnBody", reportPo.getNs())
										.getChild("contents", reportPo.getNs()).getChild("textItem", reportPo.getNs())
										.getChild("dataSource", reportPo.getNs())
										.getChild("dataItemValue", reportPo.getNs()).getAttributeValue("refDataItem");
							}
							if (col.getChild("listColumnTitle", reportPo.getNs()) != null
									&& col.getChild("listColumnTitle", reportPo.getNs()).getChild("contents",
											reportPo.getNs()) != null
									&& col.getChild("listColumnTitle", reportPo.getNs())
											.getChild("contents", reportPo.getNs())
											.getChild("textItem", reportPo.getNs()) != null) {
								if (null != col.getChild("listColumnTitle", reportPo.getNs())
										.getChild("contents", reportPo.getNs()).getChild("textItem", reportPo.getNs())
										.getChild("dataSource", reportPo.getNs())
										.getChild("dataItemLabel", reportPo.getNs())) {
									label = col.getChild("listColumnTitle", reportPo.getNs())
											.getChild("contents", reportPo.getNs())
											.getChild("textItem", reportPo.getNs())
											.getChild("dataSource", reportPo.getNs())
											.getChild("dataItemLabel", reportPo.getNs())
											.getAttributeValue("refDataItem");
								} else if (null != col.getChild("listColumnTitle", reportPo.getNs())
										.getChild("contents", reportPo.getNs()).getChild("textItem", reportPo.getNs())
										.getChild("dataSource", reportPo.getNs())
										.getChild("staticValue", reportPo.getNs())) {
									label = col.getChild("listColumnTitle", reportPo.getNs())
											.getChild("contents", reportPo.getNs())
											.getChild("textItem", reportPo.getNs())
											.getChild("dataSource", reportPo.getNs())
											.getChildText("staticValue", reportPo.getNs());
								}
							}

							Set<String> key = cognosReportData.getCatalogQueryMap().keySet();

							for (String keyName : key) {
								Map<String, Map<String, List<CatalogColumn>>> namespaceMap = cognosReportData
										.getCatalogQueryMap().get(keyName);
								for (String namespaceName : namespaceMap.keySet()) {
									Map<String, List<CatalogColumn>> tableMap = namespaceMap.get(namespaceName);
									for (String tableName : tableMap.keySet()) {
										List<CatalogColumn> catalogColumnList = tableMap.get(tableName);
										for (CatalogColumn catalogColumn : catalogColumnList) {
											if (refDataItem.equals(catalogColumn.getColumnName())) {
												catalogColumn.setBusinessColumnName(label);
												continue;
											}
										}
									}
								}
							}

							break;

						default:
							break;
						}
					}
				}
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cognosReportData;
	}

}
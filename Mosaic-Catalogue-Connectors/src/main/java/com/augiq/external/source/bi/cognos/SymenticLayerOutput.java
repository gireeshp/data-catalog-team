/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SymenticLayerOutput")
public class SymenticLayerOutput {
	private String packageName;
	private CommonDataModel commonDataModel;
	private Map<String,DataModelLayer> dataModelLayer; 
	@XmlElement(name = "commonDataModel")
	public CommonDataModel getCommonDataModel() {
		return commonDataModel;
	}
	public void setCommonDataModel(CommonDataModel commonDataModel) {
		this.commonDataModel = commonDataModel;
	}
	@XmlElementWrapper(name = "dataModelLayers")
	@XmlElement(name = "dataModelLayer")
	public Map<String, DataModelLayer> getDataModelLayer() {
		return dataModelLayer;
	}
	public void setDataModelLayer(Map<String, DataModelLayer> dataModelLayer) {
		this.dataModelLayer = dataModelLayer;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
}

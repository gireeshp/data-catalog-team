package com.augiq.external.source.bi.cognos;

public class SymenticObject {
	
	private String cognosXmlPath;
	private String modelXmlFile;
	private boolean isModelExists;
	private String symenticPath;
	private String symenticId;
	private String symenticName;
	private String dataSourceName;
	private String dbName;
	private String schemaName;
	private String connectionUrl;
	private String dbUserName;
	private String dbPassword;

	
	public String getModelXmlFile() {
		return modelXmlFile;
	}
	public void setModelXmlFile(String modelXmlFile) {
		this.modelXmlFile = modelXmlFile;
	}
	public boolean isModelExists() {
		return isModelExists;
	}
	public void setModelExists(boolean isModelExists) {
		this.isModelExists = isModelExists;
	}
	public String getCognosXmlPath() {
		return cognosXmlPath;
	}
	public void setCognosXmlPath(String cognosXmlPath) {
		this.cognosXmlPath = cognosXmlPath;
	}
	
	public String getSymenticPath() {
		return symenticPath;
	}
	public void setSymenticPath(String symenticPath) {
		this.symenticPath = symenticPath;
	}
	public String getSymenticId() {
		return symenticId;
	}
	public void setSymenticId(String symenticId) {
		this.symenticId = symenticId;
	}
	public String getSymenticName() {
		return symenticName;
	}
	public void setSymenticName(String symenticName) {
		this.symenticName = symenticName;
	}
	public String getDataSourceName() {
		return dataSourceName;
	}
	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getSchemaName() {
		return schemaName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
	public String getConnectionUrl() {
		return connectionUrl;
	}
	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}
	public String getDbUserName() {
		return dbUserName;
	}
	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}
	public String getDbPassword() {
		return dbPassword;
	}
	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
	
	
}

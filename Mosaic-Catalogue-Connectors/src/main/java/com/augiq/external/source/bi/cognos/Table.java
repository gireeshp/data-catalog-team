/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;
import java.util.Map;


public class Table {
	private String tableName;
	private String physicalTableName;
	private String aliasNameShortcut;
	private List<String> aliasNameShortcuts;
    private Map<String,Column> columnMap;
    private List<String> dataModels;
    private String nativeSql;
    private List<String> qualifyColumnList;
    private List<String> whereClauseList;
    private int qlikAliasCount;
    public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public Map<String,Column> getColumnMap() {
		return columnMap;
	}
	public void setColumnMap(Map<String,Column> columnMap) {
		this.columnMap = columnMap;
	}
	public String getAliasNameShortcut() {
		return aliasNameShortcut;
	}
	public void setAliasNameShortcut(String aliasNameShortcut) {
		this.aliasNameShortcut = aliasNameShortcut;
	}
	public String getNativeSql() {
		return nativeSql;
	}
	public void setNativeSql(String nativeSql) {
		this.nativeSql = nativeSql;
	}
	public List<String> getDataModels() {
		return dataModels;
	}
	public void setDataModels(List<String> dataModels) {
		this.dataModels = dataModels;
	}
	public List<String> getAliasNameShortcuts() {
		return aliasNameShortcuts;
	}
	public void setAliasNameShortcuts(List<String> aliasNameShortcuts) {
		this.aliasNameShortcuts = aliasNameShortcuts;
	}
	public String getPhysicalTableName() {
		return physicalTableName;
	}
	public void setPhysicalTableName(String physicalTableName) {
		this.physicalTableName = physicalTableName;
	}
	public List<String> getQualifyColumnList() {
		return qualifyColumnList;
	}
	public void setQualifyColumnList(List<String> qualifyColumnList) {
		this.qualifyColumnList = qualifyColumnList;
	}
	public List<String> getWhereClauseList() {
		return whereClauseList;
	}
	public void setWhereClauseList(List<String> whereClauseList) {
		this.whereClauseList = whereClauseList;
	}
	public int getQlikAliasCount() {
		return qlikAliasCount;
	}
	public void setQlikAliasCount(int qlikAliasCount) {
		this.qlikAliasCount = qlikAliasCount;
	}
	
		
           
}

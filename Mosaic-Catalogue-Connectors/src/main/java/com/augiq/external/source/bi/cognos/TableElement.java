/**
 * 
 */
package com.augiq.external.source.bi.cognos;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 */
public class TableElement {
	private String name;
	private String refQuery;
	private List<Column> columnList;
	private List<String> totalExpression;
	private String captionColor;
	private boolean isConditionalStyleUsed;
	private String conditionalStyleName;
	private boolean treatAsPivot;
	private List<String> groupingColumns;
	private boolean isSectionUsed;
	private Map<String, String> sortingColumns;
	/**
	 * @return the isConditionalStyleUsed
	 */
	public boolean isConditionalStyleUsed() {
		return isConditionalStyleUsed;
	}
	/**
	 * @param isConditionalStyleUsed the isConditionalStyleUsed to set
	 */
	public void setConditionalStyleUsed(boolean isConditionalStyleUsed) {
		this.isConditionalStyleUsed = isConditionalStyleUsed;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the refQuery
	 */
	public String getRefQuery() {
		return refQuery;
	}
	/**
	 * @param refQuery the refQuery to set
	 */
	public void setRefQuery(String refQuery) {
		this.refQuery = refQuery;
	}
	/**
	 * @return the columnList
	 */
	public List<Column> getColumnList() {
		return columnList;
	}
	/**
	 * @param columnList the columnList to set
	 */
	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}
	/**
	 * @return the totalExpression
	 */
	public List<String> getTotalExpression() {
		return totalExpression;
	}
	/**
	 * @param totalExpression the totalExpression to set
	 */
	public void setTotalExpression(List<String> totalExpression) {
		this.totalExpression = totalExpression;
	}
	public String getCaptionColor() {
		return captionColor;
	}
	public void setCaptionColor(String captionColor) {
		this.captionColor = captionColor;
	}
	public Object getConditionalStyleName() {
		return conditionalStyleName;
	}
	/**
	 * @param conditionalStyleName the conditionalStyleName to set
	 */
	public void setConditionalStyleName(String conditionalStyleName) {
		this.conditionalStyleName = conditionalStyleName;
	}
	public boolean isTreatAsPivot() {
		return treatAsPivot;
	}
	public void setTreatAsPivot(boolean treatAsPivot) {
		this.treatAsPivot = treatAsPivot;
	}
	public List<String> getGroupingColumns() {
		return groupingColumns;
	}
	public void setGroupingColumns(List<String> groupingColumns) {
		this.groupingColumns = groupingColumns;
	}
	public boolean isSectionUsed() {
		return isSectionUsed;
	}
	public void setSectionUsed(boolean isSectionUsed) {
		this.isSectionUsed = isSectionUsed;
	}
	public Map<String, String> getSortingColumns() {
		return sortingColumns;
	}
	public void setSortingColumns(Map<String, String> sortingColumns) {
		this.sortingColumns = sortingColumns;
	}
	
}

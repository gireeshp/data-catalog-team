package com.augiq.external.source.bi.cognos;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;


public class XmlParser {
	public XmlParser() {
		super();
	}
	public static void main(String[] args) {
		
		System.out.println("parser");
	}

	/**
	 * This method is used to parse symentic and report xml files
	 *  
	 * @param fileName This is the paramter for xml fileName (model.xml or report.xml)
	 * @return ParserObject
	 */
	public ParserObject getXmlParserObject(String fileName){
		SAXBuilder saxBuilder =null;
		Document document =null;
		Element rootElement = null;
		Namespace ns=null;
		ParserObject po=null;
		try{
			po=new ParserObject();
			saxBuilder = new SAXBuilder();
			document = saxBuilder.build(new URL("file:///" + fileName));
			rootElement = document.getRootElement();
			if(rootElement.getNamespacePrefix().equals("")){
				if(rootElement.getNamespaceURI().equals("")){
					ns=Namespace.NO_NAMESPACE;
					po.setDefaultPrifix(false);
				}else{
					ns = Namespace.getNamespace("ns",rootElement.getNamespaceURI());
					po.setDefaultPrifix(true);
				}	
			}else{
				ns = rootElement.getNamespace();
				po.setDefaultPrifix(false);
			}
			po.setSaxBuilder(saxBuilder);
			po.setDocument(document);
			po.setRootElement(rootElement);
			po.setNs(ns);	
		}catch(JDOMException je){
			je.printStackTrace();
		}catch(IOException ie){
			ie.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			rootElement = null;
			document =null;
			saxBuilder =null;
			ns=null;
		}
		return po;
	}
	
	/**
	 * This method is used to replace namespace if any namespace is available in the xml files
	 *  
	 * @param po is the ParserObject instance of xml file(model.xml or report.xml)
	 * @return expression is the xpath query string
	 */
	public String getExpression(ParserObject po,String expression){
		return po.isDefaultPrifix()==true?expression:(expression.replaceAll("ns:", ""));
	}
	public List<Element> getXmlElementList(ParserObject po, String expression){
		XPathExpression<Element> xpath =null;
		List<Element> listData=null;
		try{
			expression=getExpression(po,expression);
			xpath =XPathFactory.instance().compile(expression, Filters.element(),null,po.getNs());
			listData=xpath.evaluate(po.getDocument());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			xpath = null;
		}
		return listData;
	}
}
package com.augiq.external.source.bi.cognos.scanning.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.jdom2.Element;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.augiq.external.source.bi.cognos.CatalogColumn;
import com.augiq.external.source.bi.cognos.CognosModelData;
import com.augiq.external.source.bi.cognos.CognosModelExtractor;
import com.augiq.external.source.bi.cognos.CognosReportData;
import com.augiq.external.source.bi.cognos.CognosReportMetadata;
import com.augiq.external.source.bi.cognos.CognosReportQueryExtractor;
import com.augiq.external.source.bi.cognos.CognosXmlDownloader;
import com.augiq.external.source.bi.cognos.ColumnBusinessName;
import com.augiq.external.source.bi.cognos.ParserObject;
import com.augiq.external.source.bi.cognos.Table;
import com.augiq.external.source.bi.cognos.XmlParser;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.Actions;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.constant.configuration.generic.GenericConstants;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.core.dao.searchdao.IndexDefinationDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.CognosReportPublishColumn;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.Join;
import com.augmentiq.maxiq.entity.model.configuration.bean.ReportPublishData;
import com.augmentiq.maxiq.external.source.connection.message.constants.TestConnectionEnums;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.repository.connector.external.connectiondao.ConnectorListingDAO;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.cognos.developer.schemas.bibus._3.BiBusHeader;
import com.cognos.developer.schemas.bibus._3.ContentManagerService_PortType;
import com.cognos.developer.schemas.bibus._3.ContentManagerService_ServiceLocator;
import com.cognos.developer.schemas.bibus._3.PropEnum;
import com.cognos.developer.schemas.bibus._3.QueryOptions;
import com.cognos.developer.schemas.bibus._3.SearchPathMultipleObject;
import com.cognos.developer.schemas.bibus._3.Sort;
import com.cognos.developer.schemas.bibus._3.XmlEncodedXML;
import com.cognos.org.apache.axis.client.Stub;
import com.cognos.org.apache.axis.message.SOAPHeaderElement;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class CognosBIScanner implements Scanner {

	@Autowired
	private ConnectorListingDAO biConnectorListingDAO;

	@Autowired
	private CognosXmlDownloader cognosXmlDownloader;

	@Autowired
	private CognosReportQueryExtractor cognosReportQueryExtractor;
	
	@Autowired
    private ConfigurationCreateUtil createUtil;

	private ConnectionConfig biConnectionConfig;

	private String subSourceType;

	private ApplicationContext context;

	private ContentManagerService_PortType cmService = null;

	private static final Logger logger = LoggerFactory.getLogger(CognosBIScanner.class);

	public String getSubSourceType() {
		return subSourceType;
	}

	public void setSubSourceType(String subSourceType) {
		this.subSourceType = subSourceType;
	}

	@Override
	public DataNode scan(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = (ConnectionSources) args.get("ConnectionSources");
		biConnectionConfig = getConnectionConfig(connectionSources);
		List<CognosReportMetadata> reportMetadata = fetchAllReportsOfContentStore(biConnectionConfig);
		DataNode dataNode = createDataNodeOfReportMetaData(reportMetadata);
		Gson gson = new Gson();
		System.out.println(gson.toJson(dataNode));
		return dataNode;
	}

	@Override
	public Map<String,List<String>> publishDataSources(Map<String, Object> args) throws Exception {

		Map<String, List<String>> existingDSNotifier = new HashMap<String, List<String>>();
		existingDSNotifier.put(TaskStatusEnums.SUCCESS.name(), new ArrayList<String>());
		args.put("existingDataSourceNotifier", existingDSNotifier);
		DataNode dataNode = new DataNode();
		ConnectionSources connectionSources = objectParserHandler((Object) args.get("connectionSources"),
				ConnectionSources.class);
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);
		// Complete Node Hierarchy from UI(consisting of maxied collection i.e.
		// selected/unselected)
		DataNode dataNodefromUi = objectParserHandler((Object) args.get("dataNode"), DataNode.class);
		// filtered nodes consisting of hierarchy only of selected nodes with
		// complete path
		DataNode filteredNodes = dataNode.getSelectedNodes(dataNodefromUi);
		// nodes whose datasource is to be created
		List<CognosReportMetadata> cognosReportMetaDataList = getSelectedReportList(filteredNodes);

		for (CognosReportMetadata cognosReportMetadata : cognosReportMetaDataList) {
			ReportPublishData reportPublishData = new ReportPublishData();
			reportPublishData.setReportName(cognosReportMetadata.getReportName());
			reportPublishData.setPackageName(cognosReportMetadata.getReportPackageName());
//			reportPublishData.setTables(cognosReportMetadata.getReportTableinfo());
//			reportPublishData.setTablesBusinessColumn(cognosReportMetadata.getReportTableBusinessColInfo());
			reportPublishData.setJoins(cognosReportMetadata.getJoin());
			reportPublishData.setTables(cognosReportMetadata.getReportTableBusinessColInfo());
			reportPublishData.setSearchPath(cognosReportMetadata.getReportSearchPath());
			args.put(GenericConstants.DATA_SOURCE.DATASOURCENAME, reportPublishData.getReportName());
			Long dsId = createUtil.createDataSources(args);
			if (dsId == null) {
				continue;
			}
			List<FieldMapping> fieldMappings = new ArrayList<FieldMapping>();
			FieldMapping fieldMapping = new FieldMapping();
			fieldMapping.setReportPublishData(reportPublishData);
			fieldMappings.add(fieldMapping);
			DataSource dataSource = DataRepositoryDao.getDataSource(dsId);
			dataSource.setAdvancedValidations(null);
			dataSource.setAdvancedValidationsStore(null);
			dataSource.setTransformations(null);
			dataSource.setDataCleansers(null);
			dataSource.setAppId(null);
			dataSource.setNodeId(null);
			dataSource.setConfig(connectionConfig);
			dataSource.setDataSourceType(DataSourceType.COGNOS_DATA_SOURCE);
			dataSource.setFieldMappings(fieldMappings);
			DataRepositoryDao.insertDataSource(dataSource);
			Map<String, String> updatedValue = new LinkedHashMap<String, String>();
			updatedValue.put("dataSourceType", (DataSourceType.COGNOS_DATA_SOURCE).toString());
			DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(dataSource.getId(), updatedValue);
		}
		
		return existingDSNotifier;

	}

	@Override
	public String testConnection(Map<String, Object> args) {

		Connection connection = null;
		ConnectionConfig connectionConfig = (ConnectionConfig) args.get("connectionConfig");
		this.subSourceType = args.get("subSourceType").toString();
		String message = TestConnectionEnums.FAIL.name();
		try {
			message = establishedConnection(connectionConfig);
		} catch (ClassNotFoundException | ServiceException | SystemException e) {
			return message;
		}
		cmService = null;
		return message;
	}

	public String establishedConnection(ConnectionConfig connectionConfig)
			throws ServiceException, ClassNotFoundException, SystemException {
		String message = TestConnectionEnums.FAIL.name();
		String userName = connectionConfig.getDbUserName();
		String password = connectionConfig.getDbPassword();
		Integer port = connectionConfig.getPort();
		String host = connectionConfig.getIpAddress();
		String dispatcherURL = createDispatchURL(connectionConfig);
		String nsID = connectionConfig.getNameSpace();
		message = connectToCognos(dispatcherURL);
		// message = logonToCognos(nsID, userName, password);
		return message;
	}

	public String createDispatchURL(ConnectionConfig connectionConfig) {
		Integer port = connectionConfig.getPort();
		String host = connectionConfig.getIpAddress();
		return "http://" + host + ":" + port + "/p2pd/servlet/dispatch";
	}

	private String connectToCognos(String dispatcherURL) {
		ContentManagerService_ServiceLocator cmServiceLocator = new ContentManagerService_ServiceLocator();
		try {
			URL url = new URL(dispatcherURL);
			cmService = cmServiceLocator.getcontentManagerService(url);
		} catch (ServiceException | MalformedURLException e) {
			return TestConnectionEnums.FAIL.name();
		}
		return TestConnectionEnums.SUCCESS.name();
	}

	private String logonToCognos(String nsID, String user, String pswd) {
		StringBuffer credentialXML = new StringBuffer();
		credentialXML.append("<credential>");
		credentialXML.append("<namespace>").append(nsID).append("</namespace>");
		credentialXML.append("<username>").append(user).append("</username>");
		credentialXML.append("<password>").append(pswd).append("</password>");
		credentialXML.append("</credential>");
		String encodedCredentials = credentialXML.toString();
		XmlEncodedXML xmlCredentials = new XmlEncodedXML();
		xmlCredentials.set_value(encodedCredentials);
		try {
			cmService.logon(xmlCredentials, null);
			SOAPHeaderElement temp = ((Stub) cmService)
					.getResponseHeader("http://developer.cognos.com/schemas/bibus/3/", "biBusHeader");
			BiBusHeader CMbibus = (BiBusHeader) temp
					.getValueAsType(new QName("http://developer.cognos.com/schemas/bibus/3/", "biBusHeader"));
			((Stub) cmService).setHeader("http://developer.cognos.com/schemas/bibus/3/", "biBusHeader", CMbibus);
		} catch (Exception ex) {
			return TestConnectionEnums.FAIL.name();
		}
		return TestConnectionEnums.SUCCESS.name();
	}

	@Transactional
	private ConnectionConfig getConnectionConfig(ConnectionSources connectionSources) {
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		Long connectionId = connectionSubSources.getConnectionConfig().get(0).getConnectionId();
		return biConnectorListingDAO.getConnectionConfig(connectionId);
	}

	public List<CognosReportMetadata> fetchAllReportsOfContentStore(ConnectionConfig config) {
		PropEnum props[] = new PropEnum[] { PropEnum.searchPath, PropEnum.defaultName, PropEnum.specification };
		BaseClass bc[] = null;
		Map<String, String> reportPathMap = new HashMap<String, String>();
		String dispatcherURL = createDispatchURL(config);
		String searchPath = "/content//report";
		try {
			String message = establishedConnection(config);
		} catch (ClassNotFoundException | ServiceException | SystemException e) {
			e.printStackTrace();
		}
		// connectToCognos(dispatcherURL);
		try {
			SearchPathMultipleObject spMulti = new SearchPathMultipleObject(searchPath);
			bc = cmService.query(spMulti, props, new Sort[] {}, new QueryOptions());
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("PACKAGES:\n");
		if (bc != null) {
			for (int i = 0; i < bc.length; i++) {
				if (!bc[i].getSearchPath().getValue().contains("agentDefinition")) {
					System.out.println(bc[i].getDefaultName().getValue() + " - " + bc[i].getSearchPath().getValue());
					reportPathMap.put(bc[i].getSearchPath().getValue(), bc[i].getDefaultName().getValue());
				}

				if (i > 9) {
					break;
				}

			}
		}
		
		/*BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(new File("/home/pcadmin/hari/my_workplace/mosaic/catalog/baseline/test.txt")));
		
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
			String[] split = line.split(" - ");
			reportPathMap.put(split[1], split[0]);
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		cmService = null;
		bc = null;

		return processAllReports(reportPathMap, dispatcherURL);

	}

	public List<CognosReportMetadata> processAllReports(Map<String, String> reportPathMap, String dispatcherURL) {
		CognosReportMetadata cognosReportMetadata = null;
		List<CognosReportMetadata> cognosReportMetadataList = new ArrayList<CognosReportMetadata>();
		File dir = new File(Cache.getProperty(CacheConstants.COGNOS_XML_PATH));
		if (!dir.exists()) {
			dir.mkdirs();
			System.out.println("Directory created :-" + dir.getAbsolutePath());
		}
		for (String searchPath : reportPathMap.keySet()) {
			String reportName = reportPathMap.get(searchPath);

			// if ("Blank".equals(reportName)) {
			System.out.println(" Processing report -: " + reportName);

			/*cognosXmlDownloader.downloadReportXml(searchPath, reportName,
					Cache.getProperty(CacheConstants.COGNOS_XML_PATH), dispatcherURL);*/

			String packagePath = getModelXmlPath(reportName, Cache.getProperty(CacheConstants.COGNOS_XML_PATH))
					.split("/model")[0];
			String packageName = packagePath.split("/package\\[@name='")[packagePath.split("/package\\[@name='").length
					- 1].replace("']", "");

			/*cognosXmlDownloader.downloadModelXml(packagePath.concat("/model"), packageName,
					Cache.getProperty(CacheConstants.COGNOS_XML_PATH), dispatcherURL);*/

			CognosReportData cognosReportData = cognosReportQueryExtractor.extractQueriesFromReport(packageName,
					reportName, Cache.getProperty(CacheConstants.COGNOS_XML_PATH));
			CognosModelExtractor cognosModelExtractor = new CognosModelExtractor();
			CognosModelData cognosModelData = cognosModelExtractor.extractModel(packagePath,
					Cache.getProperty(CacheConstants.COGNOS_XML_PATH), cognosReportData);
			
			/*Set<String>tableKeys = cognosModelData.getExtractLayerData().getTables().keySet();
			for (String tableName : tableKeys) {
				Table table = cognosModelData.getExtractLayerData().getTables().get(tableName);
				Set<String> ColumnSet = table.getColumnMap().keySet();
				for (String columnName : ColumnSet) {
					Set<String> key = cognosReportData.getCatalogQueryMap().keySet();
					for (String keyName : key) {
						Map<String, Map<String, List<CatalogColumn>>> namespaceMap = cognosReportData
								.getCatalogQueryMap().get(keyName);
						for (String namespaceName : namespaceMap.keySet()) {
							Map<String, List<CatalogColumn>> tableMap = namespaceMap.get(namespaceName);
							for (String tableNameReportData : tableMap.keySet()) {
								List<CatalogColumn> catalogColumnList = tableMap.get(tableNameReportData);
								for (CatalogColumn catalogColumn : catalogColumnList) {
									if (columnName.equals(catalogColumn.getColumnName())) {
										cognosModelData.getExtractLayerData().getTables().get(tableName).getColumnMap().get(columnName).setColumnBusinessName(catalogColumn.getColumnName());
										System.out.println("Column Name :" + catalogColumn.getColumnName() + " ::Column Business Name: " + catalogColumn.getBusinessColumnName());
									}
								}
							}
						}
					}
				}
				
			}*/

			System.out.println(" ---------Report Processing completion for " + reportName + " -----------");

			cognosReportMetadata = new CognosReportMetadata();
			cognosReportMetadata.setReportName(reportName);
			cognosReportMetadata.setReportPackageName(packageName);
			cognosReportMetadata.setReportSearchPath(searchPath);
			List<String> columnList = null;
			Map<String, List<String>> tableMap = new HashMap<String, List<String>>();
			if (null != cognosModelData.getExtractLayerData().getTables()) {
				for (String tableName : cognosModelData.getExtractLayerData().getTables().keySet()) {
					columnList = new ArrayList<String>();
					for (String columnName : cognosModelData.getExtractLayerData().getTables().get(tableName)
							.getColumnMap().keySet()) {
						columnList.add(cognosModelData.getExtractLayerData().getTables().get(tableName).getColumnMap()
								.get(columnName).getColumnName());
					}
					tableMap.put(tableName, columnList);
				}
			}

			cognosReportMetadata.setReportTableinfo(tableMap);
			
			ColumnBusinessName columnBusinessName = null;
			List<ColumnBusinessName> columnBusinessNameList = null;
			Map<String, List<ColumnBusinessName>> tableBusinessNameMap = new HashMap<String, List<ColumnBusinessName>>();
			if (null != cognosModelData.getExtractLayerData().getTables()) {
				for (String tableName : cognosModelData.getExtractLayerData().getTables().keySet()) {
					columnBusinessNameList = new ArrayList<ColumnBusinessName>();
					for (String columnName : cognosModelData.getExtractLayerData().getTables().get(tableName)
							.getColumnMap().keySet()) {
						/*
						 * columnBusinessNameList.add(cognosModelData.getExtractLayerData().getTables().
						 * get(tableName).getColumnMap() .get(columnName).getColumnName());
						 */
						columnBusinessName = new ColumnBusinessName();
						columnBusinessName.setColumnBusinessName(cognosModelData.getExtractLayerData().getTables()
								.get(tableName).getColumnMap().get(columnName).getColumnBusinessName());
						columnBusinessName.setColumnName(cognosModelData.getExtractLayerData().getTables()
								.get(tableName).getColumnMap().get(columnName).getColumnName());
						columnBusinessNameList.add(columnBusinessName);
					}
					tableBusinessNameMap.put(tableName, columnBusinessNameList);
				}
			}
			
			cognosReportMetadata.setReportBusinessTableColumnName(tableBusinessNameMap);
			
			
			if (null != cognosModelData.getDataModelLayerDatas().get("Centralized")) {
				cognosReportMetadata.setJoin(cognosModelData.getDataModelLayerDatas().get("Centralized").getJoinList());
			} else {
				cognosReportMetadata.setJoin(new ArrayList());
			}

			List<String> dataSourceList = new ArrayList<>();

			for (String dataSourceName : cognosModelData.getDbSchemaList()) {
				dataSourceList.add(dataSourceName);
			}
			cognosReportMetadata.setDataSourceName(dataSourceList);
			cognosReportMetadataList.add(cognosReportMetadata);
			// }
		}

		return cognosReportMetadataList;
	}

	public String getModelXmlPath(String reportName, String cognosXmlDownloadPath) {
		XmlParser xmlParser = new XmlParser();
		ParserObject reportPo = xmlParser.getXmlParserObject(cognosXmlDownloadPath + "/Report" + reportName + ".xml");
		Element reportElement = reportPo.getRootElement();
		XPathExpression<Element> xpath = XPathFactory.instance().compile(
				(xmlParser.getExpression(reportPo,
						"//ns:report//ns:query//ns:joinOperation|//ns:report//ns:query//ns:queryOperation|//ns:report//ns:query//ns:sqlText")),
				Filters.element(), null, reportPo.getNs());
		return reportElement.getChildText("modelPath", reportPo.getNs());
	}

	public DataNode createDataNodeOfReportMetaData(List<CognosReportMetadata> reportMetaData) {

		DataNode contentStoreNode = new DataNode("Content Store", false, false);

		for (CognosReportMetadata cognosReportMetadata : reportMetaData) {
			DataNode reportNode = new DataNode(cognosReportMetadata.getReportName(), true);
//			reportNode.setLabel(cognosReportMetadata.getReportName());
			DataNode modelNode = new DataNode();
			modelNode.setLabel("Model Name");
			DataNode modelName = new DataNode(cognosReportMetadata.getReportPackageName(), false);
			modelNode.getChildren().add(modelName);
			DataNode searchPathNode = new DataNode();
			searchPathNode.setLabel("Search Path");
			DataNode searchPath = new DataNode(cognosReportMetadata.getReportSearchPath(), false);
			searchPathNode.getChildren().add(searchPath);
			DataNode tableNode = new DataNode();
			tableNode.setLabel("Tables");
			
			for (String tableName : cognosReportMetadata.getReportBusinessTableColumnName().keySet()) {
				DataNode table = new DataNode();
				table.setLabel(tableName);
				for ( ColumnBusinessName columnName : cognosReportMetadata.getReportBusinessTableColumnName().get(tableName)) {
					if (null != columnName.getColumnBusinessName()) {
						DataNode columnNode = new DataNode(columnName.getColumnName(), false);
						DataNode businessLabelNode = new DataNode();
						businessLabelNode.setLabel("Business Name");
						DataNode businessColumnNode = new DataNode(columnName.getColumnBusinessName(), false);
						businessLabelNode.getChildren().add(businessColumnNode);
						columnNode.getChildren().add(businessLabelNode);
						table.getChildren().add(columnNode);
						
					} else {
						DataNode columnNode = new DataNode(columnName.getColumnName(), false);
						table.getChildren().add(columnNode);
					}
				}
				tableNode.getChildren().add(table);
			}

			DataNode joinNode = new DataNode();
			joinNode.setLabel("Joins");

			for (Join joinname : cognosReportMetadata.getJoin()) {
				DataNode join = new DataNode();
				join.setLabel(joinname.getKeyAlias());
				DataNode leftJoinTable = new DataNode();
				leftJoinTable.setLabel(joinname.getLeftTable());
				for (String leftColumnName : joinname.getLeftColumn()) {
					DataNode leftColumn = new DataNode(leftColumnName, false);
					leftJoinTable.getChildren().add(leftColumn);
				}
				DataNode rightJoinTable = new DataNode();
				rightJoinTable.setLabel(joinname.getRightTable());
				for (String rightColumnName : joinname.getRightColumn()) {
					DataNode rightColumn = new DataNode(rightColumnName, false);
					rightJoinTable.getChildren().add(rightColumn);
				}
				join.getChildren().add(leftJoinTable);
				join.getChildren().add(rightJoinTable);
				joinNode.getChildren().add(join);
			}

			/*
			 * DataNode dataSourceNode = new DataNode();
			 * dataSourceNode.setLabel("Data Sources"); for (String
			 * dataSourceName : cognosReportMetadata.getDataSourceName()) {
			 * DataNode dataSourceNameNode = new DataNode(dataSourceName,
			 * false); dataSourceNode.getChildren().add(dataSourceNameNode); }
			 */
			reportNode.getChildren().add(modelNode);
			reportNode.getChildren().add(searchPathNode);
			reportNode.getChildren().add(tableNode);
			reportNode.getChildren().add(joinNode);
			// reportNode.getChildren().add(dataSourceNode);
			contentStoreNode.getChildren().add(reportNode);
		}

		return contentStoreNode;
	}

	public static <T> T objectParserHandler(Object object, Class<T> entity)
			throws JsonParseException, JsonMappingException, IOException {
		String jsonString = new Gson().toJson(object);
		ObjectMapper mapper = new ObjectMapper();
		T obj = mapper.readValue(jsonString, entity);
		return obj;

	}

	public List<CognosReportMetadata> getSelectedReportList(DataNode dataNode) {

		List<CognosReportMetadata> cognosReportMetaDataList = new ArrayList<CognosReportMetadata>();
		Collection<DataNode> reportList = dataNode.getChildren();
		Collection<Map<String, List<String>>> reportNameWithChild = new ArrayList<Map<String, List<String>>>();
		Map<String, List<String>> currentReportWithChildDetails;
		if (null != reportList && !reportList.isEmpty()) {
			for (DataNode currentReportNode : reportList) {
				CognosReportMetadata reportMetaData = new CognosReportMetadata();
				currentReportWithChildDetails = new HashMap<String, List<String>>();
				reportMetaData.setReportName(currentReportNode.getLabel());
				Collection<DataNode> reportChildList = currentReportNode.getChildren();
				Iterator<DataNode> reportChildIterator = reportChildList.iterator();
				while (reportChildIterator.hasNext()) {
					DataNode childDataNode = (DataNode) reportChildIterator.next();
					switch (childDataNode.getLabel()) {
					case "Tables":
						Collection<DataNode> tableList = childDataNode.getChildren();
						Iterator<DataNode> tableListIterator = tableList.iterator();
						Map<String, List<String>> reportTableInfo = new HashMap<String, List<String>>();
						Map<String, List<CognosReportPublishColumn>> reportTableBusinessColInfo = new HashMap<String, List<CognosReportPublishColumn>>();
						while (tableListIterator.hasNext()) {
							DataNode tableNode = (DataNode) tableListIterator.next();
							String tableName = tableNode.getLabel();
							Collection<DataNode> columnList = tableNode.getChildren();
							List<String> columnNameList = new ArrayList<String>();
							List<CognosReportPublishColumn> columnBusinessNameList = new ArrayList<CognosReportPublishColumn>(); 
							CognosReportPublishColumn columnBusinessName = new CognosReportPublishColumn();
							Iterator<DataNode> columnListIterator = columnList.iterator();
							while (columnListIterator.hasNext()) {
								DataNode columnNode = (DataNode) columnListIterator.next();
								columnNameList.add(columnNode.getLabel());
								columnBusinessName.setColumnName(columnNode.getLabel());
								
								if (null != columnNode.getChildren()) {
									Collection<DataNode> businessNodeName = columnNode.getChildren();
									Iterator<DataNode> businessNodeNameIterator = businessNodeName.iterator();
									while (businessNodeNameIterator.hasNext()) {
										DataNode businessNode = (DataNode) businessNodeNameIterator.next();
										Collection<DataNode> businessColumnName = businessNode.getChildren();
										Iterator<DataNode> businessColumnNameIterator = businessColumnName.iterator();
										while (businessColumnNameIterator.hasNext()) {
											DataNode businessColumnNode = (DataNode) businessColumnNameIterator.next();
											columnBusinessName.setColumnBusinessName(businessColumnNode.getLabel());
										}
									}
								}
								columnBusinessNameList.add(columnBusinessName);
							}
							reportTableInfo.put(tableName, columnNameList);
							reportTableBusinessColInfo.put(tableName, columnBusinessNameList);
						}
						reportMetaData.setReportTableinfo(reportTableInfo);
						reportMetaData.setReportTableBusinessColInfo(reportTableBusinessColInfo);
						break;

					case "Joins":
						Collection<DataNode> joinNode = childDataNode.getChildren();
						Iterator<DataNode> joinNodeIterator = joinNode.iterator();
						List<Join> joinList = new ArrayList<Join>();
						boolean joinFlag = false;
						while (joinNodeIterator.hasNext()) {
							Join join = new Join();
							DataNode joinDataNode = (DataNode) joinNodeIterator.next();
							join.setKeyAlias(joinDataNode.getLabel());
							Collection<DataNode> joinTableNode = joinDataNode.getChildren();
							Iterator<DataNode> joinTableNodeIterator = joinTableNode.iterator();
							while (joinTableNodeIterator.hasNext()) {
								DataNode joinNameNode = (DataNode) joinTableNodeIterator.next();
								if (!joinFlag) {
									join.setLeftTable(joinNameNode.getLabel());
									Collection<DataNode> columnDataNode = joinNameNode.getChildren();
									Iterator<DataNode> columnDataNodeIterator = columnDataNode.iterator();
									List<String> leftColumnNameList = new ArrayList<String>();
									while (columnDataNodeIterator.hasNext()) {
										DataNode columnNode = (DataNode) columnDataNodeIterator.next();
										leftColumnNameList.add(columnNode.getLabel());
									}
									join.setLeftColumn(leftColumnNameList);
									joinFlag = true;
								} else {
									join.setRightTable(joinNameNode.getLabel());
									Collection<DataNode> columnRightDataNode = joinNameNode.getChildren();
									Iterator<DataNode> columnRightDataNodeIterator = columnRightDataNode.iterator();
									List<String> rightColumnNameList = new ArrayList<String>();
									while (columnRightDataNodeIterator.hasNext()) {
										DataNode columnRightNode = (DataNode) columnRightDataNodeIterator.next();
										rightColumnNameList.add(columnRightNode.getLabel());
									}
									join.setRightColumn(rightColumnNameList);
								}

							}

							joinList.add(join);
						}
						reportMetaData.setJoin(joinList);
						break;

					case "Search Path":
						Collection<DataNode> searchPathDataNode = childDataNode.getChildren();
						Iterator<DataNode> searchDataNodeIterator = searchPathDataNode.iterator();
						while (searchDataNodeIterator.hasNext()) {
							DataNode searchPathNameNode = (DataNode) searchDataNodeIterator.next();
							reportMetaData.setReportSearchPath(searchPathNameNode.getLabel());
						}

						break;

					case "Model Name":
						Collection<DataNode> modelDataNode = childDataNode.getChildren();
						Iterator<DataNode> modelDataNodeIterator = modelDataNode.iterator();
						while (modelDataNodeIterator.hasNext()) {
							DataNode modelNameNode = (DataNode) modelDataNodeIterator.next();
							reportMetaData.setReportPackageName(modelNameNode.getLabel());
						}

						break;
					default:
						break;
					}
				}

				cognosReportMetaDataList.add(reportMetaData);

			}
		}

		return cognosReportMetaDataList;
	}


	public static void main(String[] args) throws Exception {


		BufferedReader br = new BufferedReader(new FileReader(new File("/home/pcadmin/hari/my_workplace/mosaic/catalog/baseline/test.txt")));
		String line;
		while ((line = br.readLine()) != null) {
//			System.out.println(line);
			String[] split = line.split(" - ");
			System.out.println(split[0] + "--" + split[1]);
		}
	
	
	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}

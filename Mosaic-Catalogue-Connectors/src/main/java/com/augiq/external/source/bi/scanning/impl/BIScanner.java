package com.augiq.external.source.bi.scanning.impl;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.augiq.external.source.bi.cognos.CognosXmlDownloader;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;


public class BIScanner implements Scanner, ApplicationContextAware {

	private ApplicationContext context;
	
	@Override
	public DataNode scan(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = (ConnectionSources) args.get("ConnectionSources");
		ConnectionSubSources connectionSubSources = (ConnectionSubSources) connectionSources.getConnectionSubSources().get(0);
		Scanner scanner = ((Scanner)context.getBean(connectionSubSources.getSubConnectionType()));
		return scanner.scan(args);
	}

	@Override
	public Map<String,List<String>> publishDataSources(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = objectParserHandler((Object)args.get("connectionSources"), ConnectionSources.class);
		ConnectionSubSources connectionSubSources = (ConnectionSubSources) connectionSources.getConnectionSubSources().get(0);
		Scanner scanner = ((Scanner)context.getBean(connectionSubSources.getSubConnectionType()));
		return scanner.publishDataSources(args);
	}

	@Override
	public String testConnection(Map<String, Object> args) {
		return ((Scanner)context.getBean(args.get("subSourceType").toString())).testConnection(args);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;
	}
	
	public static <T> T objectParserHandler(Object object, Class<T> entity)
			throws JsonParseException, JsonMappingException, IOException {
		String jsonString = new Gson().toJson(object);
		ObjectMapper mapper = new ObjectMapper();
		T obj = mapper.readValue(jsonString, entity);
		return obj;

	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}

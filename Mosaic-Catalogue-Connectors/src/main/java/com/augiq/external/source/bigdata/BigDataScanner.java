package com.augiq.external.source.bigdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.constant.configuration.generic.GenericConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

/**
 * CLOUD level class to call publish, scan and test datasources for this
 * connection
 * 
 * @author 10644726
 *
 */
public class BigDataScanner implements Scanner, ApplicationContextAware {

	@Autowired
    private ConfigurationCreateUtil createUtil;
	
	private ApplicationContext context;

	@Override
	public DataNode scan(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = (ConnectionSources) args.get("ConnectionSources");
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		String subSourceType = connectionSubSources.getSubConnectionType();
		Scanner scanner = (Scanner) context.getBean(subSourceType);
		return scanner.scan(args);
	}

	@Override
	public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {
		DataNode dataNode = new DataNode();
		ConnectionSources connectionSources = RDBMSScanner.objectParserHandler((Object) args.get("connectionSources"),
				ConnectionSources.class);
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);

		DataNode dataNodefromUi = RDBMSScanner.objectParserHandler((Object) args.get("dataNode"), DataNode.class);
		DataNode filteredNodes = dataNode.getSelectedNodes(dataNodefromUi);
		if ("BLOB".equals(connectionSubSources.getSubConnectionType())) {
			args.put("filterNode", filteredNodes);
		}
		Collection<DataNode> nodesToPublish = filterNodesToPublish(filteredNodes.getChildren());

		Boolean doSampling = (Boolean) args.get("doSampling");
		Integer sampleRowCount = (Integer) args.get("sampleRowCount");

		// Publish each DataNode which is selected
		for (Iterator iterator = nodesToPublish.iterator(); iterator.hasNext();) {

			DataNode publishDataNode = (DataNode) iterator.next();

			String subSourceType = StringUtils.upperCase(connectionSubSources.getSubConnectionType());

			args.put(GenericConstants.DATA_SOURCE.DATASOURCENAME, publishDataNode.getLabel());
			Long dsId = createUtil.createDataSources(args);

			DataSource dataSource = DataRepositoryDao.getDataSource(dsId);
			dataSource.setAdvancedValidations(null);
			dataSource.setAdvancedValidationsStore(null);
			dataSource.setTransformations(null);
			dataSource.setDataCleansers(null);
			dataSource.setAppId(null);
			dataSource.setNodeId(null);

			Scanner scanner = (Scanner) context.getBean(subSourceType);

			args.put("dataSourceId", dsId);
			args.put("publishNode", publishDataNode);
			scanner.publishDataSources(args);
			
			Map<String, String> updatedValue = new LinkedHashMap<String, String>();
			updatedValue.put("dataSourceType", connectionSubSources.getSubConnectionType().toString());
			DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(dataSource.getId(), updatedValue);

		}

		return null;
	}

	/**
	 * @param dataNodes
	 *            - children datanodes of root node
	 * @return -a list of DataNode which are to be published in Catalog
	 */
	public Collection<DataNode> filterNodesToPublish(Collection<DataNode> dataNodes) {

		Collection<DataNode> validNodes = new ArrayList<DataNode>();

		for (DataNode itrDataNode : dataNodes) {
			if (!itrDataNode.getChildren().isEmpty()) {
				Collection<DataNode> subList = filterNodesToPublish(itrDataNode.getChildren());
				validNodes.addAll(subList);
			} else {
				if (itrDataNode.getIsDsNode() && itrDataNode.getSelected())
					validNodes.add(itrDataNode);
			}
		}

		return validNodes;
	}

	@Override
	public String testConnection(Map<String, Object> args) {
		String subSourceType = args.get("subSourceType").toString();
		Scanner scanner = (Scanner) context.getBean(subSourceType);
		return scanner.testConnection(args);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;

	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}


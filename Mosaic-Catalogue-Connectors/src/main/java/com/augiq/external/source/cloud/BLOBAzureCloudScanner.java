package com.augiq.external.source.cloud;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.elasticsearch.common.blobstore.BlobMetaData;
import org.elasticsearch.common.jackson.dataformat.yaml.snakeyaml.reader.StreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.services.autoscaling.model.Instance;
import com.augiq.external.source.bi.cognos.scanning.impl.CognosBIScanner;
import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.external.source.connection.message.constants.TestConnectionEnums;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.repository.connector.external.connectiondao.ConnectorListingDAO;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;
import com.augmentiq.maxiq.util.component.configuration.util.ExternalDataSourceService;
import com.augmentiq.maxiq.util.component.configuration.util.SampleRecordExtractor;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.google.gson.Gson;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.ResultSegment;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobInputStream;
import com.microsoft.azure.storage.blob.BlobProperties;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlobDirectory;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;
import com.microsoft.azure.storage.file.CloudFile;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;
import com.microsoft.azure.storage.file.ListFileItem;
import com.mysql.jdbc.Blob;

import javassist.expr.Instanceof;
import oracle.sql.BLOB;

public class BLOBAzureCloudScanner extends CloudScanner {

	@Autowired
	private ConnectorListingDAO cloudConnectorListingDAO;

	@Autowired
	private ConfigurationCreateUtil createUtil;

	private ConnectionConfig cloudConnectionConfig;

	private String subSourceType;

	private ApplicationContext context;

	public static final Logger logger = LoggerFactory.getLogger(BLOBAzureCloudScanner.class);

	// DataNode rootDataNode = new DataNode();

	public String testConnection(Map<String, Object> args) {
		Connection connection = null;
		ConnectionConfig connectionConfig = (ConnectionConfig) args.get("connectionConfig");
		this.subSourceType = args.get("subSourceType").toString();
		String message = TestConnectionEnums.SUCCESS.name();
		message = establishedConnection(connectionConfig);
		return message;
	}

	public DataNode scan(Map<String, Object> args) throws Exception {
		logger.info("scaning start......");
		ConnectionSources connectionSources = (ConnectionSources) args.get("ConnectionSources");
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		cloudConnectionConfig = getConnectionConfig(connectionSources);
		DataNode node = processBLOBAzureContainer();
		Gson gson = new Gson();
		System.out.println(gson.toJson(node));
		return node;
	}

	public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {
		Map<String, List<String>> existingDSNotifier = new HashMap<String, List<String>>();
		existingDSNotifier.put(TaskStatusEnums.SUCCESS.name(), new ArrayList<String>());
		DataNode filterNode = (DataNode) args.get("filterNode");
		DataNode publishDataNode = (DataNode) args.get("publishNode");
		Long dataSourceId = (Long) args.get("dataSourceId");
		ConnectionSources connectionSources = RDBMSScanner.objectParserHandler((Object) args.get("connectionSources"),
				ConnectionSources.class);
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);
		String connectorType = connectionSubSources.getSubConnectionType();
		connectionConfig.setFtpfilePath(publishDataNode.getFtpFileLocation());

		fetchDataSource(dataSourceId, connectionConfig, publishDataNode, filterNode, connectorType);
		return existingDSNotifier;
	}

	public String establishedConnection(ConnectionConfig connectionConfig) {

		CloudStorageAccount storageAccount = null;
		CloudBlobClient blobClient = null;
		CloudBlobContainer container = null;
		String message = TestConnectionEnums.SUCCESS.name();
		String accountName = connectionConfig.getAccountName();
		String accountKey = connectionConfig.getAccountKey();
		String defaultEndPointsProtocol = connectionConfig.getDefaultEndPointsProtocol();

		String storageConnectionString = "DefaultEndpointsProtocol=" + defaultEndPointsProtocol + ";" + "AccountName="
				+ accountName + ";" + "AccountKey=" + accountKey + ";";
		try {
			storageAccount = CloudStorageAccount.parse(storageConnectionString);
		} catch (InvalidKeyException e) {
			return TestConnectionEnums.FAIL.name();
		} catch (URISyntaxException e) {
			return message;
		}
		blobClient = storageAccount.createCloudBlobClient();
		try {
			blobClient.listContainersSegmented();
		} catch (StorageException e) {
			return TestConnectionEnums.FAIL.name();
		} finally {
			storageConnectionString = null;
			storageAccount = null;
			blobClient = null;
		}
		return message;
	}

	@Transactional
	private ConnectionConfig getConnectionConfig(ConnectionSources connectionSources) {
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		Long connectionId = connectionSubSources.getConnectionConfig().get(0).getConnectionId();
		return cloudConnectorListingDAO.getConnectionConfig(connectionId);
	}

	public DataNode processBLOBAzureContainer() throws Exception {
		CloudStorageAccount storageAccount = null;
		CloudBlobClient blobClient = null;
		CloudBlobContainer container = null;
		DataNode rootNode = new DataNode();
		rootNode.setLabel("AccountName");
		DataNode accountDataNode = new DataNode(cloudConnectionConfig.getAccountName(), false);
		String storageConnectionString = "DefaultEndpointsProtocol="
				+ cloudConnectionConfig.getDefaultEndPointsProtocol() + ";" + "AccountName="
				+ cloudConnectionConfig.getAccountName() + ";" + "AccountKey=" + cloudConnectionConfig.getAccountKey()
				+ ";";
		rootNode.getChildren().add(accountDataNode);
		try {
			storageAccount = CloudStorageAccount.parse(storageConnectionString);
		} catch (InvalidKeyException | URISyntaxException e) {
			e.printStackTrace();
		}
		blobClient = storageAccount.createCloudBlobClient();
		Iterable<CloudBlobContainer> blobContainer = blobClient.listContainers();

		for (CloudBlobContainer cloudBlobContainer : blobContainer) {
			DataNode containerDataNode = new DataNode(cloudBlobContainer.getName(), false);
			accountDataNode.getChildren().add(containerDataNode);
			System.out.println("CloudBlobContainer : - " + cloudBlobContainer.getName() + "---Starts------");
			Iterator<ListBlobItem> listBlobItemIterator = cloudBlobContainer.listBlobs().iterator();
			while (listBlobItemIterator.hasNext()) {
				Object object = listBlobItemIterator.next();
				processBLOBDirectoriesAndFiles(object, containerDataNode);
			}
			System.out.println("CloudBlobContainer : - " + cloudBlobContainer.getName() + "---Ends------");
		}
		return rootNode;
	}

	public void processBLOBDirectoriesAndFiles(Object objectAzureBlob, DataNode parentDataNode)
			throws StorageException, URISyntaxException {
		if (objectAzureBlob instanceof CloudBlockBlob) {
			CloudBlockBlob cloudBlockBlob = (CloudBlockBlob) objectAzureBlob;
			BlobProperties blobProperties = cloudBlockBlob.getProperties();
			if (blobProperties.getLength() != 0) {
				String fileName = parseBLOBAzureFileOrDirectoryName(cloudBlockBlob.getName());
				DataNode fileNode = createNode(fileName, cloudBlockBlob.getName(), false, true);
				parentDataNode.getChildren().add(fileNode);
			}
		} else if (objectAzureBlob instanceof CloudBlobDirectory) {
			CloudBlobDirectory cloudBlobDirectory = (CloudBlobDirectory) objectAzureBlob;
			String dirName = parseBLOBAzureFileOrDirectoryName(cloudBlobDirectory.getPrefix());
			DataNode newParentNode = createNode(dirName, null, false, false);
			parentDataNode.getChildren().add(newParentNode);
			System.out.println("CloudBlobDirectory 1 :- " + cloudBlobDirectory.getPrefix());
			Iterator<ListBlobItem> listBlobItemIterator = cloudBlobDirectory.listBlobs().iterator();
			while (listBlobItemIterator.hasNext()) {
				Object object = listBlobItemIterator.next();
				processBLOBDirectoriesAndFiles(object, newParentNode);
			}
		}
	}

	public String parseBLOBAzureFileOrDirectoryName(String absoluteName) {
		int _count = 0;
		String name = null;
		String[] splitAbsoluteName = absoluteName.split("/");
		for (String splitName : splitAbsoluteName) {
			if (_count == splitAbsoluteName.length - 1) {
				name = splitName;
			}
			_count++;
		}
		return name;
	}

	public DataNode createNode(String fileName, String filePath, Boolean isSelected, Boolean isDSNode) {
		DataNode dataNode = new DataNode(fileName, isSelected, isDSNode);
		dataNode.setFtpFileLocation(filePath);
		return dataNode;
	}

	public Collection<DataNode> filterNodesToPublish(Collection<DataNode> dataNodes) {

		Collection<DataNode> validNodes = new ArrayList<DataNode>();

		for (DataNode itrDataNode : dataNodes) {
			if (!itrDataNode.getChildren().isEmpty()) {
				Collection<DataNode> subList = filterNodesToPublish(itrDataNode.getChildren());
				validNodes.addAll(subList);
			} else {
				if (itrDataNode.getIsDsNode() && itrDataNode.getSelected())
					validNodes.add(itrDataNode);
			}
		}

		return validNodes;
	}

	public DataSource fetchDataSource(Long dataSourceId, ConnectionConfig config, DataNode dataNode,
			DataNode filterNode, String subsourceType) throws Exception {

		boolean isHeaderPresent = false;
		DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceId);
		FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();
		List<FieldMapping> existingFieldMappings = dataSource.getFieldMappings();
		dataSource.setDataSourceType(DataSourceType.BLOB_DATA_SOURCE);
		dataSource.setFileStoreObject(dataSource.getFileStoreObject());

		dataSource.setConfig(config);
		if (null == config) {
			config = new ConnectionConfig();
		}
		dataSource.setFileStoreObject(dataSource.getFileStoreObject());
		dataSource.setDataAtRestCompressionType(CompressionTypeEnum.SNAPPY.name());
		dataSource.setDataAtRestFileType(FileTypeEnum.PARQUET.name());
		dataSource.setConfig(config);

		if (fileDataIngesterDetails == null)
			fileDataIngesterDetails = new FileDataIngesterDetails();

		fileDataIngesterDetails.setDataSourceId(dataSourceId);
		fileDataIngesterDetails.setContainsHeader(isHeaderPresent + "");
		if (null != dataNode.getDelimiter()) {
			fileDataIngesterDetails.setDelimiter(dataNode.getDelimiter());
		} else {
			fileDataIngesterDetails.setDelimiter("$");
		}

		fileDataIngesterDetails.setOptionallyEnclosedInDoubleQuotes(dataNode.getOptionallyEnclosedInDoubleQuotes());
		fileDataIngesterDetails.setDataSourceType(DataSourceType.BLOB_DATA_SOURCE);
		Map<String, String> updatedValue = new HashMap<String, String>();
		updatedValue.put("dataSourceType", DataSourceType.BLOB_DATA_SOURCE.toString());

		DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(fileDataIngesterDetails.getDataSourceId(),
				updatedValue);
		dataSource.setFileDataIngesterDetails(fileDataIngesterDetails);
		List<FieldMapping> fieldMappings = null;
		List<String> fields = fetchSampleData(config, dataNode, filterNode, subsourceType);
		if (isHeaderPresent) {
			fieldMappings = ExternalDataSourceService.fetchFieldMapping(fields, 1L, dataSource, dataSourceId, null,
					isHeaderPresent);
		} else {
			fieldMappings = ExternalDataSourceService.fetchFieldMapping(fields, 0L, dataSource, dataSourceId, null,
					isHeaderPresent);
		}

		fieldMappings = SampleRecordExtractor.dataTypeGenerator(fieldMappings);
		dataSource.setFieldMappings(fieldMappings);
		DataRepositoryDao.insertDataSource(dataSource);

		return dataSource;
	}

	public List<String> fetchSampleData(ConnectionConfig config, DataNode dataNode, DataNode filterNode,
			String subsourcetype) throws Exception {

		List<String> sampleData = null;
		CloudStorageAccount storageAccount = null;
		CloudBlobClient blobClient = null;
		CloudBlobContainer container = null;
		CloudFileClient cloudFileClient = null;
		String storageConnectionString = "DefaultEndpointsProtocol="
				+ cloudConnectionConfig.getDefaultEndPointsProtocol() + ";" + "AccountName="
				+ cloudConnectionConfig.getAccountName() + ";" + "AccountKey=" + cloudConnectionConfig.getAccountKey()
				+ ";";
		storageAccount = CloudStorageAccount.parse(storageConnectionString);
		blobClient = storageAccount.createCloudBlobClient();
		// String containerName = getContainerName(filterNode.getChildren());
		sampleData = getSampleData(10, dataNode.getFtpFileLocation(), blobClient);
		return sampleData;
	}

	public List<String> getSampleData(int count, String name, CloudBlobClient blobClient) {
		ArrayList<String> sampleData = new ArrayList<String>();
		Iterable<CloudBlobContainer> blobContainer = blobClient.listContainers();
		for (CloudBlobContainer container : blobContainer) {
			CloudBlobContainer containerBlob = null;
			try {
				containerBlob = blobClient.getContainerReference(container.getName());
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (StorageException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CloudBlockBlob cloudBlockBlob = null;
			try {
				cloudBlockBlob = containerBlob.getBlockBlobReference(name);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (StorageException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BlobInputStream inputStream;
			try {
				inputStream = cloudBlockBlob.openInputStream();
			} catch (StorageException e) {
				continue;
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			while (count > 0) {
				try {
					line = reader.readLine();
					if (line != null) {
						sampleData.add(line);
						count--;
					} else {
						break;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			if (null != sampleData && sampleData.size() > 0) {
				break;
			}
		}

		return sampleData;
	}

	public String getFileType(String fileLocation, String storageConnectionString) throws Exception {
		CloudStorageAccount storageAccount = null;
		CloudBlobClient blobClient = null;
		CloudBlobContainer container = null;
		CloudFileClient cloudFileClient = null;
		try {
			storageAccount = CloudStorageAccount.parse(storageConnectionString);
		} catch (InvalidKeyException | URISyntaxException e) {
			e.printStackTrace();
		}

		blobClient = storageAccount.createCloudBlobClient();
		CloudBlobContainer cloudBlobContainer = blobClient.getContainerReference("otiscontainer");
		CloudBlockBlob cloudBlockBlob = cloudBlobContainer
				.getBlockBlobReference("CloseCallBy/MOSAIC_2319047/part-00000");
		int i = cloudBlockBlob.getName().lastIndexOf(".");
		System.out.println(cloudBlockBlob.getName().substring(i + 1));
		BlobInputStream inputStream = cloudBlockBlob.openInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String strLine;
		StringBuilder sb = new StringBuilder();
		while ((strLine = reader.readLine()) != null) {
			sb.append(strLine);
			System.out.println(strLine);
		}
		System.out.println(sb.toString());
		return null;
	}

	public static void main(String[] args) throws Exception {

		String storageConnectionString = "DefaultEndpointsProtocol=https;" + "AccountName=pocmosaic;"
				+ "AccountKey=CH5s2IUvtDpchX4BustEk6tgxfcsAjwTldWMPtlKXNIE4CeMMDFEDATSTJZiYi7wjPlW9bbzDJvgSMarF4i0SA==";
		String fileLocation = "asdasd/MOSAIC_2318871/part-r-00000-cd6509e6-ede9-4925-a59b-20c2e3e15af1.json";
		BLOBAzureCloudScanner blobAzureCloudScanner = new BLOBAzureCloudScanner();
		// DataNode node =
		// blobAzureCloudScanner.processBLOBAzureContainer(storageConnectionString);
		blobAzureCloudScanner.getFileType(fileLocation, storageConnectionString);

	}

}

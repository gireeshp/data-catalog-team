package com.augiq.external.source.cloud.amazons3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;

public class AmazonS3Connector extends ConnectorInstance implements Scanner, Runnable {

  Map<String, String> params_;
  private File staggingFile_;

  static final Logger logger = Logger.getLogger(AmazonS3Connector.class);

  @Override
  public void init(Request request) throws Exception {
    // TODO Auto-generated method stub
    params_ = request.getParams();
    this.request_ = request;
  }

  private void downloadUploadedFile(
      AmazonS3 s3, String bucketName, String staggingArea, String fileExtension)
      throws FileNotFoundException, IOException {
    // TODO Auto-generated method stub
    for (Bucket bucket : s3.listBuckets()) {
      ListObjectsRequest listObjectsRequest =
          new ListObjectsRequest()
              .withBucketName(bucketName)
              //.withBucketName(bucket.getName())
              .withPrefix(null);
      ObjectListing objectListing;

      System.out.println(
          "Starting Download to Local Storage Drive as you mention in properties file....");
      do {
        objectListing = s3.listObjects(listObjectsRequest);

        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {

          String keyName = objectSummary.getKey();

          GetObjectRequest request = new GetObjectRequest(bucketName, keyName);
          S3Object object = s3.getObject(request);
          S3ObjectInputStream objectContent = object.getObjectContent();

          String xmlDir = staggingArea + "/" + keyName;

          File xmlDirectory = new File(xmlDir);

          if (xmlDirectory.getName().endsWith(fileExtension)) {

            IOUtils.copy(objectContent, new FileOutputStream(staggingArea + "/" + keyName));
          } else {
            xmlDirectory.mkdir();
          }
        }
        listObjectsRequest.setMarker(objectListing.getNextMarker());
      } while (objectListing.isTruncated());
      System.out.println(
          "Completed Download to Local Storage Drive as you mention in properties file");
    }
  }

  private void displayAllBucketNamesWithFolders(
      AmazonS3 s3, String bucketName, String localFileWriteSavePath) throws IOException {
    // TODO Auto-generated method stub

    StringBuilder strbuilder = new StringBuilder();
    String staggingArea = this.request_.getStaggingArea();

    //	strbuilder.append("Listing bucket Names With Folder Content" + "\n");

    for (Bucket bucket : s3.listBuckets()) {

      File file = new File(staggingArea + "/AmazonS3BucketFolderContent.txt");
      // if file doesnt exists, then create it
      if (!file.exists()) {
        file.createNewFile();
      }

      FileWriter fw = new FileWriter(file.getAbsoluteFile());
      BufferedWriter bw = new BufferedWriter(fw);

      System.out.println("Started writing Folder Content inside file");
      System.out.println("Folders Inside" + " " + bucket.getName() + " " + "Bucket are:");
      strbuilder.append("Folders Inside" + " " + bucket.getName() + " " + "Bucket are:" + "\n");

      ListObjectsRequest listObjectsRequest =
          new ListObjectsRequest()
              .withBucketName(bucketName)
              //.withBucketName(bucket.getName())
              .withPrefix(null);
      ObjectListing objectListing;

      do {
        objectListing = s3.listObjects(listObjectsRequest);

        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {

          strbuilder.append(" - " + objectSummary.getKey() + "\n");
          System.out.println(" - " + objectSummary.getKey() + objectSummary.getLastModified());
          /*	Date dt = new Date();
          System.out.print(dt.toString()); */

        }
        listObjectsRequest.setMarker(objectListing.getNextMarker());
      } while (objectListing.isTruncated());

      bw.write(strbuilder.toString());
      bw.close();
      System.out.println("Completed writing Folder Content inside file");
    }
  }

  private void displayAllBuckets(AmazonS3 s3, String localFileWriteSavePath) throws IOException {
    // TODO Auto-generated method stub

    String staggingArea = this.request_.getStaggingArea();
    staggingFile_ = new File(staggingArea);

    logger.info("File Stored in Location path is " + staggingFile_);

    File file = new File(staggingArea + "/AmazonS3BucketNames.txt");
    // if file doesnt exists, then create it
    if (!file.exists()) {
      file.createNewFile();
    }
    StringBuilder strbuilder = new StringBuilder();
    FileWriter fw = new FileWriter(file.getAbsoluteFile());
    BufferedWriter bw = new BufferedWriter(fw);

    System.out.println("Started writing Bucket List to file");
    for (Bucket bucket : s3.listBuckets()) {
      strbuilder.append(bucket.getName() + "\n");
    }
    bw.write(strbuilder.toString());
    bw.close();
    System.out.println("Completed writing Bucket List to file");
  }

  @Override
  public void downloadData() throws Exception {
    // TODO Auto-generated method stub
    /*
     * The ProfileCredentialsProvider will return your [default]
     * credential profile by reading from the credentials file located at
     * (/root/.aws/credentials).
     */
    AWSCredentials credentials = null;

    String staggingArea = this.request_.getStaggingArea();

    //	String instanceUrl = params_.get("instanceUrl");
    String accesskeyid = params_.get("accesskeyid");
    String secretkey = params_.get("secretkey");
    String localFileWriteSavePath = params_.get("localFileWriteSavePath");
    String bucketName = params_.get("existingBucketName");
    //	String storagePath = params_.get("storagePath");
    String fileExtension = params_.get("fileExtension");

    //	logger.info("instanceUrl is " + instanceUrl);

    try {
      //   credentials = new ProfileCredentialsProvider("default").getCredentials();

      //1st Parameter in BasicAWSCredentials is Access Key ID and 2nd Parameter is Secret Access Key

      credentials = new BasicAWSCredentials(accesskeyid, secretkey);

    } catch (Exception e) {
      throw new AmazonClientException(
          "Cannot load the credentials from the credential profiles file. "
              + "Please make sure that your credentials file is at the correct "
              + "location (/root/.aws/credentials), and is in valid format.",
          e);
    }

    AmazonS3 s3 = new AmazonS3Client(credentials);
    //   Region usWest2 = Region.getRegion(Regions.US_WEST_2);
    //    s3.setRegion(usWest2);

    System.out.println("===========================================");
    System.out.println("Getting Started with Amazon S3");
    System.out.println("===========================================\n");

    try {

      displayAllBuckets(s3, localFileWriteSavePath);
      displayAllBucketNamesWithFolders(s3, bucketName, localFileWriteSavePath);
      downloadUploadedFile(s3, bucketName, staggingArea, fileExtension);

    } catch (AmazonServiceException ase) {
      System.out.println(
          "Caught an AmazonServiceException, which means your request made it "
              + "to Amazon S3, but was rejected with an error response for some reason.");
      System.out.println("Error Message:    " + ase.getMessage());
      System.out.println("HTTP Status Code: " + ase.getStatusCode());
      System.out.println("AWS Error Code:   " + ase.getErrorCode());
      System.out.println("Error Type:       " + ase.getErrorType());
      System.out.println("Request ID:       " + ase.getRequestId());
    } catch (AmazonClientException ace) {
      System.out.println(
          "Caught an AmazonClientException, which means the client encountered "
              + "a serious internal problem while trying to communicate with S3, "
              + "such as not being able to access the network.");
      System.out.println("Error Message: " + ace.getMessage());
    }
  }

/*  @Override
  public void uploadDataToHdfs() throws Exception {
    // TODO Auto-generated method stub
    String uploadLocation = params_.get("uploadLocation");
    FileUtils.uploadDataToHDFC(staggingFile_.getAbsolutePath(), uploadLocation);
  }
*/
  @Override
  public void test() throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public void downloadData(Request request) throws Exception {
    // TODO Auto-generated method stub

  }

@Override
public DataNode scan(Map<String, Object> args) throws Exception {
	DataNode dataNode = null;
	
	ConnectionSources connectionSources = (ConnectionSources)args.get("ConnectionSources");
	ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
	ConnectionConfig connectionConfig = (ConnectionConfig) connectionSubSources.getConnectionConfig().get(0);
	
	Map<String,String> awsConnDetails = new HashMap<String, String>();
	awsConnDetails.put("accesskeyid", connectionConfig.getAmazons3AccessId());
	awsConnDetails.put("secretkey", connectionConfig.getAmazons3SecretId());
	
	AmazonS3Services amazonS3Services = new AmazonS3Services();
	//List<S3BucketInfo> buckets = amazonS3Services.getBucketList(awsConnDetails);
	List<Bucket> buckets = amazonS3Services.getBucketList(awsConnDetails);
	
	DataNode rootNode = new DataNode("Root", false, "");
	
	Collection<DataNode> children = new ArrayList<DataNode>();
	
	Thread thread1 = new Thread();
	S3Scheduler s3scheduler = new S3Scheduler();
	s3scheduler.setAwsConnDetails(awsConnDetails);
	s3scheduler.setPrefix(null);
	s3scheduler.setDelimiter("/");

	if(connectionConfig.getS3BucketName() == null ) {
		for( Bucket bucket : buckets) {
			//System.out.println(" ++ "+bucket.getName());
			//if(bucket.getName().equalsIgnoreCase("mosaic-decouple")) {
			DataNode bucketNode = new DataNode(bucket.getName(),false, bucket.getName());
			Collection<DataNode> childItems =
					amazonS3Services.getChildItems(awsConnDetails, bucket.getName(), null,connectionConfig.getS3folderName());
			bucketNode.setChildren(childItems);
			children.add(bucketNode);
			//}
		}
	}
	else {
		DataNode bucketNode = new DataNode(connectionConfig.getS3BucketName(),false, connectionConfig.getS3BucketName());
		Collection<DataNode> childItems =
				amazonS3Services.getChildItems(awsConnDetails, connectionConfig.getS3BucketName(), null, connectionConfig.getS3folderName());
		bucketNode.setChildren(childItems);
		children.add(bucketNode);
	}

	rootNode.setChildren(children);
	
	System.out.println(rootNode.toString());
	return rootNode;
}

@Override
public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {

	// TODO Auto-generated method stub
	DataNode dataNode = new DataNode();
	ConnectionSources connectionSources = RDBMSScanner.objectParserHandler((Object)args.get("connectionSources"), ConnectionSources.class);
	ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
	ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);
	Map<String,String> awsConnDetails = new HashMap<String, String>();
	awsConnDetails.put("accesskeyid", connectionConfig.getAmazons3AccessId());
	awsConnDetails.put("secretkey", connectionConfig.getAmazons3SecretId());
	awsConnDetails.put("connectionName", connectionConfig.getConncetionName());
	AmazonS3Services amazonS3Services = new AmazonS3Services();
	AmazonS3 amazonS3 = amazonS3Services.getAmazonS3Conn(awsConnDetails);
	
	Long dsId = (Long) args.get("dataSourceId");
	DataNode publishDataNode = (DataNode) args.get("publishNode");
	
	
	
	ExternalDataSourceDetails externalDSDetails = new ExternalDataSourceDetails();
	externalDSDetails.setDataSourceId(dsId);
	//externalDSDetails.setAmazonS3(amazonS3);
	externalDSDetails.setDelimeter(publishDataNode.getDelimiter());
	externalDSDetails.setStoragePath(publishDataNode.getStoragePath());
	
	    DataSource finalDataSource =
	    		amazonS3Services.saveDataSourceDetails(publishDataNode,connectionConfig, dsId, externalDSDetails.getContainsHeader(), externalDSDetails);
		
	    return null;
	    
}

public AmazonS3 getAmazonS3Conn(Map<String, String> properties) {
	String accesskeyid = properties.get("accesskeyid");
	String secretkey = properties.get("secretkey");

	AWSCredentials credentials = null;
	credentials = new BasicAWSCredentials(accesskeyid, secretkey);
	
	AmazonS3 s3 = new AmazonS3Client(credentials);

	return s3;
}


@Override
public String testConnection(Map<String, Object> args) {

	String amazonOwner = null;
	ConnectionConfig connectionConfig = new ConnectionConfig();
	connectionConfig = (ConnectionConfig) args.get("connectionConfig");
	AmazonS3 s3 = getConnectionObject(connectionConfig);
	
	amazonOwner = s3.getS3AccountOwner().getDisplayName();

	String connectionStatus = (amazonOwner != null ? TaskStatusEnums.SUCCESS.name(): TaskStatusEnums.FAIL.name());
	//s3.shutdown();
	
	return connectionStatus;
}

public static AmazonS3 getConnectionObject(ConnectionConfig connectionConfig) {
	AWSCredentials credentials = null;
	credentials = new BasicAWSCredentials(connectionConfig.getAmazons3AccessId(), connectionConfig.getAmazons3SecretId());
	return new AmazonS3Client(credentials);
}

@Override
public void uploadDataToHdfs() throws Exception {
}

@Override
public void run() {
	// TODO Auto-generated method stub
	
}

@Override
public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
	// TODO Auto-generated method stub
	return null;
}

}

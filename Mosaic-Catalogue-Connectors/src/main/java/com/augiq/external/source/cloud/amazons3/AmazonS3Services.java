package com.augiq.external.source.cloud.amazons3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.entity.model.configuration.bean.S3BucketInfo;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;

/*import com.augmentiq.maxiq.core.configurationdao.DataRepositoryFetchWithChild;
import com.augmentiq.maxiq.core.configurationdao.DataSourceInstanceDao;
import com.augmentiq.maxiq.core.dao.configuration.exceoption.SystemException;
import com.augmentiq.maxiq.core.dao.mysql.MySqlOperations;
import com.augmentiq.maxiq.entity.model.configuration.Bean.DataSource;*/
//import com.augmentiq.maxiq.core.models.configuration.Bean.ExternalDataSourceDetails;
//import com.augmentiq.maxiq.core.models.configuration.Bean.FieldMapping;
//import com.augmentiq.maxiq.core.models.configuration.Bean.FileDataIngesterDetails;
//import com.augmentiq.maxiq.core.models.configuration.Bean.S3BucketInfo;
/*import com.augmentiq.maxiq.entity.model.configuration.Bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.Bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.entity.model.configuration.Bean.S3BucketInfo;
import com.augmentiq.maxiq.entity.model.configuration.Bean.ExternalDataSourceDetails;*/

/*import com.augmentiq.maxiq.core.models.connector.ConnectorConfig;
import com.augmentiq.maxiq.core.services.communicationdetails.CommunicationDetailsBSO;
import com.augmentiq.maxiq.core.services.configurationservices.ConfigurationCreateService;
import com.augmentiq.maxiq.core.services.configurationservices.DataSourceBso;
import com.augmentiq.maxiq.core.services.externaldsservice.ExternalDataSourceService;
import com.augmentiq.maxiq.core.workontopics.SampleRecordExtractor;
import com.augmentiq.maxiq.hbase.HbaseUtility;*/
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.util.component.configuration.util.ExternalDataSourceService;
import com.augmentiq.maxiq.util.component.configuration.util.SampleRecordExtractor;
/*import com.augmentiq.maxiq.CONSTATS.CHAR_CONSTANTS;
import com.augmentiq.maxiq.Cache.Cache;
import com.augmentiq.maxiq.Cache.CacheConstants;
import com.augmentiq.maxiq.Configuration.Enums.CompressionTypeEnum;
import com.augmentiq.maxiq.Configuration.Enums.DataSourceType;
import com.augmentiq.maxiq.Configuration.Enums.FileStoreObject;
import com.augmentiq.maxiq.Configuration.Enums.FileTypeEnum;
import com.augmentiq.maxiq.Configuration.Exceptions.MessageHandler.ExceptionsMessanger;
import com.augmentiq.maxiq.Configuration.Utility.ParamUtils;
import com.augmentiq.maxiq.canvas.workFlow.general.Constants;*/
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
/**
 * Changed for MAX-101 By Rushikesh Raut on 15-Sept-2016 Changed for MAX-502 By Balkrushna Patil on
 * 21-Sept-2016
 */
public class AmazonS3Services {

	public AmazonS3 getAmazonS3Conn(Map<String, String> properties) {
		String accesskeyid = properties.get("accesskeyid");
		String secretkey = properties.get("secretkey");

		AWSCredentials credentials = null;
		credentials = new BasicAWSCredentials(accesskeyid, secretkey);
		
		AmazonS3 s3 = new AmazonS3Client(credentials);

		return s3;
	}


	public static ExternalDataSourceDetails fetchExternalDataDetails(Long id) throws SystemException {
		Map<String, Object> query = new HashMap<String, Object>();
		query.put("id", id);
		return MySqlOperations.scanOneForQuery(ExternalDataSourceDetails.class, query);
	}

	/**
	 * @param dataSourceId
	 * @param amazonS3
	 * @param indicator
	 * @param eDetails
	 * @return
	 * @throws Exception
	 */
	public DataSource saveDataSourceDetails(
			DataNode publishNode,
			ConnectionConfig connectionConfig,
			Long dataSourceId,
			Boolean indicator,
			ExternalDataSourceDetails eDetails)
					throws Exception {

		//DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceId);
		DataSource dataSource = DataRepositoryDao.getDataSource(dataSourceId);
		
		FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();

		List<FieldMapping> existingFieldMappings = dataSource.getFieldMappings();

		dataSource.setDataSourceType(DataSourceType.EXTERNAL_DATA);
		dataSource.setExternalDataId(eDetails.getId());
		//dataSource.setFileStoreObject(FileStoreObject.S3);
		

		//config.setInputFileType(FileTypeEnum.TEXT.name());
		//config.setInputCompressionType(CompressionTypeEnum.UNCOMPRESSED.name());

	/*	List<ConnectorConfig> conn = new ConfigurationCreateService().getConn();
		if (conn != null) {
			for (ConnectorConfig connectorConfig : conn) {

				if (connectorConfig.getConncetionName().equals(amazonS3.getConnectionName())) {
					amazonS3.setAccesskeyid(connectorConfig.getAmazons3AccessId());
					amazonS3.setSecretkey(connectorConfig.getAmazons3SecretId());
				}
			}
		}*/
		
		
		dataSource.setConfig(connectionConfig);

		
		if (fileDataIngesterDetails == null) fileDataIngesterDetails = new FileDataIngesterDetails();

		// eDetails.setStoragePath(Cache.getProperty(CacheConstats.WEBSERVER_LOCAL_TEMP)+
		// System.currentTimeMillis());

		fileDataIngesterDetails.setContainsHeader(indicator + "");
		fileDataIngesterDetails.setDataSourceId(dataSourceId);
		fileDataIngesterDetails.setDelimiter(eDetails.getDelimeter());
		fileDataIngesterDetails.setDataSourceType(DataSourceType.EXTERNAL_DATA);
		//fileDataIngesterDetails.setServerFilePath(
		//		Cache.getProperty(CacheConstants.UPLOAD_PATH) + System.currentTimeMillis());
		fileDataIngesterDetails.setOptionallyEnclosedInDoubleQuotes(
				publishNode.getOptionallyEnclosedInDoubleQuotes());
				
		fileDataIngesterDetails.setOptionallyEnclosedInDoubleQuotes(false);
		eDetails.setStoragePath(fileDataIngesterDetails.getServerFilePath());

		if (dataSource.getExternalDataId() == null) {
			Long externalDsId =
					MySqlOperations.insert(
							eDetails, HbaseUtility.getIdAnnotationForClass(ExternalDataSourceDetails.class));
			dataSource.setExternalDataId(externalDsId);
		} else {
			/*ExternalDataSourceService.updateExternalDataSourceDetails(
					eDetails, dataSource.getExternalDataId());*/
		}

		//if (eDetails.getAmazonS3() != null) {
			dataSource.setExternalDataType(Constants.AMAZONS3);
		//} else {
		//	dataSource.setExternalDataType(
		//			Constants.GOOGLE_ANALYTICS.replace(CHAR_CONSTANTS.UNDERSCORE, CHAR_CONSTANTS.NO_SPACE));
		//}

		Map<String, String> updatedValue = new HashMap<String, String>();
		updatedValue.put("dataSourceType", fileDataIngesterDetails.getDataSourceType().toString());
		DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(
				fileDataIngesterDetails.getDataSourceId(), updatedValue);

		AmazonS3Services amazonS3Services = new AmazonS3Services();
		Map<String, String> param = new HashMap<String, String>();
		Map<String, String> properties = new HashMap<String, String>();

		properties.put("accesskeyid", connectionConfig.getAmazons3AccessId());
		properties.put("secretkey", connectionConfig.getAmazons3SecretId());

		/*param.put("key", amazonS3.getSelectedBucket().getName_());
		param.put("bucketName", amazonS3.getBucket().getName_());
		param.put("fileExtension", amazonS3.getFileExtension());
		param.put("storagePath", amazonS3.getStoragePath());
		param.put("downloadObjectType", amazonS3.getSelectedBucket().getType_());
		param.put("length", "11");*/
		
		param.put("key", publishNode.getLabel());
		param.put("bucketName", publishNode.getBucketName());
		param.put("fileExtension", null);
		param.put("storagePath", publishNode.getFtpFileLocation());
		param.put("downloadObjectType", null);
		param.put("length", "11");
		//param.put("fileName", publishNode.getLabel());
		
		dataSource.setFileDataIngesterDetails(fileDataIngesterDetails);

		List<String> fields = amazonS3Services.getFields(properties, param);
		List<FieldMapping> fieldMappings = null;
		if (indicator == null) indicator = false;

		if (indicator) {
			fieldMappings =
					//amazonS3Services.fetchFieldMapping(fields, 1L, dataSource, dataSourceId, null, indicator);
					ExternalDataSourceService.fetchFieldMapping(fields, 1L, dataSource, dataSourceId, null, indicator);
		} else {
			fieldMappings =
					//amazonS3Services.fetchFieldMapping(fields, 0L, dataSource, dataSourceId, null, indicator);
					ExternalDataSourceService.fetchFieldMapping(fields, 0L, dataSource, dataSourceId, null, indicator);
		}

		fieldMappings = SampleRecordExtractor.dataTypeGenerator(fieldMappings);

		dataSource.setFieldMappings(fieldMappings);

		dataSource.setFileStoreObject(dataSource.getFileStoreObject());
		dataSource.setDataAtRestCompressionType(CompressionTypeEnum.UNCOMPRESSED.name());
		dataSource.setDataAtRestFileType(FileTypeEnum.ANY.name());

		// TODO
		// TODO
		/*boolean isSchemaChanged =
				CommunicationDetailsBSO.verifyCurrentDataSourceFieldMappingWithExisting(
						fieldMappings, existingFieldMappings);*/

		//logger.info("Is field Mapping schema has changes :" + isSchemaChanged);

	/*	if (isSchemaChanged) {

			String uuid =
					CommunicationDetailsBSO.saveCommunicationDetails(
							dataSource.getFileDataIngesterDetails(),
							dataSource.getFieldMappings(),
							Constants.FIELDMAPPING_WARNING);
			logger.info(ParamUtils.getString(Constants.FIELDMAPPING_WARNING + "$$" + uuid));
			ExceptionsMessanger.throwException(
					new SystemException(), "ERR_134", ParamUtils.getString(uuid));
		}*/

		DataRepositoryDao.insertDataSource(dataSource);

		return dataSource;
	}


	public List<Bucket> getBucketList(Map<String, String> properties) {

		AmazonS3 s3 = getAmazonS3Conn(properties);
		List<Bucket> listBuckets = s3.listBuckets();

		return listBuckets;
	}

	public Collection<DataNode> getChildItems(
			Map<String, String> properties, String bucketName, String prefix, String searchFolder) {

		Collection<DataNode> dataNodesUnderBucket = new ArrayList<DataNode>();
		
		String delimiter = "/";
		if (prefix != null && !prefix.endsWith(delimiter)) {
			prefix += delimiter;
		}

		List<S3BucketInfo> bucketInfos = new ArrayList<S3BucketInfo>();

		AmazonS3 s3 = getAmazonS3Conn(properties);

		ListObjectsRequest listObjectsRequest =
				new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix).withDelimiter(delimiter);

		
		ObjectListing objectListing = null;

		do {
			try {
				objectListing = s3.listObjects(listObjectsRequest);
			}
			catch(Exception e) {
				System.out.println(e);
				break;
			}

			List<String> folders = objectListing.getCommonPrefixes();
			
			if(null == searchFolder) {
				for(String folder : folders) {
					//System.out.println("\t processing --> "+ folder);
						// create DataNode for each folder in a Bucket - isDataNode will be false for this node
						String[] paths = folder.split(delimiter);
						int pos = folder.split(delimiter).length -1;
						String folderName = paths[pos];
						DataNode folderNode = new DataNode(folderName,false,bucketName);
						folderNode.setFtpFileLocation(prefix);
						Collection<DataNode> subChilds = getChildItems(properties, bucketName, folder, searchFolder);
						folderNode.setChildren(subChilds);
						dataNodesUnderBucket.add(folderNode);
				}
			}
			else {
				for(String folder : folders) {
					if(folder.equals(searchFolder+"/")) {
						searchFolder = null; 
						//System.out.println("\t processing --> "+ folder);
						// create DataNode for each folder in a Bucket - isDataNode will be false for this node
						String[] paths = folder.split(delimiter);
						int pos = folder.split(delimiter).length -1;
						String folderName = paths[pos];
						DataNode folderNode = new DataNode(folderName,false,bucketName);
						folderNode.setFtpFileLocation(prefix);
						Collection<DataNode> subChilds = getChildItems(properties, bucketName, folder, searchFolder);
						folderNode.setChildren(subChilds);
						dataNodesUnderBucket.add(folderNode);
					}
				}
			}

			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {

				if (objectSummary.getKey().equalsIgnoreCase(prefix)) continue;

				if (isValidEntry(objectSummary.getKey(), prefix)) {

					ObjectMetadata objectMetadata = s3.getObjectMetadata(bucketName, objectSummary.getKey());

					String objectType = objectMetadata.getContentType();

					S3BucketInfo s3BucketInfo = new S3BucketInfo(objectSummary.getKey());

					s3BucketInfo.setLastModified_(objectSummary.getLastModified().toString());
					s3BucketInfo.setStorage_(objectSummary.getStorageClass());
					s3BucketInfo.setDisplayName_(getFileName(objectSummary.getKey()));
					if ("binary/octet-stream".equalsIgnoreCase(objectType)) {
						/*s3BucketInfo.setType_("FOLDER");
						
						// call same above method
						String newPrefix = prefix.concat(s3BucketInfo.getDisplayName_());
						List<S3BucketInfo> subChilds = getChildItems(properties,bucketName,newPrefix);
						for(S3BucketInfo bucket: subChilds) {
							System.out.println(" FFILE ---> "+ bucket.getDisplayName_());
						}*/
						
					} else {
						s3BucketInfo.setType_("FILE");
						//System.out.println(" dsNode ---> "+ s3BucketInfo.getDisplayName_());
						
						DataNode fileNode = new DataNode(s3BucketInfo.getDisplayName_(),true, bucketName);
						fileNode.setFtpFileLocation(prefix);
						dataNodesUnderBucket.add(fileNode);
					}

					bucketInfos.add(s3BucketInfo);
				}
			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());

		return dataNodesUnderBucket;
	}


	public String donwloadData(Map<String, String> connProperties, Map<String, String> param)
			throws FileNotFoundException, IOException {
		AmazonS3 s3 = getAmazonS3Conn(connProperties);

		String bucketName = param.get("bucketName");
		String prefix = param.get("key");
		String fileExtension = param.get("fileExtension");
		String localStorage = param.get("storagePath");
		String downloadObjectType = param.get("downloadObjectType");

		String delimiter = "/";
		if (prefix != null
				&& !prefix.endsWith(delimiter)
				&& downloadObjectType.equalsIgnoreCase("FOLDER")) {
			prefix += delimiter;
		}

		ListObjectsRequest listObjectsRequest =
				new ListObjectsRequest()
				.withBucketName(bucketName)
				// .withBucketName(bucket.getName())
				.withPrefix(prefix);

		ObjectListing objectListing;

		do {
			objectListing = s3.listObjects(listObjectsRequest);

			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
				String keyName = objectSummary.getKey();
				if ("FILE".equals(downloadObjectType)) {
					if (!prefix.equalsIgnoreCase(keyName)) {
						continue;
					} else {
						downloadFile(s3, bucketName, keyName, localStorage, getFileName(keyName));
						break;
					}
				}
				downloadFile(s3, bucketName, keyName, localStorage, keyName);
			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());

		return null;
	}

	private boolean downloadFile(
			AmazonS3 s3, String bucketName, String keyName, String localStorage, String displayName)
					throws FileNotFoundException, IOException {
		System.out.println("Download start ..." + keyName);
		GetObjectRequest request = new GetObjectRequest(bucketName, keyName);
		S3Object object = s3.getObject(request);
		String objectType = object.getObjectMetadata().getContentType();

		if ("binary/octet-stream".equalsIgnoreCase(objectType)) {
			File xmlDirectory = new File(localStorage + keyName);
			xmlDirectory.mkdir();
			return true;
		}

		S3ObjectInputStream objectContent = object.getObjectContent();
		IOUtils.copy(objectContent, new FileOutputStream(localStorage + displayName));

		System.out.println("Download end ..." + keyName);
		return false;
	}

	private String getFileName(String path) {
		path = StringUtils.removeEnd(path, "/");
		String[] chunks = StringUtils.split(path, "/");
		if (chunks != null) {
			return chunks[chunks.length - 1];
		}

		return null;
	}

	private boolean isValidEntry(String key, String prefix) {
		key = StringUtils.remove(key, prefix);

		if ((StringUtils.countMatches(key, "/") <= 1 && StringUtils.countMatches(key, ".") == 0)
				|| StringUtils.countMatches(key, "/") == 0 && StringUtils.countMatches(key, ".") > 0)
			return true;

		return false;
	}

	public List<String> getFields(Map<String, String> connProperties, Map<String, String> param)
			throws IOException, SystemException {
		AmazonS3 s3 = getAmazonS3Conn(connProperties);

		String bucketName = param.get("bucketName");
		String fName = param.get("key");
		String fileExtension = param.get("fileExtension");
		String localStorage = param.get("storagePath");
		String downloadObjectType = param.get("downloadObjectType");
		Long length = Long.parseLong(param.get("length"));
		String fileName = param.get("fileName");

		String delimiter = "/";
		/*if (prefix != null
				&& !prefix.endsWith(delimiter)
				&& downloadObjectType.equalsIgnoreCase("FOLDER")) {
			prefix += delimiter;
		}*/

		ListObjectsRequest listObjectsRequest =
				new ListObjectsRequest()
				.withBucketName(bucketName)
				// .withBucketName(bucket.getName())
				.withPrefix(localStorage).withDelimiter(delimiter);

		ObjectListing objectListing = null;

		do {
			objectListing = s3.listObjects(listObjectsRequest);


			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {

				if (objectSummary.getKey().equalsIgnoreCase(localStorage+"/"+fName)) continue;

				String keyName = objectSummary.getKey();

				if(keyName.equalsIgnoreCase(fName)) {
					ObjectMetadata objectMetadata = s3.getObjectMetadata(bucketName, objectSummary.getKey());

					String objectType = objectMetadata.getContentType();
					if (!"binary/octet-stream".equalsIgnoreCase(objectType)) {
						List<String> topData = getTopData(s3, bucketName, keyName, length);
						return topData;
					}
				}
			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());

		return null;
	}

	/**
	 * @param data
	 * @param headerAt
	 * @param dataSource
	 * @param dsId
	 * @param headerTemplate
	 * @param indicator
	 * @return List of Field Mappings
	 * @throws SystemException
	 */
	public List<FieldMapping> fetchFieldMapping(
			List<String> data,
			Long headerAt,
			DataSource dataSource,
			Long dsId,
			String headerTemplate,
			Boolean indicator)
					throws SystemException {
		String headerRecordFields = "";

		List<String> sampleRecordsList = new ArrayList<String>();

		Long count = headerAt;
		while (headerAt + 10L > count && data.size() > count) {
			sampleRecordsList.add(data.get(Integer.parseInt(count + "")));
			count++;
		}

		if (headerAt == 1L) headerRecordFields = data.get(Integer.parseInt((headerAt - 1L) + ""));
		else {
			int index = 0;
			int size = sampleRecordsList.size();
			while (StringUtils.isBlank(headerRecordFields) && index < size) {
				headerRecordFields = StringUtils.upperCase(sampleRecordsList.get(index));
				index++;
			}
		}

		Map<Integer, FieldMapping> fieldMapppingMap = null;
	/*	if (dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
			fieldMapppingMap =
					SampleRecordExtractor.generateFieldMappingFromHeader(
							SampleRecordExtractor.getTokenisedStringArrayFromStringUsingDelimiter(
									headerRecordFields, dataSource.getFileDataIngesterDetails().getDelimiter()),
							headerTemplate,
							dsId,
							!indicator,
							dataSource);
		} else {
			fieldMapppingMap =
					SampleRecordExtractor.generateFieldMappingFromHeader(
							StringUtils.splitPreserveAllTokens(
									headerRecordFields, dataSource.getFileDataIngesterDetails().getDelimiter()),
							headerTemplate,
							dsId,
							!indicator,
							dataSource);
		}

		if (sampleRecordsList.size() > 0) {

			int counter = 1;

			for (String sampleRecord : sampleRecordsList) {

				String[] fieldValues = null;

				if (null == dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()
						|| false
						== dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
					sampleRecord = sampleRecord.replaceAll("\"", "");
				}

				int position = 0;

				if (dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
					fieldValues =
							SampleRecordExtractor.getTokenisedStringArrayFromStringUsingDelimiter(
									sampleRecord, dataSource.getFileDataIngesterDetails().getDelimiter());

				} else {
					fieldValues =
							StringUtils.splitByWholeSeparatorPreserveAllTokens(
									sampleRecord, dataSource.getFileDataIngesterDetails().getDelimiter());
				}

				for (String fieldValue : fieldValues) {
					if (fieldMapppingMap.containsKey(position)) {

						FieldMapping fieldMapping = fieldMapppingMap.get(position);

						if (counter == 1) fieldMapping.setSampleRecord1(fieldValue);
						else if (counter == 2) fieldMapping.setSampleRecord2(fieldValue);
						else if (counter == 3) fieldMapping.setSampleRecord3(fieldValue);
						else if (counter == 4) fieldMapping.setSampleRecord4(fieldValue);
						else if (counter == 5) fieldMapping.setSampleRecord5(fieldValue);
						else if (counter == 6) fieldMapping.setSampleRecord6(fieldValue);
						else if (counter == 7) fieldMapping.setSampleRecord7(fieldValue);
						else if (counter == 8) fieldMapping.setSampleRecord8(fieldValue);
						else if (counter == 9) fieldMapping.setSampleRecord9(fieldValue);
						else if (counter == 10) fieldMapping.setSampleRecord10(fieldValue);
					}
					position++;
				}

				counter++;
			}

			List<FieldMapping> fieldMappings = new ArrayList<>();

			for (Entry<Integer, FieldMapping> entry : fieldMapppingMap.entrySet()) {

				String string = SampleRecordExtractor.outPutFieldValidation(entry.getValue());
				if (string != null) {
					entry.getValue().setFieldName(entry.getValue().getFieldName() + "_1");
				}
				fieldMappings.add(entry.getValue());
			}

			System.out.println(fieldMappings);

			return fieldMappings;
		}*/
		
		List<FieldMapping> fieldMappings = new ArrayList<>();
		return fieldMappings;
		
		//return null;
	}

	private List<String> getTopData(AmazonS3 s3, String bucketName, String key, long length)
			throws IOException {

		List<String> lines = new ArrayList<String>();

		GetObjectRequest request = new GetObjectRequest(bucketName, key);
		S3Object object = s3.getObject(request);

		S3Object s3object = s3.getObject(new GetObjectRequest(bucketName, key));
		System.out.println(s3object.getObjectMetadata().getContentType());
		System.out.println(s3object.getObjectMetadata().getContentLength());
		BufferedReader reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
		String line;
		int i = 0;
		while ((line = reader.readLine()) != null) {
			if (i++ < length) lines.add(line);
			else break;
		}

		return lines;
	}

	/*  public static void main(String args[]) {
	  Map<String,String> cred = new HashMap<String,String>();
	  cred.put("accesskeyid", "AKIAJXUYEGGD2ZIUECIA");
	  cred.put("secretkey","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");
	  AmazonS3Services ams = new AmazonS3Services();
	  ams.showBuckets(cred,"testbucket8080");
  }*/

	public void  showBuckets(Map<String,String> args, String bucketName) {

		//AmazonS3 s3client = getAmazonS3Conn(args);

		/*  ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName).withPrefix("").withDelimiter("/");
      ListObjectsV2Result listing = s3client.listObjectsV2(req);
      for (String commonPrefix : listing.getCommonPrefixes()) {
              System.out.println(commonPrefix);
      }
      for (S3ObjectSummary summary: listing.getObjectSummaries()) {
          System.out.println(summary.getKey());
      }*/

		String prefix = null;

		String delimiter = "/";
		if (prefix != null && !prefix.endsWith(delimiter)) {
			prefix += delimiter;
		}

		List<S3BucketInfo> bucketInfos = new ArrayList<S3BucketInfo>();

		AmazonS3 s3 = getAmazonS3Conn(args);

		ListObjectsRequest listObjectsRequest =
				new ListObjectsRequest().withBucketName(bucketName).withPrefix(prefix);

		ObjectListing objectListing;

		do {
			objectListing = s3.listObjects(listObjectsRequest);

			for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {

				if (objectSummary.getKey().equalsIgnoreCase(prefix)) continue;

				if (isValidEntry(objectSummary.getKey(), prefix)) {

					ObjectMetadata objectMetadata = s3.getObjectMetadata(bucketName, objectSummary.getKey());

					String objectType = objectMetadata.getContentType();

					S3BucketInfo s3BucketInfo = new S3BucketInfo(objectSummary.getKey());

					if ("binary/octet-stream".equalsIgnoreCase(objectType)) {
						s3BucketInfo.setType_("FOLDER");
					} else {
						s3BucketInfo.setType_("FILE");
					}
					s3BucketInfo.setLastModified_(objectSummary.getLastModified().toString());
					s3BucketInfo.setStorage_(objectSummary.getStorageClass());
					s3BucketInfo.setDisplayName_(getFileName(objectSummary.getKey()));
					bucketInfos.add(s3BucketInfo);
				}
			}
			listObjectsRequest.setMarker(objectListing.getNextMarker());
		} while (objectListing.isTruncated());

	}
}


package com.augiq.external.source.cloud.amazons3;

public abstract class ConnectorInstance {

  protected Request request_;

  public abstract void init(Request request) throws Exception;

  public abstract void downloadData() throws Exception;

  public abstract void uploadDataToHdfs() throws Exception;

  public abstract void test() throws Exception;

  public abstract void downloadData(Request request) throws Exception;
}

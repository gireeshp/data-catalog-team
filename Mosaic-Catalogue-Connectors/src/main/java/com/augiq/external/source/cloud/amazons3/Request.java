package com.augiq.external.source.cloud.amazons3;
import java.util.Map;

public class Request {

  private String type_;
  private String serviceRequest_;
  private String staggingArea_;
  private Map<String, String> params_;

  public String getStaggingArea() {
    return staggingArea_;
  }

  public void setStaggingArea(String staggingArea) {
    staggingArea_ = staggingArea;
  }

  public String getType() {
    return type_;
  }

  public void setType(String type) {
    type_ = type;
  }

  public String getServiceRequest() {
    return serviceRequest_;
  }

  public void setServiceRequest(String serviceRequest) {
    serviceRequest_ = serviceRequest;
  }

  public Map<String, String> getParams() {
    return params_;
  }

  public void setParams(Map<String, String> params) {
    params_ = params;
  }
}


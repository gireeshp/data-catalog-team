package com.augiq.external.source.cloud.hdfs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONArray;
import org.json.JSONObject;

import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.util.component.configuration.util.ExternalDataSourceService;
import com.augmentiq.maxiq.util.component.configuration.util.SampleRecordExtractor;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

public class HdfsScanner implements Scanner {

	@Override
	public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {

		Map<String, List<String>> existingDSNotifier = new HashMap<String, List<String>>();
		existingDSNotifier.put(TaskStatusEnums.SUCCESS.name(), new ArrayList<String>());

		DataNode publishDataNode = (DataNode) args.get("publishNode");
		Long dataSourceId = (Long) args.get("dataSourceId");
		ConnectionSources connectionSources = RDBMSScanner.objectParserHandler((Object)args.get("connectionSources"), ConnectionSources.class);
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);

		boolean isHeaderPresent = false;
		
		DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceId);

		FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();

		List<FieldMapping> existingFieldMappings = dataSource.getFieldMappings();

		dataSource.setDataSourceType(DataSourceType.HDFS);

		if (null == connectionConfig) {
			connectionConfig = new ConnectionConfig();
		}

		dataSource.setFileStoreObject(dataSource.getFileStoreObject());
		dataSource.setDataAtRestCompressionType(CompressionTypeEnum.SNAPPY.name());
		dataSource.setDataAtRestFileType(FileTypeEnum.PARQUET.name());
		dataSource.setConfig(connectionConfig);

		if (fileDataIngesterDetails == null) fileDataIngesterDetails = new FileDataIngesterDetails();

		fileDataIngesterDetails.setDataSourceId(dataSourceId);
		fileDataIngesterDetails.setContainsHeader(isHeaderPresent + "");
		fileDataIngesterDetails.setDelimiter(publishDataNode.getDelimiter());
		fileDataIngesterDetails.setOptionallyEnclosedInDoubleQuotes(publishDataNode.getOptionallyEnclosedInDoubleQuotes());
		fileDataIngesterDetails.setDataSourceType(DataSourceType.EXTERNAL_DATA);
		//	fileDataIngesterDetails.setServerFilePath(
		//	Cache.getProperty(CacheConstants.UPLOAD_PATH) + System.currentTimeMillis());

		Map<String, String> updatedValue = new HashMap<String, String>();
		updatedValue.put("dataSourceType", DataSourceType.EXTERNAL_DATA.toString());

		DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(
				fileDataIngesterDetails.getDataSourceId(), updatedValue);

		dataSource.setFileDataIngesterDetails(fileDataIngesterDetails);

		List<FieldMapping> fieldMappings = null;
		List<String> fields = fetchSampleData(connectionConfig, publishDataNode);

		if (isHeaderPresent) {
			fieldMappings =
					ExternalDataSourceService.fetchFieldMapping(
							fields, 1L, dataSource, dataSourceId, null, isHeaderPresent);
		} else {
			fieldMappings =
					ExternalDataSourceService.fetchFieldMapping(
							fields, 0L, dataSource, dataSourceId, null, isHeaderPresent);
		}

		fieldMappings = SampleRecordExtractor.dataTypeGenerator(fieldMappings);
		dataSource.setFieldMappings(fieldMappings);

		DataRepositoryDao.insertDataSource(dataSource);

		
		return null;
	}

	public List<String> fetchSampleData(ConnectionConfig connectionConfig, DataNode dataNode) throws Exception {
		
		List<String> sampleData = new ArrayList<String>();
		int sampleDataCount = 10;  // dataNode.getSampleCount();  to be used later
		
		URL url;
		try {
			
			String webAPIString = getWebhdfsUrl(connectionConfig, dataNode.getFtpFileLocation(), "?op=OPEN");
			url = new URL(webAPIString);
			//url = new URL("http://172.20.99.20:50070/webhdfs/v1/user/10644726/Bike/TVS_SALES.txt?op=OPEN");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setDoInput(true);

			InputStream in = con.getInputStream();
			
			StringBuffer line = new StringBuffer();
			int ch;

			while((ch = in.read())!= -1) {
				char data = (char)ch;
				
				if(data != '\n') {   // @TODO - Verify endofLine in generic way
					line.append(data);

					// additional check 
					if(line.length() > 10000) {
						break;
					}
					
				}
				else {
					sampleData.add(line.toString());
					sampleDataCount--;
					line.setLength(0);
					
					if(sampleDataCount <= 0) 
						break;
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sampleData;
	}
	
	public static void main(String args[]) {
		
	}

	@Override
	public DataNode scan(Map<String, Object> args) throws Exception {

		DataNode dataNode = null;

		ConnectionSources connectionSources = (ConnectionSources)args.get("ConnectionSources");
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = (ConnectionConfig) connectionSubSources.getConnectionConfig().get(0);

		String connectorType = connectionSubSources.getSubConnectionType();
		String url = "";

		if(connectionConfig.getFtpfilePath() != null) {
			url = getWebhdfsUrl(connectionConfig, connectionConfig.getFtpfilePath() , "?op=LISTSTATUS");
			dataNode = new DataNode(connectionConfig.getFtpfilePath(),false, false);
			dataNode.setFtpFileLocation(connectionConfig.getFtpfilePath());
		}
		else {
			url = getWebhdfsUrl(connectionConfig, connectionConfig.getHdfsRootDirectory() , "?op=LISTSTATUS");
			dataNode = new DataNode(connectionConfig.getHdfsRootDirectory(),false, false);
			dataNode.setFtpFileLocation(connectionConfig.getHdfsRootDirectory());
		}

		createDataNodeForHDFS(dataNode, connectionConfig);

		return dataNode;
	}

	public void createDataNodeForHDFS(DataNode ParentNode, ConnectionConfig connectionConfig) {

		String fileSeperator = "/";

		String url = getWebhdfsUrl(connectionConfig, ParentNode.getFtpFileLocation() , "?op=LISTSTATUS");
		String responseBody = callingWebHdfsAPI(url);
		JSONObject jsonResponse = new JSONObject(responseBody);
		JSONObject fileStatuses = jsonResponse.getJSONObject("FileStatuses");
		JSONArray arr = fileStatuses.getJSONArray("FileStatus");
		for(int i=0; i < arr.length() ; i++)  {
			JSONObject obj = (JSONObject) arr.get(i);

			String name =  obj.getString("pathSuffix");
			String type = obj.getString("type");

			if(type.equals("DIRECTORY")) {
				DataNode newParent = new DataNode(name,false,false);
				newParent.setFtpFileLocation(ParentNode.getFtpFileLocation()+fileSeperator+name);
				createDataNodeForHDFS(newParent,connectionConfig);
				ParentNode.getChildren().add(newParent);
			}
			else {
				DataNode fileNode = new DataNode(name,false,true);
				fileNode.setFtpFileLocation(ParentNode.getFtpFileLocation()+fileSeperator+name);
				ParentNode.getChildren().add(fileNode);
			}
		}
	}

	public String callingWebHdfsAPI(String url) {
		String responseBody = null;
		HttpClient client = new HttpClient();
		GetMethod pm = new GetMethod(url);
		try {
			int response = client.executeMethod(pm);

			if(!(response >= 200 && response < 300)) {
				System.out.println("curl -i "+url);
				return null;
			}
			responseBody = pm.getResponseBodyAsString();
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
		return responseBody;
	}

	@Override
	public String testConnection(Map<String, Object> args) {

		String connectionSuccessful = TaskStatusEnums.FAIL.name();
		ConnectionConfig connectionConfig = (ConnectionConfig) args.get("connectionConfig");
		String url = getWebhdfsUrl(connectionConfig, "" , "?op=GETHOMEDIRECTORY");
		HttpClient client = new HttpClient();
		GetMethod pm = new GetMethod(url);

		try {
			int response = client.executeMethod(pm);
			if(response == 200) {
				connectionSuccessful = TaskStatusEnums.SUCCESS.name();
			}
			String responseBody = pm.getResponseBodyAsString();
			JSONObject jsonResponse = new JSONObject(responseBody);
			String homeDirectory = (String) jsonResponse.get("Path");

			if(homeDirectory.contains("dr.who")) {
				homeDirectory = homeDirectory.substring(0, homeDirectory.lastIndexOf("/")+1) +
						connectionConfig.getDbUserName();
			}
			connectionConfig.setHdfsRootDirectory(homeDirectory);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  

		return connectionSuccessful;
	}

	public String getWebhdfsUrl(ConnectionConfig connectionConfig, String currDirectory, String operation) {
		String url = "http://" + connectionConfig.getIpAddress() + ":" + connectionConfig.getPort() + "/webhdfs/v1" + currDirectory + operation;
		return url;
	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}

package com.augiq.external.source.ftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;

//import com.augmentiq.maxiq.Configuration.Utility.ParamUtils;

public class FTPDataSource {

 // private static final Logger logger = LoggerFactory.getLogger(FTPDataSource.class);

  public FileTransfer fileTransfer = new FileTransfer();
  public FTPClient ftpClient;
  public String directoryPath;
  public List<String> filesList;
  public FTPFile[] filesListFTPFileObject;
  public String pathSaperator;

  public FTPDataSource(FileTransfer fileTransfer) {
    this.fileTransfer = fileTransfer;
  }

  public boolean getConnection() throws Exception {
   // logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getConnection()");
    try {
      ftpClient = new FTPClient();
      ftpClient.connect(fileTransfer.getHost(), fileTransfer.getPort());
      int replyCode = ftpClient.getReplyCode();
      if (!FTPReply.isPositiveCompletion(replyCode)) {

        ftpClient.disconnect();
     //   logger.warn(LoggerConstants.LOG_MAXIQWEB + "FTP server refused connection.");
     //   logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getConnection()");
        throw new SystemException("FTP server refused connection!.");
      }
      ftpClient.login(fileTransfer.getUser(), fileTransfer.getPassword());
      replyCode = ftpClient.getReplyCode();
      if (!FTPReply.isPositiveCompletion(replyCode)) {
        ftpClient.disconnect();
       // logger.warn(LoggerConstants.LOG_MAXIQWEB + "FTP server refused connection.");
       // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getConnection()");
        throw new SystemException("FTP server refused connection.");
      }

      // use local passive mode to pass firewall
      ftpClient.enterLocalPassiveMode();
     // logger.debug(LoggerConstants.LOG_MAXIQWEB + " Connected to ftpClient");

    } catch (Exception e) {
      e.printStackTrace();
      exit();
      //logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      //logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getConnection()");
      throw new Exception(e.getMessage());
    }

   // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getConnection()");
    return true;
  }

  public boolean getTestConnection() throws Exception {

   // logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getTestConnection()");
    try {
      ftpClient = new FTPClient();
      ftpClient.connect(fileTransfer.getHost(), fileTransfer.getPort());
      int replyCode = ftpClient.getReplyCode();
      if (!FTPReply.isPositiveCompletion(replyCode)) {
        throw new SystemException("FTP server refused connection!.");
      }
      ftpClient.login(fileTransfer.getUser(), fileTransfer.getPassword());
      replyCode = ftpClient.getReplyCode();
      if (!FTPReply.isPositiveCompletion(replyCode)) {
        throw new SystemException("FTP server refused connection.");
      }

      ftpClient.logout();
      ftpClient.disconnect();
    //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " Connected to ftpClient");

    } catch (IOException e) {
      e.printStackTrace();
     // logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
     // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getTestConnection()");
      throw new IOException(e.getMessage());
    }

   // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getConnection()");
    return true;
  }

 /* public boolean downloadData() throws Exception {
   // logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> downloadData()");
    try {
      this.getConnection();
      this.setDirPathAndFileName();

      if (!ftpClient.changeWorkingDirectory(this.directoryPath)) {
        exit();
     //   logger.warn(LoggerConstants.LOG_MAXIQWEB + "Invalid Remote file path");
     //   logger.debug(LoggerConstants.LOG_MAXIQWEB + " << downloadData()");
        throw new SystemException("Invalid Remote file path");
      }

      FTPFile[] files = filesListFTPFileObject;

      for (FTPFile file : files) {
        if (file.isDirectory()) {
       //   logger.debug(LoggerConstants.LOG_MAXIQWEB + "Skipping Directory " + file.getName());
          continue;
        }
        FTPUtil.downloadSingleFile(
            ftpClient,
            this.directoryPath + "/" + file.getName(),
            fileTransfer.getLocalFilePath() + "/" + file.getName());
      }
    } catch (Exception e) {
      e.printStackTrace();
      exit();
    //  logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
    //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " << downloadData()");
      throw new Exception(e.getMessage());
    }
    exit();
  //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> downloadData()");
    return true;
  }*/

  public boolean setDirPathAndFileName() throws IOException, SystemException {

	    this.filesList = new ArrayList<String>();
	    int index = StringUtils.lastIndexOf(fileTransfer.getRemoteFilePath(), "/");
	    if (-1 == index) {
	      this.pathSaperator = "\\";
	    } else {
	      this.pathSaperator = "/";
	    }

	    // if(!fileTransfer.getRemoteFilePath().substring(0,1).equals("/")){
	    // fileTransfer.setRemoteFilePath( "/" +
	    // fileTransfer.getRemoteFilePath());
	    // }

	    if (ftpClient.changeWorkingDirectory(fileTransfer.getRemoteFilePath())) {
	      this.directoryPath = fileTransfer.getRemoteFilePath();
	      FTPFile[] allFiles = ftpClient.listFiles();
	      for (FTPFile file : allFiles) {
	        if (file.isDirectory()) continue;
	        this.filesList.add(file.getName());
	      }
	      this.filesListFTPFileObject = allFiles;
	    } else {
	      String dir =
	          fileTransfer
	              .getRemoteFilePath()
	              .substring(0, fileTransfer.getRemoteFilePath().lastIndexOf(this.pathSaperator));
	      String fileName =
	          fileTransfer
	              .getRemoteFilePath()
	              .substring(
	                  fileTransfer.getRemoteFilePath().lastIndexOf(this.pathSaperator) + 1,
	                  fileTransfer.getRemoteFilePath().length());
	      this.directoryPath = dir;
	      if (!ftpClient.changeWorkingDirectory(dir)) {
	        this.directoryPath = null;
	        exit();
	       // logger.warn(LoggerConstants.LOG_MAXIQWEB + "Invalid Remote file path");
	       // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << downloadData()");
	        ExceptionsMessanger.throwException(new SystemException(), "ERR_134");
	      }
	      FTPFile[] allFiles = ftpClient.listFiles();

	      for (FTPFile file : allFiles) {
	        if (null != fileName && file.getName().equals(fileName)) {
	          if (file.isDirectory()) continue;
	          this.filesList.add(file.getName());
	          this.filesListFTPFileObject = new FTPFile[] {file};
	        }
	      }

	      if (null == this.filesList || this.filesList.isEmpty()) {
	        exit();
	      //  logger.warn(LoggerConstants.LOG_MAXIQWEB + "Invalid Remote file path");
	      //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " << downloadData()");
	        ExceptionsMessanger.throwException(new SystemException(), "ERR_134");
	      }
	    }
	    this.directoryPath = this.directoryPath.trim();
	    String lastChar =
	        this.directoryPath.substring(this.directoryPath.length() - 1, this.directoryPath.length());
	    if (lastChar.equals(this.pathSaperator)) {
	      this.directoryPath = this.directoryPath.substring(0, this.directoryPath.length() - 1);
	    }
	    return true;
  }

  public List<String> getSampleData(int count, String name) throws Exception {

	  this.getConnection();
    this.setDirPathAndFileName();
    ftpClient.changeWorkingDirectory(this.directoryPath);
    ArrayList<String> sampleData = new ArrayList<String>();
    List<String> filesList = this.filesList;

    int index = 0;

      BufferedReader reader = null;
      InputStream stream = null;
      String line = null;
      try {
        stream = null;
        reader = null;
        //stream = ftpClient.retrieveFileStream(filesList.get(index));
        stream = ftpClient.retrieveFileStream(name);
        reader = new BufferedReader(new InputStreamReader(stream));
        while (count > 0 ) {
          line = reader.readLine();
          if (line != null) {
            sampleData.add(line);
            count--;
          }
          else {
        	  break;
          }
        }
        ftpClient.completePendingCommand();
      } catch (Exception e) {
        e.printStackTrace();
        exit();
        throw new Exception(e.getMessage());
      }

    //exit();
    return sampleData;
  }

  public void exit() throws IOException {
  //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " << exit()");
    if (null != ftpClient) {
      ftpClient.logout();
      ftpClient.disconnect();
      ftpClient = null;
    }
  //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> exit()");
  }
}

package com.augiq.external.source.ftp;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.augmentiq.maxiq.base.encryption.decription.userinfo.SecretService;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;

public class FileTransfer implements Serializable {

  private int port;

  private String connectorType;
  private String host;
  private String user;
  private String password;
  private String remoteFilePath;
  private String localFilePath;

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public String getConnectorType() {
    return connectorType;
  }

  public void setConnectorType(String connectorType) {
    this.connectorType = connectorType;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRemoteFilePath() {
    return remoteFilePath;
  }

  public void setRemoteFilePath(String remoteFilePath) {
    this.remoteFilePath = remoteFilePath;
  }

  public String getLocalFilePath() {
    return localFilePath;
  }

  public void setLocalFilePath(String localFilePath) {
    this.localFilePath = localFilePath;
  }

  @Override
  public String toString() {
    return "FileTransfer [connectorType="
        + connectorType
        + ", host="
        + host
        + ", port="
        + port
        + ", user="
        + user
        + ", password="
        + password
        + ", remoteFilePath="
        + remoteFilePath
        + ", localFilePath="
        + localFilePath
        + "]";
  }

  public FileTransfer setValuesFromConnectorConfig(ConnectionConfig config, String indicator, String connectorType)
      throws Exception {
    SecretService secretService = new SecretService();
    this.setConnectorType(connectorType);
    this.setHost(config.getIpAddress());
    this.setPort(config.getPort());
    this.setUser(config.getDbUserName());
    this.setPassword(
        StringUtils.equalsIgnoreCase(indicator, "true")
            ? config.getDbPassword()
            : secretService.decrypt(config.getDbPassword()));

    this.setRemoteFilePath(config.getFtpfilePath());

    return this;
  }
}

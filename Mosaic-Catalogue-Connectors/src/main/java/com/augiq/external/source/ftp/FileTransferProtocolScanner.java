package com.augiq.external.source.ftp;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.amazonaws.services.s3.AmazonS3;
import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.constant.configuration.generic.GenericConstants;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
/*import com.augmentiq.maxiq.Configuration.Enums.ConnectorType;
import com.augmentiq.maxiq.Configuration.Enums.DataSourceType;
import com.augmentiq.maxiq.Configuration.generic.GenericConstants;
import com.augmentiq.maxiq.core.configurationdao.DataSourceInstanceDao;
import com.augmentiq.maxiq.entity.model.configuration.Bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.Bean.FieldMapping;
import com.augmentiq.maxiq.external.source.rdbms.connection.constants.ConnectionConstant;*/
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;
/*import com.augmrntiq.maxiq.external.factory.Connector.s3.AmazonS3Connector;*/
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;

/**
 * CLOUD level class to call publish, scan and test datasources for this connection
 * @author 10644726
 *
 */
public class FileTransferProtocolScanner implements Scanner,ApplicationContextAware{

	private ApplicationContext context;

	@Override
	public DataNode scan(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = (ConnectionSources)args.get("ConnectionSources");
		ConnectionSubSources  connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		String subSourceType = connectionSubSources.getSubConnectionType();
		Scanner scanner = (Scanner)context.getBean(subSourceType);
		return scanner.scan(args);
	}

	@Override
	public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {
		
		Map<String, List<String>> existingDSNotifier = new HashMap<String, List<String>>();
		existingDSNotifier.put(TaskStatusEnums.SUCCESS.name(), new ArrayList<String>());
		args.put("existingDataSourceNotifier", existingDSNotifier);

		
		DataNode dataNode = new DataNode();
		ConnectionSources connectionSources = RDBMSScanner.objectParserHandler((Object)args.get("connectionSources"), ConnectionSources.class);
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);

		DataNode dataNodefromUi = RDBMSScanner.objectParserHandler((Object) args.get("dataNode"),DataNode.class);
		DataNode filteredNodes = dataNode.getSelectedNodes(dataNodefromUi);
		Collection<DataNode> nodesToPublish = filterNodesToPublish(filteredNodes.getChildren());

		Boolean doSampling = (Boolean) args.get("doSampling"); 
		Integer sampleRowCount = (Integer)args.get("sampleRowCount");  

		// Publish each DataNode which is selected 
		for (Iterator iterator = nodesToPublish.iterator(); iterator.hasNext();) {

			DataNode publishDataNode = (DataNode) iterator.next();   

			String subSourceType = StringUtils.upperCase(connectionSubSources.getSubConnectionType());

			args.put(GenericConstants.DATA_SOURCE.DATASOURCENAME, publishDataNode.getLabel());
			ConfigurationCreateUtil configurationCreateUtil = new ConfigurationCreateUtil();
			Long dsId =  configurationCreateUtil.createDataSources(args);

			DataSource dataSource = DataRepositoryDao.getDataSource(dsId);
			dataSource.setAdvancedValidations(null);
			dataSource.setAdvancedValidationsStore(null);
			dataSource.setTransformations(null);
			dataSource.setDataCleansers(null);
			dataSource.setAppId(null);
			dataSource.setNodeId(null);

			Scanner scanner = (Scanner)context.getBean(subSourceType);
			
			args.put("dataSourceId", dsId);
			args.put("publishNode", publishDataNode);
			scanner.publishDataSources(args);
		}
		
		return existingDSNotifier;
	}
	
	@Override
	public String testConnection(Map<String, Object> args) {
		String subSourceType = args.get("subSourceType").toString();
		Scanner scanner = (Scanner)context.getBean(subSourceType);
		return scanner.testConnection(args);
	}
	
	/**
	 * @param dataNodes - children datanodes of root node
	 * @return -a list of DataNode which are to be published in Catalog
	 */
	public Collection<DataNode> filterNodesToPublish(Collection<DataNode> dataNodes) {

		Collection<DataNode> validNodes = new ArrayList<DataNode>();

		for(DataNode itrDataNode: dataNodes) {
			if(!itrDataNode.getChildren().isEmpty()) {
				Collection<DataNode> subList = filterNodesToPublish(itrDataNode.getChildren());
				validNodes.addAll(subList);
			}
			else {
				if(itrDataNode.getIsDsNode() && itrDataNode.getSelected())
					validNodes.add(itrDataNode);
			}
		}

		return validNodes;
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;

	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}

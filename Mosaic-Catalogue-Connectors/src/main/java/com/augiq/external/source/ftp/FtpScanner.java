package com.augiq.external.source.ftp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.base.encryption.decription.userinfo.SecretService;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;

public class FtpScanner implements Scanner{

	@Override
	public DataNode scan(Map<String, Object> args) throws Exception {
		DataNode dataNode = null;

		ConnectionSources connectionSources = (ConnectionSources)args.get("ConnectionSources");
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = (ConnectionConfig) connectionSubSources.getConnectionConfig().get(0);

		String connectorType = connectionSubSources.getSubConnectionType();

		ProtocolHandler protocolHandler = new ProtocolHandler(connectionConfig, "true", connectorType);
		dataNode = protocolHandler.createDataNodeForFTP(connectionConfig);

		return dataNode;
	}

	@Override
	public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {

		Map<String, List<String>> existingDSNotifier = new HashMap<String, List<String>>();
		existingDSNotifier.put(TaskStatusEnums.SUCCESS.name(), new ArrayList<String>());

		DataNode publishDataNode = (DataNode) args.get("publishNode");
		Long dataSourceId = (Long) args.get("dataSourceId");
		//DataSource dataSource = DataRepositoryDao.getDataSource(dataSourceId);

		ConnectionSources connectionSources = RDBMSScanner.objectParserHandler((Object)args.get("connectionSources"), ConnectionSources.class);
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);

		String connectorType = connectionSubSources.getSubConnectionType();

		connectionConfig.setFtpfilePath(publishDataNode.getFtpFileLocation());
		ProtocolHandler protocolHandler = new ProtocolHandler(connectionConfig, "true", connectorType);

		DataSource datasourceresult = protocolHandler.fetchDataSource(dataSourceId, connectionConfig, publishDataNode, connectorType);

		return existingDSNotifier;
	}

	@Override
	public String testConnection(Map<String, Object> args) {

		ConnectionConfig connectionConfig = (ConnectionConfig) args.get("connectionConfig");
		String connectorType = (String) args.get("subSourceType");

		String message = TaskStatusEnums.FAIL.name();
		ProtocolHandler protocolHandler;
		try {
			protocolHandler = new ProtocolHandler(connectionConfig, "true", connectorType);
			boolean con = protocolHandler.testConnection();

			if(con) 
				message = TaskStatusEnums.SUCCESS.name();

		} catch (Exception e) {

			//TODO - add exception in info logs..
			// log message =e.printStackTrace();
		}		
		return message;
	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}

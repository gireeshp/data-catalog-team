package com.augiq.external.source.ftp;
//package com.augmentiq.maxiq.CONSTATS;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.ConnectorType;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.model.configuration.factory.FieldMappingFactory;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.util.component.configuration.util.ExternalDataSourceService;
import com.augmentiq.maxiq.util.component.configuration.util.HardCodeServices;
import com.augmentiq.maxiq.util.component.configuration.util.SampleRecordExtractor;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
/*import com.augmentiq.maxiq.CONSTATS.LoggerConstants;
import com.augmentiq.maxiq.Cache.Cache;
import com.augmentiq.maxiq.Cache.CacheConstants;
import com.augmentiq.maxiq.Configuration.Enums.CompressionTypeEnum;
import com.augmentiq.maxiq.Configuration.Enums.ConnectorType;
import com.augmentiq.maxiq.Configuration.Enums.DataSourceType;
import com.augmentiq.maxiq.Configuration.Enums.FileTypeEnum;
import com.augmentiq.maxiq.Configuration.Exceptions.MessageHandler.ExceptionsMessanger;
import com.augmentiq.maxiq.Configuration.Utility.ParamUtils;
import com.augmentiq.maxiq.canvas.workFlow.general.Constants;
import com.augmentiq.maxiq.core.configurationdao.DataRepositoryDao;
import com.augmentiq.maxiq.core.configurationdao.DataRepositoryFetchWithChild;
import com.augmentiq.maxiq.core.configurationdao.DataSourceInstanceDao;
import com.augmentiq.maxiq.core.dao.configuration.exceoption.SystemException;
import com.augmentiq.maxiq.core.models.configuration.Bean.DataSource;
import com.augmentiq.maxiq.core.models.configuration.Bean.FieldMapping;
import com.augmentiq.maxiq.core.models.configuration.Bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.core.models.configuration.Bean.FileTransfer;
import com.augmentiq.maxiq.core.models.connector.ConnectorConfig;
import com.augmentiq.maxiq.core.services.communicationdetails.CommunicationDetailsBSO;
import com.augmentiq.maxiq.core.services.configurationservices.DataSourceBso;
import com.augmentiq.maxiq.core.services.externaldsservice.ExternalDataSourceService;
import com.augmentiq.maxiq.core.workontopics.SampleRecordExtractor;*/
import com.jcraft.jsch.JSchException;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Changed for MAX-101 By Rushikesh Raut on 21-Sept-2016 Changed for MAX-502 By Balkrushna Patil on
 * 21-Sept-2016
 */
public class ProtocolHandler {
	private static final Logger logger = LoggerFactory.getLogger(ProtocolHandler.class);

	public FTPDataSource ftp = null;
	public SFTPDataSource sftp = null;
	public ConnectionConfig config;
	public FileTransfer fileTransfer = new FileTransfer();

	public ProtocolHandler(ConnectionConfig config, String indicator, String connectorType) throws Exception {
		fileTransfer.setValuesFromConnectorConfig(config, indicator, connectorType);
		ftp = new FTPDataSource(fileTransfer);
		sftp = new SFTPDataSource(fileTransfer);
	}

	public void getConnection() throws Exception {
		//logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getConnection()");
		try {
			if (("FTP SERVER").equalsIgnoreCase(fileTransfer.getConnectorType())) {
				ftp = new FTPDataSource(fileTransfer);
				ftp.getConnection();
			} else if (("SFTP SERVER").equalsIgnoreCase(fileTransfer.getConnectorType())) {
				sftp = new SFTPDataSource(fileTransfer);
				sftp.getConnection();
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new IOException(e.getMessage());
		} catch (JSchException e) {
			e.printStackTrace();
			throw new JSchException(e.getMessage());
		}
	}
	/**
	 * @param dataSourceId
	 * @param delimiter
	 * @param config
	 * @return
	 * @throws Exception
	 */
	public DataSource fetchDataSource(Long dataSourceId, ConnectionConfig config, DataNode dataNode, String subsourceType)
			throws Exception {

		boolean isHeaderPresent = false;

		DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceId);

		FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();

		List<FieldMapping> existingFieldMappings = dataSource.getFieldMappings();

		dataSource.setDataSourceType(DataSourceType.FTP_DATA);

		if (null == config) {
			config = new ConnectionConfig();
		}

		dataSource.setFileStoreObject(dataSource.getFileStoreObject());
		dataSource.setDataAtRestCompressionType(CompressionTypeEnum.SNAPPY.name());
		dataSource.setDataAtRestFileType(FileTypeEnum.PARQUET.name());
		dataSource.setConfig(config);

		if (fileDataIngesterDetails == null) fileDataIngesterDetails = new FileDataIngesterDetails();

		fileDataIngesterDetails.setDataSourceId(dataSourceId);
		fileDataIngesterDetails.setContainsHeader(isHeaderPresent + "");
		fileDataIngesterDetails.setDelimiter(dataNode.getDelimiter());
		fileDataIngesterDetails.setOptionallyEnclosedInDoubleQuotes(dataNode.getOptionallyEnclosedInDoubleQuotes());
		fileDataIngesterDetails.setDataSourceType(DataSourceType.FTP_DATA);
	//	fileDataIngesterDetails.setServerFilePath(
		//		Cache.getProperty(CacheConstants.UPLOAD_PATH) + System.currentTimeMillis());

		Map<String, String> updatedValue = new HashMap<String, String>();
		updatedValue.put("dataSourceType", DataSourceType.FTP_DATA.toString());

		DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(
				fileDataIngesterDetails.getDataSourceId(), updatedValue);

		dataSource.setFileDataIngesterDetails(fileDataIngesterDetails);

		List<FieldMapping> fieldMappings = null;
		List<String> fields = fetchSampleData(config, dataNode, subsourceType);

		if (isHeaderPresent) {
			fieldMappings =
					ExternalDataSourceService.fetchFieldMapping(
							fields, 1L, dataSource, dataSourceId, null, isHeaderPresent);
		} else {
			fieldMappings =
					ExternalDataSourceService.fetchFieldMapping(
							fields, 0L, dataSource, dataSourceId, null, isHeaderPresent);
		}

		fieldMappings = SampleRecordExtractor.dataTypeGenerator(fieldMappings);
		dataSource.setFieldMappings(fieldMappings);

	/*	boolean isSchemaChanged =
				CommunicationDetailsBSO.verifyCurrentDataSourceFieldMappingWithExisting(
						fieldMappings, existingFieldMappings);

		//logger.info("Is field Mapping schema has changes :" + isSchemaChanged);

		if (isSchemaChanged) {

			String uuid =
					CommunicationDetailsBSO.saveCommunicationDetails(
							config, dataSource.getFieldMappings(), Constants.FIELDMAPPING_WARNING);
			ExceptionsMessanger.throwException(
					new SystemException(), "ERR_134", ParamUtils.getString(uuid));
		}*/

		DataRepositoryDao.insertDataSource(dataSource);

		// logger.debug(
		//   LoggerConstants.LOG_MAXIQWEB
		//     + " : << fetchDataSource()"
		//   + ParamUtils.getString(dataSource));
		return dataSource;
	}
	 
	public List<String> fetchSampleData(ConnectionConfig config, DataNode dataNode, String subsourcetype) throws Exception {
		//logger.debug(
		//  LoggerConstants.LOG_MAXIQWEB + " : >> fetchSampleData()" + ParamUtils.getString(config));
		List<String> sampleData = null;
		if (("FTP SERVER").equalsIgnoreCase(subsourcetype)) {
			sampleData = ftp.getSampleData(10,dataNode.getLabel());
			ftp.exit();
		} else if (("SFTP SERVER").equalsIgnoreCase(subsourcetype)) {
			//sampleData = sftp.getSampleData(10);
			//sftp.exit();
		}
		//  logger.debug(
		//    LoggerConstants.LOG_MAXIQWEB
		//      + " : << fetchSampleData()"
		//    + ParamUtils.getString(sampleData));
		return sampleData;
	}

	public boolean testConnection() throws Exception {
		try {
			if (("FTP SERVER").equalsIgnoreCase(fileTransfer.getConnectorType())) {
				ftp = new FTPDataSource(fileTransfer);
				if (ftp.getTestConnection()) {
					return true;
				}
			} else if (ConnectorType.SFTP.toString().equalsIgnoreCase(fileTransfer.getConnectorType())) {
				sftp = new SFTPDataSource(fileTransfer);
				if (sftp.getConnection()) {
					return true;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			ftp.exit();

			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		ftp.exit();

		return false;
	}

	/*public void downloadData(DataSource dataSource) throws Exception {

		//  logger.debug(
		//    LoggerConstants.LOG_MAXIQWEB + " >> downloadData()" + ParamUtils.getString(dataSource));
		getConnection();
		ConnectorConfig config = DataSourceBso.getConnectorConfig(dataSource.getConfig());
		config.setDefaultLocationForFTP(dataSource.getConfig().getDefaultLocationForFTP());
		dataSource.setConfig(config);

		fileTransfer.setLocalFilePath(dataSource.getFileDataIngesterDetails().getServerFilePath());

		if (ConnectorType.FTP.toString().equalsIgnoreCase(config.getConnectorType())) {
			ftp = new FTPDataSource(fileTransfer);
			ftp.downloadData();
		} else if (ConnectorType.SFTP.toString().equalsIgnoreCase(config.getConnectorType())) {
			sftp = new SFTPDataSource(fileTransfer);
			sftp.downloadData();
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << downloadData()");
	}

*/

	public DataNode createDataNodeForFTP(ConnectionConfig config) {
		DataNode rootNode = new DataNode("Root_Directory", false, false);
		try {
			this.getConnection();
			ftp.setDirPathAndFileName();
			
			FTPFile[] files = ftp.ftpClient.listFiles();
			createDataNode(rootNode, files, ftp.directoryPath, config);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootNode;
		
	}


	public DataNode createDataNode(DataNode parentNode, FTPFile[] files, String dirPath, ConnectionConfig config) throws IOException {  

		ftp.ftpClient.changeWorkingDirectory(dirPath);
		ftp.directoryPath = dirPath;
		
		for(FTPFile file: files) {
			if(file.isFile()) {
				DataNode fileNode = createNode(file.getName(), dirPath, config, false, true);
				attachNode(parentNode, fileNode);
			}
			else {
				if(file.isDirectory()) {
					
					String newDirPath = dirPath + ftp.pathSaperator + file.getName();
					ftp.ftpClient.changeWorkingDirectory(newDirPath);
					ftp.fileTransfer.setRemoteFilePath(newDirPath);
					ftp.directoryPath = newDirPath;
					
					/*try {
						ftp.setDirPathAndFileName();
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					
					DataNode newParent = createNode(file.getName(), newDirPath,config, false, false);
					FTPFile[] newFiles = ftp.ftpClient.listFiles();  //(newDirPath);
					
					createDataNode(newParent, newFiles, newDirPath, config);
					attachNode(parentNode, newParent);
				}
			}
		}

		return parentNode;
	}

	public DataNode createNode(String fileName, String filePath,ConnectionConfig connectionConfig, Boolean isSelected, Boolean isDSNode) {
		DataNode dataNode = new DataNode(fileName,isSelected,isDSNode);
		dataNode.setFtpFileLocation(filePath);
		return dataNode;
	}

	public void attachNode(DataNode parentNode, DataNode childNode) {
		parentNode.getChildren().add(childNode); 
	}

}


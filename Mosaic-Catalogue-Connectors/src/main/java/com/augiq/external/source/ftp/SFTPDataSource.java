package com.augiq.external.source.ftp;


import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
//import com.augmentiq.maxiq.CONSTATS.LoggerConstants;
//import com.augmentiq.maxiq.Configuration.Utility.ParamUtils;
//import com.augmentiq.maxiq.core.dao.configuration.exceoption.SystemException;
//import com.augmentiq.maxiq.core.models.configuration.Bean.FileTransfer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

public class SFTPDataSource {

  public Session session = null;
  public Channel channel = null;
  public ChannelSftp channelSftp = null;
  public FileTransfer fileTransfer = null;

  private static File staggingFile_;
//  static final Logger logger = Logger.getLogger(SFTPDataSource.class);

  public SFTPDataSource(FileTransfer fileTransfer) throws Exception {
    this.fileTransfer = fileTransfer;
  }

  public boolean getConnection() throws SystemException, JSchException {
    //logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getConnection()");
    try {
      JSch jsch = new JSch();
      session =
          jsch.getSession(fileTransfer.getUser(), fileTransfer.getHost(), fileTransfer.getPort());
      session.setPassword(fileTransfer.getPassword());

      Properties config = new Properties();
      config.put("StrictHostKeyChecking", "no");
      session.setConfig(config);
      session.connect();
      channel = session.openChannel("sftp");
      channel.connect();
      if (!channel.isConnected()) {
  //      logger.warn(LoggerConstants.LOG_MAXIQWEB + "Unable to Connect to SFTP");
  //      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getConnection()");
        return false;
      }
      channelSftp = (ChannelSftp) channel;

    } catch (JSchException e) {

      e.printStackTrace();
 //     logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
 //     logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getConnection()");
      throw new JSchException(e.getMessage());
    }
 //   logger.debug(
   //     LoggerConstants.LOG_MAXIQWEB + " : << getConnection()" + ParamUtils.getString(true));
    return true;
  }

  private ArrayList<String> getFileList() throws SftpException {
  //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getFileList()");
    String dirPath;
    ArrayList<String> list = new ArrayList<String>();

    // check if path is dir or file
    // if null, then its dir

    String fileName = getFileNameFromPath(fileTransfer.getRemoteFilePath());
    if (fileName == null) {
      dirPath = fileTransfer.getRemoteFilePath();
      channelSftp.cd(dirPath);
      Vector<LsEntry> entries = channelSftp.ls("*.*");
      for (LsEntry entry : entries) {
        list.add(entry.getFilename());
      }
    } else {
      list.add(fileName);
    }
  //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getFileList()" + ParamUtils.getString(list));
    return list;
  }

  private Vector<LsEntry> getFileListAsLsEntry() throws SftpException {
 //   logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getFileListAsLsEntry()");
    String dirPath;
    Vector<LsEntry> list = new Vector<LsEntry>();

    // check if path is dir or file
    // if null, then its dir

    String fileName = getFileNameFromPath(fileTransfer.getRemoteFilePath());
    if (fileName == null) {
      dirPath = fileTransfer.getRemoteFilePath();
      channelSftp.cd(dirPath);
      Vector<LsEntry> entries = channelSftp.ls("*.*");
      for (LsEntry entry : entries) {
        if (!entry.getAttrs().isDir()) {
          list.addElement(entry);
        }
      }
    } else {
      channelSftp.cd(getDirPath(fileTransfer.getRemoteFilePath()));
      Vector<LsEntry> entries = channelSftp.ls("*.*");
      for (LsEntry entry : entries) {
        if (!entry.getAttrs().isDir() && entry.getFilename().equals(fileName)) {
          list.addElement(entry);
        }
      }
    }
 //   logger.debug(
   //     LoggerConstants.LOG_MAXIQWEB + " : >> getFileListAsLsEntry()" + ParamUtils.getString(list));
    return list;
  }

  private String getDirPath(String path) throws SftpException {

    // this function return directory path from given path if we are not
    // sure given path is only directory or full file path

    String dirPath = path;
    SftpATTRS filesAndDirs = channelSftp.lstat(path);
    if (!filesAndDirs.isDir()) {
      dirPath = path.substring(0, path.lastIndexOf("/"));
      SftpATTRS chkIsDIr = channelSftp.lstat(dirPath);
      if (!chkIsDIr.isDir()) {
        return null;
      }
    }

    return dirPath;
  }

  @SuppressWarnings("finally")
  private String getFileNameFromPath(String path) throws SftpException {
    String fileName = null;
    // this function return filename from given path if we are not sure
    // given path is only directory or full file path
    // it return null if its dir
    try {
      SftpATTRS filesAndDirs = channelSftp.lstat(path);
      if (!filesAndDirs.isDir()) {
        fileName = path.substring(path.lastIndexOf("/") + 1);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      return fileName;
    }
  }

  public ArrayList<String> getSampleData(int count) throws Exception {

   // logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getSampleData()");
    getConnection();
    List<String> sampleRecordsList = new ArrayList<String>();
    ArrayList<String> fileList = getFileList();
    ArrayList<String> sampleData = new ArrayList<String>();

    String directoryName;
    if (null == getFileNameFromPath(fileTransfer.getRemoteFilePath())) {
      directoryName = fileTransfer.getRemoteFilePath().trim();
      if ((directoryName.lastIndexOf("/") + 1) == directoryName.length()) {
        directoryName = directoryName.substring(0, directoryName.lastIndexOf("/"));
      }
    } else {
      directoryName = getDirPath(fileTransfer.getRemoteFilePath());
    }

    int fileIndex = 0;
    InputStream stream = channelSftp.get(directoryName + "/" + fileList.get(fileIndex));
    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(stream));
      while (0 < count--) {
        String line = br.readLine();

        if (null == line) {
          fileIndex++;
          if (fileIndex >= fileList.size()) {
            break;
          }
          stream = channelSftp.get(directoryName + "/" + fileList.get(fileIndex));
          br = new BufferedReader(new InputStreamReader(stream));
          count++;
          continue;
        }
        sampleData.add(line);
      }

    } finally {
      stream.close();
    }
  //  logger.debug(
    //    LoggerConstants.LOG_MAXIQWEB + " : >> getSampleData()" + ParamUtils.getString(sampleData));
    return sampleData;
  }

  public void downloadData() throws Exception {
    //logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << downloadData()");
    // TODO Auto-generated method stub
    // Create local folders if absent.
    getConnection();
    String sourcePath = fileTransfer.getRemoteFilePath();
    String fileName = getFileNameFromPath(fileTransfer.getRemoteFilePath());

    if (null != fileName) {
      sourcePath = getDirPath(sourcePath);
    }

    channelSftp.cd(sourcePath);
    try {
      new File(fileTransfer.getLocalFilePath()).mkdirs();
    } catch (Exception e) {
      e.printStackTrace();
   //   logger.warn(
   //       LoggerConstants.LOG_MAXIQWEB + " Error at : " + fileTransfer.getLocalFilePath() + e);
   //   logger.debug(LoggerConstants.LOG_MAXIQWEB + " << downloadData()");
      throw new Exception(e.getMessage());
    }

    // Copy remote files one by one.

    copyFilesOnlyFromFolder(sourcePath, fileTransfer.getLocalFilePath(), channelSftp);
 //   logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> downloadData()");
  }

  public void exit() {
  //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << exit()");
    channel.disconnect();
    channelSftp.exit();
    session.disconnect();
  //  logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> exit()");
  }

  private void copyFilesOnlyFromFolder(String sourcePath, String destPath, ChannelSftp channelSftp)
      throws SftpException {

    channelSftp.cd(sourcePath);
    channelSftp.lcd(destPath);
    Vector<LsEntry> fileList = getFileListAsLsEntry();
    for (LsEntry file : fileList) {
      if (!file.getAttrs().isDir()) { // If it is a file (not a
        // directory).
        if (!(new File(destPath + "/" + file.getFilename())).exists()
            || (file.getAttrs().getMTime()
                > Long.valueOf(
                        new File(destPath + "/" + file.getFilename()).lastModified() / (long) 1000)
                    .intValue())) { // Download only if changed later.
          new File(destPath + "/" + file.getFilename());
          channelSftp.get(
              sourcePath + "/" + file.getFilename(), destPath + "/" + file.getFilename()); // Grab
          // //
          // filename]).
        }
      }
    }
  }
}

package com.augiq.external.source.nosql;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public class NoSQLScanner implements Scanner , ApplicationContextAware {

	@Autowired
    private ConfigurationCreateUtil createUtil;
	
	private ApplicationContext context;
	
	@Override
	public DataNode scan(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = (ConnectionSources) args.get("ConnectionSources");
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		String subSourceType = connectionSubSources.getSubConnectionType();
		Scanner scanner = (Scanner) context.getBean(subSourceType);
		return scanner.scan(args);
	}

	@Override
	public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = objectParserHandler((Object)args.get("connectionSources"), ConnectionSources.class);
		ConnectionSubSources connectionSubSources = (ConnectionSubSources) connectionSources.getConnectionSubSources().get(0);
		Scanner scanner = ((Scanner)context.getBean(connectionSubSources.getSubConnectionType()));
		return scanner.publishDataSources(args);
	}

	@Override
	public String testConnection(Map<String, Object> args) {
		String subSourceType = args.get("subSourceType").toString();
		Scanner scanner = (Scanner) context.getBean(subSourceType);
		return scanner.testConnection(args);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {

		this.context = context;
	}

	public static <T> T objectParserHandler(Object object, Class<T> entity)
			throws JsonParseException, JsonMappingException, IOException {
		String jsonString = new Gson().toJson(object);
		ObjectMapper mapper = new ObjectMapper();
		T obj = mapper.readValue(jsonString, entity);
		return obj;

	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}

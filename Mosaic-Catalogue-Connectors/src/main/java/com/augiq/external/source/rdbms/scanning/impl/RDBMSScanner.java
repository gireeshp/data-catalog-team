package com.augiq.external.source.rdbms.scanning.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.transaction.annotation.Transactional;

import com.augiq.external.source.rdbms.schema.resolver.reader.SchemaReader;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.constant.configuration.enums.ConnectorType;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;
import com.augmentiq.maxiq.constant.configuration.generic.GenericConstants;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserObjectMapping;
import com.augmentiq.maxiq.external.source.rdbms.scanner.utility.TableListingUtil;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.repository.connector.external.connectiondao.ConnectorListingDAO;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;
import com.augmentiq.maxiq.util.external.source.rdbms.connection.lookup.directory.ConnectionLookUpDirectory;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Transactional
public class RDBMSScanner implements Scanner, ApplicationContextAware {

	@Autowired
	private ConnectorListingDAO rdbmsconnectorListingDAO;

	@Autowired
	private ConfigurationCreateUtil createUtil;

	private ConnectionConfig rdbmsConnectionConfig;

	private String subSourceType;

	private ApplicationContext context;

	private static final Logger logger = LoggerFactory.getLogger(RDBMSScanner.class);

	public String getSubSourceType() {
		return subSourceType;
	}

	public void setSubSourceType(String subSourceType) {
		this.subSourceType = subSourceType;
	}

	public List<String> getSeletedDSNames(List<DataNode> dataNodes) {
		List<String> dsNames = new ArrayList<String>();
		for (DataNode dataNode : dataNodes) {
			if (dataNode.getIsDsNode() && dataNode.getSelected())
				dsNames.add(dataNode.getLabel());
		}
		return dsNames;
	}

	@Override
	public Map<String, List<String>> publishDataSources(Map<String, Object> args) throws Exception {

		Map<String, List<String>> existingDSNotifier = new HashMap<String, List<String>>();
		existingDSNotifier.put(TaskStatusEnums.SUCCESS.name(), new ArrayList<String>());
		args.put("existingDataSourceNotifier", existingDSNotifier);
		DataNode dataNode = new DataNode();
		ConnectionSources connectionSources = objectParserHandler((Object) args.get("connectionSources"),
				ConnectionSources.class);
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);

		// Complete Node Hierarchy from UI(consisting of maxied collection i.e.
		// selected/unselected)
		DataNode dataNodefromUi = objectParserHandler((Object) args.get("dataNode"), DataNode.class);
		// filtered nodes consisting of hierarchy only of selected nodes with
		// complete path
		DataNode filteredNodes = dataNode.getSelectedNodes(dataNodefromUi);
		// nodes whose datasource is to be created

		Collection<Map<String, List<String>>> dbWithTablesAsChild = getDbWithTablesList(filteredNodes);
		Iterator<Map<String, List<String>>> dbWithTblItr = dbWithTablesAsChild.iterator();
		Boolean doSampling = (Boolean) args.get("doSampling");
		Integer sampleRowCount = (Integer) args.get("sampleRowCount");
		while (dbWithTblItr.hasNext()) {
			List<FieldMapping> fieldMappings = null;
			String databaseName = null;
			List<String> currentTablesAndViewsList = null;
			Map<String, List<String>> currentDbWithtbldetails = dbWithTblItr.next();
			Set<Entry<String, List<String>>> set = currentDbWithtbldetails.entrySet();
			Iterator<Map.Entry<String, List<String>>> entryItr = set.iterator();
			while (entryItr.hasNext()) {
				Map.Entry<String, List<String>> entry = entryItr.next();
				databaseName = entry.getKey();
				currentTablesAndViewsList = entry.getValue();
				Iterator<String> iterator = currentTablesAndViewsList.iterator();
				while (iterator.hasNext()) {
					Connection connection = null;
					this.subSourceType = StringUtils.upperCase(connectionSubSources.getSubConnectionType());
					if (subSourceType.equals(ConnectorType.ORACLE.name())) {
						connection = establishRdbmsConnection(connectionConfig);

					} else {
						connection = establishRdbmsConnection(connectionConfig, databaseName);
					}
					String tableName = iterator.next();
					args.put(GenericConstants.DATA_SOURCE.DATASOURCENAME, tableName);
					Long dsId = createUtil.createDataSources(args);
					if (dsId == null) {
						continue;
					}
					fieldMappings = getFieldMapping(databaseName, tableName, connection);
					if (doSampling != null && sampleRowCount != null && doSampling == true) {
						fieldMappings = addSampleData(fieldMappings, connection, tableName, sampleRowCount);
					}
					DataSource dataSource = DataRepositoryDao.getDataSource(dsId);
					dataSource.setAdvancedValidations(null);
					dataSource.setAdvancedValidationsStore(null);
					dataSource.setTransformations(null);
					dataSource.setDataCleansers(null);
					dataSource.setAppId(null);
					dataSource.setNodeId(null);
					dataSource.setExpert(((Integer) args.get("expert")).longValue());
					dataSource.setTags(
							args.get("tags") != null ? (List<String>) args.get("tags") : new ArrayList<String>());
					dataSource.setRefreshedOn(System.currentTimeMillis());
					if (null != dataSource && null != fieldMappings) {
						ConnectionConfig config = connectionConfig;
						String driverUrl = null;
						if (connectionSubSources.getSubConnectionType().equals(ConnectorType.ORACLE.name())) {
							driverUrl = ConnectionLookUpDirectory.generateRdbmsConnectUrl(
									connectionSubSources.getSubConnectionType(), config.getConnectionUrl(),
									config.getPort(), config.getSid());
						} else {
							driverUrl = ConnectionLookUpDirectory.generateRdbmsConnectUrl(
									connectionSubSources.getSubConnectionType(), config.getConnectionUrl(),
									config.getPort(), null);
							driverUrl = driverUrl + "/" + databaseName;
						}
						config.setConnectionUrl(driverUrl);
						config.setDbName(databaseName);
						dataSource.setConfig(config);
						if (subSourceType.equals(ConnectorType.POSTGRES.name())) {
							String prestoTableAlias = connectionSubSources.getSubConnectionType().toLowerCase() + "."
									+ "public" + "." + tableName;
							dataSource.setPrestoTableAlias(prestoTableAlias);
						} else {
							String prestoTableAlias = connectionSubSources.getSubConnectionType().toLowerCase() + "."
									+ databaseName + "." + tableName;
							dataSource.setPrestoTableAlias(prestoTableAlias);
						}

						// dataSource.setDataSourceType(DataSourceType.RDBMS_DATA_SOURCE);
						if (subSourceType.equals(ConnectorType.MYSQL.name()))
							dataSource.setDataSourceType(DataSourceType.MYSQL);
						if (subSourceType.equals(ConnectorType.POSTGRES.name()))
							dataSource.setDataSourceType(DataSourceType.POSTGRESQL);
						if (subSourceType.equals(ConnectorType.ORACLE.name()))
							dataSource.setDataSourceType(DataSourceType.ORACLE);

						dataSource.setFieldMappings(fieldMappings);
						dataSource.setConnectionSources(connectionSources);
						DataRepositoryDao.insertDataSource(dataSource);
						Map<String, String> updatedValue = new LinkedHashMap<String, String>();
						updatedValue.put("dataSourceType", (dataSource.getDataSourceType()).toString());
						DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(dataSource.getId(), updatedValue);
					}

				}

			}

		}

		return existingDSNotifier;
	}

	public DataNode scan(Map<String, Object> args) throws Exception {
		ConnectionSources connectionSources = (ConnectionSources) args.get("ConnectionSources");
		rdbmsConnectionConfig = getConnectionConfig(connectionSources);
		DataNode schemas = getSchemaDetails(rdbmsConnectionConfig, connectionSources);
		Gson gson = new Gson();
		return schemas;
	}

	@Transactional
	private ConnectionConfig getConnectionConfig(ConnectionSources connectionSources) {
		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		Long connectionId = connectionSubSources.getConnectionConfig().get(0).getConnectionId();
		return rdbmsconnectorListingDAO.getConnectionConfig(connectionId);
	}

	public Connection establishRdbmsConnection(ConnectionConfig connectionConfig)
			throws SQLException, ClassNotFoundException, SystemException {
		String userName = connectionConfig.getDbUserName();
		String password = connectionConfig.getDbPassword();
		Integer port = connectionConfig.getPort();
		String sid = connectionConfig.getSid();
		String host = connectionConfig.getIpAddress();
		String druverUrl = ConnectionLookUpDirectory.generateRdbmsConnectUrl(subSourceType, host, port, sid);
		String driverClass = ConnectionLookUpDirectory.lookupRdbmsDriverClass(subSourceType);
		Class.forName(driverClass);

		Connection connection = DriverManager.getConnection(druverUrl, userName, password);
		return connection;
	}

	private Connection establishRdbmsConnection(ConnectionConfig connectionConfig, String databaseName)
			throws SQLException, ClassNotFoundException, SystemException {
		String userName = connectionConfig.getDbUserName();
		String password = connectionConfig.getDbPassword();
		Integer port = connectionConfig.getPort();
		String sid = connectionConfig.getSid();
		String host = connectionConfig.getIpAddress();
		String driverUrl = ConnectionLookUpDirectory.generateRdbmsConnectUrl(subSourceType, host, port, sid);
		if ("POSTGRES".equals(subSourceType)) {
			driverUrl = driverUrl + databaseName;
		} else {
			driverUrl = driverUrl + "/" + databaseName;
		}

		String driverClass = ConnectionLookUpDirectory.lookupRdbmsDriverClass(subSourceType);
		Class.forName(driverClass);

		Connection connection = DriverManager.getConnection(driverUrl, userName, password);
		return connection;
	}

	private void releaseConnection(List<ResultSet> resultSetList, Connection connection) {

		if (resultSetList != null) {
			Iterator<ResultSet> resultSetItr = resultSetList.iterator();
			while (resultSetItr.hasNext()) {
				ResultSet resultSet = resultSetItr.next();
				try {
					if (resultSet != null) {
						resultSet.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void releaseConnection(ResultSet resultSet, Connection connection) {

		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void releaseConnection(Connection connection) {

		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void releaseResultSet(List<ResultSet> resultSetList) {

		Iterator<ResultSet> resultSetItr = resultSetList.iterator();
		while (resultSetItr.hasNext()) {
			ResultSet resultSet = resultSetItr.next();
			try {
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public DataNode getSchemaDetails(ConnectionConfig connectionConfig, ConnectionSources connectionSources)
			throws SystemException {

		ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
		this.subSourceType = StringUtils.upperCase(connectionSubSources.getSubConnectionType());
		// Collection<DataNode> listOfNodes = new ArrayList<DataNode>();
		final DataNode schemaNode = new DataNode("Schemas", false, false);
		SchemaReader schemaReader = null;

		String databaseName = null;
		Connection connection = null;
		ResultSet schemaResultset = null;
		try {
			connection = establishRdbmsConnection(connectionConfig);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@//"
		// + hostUrl + ":" + mySQLPort, userName, password);

		// --- LISTING DATABASE SCHEMA NAMES ---

		if (subSourceType.equals(ConnectorType.MYSQL.name())) {
			schemaReader = (SchemaReader) context.getBean(subSourceType);
		} else if (subSourceType.equals(ConnectorType.POSTGRES.name())) {
			schemaReader = (SchemaReader) context.getBean(subSourceType);
		} else {
			schemaReader = (SchemaReader) context.getBean(ConnectorType.OTHERS.name());
		}
		try {
			if (subSourceType.equals(ConnectorType.POSTGRES.name())) {
				schemaResultset = schemaReader.listAllSchemaNames(connection);

			} else {
				schemaResultset = schemaReader.listAllSchemaNames(connection);
			}
			List<Thread> threads = new ArrayList<Thread>();
			Thread currentThread = Thread.currentThread();
			while (schemaResultset.next()) {
				/*
				 * Here we are creating objects of DataNode. Schema node will contain Database
				 * name and its child will be Table and Views. Child of tableNode will be list
				 * of tables under current schema. Child of viewNode will be list of views under
				 * current Schema. This is designed in this way due to front end api requirement
				 */

				DataNode dbNode = new DataNode();
				DataNode tableNode = new DataNode();
				DataNode viewNode = new DataNode();

				databaseName = schemaReader.getCurrentSchemaName(schemaResultset);
				if ("postgres".equals(databaseName)) {
					continue;
				}
				dbNode.setLabel(databaseName);
				tableNode.setLabel("Table");
				viewNode.setLabel("View");
				TableListingUtil tlu = new TableListingUtil();
				if (subSourceType.equals(ConnectorType.POSTGRES.name())) {
					try {
						connection = null;
						connection = establishRdbmsConnection(connectionConfig, databaseName);
						tlu.setConnection(connection);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					tlu.setConnection(connection);
				}

				tlu.setDbName(databaseName);
				tlu.setDbNode(dbNode);
				tlu.setSchemaNode(schemaNode);
				tlu.setSchemaReader(schemaReader);
				tlu.setTableNode(tableNode);
				tlu.setViewNode(viewNode);
				Thread tableThread = new Thread(tlu);
				tableThread.start();
				threads.add(tableThread);

			}
			for (Thread thread : threads) {
				if (thread.isAlive()) {
					try {
						thread.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			List<ResultSet> resultSetList = new ArrayList<>();
			resultSetList.add(schemaResultset);
			releaseConnection(resultSetList, connection);
		}

		return schemaNode;
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;

	}

	public List<FieldMapping> getFieldMapping(String databaseName, String tableName, Connection connection) {
		List<FieldMapping> fieldMappings = new ArrayList<>();
		if (connection != null) {
			String primaryKey = null;
			ResultSet resultsetPk = null;
			ResultSet resultsetColumns = null;
			try {
				resultsetPk = connection.getMetaData().getPrimaryKeys(null, null, tableName);
				while (resultsetPk.next()) {
					primaryKey = resultsetPk.getString("COLUMN_NAME");
				}
				resultsetColumns = connection.getMetaData().getColumns(databaseName, null, tableName, "%");
				int i = 1;
				while (resultsetColumns.next()) {
					long fieldId = i;
					String fieldName = resultsetColumns.getString(4);
					FieldMapping fieldMapping = new FieldMapping();
					fieldMapping.setId(fieldId);
					fieldMapping.setFieldName(fieldName);
					if (fieldName.equals(primaryKey)) {
						fieldMapping.setIsPrimaryKey(true);
					}
					Integer columnType = resultsetColumns.getInt(5);

					Integer j = i - 1;
					Long pos = j.longValue();
					fieldMapping.setPosition(pos);
					if (columnType == Types.VARCHAR || columnType == Types.CHAR) {
						fieldMapping.setFieldDataType(FieldDataTypes.STRING);
					} else if (columnType == Types.INTEGER || columnType == Types.SMALLINT
							|| columnType == Types.TINYINT) {
						fieldMapping.setFieldDataType(FieldDataTypes.INTEGER);
					} else if (columnType == Types.BIGINT) {
						fieldMapping.setFieldDataType(FieldDataTypes.LONG);
					} else if (columnType == Types.DOUBLE || columnType == Types.FLOAT || columnType == Types.NUMERIC) {
						fieldMapping.setFieldDataType(FieldDataTypes.DOUBLE);
					} else if (columnType == Types.DATE) {
						fieldMapping.setFieldDataType(FieldDataTypes.DATE);
					} else if (columnType == Types.TIMESTAMP) {
						fieldMapping.setFieldDataType(FieldDataTypes.TIMESTAMP);
					} else if (columnType == Types.DECIMAL) {
						fieldMapping.setFieldDataType(FieldDataTypes.DECIMAL);
						/*
						 * ResultSetMetaData rmd = resultsetColumns.getMetaData();
						 * fieldMapping.setPrecision(rmd.getPrecision(i));
						 * fieldMapping.setScale(rmd.getScale(i));
						 */
					} else {
						fieldMapping.setFieldDataType(FieldDataTypes.STRING);
					}
					i++;
					fieldMappings.add(fieldMapping);
				}

			} catch (SQLException e) {

				e.printStackTrace();
			} finally {
				List<ResultSet> resultSetList = new ArrayList<>();
				resultSetList.add(resultsetPk);
				resultSetList.add(resultsetColumns);
				releaseResultSet(resultSetList);
			}
		}
		return fieldMappings;

	}

	public static Map<String, Integer> getSuitableFromats(FieldMapping field, FieldDataTypes type) {

		List<String> allPossibleFormats = new ArrayList<String>();
		List<String> sampleRecordList = field.getSampleRecords();
		Iterator<String> iterator = sampleRecordList.iterator();
		while (iterator.hasNext()) {
			allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(iterator.next(), type));
		}
		Map<String, Integer> formatCounter = new HashMap<String, Integer>();
		if (allPossibleFormats.size() > 0) {
			for (String format : allPossibleFormats) {
				if (!formatCounter.containsKey(format)) {
					formatCounter.put(format, 0);
				}
				formatCounter.put(format, formatCounter.get(format) + 1);
			}
		}

		return formatCounter;
	}

	public List<FieldMapping> addSampleData(List<FieldMapping> fieldMappings, Connection connection, String tableName,
			Integer sampleRows) throws SQLException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> addSampleData()" + ParamUtils.getString(fieldMappings));
		PreparedStatement preparedStatement = connection
				.prepareStatement(ConnectionLookUpDirectory.formulateQuery(tableName));
		preparedStatement.setMaxRows(sampleRows);
		preparedStatement.setFetchSize(sampleRows);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			int count = 0;
			int columnCount = resultSet.getMetaData().getColumnCount();

			for (int i = 1; i <= columnCount; i++) {

				String rowLevelColumnValue = null;
				try {
					rowLevelColumnValue = resultSet.getString(i);
					if (rowLevelColumnValue.contains("\"")) {
						rowLevelColumnValue = rowLevelColumnValue.replaceAll("\"", "");
					}
				} catch (Exception e) {
					rowLevelColumnValue = null;
				}
				FieldMapping fieldMapping = fieldMappings.get(i - 1);
				fieldMapping.getSampleRecords().add(count, rowLevelColumnValue);

			}

			count++;

		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << addSampleData()" + ParamUtils.getString(fieldMappings));

		releaseConnection(resultSet, connection);
		addDateFormat(fieldMappings);
		return fieldMappings;
	}

	private void addDateFormat(List<FieldMapping> fieldMappings) {

		if (fieldMappings != null && fieldMappings.size() > 0) {
			for (FieldMapping field : fieldMappings) {
				if (field.getFieldDataType() == FieldDataTypes.DATE) {
					// SampleRecordExtractor.checkDate(field);
					checkDate(field);
				} else if (field.getFieldDataType() == FieldDataTypes.TIMESTAMP) {
					// SampleRecordExtractor.checkTimeStamp(field);
					checkTimeStamp(field);
				}
			}
		}
	}

	public static Boolean checkDate(FieldMapping field) {

		Map<String, Integer> formatCounter = getSuitableFromats(field, FieldDataTypes.DATE);

		if (formatCounter.size() > 0) {
			String maxFormat = "";
			Integer max = 0;

			for (Entry<String, Integer> e : formatCounter.entrySet()) {
				String format = e.getKey();
				Integer counter = e.getValue();

				if (counter > max) {
					max = counter;
					maxFormat = format;
				}
			}

			if (StringUtils.isNotBlank(maxFormat)) {
				field.setFormat(maxFormat);
				field.setFieldDataType(FieldDataTypes.DATE);
				return true;
			}
		}

		if (field.getFieldDataType() == FieldDataTypes.DATE) {
			return true; // If header template says it is a date field, then it
			// is, even if there is no valid dates found
		}

		return false;
	}

	public static Boolean checkTimeStamp(FieldMapping field) {

		Map<String, Integer> formatCounter = getSuitableFromats(field, FieldDataTypes.TIMESTAMP);

		if (formatCounter.size() > 0) {
			String maxFormat = "";
			Integer max = 0;

			for (Entry<String, Integer> e : formatCounter.entrySet()) {
				String format = e.getKey();
				Integer counter = e.getValue();

				if (counter > max) {
					max = counter;
					maxFormat = format;
				}
			}

			if (StringUtils.isNotBlank(maxFormat)) {
				field.setFormat(maxFormat);
				field.setFieldDataType(FieldDataTypes.TIMESTAMP);
				return true;
			}
		}

		if (field.getFieldDataType() == FieldDataTypes.TIMESTAMP) {
			return true; // If header template says it is a TimeStamp field,
			// then it
			// is, even if there is no valid dates found
		}

		return false;
	}

	public Collection<Map<String, List<String>>> getDbWithTablesList(DataNode dataNode) {
		Collection<DataNode> databaseList = dataNode.getChildren();// All
		// databases
		// node list
		// will be
		// stored
		Collection<Map<String, List<String>>> dbWithTablesAsChild = new ArrayList<Map<String, List<String>>>();
		if (databaseList != null && !databaseList.isEmpty()) {
			for (DataNode currentDatabase : databaseList) {
				Map<String, List<String>> currentDbWithtbldetails = new HashMap<>();
				String DatabaseName = currentDatabase.getLabel();// current db
				Collection<DataNode> TablesAndViewsList = currentDatabase.getChildren();// list
				// of
				// tables
				// and
				// vies
				// in
				// that
				// db
				Iterator<DataNode> tblAndviewlistItr = TablesAndViewsList.iterator();
				List<String> tableNames = new ArrayList<>();
				while (tblAndviewlistItr.hasNext()) {
					Collection<DataNode> tableOrViewList = tblAndviewlistItr.next().getChildren();// list
					// of
					// table
					// or
					// view
					Iterator<DataNode> tbl0rviewItr = tableOrViewList.iterator();
					while (tbl0rviewItr.hasNext()) {
						DataNode currentTable = tbl0rviewItr.next();
						tableNames.add(currentTable.getLabel());
					}

				}
				currentDbWithtbldetails.put(DatabaseName, tableNames);
				dbWithTablesAsChild.add(currentDbWithtbldetails);
			}

		}
		return dbWithTablesAsChild;
	}

	/**
	 * @author anuj
	 * @param object
	 *            - which needs to be parsed
	 * @param entity
	 *            - type in which object needs to be parsed
	 * @return parsed object
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 *             generic method to parse any object to its corresponding type
	 *             usually helpful to convert object received from ui to its
	 *             corresponding pojo type
	 */

	public static <T> T objectParserHandler(Object object, Class<T> entity)
			throws JsonParseException, JsonMappingException, IOException {
		String jsonString = new Gson().toJson(object);
		ObjectMapper mapper = new ObjectMapper();
		T obj = mapper.readValue(jsonString, entity);
		return obj;

	}

	@Override
	public String testConnection(Map<String, Object> args) {
		Connection connection = null;
		ConnectionConfig connectionConfig = (ConnectionConfig) args.get("connectionConfig");
		this.subSourceType = args.get("subSourceType").toString();
		String message = TaskStatusEnums.FAIL.name();
		try {
			connection = establishRdbmsConnection(connectionConfig);
		} catch (ClassNotFoundException | SQLException | SystemException e) {
			return message;
		}
		if (connection != null) {
			message = TaskStatusEnums.SUCCESS.name();
			releaseConnection(connection);
			return message;
		}
		return message;
	}

	@Override
	public List<List<Object>> runDataSourceQuery(DataSource dataSource, int limit)
			throws ClassNotFoundException, SQLException, SystemException {
		logger.debug("Mosaic-Catalogue-Connectors: ClassName : RDBMSScanner MethodName : runDataSourceQuery : ");
		List<List<Object>> tableData = new ArrayList<>();
		if (dataSource != null) {
			ConnectionSources connectionSources = dataSource.getConnectionSources();
			ConnectionSubSources connectionSubSources = connectionSources.getConnectionSubSources().get(0);
			ConnectionConfig connectionConfig = connectionSubSources.getConnectionConfig().get(0);

			Connection connection = null;
			String databaseName = connectionConfig.getDbName();
			this.subSourceType = StringUtils.upperCase(connectionSubSources.getSubConnectionType());
			if (subSourceType.equals(ConnectorType.ORACLE.name())) {
				connection = establishRdbmsConnection(connectionConfig);

			} else {
				connection = establishRdbmsConnection(connectionConfig, databaseName);
			}
			String tableName = dataSource.getDataSourceName();
			ResultSet resultsetColumns = connection.getMetaData().getColumns(databaseName, null, tableName,
					CHAR_CONSTANTS.PERCENTAGE);
			List<Object> columnName = new ArrayList<>();
			while (resultsetColumns.next()) {
				columnName.add(resultsetColumns.getString(QueryConstants.DatasourceServiceApimanager.COLUMN_NAME));
			}
			tableData.add(columnName);
			// Get table data
			PreparedStatement preparedStatement = connection
					.prepareStatement(ConnectionLookUpDirectory.formulateQuery(tableName));
			preparedStatement.setMaxRows(limit);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				int count = 0;
				int columnCount = resultSet.getMetaData().getColumnCount();
				List<Object> rowData = new ArrayList<>();
				for (int i = 1; i <= columnCount; i++) {
					String rowLevelColumnValue = null;
					try {
						rowLevelColumnValue = resultSet.getString(i);
						if (rowLevelColumnValue.contains("\"")) {
							rowLevelColumnValue = rowLevelColumnValue.replaceAll("\"", "");
						}
					} catch (Exception e) {
						rowLevelColumnValue = null;
					}
					rowData.add(rowLevelColumnValue);
				}
				tableData.add(rowData);
			}
		}
		logger.debug("Mosaic-Catalogue-Connectors: ClassName : RDBMSScanner MethodName : runDataSourceQuery : End here");
		return tableData;
	}
}
package com.augiq.external.source.rdbms.schema.resolver.reader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface SchemaReader {

    public ResultSet listAllSchemaNames(Connection connection) throws SQLException;

    public String getCurrentSchemaName(ResultSet resultSet) throws SQLException;

    public ResultSet listAllTableNames(Connection connection, String databaseName) throws SQLException;
    // testing
    // public List<String> listAllSystemTables(Connection connection,String
    // databaseName) throws SQLException;
}

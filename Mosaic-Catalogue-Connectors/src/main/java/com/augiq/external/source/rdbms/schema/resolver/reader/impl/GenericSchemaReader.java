package com.augiq.external.source.rdbms.schema.resolver.reader.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

public class GenericSchemaReader implements com.augiq.external.source.rdbms.schema.resolver.reader.SchemaReader {

    public ResultSet listAllSchemaNames(Connection connection) throws SQLException {
	return connection.getMetaData().getSchemas();
    }

    public String getCurrentSchemaName(ResultSet resultSet) throws SQLException {
	return resultSet.getString(1);
    }

    public ResultSet listAllTableNames(Connection connection, String databaseName) throws SQLException {
	String[] types = { "TABLE", "VIEW" };
	return connection.getMetaData().getTables(null, databaseName, "%", types);
    }

    /*
     * @Override public List<String> listAllSystemTables(Connection connection,
     * String databaseName) throws SQLException { String[] types = {
     * "SYSTEM TABLE" }; List<String> sysTable = new ArrayList<String>();
     * ResultSet resultSet = connection.getMetaData().getTables(null,
     * databaseName, "%", types); while(resultSet.next()){
     * sysTable.add(resultSet.getString(3));
     * 
     * } return sysTable; }
     */

}

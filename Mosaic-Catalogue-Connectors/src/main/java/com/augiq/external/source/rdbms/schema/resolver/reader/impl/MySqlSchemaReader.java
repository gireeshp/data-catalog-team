package com.augiq.external.source.rdbms.schema.resolver.reader.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlSchemaReader implements com.augiq.external.source.rdbms.schema.resolver.reader.SchemaReader {

    public ResultSet listAllSchemaNames(Connection connection) throws SQLException {
	return connection.getMetaData().getCatalogs();

    }

    public String getCurrentSchemaName(ResultSet resultSet) throws SQLException {
	return resultSet.getString("TABLE_CAT");

    }

    public ResultSet listAllTableNames(Connection connection, String databaseName) throws SQLException {
	String[] types = { "TABLE", "VIEW" };
	return connection.getMetaData().getTables(databaseName, null, "%", types);

    }

    /*
     * @Override public List<String> listAllSystemTables(Connection connection,
     * String databaseName) throws SQLException { String[] types = {
     * "SYSTEM TABLE" }; List<String> sysTable = new ArrayList<String>();
     * ResultSet resultSet = connection.getMetaData().getTables(databaseName,
     * null, "%", types); while(resultSet.next()){
     * sysTable.add(resultSet.getString(3));
     * 
     * } return sysTable; }
     */

}

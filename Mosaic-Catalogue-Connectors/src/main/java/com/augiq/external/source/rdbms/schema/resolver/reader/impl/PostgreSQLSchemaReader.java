package com.augiq.external.source.rdbms.schema.resolver.reader.impl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.util.external.source.rdbms.connection.lookup.directory.ConnectionLookUpDirectory;

public class PostgreSQLSchemaReader implements com.augiq.external.source.rdbms.schema.resolver.reader.SchemaReader{

	@Override
	public ResultSet listAllSchemaNames(Connection connection) throws SQLException {
		ResultSet resultSet = null;
		try {
            java.sql.PreparedStatement ps = connection.prepareStatement("SELECT datname FROM pg_database WHERE datistemplate = false;");
             resultSet = ps.executeQuery();
           /*  while (resultSet.next()) {
            	System.out.println(resultSet.getString(1));
            	if (!"postgres".equals(resultSet.getString(1))) {
            		Connection con = establishPostgreSQLConnectionDB(resultSet.getString(1));
                	DatabaseMetaData md = con.getMetaData();
                	 ResultSet rs = md.getTables(null, null, "%", new String[] { "TABLE","VIEW"});
                	    while (rs.next()) {
                	      System.out.println(rs.getString("TABLE_NAME"));
                	    }
                	    rs = null;
                	    
    			}
				}*/
            	
             
        } catch (Exception e) {
            e.printStackTrace();
        }
		return resultSet;
	}

	@Override
	public String getCurrentSchemaName(ResultSet resultSet) throws SQLException {
		return resultSet.getString(1);
	}

	@Override
	public ResultSet listAllTableNames(Connection connection, String databaseName) throws SQLException {
		String[] types = { "TABLE", "VIEW" };
		return connection.getMetaData().getTables(null, null, "%", types);
	}
	
	 public Connection establishRdbmsConnection() throws SQLException, ClassNotFoundException, SystemException {
			String userName = "maxiq";
			String password = "maxiq";
			Integer port = 5432;
			String sid = null;
			String host = "dataconnector.canadacentral.cloudapp.azure.com";
			String druverUrl = ConnectionLookUpDirectory.generateRdbmsConnectUrl("POSTGRES", host, port, sid);
			String driverClass = ConnectionLookUpDirectory.lookupRdbmsDriverClass("POSTGRES");
			Class.forName(driverClass);

			Connection connection = DriverManager.getConnection(druverUrl, userName, password);
			return connection;
	 }
	 
	 public Connection establishPostgreSQLConnectionDB(String dbName) throws Exception {
		 String userName = "maxiq";
			String password = "maxiq";
			Integer port = 5432;
			String sid = null;
			String host = "dataconnector.canadacentral.cloudapp.azure.com";
			String druverUrl = "jdbc:postgresql://" + host + ":" + port+"/"+dbName;
			String driverClass = ConnectionLookUpDirectory.lookupRdbmsDriverClass("POSTGRES");
			Class.forName(driverClass);

			Connection connection = DriverManager.getConnection(druverUrl, userName, password);
			return connection;
	 }
	 
	 public static void main(String[] args) throws Exception {
		
		 PostgreSQLSchemaReader schema = new PostgreSQLSchemaReader();
		 Connection connection = schema.establishRdbmsConnection();
		 schema.listAllSchemaNames(connection);
	}

}

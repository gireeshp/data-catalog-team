package com.augiq.external.source.scanning;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;

public interface Scanner {

    public DataNode scan(Map<String, Object> args) throws Exception;

    public Map<String,List<String>> publishDataSources(Map<String, Object> args) throws Exception;

    public String testConnection(Map<String, Object> args);
    
   public List<List<Object>>  runDataSourceQuery(DataSource dataSource, int limit) throws Exception; 
}

package com.augmentiq.maxiq.external.source.rdbms.scanner.utility;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.augiq.external.source.rdbms.schema.resolver.reader.SchemaReader;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;

public class TableListingUtil implements Runnable {

    private String dbName;
    private DataNode dbNode;
    private DataNode tableNode;
    private DataNode viewNode;
    private SchemaReader schemaReader;
    private ResultSet tableResultset = null;
    private Connection connection;
    private DataNode schemaNode;

    public String getDbName() {
	return dbName;
    }

    public void setDbName(String dbName) {
	this.dbName = dbName;
    }

    public DataNode getDbNode() {
	return dbNode;
    }

    public void setDbNode(DataNode dbNode) {
	this.dbNode = dbNode;
    }

    public DataNode getTableNode() {
	return tableNode;
    }

    public void setTableNode(DataNode tableNode) {
	this.tableNode = tableNode;
    }

    public DataNode getViewNode() {
	return viewNode;
    }

    public void setViewNode(DataNode viewNode) {
	this.viewNode = viewNode;
    }

    public SchemaReader getSchemaReader() {
	return schemaReader;
    }

    public void setSchemaReader(SchemaReader schemaReader) {
	this.schemaReader = schemaReader;
    }

    public Connection getConnection() {
	return connection;
    }

    public void setConnection(Connection connection) {
	this.connection = connection;
    }

    public DataNode getSchemaNode() {
	return schemaNode;
    }

    public void setSchemaNode(DataNode schemaNode) {
	this.schemaNode = schemaNode;
    }

    @Override
    public void run() {
	try {
	    listAllTables();
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} finally {
	    releaseResultSet(tableResultset);
	}
    }

    public void listAllTables() throws SQLException {
	tableResultset = schemaReader.listAllTableNames(connection, dbName);
	String tableName = "";
	while (tableResultset.next()) {
	    tableName = tableResultset.getString(3);

	    DataNode table = new DataNode(tableName, true);
	    if (tableResultset.getString(4).equals("VIEW")) {
		viewNode.getChildren().add(table);
	    } else {
		tableNode.getChildren().add(table);
	    }

	}
	dbNode.getChildren().add(tableNode);
	dbNode.getChildren().add(viewNode);
	schemaNode.addChild(dbNode);
    }

    private void releaseResultSet(ResultSet resultSet) {

	if (resultSet != null) {
	    try {
		resultSet.close();
	    } catch (SQLException e) {
		e.printStackTrace();
	    }
	}

    }

}

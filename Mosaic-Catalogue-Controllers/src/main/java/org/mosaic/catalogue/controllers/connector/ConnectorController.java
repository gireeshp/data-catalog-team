package org.mosaic.catalogue.controllers.connector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;
import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.services.external.connection.ConnectorListingBSO;
import com.google.gson.Gson;

@Controller
public class ConnectorController extends AjaxErrorHandler {

    @Autowired
    private ConnectorListingBSO connectorListingBSO;

    public ConnectorListingBSO getConnectorListingBSO() {
	return connectorListingBSO;
    }

    public void setConnectorListingBSO(ConnectorListingBSO connectorListingBSO) {
	this.connectorListingBSO = connectorListingBSO;
	System.out.println("connectorListingBSO initialized");
    }

    @RequestMapping(value = "/listConnectionSource", method = { RequestMethod.POST })
    public @ResponseBody List<ConnectionSources> listConnectionSource() throws Exception {

	List<ConnectionSources> connectionSourcesList = connectorListingBSO.listConnectionSource();
	System.out.println(new Gson().toJson(connectionSourcesList));
	return connectionSourcesList;
    }

    @RequestMapping(value = "/listConnectionSourceAndSubSource", method = { RequestMethod.POST })
    public @ResponseBody ConnectionSources listConnectionSourceAndSubSource(@RequestParam String sourceId) throws Exception {

	ConnectionSources connectionSources = connectorListingBSO.listConnectionSourceAndSubSource(Long.valueOf(sourceId));
	return connectionSources;
    }

    @RequestMapping(value = "/listAllSubSourceConnection", method = { RequestMethod.POST })
    public @ResponseBody ConnectionSources listSubSourceConnections(@RequestBody Map<String, Object> reqParam) throws Exception {

	ConnectionSources connectionSources = connectorListingBSO.listSubSourceConnections(reqParam);
	return connectionSources;
    }

    @RequestMapping(value = "/saveNewSubSourceConnection", method = { RequestMethod.POST })
    public @ResponseBody Map<String, Object> saveNewSubSourceConnection(@RequestBody Map<String, Object> reqParam, @CookieValue("userId") String userId) throws Exception {
	Map<String, Object> connectionInfo = new HashMap<>();
	try {
	    connectionInfo = connectorListingBSO.saveNewSubSourceConnection(reqParam, userId);
	    return connectionInfo;
	} catch (ConstraintViolationException ex) {
	    String connectionName =  reqParam.get("connectionName").toString();
	    connectionInfo.put(TaskStatusEnums.FAIL.name(), "Connection with name " + connectionName + " already exists");
	    return connectionInfo;
	}
    }

    @RequestMapping(value = "/testConnection", method = { RequestMethod.POST })
    public @ResponseBody String testConnection(@RequestBody Map<String, Object> reqParam) throws Exception {

	return connectorListingBSO.testConnection(reqParam);

    }

    @RequestMapping(value = "/deleteExistingSubSourceConnection", method = { RequestMethod.POST })
    public @ResponseBody Map<String, Object> deleteExistingSubSourceConnection(@RequestBody Map<String, Object> reqParam, @CookieValue("userId") String userId) throws Exception {

	connectorListingBSO.deleteExistingSubSourceConnection(reqParam);
	return null;
    }

    /*
     * @RequestMapping(value = "/listAllSchema",method = {RequestMethod.POST} )
     * public @ResponseBody Map<String, Object> listAllSchema(
     * 
     * @RequestBody Map<String,Object> reqParam) throws Exception {
     * 
     * Map<String, Object> nodeWithConnectionInfo =
     * connectorListingBSO.listAllSchema(reqParam); return
     * nodeWithConnectionInfo;
     * 
     * }
     */

    @RequestMapping(value = "/listAllSchema", method = { RequestMethod.POST })
    public @ResponseBody Map<String, Object> listAllSchema(@RequestBody Map<String, Object> reqParam) throws Exception {

	Map<String, Object> nodeWithConnectionInfo = connectorListingBSO.listAllSchema(reqParam);
	return nodeWithConnectionInfo;

    }

    @RequestMapping(value = "/publishDataSources", method = { RequestMethod.POST })
    public @ResponseBody Map<String,List<String>>  publishDataSources(@RequestBody Map<String, Object> reqData, @CookieValue("userId") String userId, @CookieValue("groupId") Long groupId) throws Exception {

	return connectorListingBSO.publishDataSources(reqData, userId, groupId);
	

    }
    
    @RequestMapping(value = "/checkDuplicateDSNames", method = { RequestMethod.POST })
    public @ResponseBody Map<String,List<String>> checkDuplicateDSNames(@RequestBody List<DataNode> dataNodes)
    {
    	return connectorListingBSO.checkDuplicateDSNames(dataNodes);
    }
    

}

package org.mosaic.catalogue.controllers.datasource.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.RandomUtility;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstanceDTO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DatasourceServiceApimanager;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.services.ds.apiexposure.DataSourceExposeBSO;
import com.augmentiq.maxiq.services.external.connection.ConnectorListingBSO;
import com.augmentiq.maxiq.util.app.context.MosaicAppContext;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

@Controller
public class DataSourceExposeApIController {
	@Autowired
	private MosaicAppContext mosaicAppContext;
	private static final Logger logger = LoggerFactory.getLogger(DataSourceExposeApIController.class);

	@RequestMapping(value = "/fetchDataSourceForAPI", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody static Collection<DataSourceInstanceDTO> fetchDataSourceInCurrentProjects(
			@CookieValue("userId") String userId)
			throws SystemException, ClassNotFoundException, IOException, ConnectionException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceForAPI() " + ParamUtils.getString(userId));

		List<DataSourceInstanceDTO> domains = DataSourceInstanceDao
				.getDataSourceInstancesFromProjectIdGroupIdAndUserId(Long.valueOf(userId));
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceForAPI() " + ParamUtils.getString(domains));
		return domains;
	}

	// status = 0 = inactive
	// deleted = 1 = deleted
	@RequestMapping(value = "/fetchDataSourceDeployedStatus", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody static DatasourceServiceApimanager fetchDataSourceDeployedStatus(
			@RequestParam Long dataSourceId, @RequestParam String dataSourceName, @CookieValue("userId") String userId)
			throws SystemException, ClassNotFoundException, IOException, ConnectionException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceDeployedStatus() "
				+ ParamUtils.getString(dataSourceId, dataSourceName, userId));
		DatasourceServiceApimanager datasourceServiceApimanager = DataSourceExposeBSO
				.fetchDataSourceApiDetails(Long.parseLong(userId), dataSourceId, dataSourceName);
		if (datasourceServiceApimanager == null) {
			datasourceServiceApimanager = new DatasourceServiceApimanager();
			datasourceServiceApimanager.setUserId(Long.valueOf(userId));
			datasourceServiceApimanager.setStatus(0);
			datasourceServiceApimanager.setDataSourceId(dataSourceId);
			datasourceServiceApimanager.setDataSourceName(dataSourceName);
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceDeployedStatus() "
				+ ParamUtils.getString(datasourceServiceApimanager));

		return datasourceServiceApimanager;
	}

	/*
	 * 0 = inactive status 1 = active status 1 = not deleted(viz running) 0 =
	 * deleted
	 */
	@RequestMapping(value = "/toggleDeployUndeploy", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody static DatasourceServiceApimanager deployUndeployToggleAPI(
			@RequestBody DatasourceServiceApimanager dataSourceMngr, @CookieValue("userId") String userId)
			throws SystemException, ClassNotFoundException, IOException, ConnectionException {

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deployUndeployToggleAPI() "
				+ ParamUtils.getString(dataSourceMngr, userId));

		DatasourceServiceApimanager datasourceServiceApimanager = DataSourceExposeBSO.fetchDataSourceApiDetails(
				Long.parseLong(userId), dataSourceMngr.getDataSourceId(), dataSourceMngr.getDataSourceName());
		if (dataSourceMngr.getStatus() == 1) {
			if (datasourceServiceApimanager == null) {
				datasourceServiceApimanager = new DatasourceServiceApimanager();
				datasourceServiceApimanager.setUserId(Long.valueOf(userId));
				datasourceServiceApimanager.setDataSourceName(dataSourceMngr.getDataSourceName());
				datasourceServiceApimanager.setDataSourceId(dataSourceMngr.getDataSourceId());
				datasourceServiceApimanager.setStatus(1);
				datasourceServiceApimanager.setCreatedDate(System.currentTimeMillis());
				datasourceServiceApimanager.setDeleted(1);
				datasourceServiceApimanager.setApiKey(RandomUtility.getRandomString(40));
				datasourceServiceApimanager.setQuery(dataSourceMngr.getQuery());
				datasourceServiceApimanager.setId(DataSourceExposeBSO.persistApiInfo(datasourceServiceApimanager));
			} else {
				datasourceServiceApimanager.setStatus(dataSourceMngr.getStatus());
				datasourceServiceApimanager.setModifiedDate(System.currentTimeMillis());
				datasourceServiceApimanager.setQuery(dataSourceMngr.getQuery());
				DataSourceExposeBSO.updateApiInfo(datasourceServiceApimanager);
			}
		} else {
			if (datasourceServiceApimanager != null) {
				datasourceServiceApimanager.setStatus(0);
				datasourceServiceApimanager.setQuery(dataSourceMngr.getQuery());
				datasourceServiceApimanager.setModifiedDate(System.currentTimeMillis());
				DataSourceExposeBSO.updateApiInfo(datasourceServiceApimanager);
			}
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deployUndeployToggleAPI() "
				+ ParamUtils.getString(datasourceServiceApimanager));

		return datasourceServiceApimanager;
	}

	@RequestMapping(value = "/getDeployedDS", method = { RequestMethod.GET, RequestMethod.POST })
	public static @ResponseBody List<Map<String, Object>> getDeployedDS(@CookieValue("userId") String userId)
			throws Exception {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getDeployedDS {}", userId);
		return DataSourceExposeBSO.getDeployedDS(userId);
	}

	@RequestMapping(value = "/runDataSourceExposeQueryApI", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody List<List<Object>> runDataSourceExposeQueryApI(
			@RequestBody(required = false) Map<String, Object> queryDetails,
			@RequestParam(value = "id", required = false, defaultValue = "0") String unqId,
			@RequestParam(value = "apiKey", required = false, defaultValue = "0") String apiKey,
			@CookieValue(value = "groupId", required = false, defaultValue = "0") Long groupId) throws Exception {

		String unqUserId = null;
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> runHiveQueryApi() "
				+ ParamUtils.getString(queryDetails, unqId, apiKey));
		queryDetails = (queryDetails != null ? queryDetails : new HashMap<String, Object>());
		Long apiUnqId = Long.parseLong(unqId);
		if (apiKey.equals(0) && apiUnqId == 0) {
			throw new Exception("Api is not accessible");
		}
		DatasourceServiceApimanager datasourceServiceApimanager = DataSourceExposeBSO
				.fectApiDetailsByApiKeyAndApiId(apiUnqId, apiKey);
		if (datasourceServiceApimanager == null) {
			throw new Exception("Api is not accessible");
		}
		if (datasourceServiceApimanager != null && datasourceServiceApimanager.getStatus() == 0) {

			throw new Exception("Api is not accessible");
		}
		if (queryDetails.containsKey(QueryConstants.DatasourceServiceApimanager.QUERY) == false) {
			queryDetails.put((QueryConstants.DatasourceServiceApimanager.QUERY),
					datasourceServiceApimanager.getQuery());
		}
		String query = ((String) queryDetails.get((QueryConstants.DatasourceServiceApimanager.QUERY))).trim();
		int limit = Integer
				.valueOf(query != null && !query.isEmpty() ? query.substring(query.lastIndexOf("limit") + 6) : "");

		Long dsId = datasourceServiceApimanager.getDataSourceId();

		// DataSource dataSource =
		// DataRepositoryFetchWithChild.getDataSourceByName(dataSourceName);
		DataSource dataSource = DataRepositoryDao.getDataSource(dsId);

		ConnectionConfig connectorConfig = null;
		String connectionType = null;
		String connectionName = null;
		ConnectionSources connectionSources = null;
		if (dataSource != null && dataSource.getConfig() != null) {
			connectorConfig = dataSource.getConfig();
			connectionName = connectorConfig.getConncetionName();
			connectionSources = dataSource.getConnectionSources();
			connectionType = connectionSources.getConnectionType();
			if (!connectionType.equals("RDBMS"))
				connectionType = connectionSources.getConnectionSubSources() != null
						? connectionSources.getConnectionSubSources().get(0).getSubConnectionType()
						: "";

		}

		ApplicationContext applicationContext = mosaicAppContext.getContext();
		Scanner scanner = (Scanner) applicationContext.getBean(connectionType);
		List<List<Object>> response = scanner.runDataSourceQuery(dataSource, limit);

		// List<FieldMapping> data1 = dataSource.getFieldMappings();
		// for (FieldMapping fieldMapping : data1) {
		// List<Object> objectList = new
		// ArrayList<Object>(fieldMapping.getSampleRecords());
		// response.add(objectList);
		// }
		return response;
	}

	@RequestMapping(value = "/getMetaDataOfModel", method = { RequestMethod.GET, RequestMethod.POST })
	public static @ResponseBody Map<String, String> getMetaDataOfModel() throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("apiUrl", Cache.getProperty(CacheConstants.MODELS_API_URL));
		return map;
	}

	/*
	 * @RequestMapping( value = "/runHiveQueryApi", method = {RequestMethod.GET,
	 * RequestMethod.POST} ) public @ResponseBody List<List<Object>>
	 * runHiveQueryApi(
	 * 
	 * @RequestBody Map<String, Object> queryDetails,
	 * 
	 * @RequestParam(value = "id", required = false, defaultValue = "0") String
	 * unqId,
	 * 
	 * @RequestParam(value = "apiKey", required = false, defaultValue = "0") String
	 * apiKey,
	 * 
	 * @CookieValue(value = "groupId", required = false, defaultValue = "0") Long
	 * groupId) throws Exception { String unqUserId = null; logger.debug(
	 * LoggerConstants.LOG_MAXIQWEB + " >> runHiveQueryApi() " +
	 * ParamUtils.getString(queryDetails, unqId, apiKey));
	 * 
	 * 
	 * if(!DataSourceExposeBSO.isValidateUser(emailId, password,1L,unqUserId)){
	 * throw new Exception("Please enter valid credentials"); }
	 * 
	 * Long apiUnqId = Long.parseLong(unqId); if (apiKey.equals(0) && apiUnqId == 0)
	 * { throw new Exception("Api is not accessible"); } DatasourceServiceApimanager
	 * datasourceServiceApimanager =
	 * DataSourceExposeBSO.fectApiDetailsByApiKeyAndApiId(apiUnqId, apiKey); if
	 * (datasourceServiceApimanager == null) { throw new
	 * Exception("Api is not accessible"); } if (datasourceServiceApimanager != null
	 * && datasourceServiceApimanager.getDeleted() == 0) {
	 * 
	 * throw new Exception("Api is not accessible"); }
	 * 
	 * groupId = (groupId != null) ? groupId : 0;
	 * 
	 * List<List<Object>> fetchDataHive = new ArrayList<>();
	 * queryDetails.put(GENERAL_CONSTANTS.HIVE_QUERY_GROUP_ID, groupId);
	 * queryDetails.put( GENERAL_CONSTANTS.KILL_JOB_USER_ID,
	 * datasourceServiceApimanager.getUserId().toString());
	 * queryDetails.put(GENERAL_CONSTANTS.PROJECTID, "1");
	 * queryDetails.put(GENERAL_CONSTANTS.HIVE_QUERY_IS_HBASE, false);
	 * 
	 * String jobName = SetupJobInstance.createJobNameForHiveQuery(
	 * StringUtils.trim(SetupJobInstance.EXPLORE_QUERY), UUIDGenerator.getUuid(),
	 * unqUserId); queryDetails.put(GENERAL_CONSTANTS.JOB_NAME, jobName); String
	 * substring = StringUtils.substring( jobName,
	 * jobName.indexOf(CHAR_CONSTANTS.HYPHEN) + 1, jobName.length()); String id =
	 * StringUtils.substring( substring, substring.indexOf(CHAR_CONSTANTS.HYPHEN) +
	 * 1, substring.length()); CommunicationDetails communicationDetails = new
	 * CommunicationDetails(); communicationDetails.setUuid(id);
	 * communicationDetails.setField1(queryDetails);
	 * 
	 * CommunicationDetailsBSO.saveCommunicationDetails(communicationDetails);
	 * 
	 * MessageHandler.setMessage(id, false); MessageHandler.executeForOthers( id,
	 * ObjectSerializationHandler.toString(""), MessageTypes.RUN_QUERY);
	 * 
	 * communicationDetails =
	 * CommunicationDetailsBSO.fetchCommunicationsDetailsByuuId(id);
	 * 
	 * MessageHandler.removeServerCacheDataOnly(id);
	 * 
	 * List<Object> result = (List<Object>) communicationDetails.getField2();
	 * 
	 * logger.info("Result is " + result); String error = null !=
	 * communicationDetails.getField3() ?
	 * communicationDetails.getField3().toString() : "";
	 * 
	 * if (StringUtils.isNotBlank(error) && !StringUtils.equalsIgnoreCase(error,
	 * "null")) { throw new SystemException(error); }
	 * 
	 * for (Object row : result) { List<Object> child = (List<Object>) row;
	 * fetchDataHive.add(child); }
	 * 
	 * return fetchDataHive; }
	 * 
	 * @RequestMapping( value = "/getDeployedDS", method = {RequestMethod.GET,
	 * RequestMethod.POST} ) public static @ResponseBody List<Map<String, Object>>
	 * getDeployedDS(
	 * 
	 * @CookieValue("userId") String userId) throws Exception {
	 * logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getDeployedDS {}", userId);
	 * return DataSourceExposeBSO.getDeployedDS(userId); }
	 * 
	 * @RequestMapping( value = "/getDeployedDSApiByDatasourceId", method =
	 * {RequestMethod.GET, RequestMethod.POST} ) public static @ResponseBody
	 * List<Map<String, Object>> getDeployedDSApiByDatasourceId(
	 * 
	 * @CookieValue("userId") String userId, @RequestParam Long dataSourceId) throws
	 * Exception { logger.debug( LoggerConstants.LOG_MAXIQWEB +
	 * ": >> getDeployedDSApiByDatasourceId {} {}", userId, dataSourceId); return
	 * DataSourceExposeBSO.getDeployedDSApiByDatasourceId(userId, dataSourceId); }
	 * 
	 * @RequestMapping( value = "/getDSExposeApiMetaData", method =
	 * {RequestMethod.GET, RequestMethod.POST} ) public static @ResponseBody
	 * Map<String, String> getDSExposeApiMetaData() throws Exception {
	 * logger.debug(LoggerConstants.LOG_MAXIQWEB +
	 * ": >> getDeployedDSApiByDatasourceId"); return
	 * DataSourceExposeBSO.getDSExposeApiMetaData(); }
	 */
}

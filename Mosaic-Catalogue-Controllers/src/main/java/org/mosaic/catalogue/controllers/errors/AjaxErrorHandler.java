package org.mosaic.catalogue.controllers.errors;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;

@ControllerAdvice
public class AjaxErrorHandler {
    Map<String, Logger> loggerMap = new HashMap<String, Logger>();
    private static final Logger loggerClass = LoggerFactory.getLogger(AjaxErrorHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<String> handleException(Exception e) throws ClassNotFoundException {

	loggerClass.debug(LoggerConstants.LOG_MAXIQWEB + " >>  handleException() " + ParamUtils.getString(e));

	String className = null;
	for (StackTraceElement stackTraceElement : e.getStackTrace()) {
	    className = stackTraceElement.getClassName();

	    // for identification of controller name in stacktrace
	    if (className.contains(".controller.") && !className.contains(".AjaxErrorHandler.")) {

		if (!loggerMap.containsKey(className)) {
		    loggerMap.put(className, LoggerFactory.getLogger(Class.forName(className)));
		}

		Logger logger = loggerMap.get(className);

		StringWriter stringWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stringWriter));
		logger.error(LoggerConstants.LOG_MAXIQWEB + stringWriter.toString());
	    }
	}
	loggerClass.debug(LoggerConstants.LOG_MAXIQWEB + " <<  handleException() " + ParamUtils.getString(new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR)));
	return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

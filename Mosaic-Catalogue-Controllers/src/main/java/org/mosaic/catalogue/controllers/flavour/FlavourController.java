package org.mosaic.catalogue.controllers.flavour;

import java.util.List;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.services.flavour.FlavourService;

@Controller
public class FlavourController extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(FlavourController.class);

    /**
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getPersonas", method = { RequestMethod.POST })
    public @ResponseBody List<String> getPersonas(@CookieValue("userId") String userId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getPersonas():userId" + userId);
	List<String> personasList = FlavourService.getPersonas(userId);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getPersonas():personasList" + personasList);
	return personasList;
    }

    /**
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getActions", method = { RequestMethod.POST })
    public @ResponseBody List<String> getActions(@CookieValue("userId") String userId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getActions():userId" + userId);
	List<String> actions = FlavourService.getActions(userId);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getActions():actions" + actions);
	return actions;
    }

    /**
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getBlockedUrlList", method = { RequestMethod.POST })
    public @ResponseBody List<String> getBlockedUrls(@CookieValue("userId") String userId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getBlockedUrlList():userId" + userId);
	List<String> urlList = FlavourService.getBlockedUrlList(userId);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getBlockedUrlList():urlList" + urlList);
	return urlList;
    }
}

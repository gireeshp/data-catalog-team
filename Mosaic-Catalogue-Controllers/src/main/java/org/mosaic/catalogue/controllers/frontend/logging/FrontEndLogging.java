package org.mosaic.catalogue.controllers.frontend.logging;

import javax.servlet.http.HttpServletRequest;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FrontEndLogging extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(FrontEndLogging.class);

    @RequestMapping("/logRequest")
    public void getLogged(HttpServletRequest httpServletRequest) {
    }
}

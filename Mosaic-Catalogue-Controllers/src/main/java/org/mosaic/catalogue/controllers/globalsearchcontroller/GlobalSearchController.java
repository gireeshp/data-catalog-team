package org.mosaic.catalogue.controllers.globalsearchcontroller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.StackTraceReader;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchResult;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchWithObjectInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.ObjectRequestToApproveDTO;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchWithObjectInstance.Item;
import com.augmentiq.maxiq.model.globalsearch.GlobalSearchInput;
import com.augmentiq.maxiq.services.mycollection.MyCollectionBSO;
import com.augmentiq.maxiq.services.object.request.ObjectRequestBSO;
import com.augmentiq.maxiq.services.setup.bso.UserManagementBso;
import com.augmentiq.maxiq.util.globalsearch.factory.GlobalSearchFactory;

@Controller
public class GlobalSearchController extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalSearchController.class);

    @RequestMapping(value = "/getCreatedByList", method = { RequestMethod.POST })
    public @ResponseBody List<String> globalSearch(@CookieValue("groupId") Long groupId) {
	List<String> list1 = UserManagementBso.listOfUsers(groupId);
	return list1;
    }

    @RequestMapping(value = "/getSearchResult", method = { RequestMethod.POST })
    public @ResponseBody GlobalSearchWithObjectInstance globalSearchData(@CookieValue("groupId") Long groupId, @RequestBody GlobalSearchInput globalSearchInput, @CookieValue("userId") String userId) throws Exception {
	globalSearchInput.setGroupBy(groupId);
	if (globalSearchInput.getCreatedBy() != null && globalSearchInput.getCreatedBy().size() == 0) {
	    globalSearchInput.setCreatedBy(null);
	}
	globalSearchInput.setGroupBy(null);
	GlobalSearchResult searchForGivenInput = GlobalSearchFactory.searchForGivenInput(globalSearchInput);
	return ObjectRequestBSO.getDataSourcesFromGlobalSearchResult(searchForGivenInput, userId, groupId);
    }

    @RequestMapping(value = "/getSearchResultForDiscovery", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody GlobalSearchWithObjectInstance getSearchResultForDiscovery(@CookieValue("groupId") Long groupId, @CookieValue("userId") String userId, @RequestBody GlobalSearchInput globalSearchInput) throws Exception {
	globalSearchInput.setGroupBy(null);
	if (globalSearchInput.getCreatedBy() != null && globalSearchInput.getCreatedBy().size() == 0) {
	    globalSearchInput.setCreatedBy(null);
	}

	if (globalSearchInput.getCategory() == null) {
	    globalSearchInput.setSubCategory(null);
	}

	GlobalSearchResult searchForGivenInput = GlobalSearchFactory.searchForGivenInputForDiscovery(globalSearchInput);
	return ObjectRequestBSO.getDataSourcesFromGlobalSearchResult(searchForGivenInput, userId, groupId);
    }

    @RequestMapping(value = "/getSearchCollectionResult", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody GlobalSearchWithObjectInstance getSearchCollectionResult(@CookieValue("groupId") Long groupId, @RequestBody GlobalSearchInput globalSearchInput, @CookieValue("userId") String userId) throws Exception {
	globalSearchInput.setGroupBy(groupId);
	if (globalSearchInput.getCreatedBy() != null && globalSearchInput.getCreatedBy().size() == 0) {
	    globalSearchInput.setCreatedBy(null);
	}
	globalSearchInput.setGroupBy(null);

	Collection<DataSourceInstance> dataSourceListFromCollection = null;
	try {
	    dataSourceListFromCollection = MyCollectionBSO.getAllOfMyCollection(Long.valueOf(userId));
	    if (null == dataSourceListFromCollection || dataSourceListFromCollection.size() == 0) {
		return new GlobalSearchWithObjectInstance(new LinkedList<DataSourceInstance>(), 0l, new LinkedList<Item>(), new LinkedList<ObjectRequestToApproveDTO>(), new GlobalSearchResult());
	    }

	} catch (Exception e) {
	    logger.error(LoggerConstants.LOG_MAXIQWEB + CHAR_CONSTANTS.SPACE + StackTraceReader.stringFromStackTrace(e));
	    dataSourceListFromCollection = new ArrayList<>(1);
	}

	List<String> dsNames = new ArrayList<String>(dataSourceListFromCollection.size());
	for (DataSourceInstance ds : dataSourceListFromCollection) {
	    dsNames.add(ds.getDataSourceName());
	}
	globalSearchInput.setName(dsNames);

	GlobalSearchResult searchForGivenInput = GlobalSearchFactory.searchForGivenInput(globalSearchInput);
	return ObjectRequestBSO.getDataSourcesFromGlobalSearchResult(searchForGivenInput, userId, groupId);
    }
}
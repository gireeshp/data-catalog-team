package org.mosaic.catalogue.controllers.globalsearchcontroller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.dao.globalsearch.GlobalSearchIndexDAOImpl;
import com.augmentiq.maxiq.dao.globalsearch.LoadMetaDataDAOImpl;
import com.augmentiq.maxiq.model.globalsearch.SearchIndexDTO;

/**
 * @author Ajinkya Marathe Created for execution of job managed by
 *         dispatcher-servlet.xml Which is responsible to create index &
 *         documents in elastic search
 */
public class GlobalSearchJob {

    private static final Logger logger = LoggerFactory.getLogger(GlobalSearchJob.class);
    private static String INDEX_NAME = "";

    static {
	try {
	    INDEX_NAME = Cache.getProperty(CacheConstants.GLOBAL_SEARCH_ELASTIC_SEARCH_INDEX_NAME);
	} catch (Exception e) {
	    logger.error(GlobalSearchIndexDAOImpl.class.getClass().getName() + " INDEX_NAME not configured in config.properties");
	}
    }

    public synchronized void process() {
	GlobalSearchIndexDAOImpl globalSearchIndexDAOImpl = new GlobalSearchIndexDAOImpl();
	LoadMetaDataDAOImpl loadMetaDataDAOImpl = new LoadMetaDataDAOImpl();
	List<SearchIndexDTO> checkDataFirst = loadMetaDataDAOImpl.getAllDatasourcesAndFlow();
	if (checkDataFirst != null && checkDataFirst.size() > 0) {
	    if (!globalSearchIndexDAOImpl.checkIndexExistORNot()) {
		// Creating index first time only & loading documents
		try {
		    if (globalSearchIndexDAOImpl.createIndex(INDEX_NAME)) {
			try {
			    globalSearchIndexDAOImpl.loadAllDocuments();
			} catch (Exception e) {
			    logger.error(LoggerConstants.LOG_MAXIQWEB + " Exception while loading documents in Job " + e.getStackTrace());
			}
		    }
		} catch (Exception e) {
		    logger.error(LoggerConstants.LOG_MAXIQWEB + " Exception while creating index" + e.getStackTrace());
		}
	    } else {
		try {
		    globalSearchIndexDAOImpl.loadAllDocuments();
		} catch (Exception e) {
		    logger.error(LoggerConstants.LOG_MAXIQWEB + " Exception while loading documents in Job " + e.getStackTrace());
		}
	    }
	}
    }
}
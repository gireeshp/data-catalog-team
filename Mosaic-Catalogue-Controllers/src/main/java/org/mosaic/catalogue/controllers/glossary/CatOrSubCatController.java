package org.mosaic.catalogue.controllers.glossary;

import java.util.List;
import java.util.Map;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.entity.model.configuration.bean.CategorySubCatMaster;
import com.augmentiq.maxiq.entity.model.glossary.catsubcat.CatSubCatMasterDTO;
import com.augmentiq.maxiq.services.glossary.catsubcat.CatSubCatMasterBSO;

/**
 * Need to add category and sub category easily accessible from our platform
 *
 * @author Ajinkya Marathe
 * @version 5-Oct-2017
 */
@Controller
public class CatOrSubCatController extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(CatOrSubCatController.class);

    CatSubCatMasterBSO catsubCatMasterBSO = new CatSubCatMasterBSO();

    @RequestMapping(value = "/insertCategoryOrSubCategoy", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody void insertCategoryOrSubCategoy(@RequestBody CategorySubCatMaster categorySubCatMaster, @CookieValue("userId") Long userId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> insertCategoryOrSubCategoy {}", userId);
	Boolean catMaster = catsubCatMasterBSO.getCategoryAndSubCategoryByName(categorySubCatMaster.getCatOrSubCatName());

	if (catMaster) {
	    ExceptionsMessanger.throwException(new SystemException(), "ERR_019", null);
	} else {
	    categorySubCatMaster.setCreatedBy(userId);
	    catsubCatMasterBSO.insertCatSubCatMaster(categorySubCatMaster);
	}
	logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << insertCategoryOrSubCategoy {}", userId);
    }

    @RequestMapping(value = "/deleteFromCategoryAndSubCat", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody void deleteFromCategoryAndSubCat(@RequestBody CategorySubCatMaster categorySubCatMaster, @CookieValue("userId") Long userId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> deleteFromCategoryAndSubCat {}", userId);
	catsubCatMasterBSO.deleteFromCategoryAndSubCat(categorySubCatMaster.getId(), userId);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << deleteFromCategoryAndSubCat {}", userId);
    }

    @RequestMapping(value = "/getAllActiveCategorySubCatMaster", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody List<CatSubCatMasterDTO> getAllActiveCategorySubCatMaster() throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getAllActiveCategorySubCatMaster {}");
	return catsubCatMasterBSO.getAllOfActiveCategorySubCatMaster();
    }

    @RequestMapping(value = "/getAllCategorySubCatMaster", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody Map<String, List<String>> getAllCategorySubCatMaster() throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getAllCategorySubCatMaster {}");
	return catsubCatMasterBSO.getAllOfCategorySubCatMaster();
    }
}

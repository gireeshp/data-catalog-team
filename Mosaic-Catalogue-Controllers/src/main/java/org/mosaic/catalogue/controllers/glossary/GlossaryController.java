package org.mosaic.catalogue.controllers.glossary;

import java.util.List;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlossaryMaster;
import com.augmentiq.maxiq.model.services.glossary.GlossaryDTO;
import com.augmentiq.maxiq.services.glossary.bso.GlossaryBSO;

/**
 * Need to add glossary to refer it from DS or in future some other. while
 * searching, accessiblity of fields easily from search.
 *
 * @author Ajinkya Marathe
 * @version 22-Jun-2017
 */
@Controller
public class GlossaryController extends AjaxErrorHandler {

    GlossaryBSO glossaryBso = new GlossaryBSO();

    @RequestMapping(value = "/addTag", method = { RequestMethod.POST })
    public @ResponseBody Long addTag(@RequestBody GlossaryMaster glossaryMaster, @CookieValue("groupId") Long groupId, @CookieValue("userId") Long userId) throws SystemException {
	glossaryMaster.setId(0L);
	glossaryMaster.setCreatedBy(userId);
	glossaryMaster.setGroupId(groupId);
	glossaryMaster.setCreatedDate(System.currentTimeMillis());
	glossaryMaster.setStatus(Integer.parseInt(QueryConstants.GlossaryMaster.ACTIVE));
	return glossaryBso.addTag(glossaryMaster);
    }

    @RequestMapping(value = "/updateTag", method = { RequestMethod.POST })
    public @ResponseBody Long updateTag(@RequestBody GlossaryMaster glossaryMaster, @CookieValue("groupId") Long groupId, @CookieValue("userId") Long userId) throws SystemException {
	glossaryMaster.setModifiedBy(userId);
	glossaryMaster.setModifiedDate(System.currentTimeMillis());
	return glossaryBso.addTag(glossaryMaster);
    }

    @RequestMapping(value = "/deleteTag", method = { RequestMethod.POST })
    public @ResponseBody void deleteTag(@RequestBody GlossaryMaster glossaryMaster, @CookieValue("userId") Long userId) throws SystemException {
	glossaryMaster.setModifiedBy(userId);
	glossaryMaster.setModifiedDate(System.currentTimeMillis());
	glossaryBso.deleteTag(glossaryMaster);
    }

    @RequestMapping(value = "/getAllActiveTags", method = { RequestMethod.POST })
    public @ResponseBody List<GlossaryDTO> getAllActiveTags() throws SystemException {
	return glossaryBso.getAllActiveTags();
    }

    @RequestMapping(value = "/getAllListOfDSOfTag", method = { RequestMethod.POST })
    public @ResponseBody List<String> getAllActiveTags(@RequestBody GlossaryMaster glossaryMaster) throws SystemException {
	return glossaryBso.getAllDSWithWithTagUsed(glossaryMaster);
    }
}

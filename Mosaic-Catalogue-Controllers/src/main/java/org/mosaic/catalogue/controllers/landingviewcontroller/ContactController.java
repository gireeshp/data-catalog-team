package org.mosaic.catalogue.controllers.landingviewcontroller;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserFeedback;
import com.augmentiq.maxiq.services.setup.bso.UserManagementBso;

@Controller
public class ContactController extends AjaxErrorHandler {
    @RequestMapping(value = "/saveConatctUsDetails", method = RequestMethod.POST)
    public @ResponseBody void saveConatctUsDetails(@RequestBody UserFeedback userFeedback) throws SystemException {
	// userFeedback.setId(Sequences.getNextId(Sequences.USER_FEEDBACK));
	userFeedback.setUserName(userFeedback.getUserName());
	userFeedback.setBusinessEmail(userFeedback.getBusinessEmail());
	userFeedback.setComments(userFeedback.getComments());
	UserManagementBso.saveConatctUsDetails(userFeedback);
    }
}

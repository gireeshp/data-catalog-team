package org.mosaic.catalogue.controllers.navigators;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;

@Controller
public class SignInUser extends AjaxErrorHandler {
    private static final Logger logger = LoggerFactory.getLogger(SignInUser.class);

    @RequestMapping(value = "redirectToAddNewDataSourceAction")
    public ModelAndView signInUser() {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> signInUser()");

	ModelAndView andView = new ModelAndView("AddNewDataSource");

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << signInUser() " + ParamUtils.getString(andView));
	return andView;
    }
}

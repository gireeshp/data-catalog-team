package org.mosaic.catalogue.controllers.object.request.approval.workflow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ObjectSerializationHandler;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.configuration.enums.DataRequestEnum;
import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.DatasourceApprovalMapper;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchWithObjectInstance;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.entity.model.usergroup.UserGroups;
import com.augmentiq.maxiq.model.access.manage.ShareAccessDTO;
import com.augmentiq.maxiq.model.object.request.DataRequestDTO;
import com.augmentiq.maxiq.object.request.dao.ObjectRequestDAO;
import com.augmentiq.maxiq.services.access.manage.AccessManageBSO;
import com.augmentiq.maxiq.services.mycollection.MyCollectionBSO;
import com.augmentiq.maxiq.services.object.request.ObjectRequestBSO;

@Controller
public class ObjectRequestApprovalWorkflow extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(ObjectRequestApprovalWorkflow.class);

    @RequestMapping(value = "/createRequestForObject", method = { RequestMethod.POST })
    public @ResponseBody String createRequestForObject(@CookieValue("userId") String userId, @RequestBody Map<String, Object> reqDataObject) throws Exception {

	Boolean isRequestedForGroup = Boolean.valueOf(reqDataObject.get(QueryConstants.Parameters.IS_REQUESTED_FOR_GROUP) + "");

	DataRequestDTO dataRequestDTO = (DataRequestDTO) ObjectSerializationHandler.toObject(ObjectSerializationHandler.toString(reqDataObject.get(QueryConstants.Parameters.DATA_REQUEST_DTO)), DataRequestDTO.class);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> createRequestForObject() " + ParamUtils.getString(dataRequestDTO, Long.valueOf(userId), isRequestedForGroup));
	ObjectRequestBSO.createObjectRequest(dataRequestDTO, Long.valueOf(userId), isRequestedForGroup);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << createRequestForObject() " + ParamUtils.getString(dataRequestDTO, Long.valueOf(userId), isRequestedForGroup));
	return Constants.SUCCESS;
    }

    @RequestMapping(value = "/getCollectionAndRequestStatusOfDatasource", method = { RequestMethod.POST })
    public @ResponseBody String getCollectionAndRequestStatusOfDatasource(@CookieValue("userId") String userId, @CookieValue("groupId") String groupId, @RequestBody Map<String, Object> reqDataObject) throws ConnectionException, SystemException {

	String objectId = reqDataObject.get(QueryConstants.Parameters.OBJECT_ID) + "";
	String objectType = reqDataObject.get(QueryConstants.Parameters.OBJECT_TYPE) + "";

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getCollectionAndRequestStatusOfDatasource() " + ParamUtils.getString(userId, groupId, objectId, objectType));
	DataRequestEnum dataRequestEnum = null;
	try {
	    dataRequestEnum = ObjectRequestBSO.getStausOfObjectRequestAndCollectionForObject(userId, groupId, objectId, objectType);
	} catch (Exception e) {
	    logger.error(LoggerConstants.LOG_MAXIQWEB + " << getCollectionAndRequestStatusOfDatasource() " + ParamUtils.getString(userId, groupId, objectId, objectType, e.getStackTrace()));
	    return Constants.FAIL;
	}
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getCollectionAndRequestStatusOfDatasource() " + ParamUtils.getString(userId, groupId, objectId, objectType));

	return dataRequestEnum.getName();
    }
    @RequestMapping(value = "/getAddedMycollectionDataSource", method = { RequestMethod.POST })
    public @ResponseBody String getAddedMycollectionDataSource(@CookieValue("userId") String userId,  @RequestBody Map<String, Object> reqDataObject) throws ConnectionException, SystemException {

	String objectId = reqDataObject.get(QueryConstants.Parameters.OBJECT_ID) + "";
	String objectType = reqDataObject.get(QueryConstants.Parameters.OBJECT_TYPE) + "";

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getAddedMycollectionDataSource() " + ParamUtils.getString(userId, objectId, objectType));
	DataRequestEnum dataRequestEnum = null;
	try {
	    dataRequestEnum = ObjectRequestBSO.getAddedMycollectionDataSource(Long.parseLong(userId),Long.parseLong(objectId), objectType);
	} catch (Exception e) {
	    logger.error(LoggerConstants.LOG_MAXIQWEB + " << getAddedMycollectionDataSource() " + ParamUtils.getString(userId,  objectId, objectType, e.getStackTrace()));
	    return Constants.FAIL;
	}
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getAddedMycollectionDataSource() " + ParamUtils.getString(userId, objectId, objectType));

	return dataRequestEnum.getName();
    }

    @RequestMapping(value = "/addToMyCollection", method = { RequestMethod.POST })
    public @ResponseBody String addToMyCollection(@CookieValue("userId") String userId, @RequestBody Map<String, Object> reqDataObject) throws NumberFormatException, Exception {
	String objectId = reqDataObject.get(QueryConstants.Parameters.OBJECT_ID) + "";
	String objectType = reqDataObject.get(QueryConstants.Parameters.OBJECT_TYPE) + "";

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> createRequestForDatasource() " + ParamUtils.getString(Long.parseLong(objectId), objectType, Long.valueOf(userId)));
	MyCollectionBSO.addToMyCollection(Long.parseLong(objectId), objectType, Long.valueOf(userId));

	return Constants.SUCCESS;
    }

    @RequestMapping(value = "/removeFromMyCollection", method = { RequestMethod.POST })
    public @ResponseBody String removeFromMyCollection(@CookieValue("userId") String userId, @RequestBody Map<String, Object> reqDataObject) throws ConnectionException, SystemException {
	String objectId = reqDataObject.get(QueryConstants.Parameters.OBJECT_ID) + "";
	String objectType = reqDataObject.get(QueryConstants.Parameters.OBJECT_TYPE) + "";

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> removeFromMyCollection() {} {} {}", objectId, objectType, userId);
	try {
	    MyCollectionBSO.removeFromMyCollection(Long.parseLong(objectId), objectType, Long.valueOf(userId));
	} catch (Exception e) {
	    logger.error(LoggerConstants.LOG_MAXIQWEB + " << removeFromMyCollection() " + ParamUtils.getString(Long.parseLong(objectId), objectType, Long.valueOf(userId)));
	    return Constants.FAIL;
	}
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << removeFromMyCollection() " + ParamUtils.getString(Long.parseLong(objectId), objectType, Long.valueOf(userId)));
	return Constants.SUCCESS;
    }

    @RequestMapping(value = "/getApprovalObjects", method = { RequestMethod.POST })
    public @ResponseBody GlobalSearchWithObjectInstance getObjectsForApproval(@CookieValue("userId") Long userId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getApprovalObjects() " + ParamUtils.getString(userId));
	return ObjectRequestBSO.getObjectsForApproval(Long.valueOf(userId));
    }

    @RequestMapping(value = "/getAllRequestedObjects", method = { RequestMethod.POST })
    public @ResponseBody GlobalSearchWithObjectInstance getAllRequestedObjects(@CookieValue("userId") String userId, @CookieValue("groupId") Long groupId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getAllRequestedObjects() " + ParamUtils.getString(Long.valueOf(groupId), userId));
	GlobalSearchWithObjectInstance dataRequestList = null;
	try {
	    dataRequestList = ObjectRequestBSO.getAllRequestedObjects(Long.valueOf(userId), Long.valueOf(groupId));
	} catch (Exception e) {
	    logger.error(LoggerConstants.LOG_MAXIQWEB + " << getAllRequestedObjects() " + ParamUtils.getString(userId, groupId, e.getStackTrace()));
	    e.printStackTrace();
	}

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getAllRequestedObjects() " + ParamUtils.getString(dataRequestList));
	return dataRequestList;
    }

    @RequestMapping(value = "/getAllMyCollection", method = { RequestMethod.POST })
    public @ResponseBody Collection<DataSourceInstance> getAllMyCollectionDataSources(@CookieValue("userId") String userId) throws ConnectionException, SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getAllMyCollectionDataSources() " + ParamUtils.getString(Long.valueOf(userId)));
	Collection<DataSourceInstance> dataSourceListFromCollection = null;
	try {
	    dataSourceListFromCollection = MyCollectionBSO.getAllOfMyCollection(Long.valueOf(userId));
	} catch (Exception e) {
	    logger.error(LoggerConstants.LOG_MAXIQWEB + " << getAllMyCollectionDataSources() " + ParamUtils.getString(Long.valueOf(userId), e.getStackTrace()));
	    return dataSourceListFromCollection;
	}
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getAllMyCollectionDataSources() " + ParamUtils.getString(dataSourceListFromCollection));
	return dataSourceListFromCollection;
    }

    @RequestMapping(value = "/acceptObjectRequest", method = { RequestMethod.POST })
    public @ResponseBody String acceptObjectRequest(@CookieValue("userId") String userId, @RequestBody Map<String, Object> reqDataObject) throws Exception {

	Long requestId = Long.parseLong(reqDataObject.get(QueryConstants.Parameters.REQUESTED_ID) + "");
	String justification = reqDataObject.get(QueryConstants.Parameters.JUSTIFICATION) + "";

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> acceptObjectRequest() " + ParamUtils.getString(userId, requestId, justification));
	DataRequestDTO dataRequestDto = new DataRequestDTO(ObjectRequestBSO.getDataRequestById(requestId));

	DatasourceApprovalMapper approver = AccessManageBSO.getDataSourceApprover(dataRequestDto.getObjectId(), Long.valueOf(userId));
	if (approver != null) {
	    if (approver.getParent_level() != -1) {
		DatasourceApprovalMapper parentApprover = AccessManageBSO.getParentDataSourceApprover(approver.getObjectId(), approver.getParent_level());
		dataRequestDto.setRequestedUserId(parentApprover.getUserId());
		ObjectRequestBSO.createObjectRequest(dataRequestDto, dataRequestDto.getRequestedBy());
		ObjectRequestDAO.acceptDataSourceRequest(requestId, justification);
		return Constants.SUCCESS;
	    }
	}

	ObjectRequestBSO.acceptObjectRequest(requestId, justification);

	ObjectRequestBSO.setNotification(dataRequestDto.getRequestedBy(), ObjectTypes.DATA_SOURCE, requestId, DataRequestEnum.ACCEPT);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << acceptObjectRequest() " + ParamUtils.getString());
	return Constants.SUCCESS;
    }

    @RequestMapping(value = "/rejectObjectRequest", method = { RequestMethod.POST })
    public @ResponseBody String rejectObjectRequest(@RequestBody Map<String, Object> reqDataObject) throws Exception {

	Long requestId = Long.parseLong(reqDataObject.get(QueryConstants.Parameters.REQUESTED_ID) + "");
	String justification = reqDataObject.get(QueryConstants.Parameters.JUSTIFICATION) + "";

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> rejectObjectRequest() " + ParamUtils.getString(requestId, justification));

	ObjectRequestBSO.rejectObjectRequest(requestId, justification);

	DataRequestDTO dataRequestDto = new DataRequestDTO(ObjectRequestBSO.getDataRequestById(requestId));
	ObjectRequestBSO.setNotification(dataRequestDto.getRequestedBy(), ObjectTypes.DATA_SOURCE, requestId, DataRequestEnum.REJECT);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << rejectObjectRequest() " + ParamUtils.getString(requestId));
	return Constants.SUCCESS;
    }

    @RequestMapping(value = "/rejectMultiObjectRequest", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody String rejectMultiObjectRequest(@RequestParam Map<Long, String> requestIdAndJustification) throws ConnectionException, SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> rejectObjectRequest() " + ParamUtils.getString(requestIdAndJustification));

	ObjectRequestBSO.rejectMultiObjectRequest(requestIdAndJustification);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << rejectObjectRequest() " + ParamUtils.getString(requestIdAndJustification));
	return Constants.SUCCESS;
    }

    // not required
    @RequestMapping(value = "/shareThisObject", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody static void shareThisObject(@RequestParam Long objectId, @RequestParam String objectType, @RequestBody ShareAccessDTO shareData) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> shareThisObject()" + ParamUtils.getString(objectId, objectType));
	AccessManageBSO.shareObjectWithGroups(objectId, objectType, shareData.getGroupAccessMap());
	AccessManageBSO.shareObjectWithUsers(objectId, objectType, shareData.getUserAccessMap());
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << shareThisObject()" + ParamUtils.getString(objectId, objectType));
    }

    // not required
    @RequestMapping(value = "/revokeAccessOfThisObject", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody static void revokeAccessOfThisObject(@RequestParam Long objectId, @RequestParam String objectType, @RequestBody ShareAccessDTO shareData) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> revokeAccessOfThisObject()" + ParamUtils.getString(objectId));
	AccessManageBSO.revokeAccessOfThisObjectFromGroups(objectId, objectType, shareData.getGroupAccessMap());
	AccessManageBSO.revokeAccessOfThisObjectFromUsers(objectId, objectType, shareData.getUserAccessMap());
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << revokeAccessOfThisObject()" + ParamUtils.getString(objectId));
    }

    // not required
    @RequestMapping(value = "/getAllAccessesOfThisObject", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody static ShareAccessDTO getAllAccessesOfThisObject(@RequestParam Long objectId, @RequestParam String objectType) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getAllAccessesOfThisObject()" + ParamUtils.getString(objectId) + objectType);
	ShareAccessDTO sharedAccesses = AccessManageBSO.getSharedAccessForThisObject(objectId, objectType);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getAllAccessesOfThisObject()" + ParamUtils.getString(sharedAccesses));
	return sharedAccesses;
    }

    // required
    @RequestMapping(value = "/getUserListForShareManagement", method = { RequestMethod.POST })
    public @ResponseBody static List<ApplicationUser> getUserListForShareManagement(@CookieValue("userId") Long userId) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getUserListForShareManagement()" + ParamUtils.getString(userId));
	List<ApplicationUser> userList = AccessManageBSO.getListOfApplicationUsers(userId);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getUserListForShareManagement()" + ParamUtils.getString(userId));
	return userList;
    }

    // required
    @RequestMapping(value = "/getGroupListForShareManagement", method = { RequestMethod.POST })
    public @ResponseBody static List<UserGroups> getGroupListForShareManagement(@CookieValue("userId") Long userId) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getGroupListForShareManage()" + ParamUtils.getString(userId));

	List<UserGroups> groupList = AccessManageBSO.getListOfUserGroup(userId);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getGroupListForShareManage()" + ParamUtils.getString(userId));
	return groupList;
    }

    // required
    @RequestMapping(value = "/getPendingRequestCount", method = { RequestMethod.POST })
    public @ResponseBody static Long getPendingRequestCount(@CookieValue("userId") Long userId) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getPendingRequestNotification()" + ParamUtils.getString(userId));

	Long count = AccessManageBSO.getPendingRequestCount(userId);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getPendingRequestNotification()" + ParamUtils.getString(userId));
	return count;
    }

    // required
    @RequestMapping(value = "/getListOfGroupsToForWhichObjectNeedsToBeShared", method = { RequestMethod.POST })
    public @ResponseBody static List<UserGroups> getListOfGroupsForObjectSharing(@CookieValue("userId") Long userId, @RequestBody Map<String, Object> reqDataObject) throws SystemException {

	Long objectId = Long.parseLong(reqDataObject.get(QueryConstants.Parameters.OBJECT_ID) + "");
	String objectType = reqDataObject.get(QueryConstants.Parameters.OBJECT_TYPE) + "";

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getListOfGroupsForObjectSharing()" + ParamUtils.getString(userId) + " " + ParamUtils.getString(objectId) + " " + objectType);

	List<UserGroups> userGroups = AccessManageBSO.getListOfUserGroupForObjectSharing(userId);

	List<UserGroups> filterUserList = AccessManageBSO.filterGroupList(userGroups, objectId, objectType);
	userGroups.removeAll(filterUserList);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getListOfGroupsForObjectSharing()" + ParamUtils.getString(userId) + " " + ParamUtils.getString(objectId) + " " + objectType);
	return userGroups;
    }

    // required
    @RequestMapping(value = "/getNotifications", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody static Map<String, Long> getNotifications(@CookieValue("userId") Long userId) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getNotifications()" + ParamUtils.getString(userId));

	Map<String, Long> notifications = new HashMap<String, Long>();
	notifications = AccessManageBSO.getNotificationsOfUser(userId);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << getNotifications()" + ParamUtils.getString(userId));

	return notifications;
    }

    // required
    @RequestMapping(value = "/userNotified", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody static void userNotified(@CookieValue("userId") Long userId) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> userNotified()" + ParamUtils.getString(userId));

	ObjectRequestBSO.deleteNotification(userId);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << userNotified()" + ParamUtils.getString(userId));
    }

    // required
    @RequestMapping(value = "/saveDataSourceApproverList", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody String saveDataSourceApproverList(@RequestBody List<DatasourceApprovalMapper> datasourceApprovalMapper) throws SystemException {

	String message = Constants.FAIL;

	if (datasourceApprovalMapper != null && !datasourceApprovalMapper.isEmpty()) {

	    message = AccessManageBSO.reFreshApproverDetails(datasourceApprovalMapper);
	}

	return message;
    }

    // required
    @RequestMapping(value = "/getDataSourceApproverList", method = { RequestMethod.GET, RequestMethod.POST })
    public @ResponseBody List<DatasourceApprovalMapper> getDataSourceApproverList(@RequestParam Long objectId) throws SystemException {
	List<DatasourceApprovalMapper> datasourceApprovalMapper = AccessManageBSO.getDataSourceApproverListByObjectId(objectId);
	return datasourceApprovalMapper;
    }

}

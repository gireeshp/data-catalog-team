package org.mosaic.catalogue.controllers.repo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ObjectSerializationHandler;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.configuration.enums.Actions;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.entity.model.configuration.bean.DashBoardDTO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceFeedback;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceFeedbackDTO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.services.configuration.DataSourceBso;
import com.augmentiq.maxiq.services.datasource.dashboard.DataDasboardBSO;
import com.augmentiq.maxiq.services.external.connection.ConnectorListingBSO;
import com.augmentiq.maxiq.services.userservice.ApplicationUserServices;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

@Controller
public class DataSourceController extends AjaxErrorHandler {
	private static final Logger logger = LoggerFactory.getLogger(DataSourceController.class);

	@Autowired
	private DataSourceBso dataSourceBso;

	@Autowired
	private ConnectorListingBSO connectorListingBSO;

	@RequestMapping(value = "/getDataSource", method = { RequestMethod.POST })
	public @ResponseBody DataSource attachDataSourceToRepo(@CookieValue("userId") String userId,
			@RequestBody String datasourcename) throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> attachDataSourceToRepo() "
				+ ParamUtils.getString(userId, datasourcename));
		DataSource dataSource = dataSourceBso.getDataSource(datasourcename, userId);
		if (null != dataSource && null != dataSource.getDataSourceType()
				&& (dataSource.getDataSourceType().equals(DataSourceType.RDBMS_DATA_SOURCE)
						|| dataSource.getDataSourceType().equals(DataSourceType.NOSQL))) {

			Long connectionId = dataSource.getConfig().getConnectionId();
			ConnectionConfig connectionConfig = null;
			if (connectionId != null) {
				connectionConfig = connectorListingBSO.getConnectionConfigById(connectionId);
			}
			dataSource.setConfig(connectionConfig);
		}

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << attachDataSourceToRepo() " + ParamUtils.getString(dataSource));

		return dataSource;
	}

	@RequestMapping(value = "/getDataSourceInstance", method = { RequestMethod.POST })
	public @ResponseBody DataSourceInstance getDateRepositoryInstance(@RequestBody String dataSourceName)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDateRepositoryInstance() "
				+ ParamUtils.getString(dataSourceName));

		DataSourceInstance dataSourceInstance = dataSourceBso.getDataSourceInstance(dataSourceName);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDateRepositoryInstance() "
				+ ParamUtils.getString(dataSourceInstance));
		return dataSourceInstance;
	}

	@RequestMapping(value = "/saveDataSourceFeedback", method = { RequestMethod.POST })
	public @ResponseBody void saveDataSourceFeedback(@CookieValue("userId") Long userId,
			@CookieValue("groupId") Long groupId, @RequestBody Map<String, Object> reqDataObject,
			@CookieValue("projectId") Long projectId) throws Exception {

		String dataSourceId = reqDataObject.get(QueryConstants.Parameters.DATASOURCEID) + "";

		DataSourceFeedback sourceFeedback = (DataSourceFeedback) ObjectSerializationHandler.toObject(
				ObjectSerializationHandler.toString(reqDataObject.get(QueryConstants.Parameters.SOURCE_FEED_BACK)),
				DataSourceFeedback.class);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> saveDataSourceFeedback()"
				+ ParamUtils.getString(userId, groupId, sourceFeedback, dataSourceId));
		dataSourceBso.saveDataSourceFeedback(userId, groupId, sourceFeedback, dataSourceId);
		dataSourceBso.saveActivityData(projectId, userId + "", ObjectTypes.DATA_SOURCE, dataSourceId,
				DataSourceInstanceDao.getDataSourceInstance(Long.parseLong(dataSourceId)).getDataSourceName(),
				Actions.UPDATE);
	}

	@RequestMapping(value = "/fetchDataSourceFeedback", method = { RequestMethod.POST })
	public @ResponseBody List<DataSourceFeedbackDTO> fetchDataSourceFeedback(@CookieValue("userId") Long userId,
			@CookieValue("groupId") Long groupId, @RequestBody String dataSourceId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceFeedback()"
				+ ParamUtils.getString(userId, groupId, dataSourceId));

		List<DataSourceFeedbackDTO> fetchDataSourceFeedback = dataSourceBso.fetchDataSourceFeedback(userId, groupId,
				dataSourceId);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceFeedback()"
				+ ParamUtils.getString(fetchDataSourceFeedback));
		return fetchDataSourceFeedback;
	}

	@RequestMapping(value = "/fetchDataSourceFeedbackRatingCount", method = { RequestMethod.POST })
	public @ResponseBody List<Map<String, Object>> fetchDataSourceFeedbackRatingCount(@RequestBody String dataSourceId)
			throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceFeedbackRatingCount()"
				+ ParamUtils.getString(dataSourceId));

		List<Map<String, Object>> fetchDataSourceFeedback = dataSourceBso
				.fetchDataSourceFeedbackRatingCount(dataSourceId);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceFeedbackRatingCount()"
				+ ParamUtils.getString(fetchDataSourceFeedback));
		return fetchDataSourceFeedback;
	}

	@RequestMapping(value = "/updateDataSourceRating", method = { RequestMethod.POST })
	public @ResponseBody void updateDataSourceRating(@CookieValue("userId") Long userId,
			@CookieValue("groupId") Long groupId, @RequestBody Map<String, Object> reqDataObject,
			@CookieValue("projectId") Long projectId) throws Exception {

		Long rating = Long.parseLong(reqDataObject.get(QueryConstants.Parameters.RATING) + "");
		String dataSourceId = reqDataObject.get(QueryConstants.Parameters.DATASOURCEID) + "";

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateDataSourceRating()"
				+ ParamUtils.getString(userId, groupId, rating, dataSourceId));

		dataSourceBso.checkUserAlreadyExist(userId, groupId, rating, dataSourceId);
		dataSourceBso.saveActivityData(projectId, userId + "", ObjectTypes.DATA_SOURCE, dataSourceId,
				DataSourceInstanceDao.getDataSourceInstance(Long.parseLong(dataSourceId)).getDataSourceName(),
				Actions.UPDATE);
	}

	@RequestMapping(value = "/getAvgRatingForDataSource", method = { RequestMethod.POST })
	public @ResponseBody List<DataSourceFeedback> getAvgRatingForDataSource(@CookieValue("userId") Long userId,
			@CookieValue("groupId") Long groupId, @RequestBody String dataSourceId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getAvgRatingForDataSource()"
				+ ParamUtils.getString(userId, groupId, dataSourceId));

		List<DataSourceFeedback> avgRatingForDataSource = dataSourceBso.getAvgRatingForDataSource(userId, groupId,
				dataSourceId);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getAvgRatingForDataSource()"
				+ ParamUtils.getString(avgRatingForDataSource));
		return avgRatingForDataSource;
	}

	@RequestMapping(value = "/getEachUserRatingForDataSource", method = { RequestMethod.POST })
	public @ResponseBody List<DataSourceFeedback> getEachUserRatingForDataSource(@CookieValue("groupId") Long groupId,
			@RequestBody String dataSourceId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getEachUserRatingForDataSource()"
				+ ParamUtils.getString(groupId, dataSourceId));

		List<DataSourceFeedback> avgRatingForDataSource = dataSourceBso.getEachUserRatingForDataSource(groupId,
				dataSourceId);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getEachUserRatingForDataSource()"
				+ ParamUtils.getString(avgRatingForDataSource));
		return avgRatingForDataSource;
	}

	@RequestMapping(value = "/getCategories", method = { RequestMethod.POST })
	public @ResponseBody Map<String, List<String>> getCategories(@CookieValue("userId") String userId,
			@CookieValue("groupId") Long groupId) throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getCategories() " + ParamUtils.getString(userId, groupId));
		return ApplicationUserServices.categories;
	}

	@RequestMapping(value = "/updateDataSourceVote", method = { RequestMethod.POST })
	public @ResponseBody String updateDataSourceVote(@CookieValue("userId") Long userId,
			@CookieValue("groupId") Long groupId, @RequestBody Map<String, Object> reqDataObject) throws Exception {

		Integer vote = Integer.parseInt(reqDataObject.get(QueryConstants.Parameters.VOTE) + "");
		Long dataSourceId = Long.parseLong(reqDataObject.get(QueryConstants.Parameters.DATASOURCEID) + "");
		Long feedbackId = Long.parseLong(reqDataObject.get(QueryConstants.Parameters.FEEDBACK_ID) + "");
		// Long projectId =
		// Long.parseLong(reqDataObject.get(QueryConstants.Parameters.OWNER_PROJECT_ID)
		// + "");

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateDataSourceVote()"
				+ ParamUtils.getString(userId, groupId, vote, dataSourceId, feedbackId));
		dataSourceBso.saveActivityData(0L, userId + "", ObjectTypes.DATA_SOURCE, dataSourceId + "",
				DataSourceInstanceDao.getDataSourceInstance(dataSourceId).getDataSourceName(), Actions.UPDATE);
		return dataSourceBso.updateDataSourceFeedbackVote(userId, groupId, vote, dataSourceId, feedbackId);
	}

	@RequestMapping(value = "/getDataDashboardCountDetails", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody List<DashBoardDTO> getDataDashboardCountDetails(String dataSourceId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >>  getDataDashboardCountDetails() "
				+ ParamUtils.getString(dataSourceId));
		List<DashBoardDTO> countList = DataDasboardBSO.getDataDashboardCountDetails(dataSourceId);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " <<  getDataDashboardCountDetails() " + ParamUtils.getString());
		return countList;
	}

	@RequestMapping(value = "/deleteDataSource", method = { RequestMethod.POST })
	public @ResponseBody void deleteDataSource(@CookieValue("userId") String userId, @RequestBody Long dataSourceId)
			throws Exception {
		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " : >> deleteDataSource() : [userId {}, dataSourceId {}, groupId {}]",
				userId, dataSourceId);
		DataSourceBso dataSourceBso = new DataSourceBso();
		DataSourceInstance dataSourceInstance = DataSourceInstanceDao.getDataSourceInstance(dataSourceId);

		if (null != dataSourceInstance) {
			DataSourceInstanceDao.deleteDataSource(dataSourceId, 0L);
		}

		dataSourceBso.saveActivityData(0L, userId, ObjectTypes.DATA_SOURCE, dataSourceId + "",
				dataSourceInstance.getDataSourceName(), Actions.REMOVE);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << deleteDataSource() : [return : ]");
	}

}

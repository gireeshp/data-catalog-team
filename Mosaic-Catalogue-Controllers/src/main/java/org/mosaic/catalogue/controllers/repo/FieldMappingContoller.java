package org.mosaic.catalogue.controllers.repo;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.InvalidDateFormatException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.services.configuration.DataSourceBso;
import com.augmentiq.maxiq.services.configuration.FieldMappingBso;
import com.augmentiq.maxiq.util.common.StringUtil;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;


/** Changed for MAX-626 By Ajinkya Marathe on 02-Jan-2017 */
@Controller
public class FieldMappingContoller extends AjaxErrorHandler {

  private static final Logger logger = LoggerFactory.getLogger(FieldMappingContoller.class);

//  @RequestMapping(
//    value = "/updateFieldMapping",
//    method = {RequestMethod.POST},
//    produces = "application/*"
//  )
//  public @ResponseBody void attachDataSourceToRepo(
//      @RequestBody Map<String, Object> reqDataObject, @CookieValue("userId") String userId)
//      throws UnknownHostException, SystemException, InvalidDateFormatException,
//          ConnectionException {
//    Long datasourceId =
//        Long.parseLong(reqDataObject.get(QueryConstants.Parameters.DATASOURCEID) + "");
//
//    FieldMapping fieldMapping =
//        (FieldMapping)
//            ObjectSerializationHandler.toObject(
//                ObjectSerializationHandler.toString(
//                    reqDataObject.get(QueryConstants.Parameters.FIELD_MAPPING)),
//                FieldMapping.class);
//
//    List<String> tags = (List<String>) reqDataObject.get(QueryConstants.Parameters.TAGS);
//
//    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> attachDataSourceToRepo()");
//    DataSource datasource = DataSourceBso.getDSWithUpdatedDSInfo(datasourceId, null, null, null);
//    FieldMappingBso bso = new FieldMappingBso();
//    bso.insertFieldMapping(fieldMapping, datasource, tags, userId);
//
//    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << attachDataSourceToRepo()");
//  }

  @RequestMapping(
    value = "/updateAllFieldMappings",
    method = {RequestMethod.GET, RequestMethod.POST},
    produces = "application/*"
  )
  public @ResponseBody String updateAllFieldMapping(
      @RequestBody(required = false) List<FieldMapping> fieldMapping,
      @RequestParam Long datasourceId,
      @RequestParam List<String> tags,
      @RequestParam String category,
      @RequestParam String subCategory,
      @RequestParam String descriptions,
      @CookieValue("userId") String userId)
      throws UnknownHostException, SystemException, InvalidDateFormatException,
          ConnectionException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> updateAllFieldMapping() "
            + ParamUtils.getString(fieldMapping, datasourceId));
    //		If category is null or empty then here set category `and subcategory as 'DEFAULT' value
    if (!StringUtil.isNotNull(category)) {
      category = Constants.DEFAULT;
      subCategory = Constants.DEFAULT;
    }
    Map<String, String> values = new HashMap<String, String>();
    DataSource datasource =
        DataSourceBso.getDSWithUpdatedDSInfo(datasourceId, category, subCategory, descriptions);
    if (null != fieldMapping && fieldMapping.size() > 0) {
      FieldMappingBso bso = new FieldMappingBso();

      String string = bso.outPutFieldValidationForField(fieldMapping);
      if (string != null) {
        logger.debug(
            LoggerConstants.LOG_MAXIQWEB
                + " << updateAllFieldMapping() "
                + ParamUtils.getString(string));
        return string;
      }

      Iterator<FieldMapping> i = fieldMapping.iterator();
      while (i.hasNext()) {
        FieldMapping fieldMappings = i.next();
        bso.insertFieldMapping(fieldMappings, datasource, tags, userId);
      }
    } else if (null != datasource) {
      //update tags even though fieldMapping/data insertion is not choosed
      datasource.setTags(tags != null ? tags : new ArrayList<String>());
      FieldMappingBso.insertDataSource(datasource);
    }
    // Updated is modified status for Global Search
    values.put(
        QueryConstants.GlobalSearch.IS_MODIFIED, QueryConstants.GlobalSearch.IS_MODIFIED_TRUE);
    values.put(QueryConstants.DataSourceInstance.CATEGORY, category);
    values.put(QueryConstants.DataSourceInstance.SUBCATEGORY, subCategory);
    values.put(QueryConstants.DataSourceInstance.DESCRIPTION, descriptions);

    try {
      DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(datasourceId, values);
    } catch (SystemException e) {
      e.printStackTrace();
    }

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << updateAllFieldMapping()");
    return null;
  }
}

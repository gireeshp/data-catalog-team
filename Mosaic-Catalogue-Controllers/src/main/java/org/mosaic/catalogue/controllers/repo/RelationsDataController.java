package org.mosaic.catalogue.controllers.repo;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.augmentiq.constant.maxiq.DATACONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.configuration.generic.GenericConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.services.configuration.RelationsService;

@RestController
@RequestMapping("/relationship")
public class RelationsDataController extends AjaxErrorHandler {

  private static final Logger logger = LoggerFactory.getLogger(RelationsDataController.class);

  @RequestMapping(
    value = "/getDsDagData",
    method = {RequestMethod.POST}
  )
  public JSONObject getDsDagData(@RequestBody Long dsId) throws Exception {
    return RelationsService.getSingleRootDagData(dsId);
  }

  @RequestMapping(
    value = "/getDataFields",
    method = {RequestMethod.POST}
  )
  public DataSource getDataFields(@RequestBody Long dsId) throws Exception {
    return RelationsService.getDataFields(dsId);
  }

  @RequestMapping(
    value = "/getCollectionDsDagData",
    method = {RequestMethod.POST}
  )
  public JSONObject getCollectionDsDagData(@RequestBody String dsId) throws Exception {
    JSONObject collectionDagData = null;
    try {
      if (dsId.length() > 0) {
        collectionDagData = RelationsService.getCollectionDagData(StringUtils.removeEnd(dsId, ","));
      }
    } catch (Exception e) {
      // TODO: handle exception
      logger.warn(
          LoggerConstants.LOG_MAXIQWEB
              + " : >>  getCollectionDsDagData()"
              + ParamUtils.getString(dsId));
    }
    return collectionDagData;
  }

  @RequestMapping(
    value = "/fetchAvailableRelations",
    method = {RequestMethod.POST}
  )
  public JSONObject fetchAvailableRelations() throws Exception {
    return RelationsService.fetchAvailableRelations();
  }

  @RequestMapping(
    value = "/fetchDataSource",
    method = {RequestMethod.POST}
  )
  public List<Map<String, Object>> fetchDataSource() throws Exception {
    return RelationsService.fetchDataSource();
  }

  @RequestMapping(
    value = "/requestMapping",
    method = {RequestMethod.POST}
  )
  public JSONObject requestMapping(@RequestBody String dsIds) throws SystemException {
    JSONParser jsonParser = new JSONParser();
    JSONObject parse;
    try {
      parse = (JSONObject) jsonParser.parse(dsIds);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException("Error while parsing request parameters..!");
    }
    return RelationsService.buildFieldMappingObject(
        parse.get("sourceDsId") + "-" + parse.get("targetDsId") + "");
  }

  @RequestMapping(
    value = "/saveFieldMapping",
    method = {RequestMethod.POST}
  )
  public void saveFieldMapping(@CookieValue("userId") String userId, @RequestBody String datasource)
      throws SystemException {

    JSONParser jsonParser = new JSONParser();
    JSONObject parse;
    try {
      parse = (JSONObject) jsonParser.parse(datasource);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException("Error while parsing request parameters..!");
    }
    parse.put(RelationsService.USER_ID, userId);
    RelationsService.saveRelation(parse);
  }

  @RequestMapping(
    value = "/deleteRelation",
    method = {RequestMethod.POST}
  )
  public void deleteRelation(@RequestBody String dsIds) throws SystemException {
    JSONParser jsonParser = new JSONParser();
    JSONObject parse;
    try {
      parse = (JSONObject) jsonParser.parse(dsIds);
      RelationsService.deletePreviousEntries(
          parse.get("sourceDsId") + "", parse.get("targetDsId") + "");
    } catch (ParseException | IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException("Error while processing request...!");
    }
  }

  @RequestMapping(
    value = "/fetchDataSourceName",
    method = {RequestMethod.POST}
  )
  public List<Map<String, Object>> fetchDataSourceName(@RequestBody Map<String, Object> reqData)
      throws Exception {

    String dataSourceId = reqData.get(DATACONSTANTS.DATA_SOURCE.DATA_SOURCE_ID) + "";
    Long id = Long.parseLong(dataSourceId);

    return RelationsService.fetchDataSourceFromId(id);
  }
}

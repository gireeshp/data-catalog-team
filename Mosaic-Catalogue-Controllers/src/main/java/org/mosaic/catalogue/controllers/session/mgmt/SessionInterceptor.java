package org.mosaic.catalogue.controllers.session.mgmt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.services.setup.bso.UserManagementBso;
import com.augmentiq.maxiq.util.session.registry.SessionRegistry;

public class SessionInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

	HttpSession session = request.getSession(false);

	if (null == session) {
	    response.setStatus(10000);
	    return false;
	}

	ApplicationUser user = (ApplicationUser) session.getAttribute(SessionKeys.USER);

	if (null == user) {
	    response.setStatus(10000);
	    return false;
	} else {
	    String emailId = user.getEmailId();
	    if (null == SessionRegistry.sessions.get(emailId)) {
		UserManagementBso.removeAttributeFromSession(request, SessionKeys.USER);
		SessionRegistry.removeSession(emailId, request.getSession(false).getId());
		response.setStatus(401);
		return false;
	    } else if (null == SessionRegistry.roles.get(emailId)) {
		response.setStatus(402);
		return false;
	    }
	}

	return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	// TODO Auto-generated method stub
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	// TODO Auto-generated method stub
    }
}

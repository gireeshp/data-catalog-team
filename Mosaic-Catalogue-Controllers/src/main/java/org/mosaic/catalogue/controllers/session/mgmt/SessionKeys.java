package org.mosaic.catalogue.controllers.session.mgmt;

public class SessionKeys {
    public static final String USER = "user";
    public static final String EMAIL_ID = "emailId";
    public static final String USER_ID = "userid";
    public static final String GROUP_ID = "groupid";
    public static final String GROUP_MAP = "groupmap";
    public static final String ACTION_CODE = "acotioncode";
}

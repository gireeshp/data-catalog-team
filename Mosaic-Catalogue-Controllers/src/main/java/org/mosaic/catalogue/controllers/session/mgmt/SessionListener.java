package org.mosaic.catalogue.controllers.session.mgmt;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.util.session.registry.SessionRegistry;

public class SessionListener implements HttpSessionListener, HttpSessionAttributeListener {

    @Override
    public void sessionCreated(HttpSessionEvent event) {
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
	ApplicationUser user = (ApplicationUser) event.getSession().getAttribute(SessionKeys.USER);

	if (user != null) {
	    SessionRegistry.removeSession(user.getEmailId(), event.getSession().getId());
	} else {
	    SessionRegistry.removeSession(null, event.getSession().getId());
	}
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent arg0) {
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent arg0) {
	// TODO Auto-generated method stub
	// logger_.info("Attribute Replaced method was used");

    }
}

package org.mosaic.catalogue.controllers.setup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ObjectSerializationHandler;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.RoleNameAlreadyExistsException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.setup.domains.ActionsMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.model.setup.domains.RoleActions;
import com.augmentiq.maxiq.services.setup.bso.AccessConfigBso;
import com.augmentiq.maxiq.services.setup.bso.UserManagementBso;
import com.augmentiq.maxiq.services.userservice.ApplicationUserServices;
import com.augmentiq.maxiq.util.session.registry.SessionRegistry;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@RequestMapping("/accessConfig")
public class AccessConfigController extends AjaxErrorHandler {

    // required
    @RequestMapping(value = "/getAccessDetils", method = { RequestMethod.POST })
    public List<Map<String, Object>> getGroupsData() throws Exception {
	return AccessConfigBso.fetchAccessData();
    }

    // required
    @RequestMapping(value = "/fetchAvailableActions", method = { RequestMethod.POST })
    public List<ActionsMaster> fetchAvailableActions() throws SystemException, IOException {
	return AccessConfigBso.fetchAvailableActions();
    }

    // required
    @RequestMapping(value = "/fetchAllApps", method = { RequestMethod.POST })
    public List<Map<String, Object>> fetchAllApps() throws SystemException, IOException {
	return AccessConfigBso.fetchAllApps();
    }

    /**
     * This method is to create new role MAX-1848 : When get data in string it
     * cannot convert in json so we used Map<String, Object> to get data by key
     *
     * @param userId
     * @param reqDataObject
     * @throws SystemException
     * @throws IOException
     * @throws RoleNameAlreadyExistsException
     * @throws JSONException
     * @since 18 Jan 2018
     */
    // required
    @RequestMapping(value = "/createRoleAction", method = { RequestMethod.POST })
    public void createRoleActions(@CookieValue("userId") String userId, @RequestBody Map<String, Object> reqDataObject
    // @RequestBody RoleActions roleActions,
    // @RequestParam Integer appId
    ) throws SystemException, IOException, RoleNameAlreadyExistsException, JSONException {
	Integer appId = Integer.parseInt(reqDataObject.get(QueryConstants.Parameters.APP_ID) + "");
	RoleActions roleActions = (RoleActions) ObjectSerializationHandler.toObject(ObjectSerializationHandler.toString(reqDataObject.get(QueryConstants.Parameters.ROLEACTIONS)), RoleActions.class);
	AccessConfigBso.createRole(roleActions, userId, appId);
    }

    /**
     * This method is to update the role MAX-1848 : When get data in string it
     * cannot convert in json. so we used Map<String, Object> to get data by key
     *
     * @author Mayuri
     * @param userId
     * @param reqDataObject
     * @throws Exception
     * @since 18 Jan 2018
     */

    // required
    @RequestMapping(value = "/updateRoleAction", method = { RequestMethod.POST })
    public void updateRoleAction(@CookieValue("userId") String userId, @RequestBody Map<String, Object> reqDataObject) throws Exception {
	Integer appId = Integer.parseInt(reqDataObject.get(QueryConstants.Parameters.APP_ID) + "");
	RoleActions roleActions = (RoleActions) ObjectSerializationHandler.toObject(ObjectSerializationHandler.toString(reqDataObject.get(QueryConstants.Parameters.ROLEACTIONS)), RoleActions.class);

	AccessConfigBso.updateRole(roleActions, userId, appId);
    }

    // required
    @RequestMapping(value = "/deleteAccess", method = { RequestMethod.POST })
    public void createRoleAction(@CookieValue("userId") String userId, @RequestBody String AccessId) throws Exception {
	AccessConfigBso.deleteRole(AccessId, userId);
    }

    // required
    @RequestMapping(value = "/loadAccessControls", method = RequestMethod.POST)
    public Map<String, Object> loadAccessControls(HttpServletRequest request, HttpServletResponse response, @CookieValue("groupMap") String groupData, @CookieValue("emailCookie") String emailId, @CookieValue("actionCode") String actionCode) throws Exception {
	Map<String, Object> result = new HashMap<String, Object>();
	ApplicationUser user = ApplicationUserServices.getUserInfoWithoutGroupId(emailId);
	List<Map<String, Object>> masterRolesBasedDetails = AccessConfigBso.fetchAllMasterRolesBasedDetails(user.getUnqUserId());
	PersonaMaster personaMaster = AccessConfigBso.fetchLastUpdatePersonaName(user.getLastUsedPersonaId());

	if (null == personaMaster) {
	    personaMaster = AccessConfigBso.fetchMaxFromPersonaId();
	}
	List<Object> masterRoleObj = new ArrayList<Object>();
	for (Map<String, Object> masterRoles : masterRolesBasedDetails) {
	    masterRoleObj.add(masterRoles.get("actionCode"));
	}
	if (StringUtils.isNotEmpty(emailId)) {
	    // TODO READ DATA FROM COOKIES AND SET IT
	    result.put("MASTERS_CODES", masterRoleObj);
	    result.put("ACTIONS_CODES", actionCode.split(","));
	    result.put("PERSONA_NAME", personaMaster.getPersona_name());
	    JSONObject jsonObject = new JSONObject("{ \"data\" : " + groupData + "}");
	    List<HashMap<Object, Object>> objectList = ObjectSerializationHandler.toObjectList(jsonObject.getString("data") + "", Map.class, new HashMap<>());
	    result.put("GROUP_DETAILS", objectList);
	}
	return result;
    }

    /*
     * @Deprecated
     * 
     * @SuppressWarnings("unchecked") private static void manageCookies(
     * Cookie[] cookies, ApplicationUser user, HttpServletResponse response,
     * Map<String, Object> result) throws SystemException {
     * 
     * Map<String, String> groupSubgroupRoleDetails = (Map<String, String>)
     * AccessConfigBso.getGroupSubgroupRoleDetails(user.getEmailId(),
     * 1l).get(0);
     * 
     * @SuppressWarnings("unused") Map<String, String> userCookie = new
     * HashMap<String, String>(); if (cookies != null) { for (Cookie cookie :
     * cookies) { String cookieName = cookie.getName(); if
     * (StringUtils.equals(cookieName, "subGroupIds")) {
     * cookie.setValue(groupSubgroupRoleDetails.get("subGroupIdList"));
     * response.addCookie(cookie); } else if (StringUtils.equals(cookieName,
     * "roleNames")) {
     * cookie.setValue(groupSubgroupRoleDetails.get("roleNameList"));
     * response.addCookie(cookie); } else if (StringUtils.equals(cookieName,
     * "actionCode")) { String actionsCodes =
     * groupSubgroupRoleDetails.get("actionCodeList");
     * result.put("ACTIONS_CODES", actionsCodes.split(","));
     * cookie.setValue(actionsCodes); response.addCookie(cookie); } else if
     * (StringUtils.equals(cookieName, "subGroupNames")) {
     * cookie.setValue(groupSubgroupRoleDetails.get("subGroupNameList"));
     * response.addCookie(cookie); } else if (StringUtils.equals(cookieName,
     * "emailCookie")) { cookie.setValue(user.getEmailId());
     * response.addCookie(cookie); } else if (StringUtils.equals(cookieName,
     * "userId")) { cookie.setValue(new String(user.getFirstName() + " " +
     * user.getLastName())); response.addCookie(cookie); } else if
     * (StringUtils.equals(cookieName, "groupId")) {
     * cookie.setValue(user.getGroupId() + ""); response.addCookie(cookie); } }
     * // response.addCookie(cookie); } }
     */

    @RequestMapping(value = "/fetchNewRoleList", method = { RequestMethod.GET, RequestMethod.POST })
    public Map<String, Object> fetchNewRoleList(HttpServletRequest request, HttpServletResponse response, @CookieValue("emailCookie") String emailId) throws SystemException, JsonParseException, JsonMappingException, IOException, JSONException {
	List<? extends Object> groupSubgroupRoleDetails = AccessConfigBso.getGroupSubgroupRoleDetails(emailId, 1l);
	ApplicationUser user = ApplicationUserServices.getUserBasedOnEmailId(emailId);
	Map<String, Object> result = new HashMap<String, Object>();
	@SuppressWarnings("unchecked")
	Map<String, String> groupSubgroupRoleDetail = (Map<String, String>) groupSubgroupRoleDetails.get(0);
	String actionsCodes = groupSubgroupRoleDetail.get("actionCodeList");
	Cookie userActionCodesCookie = new Cookie("actionCode", StringUtils.isNotEmpty(actionsCodes) && !StringUtils.equalsIgnoreCase(actionsCodes, "null") ? actionsCodes : "");
	result.put("ACTIONS_CODES", StringUtils.isNotEmpty(actionsCodes) && !StringUtils.equalsIgnoreCase(actionsCodes, "null") ? actionsCodes.split(",") : "");
	SessionRegistry.addRoles(emailId, UserManagementBso.fetchRoleIdsForUser(user.getUnqUserId()));
	response.addCookie(userActionCodesCookie);

	return result;
    }

    /*
     * @RequestMapping( value = "/fetchAllMasterRolesBasedDetails", method =
     * {RequestMethod.GET} ) public List<Map<String, Object>>
     * fetchAllMasterRolesBasedDetails(@RequestParam Long unqUserId) throws
     * Exception { return
     * AccessConfigBso.fetchAllMasterRolesBasedDetails(unqUserId); }
     */

    @RequestMapping(value = "/fetchAllMasterRolesUsingNameDetails", method = { RequestMethod.POST })
    public Map<String, Object> fetchAllMasterRolesUsingNameDetails(@RequestBody String masterRoleName, @CookieValue("userId") Long userId) throws Exception {
	Map<String, Object> result = new HashMap<String, Object>();
	List<Map<String, Object>> masterRoleDetails = AccessConfigBso.fetchAllMasterRolesUsingNameDetails(masterRoleName);
	List<Object> masterRoleObj = new ArrayList<Object>();
	for (Map<String, Object> masterRoles : masterRoleDetails) {
	    masterRoleObj.add(masterRoles.get("actionCode"));
	}
	AccessConfigBso.updateLastUpdatePersonaId(masterRoleName, userId);

	result.put("MASTERS_CODES", masterRoleObj);

	result.put("PERSONA_NAME", masterRoleName);

	return result;
    }
}
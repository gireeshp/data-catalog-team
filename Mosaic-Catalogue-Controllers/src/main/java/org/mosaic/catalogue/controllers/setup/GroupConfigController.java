package org.mosaic.catalogue.controllers.setup;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.setup.domains.QueuesMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.UserGroup;
import com.augmentiq.maxiq.entity.model.setup.domains.UserSubGroups;
import com.augmentiq.maxiq.services.setup.bso.AccessConfigBso;
import com.augmentiq.maxiq.services.setup.bso.GroupConfigBso;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;

@RestController
@RequestMapping("/groupConfig")
public class GroupConfigController extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(GroupConfigController.class);

    @Autowired
    private ConfigurationCreateUtil createUtil;

    @RequestMapping(value = "/getGroupsData", method = { RequestMethod.POST })
    public Map<String, Object> getGroupsData() throws Exception {
	Map<String, Object> groupSgroupData = new HashMap<String, Object>();
	groupSgroupData.put("GROUPS", GroupConfigBso.fetchGroups());
	groupSgroupData.put("SUBGROUPS", GroupConfigBso.fetchSubGroups());
	return groupSgroupData;
    }

    @RequestMapping(value = "/fetchAvailableQueues", method = { RequestMethod.POST })
    public List<QueuesMaster> fetchAvailableQueues() throws SystemException, IOException {
	return GroupConfigBso.fetchAvailableQueues();
    }

    @RequestMapping(value = "/fetchAvailableGroups", method = { RequestMethod.POST })
    public List<Map<String, Object>> fetchAvailableGroups() throws SystemException, IOException {
	return GroupConfigBso.fetchAvailableGroups();
    }

    @RequestMapping(value = "/createGroupAction", method = { RequestMethod.POST })
    public Long createGroupAction(@CookieValue("userId") String userId, @RequestBody UserGroup userGroup) throws Exception {
	userGroup.setInsertedBy(userId);
	Long usergroupId = GroupConfigBso.createGroupAction(userGroup);

	if (null != usergroupId && usergroupId > 0) {
	    try {
		Long repoId = createUtil.createDataRepository(userGroup.getGroupName() + CHAR_CONSTANTS.UNDER_SCORE + "GlobalRepo", userId, usergroupId, null);
	    } catch (SystemException e) {
		logger.error("{}", e.getMessage());
	    }
	}
	return usergroupId;
    }

    @RequestMapping(value = "/createSubgroupAction", method = { RequestMethod.POST })
    public void createSubgroupAction(@CookieValue("userId") String userId, @RequestBody UserSubGroups userSubGroups) throws Exception {
	userSubGroups.setInsertedBy(userId);
	userSubGroups.setInsertTs(DateUtils.getCurrentTimestamp().toString());
	userSubGroups.setUpdateTs(QueryConstants.DEFAULT_TIMESTAMP);
	// userSubGroups.setUpdateTs("2017-09-21 11:55:05");
	GroupConfigBso.createSubgroupAction(userSubGroups);
    }

    @RequestMapping(value = "/updateSubgroupAction", method = { RequestMethod.POST })
    public void updateSubgroupAction(@CookieValue("userId") String userId, @RequestBody UserSubGroups userSubGroups) throws Exception {
	userSubGroups.setUpdatedBy(userId);
	userSubGroups.setUpdateTs(DateUtils.getCurrentTimestamp().toString());
	GroupConfigBso.upadteSubgroupAction(userSubGroups);

    }

    @RequestMapping(value = "/deleteGroup", method = { RequestMethod.POST })
    public void createGroupAction(@CookieValue("userId") String userId, @RequestBody Long groupId) throws Exception {
	UserGroup userGroup = new UserGroup();
	userGroup.setGroupId(groupId);
	GroupConfigBso.deleteGroup(userGroup, userId);
    }

    @RequestMapping(value = "/deleteSubgroup", method = { RequestMethod.POST })
    public void deleteSubgroup(@CookieValue("userId") String userId, @RequestBody Long subgroupId) throws Exception {
	UserSubGroups userSubgroup = new UserSubGroups();
	userSubgroup.setSubGroupId(subgroupId);
	GroupConfigBso.deleteSubGroup(userSubgroup, userId);
    }

    @RequestMapping(value = "/updateGroupAction", method = { RequestMethod.POST })
    public void updateGroupAction(@CookieValue("userId") String userId, @RequestBody UserGroup userGroup) throws Exception {
	userGroup.setUpdatedBy(userId);
	GroupConfigBso.updateGroupAction(userGroup);
    }

    @RequestMapping(value = "/getQueueName", method = { RequestMethod.POST })
    public String getQueueNameFromGroupId(@CookieValue("groupId") String groupId) throws NumberFormatException, SystemException {
	String queueNameByGroupId = AccessConfigBso.getQueueNameByGroupId(Long.parseLong(groupId));
	return queueNameByGroupId;
    }
}
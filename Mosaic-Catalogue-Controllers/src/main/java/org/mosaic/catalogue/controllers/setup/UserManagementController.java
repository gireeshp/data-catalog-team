package org.mosaic.catalogue.controllers.setup;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.EntityNameAlreadyExistsException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.model.setup.domains.ManageUser;
import com.augmentiq.maxiq.services.setup.bso.UserManagementBso;
import com.augmentiq.maxiq.util.session.registry.SessionRegistry;

@RestController
@RequestMapping("/userManagement")
public class UserManagementController extends AjaxErrorHandler {

    // required
    @RequestMapping(value = "/getUserDetails", method = { RequestMethod.GET })
    public List<Map<String, Object>> getUserDetails() throws Exception {
	return UserManagementBso.getUserDetails();
    }

    // required
    @RequestMapping(value = "/fetchSolutionsForUser", method = { RequestMethod.POST })
    public List<Map<String, Object>> fetchSolutionsForUser(@RequestBody String unqUserId) throws SystemException, IOException {
	return UserManagementBso.fetchSolutionsForUser(unqUserId);
    }

    // required
    @RequestMapping(value = "/fetchGroupSubgroupRoles", method = { RequestMethod.GET })
    public List<Map<String, Object>> fetchGroupSubgroupRoles() throws SystemException, IOException {
	return UserManagementBso.fetchGroupSubgroupRoles();
    }

    // required
    @RequestMapping(value = "/fetchAllPersonaDetails", method = { RequestMethod.POST })
    public List<PersonaMaster> fetchAllPersonaDetails(@CookieValue("userId") String userId) throws SystemException, IOException {
	return UserManagementBso.fetchAllPersonaDetails(userId);
    }

    // required

    @RequestMapping(value = "/createUserAction", method = { RequestMethod.POST })
    public void createUserAction(@CookieValue("userId") String userId, @CookieValue("clientId") Long clientId, @RequestBody ManageUser manageUser) throws SystemException, IOException, EntityNameAlreadyExistsException {
	UserManagementBso.createUser(userId, manageUser, clientId);
    }

    // required
    @RequestMapping(value = "/deleteUser", method = { RequestMethod.POST })
    public void deleteUser(@CookieValue("userId") String deletedBy, @RequestBody Long userId) throws SystemException, IOException {
	UserManagementBso.deleteUser(userId, deletedBy);
    }

    // required
    @RequestMapping(value = "/updateUser", method = { RequestMethod.POST })
    public void updateUser(@CookieValue("userId") String userId, @RequestBody ManageUser manageUser, @RequestParam String indicator, HttpServletRequest request, HttpServletResponse response, @CookieValue("clientId") Long clientId) throws SystemException, IOException, EntityNameAlreadyExistsException {
	UserManagementBso.updateUser(userId, manageUser, indicator, clientId);
	SessionRegistry.removeSession(manageUser.getUserDetails().getEmailId(), null);
    }

    // required
    @RequestMapping(value = "/getAllUserDetails", method = { RequestMethod.POST, RequestMethod.GET })
    public List<ApplicationUser> getAllUserDetails() throws SystemException {
	List<ApplicationUser> allUserDetails = UserManagementBso.getAllUserDetails();
	return allUserDetails;
    }
}

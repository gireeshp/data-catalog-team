package org.mosaic.catalogue.controllers.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.mosaic.catalogue.controllers.session.mgmt.SessionKeys;

import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.util.session.registry.SessionRegistry;

public class SessionController extends AjaxErrorHandler {

    public void setAttributeIntoSession(HttpServletRequest httpRequest, String key, Object value) {
	HttpSession httpSession = httpRequest.getSession();
	if (httpSession != null) {
	    httpSession.setAttribute(key, value);
	}
    }

    public static void removeAttributeIntoSession(HttpServletRequest httpRequest, String key) {
	HttpSession httpSession = httpRequest.getSession();
	if (httpSession != null) {
	    httpSession.removeAttribute(key);
	}
    }

    public Object getAttributeFromSession(HttpServletRequest httpRequest, String key) {
	HttpSession httpSession = httpRequest.getSession();
	Object value = null;
	if (httpSession != null) {
	    value = httpSession.getAttribute(key);
	}
	return value;
    }

    public void createNewSession(HttpServletRequest request) throws Exception {

	HttpSession httpSession = request.getSession(false);

	if (httpSession != null) {
	    ApplicationUser user = (ApplicationUser) getAttributeFromSession(request, SessionKeys.USER);
	    if (user == null)
		httpSession.invalidate();
	}
    }

    public Boolean checkConcurrentSession(String email) {
	return SessionRegistry.isSessionAvailable(email);
    }
}

package org.mosaic.catalogue.controllers.user;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.mosaic.catalogue.controllers.session.mgmt.SessionKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.constant.maxiq.MessageConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionMessages;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.cache.support.SharedVariables;
import com.augmentiq.maxiq.configuration.setupdao.UserManagementDao;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.cache.SharedConstants;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.core.dao.userdao.ApplicationUserDao;
import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.UserGroup;
import com.augmentiq.maxiq.entity.model.setup.domains.UserRoleMaster;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.services.setup.bso.AccessConfigBso;
import com.augmentiq.maxiq.services.setup.bso.UserManagementBso;
import com.augmentiq.maxiq.services.userservice.ApplicationUserServices;
import com.augmentiq.maxiq.util.session.registry.SessionRegistry;

@RestController
public class SignInController extends SessionController {

    private static final Logger logger = LoggerFactory.getLogger(SignInController.class);

    @RequestMapping(value = "/getUserNameFromId", method = RequestMethod.POST)
    public @ResponseBody String getUserNameFromId(@RequestBody String userId) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserInfo() " + ParamUtils.getString(userId));
	return ApplicationUserServices.getUserName(userId);
    }

    @RequestMapping(value = "/getGlobalData", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> getGlobalData() throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getGlobalData() ");
	Map<String, Object> map = new HashMap<>();
	map.put(CacheConstants.USER_GUIDE_LINK, Cache.getProperty(CacheConstants.USER_GUIDE_LINK));
	map.put(CacheConstants.WHATS_NEW_RELEASE_NOTES, Cache.getProperty(CacheConstants.WHATS_NEW_RELEASE_NOTES));
	return map;
    }

    @RequestMapping(value = "/updateGroupNameInApplicationUser", method = { RequestMethod.POST })
    public @ResponseBody void updateGroupNameInApplicationUser(HttpServletRequest request, HttpServletResponse response, @CookieValue("emailCookie") String emailCookie, @CookieValue("userId") Long userId, @RequestBody Long groupId) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateGroupNameInApplicationUser() " + ParamUtils.getString(userId, groupId));
	if (groupId != null) {
	    // Update GropuID in DB
	    ApplicationUserDao.updateGroupNameInApplicationUser(userId, groupId);
	    // Update GroupId in Cookies
	    Cookie userGroupIdCookie = new Cookie("groupId", String.valueOf(groupId));
	    response.addCookie(userGroupIdCookie);
	    // Update GroupId in Session
	    ApplicationUser user = ApplicationUserServices.getUserBasedOnEmailId(emailCookie);
	    setAttributeIntoSession(request, SessionKeys.USER, user);
	}
    }

    @RequestMapping(value = "/getPasswordExpiryDays", method = RequestMethod.POST)
    public @ResponseBody Long getPasswordExpiryDays(@RequestBody String email) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getPasswordExpiryDays() " + ParamUtils.getString(email));

	Long remainingDays = ApplicationUserServices.getRemainingDaysUsingEmail(email);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getPasswordExpiryDays() " + ParamUtils.getString(remainingDays));
	return remainingDays;
    }

    @RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
    public @ResponseBody ApplicationUser getUserInfo(@RequestBody Map<String, Object> reqDataObject) throws SystemException, ParseException {
	String email = reqDataObject.get(QueryConstants.Parameters.EMAIL) + "";
	Long groupId = Long.parseLong(reqDataObject.get(QueryConstants.Parameters.GROUP_ID) + "");

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserInfo() " + ParamUtils.getString(email, groupId));

	ApplicationUser applicationUser = ApplicationUserServices.getUserEmailGroupId(email, groupId);
	applicationUser.setGroupName(ApplicationUserServices.getGroupName(groupId));

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getUserInfo() " + ParamUtils.getString(applicationUser));
	return applicationUser;
    }

    /**
     * Do not change following method nor remove this method This method is used
     * for updating new CSRF token after login
     */
    @RequestMapping(value = "/getUpdatedKey", method = RequestMethod.GET)
    public @ResponseBody String getUpdatedKey(@RequestBody String test) throws SystemException, ParseException {
	return "Success";
    }

    @RequestMapping(value = "/updateSessionid", method = { RequestMethod.POST })
    public @ResponseBody String updateSessionid(@RequestBody Map<String, String> emailIdAndPassword, HttpServletRequest request, HttpServletResponse response) throws SystemException, ParseException {

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> loginUser() " + ParamUtils.getString(emailIdAndPassword));
	/** * Code Added for Session Changes Start */
	String flag = "";
	String emailId = emailIdAndPassword.get(Sequences.EMAILID);
	HttpSession session = request.getSession(false);
	ApplicationUser user = ApplicationUserServices.getUserBasedOnEmailId(emailId);

	if (session != null && !session.isNew()) {
	    session.invalidate();
	}
	session = request.getSession(true);

	if (checkConcurrentSession(user.getEmailId())) {
	    flag = "False";
	} else {
	    flag = "Success";
	}

	/*
	 * String alreadyUser = emailIdAndPassword.get(Sequences.EMAILID);
	 * HttpSession session = request.getSession(false); HttpSession
	 * sessionByEmail = SessionRegistry.getSessionByEmail(alreadyUser); if
	 * (sessionByEmail!=null) {
	 * //UserManagementBso.removeAttributeFromSession(request,
	 * SessionKeys.USER); SessionRegistry.removeSession(alreadyUser,
	 * SessionRegistry.getSession(alreadyUser)); }
	 */

	// SessionRegistry.addSessionByEmail(alreadyUser, session);
	return flag;
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> loginUser(@RequestBody Map<String, String> emailIdAndPassword, HttpServletRequest request, HttpServletResponse response) throws Throwable {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> loginUser() " + ParamUtils.getString(emailIdAndPassword));

	return commanSignin(emailIdAndPassword, request, response, QueryConstants.Parameters.MOSAIC_APPLICATION);
    }

    /**
     * Date 19-01-2018 This method is used for signin purpose for both mosaic
     * and other application
     *
     * @param emailIdAndPassword
     * @param request
     * @param response
     * @param source
     * @return
     */
    public Map<String, Object> commanSignin(Map<String, String> emailIdAndPassword, HttpServletRequest request, HttpServletResponse response, String source) {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> loginUser() " + ParamUtils.getString(emailIdAndPassword));

	Map<String, Object> result = new HashMap<String, Object>();

	try {

	    Integer isUserExistsInSystem = null;
	    Long appId = Long.parseLong(emailIdAndPassword.get(MessageConstants.APP_ID));

	    String emailId = emailIdAndPassword.get(Sequences.EMAILID);
	    String password = emailIdAndPassword.get(Sequences.PASSWORD);

	    if (StringUtils.equalsIgnoreCase(Cache.getProperty(CacheConstants.AUTH_METHOD), CacheConstants.AUTH_METHOD_MAXIQ)) {

		// for hashCoded password
		String pass = GenericStoreDao.encodedPassword(password);

		// check if userid & password matches in database
		isUserExistsInSystem = ApplicationUserServices.checkIfUserAlreadyInSystem(emailId, pass, appId);

	    } else {
		result.put("RESULT", ExceptionsMessanger.msg(ExceptionMessages.INVALID_AUTH_METHOD));
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
		return result;
	    }

	    ApplicationUser appUserProfile = GenericStoreDao.getUserProfile(emailId, appId);
	    if (0 == isUserExistsInSystem) {
		// reset login failure count after succesful login.
		ApplicationUserServices.insertIntoAudit(appUserProfile);
		ApplicationUserServices.resetLoginFailureCnt(emailId);
		// check if user is in active mode
		if (!ApplicationUserServices.checkIfUserActive(emailId)) {
		    result.put(Sequences.RESULT, "User is not active");
		    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
		    return result;
		}

		return readyForSignIn(request, response, result, emailId, appId, source);

	    } else if (isUserExistsInSystem == 1) {
		result.put(Sequences.RESULT, ExceptionsMessanger.msg(ExceptionMessages.USER_NOT_REGISTERED));
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
		return result;
	    } else if (isUserExistsInSystem == 2) {
		Long loginFailureCnt = appUserProfile.getLoginFailureCount();

		Long finalCount = (Long.parseLong(Cache.getProperty(CacheConstants.LOGIN_FAILURE_CNT)) - (loginFailureCnt + 1));

		result.put(Sequences.RESULT, ExceptionsMessanger.msg(ExceptionMessages.INVALID_EMAIL_PWD + finalCount + ExceptionMessages.INVALID_EMAIL_PWD_COUNT_LEFT));
		Long diffMinutes = ApplicationUserServices.getTimeStampDiffFromLastFailure(appUserProfile);
		// Long diff = LastFailureAttemptTs.get - todaysDate;
		if (diffMinutes > Long.parseLong(Cache.getProperty(CacheConstants.LOGIN_FAILURE_CNT_RESET_TS))) {
		    ApplicationUserServices.resetLoginFailureCnt(emailId);
		    ApplicationUserServices.setLoginFailureCnt(emailId, loginFailureCnt + 1);
		} else {
		    ApplicationUserServices.setLoginFailureCnt(emailId, loginFailureCnt + 1);
		}
		ApplicationUserServices.insertIntoAudit(appUserProfile);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
		return result;
	    } else if (isUserExistsInSystem == 3) {
		result.put(Sequences.RESULT, ExceptionsMessanger.msg(ExceptionMessages.LOGIN_MAX_TRY));
		ApplicationUserServices.insertIntoAudit(appUserProfile);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
		return result;
	    } else if (isUserExistsInSystem == 4) {
		result.put(Sequences.RESULT, ExceptionsMessanger.msg(ExceptionMessages.USER_LOCK));
		ApplicationUserServices.insertIntoAudit(appUserProfile);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
		return result;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    result.put(Sequences.RESULT, e.getMessage());
	}
	return result;
    }

    private Map<String, Object> readyForSignIn(HttpServletRequest request, HttpServletResponse response, Map<String, Object> result, String emailId, Long appId, String source) throws Exception {
	logger.debug("Class SigninController Method : readyForSignIn Start here.");
	// check password expiry of user
	Integer isPasswordExpired = ApplicationUserServices.checkIfPasswordExpiredOrNot(emailId);

	// Checking for newly created user as well is password expired.
	if (2 == isPasswordExpired) {
	    result.put(Sequences.RESULT, "ResetPassword");
	    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
	    return result;
	} else if (1 == isPasswordExpired) {
	    result.put(Sequences.RESULT, "Your Password got expired please change your password");
	    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << loginUser() " + ParamUtils.getString(result));
	    return result;
	}
	if (source.equals(QueryConstants.Parameters.MOSAIC_APPLICATION)) {
	    HttpSession session = request.getSession(false);
	    ApplicationUser appuser = ApplicationUserServices.getUserBasedOnEmailId(emailId);

	    if (session != null && !session.isNew()) {
		session.invalidate();
	    }

	    if (checkConcurrentSession(appuser.getEmailId())) {
		result.put(Sequences.RESULT, GENERAL_CONSTANTS.ALREADYSESSION);
		return result;
	    }

	    session = request.getSession(true);
	}

	// intercept request and lock browser , ip , platform
	ApplicationUser user = ApplicationUserServices.getUserBasedOnEmailId(emailId);

	ApplicationUserServices.lockUserIp(request, user);
	ApplicationUserServices.lockUserBrowser(request, user);
	ApplicationUserServices.lockUserOs(request, user);

	// insert new information of application user
	GenericStoreDao.insertNewUserProfile(user);

	// apply cookie on response
	if (source.equals(QueryConstants.Parameters.MOSAIC_APPLICATION))
	    manageCookies(user, appId, response, result);

	// allow user to login
	result.put(Sequences.RESULT, "Success");
	result.put("userId", user.getUnqUserId());

	emailId = user.getEmailId();
	if (source.equals(QueryConstants.Parameters.MOSAIC_APPLICATION)) {
	    SessionRegistry.addRoles(emailId, UserManagementBso.fetchRoleIdsForUser(user.getUnqUserId()));
	    try {
		SessionRegistry.addSession(emailId, request.getSession(false).getId());
	    } catch (Throwable e) {
		// If session already invalidated
	    }
	    SessionRegistry.addSessionByEmail(emailId, request.getSession());
	    setAttributeIntoSession(request, SessionKeys.USER, user);
	} else if (source.equals(QueryConstants.Parameters.OTHER_APPLICATION)) {
	    List<UserRoleMaster> userRole = UserManagementDao.getUserRoleDetails(user.getUnqUserId());
	    List<UserGroup> userGroup = UserManagementDao.getUserGroupDetails(user.getUnqUserId());
	    result.put("userRole", userRole);
	    result.put("userGroup", userGroup);
	}
	logger.debug("Class SigninController Method : readyForSignIn End here. result = " + result);
	return result;
    }

    @RequestMapping(value = "/signout", method = RequestMethod.POST)
    public @ResponseBody void logoutUser(@CookieValue(value = "emailCookie") String emailId, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	ApplicationUser user = ApplicationUserServices.getUserInfoWithoutGroupId(emailId);

	if (null != user) {
	    emailId = user.getEmailId();
	}

	/*
	 * HttpSession sessionByEmail =
	 * SessionRegistry.getSessionByEmail(emailId);
	 * sessionByEmail.removeAttribute(SessionKeys.USER);
	 */
	UserManagementBso.removeAttributeFromSession(request, SessionKeys.USER);
	SessionRegistry.removeSession(emailId, request.getSession(false).getId());

	/** Code Changes for Session change End */

	// HttpSession session = request.getSession(false);
	if (session != null && !session.isNew()) {
	    session.invalidate();
	}
	session = request.getSession(true);

	/** Code Changes for Session change End */
    }

    @RequestMapping(value = "/cleanInActiveUser", method = RequestMethod.POST)
    public @ResponseBody void cleanInActiveUser(@RequestParam String emailId, HttpServletRequest request, HttpServletResponse response) throws Exception {
	ApplicationUser user = (ApplicationUser) request.getSession(false).getAttribute(SessionKeys.USER);
	if (null == user) {
	    user = ApplicationUserServices.getUserInfoWithoutGroupId(emailId);

	    if (null != user) {
		emailId = user.getEmailId();
	    }

	    UserManagementBso.removeAttributeFromSession(request, SessionKeys.USER);
	    SessionRegistry.removeSession(emailId, request.getSession(false).getId());
	}
    }

    @RequestMapping(value = "/resetPasswordUser", method = RequestMethod.POST)
    public @ResponseBody void resetPasswordUser(@RequestParam String emailId, @RequestBody Map<String, String> detailsOfPassword) throws Exception {
	if (StringUtils.isNotBlank(emailId)) {
	    ApplicationUserServices.resetPasswordUser(emailId, detailsOfPassword);
	}
    }

    private static void manageCookies(ApplicationUser user, Long appId, HttpServletResponse response, Map<String, Object> result) throws Exception {

	List<Map<String, Object>> masterRolesBasedDetails = AccessConfigBso.fetchAllMasterRolesBasedDetails(user.getUnqUserId());
	List<Object> masterRoleObj = new ArrayList<Object>();
	for (Map<String, Object> masterRoles : masterRolesBasedDetails) {
	    masterRoleObj.add(masterRoles.get("actionCode"));
	}
	PersonaMaster personaMaster = AccessConfigBso.fetchLastUpdatePersonaName(user.getLastUsedPersonaId());
	if (null == personaMaster) {
	    personaMaster = AccessConfigBso.fetchMaxFromPersonaId();
	}

	List<? extends Object> groupSubgroupRoleDetails = AccessConfigBso.getGroupSubgroupRoleDetails(user.getEmailId(), appId);

	// Client id
	Long clientId = ApplicationUserServices.getClientId(user.getUnqUserId());

	@SuppressWarnings("unchecked")
	Map<String, String> groupSubgroupRoleDetail = (Map<String, String>) groupSubgroupRoleDetails.get(0);

	String groupIds = groupSubgroupRoleDetail.get("groupIdList");

	String groupList = groupSubgroupRoleDetail.get("groupNameList");
	List<Map<String, Object>> groupMap = null;
	if (StringUtils.isNotEmpty(groupList) && !StringUtils.equalsIgnoreCase(groupList, "null")) {
	    groupMap = ApplicationUserServices.getGroupMap(groupList, groupIds, user);
	    result.put("GROUP_DETAILS", groupMap);
	} else {
	    result.put("GROUP_DETAILS", "");
	}

	Cookie userGroupMapCookie = new Cookie("groupMap", null != groupMap ? groupMap + "" : "");

	String actionsCodes = groupSubgroupRoleDetail.get("actionCodeList");

	Cookie userActionCodesCookie = new Cookie("actionCode", StringUtils.isNotEmpty(actionsCodes) && !StringUtils.equalsIgnoreCase(actionsCodes, "null") ? actionsCodes : "");

	result.put("ACTIONS_CODES", StringUtils.isNotEmpty(actionsCodes) && !StringUtils.equalsIgnoreCase(actionsCodes, "null") ? actionsCodes.split(",") : "");

	result.put("MASTERS_CODES", masterRoleObj);

	result.put("PERSONA_NAME", personaMaster.getPersona_name());

	Cookie userAuthEmailCookie = new Cookie("emailCookie", user.getEmailId());

	Cookie userAuthIdCookie = new Cookie("userId", user.getUnqUserId().toString());

	Cookie projectCookie = new Cookie("projectId", "-1");

	Cookie ClientId = new Cookie("clientId", clientId + "");

	String selectedGroupId = "";
	for (Map<String, Object> group : groupMap) {
	    String isTrue = group.get(GENERAL_CONSTANTS.ISSELECTED) + "";
	    if (isTrue.equalsIgnoreCase(GENERAL_CONSTANTS.TRUE)) {
		selectedGroupId = group.get(GENERAL_CONSTANTS.HIVE_QUERY_GROUP_ID) + "";
		break;
	    }
	}

	Cookie userGroupIdCookie = new Cookie("groupId", StringUtils.isNotBlank(groupIds) && !StringUtils.equalsIgnoreCase(groupIds, "null") ? selectedGroupId : "");

	response.addCookie(userAuthIdCookie);

	response.addCookie(userAuthEmailCookie);

	response.addCookie(projectCookie);

	response.addCookie(userGroupIdCookie);

	response.addCookie(userGroupMapCookie);

	response.addCookie(userActionCodesCookie);

	response.addCookie(ClientId);

	SharedVariables.set(SharedConstants.QUEUE_NAME, AccessConfigBso.getQueueNameByGroupId(Long.valueOf(userGroupIdCookie.getValue())));
    }

    @RequestMapping(value = "/killActiveSessionByEmailId", method = RequestMethod.POST)
    public @ResponseBody String checkAlreadyActiveSession(@RequestBody Map<String, String> emailIdAndPassword, HttpServletRequest request, HttpServletResponse response) throws Exception {
	Map<String, Object> result = new HashMap<String, Object>();
	String emailId = emailIdAndPassword.get(Sequences.EMAILID);

	ApplicationUser user = ApplicationUserServices.getUserInfoWithoutGroupId(emailId);

	if (null != user) {
	    emailId = user.getEmailId();
	}

	Long appId = Long.parseLong(emailIdAndPassword.get(MessageConstants.APP_ID));

	try {
	    HttpSession sessionByEmail = SessionRegistry.getSessionByEmail(emailId);
	    sessionByEmail.removeAttribute(SessionKeys.USER);
	    SessionRegistry.removeSession(emailId, SessionRegistry.getSession(emailId));
	    sessionByEmail.invalidate();
	} catch (Throwable e) {
	    // if session already terminated
	}

	return "Success"; // readyForSignIn(request, response, result, emailId,
			  // appId);
    }

    /**
     * /signinOtherApp added date: 11-01-2018
     *
     * @author Sunil Jagtap
     * @param emailIdAndPassword
     * @param request
     * @param response
     * @return
     * @throws Throwable
     *             /signinOtherApp controller add for other application sigin
     *             purpose.
     */
    @RequestMapping(value = "/signinOtherApp", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> loginUserOtherApp(@RequestBody Map<String, String> emailIdAndPassword, HttpServletRequest request, HttpServletResponse response) throws Throwable {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> signinOtherApp() " + ParamUtils.getString(emailIdAndPassword));
	return commanSignin(emailIdAndPassword, request, response, QueryConstants.Parameters.OTHER_APPLICATION);
    }

    @RequestMapping(value = "/mytest", method = RequestMethod.GET)
    public @ResponseBody String mytest() throws Throwable {
	return "worked";
    }
}

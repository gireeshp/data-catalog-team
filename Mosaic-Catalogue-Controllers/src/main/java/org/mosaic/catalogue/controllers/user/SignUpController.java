package org.mosaic.catalogue.controllers.user;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionMessages;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.UserStatus;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.services.userservice.ApplicationUserServices;

@Controller
public class SignUpController extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(SignUpController.class);

    @RequestMapping(value = "/getUser", method = { RequestMethod.POST }, produces = "application/json;charset=UTF-8")
    public @ResponseBody String checkUser(@RequestParam String email, HttpServletResponse res) throws SystemException, IOException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> checkUser() " + ParamUtils.getString(email, res));

	String errorMessage = null;
	if (!StringUtils.isBlank(email)) {
	    ApplicationUser applicationUser = GenericStoreDao.getUserProfile(email);
	    if (null != applicationUser) {
		errorMessage = ExceptionsMessanger.msg(ExceptionMessages.USER_ALREADY_EXISTS);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << checkUser() " + ParamUtils.getString(errorMessage));
		return errorMessage;
	    }
	}
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << checkUser() " + ParamUtils.getString(errorMessage));
	return errorMessage;
    }

    @RequestMapping(value = "/getUserDetails", method = { RequestMethod.POST }, produces = "application/json;charset=UTF-8")
    public @ResponseBody ApplicationUser getUserDetails(@RequestParam String email) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserDetails() " + ParamUtils.getString(email));

	ApplicationUser applicationUser = null;
	if (!StringUtils.isBlank(email)) {
	    applicationUser = GenericStoreDao.getUserProfile(email);
	    if (null != applicationUser) {
		return applicationUser;
	    }
	}
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getUserDetails() " + ParamUtils.getString());
	return null;
    }

    @RequestMapping(value = "/signup", method = { RequestMethod.POST }, produces = "application/json;charset=UTF-8")
    public @ResponseBody String signUpUser(@RequestBody ApplicationUser applicationUser, HttpServletRequest request, HttpServletResponse response) throws Exception {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> signUpUser() " + ParamUtils.getString(applicationUser, request, response));
	String result = "";

	if (null != applicationUser) {

	    // check if user already existed
	    ApplicationUser fromdbApplicationUser = GenericStoreDao.getUserProfile(applicationUser.getEmailId());
	    // create user's account which has been already deleted and he is
	    // trying to signup again with same email address
	    if (fromdbApplicationUser != null && fromdbApplicationUser.getStatus().equals(UserStatus.DELETED.name())) {
		fromdbApplicationUser.setPassword(GenericStoreDao.encodedPassword(applicationUser.getPassword()));
		fromdbApplicationUser.setStatus(UserStatus.INACTIVE.name());
		fromdbApplicationUser.setFirstName(applicationUser.getFirstName());
		fromdbApplicationUser.setLastName(applicationUser.getLastName());
		ApplicationUserServices.recreateSoftDeletedUser(fromdbApplicationUser);
		result = "Account created successfuly";
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << signUpUser() " + ParamUtils.getString(result));
		String passwordBackUp = fromdbApplicationUser.getPassword();

		return result;
	    }

	    if (null == fromdbApplicationUser) {

		ApplicationUserServices.lockUserIp(request, applicationUser);

		ApplicationUserServices.lockUserBrowser(request, applicationUser);

		ApplicationUserServices.lockUserOs(request, applicationUser);

		applicationUser.setUserId(applicationUser.getFirstName());

		applicationUser.setStatus(UserStatus.INACTIVE.name());

		// applicationUser.setUserCreatedDate(System.currentTimeMillis());
		applicationUser.setCreatedTs(DateUtils.getCurrentTimestamp() + "");
		applicationUser.setUpdatedTs(QueryConstants.DEFAULT_TIMESTAMP);

		String passwordBackUp = applicationUser.getPassword();
		// for hashCoded password
		applicationUser.setPassword(GenericStoreDao.encodedPassword(applicationUser.getPassword()));

		applicationUser.setPasswordexpirydays(Long.valueOf(Cache.getProperty(CacheConstants.PASS_EXP_DAYS)));

		applicationUser.setGroupId(0l);

		applicationUser.setGroupName("");

		applicationUser.setLastUsedPersonaId(0L);

		if (applicationUser.getLastFailureAttemptTs() == null || StringUtils.equalsIgnoreCase(applicationUser.getLastFailureAttemptTs(), "null")) {
		    applicationUser.setLastFailureAttemptTs("0000-00-00 00:00:00");
		    applicationUser.setLoginFailureCount(0L);
		}

		// insert the user

		GenericStoreDao.insertNewUserProfile(applicationUser);

		result = "Account created successfuly";
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << signUpUser() " + ParamUtils.getString(result));

		return result;
	    } else {

		ExceptionsMessanger.throwException(new SystemException(), "ERR_177");
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << signUpUser() " + ParamUtils.getString(result));
		return result;
	    }
	}

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << signUpUser() " + ParamUtils.getString(result));
	return result;
    }
}

package org.mosaic.catalogue.controllers.visualize;

import org.mosaic.catalogue.controllers.errors.AjaxErrorHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;

@RestController
public class VisualizeController extends AjaxErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(VisualizeController.class);

    @RequestMapping(value = "/getVisualizePath", method = { RequestMethod.POST })
    public @ResponseBody String getVisualizePath() throws SystemException {
	logger.debug("Getting kibana path from config.prpoerties");
	String kibanaPath = Cache.getProperty(CacheConstants.VISUALIZE_PATH);

	logger.debug("Path of kibana is " + kibanaPath);
	return kibanaPath;
    }
}

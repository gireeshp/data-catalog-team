package org.mosaic.catalogue.interceptors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class CookieInterceptor extends HandlerInterceptorAdapter {

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#
     * postHandle(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, java.lang.Object,
     * org.springframework.web.servlet.ModelAndView)
     */

    /*
     * (non-Javadoc)
     * 
     * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#
     * preHandle(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	System.out.println("In post Handle Cookie");
	return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	System.out.println("In post Handle Cookie");
	Boolean isAvailable = false;
	Cookie[] cookies = request.getCookies();

	for (Cookie cookie : cookies) {
	    if (cookie.getName().equalsIgnoreCase("userId")) {
		isAvailable = true;
		System.out.println("Cookies in Interceptor: " + isAvailable + "=> " + cookie.getValue());
	    }
	}

	if (!isAvailable) {
	    for (Object value : modelAndView.getModel().values()) {
		if (value instanceof ApplicationUser) {
		    System.out.println("Cookies in Interceptor: " + isAvailable + "=> " + ((ApplicationUser) value).getUserId());
		    Cookie maxIqCookie = new Cookie("userId", ((ApplicationUser) value).getUserId());
		    response.addCookie(maxIqCookie);
		}
	    }
	}
    }
}

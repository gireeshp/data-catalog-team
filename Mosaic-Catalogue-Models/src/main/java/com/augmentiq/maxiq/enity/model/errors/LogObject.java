package com.augmentiq.maxiq.enity.model.errors;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;
import com.augmentiq.maxiq.constant.models.errors.enums.ErrorTypes;

@TableName(tableName = Sequences.ERRORED_RECORDS)
public class LogObject {
  @Id private Long id;
  private ErrorTypes errorType;
  private String erroredRecord;
  private Long jobId;
  private String errorField;

  public LogObject() {}

  public LogObject(String erroredRecord_, String errorField_, ErrorTypes errorType_, Long jobId_) {
    this.erroredRecord = erroredRecord_;
    this.errorField = errorField_;
    this.errorType = errorType_;
    this.jobId = jobId_;
  }

  public ErrorTypes getErrorType() {
    return errorType;
  }

  public void setErrorType(ErrorTypes errorType) {
    this.errorType = errorType;
  }

  public String getErroredRecord() {
    return erroredRecord;
  }

  public void setErroredRecord(String erroredRecord) {
    this.erroredRecord = erroredRecord;
  }

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

  public String getErrorField() {
    return errorField;
  }

  public void setErrorField(String errorField) {
    this.errorField = errorField;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return "LogObject [id="
        + id
        + ", errorType="
        + errorType
        + ", erroredRecord="
        + erroredRecord
        + ", jobId="
        + jobId
        + ", errorField="
        + errorField
        + "]";
  }
}

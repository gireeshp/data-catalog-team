package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.constant.configuration.enums.AdvancedTransformationTypes;
public class APIDomain implements Serializable {

  private List<Integer> outputFieldsPos_;
  private String outDel_;
  private Map<Integer, List<Integer>> inFieldPosForApi_;
  private String[] transformFnInputTypes_;

  private String name_;
  private String initFn_;
  private String transformFn_;
  private AdvancedTransformationTypes advancedTransformationTypes_;

  public AdvancedTransformationTypes getAdvancedTransformationTypes() {
    return advancedTransformationTypes_;
  }

  public void setAdvancedTransformationTypes(
      AdvancedTransformationTypes advancedTransformationTypes) {
    advancedTransformationTypes_ = advancedTransformationTypes;
  }

  public String getName() {
    return name_;
  }

  public void setName(String name) {
    name_ = name;
  }

  public List<Integer> getOutputFieldsPos() {
    return outputFieldsPos_;
  }

  public void setOutputFieldsPos(List<Integer> outputFieldsPos) {
    outputFieldsPos_ = outputFieldsPos;
  }

  public String getOutDel() {
    return outDel_;
  }

  public void setOutDel(String outDel) {
    outDel_ = outDel;
  }

  public String getInitFn() {
    return initFn_;
  }

  public void setInitFn(String initFn) {
    initFn_ = initFn;
  }

  public String[] getTransformFnInputTypes() {
    return transformFnInputTypes_;
  }

  public void setTransformFnInputTypes(String[] transformFnInputTypes) {
    transformFnInputTypes_ = transformFnInputTypes;
  }

  public String getTransformFn() {
    return transformFn_;
  }

  public void setTransformFn(String transformFn) {
    transformFn_ = transformFn;
  }

  public Map<Integer, List<Integer>> getInFieldPosForApi() {
    return inFieldPosForApi_;
  }

  public void setInFieldPosForApi(Map<Integer, List<Integer>> inFieldPosForApi) {
    inFieldPosForApi_ = inFieldPosForApi;
  }

  public APIDomain() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "APIDomain [outputFieldsPos_="
        + outputFieldsPos_
        + ", outDel_="
        + outDel_
        + ", inFieldPosForApi_="
        + inFieldPosForApi_
        + ", transformFnInputTypes_="
        + Arrays.toString(transformFnInputTypes_)
        + ", name_="
        + name_
        + ", initFn_="
        + initFn_
        + ", transformFn_="
        + transformFn_
        + ", advancedTransformationTypes_="
        + advancedTransformationTypes_
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result =
        prime * result
            + ((advancedTransformationTypes_ == null)
                ? 0
                : advancedTransformationTypes_.hashCode());
    result = prime * result + ((inFieldPosForApi_ == null) ? 0 : inFieldPosForApi_.hashCode());
    result = prime * result + ((initFn_ == null) ? 0 : initFn_.hashCode());
    result = prime * result + ((name_ == null) ? 0 : name_.hashCode());
    result = prime * result + ((outDel_ == null) ? 0 : outDel_.hashCode());
    result = prime * result + ((outputFieldsPos_ == null) ? 0 : outputFieldsPos_.hashCode());
    result = prime * result + Arrays.hashCode(transformFnInputTypes_);
    result = prime * result + ((transformFn_ == null) ? 0 : transformFn_.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    APIDomain other = (APIDomain) obj;
    if (advancedTransformationTypes_ != other.advancedTransformationTypes_) return false;
    if (inFieldPosForApi_ == null) {
      if (other.inFieldPosForApi_ != null) return false;
    } else if (!inFieldPosForApi_.equals(other.inFieldPosForApi_)) return false;
    if (initFn_ == null) {
      if (other.initFn_ != null) return false;
    } else if (!initFn_.equals(other.initFn_)) return false;
    if (name_ == null) {
      if (other.name_ != null) return false;
    } else if (!name_.equals(other.name_)) return false;
    if (outDel_ == null) {
      if (other.outDel_ != null) return false;
    } else if (!outDel_.equals(other.outDel_)) return false;
    if (outputFieldsPos_ == null) {
      if (other.outputFieldsPos_ != null) return false;
    } else if (!outputFieldsPos_.equals(other.outputFieldsPos_)) return false;
    if (!Arrays.equals(transformFnInputTypes_, other.transformFnInputTypes_)) return false;
    if (transformFn_ == null) {
      if (other.transformFn_ != null) return false;
    } else if (!transformFn_.equals(other.transformFn_)) return false;
    return true;
  }
}

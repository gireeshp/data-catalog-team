package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;

import com.augmentiq.maxiq.constant.configuration.enums.EvaluationCondition;

/**
 * * This class contains the advanced validations to be applied on the data. Contains the list of
 * fields and the validdation to be applied.
 *
 * @author shiva
 */
public class AdvancedValidations implements Serializable {
  private Long id;
  private List<FieldMapping> fields;
  private List<Long> fieldsStore;
  private EvaluationCondition evalCondition;
  private List<DataFilter> dataFilters;
  private List<DataValidation> dataValidations;

  public List<FieldMapping> getFields() {
    return fields;
  }

  public void setFields(List<FieldMapping> fields) {
    this.fields = fields;
  }

  public EvaluationCondition getEvalCondition() {
    return evalCondition;
  }

  public void setEvalCondition(EvaluationCondition evalCondition) {
    this.evalCondition = evalCondition;
  }

  public List<DataFilter> getDataFilters() {
    return dataFilters;
  }

  public void setDataFilters(List<DataFilter> dataFilters) {
    this.dataFilters = dataFilters;
  }

  public List<DataValidation> getDataValidations() {
    return dataValidations;
  }

  public void setDataValidations(List<DataValidation> dataValidations) {
    this.dataValidations = dataValidations;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public AdvancedValidations() {}

  public List<Long> getFieldsStore() {
    return fieldsStore;
  }

  public void setFieldsStore(List<Long> fieldsStore) {
    this.fieldsStore = fieldsStore;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dataFilters == null) ? 0 : dataFilters.hashCode());
    result = prime * result + ((dataValidations == null) ? 0 : dataValidations.hashCode());
    result = prime * result + ((evalCondition == null) ? 0 : evalCondition.hashCode());
    result = prime * result + ((fields == null) ? 0 : fields.hashCode());
    result = prime * result + ((fieldsStore == null) ? 0 : fieldsStore.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public String toString() {
    return "AdvancedValidations [id="
        + id
        + ", fields="
        + fields
        + ", fieldsStore="
        + fieldsStore
        + ", evalCondition="
        + evalCondition
        + ", dataFilters="
        + dataFilters
        + ", dataValidations="
        + dataValidations
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AdvancedValidations other = (AdvancedValidations) obj;
    if (dataFilters == null) {
      if (other.dataFilters != null) return false;
    } else if (!dataFilters.equals(other.dataFilters)) return false;
    if (dataValidations == null) {
      if (other.dataValidations != null) return false;
    } else if (!dataValidations.equals(other.dataValidations)) return false;
    if (evalCondition != other.evalCondition) return false;
    if (fields == null) {
      if (other.fields != null) return false;
    } else if (!fields.equals(other.fields)) return false;
    if (fieldsStore == null) {
      if (other.fieldsStore != null) return false;
    } else if (!fieldsStore.equals(other.fieldsStore)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    return true;
  }
}

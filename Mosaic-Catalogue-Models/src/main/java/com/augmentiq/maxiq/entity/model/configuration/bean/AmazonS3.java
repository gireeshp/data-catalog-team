package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;

public class AmazonS3 implements Serializable {

  private String keyName;
  private S3BucketInfo bucket;
  private S3BucketInfo selectedBucket;
  private List<S3BucketInfo> hierarchyInfo;
  private String fileExtension;
  private String downloadObjectType;
  private String storagePath;
  private String accesskeyid;
  private String secretkey;
  private String finalPath;
  private Boolean optionallyEnclosedInDoubleQuotes = false;
  private String connectionName;

  public Boolean getOptionallyEnclosedInDoubleQuotes() {
    return optionallyEnclosedInDoubleQuotes;
  }

  public void setOptionallyEnclosedInDoubleQuotes(Boolean optionallyEnclosedInDoubleQuotes) {
    this.optionallyEnclosedInDoubleQuotes = optionallyEnclosedInDoubleQuotes;
  }

  public S3BucketInfo getSelectedBucket() {
    return selectedBucket;
  }

  public void setSelectedBucket(S3BucketInfo selectedBucket) {
    this.selectedBucket = selectedBucket;
  }

  public String getFinalPath() {
    return finalPath;
  }

  public void setFinalPath(String finalPath) {
    this.finalPath = finalPath;
  }

  public List<S3BucketInfo> getHierarchyInfo() {
    return hierarchyInfo;
  }

  public void setHierarchyInfo(List<S3BucketInfo> hierarchyInfo) {
    this.hierarchyInfo = hierarchyInfo;
  }

  public S3BucketInfo getBucket() {
    return bucket;
  }

  public void setBucket(S3BucketInfo bucket) {
    this.bucket = bucket;
  }

  public String getKeyName() {
    return keyName;
  }

  public void setKeyName(String keyName) {
    this.keyName = keyName;
  }

  public String getFileExtension() {
    return fileExtension;
  }

  public void setFileExtension(String fileExtension) {
    this.fileExtension = fileExtension;
  }

  public String getDownloadObjectType() {
    return downloadObjectType;
  }

  public void setDownloadObjectType(String downloadObjectType) {
    this.downloadObjectType = downloadObjectType;
  }

  public String getStoragePath() {
    return storagePath;
  }

  public void setStoragePath(String storagePath) {
    this.storagePath = storagePath;
  }

  public String getAccesskeyid() {
    return accesskeyid;
  }

  public void setAccesskeyid(String accesskeyid) {
    this.accesskeyid = accesskeyid;
  }

  public String getSecretkey() {
    return secretkey;
  }

  public void setSecretkey(String secretkey) {
    this.secretkey = secretkey;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  @Override
  public String toString() {
    return "AmazonS3 [keyName="
        + keyName
        + ", bucket="
        + bucket
        + ", selectedBucket="
        + selectedBucket
        + ", hierarchyInfo="
        + hierarchyInfo
        + ", fileExtension="
        + fileExtension
        + ", downloadObjectType="
        + downloadObjectType
        + ", storagePath="
        + storagePath
        + ", accesskeyid="
        + accesskeyid
        + ", secretkey="
        + secretkey
        + ", finalPath="
        + finalPath
        + ", optionallyEnclosedInDoubleQuotes="
        + optionallyEnclosedInDoubleQuotes
        + ", connectionName="
        + connectionName
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.constant.configuration.InputParamTypes;

public class AppCombiner {

  private String name;
  private String type;
  private InputParamTypes inputParamType;
  private Transpose transpase;
  private String defaultValue;

  public AppCombiner() {}

  public AppCombiner(
      String name,
      String type,
      InputParamTypes inputParamType,
      Transpose transpase,
      String defaultValue) {
    this.name = name;
    this.type = type;
    this.inputParamType = inputParamType;
    this.transpase = transpase;
    this.defaultValue = defaultValue;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public InputParamTypes getInputParamType() {
    return inputParamType;
  }

  public void setInputParamType(InputParamTypes inputParamType) {
    this.inputParamType = inputParamType;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Transpose getTranspase() {
    return transpase;
  }

  public void setTranspase(Transpose transpase) {
    this.transpase = transpase;
  }

  @Override
  public String toString() {
    return "AppCombiner [name="
        + name
        + ", type="
        + type
        + ", inputParamType="
        + inputParamType
        + ", transpase="
        + transpase
        + ", defaultValue="
        + defaultValue
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
    result = prime * result + ((inputParamType == null) ? 0 : inputParamType.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((transpase == null) ? 0 : transpase.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AppCombiner other = (AppCombiner) obj;
    if (defaultValue == null) {
      if (other.defaultValue != null) return false;
    } else if (!defaultValue.equals(other.defaultValue)) return false;
    if (inputParamType != other.inputParamType) return false;
    if (name == null) {
      if (other.name != null) return false;
    } else if (!name.equals(other.name)) return false;
    if (transpase == null) {
      if (other.transpase != null) return false;
    } else if (!transpase.equals(other.transpase)) return false;
    if (type == null) {
      if (other.type != null) return false;
    } else if (!type.equals(other.type)) return false;
    return true;
  }
}

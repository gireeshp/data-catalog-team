package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
import java.util.Set;

public class AppDeppendencies {
  private String fieldName;
  private Set<String> dependencies;
  private Set<String> dependent;

  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  public Set<String> getDependencies() {
    return dependencies;
  }

  public void setDependencies(Set<String> dependencies) {
    this.dependencies = dependencies;
  }

  public Set<String> getDependent() {
    return dependent;
  }

  public void setDependent(Set<String> dependent) {
    this.dependent = dependent;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AppDeppendencies [fieldName=");
    builder.append(fieldName);
    builder.append(", dependencies=");
    builder.append(dependencies);
    builder.append(", dependent=");
    builder.append(dependent);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dependencies == null) ? 0 : dependencies.hashCode());
    result = prime * result + ((dependent == null) ? 0 : dependent.hashCode());
    result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AppDeppendencies other = (AppDeppendencies) obj;
    if (dependencies == null) {
      if (other.dependencies != null) return false;
    } else if (!dependencies.equals(other.dependencies)) return false;
    if (dependent == null) {
      if (other.dependent != null) return false;
    } else if (!dependent.equals(other.dependent)) return false;
    if (fieldName == null) {
      if (other.fieldName != null) return false;
    } else if (!fieldName.equals(other.fieldName)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * @author Mayuri on 20-nov-2015 CREATE TABLE app_global_param (id int(20),type
 *     varchar(40),componantId int(20),appId int(20),globalParamValue varchar(40),globalParamName
 *     varchar(40));
 */
@TableName(tableName = Sequences.APP_GLOBAL_PARAM)
public class AppSettingInstance implements Serializable {
  /** */
  @Id private Long id;

  private String type;
  private String globalParamName;
  private Long componantId;
  private Long appId;
  private String globalParamValue;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getGlobalParamName() {
    return globalParamName;
  }

  public void setGlobalParamName(String globalParamType) {
    this.globalParamName = globalParamType;
  }

  public Long getComponantId() {
    return componantId;
  }

  public void setComponantId(Long componantId) {
    this.componantId = componantId;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public String getGlobalParamValue() {
    return globalParamValue;
  }

  public void setGlobalParamValue(String globalParamValue) {
    this.globalParamValue = globalParamValue;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((componantId == null) ? 0 : componantId.hashCode());
    result = prime * result + ((globalParamName == null) ? 0 : globalParamName.hashCode());
    result = prime * result + ((globalParamValue == null) ? 0 : globalParamValue.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AppSettingInstance other = (AppSettingInstance) obj;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (componantId == null) {
      if (other.componantId != null) return false;
    } else if (!componantId.equals(other.componantId)) return false;
    if (globalParamName == null) {
      if (other.globalParamName != null) return false;
    } else if (!globalParamName.equals(other.globalParamName)) return false;
    if (globalParamValue == null) {
      if (other.globalParamValue != null) return false;
    } else if (!globalParamValue.equals(other.globalParamValue)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (type == null) {
      if (other.type != null) return false;
    } else if (!type.equals(other.type)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "AppSettingInstance [id="
        + id
        + ", type="
        + type
        + ", globalParamName="
        + globalParamName
        + ", componantId="
        + componantId
        + ", appId="
        + appId
        + ", globalParamValue="
        + globalParamValue
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

import com.augmentiq.maxiq.constant.configuration.enums.AppValidateEnumStatus;

public class AppValidate {

  private AppValidateEnumStatus enumStatus;
  private String msg;
  private Long errorCount;
  private List<String> errorMsg;

  public AppValidateEnumStatus getEnumStatus() {
    return enumStatus;
  }

  public void setEnumStatus(AppValidateEnumStatus enumStatus) {
    this.enumStatus = enumStatus;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Long getErrorCount() {
    return errorCount;
  }

  public void setErrorCount(Long errorCount) {
    this.errorCount = errorCount;
  }

  public List<String> getErrorMsg() {
    return errorMsg;
  }

  public void setErrorMsg(List<String> errorMsg) {
    this.errorMsg = errorMsg;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AppValidate [enumStatus=");
    builder.append(enumStatus);
    builder.append(", msg=");
    builder.append(msg);
    builder.append(", errorCount=");
    builder.append(errorCount);
    builder.append(", errorMsg=");
    builder.append(errorMsg);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((enumStatus == null) ? 0 : enumStatus.hashCode());
    result = prime * result + ((errorCount == null) ? 0 : errorCount.hashCode());
    result = prime * result + ((errorMsg == null) ? 0 : errorMsg.hashCode());
    result = prime * result + ((msg == null) ? 0 : msg.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AppValidate other = (AppValidate) obj;
    if (enumStatus != other.enumStatus) return false;
    if (errorCount == null) {
      if (other.errorCount != null) return false;
    } else if (!errorCount.equals(other.errorCount)) return false;
    if (errorMsg == null) {
      if (other.errorMsg != null) return false;
    } else if (!errorMsg.equals(other.errorMsg)) return false;
    if (msg == null) {
      if (other.msg != null) return false;
    } else if (!msg.equals(other.msg)) return false;
    return true;
  }
}

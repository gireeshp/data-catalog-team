package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;

public class AugmentiqHiveException extends SystemException {

  public AugmentiqHiveException(String errorMessage, Throwable throwable) {
    super(errorMessage, throwable);
  }

  /** */
  private static final long serialVersionUID = 1L;
}

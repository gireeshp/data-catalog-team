package com.augmentiq.maxiq.entity.model.configuration.bean;
/** Changed for MAX-101 By Rushikesh Raut on 15-Sept-2016 */
public interface BeanConstants {
  public interface FileDataIngesterDetailsConst {
    public final String AVAILABLE_TO_USE = "availableToUse";
    public final String CONTAINS_HEADER = "containsHeader";
    public final String DATA_SOURCE_ID = "dataSourceId";
    public final String DATA_SOURCE_TYPE = "dataSourceType";
    public final String FILE_TYPE = "dataAtRestFileType";
    public final String COMPRESSION_TYPE = "dataAtRestCompressionType";
    public final String DELIMITER = "delimiter";
    public final String HEADER_STARTING = "headerStarting";
    public final String HEADER_TEMPLATE = "headerTemplate";
    public final String IS_DELIMITER_REGEX = "isDelimiterRegEx";
    public final String RECORD_LOAD_LIMIT = "recordLoadLimit";
    public final String RECORD_START_LINE = "recordStartLine";
    public final String RECORD_STARTS_WITH = "recordStartsWith";
    public final String RECORDS_IGNORE = "recordsIgnore";
    public final String SERVER_FILE_PATH = "serverFilePath";
    public final String STARTING_REGULAR_EXPRE = "startingRegularExpre";
    public final String WEBSERVER_TEMPPATH = "webServerTempPath";
    public final String EXCEL_RANGES = "excelRanges";
    public final String INPUT_FILE_TYPE = "inputFileType";
    public final String INPUT_COMPRESSION_TYPE = "inputCompressionType";
    public final String OUTPUT_FILE_TYPE = "outputFileType";
    public final String OUTPUT_COMPRESSION_TYPE = "outputCompressionType";
    public final String IS_OPTIONALLY_ENCLOSED_IN_DOUBLEQUOTES = "optionallyEnclosedInDoubleQuotes";
  }
}

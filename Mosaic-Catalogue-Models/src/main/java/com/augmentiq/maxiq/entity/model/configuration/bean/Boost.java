package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 *
drop table boost;
create table boost (id int(10) not null auto_increment,	orderLevel varchar(40), numberOfExecutors varchar(10), executorsMemory varchar(10), driverMemory varchar(10), numberOfCores varchar(10), isDefault varchar(10), insertedBy varchar(40), insertedDate timestamp, lastModifiedBy varchar(40), lastModifiedDate timestamp, status varchar(10), primary key(id));

select max(actionsMasterId)+1 from actions_master;
insert into actions_master (actionsMasterId, appId, action, status, subsetOf, actionCode)  values (120, 1, 'EDIT_BOOST', 'ACTIVE', 'VIEW_BOOST', 'EDIT_BOOST');
insert into actions_master (actionsMasterId, appId, action, status, subsetOf, actionCode)  values (121, 1, 'ADD_BOOST', 'ACTIVE', 'VIEW_BOOST', 'ADD_BOOST');
insert into actions_master (actionsMasterId, appId, action, status, subsetOf, actionCode)  values (122, 1, 'VIEW_BOOST', 'ACTIVE', 'MAXIQ', 'VIEW_BOOST');
insert into actions_master (actionsMasterId, appId, action, status, subsetOf, actionCode)  values (123, 1, 'DEL_BOOST', 'ACTIVE', 'VIEW_BOOST', 'DEL_BOOST');
insert into persona_action_mapping values(3,122);
*/
@TableName(tableName = Sequences.BOOST)
public class Boost implements Serializable {

  @Id public Long id;
  public String orderLevel;
  public String numberOfExecutors;
  public String executorsMemory;
  public String driverMemory;
  public String numberOfCores;
  public String isDefault;
  public String insertedBy;
  public String insertedDate;
  public String lastModifiedBy;
  public String lastModifiedDate;
  public String status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOrderLevel() {
    return orderLevel;
  }

  public void setOrderLevel(String orderLevel) {
    this.orderLevel = orderLevel;
  }

  public String getNumberOfExecutors() {
    return numberOfExecutors;
  }

  public void setNumberOfExecutors(String numberOfExecutors) {
    this.numberOfExecutors = numberOfExecutors;
  }

  public String getExecutorsMemory() {
    return executorsMemory;
  }

  public void setExecutorsMemory(String executorsMemory) {
    this.executorsMemory = executorsMemory;
  }

  public String getDriverMemory() {
    return driverMemory;
  }

  public void setDriverMemory(String driverMemory) {
    this.driverMemory = driverMemory;
  }

  public String getNumberOfCores() {
    return numberOfCores;
  }

  public void setNumberOfCores(String numberOfCores) {
    this.numberOfCores = numberOfCores;
  }

  public String getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(String isDefault) {
    this.isDefault = isDefault;
  }

  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getInsertedDate() {
    return insertedDate;
  }

  public void setInsertedDate(String insertedDate) {
    this.insertedDate = insertedDate;
  }

  public String getLastModifiedBy() {
    return lastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    this.lastModifiedBy = lastModifiedBy;
  }

  public String getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(String lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Boost [id="
        + id
        + ", orderLevel="
        + orderLevel
        + ", numberOfExecutors="
        + numberOfExecutors
        + ", executorsMemory="
        + executorsMemory
        + ", driverMemory="
        + driverMemory
        + ", numberOfCores="
        + numberOfCores
        + ", isDefault="
        + isDefault
        + ", insertedBy="
        + insertedBy
        + ", insertedDate="
        + insertedDate
        + ", lastModifiedBy="
        + lastModifiedBy
        + ", lastModifiedDate="
        + lastModifiedDate
        + ", status="
        + status
        + "]";
  }
}

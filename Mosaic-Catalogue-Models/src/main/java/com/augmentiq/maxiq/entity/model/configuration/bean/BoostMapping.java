package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 *
drop table boost_mapping;
create table boost_mapping (id int(10) not null auto_increment,	objectType varchar(50), objectId varchar(50), boostId varchar(40), primary key(id));
*/
@TableName(tableName = Sequences.BOOST_MAPPING)
public class BoostMapping implements Serializable {

  @Id public Long id;
  public String objectType;
  public String objectId;
  public String boostId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getObjectType() {
    return objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  public String getObjectId() {
    return objectId;
  }

  public void setObjectId(String objectId) {
    this.objectId = objectId;
  }

  public String getBoostId() {
    return boostId;
  }

  public void setBoostId(String boostId) {
    this.boostId = boostId;
  }

  @Override
  public String toString() {
    return "Boost [id="
        + id
        + ", objectType="
        + objectType
        + ", objectId="
        + objectId
        + ", boostId="
        + boostId
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 *
drop table boost_mapping_reduce;
create table boost_mapping_reduce (jobInstanceId varchar(40),	boostId varchar(40));
*/
@TableName(tableName = Sequences.BOOST_MAPPING_REDUCE)
public class BoostMappingReduce implements Serializable {

  @Id public String jobInstanceId;
  public String boostId;

  public String getJobInstanceId() {
    return jobInstanceId;
  }

  public void setJobInstanceId(String jobInstanceId) {
    this.jobInstanceId = jobInstanceId;
  }

  public String getBoostId() {
    return boostId;
  }

  public void setBoostId(String boostId) {
    this.boostId = boostId;
  }

  @Override
  public String toString() {
    return "BoostMappingReduce [jobInstanceId=" + jobInstanceId + ", boostId=" + boostId + "]";
  }
}

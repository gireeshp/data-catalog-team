package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.CATEGORY_SUBCATEGORY)
public class CategorySubCatMaster implements Serializable {

  @Id private Long id;
  private String catOrSubCatName;
  private Long parentId;
  private Long createdDate;
  private Long createdBy;
  private Long modifiedDate = 0l;
  private Long modifiedBy = 0l;
  private String shortDescription;
  private String longDescription;
  private Integer status;

  public CategorySubCatMaster(
      Long id,
      String catOrSubCatName,
      Long parentId,
      Long createdDate,
      Long createdBy,
      Long modifiedDate,
      Long modifiedBy,
      String shortDescription,
      String longDescription,
      Integer status) {
    super();
    this.id = id;
    this.catOrSubCatName = catOrSubCatName;
    this.parentId = parentId;
    this.createdDate = createdDate;
    this.createdBy = createdBy;
    this.modifiedDate = modifiedDate;
    this.modifiedBy = modifiedBy;
    this.shortDescription = shortDescription;
    this.longDescription = longDescription;
    this.status = status;
  }

  public CategorySubCatMaster() {
    super();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCatOrSubCatName() {
    return catOrSubCatName;
  }

  public void setCatOrSubCatName(String catOrSubCatName) {
    this.catOrSubCatName = catOrSubCatName;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public Long getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Long modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getLongDescription() {
    return longDescription;
  }

  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "CategorySubCatMaster [id="
        + id
        + ", catOrSubCatName="
        + catOrSubCatName
        + ", parentId="
        + parentId
        + ", createdDate="
        + createdDate
        + ", createdBy="
        + createdBy
        + ", modifiedDate="
        + modifiedDate
        + ", modifiedBy="
        + modifiedBy
        + ", shortDescription="
        + shortDescription
        + ", longDescription="
        + longDescription
        + ", status="
        + status
        + "]";
  }
}

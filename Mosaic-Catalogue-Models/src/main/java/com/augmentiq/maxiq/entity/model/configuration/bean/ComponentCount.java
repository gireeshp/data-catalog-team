package com.augmentiq.maxiq.entity.model.configuration.bean;

public class ComponentCount {
  private String cnt;
  private String componentName;

  public String getCnt() {
    return cnt;
  }

  public void setCnt(String cnt) {
    this.cnt = cnt;
  }

  public String getComponentName() {
    return componentName;
  }

  public void setComponentName(String componentName) {
    this.componentName = componentName;
  }

  @Override
  public String toString() {
    return "ComponentCount [cnt=" + cnt + ", componentName=" + componentName + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

public class ContainsListFilter extends DataFilter {
  private List<String> matchingList;
  private boolean exactMatch;
  private boolean caseSensitive;

  public List<String> getMatchingList() {
    return matchingList;
  }

  public void setMatchingList(List<String> matchingList) {
    this.matchingList = matchingList;
  }

  public boolean isExactMatch() {
    return exactMatch;
  }

  public void setExactMatch(boolean exactMatch) {
    this.exactMatch = exactMatch;
  }

  public boolean isCaseSensitive() {
    return caseSensitive;
  }

  public void setCaseSensitive(boolean caseSensitive) {
    this.caseSensitive = caseSensitive;
  }

  public ContainsListFilter() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ContainsListFilter [matchingList="
        + matchingList
        + ", exactMatch="
        + exactMatch
        + ", caseSensitive="
        + caseSensitive
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (caseSensitive ? 1231 : 1237);
    result = prime * result + (exactMatch ? 1231 : 1237);
    result = prime * result + ((matchingList == null) ? 0 : matchingList.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!super.equals(obj)) return false;
    if (getClass() != obj.getClass()) return false;
    ContainsListFilter other = (ContainsListFilter) obj;
    if (caseSensitive != other.caseSensitive) return false;
    if (exactMatch != other.exactMatch) return false;
    if (matchingList == null) {
      if (other.matchingList != null) return false;
    } else if (!matchingList.equals(other.matchingList)) return false;
    return true;
  }
}

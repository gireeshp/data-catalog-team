package com.augmentiq.maxiq.entity.model.configuration.bean;

public class ContainsStringFilter extends DataFilter {
  private String matchingString_;
  private boolean exactMatch;
  private boolean caseSensitive;

  public String getRegexString() {
    return matchingString_;
  }

  public void setRegexString(String regexString) {
    this.matchingString_ = regexString;
  }

  public boolean isExactMatch() {
    return exactMatch;
  }

  public void setExactMatch(boolean exactMatch) {
    this.exactMatch = exactMatch;
  }

  public boolean isCaseSensitive() {
    return caseSensitive;
  }

  public void setCaseSensitive(boolean caseSensitive) {
    this.caseSensitive = caseSensitive;
  }

  public ContainsStringFilter() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ContainsStringFilter [matchingString_="
        + matchingString_
        + ", exactMatch="
        + exactMatch
        + ", caseSensitive="
        + caseSensitive
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (caseSensitive ? 1231 : 1237);
    result = prime * result + (exactMatch ? 1231 : 1237);
    result = prime * result + ((matchingString_ == null) ? 0 : matchingString_.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!super.equals(obj)) return false;
    if (getClass() != obj.getClass()) return false;
    ContainsStringFilter other = (ContainsStringFilter) obj;
    if (caseSensitive != other.caseSensitive) return false;
    if (exactMatch != other.exactMatch) return false;
    if (matchingString_ == null) {
      if (other.matchingString_ != null) return false;
    } else if (!matchingString_.equals(other.matchingString_)) return false;
    return true;
  }
}

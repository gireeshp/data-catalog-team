package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.DataStatus;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * Each custom component instance will have an information about the custom component run will be
 * stored.
 */
@TableName(tableName = Sequences.CUSTOM_COMPONENT_INSTANCE)
/*Alter table customcomponentinstance add column distFilePath varchar(50)*/
public class CustomComponentInstance {
  @Id private Long componentId;
  private String componentName;
  private String componentType;
  private String componentDescription;
  private String owner;
  private Long componentCreatedDate;
  private Long componentUpdatedDate;
  private DataStatus componentStatus;

  private CustomYamlConfigData customYamlConfigData;
  private String supportingComponentType;
  private String installLocation;
  private String supportingComponentJarsPath;
  private Long groupId;
  private String distFilePath;

  private Long ownerProjectId;
  private String configurationFilePath;
  private String resourceFilePath;

  public String getConfigurationFilePath() {
    return configurationFilePath;
  }

  public void setConfigurationFilePath(String configurationFilePath) {
    this.configurationFilePath = configurationFilePath;
  }

  public String getResourceFilePath() {
    return resourceFilePath;
  }

  public void setResourceFilePath(String resourceFilePath) {
    this.resourceFilePath = resourceFilePath;
  }

  public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  public String getDistFilePath() {
    return distFilePath;
  }

  public void setDistFilePath(String distFilePath) {
    this.distFilePath = distFilePath;
  }

  public Long getComponentId() {
    return componentId;
  }

  public void setComponentId(Long componentId) {
    this.componentId = componentId;
  }

  public String getComponentName() {
    return componentName;
  }

  public void setComponentName(String componentName) {
    this.componentName = componentName;
  }

  public String getComponentType() {
    return componentType;
  }

  public void setComponentType(String componentType) {
    this.componentType = componentType;
  }

  public String getComponentDescription() {
    return componentDescription;
  }

  public void setComponentDescription(String componentDescription) {
    this.componentDescription = componentDescription;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public Long getComponentCreatedDate() {
    return componentCreatedDate;
  }

  public void setComponentCreatedDate(Long componentCreatedDate) {
    this.componentCreatedDate = componentCreatedDate;
  }

  public Long getComponentUpdatedDate() {
    return componentUpdatedDate;
  }

  public void setComponentUpdatedDate(Long componentUpdatedDate) {
    this.componentUpdatedDate = componentUpdatedDate;
  }

  public DataStatus getComponentStatus() {
    return componentStatus;
  }

  public void setComponentStatus(DataStatus componentStatus) {
    this.componentStatus = componentStatus;
  }

  public CustomYamlConfigData getCustomYamlConfigData() {
    return customYamlConfigData;
  }

  public void setCustomYamlConfigData(CustomYamlConfigData customYamlConfigData) {
    this.customYamlConfigData = customYamlConfigData;
  }

  public String getSupportingComponentType() {
    return supportingComponentType;
  }

  public void setSupportingComponentType(String supportingComponentType) {
    this.supportingComponentType = supportingComponentType;
  }

  public String getInstallLocation() {
    return installLocation;
  }

  public void setInstallLocation(String installLocation) {
    this.installLocation = installLocation;
  }

  public String getSupportingComponentJarsPath() {
    return supportingComponentJarsPath;
  }

  public void setSupportingComponentJarsPath(String supportingComponentJarsPath) {
    this.supportingComponentJarsPath = supportingComponentJarsPath;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  @Override
  public String toString() {
    return "CustomComponentInstance [componentId="
        + componentId
        + ", componentName="
        + componentName
        + ", componentType="
        + componentType
        + ", componentDescription="
        + componentDescription
        + ", owner="
        + owner
        + ", componentCreatedDate="
        + componentCreatedDate
        + ", componentUpdatedDate="
        + componentUpdatedDate
        + ", componentStatus="
        + componentStatus
        + ", customYamlConfigData="
        + customYamlConfigData
        + ", supportingComponentType="
        + supportingComponentType
        + ", installLocation="
        + installLocation
        + ", supportingComponentJarsPath="
        + supportingComponentJarsPath
        + ", groupId="
        + groupId
        + ", distFilePath="
        + distFilePath
        + ", ownerProjectId="
        + ownerProjectId
        + " ,resourceFilePath ="
        + resourceFilePath
        + ", configurationFilePath"
        + configurationFilePath
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result =
        prime * result + ((componentCreatedDate == null) ? 0 : componentCreatedDate.hashCode());
    result =
        prime * result + ((componentDescription == null) ? 0 : componentDescription.hashCode());
    result = prime * result + ((componentId == null) ? 0 : componentId.hashCode());
    result = prime * result + ((componentName == null) ? 0 : componentName.hashCode());
    result = prime * result + ((componentStatus == null) ? 0 : componentStatus.hashCode());
    result = prime * result + ((componentType == null) ? 0 : componentType.hashCode());
    result =
        prime * result + ((componentUpdatedDate == null) ? 0 : componentUpdatedDate.hashCode());
    result =
        prime * result + ((customYamlConfigData == null) ? 0 : customYamlConfigData.hashCode());
    result = prime * result + ((distFilePath == null) ? 0 : distFilePath.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((installLocation == null) ? 0 : installLocation.hashCode());
    result = prime * result + ((owner == null) ? 0 : owner.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    result =
        prime * result
            + ((supportingComponentJarsPath == null) ? 0 : supportingComponentJarsPath.hashCode());
    result =
        prime * result
            + ((supportingComponentType == null) ? 0 : supportingComponentType.hashCode());
    result = prime * result + ((resourceFilePath == null) ? 0 : resourceFilePath.hashCode());
    result =
        prime * result + ((configurationFilePath == null) ? 0 : configurationFilePath.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    CustomComponentInstance other = (CustomComponentInstance) obj;
    if (componentCreatedDate == null) {
      if (other.componentCreatedDate != null) return false;
    } else if (!componentCreatedDate.equals(other.componentCreatedDate)) return false;
    if (componentDescription == null) {
      if (other.componentDescription != null) return false;
    } else if (!componentDescription.equals(other.componentDescription)) return false;
    if (componentId == null) {
      if (other.componentId != null) return false;
    } else if (!componentId.equals(other.componentId)) return false;
    if (componentName == null) {
      if (other.componentName != null) return false;
    } else if (!componentName.equals(other.componentName)) return false;
    if (componentStatus != other.componentStatus) return false;
    if (componentType == null) {
      if (other.componentType != null) return false;
    } else if (!componentType.equals(other.componentType)) return false;
    if (componentUpdatedDate == null) {
      if (other.componentUpdatedDate != null) return false;
    } else if (!componentUpdatedDate.equals(other.componentUpdatedDate)) return false;
    if (customYamlConfigData == null) {
      if (other.customYamlConfigData != null) return false;
    } else if (!customYamlConfigData.equals(other.customYamlConfigData)) return false;
    if (distFilePath == null) {
      if (other.distFilePath != null) return false;
    } else if (!distFilePath.equals(other.distFilePath)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (installLocation == null) {
      if (other.installLocation != null) return false;
    } else if (!installLocation.equals(other.installLocation)) return false;
    if (owner == null) {
      if (other.owner != null) return false;
    } else if (!owner.equals(other.owner)) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    if (supportingComponentJarsPath == null) {
      if (other.supportingComponentJarsPath != null) return false;
    } else if (!supportingComponentJarsPath.equals(other.supportingComponentJarsPath)) return false;
    if (supportingComponentType == null) {
      if (other.supportingComponentType != null) return false;
    } else if (!supportingComponentType.equals(other.supportingComponentType)) return false;
    if (configurationFilePath == null) {
      if (other.configurationFilePath != null) return false;
    } else if (!configurationFilePath.equals(other.configurationFilePath)) return false;
    if (resourceFilePath == null) {
      if (other.resourceFilePath != null) return false;
    } else if (!resourceFilePath.equals(other.resourceFilePath)) return false;
    return true;
  }
}

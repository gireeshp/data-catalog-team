package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Map;

public class CustomValidation implements Serializable {
  private Long id;
  private Map<String, Long> customInputList;
  private String componentName;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Map<String, Long> getCustomInputList() {
    return customInputList;
  }

  public void setCustomInputList(Map<String, Long> customInputList) {
    this.customInputList = customInputList;
  }

  public String getComponentName() {
    return componentName;
  }

  public void setComponentName(String componentName) {
    this.componentName = componentName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((componentName == null) ? 0 : componentName.hashCode());
    result = prime * result + ((customInputList == null) ? 0 : customInputList.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    CustomValidation other = (CustomValidation) obj;
    if (componentName == null) {
      if (other.componentName != null) return false;
    } else if (!componentName.equals(other.componentName)) return false;
    if (customInputList == null) {
      if (other.customInputList != null) return false;
    } else if (!customInputList.equals(other.customInputList)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "CustomValidation [id="
        + id
        + ", customInputList="
        + customInputList
        + ", componentName="
        + componentName
        + "]";
  }
}

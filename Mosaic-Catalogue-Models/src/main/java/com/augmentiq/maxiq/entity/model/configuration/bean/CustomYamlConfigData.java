package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
/**
 * Each CustomYamlConfigData instance will have an information about the configuration YAML File in
 * Custom Component.
 */
public class CustomYamlConfigData implements Serializable {
  /** */
  private String name;

  private String purpose;
  private String type;
  private Map<String, String> encloseFiles;
  private List<String> outputFields;
  private List<String> kb;
  private Map<String, String> inputFields;
  private String inputclass;
  private String runinit;
  private Map<String, String> init;
  private Map<String, String> conf;
  private Object multiOutput;
  private List<String> distcacheFile;
  private Map<String, Map<String, String>> fileConfig;

  public List<String> getDistcacheFile() {
    return distcacheFile;
  }

  public void setDistcacheFile(List<String> distcacheFile) {
    this.distcacheFile = distcacheFile;
  }

  public Object getMultiOutput() {
    return multiOutput;
  }

  public void setMultiOutput(Object multiOutput) {
    this.multiOutput = multiOutput;
  }

  public List<String> getKb() {
    return kb;
  }

  public void setKb(List<String> kb) {
    this.kb = kb;
  }

  public String getRuninit() {
    return runinit;
  }

  public void setRuninit(String runinit) {
    this.runinit = runinit;
  }

  public Map<String, String> getConf() {
    return conf;
  }

  public void setConf(Map<String, String> conf) {
    this.conf = conf;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPurpose() {
    return purpose;
  }

  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Map<String, String> getEncloseFiles() {
    return encloseFiles;
  }

  public void setEncloseFiles(Map<String, String> encloseFiles) {
    this.encloseFiles = encloseFiles;
  }

  public Map<String, String> getInputFields() {
    return inputFields;
  }

  public void setInputFields(Map<String, String> inputFields) {
    this.inputFields = inputFields;
  }

  public List<String> getOutputFields() {
    return outputFields;
  }

  public void setOutputFields(List<String> outputFields) {
    this.outputFields = outputFields;
  }

  public String getInputclass() {
    return inputclass;
  }

  public void setInputclass(String inputclass) {
    this.inputclass = inputclass;
  }

  public Map<String, String> getInit() {
    return init;
  }

  public void setInit(Map<String, String> init) {
    this.init = init;
  }

  public Map<String, Map<String, String>> getFileConfig() {
    return fileConfig;
  }

  public void setFileConfig(Map<String, Map<String, String>> fileConfig) {
    this.fileConfig = fileConfig;
  }

  @Override
  public String toString() {
    return "CustomYamlConfigData [name="
        + name
        + ", purpose="
        + purpose
        + ", type="
        + type
        + ", encloseFiles="
        + encloseFiles
        + ", outputFields="
        + outputFields
        + ", kb="
        + kb
        + ", inputFields="
        + inputFields
        + ", inputclass="
        + inputclass
        + ", runinit="
        + runinit
        + ", init="
        + init
        + ", conf="
        + conf
        + ", multiOutput="
        + multiOutput
        + ", distcacheFile="
        + distcacheFile
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((conf == null) ? 0 : conf.hashCode());
    result = prime * result + ((distcacheFile == null) ? 0 : distcacheFile.hashCode());
    result = prime * result + ((encloseFiles == null) ? 0 : encloseFiles.hashCode());
    result = prime * result + ((fileConfig == null) ? 0 : fileConfig.hashCode());
    result = prime * result + ((init == null) ? 0 : init.hashCode());
    result = prime * result + ((inputFields == null) ? 0 : inputFields.hashCode());
    result = prime * result + ((inputclass == null) ? 0 : inputclass.hashCode());
    result = prime * result + ((kb == null) ? 0 : kb.hashCode());
    result = prime * result + ((multiOutput == null) ? 0 : multiOutput.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((outputFields == null) ? 0 : outputFields.hashCode());
    result = prime * result + ((purpose == null) ? 0 : purpose.hashCode());
    result = prime * result + ((runinit == null) ? 0 : runinit.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    CustomYamlConfigData other = (CustomYamlConfigData) obj;
    if (conf == null) {
      if (other.conf != null) return false;
    } else if (!conf.equals(other.conf)) return false;
    if (distcacheFile == null) {
      if (other.distcacheFile != null) return false;
    } else if (!distcacheFile.equals(other.distcacheFile)) return false;
    if (encloseFiles == null) {
      if (other.encloseFiles != null) return false;
    } else if (!encloseFiles.equals(other.encloseFiles)) return false;
    if (fileConfig == null) {
      if (other.fileConfig != null) return false;
    } else if (!fileConfig.equals(other.fileConfig)) return false;
    if (init == null) {
      if (other.init != null) return false;
    } else if (!init.equals(other.init)) return false;
    if (inputFields == null) {
      if (other.inputFields != null) return false;
    } else if (!inputFields.equals(other.inputFields)) return false;
    if (inputclass == null) {
      if (other.inputclass != null) return false;
    } else if (!inputclass.equals(other.inputclass)) return false;
    if (kb == null) {
      if (other.kb != null) return false;
    } else if (!kb.equals(other.kb)) return false;
    if (multiOutput == null) {
      if (other.multiOutput != null) return false;
    } else if (!multiOutput.equals(other.multiOutput)) return false;
    if (name == null) {
      if (other.name != null) return false;
    } else if (!name.equals(other.name)) return false;
    if (outputFields == null) {
      if (other.outputFields != null) return false;
    } else if (!outputFields.equals(other.outputFields)) return false;
    if (purpose == null) {
      if (other.purpose != null) return false;
    } else if (!purpose.equals(other.purpose)) return false;
    if (runinit == null) {
      if (other.runinit != null) return false;
    } else if (!runinit.equals(other.runinit)) return false;
    if (type == null) {
      if (other.type != null) return false;
    } else if (!type.equals(other.type)) return false;
    return true;
  }
}

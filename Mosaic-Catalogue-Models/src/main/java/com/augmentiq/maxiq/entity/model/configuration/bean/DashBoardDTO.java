package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.constant.configuration.enums.Id;

public class DashBoardDTO implements Serializable {

  @Id private String count;
  private String name;

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "DashBoardDTO [count=" + count + ", name=" + name + "]";
  }
}

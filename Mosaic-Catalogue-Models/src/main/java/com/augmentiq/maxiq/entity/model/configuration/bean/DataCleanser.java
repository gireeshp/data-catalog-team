package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

/**
 * Row level cleansing is included in the file.
 *
 * @author shiva
 */
public class DataCleanser implements Serializable {

  private Long id_;
  private Long dataSourceId_;
  private String fromValue_;
  private String toValue_;
  private boolean regxInd_;

  public Long getId() {
    return id_;
  }

  public void setId(Long id) {
    id_ = id;
  }

  public Long getDataSourceId() {
    return dataSourceId_;
  }

  public void setDataSourceId(Long dataSourceId) {
    dataSourceId_ = dataSourceId;
  }

  public String getFromValue() {
    return fromValue_;
  }

  public void setFromValue(String fromValue) {
    fromValue_ = fromValue;
  }

  public String getToValue() {
    return toValue_;
  }

  public void setToValue(String toValue) {
    toValue_ = toValue;
  }

  public boolean isRegxInd() {
    return regxInd_;
  }

  public void setRegxInd(boolean regxInd) {
    regxInd_ = regxInd;
  }

  public DataCleanser() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DataCleanser [id_="
        + id_
        + ", dataSourceId_="
        + dataSourceId_
        + ", fromValue_="
        + fromValue_
        + ", toValue_="
        + toValue_
        + ", regxInd_="
        + regxInd_
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dataSourceId_ == null) ? 0 : dataSourceId_.hashCode());
    result = prime * result + ((fromValue_ == null) ? 0 : fromValue_.hashCode());
    result = prime * result + ((id_ == null) ? 0 : id_.hashCode());
    result = prime * result + (regxInd_ ? 1231 : 1237);
    result = prime * result + ((toValue_ == null) ? 0 : toValue_.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataCleanser other = (DataCleanser) obj;
    if (dataSourceId_ == null) {
      if (other.dataSourceId_ != null) return false;
    } else if (!dataSourceId_.equals(other.dataSourceId_)) return false;
    if (fromValue_ == null) {
      if (other.fromValue_ != null) return false;
    } else if (!fromValue_.equals(other.fromValue_)) return false;
    if (id_ == null) {
      if (other.id_ != null) return false;
    } else if (!id_.equals(other.id_)) return false;
    if (regxInd_ != other.regxInd_) return false;
    if (toValue_ == null) {
      if (other.toValue_ != null) return false;
    } else if (!toValue_.equals(other.toValue_)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.constant.configuration.enums.DataFilterType;

public class DataFilter implements Serializable {
  private DataFilterType dataFilterType_;

  public DataFilterType getDataFilterType() {
    return dataFilterType_;
  }

  public void setDataFilterType(DataFilterType dataFilterType) {
    this.dataFilterType_ = dataFilterType;
  }

  public DataFilter() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DataFilter [dataFilterType_=" + dataFilterType_ + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dataFilterType_ == null) ? 0 : dataFilterType_.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataFilter other = (DataFilter) obj;
    if (dataFilterType_ != other.dataFilterType_) return false;
    return true;
  }
}

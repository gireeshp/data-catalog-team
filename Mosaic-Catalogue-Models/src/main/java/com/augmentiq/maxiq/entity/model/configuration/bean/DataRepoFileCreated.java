package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Date;

public class DataRepoFileCreated {
  private Date date_;
  private String folderName_;

  public Date getDate() {
    return date_;
  }

  public void setDate(Date date) {
    this.date_ = date;
  }

  public String getFolderName() {
    return folderName_;
  }

  public void setFolderName(String folderName) {
    this.folderName_ = folderName;
  }
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DataRepoFileCreated [date_=" + date_ + ", folderName_=" + folderName_ + "]";
  }
  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((date_ == null) ? 0 : date_.hashCode());
    result = prime * result + ((folderName_ == null) ? 0 : folderName_.hashCode());
    return result;
  }
  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataRepoFileCreated other = (DataRepoFileCreated) obj;
    if (date_ == null) {
      if (other.date_ != null) return false;
    } else if (!date_.equals(other.date_)) return false;
    if (folderName_ == null) {
      if (other.folderName_ != null) return false;
    } else if (!folderName_.equals(other.folderName_)) return false;
    return true;
  }
}

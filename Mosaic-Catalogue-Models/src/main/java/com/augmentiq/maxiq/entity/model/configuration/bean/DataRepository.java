package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

/**
 * * Data repository is the main configuration bean. It is based on the Composite Design pattern.
 *
 * @author shiva
 */
public class DataRepository {
  private Long id;
  private String repoName;
  private List<DataSource> dataSources;
  private List<Long> dataSourcesStore;
  private List<RecordCombiner> combinerMap;
  private JobSchedular schedular;
  private String userId;
  private Long appId;

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getRepoName() {
    return repoName;
  }

  public void setRepoName(String repoName) {
    this.repoName = repoName;
  }

  public List<DataSource> getDataSources() {
    return dataSources;
  }

  public void setDataSources(List<DataSource> dataSources) {
    this.dataSources = dataSources;
  }

  public List<RecordCombiner> getCombinerMap() {
    return combinerMap;
  }

  public void setCombinerMap(List<RecordCombiner> combinerMap) {
    this.combinerMap = combinerMap;
  }

  public JobSchedular getSchedular() {
    return schedular;
  }

  public void setSchedular(JobSchedular schedular) {
    this.schedular = schedular;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<Long> getDataSourcesStore() {
    return dataSourcesStore;
  }

  public void setDataSourcesStore(List<Long> dataSourcesStore) {
    this.dataSourcesStore = dataSourcesStore;
  }

  public DataSource findDataSourceById(Long dsId) {
    if (dataSources != null && dataSources.size() > 0 && dsId != null)
      for (DataSource ds : dataSources)
        if (ds.getId() != null && ds.getId().equals(dsId)) return ds;

    return null;
  }

  @Override
  public String toString() {
    return "DataRepository [id="
        + id
        + ", repoName="
        + repoName
        + ", dataSources="
        + dataSources
        + ", dataSourcesStore="
        + dataSourcesStore
        + ", combinerMap="
        + combinerMap
        + ", schedular="
        + schedular
        + ", userId="
        + userId
        + ", appId="
        + appId
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((combinerMap == null) ? 0 : combinerMap.hashCode());
    result = prime * result + ((dataSources == null) ? 0 : dataSources.hashCode());
    result = prime * result + ((dataSourcesStore == null) ? 0 : dataSourcesStore.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((repoName == null) ? 0 : repoName.hashCode());
    result = prime * result + ((schedular == null) ? 0 : schedular.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataRepository other = (DataRepository) obj;
    if (combinerMap == null) {
      if (other.combinerMap != null) return false;
    } else if (!combinerMap.equals(other.combinerMap)) return false;
    if (dataSources == null) {
      if (other.dataSources != null) return false;
    } else if (!dataSources.equals(other.dataSources)) return false;
    if (dataSourcesStore == null) {
      if (other.dataSourcesStore != null) return false;
    } else if (!dataSourcesStore.equals(other.dataSourcesStore)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (repoName == null) {
      if (other.repoName != null) return false;
    } else if (!repoName.equals(other.repoName)) return false;
    if (schedular == null) {
      if (other.schedular != null) return false;
    } else if (!schedular.equals(other.schedular)) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    return true;
  }
}

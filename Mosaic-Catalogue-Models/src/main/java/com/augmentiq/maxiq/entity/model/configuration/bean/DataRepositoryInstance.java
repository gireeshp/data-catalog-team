package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Date;
import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.DataStatus;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * Each data repository will have an instance where all the information about the data repository
 * run will be stored. It will contain the information about the list of file paths.
 *
 * @author shiva
 */
@TableName(tableName = Sequences.DATA_REPO_INSTANCE)
public class DataRepositoryInstance {

  @Id private Long repoId;
  private String repoName;
  private Date repoCreated;
  private Date repoUpdated;
  private Long totalRecords;
  private String refreshType;
  private double size;
  private String createdBy;
  private String userId;
  private DataStatus dataRepoStatus;
  private List<DataRepoFileCreated> filePaths;
  private List<Long> dataSourceIds;
  private String lastUpdateFile;
  private Long groupId;
  private Long ownerProjectId;

  public String getRepoName() {
    return repoName;
  }

  public void setRepoName(String repoName) {
    this.repoName = repoName;
  }

  public Date getRepoCreated() {
    return repoCreated;
  }

  public void setRepoCreated(Date repoCreated) {
    this.repoCreated = repoCreated;
  }

  public Date getRepoUpdated() {
    return repoUpdated;
  }

  public void setRepoUpdated(Date repoUpdated) {
    this.repoUpdated = repoUpdated;
  }

  public Long getRepoId() {
    return repoId;
  }

  public void setRepoId(Long repoId) {
    this.repoId = repoId;
  }

  public Long getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(Long totalRecords) {
    this.totalRecords = totalRecords;
  }

  public String getRefreshType() {
    return refreshType;
  }

  public void setRefreshType(String refreshType) {
    this.refreshType = refreshType;
  }

  public double getSize() {
    return size;
  }

  public void setSize(double size) {
    this.size = size;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public DataStatus getDataRepoStatus() {
    return dataRepoStatus;
  }

  public void setDataRepoStatus(DataStatus dataRepoStatus) {
    this.dataRepoStatus = dataRepoStatus;
  }

  public List<DataRepoFileCreated> getFilePaths() {
    return filePaths;
  }

  public void setFilePaths(List<DataRepoFileCreated> filePaths) {
    this.filePaths = filePaths;
  }

  public List<Long> getDataSourceIds() {
    return dataSourceIds;
  }

  public void setDataSourceIds(List<Long> dataSourceIds) {
    this.dataSourceIds = dataSourceIds;
  }

  public String getLastUpdateFile() {
    return lastUpdateFile;
  }

  public void setLastUpdateFile(String lastUpdateFile) {
    this.lastUpdateFile = lastUpdateFile;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("DataRepositoryInstance [repoName=");
    builder.append(repoName);
    builder.append(", repoCreated=");
    builder.append(repoCreated);
    builder.append(", repoUpdated=");
    builder.append(repoUpdated);
    builder.append(", repoId=");
    builder.append(repoId);
    builder.append(", totalRecords=");
    builder.append(totalRecords);
    builder.append(", refreshType=");
    builder.append(refreshType);
    builder.append(", size=");
    builder.append(size);
    builder.append(", createdBy=");
    builder.append(createdBy);
    builder.append(", userId=");
    builder.append(userId);
    builder.append(", dataRepoStatus=");
    builder.append(dataRepoStatus);
    builder.append(", filePaths=");
    builder.append(filePaths);
    builder.append(", dataSourceIds=");
    builder.append(dataSourceIds);
    builder.append(", lastUpdateFile=");
    builder.append(lastUpdateFile);
    builder.append(", groupId=");
    builder.append(groupId);
    builder.append(", ownerProjectId=");
    builder.append(ownerProjectId);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((dataRepoStatus == null) ? 0 : dataRepoStatus.hashCode());
    result = prime * result + ((dataSourceIds == null) ? 0 : dataSourceIds.hashCode());
    result = prime * result + ((filePaths == null) ? 0 : filePaths.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((lastUpdateFile == null) ? 0 : lastUpdateFile.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    result = prime * result + ((refreshType == null) ? 0 : refreshType.hashCode());
    result = prime * result + ((repoCreated == null) ? 0 : repoCreated.hashCode());
    result = prime * result + ((repoId == null) ? 0 : repoId.hashCode());
    result = prime * result + ((repoName == null) ? 0 : repoName.hashCode());
    result = prime * result + ((repoUpdated == null) ? 0 : repoUpdated.hashCode());
    long temp;
    temp = Double.doubleToLongBits(size);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((totalRecords == null) ? 0 : totalRecords.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataRepositoryInstance other = (DataRepositoryInstance) obj;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (dataRepoStatus != other.dataRepoStatus) return false;
    if (dataSourceIds == null) {
      if (other.dataSourceIds != null) return false;
    } else if (!dataSourceIds.equals(other.dataSourceIds)) return false;
    if (filePaths == null) {
      if (other.filePaths != null) return false;
    } else if (!filePaths.equals(other.filePaths)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (lastUpdateFile == null) {
      if (other.lastUpdateFile != null) return false;
    } else if (!lastUpdateFile.equals(other.lastUpdateFile)) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    if (refreshType == null) {
      if (other.refreshType != null) return false;
    } else if (!refreshType.equals(other.refreshType)) return false;
    if (repoCreated == null) {
      if (other.repoCreated != null) return false;
    } else if (!repoCreated.equals(other.repoCreated)) return false;
    if (repoId == null) {
      if (other.repoId != null) return false;
    } else if (!repoId.equals(other.repoId)) return false;
    if (repoName == null) {
      if (other.repoName != null) return false;
    } else if (!repoName.equals(other.repoName)) return false;
    if (repoUpdated == null) {
      if (other.repoUpdated != null) return false;
    } else if (!repoUpdated.equals(other.repoUpdated)) return false;
    if (Double.doubleToLongBits(size) != Double.doubleToLongBits(other.size)) return false;
    if (totalRecords == null) {
      if (other.totalRecords != null) return false;
    } else if (!totalRecords.equals(other.totalRecords)) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    return true;
  }
}

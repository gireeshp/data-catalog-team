package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 *
CREATE TABLE `data_request` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `objectId` int(11) DEFAULT '0',
  `objectType` varchar(50) DEFAULT '0',
  `requestedBy` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `requestedAccess` int(11) DEFAULT '0',
  `last_modified_by` int(11) DEFAULT '0',
  `justification` text,
  `created_date` varchar(50) DEFAULT '0',
  `modified_date` varchar(50) DEFAULT '0',
  `group_id` int(11) DEFAULT '0',
  `rejected_justification` text,
  `requested_user_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='-1 - To reject request\n0 - To pending request\n1 - To approved request';


   Alter table `data_request` change `datasourceid`  `objectId` int(11);
   Alter table `data_request` change `fromid` `requestedBy` int(11);
   Alter table `data_request` change `requested_group_id` `requested_user_id` int(11);
   Alter table `data_request` add `objectType` varchar(50);
   Alter table `data_request` add `requestedAccess` int(11);


 */
/** @author Ajinkya Marathe on 01-Mar-2017 */
@TableName(tableName = Sequences.DATA_REQUEST)
public class DataRequest implements Serializable {

  @Id private Long id;
  private Long objectId;
  private String objectType;
  private Long requestedBy;
  private Integer
      status; //requests Could be handled here only as requested For user only or for whole group
  private Integer requestedAccess;
  private Long last_modified_by;
  private String justification;
  private Long created_date;
  private Long modified_date;
  private Long group_id;
  private Long requested_user_id;
  private String rejected_justification;

  public DataRequest() {
    super();
  }

  public DataRequest(
      Long id,
      Long datasourceid,
      Long fromid,
      Integer status,
      Long last_modified_by,
      String justification,
      Long created_date,
      Long modified_date,
      Long group_id) {
    super();
    this.id = id;
    this.objectId = datasourceid;
    this.requestedBy = fromid;
    this.status = status;
    this.last_modified_by = last_modified_by;
    this.justification = justification;
    this.created_date = created_date;
    this.modified_date = modified_date;
    this.group_id = group_id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getObjectId() {
    return objectId;
  }

  public void setObjectId(Long objectId) {
    this.objectId = objectId;
  }

  public String getObjectType() {
    return objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  public Long getRequestedBy() {
    return requestedBy;
  }

  public void setRequestedBy(Long requestedBy) {
    this.requestedBy = requestedBy;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Integer getRequestedAccess() {
    return requestedAccess;
  }

  public void setRequestedAccess(Integer requestedAccess) {
    this.requestedAccess = requestedAccess;
  }

  public Long getLast_modified_by() {
    return last_modified_by;
  }

  public void setLast_modified_by(Long lastModifiedBy) {
    this.last_modified_by = lastModifiedBy;
  }

  public String getJustification() {
    return justification;
  }

  public void setJustification(String justification) {
    this.justification = justification;
  }

  public Long getCreated_date() {
    return created_date;
  }

  public void setCreated_date(Long createdDate) {
    this.created_date = createdDate;
  }

  public Long getModified_date() {
    return modified_date;
  }

  public void setModified_date(Long modifiedDate) {
    this.modified_date = modifiedDate;
  }

  public Long getGroup_id() {
    return group_id;
  }

  public void setGroup_id(Long groupId) {
    this.group_id = groupId;
  }

  public Long getRequested_user_id() {
    return requested_user_id;
  }

  public void setRequested_user_id(Long requestedUserId) {
    this.requested_user_id = requestedUserId;
  }

  public String getRejected_justification() {
    return rejected_justification;
  }

  public void setRejected_justification(String rejectedJustification) {
    this.rejected_justification = rejectedJustification;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((created_date == null) ? 0 : created_date.hashCode());
    result = prime * result + ((group_id == null) ? 0 : group_id.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((justification == null) ? 0 : justification.hashCode());
    result = prime * result + ((last_modified_by == null) ? 0 : last_modified_by.hashCode());
    result = prime * result + ((modified_date == null) ? 0 : modified_date.hashCode());
    result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
    result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
    result =
        prime * result + ((rejected_justification == null) ? 0 : rejected_justification.hashCode());
    result = prime * result + ((requestedAccess == null) ? 0 : requestedAccess.hashCode());
    result = prime * result + ((requestedBy == null) ? 0 : requestedBy.hashCode());
    result = prime * result + ((requested_user_id == null) ? 0 : requested_user_id.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataRequest other = (DataRequest) obj;
    if (created_date == null) {
      if (other.created_date != null) return false;
    } else if (!created_date.equals(other.created_date)) return false;
    if (group_id == null) {
      if (other.group_id != null) return false;
    } else if (!group_id.equals(other.group_id)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (justification == null) {
      if (other.justification != null) return false;
    } else if (!justification.equals(other.justification)) return false;
    if (last_modified_by == null) {
      if (other.last_modified_by != null) return false;
    } else if (!last_modified_by.equals(other.last_modified_by)) return false;
    if (modified_date == null) {
      if (other.modified_date != null) return false;
    } else if (!modified_date.equals(other.modified_date)) return false;
    if (objectId == null) {
      if (other.objectId != null) return false;
    } else if (!objectId.equals(other.objectId)) return false;
    if (objectType == null) {
      if (other.objectType != null) return false;
    } else if (!objectType.equals(other.objectType)) return false;
    if (rejected_justification == null) {
      if (other.rejected_justification != null) return false;
    } else if (!rejected_justification.equals(other.rejected_justification)) return false;
    if (requestedAccess == null) {
      if (other.requestedAccess != null) return false;
    } else if (!requestedAccess.equals(other.requestedAccess)) return false;
    if (requestedBy == null) {
      if (other.requestedBy != null) return false;
    } else if (!requestedBy.equals(other.requestedBy)) return false;
    if (requested_user_id == null) {
      if (other.requested_user_id != null) return false;
    } else if (!requested_user_id.equals(other.requested_user_id)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "DataRequest [id="
        + id
        + ", objectId="
        + objectId
        + ", objectType="
        + objectType
        + ", requestedBy="
        + requestedBy
        + ", status="
        + status
        + ", requestedAccess="
        + requestedAccess
        + ", last_modified_by="
        + last_modified_by
        + ", justification="
        + justification
        + ", created_date="
        + created_date
        + ", modified_date="
        + modified_date
        + ", group_id="
        + group_id
        + ", requested_group_id="
        + requested_user_id
        + ", rejected_justification="
        + rejected_justification
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileStoreObject;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;
import com.augmentiq.maxiq.constant.storage.strategy.FileStoreObjectStorageStrategy;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;

/**
 * * Each data source information will be present in this class.
 *
 * @author shiva
 */
@TableName(tableName = Sequences.CONFIGURATION)
public class DataSource implements Serializable {

  private static final long serialVersionUID = 1L;
  private String ingection = null;
  private Integer geoCodingPresent = 0;
  private Long id;
  private Long appId;

  private Long nodeId;
  private String streamType;
  private DataSourceType dataSourceType;
  private Long dataRepoId;
  private String dataSourceName;
  private List<FieldMapping> fieldMappings;
  private List<Long> fieldMappingsStore;
  private String headerString;
  private List<AdvancedValidations> advancedValidations;
  private List<FeatureExtraction> featureExtraction;
  private List<CustomValidation> customValidation;

  private Long externalDataId;
  private String externalDataType;
  private List<Long> advancedValidationsStore;
  private List<Transformations> transformations;
  private List<DataCleanser> dataCleansers;
  private String dataSourceIdentifier;
  private String userId;
  private String createdBy;
  private Boolean preIngestion = false;
  private Boolean postIngestion = false;

  // For ServerDataSources
  private Long refDataSourceId;

  /* For Data Marketplace */
  private String category;
  private String subCategory;
  private String description;

  // data at rest options
  private String dataAtRestFileType;
  private String dataAtRestCompressionType;

  private FileDataIngesterDetails fileDataIngesterDetails;

  private String prestoTableAlias;
  // For RDBMS and excel
  private ConnectionConfig config;
  private List<ExcelRange> excelRanges;
  private ConnectionSources connectionSources;

  

// For Incremental Load
  private DataSourceLoadStrategy dataSourceLoadStrategy;

  /* For Hbase index value storing */
  private List<FieldMapping> primaryKeysForIndex;
  private List<FieldMapping> secondaryKeys;
  private FileStoreObject fileStoreObject;
  private Boolean saveForVisualize;
  private Long indexId;

  /* Input Parameter */

  private List<InputParameterDomain> dsLevelParams;

  private String typeOfActiveInstances;
  private Integer noOfActiveInstances;

  /* For Exteranl Connector    */
  private ExternalDataSourceDetails externalDataSourceDetails;

  //project

  private Long ownerProjectId;
  private Long expert;
  private Long refreshedOn;

  public Long getRefreshedOn() {
	return refreshedOn;
}

public void setRefreshedOn(Long refreshedOn) {
	this.refreshedOn = refreshedOn;
}

public Long getExpert() {
	return expert;
}

public void setExpert(Long expert) {
	this.expert = expert;
}

public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  /* Tags */
  private List<String> tags;

  public List<String> getTags() {
    return tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public Long getRefDataSourceId() {
    return refDataSourceId;
  }

  public void setRefDataSourceId(Long refDataSourceId) {
    this.refDataSourceId = refDataSourceId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStreamType() {
    return streamType;
  }

  public void setStreamType(String streamType) {
    this.streamType = streamType;
  }

  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }

  public Boolean getSaveForVisualize() {
    return saveForVisualize == null ? false : saveForVisualize;
  }

  public void setSaveForVisualize(Boolean saveForVisualize) {
    this.saveForVisualize = saveForVisualize;
  }

  public Long getIndexId() {
    return indexId;
  }

  public void setIndexId(Long indexId) {
    this.indexId = indexId;
  }

  public List<FieldMapping> getPrimaryKeysForIndex() {
    return primaryKeysForIndex;
  }

  public void setPrimaryKeysForIndex(List<FieldMapping> primaryKeysForIndex) {
    this.primaryKeysForIndex = primaryKeysForIndex;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public List<FieldMapping> getSecondaryKeys() {
    return secondaryKeys;
  }

  public void setSecondaryKeys(List<FieldMapping> secondaryKeys) {
    this.secondaryKeys = secondaryKeys;
  }

  public FileStoreObject getFileStoreObject() {
    return null == fileStoreObject
        ? FileStoreObjectStorageStrategy.getFileStoreObjectFromStorageStrategy()
        : fileStoreObject;
  }

  public void setFileStoreObject(FileStoreObject fileStoreObject) {
    this.fileStoreObject = fileStoreObject;
  }

  public String getExternalDataType() {
    return externalDataType;
  }

  public void setExternalDataType(String externalDataType) {
    this.externalDataType = externalDataType;
  }

  public Long getExternalDataId() {
    return externalDataId;
  }

  public void setExternalDataId(Long externalDataId) {
    this.externalDataId = externalDataId;
  }

  public DataSourceLoadStrategy getDataSourceLoadStrategy() {
    return dataSourceLoadStrategy;
  }

  public List<CustomValidation> getCustomValidation() {
    return customValidation;
  }

  public void setCustomValidation(List<CustomValidation> customValidation) {
    this.customValidation = customValidation;
  }

  public void setDataSourceLoadStrategy(DataSourceLoadStrategy dataSourceLoadStrategy) {
    this.dataSourceLoadStrategy = dataSourceLoadStrategy;
  }

  public List<InputParameterDomain> getDsLevelParams() {
    return dsLevelParams;
  }

  public void setDsLevelParams(List<InputParameterDomain> dsLevelParams) {
    this.dsLevelParams = dsLevelParams;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public Long getNodeId() {
    return nodeId;
  }

  public void setNodeId(Long nodeId) {
    this.nodeId = nodeId;
  }

  public String getIngection() {
    return ingection == null ? "typical" : ingection;
  }

  public void setIngection(String ingection) {
    this.ingection = ingection;
  }

  public List<ExcelRange> getExcelRanges() {
    return excelRanges;
  }

  public void setExcelRanges(List<ExcelRange> excelRanges) {
    this.excelRanges = excelRanges;
  }

  public FileDataIngesterDetails getFileDataIngesterDetails() {
    return fileDataIngesterDetails;
  }

  public void setFileDataIngesterDetails(FileDataIngesterDetails fileDataIngesterDetails) {
    this.fileDataIngesterDetails = fileDataIngesterDetails;
  }

  public Integer getGeoCodingPresent() {
    return geoCodingPresent;
  }

  public void setGeoCodingPresent(Integer geoCodingPresent) {
    this.geoCodingPresent = geoCodingPresent;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getDataRepoId() {
    return dataRepoId;
  }

  public void setDataRepoId(Long dataRepoId) {
    this.dataRepoId = dataRepoId;
  }

  public String getDataSourceName() {
    return dataSourceName;
  }

  public void setDataSourceName(String dataSourceName) {
    this.dataSourceName = dataSourceName;
  }

  public List<FieldMapping> getFieldMappings() {
    return fieldMappings;
  }

  public void setFieldMappings(List<FieldMapping> fieldMappings) {
    this.fieldMappings = fieldMappings;
  }

  public List<Long> getFieldMappingsStore() {
    return fieldMappingsStore;
  }

  public void setFieldMappingsStore(List<Long> fieldMappingsStore) {
    this.fieldMappingsStore = fieldMappingsStore;
  }

  public String getHeaderString() {
    return headerString;
  }

  public void setHeaderString(String headerString) {
    this.headerString = headerString;
  }

  public List<AdvancedValidations> getAdvancedValidations() {
    return advancedValidations;
  }

  public void setAdvancedValidations(List<AdvancedValidations> advancedValidations) {
    this.advancedValidations = advancedValidations;
  }

  public List<Long> getAdvancedValidationsStore() {
    return advancedValidationsStore;
  }

  public void setAdvancedValidationsStore(List<Long> advancedValidationsStore) {
    this.advancedValidationsStore = advancedValidationsStore;
  }

  public List<Transformations> getTransformations() {
    return transformations;
  }

  public void setTransformations(List<Transformations> transformations) {
    this.transformations = transformations;
  }

  public List<DataCleanser> getDataCleansers() {
    return dataCleansers;
  }

  public void setDataCleansers(List<DataCleanser> dataCleansers) {
    this.dataCleansers = dataCleansers;
  }

  public DataSourceType getDataSourceType() {
    return dataSourceType;
  }

  public void setDataSourceType(DataSourceType dataSourceType) {
    this.dataSourceType = dataSourceType;
  }

  public String getDataSourceIdentifier() {
    return dataSourceIdentifier;
  }

  public void setDataSourceIdentifier(String dataSourceIdentifier) {
    this.dataSourceIdentifier = dataSourceIdentifier;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

    public ConnectionConfig getConfig() {
	return config;
    }

    public void setConfig(ConnectionConfig config) {
	this.config = config;
    }

    public Boolean getPreIngestion() {
	return preIngestion;
    }

  public void setPreIngestion(Boolean preIngestion) {
    this.preIngestion = preIngestion;
  }

  public Boolean getPostIngestion() {
    return postIngestion;
  }

  public void setPostIngestion(Boolean postIngestion) {
    this.postIngestion = postIngestion;
  }

  public List<FeatureExtraction> getFeatureExtraction() {
    return featureExtraction;
  }

  public void setFeatureExtraction(List<FeatureExtraction> featureExtraction) {
    this.featureExtraction = featureExtraction;
  }

  public String getDataAtRestFileType() {
    return dataAtRestFileType == null ? FileTypeEnum.PARQUET.name() : dataAtRestFileType;
  }

  public void setDataAtRestFileType(String dataAtRestFileType) {
    this.dataAtRestFileType = dataAtRestFileType;
  }

  public String getDataAtRestCompressionType() {
    return dataAtRestCompressionType == null
        ? CompressionTypeEnum.SNAPPY.name()
        : dataAtRestCompressionType;
  }

  public void setDataAtRestCompressionType(String dataAtRestCompressionType) {
    this.dataAtRestCompressionType = dataAtRestCompressionType;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getTypeOfActiveInstances() {
    return typeOfActiveInstances == null ? "DEFAULT" : typeOfActiveInstances;
  }

  public void setTypeOfActiveInstances(String typeOfActiveInstances) {
    this.typeOfActiveInstances = typeOfActiveInstances;
  }

  public Integer getNoOfActiveInstances() {
    return noOfActiveInstances;
  }

  public void setNoOfActiveInstances(Integer noOfActiveInstances) {
    this.noOfActiveInstances = noOfActiveInstances;
  }

  public ExternalDataSourceDetails getExternalDataSourceDetails() {
    return externalDataSourceDetails;
  }

  public void setExternalDataSourceDetails(ExternalDataSourceDetails externalDataSourceDetails) {
    this.externalDataSourceDetails = externalDataSourceDetails;
  }
  
  
  public ConnectionSources getConnectionSources() {
		return connectionSources;
	}

	public void setConnectionSources(ConnectionSources connectionSources) {
		this.connectionSources = connectionSources;
	}


public String getPrestoTableAlias() {
    return prestoTableAlias;
}

public void setPrestoTableAlias(String prestoTableAlias) {
    this.prestoTableAlias = prestoTableAlias;
}

@Override
  public String toString() {
    return "DataSource [ingection="
        + ingection
        + ", geoCodingPresent="
        + geoCodingPresent
        + ", id="
        + id
        + ", appId="
        + appId
        + ", nodeId="
        + nodeId
        + ", streamType="
        + streamType
        + ", dataSourceType="
        + dataSourceType
        + ", dataRepoId="
        + dataRepoId
        + ", dataSourceName="
        + dataSourceName
        + ", fieldMappings="
        + fieldMappings
        + ", fieldMappingsStore="
        + fieldMappingsStore
        + ", headerString="
        + headerString
        + ", advancedValidations="
        + advancedValidations
        + ", featureExtraction="
        + featureExtraction
        + ", customValidation="
        + customValidation
        + ", externalDataId="
        + externalDataId
        + ", externalDataType="
        + externalDataType
        + ", advancedValidationsStore="
        + advancedValidationsStore
        + ", transformations="
        + transformations
        + ", dataCleansers="
        + dataCleansers
        + ", dataSourceIdentifier="
        + dataSourceIdentifier
        + ", userId="
        + userId
        + ", createdBy="
        + createdBy
        + ", preIngestion="
        + preIngestion
        + ", postIngestion="
        + postIngestion
        + ", refDataSourceId="
        + refDataSourceId
        + ", category="
        + category
        + ", subCategory="
        + subCategory
        + ", description="
        + description
        + ", dataAtRestFileType="
        + dataAtRestFileType
        + ", dataAtRestCompressionType="
        + dataAtRestCompressionType
        + ", fileDataIngesterDetails="
        + fileDataIngesterDetails
        + ", config="
        + config
        + ", excelRanges="
        + excelRanges
        + ", dataSourceLoadStrategy="
        + dataSourceLoadStrategy
        + ", primaryKeysForIndex="
        + primaryKeysForIndex
        + ", secondaryKeys="
        + secondaryKeys
        + ", fileStoreObject="
        + fileStoreObject
        + ", saveForVisualize="
        + saveForVisualize
        + ", indexId="
        + indexId
        + ", dsLevelParams="
        + dsLevelParams
        + ", typeOfActiveInstances="
        + typeOfActiveInstances
        + ", noOfActiveInstances="
        + noOfActiveInstances
        + ", ownerProjectId="
        + ownerProjectId
        + ", tags="
        + tags
        + ", externalDataSourceDetails="
        + externalDataSourceDetails
        +", expert="
        +expert
        +", refreshedOn"
        +refreshedOn
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((advancedValidations == null) ? 0 : advancedValidations.hashCode());
    result =
        prime * result
            + ((advancedValidationsStore == null) ? 0 : advancedValidationsStore.hashCode());
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((category == null) ? 0 : category.hashCode());
    result = prime * result + ((config == null) ? 0 : config.hashCode());
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((customValidation == null) ? 0 : customValidation.hashCode());
    result =
        prime * result
            + ((dataAtRestCompressionType == null) ? 0 : dataAtRestCompressionType.hashCode());
    result = prime * result + ((dataAtRestFileType == null) ? 0 : dataAtRestFileType.hashCode());
    result = prime * result + ((dataCleansers == null) ? 0 : dataCleansers.hashCode());
    result = prime * result + ((dataRepoId == null) ? 0 : dataRepoId.hashCode());
    result =
        prime * result + ((dataSourceIdentifier == null) ? 0 : dataSourceIdentifier.hashCode());
    result =
        prime * result + ((dataSourceLoadStrategy == null) ? 0 : dataSourceLoadStrategy.hashCode());
    result = prime * result + ((dataSourceName == null) ? 0 : dataSourceName.hashCode());
    result = prime * result + ((dataSourceType == null) ? 0 : dataSourceType.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((dsLevelParams == null) ? 0 : dsLevelParams.hashCode());
    result = prime * result + ((excelRanges == null) ? 0 : excelRanges.hashCode());
    result = prime * result + ((externalDataId == null) ? 0 : externalDataId.hashCode());
    result = prime * result + ((externalDataType == null) ? 0 : externalDataType.hashCode());
    result = prime * result + ((featureExtraction == null) ? 0 : featureExtraction.hashCode());
    result = prime * result + ((fieldMappings == null) ? 0 : fieldMappings.hashCode());
    result = prime * result + ((fieldMappingsStore == null) ? 0 : fieldMappingsStore.hashCode());
    result =
        prime * result
            + ((fileDataIngesterDetails == null) ? 0 : fileDataIngesterDetails.hashCode());
    result = prime * result + ((fileStoreObject == null) ? 0 : fileStoreObject.hashCode());
    result = prime * result + ((geoCodingPresent == null) ? 0 : geoCodingPresent.hashCode());
    result = prime * result + ((headerString == null) ? 0 : headerString.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((indexId == null) ? 0 : indexId.hashCode());
    result = prime * result + ((ingection == null) ? 0 : ingection.hashCode());
    result = prime * result + ((noOfActiveInstances == null) ? 0 : noOfActiveInstances.hashCode());
    result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    result = prime * result + ((postIngestion == null) ? 0 : postIngestion.hashCode());
    result = prime * result + ((preIngestion == null) ? 0 : preIngestion.hashCode());
    result = prime * result + ((primaryKeysForIndex == null) ? 0 : primaryKeysForIndex.hashCode());
    result = prime * result + ((refDataSourceId == null) ? 0 : refDataSourceId.hashCode());
    result = prime * result + ((saveForVisualize == null) ? 0 : saveForVisualize.hashCode());
    result = prime * result + ((secondaryKeys == null) ? 0 : secondaryKeys.hashCode());
    result = prime * result + ((streamType == null) ? 0 : streamType.hashCode());
    result = prime * result + ((subCategory == null) ? 0 : subCategory.hashCode());
    result = prime * result + ((tags == null) ? 0 : tags.hashCode());
    result = prime * result + ((transformations == null) ? 0 : transformations.hashCode());
    result =
        prime * result + ((typeOfActiveInstances == null) ? 0 : typeOfActiveInstances.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    result =
        prime * result
            + ((externalDataSourceDetails == null) ? 0 : externalDataSourceDetails.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSource other = (DataSource) obj;
    if (advancedValidations == null) {
      if (other.advancedValidations != null) return false;
    } else if (!advancedValidations.equals(other.advancedValidations)) return false;
    if (advancedValidationsStore == null) {
      if (other.advancedValidationsStore != null) return false;
    } else if (!advancedValidationsStore.equals(other.advancedValidationsStore)) return false;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (category == null) {
      if (other.category != null) return false;
    } else if (!category.equals(other.category)) return false;
    if (config == null) {
      if (other.config != null) return false;
    } else if (!config.equals(other.config)) return false;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (customValidation == null) {
      if (other.customValidation != null) return false;
    } else if (!customValidation.equals(other.customValidation)) return false;
    if (dataAtRestCompressionType == null) {
      if (other.dataAtRestCompressionType != null) return false;
    } else if (!dataAtRestCompressionType.equals(other.dataAtRestCompressionType)) return false;
    if (dataAtRestFileType == null) {
      if (other.dataAtRestFileType != null) return false;
    } else if (!dataAtRestFileType.equals(other.dataAtRestFileType)) return false;
    if (dataCleansers == null) {
      if (other.dataCleansers != null) return false;
    } else if (!dataCleansers.equals(other.dataCleansers)) return false;
    if (dataRepoId == null) {
      if (other.dataRepoId != null) return false;
    } else if (!dataRepoId.equals(other.dataRepoId)) return false;
    if (dataSourceIdentifier == null) {
      if (other.dataSourceIdentifier != null) return false;
    } else if (!dataSourceIdentifier.equals(other.dataSourceIdentifier)) return false;
    if (dataSourceLoadStrategy == null) {
      if (other.dataSourceLoadStrategy != null) return false;
    } else if (!dataSourceLoadStrategy.equals(other.dataSourceLoadStrategy)) return false;
    if (dataSourceName == null) {
      if (other.dataSourceName != null) return false;
    } else if (!dataSourceName.equals(other.dataSourceName)) return false;
    if (dataSourceType != other.dataSourceType) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (dsLevelParams == null) {
      if (other.dsLevelParams != null) return false;
    } else if (!dsLevelParams.equals(other.dsLevelParams)) return false;
    if (excelRanges == null) {
      if (other.excelRanges != null) return false;
    } else if (!excelRanges.equals(other.excelRanges)) return false;
    if (externalDataId == null) {
      if (other.externalDataId != null) return false;
    } else if (!externalDataId.equals(other.externalDataId)) return false;
    if (externalDataType == null) {
      if (other.externalDataType != null) return false;
    } else if (!externalDataType.equals(other.externalDataType)) return false;
    if (featureExtraction == null) {
      if (other.featureExtraction != null) return false;
    } else if (!featureExtraction.equals(other.featureExtraction)) return false;
    if (fieldMappings == null) {
      if (other.fieldMappings != null) return false;
    } else if (!fieldMappings.equals(other.fieldMappings)) return false;
    if (fieldMappingsStore == null) {
      if (other.fieldMappingsStore != null) return false;
    } else if (!fieldMappingsStore.equals(other.fieldMappingsStore)) return false;
    if (fileDataIngesterDetails == null) {
      if (other.fileDataIngesterDetails != null) return false;
    } else if (!fileDataIngesterDetails.equals(other.fileDataIngesterDetails)) return false;
    if (fileStoreObject != other.fileStoreObject) return false;
    if (geoCodingPresent == null) {
      if (other.geoCodingPresent != null) return false;
    } else if (!geoCodingPresent.equals(other.geoCodingPresent)) return false;
    if (headerString == null) {
      if (other.headerString != null) return false;
    } else if (!headerString.equals(other.headerString)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (indexId == null) {
      if (other.indexId != null) return false;
    } else if (!indexId.equals(other.indexId)) return false;
    if (ingection == null) {
      if (other.ingection != null) return false;
    } else if (!ingection.equals(other.ingection)) return false;
    if (noOfActiveInstances == null) {
      if (other.noOfActiveInstances != null) return false;
    } else if (!noOfActiveInstances.equals(other.noOfActiveInstances)) return false;
    if (nodeId == null) {
      if (other.nodeId != null) return false;
    } else if (!nodeId.equals(other.nodeId)) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    if (postIngestion == null) {
      if (other.postIngestion != null) return false;
    } else if (!postIngestion.equals(other.postIngestion)) return false;
    if (preIngestion == null) {
      if (other.preIngestion != null) return false;
    } else if (!preIngestion.equals(other.preIngestion)) return false;
    if (primaryKeysForIndex == null) {
      if (other.primaryKeysForIndex != null) return false;
    } else if (!primaryKeysForIndex.equals(other.primaryKeysForIndex)) return false;
    if (refDataSourceId == null) {
      if (other.refDataSourceId != null) return false;
    } else if (!refDataSourceId.equals(other.refDataSourceId)) return false;
    if (saveForVisualize == null) {
      if (other.saveForVisualize != null) return false;
    } else if (!saveForVisualize.equals(other.saveForVisualize)) return false;
    if (secondaryKeys == null) {
      if (other.secondaryKeys != null) return false;
    } else if (!secondaryKeys.equals(other.secondaryKeys)) return false;
    if (streamType == null) {
      if (other.streamType != null) return false;
    } else if (!streamType.equals(other.streamType)) return false;
    if (subCategory == null) {
      if (other.subCategory != null) return false;
    } else if (!subCategory.equals(other.subCategory)) return false;
    if (tags == null) {
      if (other.tags != null) return false;
    } else if (!tags.equals(other.tags)) return false;
    if (transformations == null) {
      if (other.transformations != null) return false;
    } else if (!transformations.equals(other.transformations)) return false;
    if (typeOfActiveInstances == null) {
      if (other.typeOfActiveInstances != null) return false;
    } else if (!typeOfActiveInstances.equals(other.typeOfActiveInstances)) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    if (externalDataSourceDetails == null) {
      if (other.externalDataSourceDetails != null) return false;
    } else if (!externalDataSourceDetails.equals(other.externalDataSourceDetails)) return false;
    return true;
  }
}

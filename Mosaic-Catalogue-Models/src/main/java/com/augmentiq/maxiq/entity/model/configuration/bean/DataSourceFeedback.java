package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 * @author: Mayuri Narawade
 * create table feedback(id varchar(40), dataSourceId varchar(40), unqUserId int(20), groupId int(10), rating varchar(20),question varchar(50),answer blob,insertTS varchar(100), userName varchar(100));*/
@TableName(tableName = Sequences.FEEDBACK)
public class DataSourceFeedback implements Serializable {
  @Id public Long id;
  public String dataSourceId;
  public String unqUserId;
  public Long groupId;
  public Long rating;
  public String question;
  public String answer;
  public String insertTS;

  public String getInsertTS() {
    return insertTS;
  }

  public void setInsertTS(String insertTS) {
    this.insertTS = insertTS;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(String dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public String getUnqUserId() {
    return unqUserId;
  }

  public void setUnqUserId(String unqUserId) {
    this.unqUserId = unqUserId;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getRating() {
    return rating;
  }

  public void setRating(Long rating) {
    this.rating = rating;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((answer == null) ? 0 : answer.hashCode());
    result = prime * result + ((dataSourceId == null) ? 0 : dataSourceId.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((insertTS == null) ? 0 : insertTS.hashCode());
    result = prime * result + ((question == null) ? 0 : question.hashCode());
    result = prime * result + ((rating == null) ? 0 : rating.hashCode());
    result = prime * result + ((unqUserId == null) ? 0 : unqUserId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSourceFeedback other = (DataSourceFeedback) obj;
    if (answer == null) {
      if (other.answer != null) return false;
    } else if (!answer.equals(other.answer)) return false;
    if (dataSourceId == null) {
      if (other.dataSourceId != null) return false;
    } else if (!dataSourceId.equals(other.dataSourceId)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (insertTS == null) {
      if (other.insertTS != null) return false;
    } else if (!insertTS.equals(other.insertTS)) return false;
    if (question == null) {
      if (other.question != null) return false;
    } else if (!question.equals(other.question)) return false;
    if (rating == null) {
      if (other.rating != null) return false;
    } else if (!rating.equals(other.rating)) return false;
    if (unqUserId == null) {
      if (other.unqUserId != null) return false;
    } else if (!unqUserId.equals(other.unqUserId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "DataSourceFeedback [id="
        + id
        + ", dataSourceId="
        + dataSourceId
        + ", unqUserId="
        + unqUserId
        + ", groupId="
        + groupId
        + ", rating="
        + rating
        + ", question="
        + question
        + ", answer="
        + answer
        + ", insertTS="
        + insertTS
        + "]";
  }
}

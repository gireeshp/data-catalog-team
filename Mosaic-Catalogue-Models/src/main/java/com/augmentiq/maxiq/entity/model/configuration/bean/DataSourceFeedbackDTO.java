package com.augmentiq.maxiq.entity.model.configuration.bean;

public class DataSourceFeedbackDTO extends DataSourceFeedback {

  Object votingCount;
  Object votingPositiveCount;
  Object votingNegativeCount;
  Object selfVote;

  public Object getVotingCount() {
    return votingCount;
  }

  public void setVotingCount(Object object) {
    this.votingCount = object;
  }

  public Object getVotingPositiveCount() {
    return votingPositiveCount;
  }

  public void setVotingPositiveCount(Object votingPositiveCount) {
    this.votingPositiveCount = votingPositiveCount;
  }

  public Object getVotingNegativeCount() {
    return votingNegativeCount;
  }

  public void setVotingNegativeCount(Object votingNegativeCount) {
    this.votingNegativeCount = votingNegativeCount;
  }

  public Object getSelfVote() {
    return selfVote;
  }

  public void setSelfVote(Object selfVote) {
    this.selfVote = selfVote;
  }

  @Override
  public String toString() {
    return "DataSourceFeedbackDTO [votingCount="
        + votingCount
        + ", votingPositiveCount="
        + votingPositiveCount
        + ", votingNegativeCount="
        + votingNegativeCount
        + ", selfVote="
        + selfVote
        + ", id="
        + id
        + ", dataSourceId="
        + dataSourceId
        + ", unqUserId="
        + unqUserId
        + ", groupId="
        + groupId
        + ", rating="
        + rating
        + ", question="
        + question
        + ", answer="
        + answer
        + ", insertTS="
        + insertTS
        + "]";
  }
}

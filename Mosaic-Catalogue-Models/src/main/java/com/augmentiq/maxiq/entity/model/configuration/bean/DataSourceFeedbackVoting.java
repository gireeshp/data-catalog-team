package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
* CREATE TABLE `feedback_voting` (
	  `id` int(11) NOT NULL,
	  `feedbackId` int(11) NOT NULL,
	  `dataSourceId` int(11) NOT NULL,
	  `unqUserId` int(11) NOT NULL,
	  `created_date` VARCHAR(45) DEFAULT NULL,
	  `status` int(11) DEFAULT NULL,
	  `type` varchar(45) DEFAULT NULL COMMENT 'Keep it in case if want to vote agains datasource/flow',
	  `vote` int(11) NOT NULL COMMENT 'use ''1'' - voted as positve & ''-1'' - voted as negative',
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;*/
@TableName(tableName = Sequences.FEEDBACK_VOTING)
public class DataSourceFeedbackVoting {
  @Id private Long id;
  private Long feedbackId;
  private Long dataSourceId;
  private Long unqUserId;
  private Long created_date;
  private Integer status;
  private String type;
  private Integer vote;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getFeedbackId() {
    return feedbackId;
  }

  public void setFeedbackId(Long feedbackId) {
    this.feedbackId = feedbackId;
  }

  public Long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(Long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public Long getUnqUserId() {
    return unqUserId;
  }

  public void setUnqUserId(Long unqUserId) {
    this.unqUserId = unqUserId;
  }

  public Long getCreated_date() {
    return created_date;
  }

  public void setCreated_date(Long created_date) {
    this.created_date = created_date;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Integer getVote() {
    return vote;
  }

  public void setVote(Integer vote) {
    this.vote = vote;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((created_date == null) ? 0 : created_date.hashCode());
    result = prime * result + ((dataSourceId == null) ? 0 : dataSourceId.hashCode());
    result = prime * result + ((feedbackId == null) ? 0 : feedbackId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    result = prime * result + ((unqUserId == null) ? 0 : unqUserId.hashCode());
    result = prime * result + ((vote == null) ? 0 : vote.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSourceFeedbackVoting other = (DataSourceFeedbackVoting) obj;
    if (created_date == null) {
      if (other.created_date != null) return false;
    } else if (!created_date.equals(other.created_date)) return false;
    if (dataSourceId == null) {
      if (other.dataSourceId != null) return false;
    } else if (!dataSourceId.equals(other.dataSourceId)) return false;
    if (feedbackId == null) {
      if (other.feedbackId != null) return false;
    } else if (!feedbackId.equals(other.feedbackId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    if (type == null) {
      if (other.type != null) return false;
    } else if (!type.equals(other.type)) return false;
    if (unqUserId == null) {
      if (other.unqUserId != null) return false;
    } else if (!unqUserId.equals(other.unqUserId)) return false;
    if (vote == null) {
      if (other.vote != null) return false;
    } else if (!vote.equals(other.vote)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "DataSourceFeedbackVoting [id="
        + id
        + ", feedbackId="
        + feedbackId
        + ", dataSourceId="
        + dataSourceId
        + ", unqUserId="
        + unqUserId
        + ", created_date="
        + created_date
        + ", status="
        + status
        + ", type="
        + type
        + ", vote="
        + vote
        + "]";
  }
}

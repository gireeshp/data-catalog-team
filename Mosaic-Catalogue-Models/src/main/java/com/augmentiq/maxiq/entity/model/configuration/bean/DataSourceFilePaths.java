package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Date;

public class DataSourceFilePaths {
  private Date createdDate_;
  private String inputFile_;
  private String outputFile_;
  private String version_;

  public Date getCreatedDate() {
    return createdDate_;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate_ = createdDate;
  }

  public String getInputFile() {
    return inputFile_;
  }

  public void setInputFile(String inputFile) {
    this.inputFile_ = inputFile;
  }

  public String getOutputFile() {
    return outputFile_;
  }

  public void setOutputFile(String outputFile) {
    this.outputFile_ = outputFile;
  }

  public String getVersion() {
    return version_;
  }

  public void setVersion(String version) {
    this.version_ = version;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DataSourceFilePaths [createdDate_="
        + createdDate_
        + ", inputFile_="
        + inputFile_
        + ", outputFile_="
        + outputFile_
        + ", version_="
        + version_
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((createdDate_ == null) ? 0 : createdDate_.hashCode());
    result = prime * result + ((inputFile_ == null) ? 0 : inputFile_.hashCode());
    result = prime * result + ((outputFile_ == null) ? 0 : outputFile_.hashCode());
    result = prime * result + ((version_ == null) ? 0 : version_.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSourceFilePaths other = (DataSourceFilePaths) obj;
    if (createdDate_ == null) {
      if (other.createdDate_ != null) return false;
    } else if (!createdDate_.equals(other.createdDate_)) return false;
    if (inputFile_ == null) {
      if (other.inputFile_ != null) return false;
    } else if (!inputFile_.equals(other.inputFile_)) return false;
    if (outputFile_ == null) {
      if (other.outputFile_ != null) return false;
    } else if (!outputFile_.equals(other.outputFile_)) return false;
    if (version_ == null) {
      if (other.version_ != null) return false;
    } else if (!version_.equals(other.version_)) return false;
    return true;
  }
}

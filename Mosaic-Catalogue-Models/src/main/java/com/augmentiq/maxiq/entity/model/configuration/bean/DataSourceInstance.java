package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Date;
import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.DataStatus;
import com.augmentiq.maxiq.constant.configuration.enums.FileStoreObject;
import com.augmentiq.maxiq.constant.configuration.enums.FlowType;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * @author shivanand on 04-aug-2015 Alter table for Adding old filepath for version maintianance
 *     alter table datasourceinstance add column oldFilePath varchar(500);
 */
@TableName(tableName = Sequences.DATASOURCE_INSTANCE)
public class DataSourceInstance {
  @Id private Long dataSourceId;
  private String ingection;
  private String dataSourceName;
  private Long appId;
  private Date createdDate;

  /*
   * This is last refreshed date - even though name suggest last updated date.
   * Need to audit old data sources if we rename this.
   */
  private Date updatedDate;
  private String fileType;
  private String compressionType;
  private DataStatus dataSourceStatus;
  private Long totalRecords;
  private Long size;
  private List<DataSourceFilePaths> filePaths;
  private String createdBy;
  private String latestFilepath;
  private String oldFilePath;
  private Long dataRepoId;
  private String dataRepoName;
  private String lastRunBy;
  private String dataSourceType;
  private Long groupId;

  // This is true if datasource is created from app as input parameter as file
  // type
  private String fileTypeIndicator;

  private String category;
  private String subCategory;
  private String description;
  private Long avgRating;
  private Long ownerProjectId;

  public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  // mode of data transfer before processing data
  // value source indicates data won't be stored anywhere instead will be
  // picked from source only to process
  private FileStoreObject fileStoreObject;

  public FileStoreObject getFileStoreObject() {
    return fileStoreObject;
  }

  public void setFileStoreObject(FileStoreObject fileStoreObject) {
    this.fileStoreObject = fileStoreObject;
  }

  private FlowType flowType;

  // DataSource versioning parameters

  private Long fieldMappingVersionId;
  private Long dataSourceVersionId;

  public Long getFieldMappingVersionId() {
    return fieldMappingVersionId;
  }

  public void setFieldMappingVersionId(Long fieldMappingVersionId) {
    this.fieldMappingVersionId = fieldMappingVersionId;
  }

  public Long getDataSourceVersionId() {
    return dataSourceVersionId;
  }

  public void setDataSourceVersionId(Long dataSourceVersionId) {
    this.dataSourceVersionId = dataSourceVersionId;
  }

  public FlowType getFlowType() {
    return flowType;
  }

  public void setFlowType(FlowType flowType) {
    this.flowType = flowType;
  }

  public Long getAvgRating() {
    return avgRating;
  }

  public void setAvgRating(Long avgRating) {
    this.avgRating = avgRating;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFileTypeIndicator() {
    return fileTypeIndicator;
  }

  public void setFileTypeIndicator(String fileTypeIndicator) {
    this.fileTypeIndicator = fileTypeIndicator;
  }

  public String getFileType() {
    return fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public String getCompressionType() {
    return compressionType;
  }

  public void setCompressionType(String compressionType) {
    this.compressionType = compressionType;
  }

  public String getOldFilePath() {
    return oldFilePath;
  }

  public void setOldFilePath(String oldFilePath) {
    this.oldFilePath = oldFilePath;
  }

  public String getIngection() {
    return ingection;
  }

  public void setIngection(String ingection) {
    this.ingection = ingection;
  }

  public String getDataRepoName() {
    return dataRepoName;
  }

  public void setDataRepoName(String dataRepoName) {
    this.dataRepoName = dataRepoName;
  }

  public String getLastRunBy() {
    return lastRunBy;
  }

  public void setLastRunBy(String lastRunBy) {
    this.lastRunBy = lastRunBy;
  }

  public String getDataSourceType() {
    return dataSourceType;
  }

  public void setDataSourceType(String dataSourceType) {
    this.dataSourceType = dataSourceType;
  }

  public Long getDataRepoId() {
    return dataRepoId;
  }

  public void setDataRepoId(Long dataRepoId) {
    this.dataRepoId = dataRepoId;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public String getDataSourceName() {
    return dataSourceName;
  }

  public void setDataSourceName(String dataSourceName) {
    this.dataSourceName = dataSourceName;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

  public DataStatus getDataSourceStatus() {
    return dataSourceStatus;
  }

  public void setDataSourceStatus(DataStatus dataSourceStatus) {
    this.dataSourceStatus = dataSourceStatus;
  }

  public long getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(long totalRecords) {
    this.totalRecords = totalRecords;
  }

  public long getSize() {
    return size;
  }

  public void setSize(long size) {
    this.size = size;
  }

  public Long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(Long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public List<DataSourceFilePaths> getFilePaths() {
    return filePaths;
  }

  public void setFilePaths(List<DataSourceFilePaths> filePaths) {
    this.filePaths = filePaths;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getLatestFilepath() {
    return latestFilepath;
  }

  public void setLatestFilepath(String latestFilepath) {
    this.latestFilepath = latestFilepath;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }

  @Override
  public String toString() {
    return "DataSourceInstance [ ingection="
        + ingection
        + ", dataSourceName="
        + dataSourceName
        + ", appId="
        + appId
        + ", createdDate="
        + createdDate
        + ", updatedDate="
        + updatedDate
        + ", fileType="
        + fileType
        + ", compressionType="
        + compressionType
        + ", dataSourceStatus="
        + dataSourceStatus
        + ", totalRecords="
        + totalRecords
        + ", size="
        + size
        + ", dataSourceId="
        + dataSourceId
        + ", filePaths="
        + filePaths
        + ", createdBy="
        + createdBy
        + ", latestFilepath="
        + latestFilepath
        + ", oldFilePath="
        + oldFilePath
        + ", dataRepoId="
        + dataRepoId
        + ", dataRepoName="
        + dataRepoName
        + ", lastRunBy="
        + lastRunBy
        + ", dataSourceType="
        + dataSourceType
        + ", groupId="
        + groupId
        + ", fileTypeIndicator="
        + fileTypeIndicator
        + ", category="
        + category
        + ", subCategory="
        + subCategory
        + ", description="
        + description
        + ", avgRating="
        + avgRating
        + ", ownerProjectId="
        + ownerProjectId
        + ", flowType="
        + flowType
        + ", fieldMappingVersionId="
        + fieldMappingVersionId
        + ", dataSourceVersionId="
        + dataSourceVersionId
        + " , fileStoreObject= "
        + fileStoreObject
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((avgRating == null) ? 0 : avgRating.hashCode());
    result = prime * result + ((category == null) ? 0 : category.hashCode());
    result = prime * result + ((compressionType == null) ? 0 : compressionType.hashCode());
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
    result = prime * result + ((dataRepoId == null) ? 0 : dataRepoId.hashCode());
    result = prime * result + ((dataRepoName == null) ? 0 : dataRepoName.hashCode());
    result = prime * result + ((dataSourceId == null) ? 0 : dataSourceId.hashCode());
    result = prime * result + ((dataSourceName == null) ? 0 : dataSourceName.hashCode());
    result = prime * result + ((dataSourceStatus == null) ? 0 : dataSourceStatus.hashCode());
    result = prime * result + ((dataSourceType == null) ? 0 : dataSourceType.hashCode());
    result = prime * result + ((dataSourceVersionId == null) ? 0 : dataSourceVersionId.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result =
        prime * result + ((fieldMappingVersionId == null) ? 0 : fieldMappingVersionId.hashCode());
    result = prime * result + ((filePaths == null) ? 0 : filePaths.hashCode());
    result = prime * result + ((fileType == null) ? 0 : fileType.hashCode());
    result = prime * result + ((fileTypeIndicator == null) ? 0 : fileTypeIndicator.hashCode());
    result = prime * result + ((flowType == null) ? 0 : flowType.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((ingection == null) ? 0 : ingection.hashCode());
    result = prime * result + ((lastRunBy == null) ? 0 : lastRunBy.hashCode());
    result = prime * result + ((latestFilepath == null) ? 0 : latestFilepath.hashCode());
    result = prime * result + ((oldFilePath == null) ? 0 : oldFilePath.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    result = prime * result + (int) (size ^ (size >>> 32));
    result = prime * result + ((subCategory == null) ? 0 : subCategory.hashCode());
    result = prime * result + (int) (totalRecords ^ (totalRecords >>> 32));
    result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
    result = prime * result + ((fileStoreObject == null) ? 0 : fileStoreObject.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSourceInstance other = (DataSourceInstance) obj;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (avgRating == null) {
      if (other.avgRating != null) return false;
    } else if (!avgRating.equals(other.avgRating)) return false;
    if (category == null) {
      if (other.category != null) return false;
    } else if (!category.equals(other.category)) return false;
    if (compressionType == null) {
      if (other.compressionType != null) return false;
    } else if (!compressionType.equals(other.compressionType)) return false;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdDate == null) {
      if (other.createdDate != null) return false;
    } else if (!createdDate.equals(other.createdDate)) return false;
    if (dataRepoId == null) {
      if (other.dataRepoId != null) return false;
    } else if (!dataRepoId.equals(other.dataRepoId)) return false;
    if (dataRepoName == null) {
      if (other.dataRepoName != null) return false;
    } else if (!dataRepoName.equals(other.dataRepoName)) return false;
    if (dataSourceId == null) {
      if (other.dataSourceId != null) return false;
    } else if (!dataSourceId.equals(other.dataSourceId)) return false;
    if (dataSourceName == null) {
      if (other.dataSourceName != null) return false;
    } else if (!dataSourceName.equals(other.dataSourceName)) return false;
    if (dataSourceStatus != other.dataSourceStatus) return false;
    if (dataSourceType == null) {
      if (other.dataSourceType != null) return false;
    } else if (!dataSourceType.equals(other.dataSourceType)) return false;
    if (dataSourceVersionId == null) {
      if (other.dataSourceVersionId != null) return false;
    } else if (!dataSourceVersionId.equals(other.dataSourceVersionId)) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (fieldMappingVersionId == null) {
      if (other.fieldMappingVersionId != null) return false;
    } else if (!fieldMappingVersionId.equals(other.fieldMappingVersionId)) return false;
    if (filePaths == null) {
      if (other.filePaths != null) return false;
    } else if (!filePaths.equals(other.filePaths)) return false;
    if (fileType == null) {
      if (other.fileType != null) return false;
    } else if (!fileType.equals(other.fileType)) return false;
    if (fileTypeIndicator == null) {
      if (other.fileTypeIndicator != null) return false;
    } else if (!fileTypeIndicator.equals(other.fileTypeIndicator)) return false;
    if (flowType != other.flowType) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (ingection == null) {
      if (other.ingection != null) return false;
    } else if (!ingection.equals(other.ingection)) return false;
    if (lastRunBy == null) {
      if (other.lastRunBy != null) return false;
    } else if (!lastRunBy.equals(other.lastRunBy)) return false;
    if (latestFilepath == null) {
      if (other.latestFilepath != null) return false;
    } else if (!latestFilepath.equals(other.latestFilepath)) return false;
    if (oldFilePath == null) {
      if (other.oldFilePath != null) return false;
    } else if (!oldFilePath.equals(other.oldFilePath)) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    if (size != other.size) return false;
    if (subCategory == null) {
      if (other.subCategory != null) return false;
    } else if (!subCategory.equals(other.subCategory)) return false;
    if (totalRecords != other.totalRecords) return false;
    if (updatedDate == null) {
      if (other.updatedDate != null) return false;
    } else if (!updatedDate.equals(other.updatedDate)) return false;
    if (fileStoreObject == null) {
      if (other.fileStoreObject != null) return false;
    } else if (!fileStoreObject.equals(other.fileStoreObject)) return false;
    return true;
  }
}

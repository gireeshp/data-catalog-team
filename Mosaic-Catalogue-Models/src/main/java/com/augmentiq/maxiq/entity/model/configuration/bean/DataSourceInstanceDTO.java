package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Date;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.FileStoreObject;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.DATASOURCE_INSTANCE)
public class DataSourceInstanceDTO {
	private long dataSourceId;
	private String dataSourceName;
	private String dataRepoName;
	private String dataSourceType;
	private Date updatedDate;
	private String category;
	private String subCategory;
	private long totalRecords;
	private Long avgRating;
	private String accessType;
	private String groupId;
	private String createdBy;
	private String fileType;

	// mode of data transfer before processing data
	// value source indicates data won't be stored anywhere instead will be picked
	// from source only to process
	private FileStoreObject fileStoreObject;

	public FileStoreObject getFileStoreObject() {
		return fileStoreObject;
	}

	public void setFileStoreObject(FileStoreObject fileStoreObject) {
		this.fileStoreObject = fileStoreObject;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public long getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(long dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public String getDataRepoName() {
		return dataRepoName;
	}

	public void setDataRepoName(String dataRepoName) {
		this.dataRepoName = dataRepoName;
	}

	public String getDataSourceType() {
		return dataSourceType;
	}

	public void setDataSourceType(String dataSourceType) {
		this.dataSourceType = dataSourceType;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public Long getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Long avgRating) {
		this.avgRating = avgRating;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "DataSourceInstanceDTO [dataSourceId=" + dataSourceId + ", dataSourceName=" + dataSourceName
				+ ", dataRepoName=" + dataRepoName + ", dataSourceType=" + dataSourceType + ", updatedDate="
				+ updatedDate + ", category=" + category + ", subCategory=" + subCategory + ", totalRecords="
				+ totalRecords + ", avgRating=" + avgRating + ", accessType=" + accessType + ", groupId=" + groupId
				+ ", createdBy=" + createdBy + ", fileType=" + fileType + ", fileStoreObject=" + fileStoreObject + "]";
	}
}

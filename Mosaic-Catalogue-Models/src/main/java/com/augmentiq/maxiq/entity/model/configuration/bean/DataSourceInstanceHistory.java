package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.DATASOURCE_INSTANCE_HISTORY)
public class DataSourceInstanceHistory implements Serializable {
  @Id private long id;
  private long dataSourceId;
  private String dataSourceName;
  private String createdBy;
  private Long createdDate;
  private String filePath;
  private long totalRecords;
  private long size;
  private String fileType;
  private String compressionType;
  private String dataSourceStatus;
  private String dataSourceType;
  private Long updatedDate;
  private Long groupId;
  private Long jobInstId;
  private String updatedBy;
  private Long fieldMappingVersionId;
  private Long dataSourceVersionId;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public String getDataSourceName() {
    return dataSourceName;
  }

  public void setDataSourceName(String dataSourceName) {
    this.dataSourceName = dataSourceName;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public long getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(long totalRecords) {
    this.totalRecords = totalRecords;
  }

  public long getSize() {
    return size;
  }

  public void setSize(long size) {
    this.size = size;
  }

  public String getFileType() {
    return fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public String getCompressionType() {
    return compressionType;
  }

  public void setCompressionType(String compressionType) {
    this.compressionType = compressionType;
  }

  public String getDataSourceStatus() {
    return dataSourceStatus;
  }

  public void setDataSourceStatus(String dataSourceStatus) {
    this.dataSourceStatus = dataSourceStatus;
  }

  public String getDataSourceType() {
    return dataSourceType;
  }

  public void setDataSourceType(String dataSourceType) {
    this.dataSourceType = dataSourceType;
  }

  public Long getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getJobInstId() {
    return jobInstId;
  }

  public void setJobInstId(Long jobInstId) {
    this.jobInstId = jobInstId;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Long getFieldMappingVersionId() {
    return fieldMappingVersionId;
  }

  public void setFieldMappingVersionId(Long fieldMappingVersionId) {
    this.fieldMappingVersionId = fieldMappingVersionId;
  }

  public Long getDataSourceVersionId() {
    return dataSourceVersionId;
  }

  public void setDataSourceVersionId(Long dataSourceVersionId) {
    this.dataSourceVersionId = dataSourceVersionId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((compressionType == null) ? 0 : compressionType.hashCode());
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
    result = prime * result + (int) (dataSourceId ^ (dataSourceId >>> 32));
    result = prime * result + ((dataSourceName == null) ? 0 : dataSourceName.hashCode());
    result = prime * result + ((dataSourceStatus == null) ? 0 : dataSourceStatus.hashCode());
    result = prime * result + ((dataSourceType == null) ? 0 : dataSourceType.hashCode());
    result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
    result = prime * result + ((fileType == null) ? 0 : fileType.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + (int) (id ^ (id >>> 32));
    result = prime * result + ((jobInstId == null) ? 0 : jobInstId.hashCode());
    result = prime * result + (int) (size ^ (size >>> 32));
    result = prime * result + (int) (totalRecords ^ (totalRecords >>> 32));
    result = prime * result + ((updatedBy == null) ? 0 : updatedBy.hashCode());
    result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
    result =
        prime * result + ((fieldMappingVersionId == null) ? 0 : fieldMappingVersionId.hashCode());
    result = prime * result + ((dataSourceVersionId == null) ? 0 : dataSourceVersionId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSourceInstanceHistory other = (DataSourceInstanceHistory) obj;
    if (compressionType == null) {
      if (other.compressionType != null) return false;
    } else if (!compressionType.equals(other.compressionType)) return false;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdDate == null) {
      if (other.createdDate != null) return false;
    } else if (!createdDate.equals(other.createdDate)) return false;
    if (dataSourceId != other.dataSourceId) return false;
    if (dataSourceName == null) {
      if (other.dataSourceName != null) return false;
    } else if (!dataSourceName.equals(other.dataSourceName)) return false;
    if (dataSourceStatus == null) {
      if (other.dataSourceStatus != null) return false;
    } else if (!dataSourceStatus.equals(other.dataSourceStatus)) return false;
    if (dataSourceType == null) {
      if (other.dataSourceType != null) return false;
    } else if (!dataSourceType.equals(other.dataSourceType)) return false;
    if (filePath == null) {
      if (other.filePath != null) return false;
    } else if (!filePath.equals(other.filePath)) return false;
    if (fileType == null) {
      if (other.fileType != null) return false;
    } else if (!fileType.equals(other.fileType)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (id != other.id) return false;
    if (jobInstId == null) {
      if (other.jobInstId != null) return false;
    } else if (!jobInstId.equals(other.jobInstId)) return false;
    if (size != other.size) return false;
    if (totalRecords != other.totalRecords) return false;
    if (updatedBy == null) {
      if (other.updatedBy != null) return false;
    } else if (!updatedBy.equals(other.updatedBy)) return false;
    if (updatedDate == null) {
      if (other.updatedDate != null) return false;
    } else if (!updatedDate.equals(other.updatedDate)) return false;
    if (fieldMappingVersionId == null) {
      if (other.fieldMappingVersionId != null) return false;
    } else if (!fieldMappingVersionId.equals(other.fieldMappingVersionId)) return false;
    if (dataSourceVersionId == null) {
      if (other.dataSourceVersionId != null) return false;
    } else if (!dataSourceVersionId.equals(other.dataSourceVersionId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "DataSourceInstanceHistory [id="
        + id
        + ", dataSourceId="
        + dataSourceId
        + ", dataSourceName="
        + dataSourceName
        + ", createdBy="
        + createdBy
        + ", createdDate="
        + createdDate
        + ", filePath="
        + filePath
        + ", totalRecords="
        + totalRecords
        + ", size="
        + size
        + ", fileType="
        + fileType
        + ", compressionType="
        + compressionType
        + ", dataSourceStatus="
        + dataSourceStatus
        + ", dataSourceType="
        + dataSourceType
        + ", updatedDate="
        + updatedDate
        + ", groupId="
        + groupId
        + ", jobInstId="
        + jobInstId
        + ", updatedBy="
        + updatedBy
        + ", fieldMappingVersionId="
        + fieldMappingVersionId
        + ", dataSourceVersionId="
        + dataSourceVersionId
        + "]";
  }
}

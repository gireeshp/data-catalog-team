package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Date;

public class DataSourceInstanceHistoryDTO implements Serializable {
  private String snapshotId;

  public String getSnapshotId() {
    return snapshotId;
  }

  public void setSnapshotId(String snapshotId) {
    this.snapshotId = snapshotId;
  }

  private long id;

  private String dataSourceName;
  private Date createdDate;

  /*
   * This is last refreshed date - even though name suggest last updated date.
   * Need to audit old data sources if we rename this.
   */
  private Date updatedDate;
  private String fileType;
  private Long jobInstId;
  private String compressionType;
  private String dataSourceStatus;
  private long totalRecords;
  private long size;
  private long dataSourceId;
  private String createdBy;
  private String filePath;
  private String dataSourceType;
  private Long groupId;
  private Long fieldMappingVersionId;
  private Long dataSourceVersionId;

  //This is true if datasource is created from app as input parameter as file type
  public String getFileType() {
    return fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public Long getjobInstId() {
    return jobInstId;
  }

  public void setjobInstId(Long jobInstId) {
    this.jobInstId = jobInstId;
  }

  public String getCompressionType() {
    return compressionType;
  }

  public void setCompressionType(String compressionType) {
    this.compressionType = compressionType;
  }

  public String getDataSourceType() {
    return dataSourceType;
  }

  public void setDataSourceType(String dataSourceType) {
    this.dataSourceType = dataSourceType;
  }

  /*	public Long getId() {
  	return id;
  }

  public void setId(Long id) {
  	this.id = id;
  }*/

  public String getDataSourceName() {
    return dataSourceName;
  }

  public void setDataSourceName(String dataSourceName) {
    this.dataSourceName = dataSourceName;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

  public String getDataSourceStatus() {
    return dataSourceStatus;
  }

  public void setDataSourceStatus(String dataSourceStatus) {
    this.dataSourceStatus = dataSourceStatus;
  }

  public long getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(long totalRecords) {
    this.totalRecords = totalRecords;
  }

  public long getSize() {
    return size;
  }

  public void setSize(long size) {
    this.size = size;
  }

  public long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public void setFilePath() {
    this.filePath = filePath;
  }

  public String getFilePath() {
    return filePath;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public Long getFieldMappingVersionId() {
    return fieldMappingVersionId;
  }

  public void setFieldMappingVersionId(Long fieldMappingVersionId) {
    this.fieldMappingVersionId = fieldMappingVersionId;
  }

  public Long getDataSourceVersionId() {
    return dataSourceVersionId;
  }

  public void setDataSourceVersionId(Long dataSourceVersionId) {
    this.dataSourceVersionId = dataSourceVersionId;
  }
}

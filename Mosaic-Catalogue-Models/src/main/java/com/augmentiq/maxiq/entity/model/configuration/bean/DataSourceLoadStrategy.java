package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.constant.configuration.enums.LoadStategyType;

/** Created by shivanand on 8/4/2015. */

public class DataSourceLoadStrategy implements Serializable {
  private LoadStategyType loadStategyType;
  private UpdateLoadStategy updateLoadStategy;

  public LoadStategyType getLoadStategyType() {
    return loadStategyType == null
        ? LoadStategyType.REPLACEALL
        : loadStategyType == LoadStategyType.UPDATE ? LoadStategyType.APPENDALL : loadStategyType;
  }

  public void setLoadStategyType(LoadStategyType loadStategyType) {
    this.loadStategyType = loadStategyType;
  }

  public UpdateLoadStategy getUpdateLoadStategy() {
    return updateLoadStategy;
  }

  public void setUpdateLoadStategy(UpdateLoadStategy updateLoadStategy) {
    this.updateLoadStategy = updateLoadStategy;
  }

  @Override
  public String toString() {
    return "DataSourceLoadStrategy [loadStategyType="
        + loadStategyType
        + ", updateLoadStategy="
        + updateLoadStategy
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataSourceSnapshotDTO implements Serializable {

  private Long snapshotId;
  private Date dateTime;

  private List<DataSourceInstanceHistoryDTO> jobInstances = new ArrayList<>();

  public DataSourceSnapshotDTO(
      Long snapshotId, Date dateTime, List<DataSourceInstanceHistoryDTO> jobInstances) {
    super();
    this.snapshotId = snapshotId;
    this.dateTime = dateTime;
    this.jobInstances = jobInstances;
  }

  public List<DataSourceInstanceHistoryDTO> getJobInstances() {
    return jobInstances;
  }

  public void setJobInstances(List<DataSourceInstanceHistoryDTO> jobInstances) {
    this.jobInstances = jobInstances;
  }

  public Long getSnapshotId() {
    return snapshotId;
  }

  public void setSnapshotId(Long snapshotId) {
    this.snapshotId = snapshotId;
  }

  public Date getDateTime() {
    return dateTime;
  }

  public void setDateTime(Date dateTime) {
    this.dateTime = dateTime;
  }

  @Override
  public String toString() {
    return "DataSourceSnapshotDTO [snapshotId="
        + snapshotId
        + ", dateTime="
        + dateTime
        + ", jobInstances="
        + jobInstances
        + "]";
  }
}

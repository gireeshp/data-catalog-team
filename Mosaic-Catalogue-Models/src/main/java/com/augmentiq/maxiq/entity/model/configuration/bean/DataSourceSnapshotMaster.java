package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Date;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.DATASOURCE_SNAPSHOT_MASTER)
public class DataSourceSnapshotMaster implements Serializable {
  @Id private long snapShotId;
  private long dataSourceId;
  private Date snapshotDate;
  private String snapShotStatus;

  /*	public DataSourceSnapshotMaster(long snapShotId, long dataSourceId, Date snapshotDate, String snapShotStatus) {
  	super();
  	this.snapShotId = snapShotId;
  	this.dataSourceId = dataSourceId;
  	this.snapshotDate = snapshotDate;
  	this.snapShotStatus = snapShotStatus;
  }*/

  public Date getSnapshotDate() {
    return snapshotDate;
  }

  public void setSnapshotDate(Date snapshotDate) {
    this.snapshotDate = snapshotDate;
  }

  public long getSnapShotId() {
    return snapShotId;
  }

  public void setSnapShotId(long snapShotId) {
    this.snapShotId = snapShotId;
  }

  public long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public String getSnapShotStatus() {
    return snapShotStatus;
  }

  public void setSnapShotStatus(String snapShotStatus) {
    this.snapShotStatus = snapShotStatus;
  }

  @Override
  public String toString() {
    return "DataSourceSnapshotMaster [snapShotId="
        + snapShotId
        + ", dataSourceId="
        + dataSourceId
        + ", snapshotDate="
        + snapshotDate
        + ", snapShotStatus="
        + snapShotStatus
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (dataSourceId ^ (dataSourceId >>> 32));
    result = prime * result + (int) (snapShotId ^ (snapShotId >>> 32));
    result = prime * result + ((snapShotStatus == null) ? 0 : snapShotStatus.hashCode());
    result = prime * result + ((snapshotDate == null) ? 0 : snapshotDate.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSourceSnapshotMaster other = (DataSourceSnapshotMaster) obj;
    if (dataSourceId != other.dataSourceId) return false;
    if (snapShotId != other.snapShotId) return false;
    if (snapShotStatus == null) {
      if (other.snapShotStatus != null) return false;
    } else if (!snapShotStatus.equals(other.snapShotStatus)) return false;
    if (snapshotDate == null) {
      if (other.snapshotDate != null) return false;
    } else if (!snapshotDate.equals(other.snapshotDate)) return false;
    return true;
  }
}

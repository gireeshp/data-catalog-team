package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.CONFIGURATIONVERSION)
public class DataSourceVersion {
  @Id private Long id;
  private Long dataSourceVersionId;
  private Long fieldMappingVersionId;
  private Long dataSourceId;
  private DataSource dataSource;
  private List<FieldMapping> fieldMappings;
  private String insertedBy;
  private String insertedOn;

  // data at rest options
  private String dataAtRestFileType;
  private String dataAtRestCompressionType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(Long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public Long getDataSourceVersionId() {
    return dataSourceVersionId;
  }

  public void setDataSourceVersionId(Long dataSourceVersionId) {
    this.dataSourceVersionId = dataSourceVersionId;
  }

  public Long getFieldMappingVersionId() {
    return fieldMappingVersionId;
  }

  public void setFieldMappingVersionId(Long fieldMappingVersionId) {
    this.fieldMappingVersionId = fieldMappingVersionId;
  }

  public DataSource getDataSource() {
    return dataSource;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public List<FieldMapping> getFieldMappings() {
    return fieldMappings;
  }

  public void setFieldMappings(List<FieldMapping> fieldMappings) {
    this.fieldMappings = fieldMappings;
  }

  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getInsertedOn() {
    return insertedOn;
  }

  public void setInsertedOn(String insertedOn) {
    this.insertedOn = insertedOn;
  }

  public String getDataAtRestFileType() {
    return dataAtRestFileType;
  }

  public void setDataAtRestFileType(String dataAtRestFileType) {
    this.dataAtRestFileType = dataAtRestFileType;
  }

  public String getDataAtRestCompressionType() {
    return dataAtRestCompressionType;
  }

  public void setDataAtRestCompressionType(String dataAtRestCompressionType) {
    this.dataAtRestCompressionType = dataAtRestCompressionType;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result =
        prime * result
            + ((dataAtRestCompressionType == null) ? 0 : dataAtRestCompressionType.hashCode());
    result = prime * result + ((dataAtRestFileType == null) ? 0 : dataAtRestFileType.hashCode());
    result = prime * result + ((dataSource == null) ? 0 : dataSource.hashCode());
    result = prime * result + ((dataSourceId == null) ? 0 : dataSourceId.hashCode());
    result = prime * result + ((dataSourceVersionId == null) ? 0 : dataSourceVersionId.hashCode());
    result =
        prime * result + ((fieldMappingVersionId == null) ? 0 : fieldMappingVersionId.hashCode());
    result = prime * result + ((fieldMappings == null) ? 0 : fieldMappings.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((insertedBy == null) ? 0 : insertedBy.hashCode());
    result = prime * result + ((insertedOn == null) ? 0 : insertedOn.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSourceVersion other = (DataSourceVersion) obj;
    if (dataAtRestCompressionType == null) {
      if (other.dataAtRestCompressionType != null) return false;
    } else if (!dataAtRestCompressionType.equals(other.dataAtRestCompressionType)) return false;
    if (dataAtRestFileType == null) {
      if (other.dataAtRestFileType != null) return false;
    } else if (!dataAtRestFileType.equals(other.dataAtRestFileType)) return false;
    if (dataSource == null) {
      if (other.dataSource != null) return false;
    } else if (!dataSource.equals(other.dataSource)) return false;
    if (dataSourceId == null) {
      if (other.dataSourceId != null) return false;
    } else if (!dataSourceId.equals(other.dataSourceId)) return false;
    if (dataSourceVersionId == null) {
      if (other.dataSourceVersionId != null) return false;
    } else if (!dataSourceVersionId.equals(other.dataSourceVersionId)) return false;
    if (fieldMappingVersionId == null) {
      if (other.fieldMappingVersionId != null) return false;
    } else if (!fieldMappingVersionId.equals(other.fieldMappingVersionId)) return false;
    if (fieldMappings == null) {
      if (other.fieldMappings != null) return false;
    } else if (!fieldMappings.equals(other.fieldMappings)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (insertedBy == null) {
      if (other.insertedBy != null) return false;
    } else if (!insertedBy.equals(other.insertedBy)) return false;
    if (insertedOn == null) {
      if (other.insertedOn != null) return false;
    } else if (!insertedOn.equals(other.insertedOn)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "DataSourceVersion [id="
        + id
        + ", dataSourceVersionId="
        + dataSourceVersionId
        + ", fieldMappingVersionId="
        + fieldMappingVersionId
        + ", dataSourceId="
        + dataSourceId
        + ", dataSource="
        + dataSource
        + ", fieldMappings="
        + fieldMappings
        + ", insertedBy="
        + insertedBy
        + ", insertedOn="
        + insertedOn
        + ", dataAtRestFileType="
        + dataAtRestFileType
        + ", dataAtRestCompressionType="
        + dataAtRestCompressionType
        + "]";
  }
}

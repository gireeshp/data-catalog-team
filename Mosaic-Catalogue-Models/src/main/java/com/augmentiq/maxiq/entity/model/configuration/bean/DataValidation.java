package com.augmentiq.maxiq.entity.model.configuration.bean;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.augmentiq.maxiq.constant.configuration.enums.DataValidatorType;

/**
 * *
 *
 * @author shiva This contains all the validations to be applied on field
 */
public class DataValidation implements Serializable {

  private DataValidatorType validator;

  private Integer lengthRangeFrom = -1;
  private Integer lengthRangeTo = -1;
  private String operator;
  private String dateFormat;
  private Set<String> multiValues;
  private List<String> inputList;
  private String rejectData;
  private String rejectFieldValue;
  private Map<Long, String> advRejectFields = new HashMap<Long, String>();

  public Map<Long, String> getAdvRejectFields() {
    return advRejectFields;
  }

  public void setAdvRejectFields(Map<Long, String> advRejectFields) {
    this.advRejectFields = advRejectFields;
  }

  public DataValidatorType getValidator() {
    return validator;
  }

  public void setValidator(DataValidatorType validator) {
    this.validator = validator;
  }

  public Integer getLengthRangeFrom() {
    return lengthRangeFrom;
  }

  public void setLengthRangeFrom(Integer lengthRangeFrom) {
    this.lengthRangeFrom = lengthRangeFrom;
  }

  public Integer getLengthRangeTo() {
    return lengthRangeTo;
  }

  public void setLengthRangeTo(Integer lengthRangeTo) {
    this.lengthRangeTo = lengthRangeTo;
  }

  public String getOperator() {
    return operator;
  }

  public Set<String> getMultiValues() {
    return multiValues;
  }

  public void setMultiValues(Set<String> multiValues) {
    this.multiValues = multiValues;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public String getDateFormat() {
    return dateFormat;
  }

  public void setDateFormat(String dateFormat) {
    this.dateFormat = dateFormat;
  }

  public List<String> getInputList() {
    return inputList;
  }

  public void setInputList(List<String> inputList) {
    this.inputList = inputList;
  }

  public String getRejectData() {
    return rejectData;
  }

  public void setRejectData(String rejectData) {
    this.rejectData = rejectData;
  }

  public String getRejectFieldValue() {
    return rejectFieldValue;
  }

  public void setRejectFieldValue(String rejectFieldValue) {
    this.rejectFieldValue = rejectFieldValue;
  }

  @Override
  public String toString() {
    return "DataValidation [validator="
        + validator
        + ", lengthRangeFrom="
        + lengthRangeFrom
        + ", lengthRangeTo="
        + lengthRangeTo
        + ", operator="
        + operator
        + ", dateFormat="
        + dateFormat
        + ", multiValues="
        + multiValues
        + ", inputList="
        + inputList
        + ", rejectData="
        + rejectData
        + ", rejectFieldValue="
        + rejectFieldValue
        + ", advRejectFields="
        + advRejectFields
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((advRejectFields == null) ? 0 : advRejectFields.hashCode());
    result = prime * result + ((dateFormat == null) ? 0 : dateFormat.hashCode());
    result = prime * result + ((inputList == null) ? 0 : inputList.hashCode());
    result = prime * result + ((lengthRangeFrom == null) ? 0 : lengthRangeFrom.hashCode());
    result = prime * result + ((lengthRangeTo == null) ? 0 : lengthRangeTo.hashCode());
    result = prime * result + ((multiValues == null) ? 0 : multiValues.hashCode());
    result = prime * result + ((operator == null) ? 0 : operator.hashCode());
    result = prime * result + ((rejectData == null) ? 0 : rejectData.hashCode());
    result = prime * result + ((rejectFieldValue == null) ? 0 : rejectFieldValue.hashCode());
    result = prime * result + ((validator == null) ? 0 : validator.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataValidation other = (DataValidation) obj;
    if (advRejectFields == null) {
      if (other.advRejectFields != null) return false;
    } else if (!advRejectFields.equals(other.advRejectFields)) return false;
    if (dateFormat == null) {
      if (other.dateFormat != null) return false;
    } else if (!dateFormat.equals(other.dateFormat)) return false;
    if (inputList == null) {
      if (other.inputList != null) return false;
    } else if (!inputList.equals(other.inputList)) return false;
    if (lengthRangeFrom == null) {
      if (other.lengthRangeFrom != null) return false;
    } else if (!lengthRangeFrom.equals(other.lengthRangeFrom)) return false;
    if (lengthRangeTo == null) {
      if (other.lengthRangeTo != null) return false;
    } else if (!lengthRangeTo.equals(other.lengthRangeTo)) return false;
    if (multiValues == null) {
      if (other.multiValues != null) return false;
    } else if (!multiValues.equals(other.multiValues)) return false;
    if (operator == null) {
      if (other.operator != null) return false;
    } else if (!operator.equals(other.operator)) return false;
    if (rejectData == null) {
      if (other.rejectData != null) return false;
    } else if (!rejectData.equals(other.rejectData)) return false;
    if (rejectFieldValue == null) {
      if (other.rejectFieldValue != null) return false;
    } else if (!rejectFieldValue.equals(other.rejectFieldValue)) return false;
    if (validator != other.validator) return false;
    return true;
  }
}

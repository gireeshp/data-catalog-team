package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 * CREATE TABLE `datasource_approval_mapper` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `objectId` int(10) NOT NULL,
  `level` int(10) NOT NULL,
  `parent_level` int(10)  Null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

 */

@TableName(tableName = Sequences.DATASOURCE_APPROVAL_MAPPER)
public class DatasourceApprovalMapper {
  @Id private Long id;
  private Long userId;
  private Long objectId;
  private Long level;
  private Long parent_level;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getObjectId() {
    return objectId;
  }

  public void setObjectId(Long objectId) {
    this.objectId = objectId;
  }

  public Long getLevel() {
    return level;
  }

  public void setLevel(Long level) {
    this.level = level;
  }

  public Long getParent_level() {
    return parent_level;
  }

  public void setParent_level(Long parent_level) {
    this.parent_level = parent_level;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * CREATE TABLE `datasource_service_apimanager` ( `id` int(10) NOT NULL
 * AUTO_INCREMENT, `dataSourceId` int(10) DEFAULT NULL, `dataSourceName`
 * varchar(40) DEFAULT NULL, `userId` int(10) DEFAULT NULL, `apiKey` varchar(50)
 * DEFAULT NULL, `status` int(10) DEFAULT NULL, `deleted` int(10) DEFAULT NULL,
 * `createdDate` varchar(40) DEFAULT NULL, `modifiedDate` varchar(40) DEFAULT
 * NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT
 * CHARSET=utf8
 */
@TableName(tableName = Sequences.DATASOURCE_SERVICE_APIMANAGER)
public class DatasourceServiceApimanager {

	@Id
	private Long id;
	private Long dataSourceId;
	private String dataSourceName;
	private Long userId;
	private String apiKey;
	private Integer status;
	private Integer deleted;
	private Long createdDate;
	private Long modifiedDate;
	private String query;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Long dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getDeleted() {
		return deleted;
	}

	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "DatasourceServiceApimanager [id=" + id + ", dataSourceId=" + dataSourceId + ", dataSourceName="
				+ dataSourceName + ", userId=" + userId + ", apiKey=" + apiKey + ", status=" + status + ", deleted="
				+ deleted + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", query=" + query
				+ "]";
	}

}

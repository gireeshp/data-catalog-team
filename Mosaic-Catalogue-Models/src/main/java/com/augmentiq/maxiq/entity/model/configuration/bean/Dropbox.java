package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
public class Dropbox {

  private String appName;
  private String accessToken;
  private String storagePath;
  private String filePath;
  private List<DropboxInfo> hierarchyInfo;
  private DropboxInfo folders;
  private DropboxInfo selectedFolder;
  private Boolean optionallyEnclosedInDoubleQuotes = false;
  private String fileExtension;
  private String downloadObjectType;
  private String connectionName;

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getStoragePath() {
    return storagePath;
  }

  public void setStoragePath(String storagePath) {
    this.storagePath = storagePath;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public List<DropboxInfo> getHierarchyInfo() {
    return hierarchyInfo;
  }

  public void setHierarchyInfo(List<DropboxInfo> hierarchyInfo) {
    this.hierarchyInfo = hierarchyInfo;
  }

  public DropboxInfo getFolders() {
    return folders;
  }

  public void setFolders(DropboxInfo folders) {
    this.folders = folders;
  }

  public DropboxInfo getSelectedFolder() {
    return selectedFolder;
  }

  public void setSelectedFolder(DropboxInfo selectedFolder) {
    this.selectedFolder = selectedFolder;
  }

  public Boolean getOptionallyEnclosedInDoubleQuotes() {
    return optionallyEnclosedInDoubleQuotes;
  }

  public void setOptionallyEnclosedInDoubleQuotes(Boolean optionallyEnclosedInDoubleQuotes) {
    this.optionallyEnclosedInDoubleQuotes = optionallyEnclosedInDoubleQuotes;
  }

  public String getFileExtension() {
    return fileExtension;
  }

  public void setFileExtension(String fileExtension) {
    this.fileExtension = fileExtension;
  }

  public String getDownloadObjectType() {
    return downloadObjectType;
  }

  public void setDownloadObjectType(String downloadObjectType) {
    this.downloadObjectType = downloadObjectType;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  @Override
  public String toString() {
    // TODO Auto-generated method stub
    return "Dropbox [ appName= "
        + appName
        + ", accessToken"
        + accessToken
        + ", storagePath "
        + storagePath
        + ", "
        + storagePath
        + ", dropboxInfo="
        + hierarchyInfo
        + ", optionallyEnclosedInDoubleQuotes="
        + optionallyEnclosedInDoubleQuotes
        + ", fileExtension ="
        + fileExtension
        + ", downloadObjectType ="
        + downloadObjectType
        + " ] ";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;
public class DropboxInfo {

  private String name;
  private String path;
  private String iconName;
  private String type;
  private String fileLastModified;

  public DropboxInfo(String name) {
    super();
    this.name = name;
  }

  public DropboxInfo() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getIconName() {
    return iconName;
  }

  public void setIconName(String iconName) {
    this.iconName = iconName;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getFileLastModified() {
    return fileLastModified;
  }

  public void setFileLastModified(String fileLastModified) {
    this.fileLastModified = fileLastModified;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

public class ExcelRange implements Serializable {

  private String xlsFilePathFrom;
  private String xlsFilePathTo;
  private Boolean xlsHeaderOptions = false;
  private String xlsSheetName;

  public String getXlsFilePathFrom() {
    return xlsFilePathFrom;
  }

  public void setXlsFilePathFrom(String xlsFilePathFrom) {
    this.xlsFilePathFrom = xlsFilePathFrom;
  }

  public String getXlsFilePathTo() {
    return xlsFilePathTo;
  }

  public void setXlsFilePathTo(String xlsFilePathTo) {
    this.xlsFilePathTo = xlsFilePathTo;
  }

  public Boolean getXlsHeaderOptions() {
    return xlsHeaderOptions;
  }

  public void setXlsHeaderOptions(Boolean xlsHeaderOptions) {
    this.xlsHeaderOptions = xlsHeaderOptions;
  }

  public String getXlsSheetName() {
    return xlsSheetName;
  }

  public void setXlsSheetName(String xlsSheetName) {
    this.xlsSheetName = xlsSheetName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((xlsFilePathFrom == null) ? 0 : xlsFilePathFrom.hashCode());
    result = prime * result + ((xlsFilePathTo == null) ? 0 : xlsFilePathTo.hashCode());
    result = prime * result + ((xlsHeaderOptions == null) ? 0 : xlsHeaderOptions.hashCode());
    result = prime * result + ((xlsSheetName == null) ? 0 : xlsSheetName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    ExcelRange other = (ExcelRange) obj;
    if (xlsFilePathFrom == null) {
      if (other.xlsFilePathFrom != null) return false;
    } else if (!xlsFilePathFrom.equals(other.xlsFilePathFrom)) return false;
    if (xlsFilePathTo == null) {
      if (other.xlsFilePathTo != null) return false;
    } else if (!xlsFilePathTo.equals(other.xlsFilePathTo)) return false;
    if (xlsHeaderOptions == null) {
      if (other.xlsHeaderOptions != null) return false;
    } else if (!xlsHeaderOptions.equals(other.xlsHeaderOptions)) return false;
    if (xlsSheetName == null) {
      if (other.xlsSheetName != null) return false;
    } else if (!xlsSheetName.equals(other.xlsSheetName)) return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ExcelRange [xlsFilePathFrom=");
    builder.append(xlsFilePathFrom);
    builder.append(", xlsFilePathTo=");
    builder.append(xlsFilePathTo);
    builder.append(", xlsHeaderOptions=");
    builder.append(xlsHeaderOptions);
    builder.append(", xlsSheetName=");
    builder.append(xlsSheetName);
    builder.append("]");
    return builder.toString();
  }
}

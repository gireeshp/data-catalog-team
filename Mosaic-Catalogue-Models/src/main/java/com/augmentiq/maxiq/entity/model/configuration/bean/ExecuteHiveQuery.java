package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.EXECUTE_QUERIES)
public class ExecuteHiveQuery {

  @Id private String id;
  private Object input;
  private Object output;
  private Object error;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Object getInput() {
    return input;
  }

  public void setInput(Object input) {
    this.input = input;
  }

  public Object getOutput() {
    return output;
  }

  public void setOutput(Object output) {
    this.output = output;
  }

  public Object getError() {
    return error;
  }

  public void setError(Object error) {
    this.error = error;
  }

  @Override
  public String toString() {
    return "ExecuteHiveQuery [id="
        + id
        + ", input="
        + input
        + ", output="
        + output
        + ", error="
        + error
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * @ Yogesh L. on 01-June-2017 alter table externaldatadetails add column salesforce blob null after
 * storagepath; alter table externaldatadetails add column connectortype varchar(500) null after
 * salesforce; @ Yogesh L. on 21-June-2017 alter table externaldatadetails add column dropbox blob
 * null after salesforce;
 *
 * <p>* @ Yogesh L. on 27-June-2017 alter table externaldatadetails add column facebookPage blob
 * null after dropbox; alter table externaldatadetails add column hackyl blob null after
 * facebookPage; @ Yogesh L. on 25-Aug-2017 alter table externaldatadetails add column Probe42 blob
 * null after hackyl; alter table externaldatadetails add column SapErp blob null after Probe42;
 */

@TableName(tableName = Sequences.EXTERNALDATADETAILS)
public class ExternalDataSourceDetails {
  @Id private Long id;
  private Long dataSourceId;
  private Boolean containsHeader;
  private AmazonS3 amazonS3;
  private GoogleAnalytic googleAnalytics;
  private String delimeter;
  private String storagePath;
  private Salesforce salesforce;
  private String connectorType;
  private Dropbox dropbox;
  private FacebookPage facebookPage;
  private Hackyl hackyl;
  private Probe42 probe42;
  private SapErp sapErp;

  public Boolean getContainsHeader() {
    return containsHeader;
  }

  public void setContainsHeader(Boolean containsHeader) {
    this.containsHeader = containsHeader;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(Long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public AmazonS3 getAmazonS3() {
    return amazonS3;
  }

  public void setAmazonS3(AmazonS3 amazonS3) {
    this.amazonS3 = amazonS3;
  }

  public GoogleAnalytic getGoogleAnalytics() {
    return googleAnalytics;
  }

  public void setGoogleAnalytics(GoogleAnalytic googleAnalytics) {
    this.googleAnalytics = googleAnalytics;
  }

  public String getDelimeter() {
    return delimeter;
  }

  public void setDelimeter(String delimeter) {
    this.delimeter = delimeter;
  }

  public String getStoragePath() {
    return storagePath;
  }

  public void setStoragePath(String storagePath) {
    this.storagePath = storagePath;
  }

  public Salesforce getSalesforce() {
    return salesforce;
  }

  public void setSalesforce(Salesforce salesforce) {
    this.salesforce = salesforce;
  }

  public String getConnectorType() {
    return connectorType;
  }

  public void setConnectorType(String connectorType) {
    this.connectorType = connectorType;
  }

  public Dropbox getDropbox() {
    return dropbox;
  }

  public void setDropbox(Dropbox dropbox) {
    this.dropbox = dropbox;
  }

  public FacebookPage getFacebookPage() {
    return facebookPage;
  }

  public void setFacebookPage(FacebookPage facebookPage) {
    this.facebookPage = facebookPage;
  }

  public Hackyl getHackyl() {
    return hackyl;
  }

  public void setHackyl(Hackyl hackyl) {
    this.hackyl = hackyl;
  }

  public Probe42 getProbe42() {
    return probe42;
  }

  public void setProbe42(Probe42 probe42) {
    this.probe42 = probe42;
  }

  public SapErp getSapErp() {
    return sapErp;
  }

  public void setSapErp(SapErp sapErp) {
    this.sapErp = sapErp;
  }

  @Override
  public String toString() {
    return "ExternalDataSourceDetails [id="
        + id
        + ", dataSourceId="
        + dataSourceId
        + ", containsHeader="
        + containsHeader
        + ", amazonS3="
        + amazonS3
        + ", googleAnalytics="
        + googleAnalytics
        + ", delimeter="
        + delimeter
        + ", storagePath="
        + storagePath
        + ", salesforce="
        + salesforce
        + ", connectorType="
        + connectorType
        + ", dropbox="
        + dropbox
        + ", facebookPage="
        + facebookPage
        + ", hackyl="
        + hackyl
        + ", probe42="
        + probe42
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;
public class FacebookPage {

  private String connectionName;
  private String facebookAccessToken;
  private String facebookSecretId;
  private String storagePath;
  private Boolean optionallyEnclosedInDoubleQuotes = false;
  private String facebookPageName;
  private String facebookDataType;

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  public String getStoragePath() {
    return storagePath;
  }

  public void setStoragePath(String storagePath) {
    this.storagePath = storagePath;
  }

  public Boolean getOptionallyEnclosedInDoubleQuotes() {
    return optionallyEnclosedInDoubleQuotes;
  }

  public void setOptionallyEnclosedInDoubleQuotes(Boolean optionallyEnclosedInDoubleQuotes) {
    this.optionallyEnclosedInDoubleQuotes = optionallyEnclosedInDoubleQuotes;
  }

  public String getFacebookPageName() {
    return facebookPageName;
  }

  public void setFacebookPageName(String facebookPageName) {
    this.facebookPageName = facebookPageName;
  }

  public String getFacebookDataType() {
    return facebookDataType;
  }

  public void setFacebookDataType(String facebookDataType) {
    this.facebookDataType = facebookDataType;
  }

  public String getFacebookAccessToken() {
    return facebookAccessToken;
  }

  public void setFacebookAccessToken(String facebookAccessToken) {
    this.facebookAccessToken = facebookAccessToken;
  }

  public String getFacebookSecretId() {
    return facebookSecretId;
  }

  public void setFacebookSecretId(String facebookSecretId) {
    this.facebookSecretId = facebookSecretId;
  }
}

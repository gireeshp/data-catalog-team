package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class FeatureExtraction implements Serializable {
  private Long id;
  private Long customComponentId;
  private String featureExtractionType;
  private String description;
  private Map<String, Long> featureInputList;
  private List<String> featureOutputList;
  private Map<String, FieldMapping> outputMap;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCustomComponentId() {
    return customComponentId;
  }

  public void setCustomComponentId(Long customComponentId) {
    this.customComponentId = customComponentId;
  }

  public String getFeatureExtractionType() {
    return featureExtractionType;
  }

  public void setFeatureExtractionType(String featureExtractionType) {
    this.featureExtractionType = featureExtractionType;
  }

  public Map<String, Long> getFeatureInputList() {
    return featureInputList;
  }

  public void setFeatureInputList(Map<String, Long> featureInputList) {
    this.featureInputList = featureInputList;
  }

  public List<String> getFeatureOutputList() {
    return featureOutputList;
  }

  public void setFeatureOutputList(List<String> featureOutputList) {
    this.featureOutputList = featureOutputList;
  }

  public Map<String, FieldMapping> getOutputMap() {
    return outputMap;
  }

  public void setOutputMap(Map<String, FieldMapping> outputMap) {
    this.outputMap = outputMap;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "FeatureExtraction [id="
        + id
        + ", customComponentId="
        + customComponentId
        + ", featureExtractionType="
        + featureExtractionType
        + ", description="
        + description
        + ", featureInputList="
        + featureInputList
        + ", featureOutputList="
        + featureOutputList
        + ", outputMap="
        + outputMap
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((customComponentId == null) ? 0 : customComponentId.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result =
        prime * result + ((featureExtractionType == null) ? 0 : featureExtractionType.hashCode());
    result = prime * result + ((featureInputList == null) ? 0 : featureInputList.hashCode());
    result = prime * result + ((featureOutputList == null) ? 0 : featureOutputList.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((outputMap == null) ? 0 : outputMap.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    FeatureExtraction other = (FeatureExtraction) obj;
    if (customComponentId == null) {
      if (other.customComponentId != null) return false;
    } else if (!customComponentId.equals(other.customComponentId)) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (featureExtractionType == null) {
      if (other.featureExtractionType != null) return false;
    } else if (!featureExtractionType.equals(other.featureExtractionType)) return false;
    if (featureInputList == null) {
      if (other.featureInputList != null) return false;
    } else if (!featureInputList.equals(other.featureInputList)) return false;
    if (featureOutputList == null) {
      if (other.featureOutputList != null) return false;
    } else if (!featureOutputList.equals(other.featureOutputList)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (outputMap == null) {
      if (other.outputMap != null) return false;
    } else if (!outputMap.equals(other.outputMap)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;

public class FieldMapping implements Comparable<FieldMapping>, Serializable {

	/** */
	private static final long serialVersionUID = -6617105988389659673L;

	private Long id;
	private Long dataSourceId;
	private String fieldName;
	private String alias;
	private Long position;
	private Boolean isPrimaryKey;
	private FieldDataTypes fieldDataType;
	private List<String> tags;
	private String replaceNullWith;
	private List<String> sampleRecords = new ArrayList<>();
	private String format;
	private Boolean addVarIndicator = false;
	private Boolean preIngestion;
	private Boolean postIngestion;
	private Boolean preValueFrequency = false;
	private Boolean posValueFrequency = false;
	private Integer precision = 0;
	private Integer scale = 0;
	private Object embeddedFieldDataType;
	private ReportPublishData reportPublishData;
	private List<FieldMapping> listFieldMapping;
	private List<List<String>> arraySampleRecords = new ArrayList<List<String>>();
	
	public Integer getPrecision() {
		return precision;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public Integer getScale() {
		return scale;
	}


	@Override
	public String toString() {
		return "FieldMapping [id=" + id + ", dataSourceId=" + dataSourceId + ", fieldName=" + fieldName + ", alias="
				+ alias + ", position=" + position + ", isPrimaryKey=" + isPrimaryKey + ", fieldDataType="
				+ fieldDataType + ", tags=" + tags + ", replaceNullWith=" + replaceNullWith + ", sampleRecords="
				+ sampleRecords + ", format=" + format + ", addVarIndicator=" + addVarIndicator + ", preIngestion="
				+ preIngestion + ", postIngestion=" + postIngestion + ", preValueFrequency=" + preValueFrequency
				+ ", posValueFrequency=" + posValueFrequency + ", precision=" + precision + ", scale=" + scale
				+ ", embeddedFieldDataType=" + embeddedFieldDataType + ", reportPublishData=" + reportPublishData
				+ ", listFieldMapping=" + listFieldMapping + ", arraySampleRecords=" + arraySampleRecords + ", key="
				+ key + ", descriptions=" + descriptions + ", pIIId=" + pIIId + ", dataValidations=" + dataValidations
				+ ", dataFilters=" + dataFilters + ", transformation=" + transformation + ", derivedFieldInd="
				+ derivedFieldInd + ", feautureExtractionId=" + feautureExtractionId + "]";
	}

	public void setScale(Integer scale) {
		this.scale = scale;
	}

	private Boolean key = false;
	private String descriptions;
	private Boolean pIIId = false;

	public Boolean getpIIId() {
		return pIIId;
	}

	public void setpIIId(Boolean pIIId) {
		this.pIIId = pIIId;
	}

	public Boolean getKey() {
		return key;
	}

	public void setKey(Boolean key) {
		this.key = key;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	public Boolean getPreValueFrequency() {
		return preValueFrequency;
	}

	public void setPreValueFrequency(Boolean preValueFrequency) {
		this.preValueFrequency = preValueFrequency;
	}

	public Boolean getPosValueFrequency() {
		return posValueFrequency;
	}

	public void setPosValueFrequency(Boolean posValueFrequency) {
		this.posValueFrequency = posValueFrequency;
	}

	private List<DataValidation> dataValidations;
	private List<DataFilter> dataFilters;
	private List<Transformations> transformation;

	private Boolean derivedFieldInd = false;
	private Long feautureExtractionId;

	public Boolean getPreIngestion() {
		return preIngestion == null ? false : preIngestion;
	}

	public void setPreIngestion(Boolean preIngestion) {
		this.preIngestion = preIngestion;
	}

	public Boolean getPostIngestion() {
		return postIngestion == null ? false : postIngestion;
	}

	public void setPostIngestion(Boolean postIngestion) {
		this.postIngestion = postIngestion;
	}

	public Boolean getAddVarIndicator() {
		return addVarIndicator;
	}

	public void setAddVarIndicator(Boolean addVarIndicator) {
		this.addVarIndicator = addVarIndicator;
	}

	public Long getFeautureExtractionId() {
		return feautureExtractionId;
	}

	public void setFeautureExtractionId(Long feautureExtractionId) {
		this.feautureExtractionId = feautureExtractionId;
	}

	public Boolean getDerivedFieldInd() {
		return derivedFieldInd;
	}

	public void setDerivedFieldInd(Boolean derivedFieldInd) {
		this.derivedFieldInd = derivedFieldInd;
	}

	public List<Transformations> getTransformation() {
		return transformation;
	}

	public void setTransformation(List<Transformations> transformation) {
		this.transformation = transformation;
	}

	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getReplaceNullWith() {
		return replaceNullWith;
	}

	public void setReplaceNullWith(String replaceNullWith) {
		this.replaceNullWith = replaceNullWith;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public List<DataValidation> getDataValidations() {
		return dataValidations;
	}

	public void setDataValidations(List<DataValidation> dataValidations) {
		this.dataValidations = dataValidations;
	}

	public List<DataFilter> getDataFilters() {
		return dataFilters;
	}

	public void setDataFilters(List<DataFilter> dataFilters) {
		this.dataFilters = dataFilters;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Long dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public FieldDataTypes getFieldDataType() {
		return fieldDataType;
	}

	public void setFieldDataType(FieldDataTypes fieldDataType) {
		this.fieldDataType = fieldDataType;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Object getEmbeddedFieldDataType() {
		return embeddedFieldDataType;
	}

	public void setEmbeddedFieldDataType(Object embeddedFieldDataType) {
		this.embeddedFieldDataType = embeddedFieldDataType;
	}

	/**
	 * @param values
	 * @return
	 */
	private String checkingEscapeSequence(String values) {
		return (values != null && values.contains("\"")) ? StringEscapeUtils.escapeJava(values) : values;
	}

	public FieldMapping() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */

	@Override
	public int compareTo(FieldMapping field) {

		if (null != field) {
			return this.position > field.position ? 1 : this.position < field.position ? -1 : 0;
		}
		return -1;
	}

	public Boolean getIsPrimaryKey() {
		return isPrimaryKey;
	}

	public void setIsPrimaryKey(Boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public List<String> getSampleRecords() {
		return sampleRecords;
	}

	public void setSampleRecords(List<String> sampleRecords) {
		this.sampleRecords = sampleRecords;
	}

	public ReportPublishData getReportPublishData() {
		return reportPublishData;
	}

	public void setReportPublishData(ReportPublishData reportPublishData) {
		this.reportPublishData = reportPublishData;
	}

	public List<FieldMapping> getListFieldMapping() {
		return listFieldMapping;
	}

	public void setListFieldMapping(List<FieldMapping> listFieldMapping) {
		this.listFieldMapping = listFieldMapping;
	}

	public List<List<String>> getArraySampleRecords() {
		return arraySampleRecords;
	}

	public void setArraySampleRecords(List<List<String>> arraySampleRecords) {
		this.arraySampleRecords = arraySampleRecords;
	}

	
	
	

	/*@Override
	public String toString() {
		return "FieldMapping [id=" + id + ", dataSourceId=" + dataSourceId + ", fieldName=" + fieldName + ", alias="
				+ alias + ", position=" + position + ", fieldDataType=" + fieldDataType + ", replaceNullWith="
				+ replaceNullWith + ", sampleRecord1=" + sampleRecords.get + ", sampleRecord2=" + sampleRecord2
				+ ", sampleRecord3=" + sampleRecord3 + ", sampleRecord4=" + sampleRecord4 + ", sampleRecord5="
				+ sampleRecord5 + ", sampleRecord6=" + sampleRecord6 + ", sampleRecord7=" + sampleRecord7
				+ ", sampleRecord8=" + sampleRecord8 + ", sampleRecord9=" + sampleRecord9 + ", sampleRecord10="
				+ sampleRecord10 + ", format=" + format + ", addVarIndicator=" + addVarIndicator + ", preIngestion="
				+ preIngestion + ", postIngestion=" + postIngestion + ", preValueFrequency=" + preValueFrequency
				+ ", posValueFrequency=" + posValueFrequency + ", key=" + key + ", descriptions=" + descriptions
				+ ", pIIId=" + pIIId + ", dataValidations=" + dataValidations + ", dataFilters=" + dataFilters
				+ ", transformation=" + transformation + ", derivedFieldInd=" + derivedFieldInd
				+ ", feautureExtractionId=" + feautureExtractionId + ", tags=" + tags + ", embeddedFieldDataType= "
				+ embeddedFieldDataType + "]";
	}*/

/*	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addVarIndicator == null) ? 0 : addVarIndicator.hashCode());
		result = prime * result + ((alias == null) ? 0 : alias.hashCode());
		result = prime * result + ((dataFilters == null) ? 0 : dataFilters.hashCode());
		result = prime * result + ((dataSourceId == null) ? 0 : dataSourceId.hashCode());
		result = prime * result + ((dataValidations == null) ? 0 : dataValidations.hashCode());
		result = prime * result + ((derivedFieldInd == null) ? 0 : derivedFieldInd.hashCode());
		result = prime * result + ((descriptions == null) ? 0 : descriptions.hashCode());
		result = prime * result + ((feautureExtractionId == null) ? 0 : feautureExtractionId.hashCode());
		result = prime * result + ((fieldDataType == null) ? 0 : fieldDataType.hashCode());
		result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + ((format == null) ? 0 : format.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((pIIId == null) ? 0 : pIIId.hashCode());
		result = prime * result + ((posValueFrequency == null) ? 0 : posValueFrequency.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((postIngestion == null) ? 0 : postIngestion.hashCode());
		result = prime * result + ((preIngestion == null) ? 0 : preIngestion.hashCode());
		result = prime * result + ((preValueFrequency == null) ? 0 : preValueFrequency.hashCode());
		result = prime * result + ((replaceNullWith == null) ? 0 : replaceNullWith.hashCode());
		result = prime * result + ((sampleRecord1 == null) ? 0 : sampleRecord1.hashCode());
		result = prime * result + ((sampleRecord10 == null) ? 0 : sampleRecord10.hashCode());
		result = prime * result + ((sampleRecord2 == null) ? 0 : sampleRecord2.hashCode());
		result = prime * result + ((sampleRecord3 == null) ? 0 : sampleRecord3.hashCode());
		result = prime * result + ((sampleRecord4 == null) ? 0 : sampleRecord4.hashCode());
		result = prime * result + ((sampleRecord5 == null) ? 0 : sampleRecord5.hashCode());
		result = prime * result + ((sampleRecord6 == null) ? 0 : sampleRecord6.hashCode());
		result = prime * result + ((sampleRecord7 == null) ? 0 : sampleRecord7.hashCode());
		result = prime * result + ((sampleRecord8 == null) ? 0 : sampleRecord8.hashCode());
		result = prime * result + ((sampleRecord9 == null) ? 0 : sampleRecord9.hashCode());
		result = prime * result + ((transformation == null) ? 0 : transformation.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((embeddedFieldDataType == null) ? 0 : embeddedFieldDataType.hashCode());
		return result;
	}*/

/*	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FieldMapping other = (FieldMapping) obj;
		if (addVarIndicator == null) {
			if (other.addVarIndicator != null)
				return false;
		} else if (!addVarIndicator.equals(other.addVarIndicator))
			return false;
		if (alias == null) {
			if (other.alias != null)
				return false;
		} else if (!alias.equals(other.alias))
			return false;
		if (dataFilters == null) {
			if (other.dataFilters != null)
				return false;
		} else if (!dataFilters.equals(other.dataFilters))
			return false;
		if (dataSourceId == null) {
			if (other.dataSourceId != null)
				return false;
		} else if (!dataSourceId.equals(other.dataSourceId))
			return false;
		if (dataValidations == null) {
			if (other.dataValidations != null)
				return false;
		} else if (!dataValidations.equals(other.dataValidations))
			return false;
		if (derivedFieldInd == null) {
			if (other.derivedFieldInd != null)
				return false;
		} else if (!derivedFieldInd.equals(other.derivedFieldInd))
			return false;
		if (descriptions == null) {
			if (other.descriptions != null)
				return false;
		} else if (!descriptions.equals(other.descriptions))
			return false;
		if (feautureExtractionId == null) {
			if (other.feautureExtractionId != null)
				return false;
		} else if (!feautureExtractionId.equals(other.feautureExtractionId))
			return false;
		if (fieldDataType != other.fieldDataType)
			return false;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (format == null) {
			if (other.format != null)
				return false;
		} else if (!format.equals(other.format))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (pIIId == null) {
			if (other.pIIId != null)
				return false;
		} else if (!pIIId.equals(other.pIIId))
			return false;
		if (posValueFrequency == null) {
			if (other.posValueFrequency != null)
				return false;
		} else if (!posValueFrequency.equals(other.posValueFrequency))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (postIngestion == null) {
			if (other.postIngestion != null)
				return false;
		} else if (!postIngestion.equals(other.postIngestion))
			return false;
		if (preIngestion == null) {
			if (other.preIngestion != null)
				return false;
		} else if (!preIngestion.equals(other.preIngestion))
			return false;
		if (preValueFrequency == null) {
			if (other.preValueFrequency != null)
				return false;
		} else if (!preValueFrequency.equals(other.preValueFrequency))
			return false;
		if (replaceNullWith == null) {
			if (other.replaceNullWith != null)
				return false;
		} else if (!replaceNullWith.equals(other.replaceNullWith))
			return false;
		if (sampleRecord1 == null) {
			if (other.sampleRecord1 != null)
				return false;
		} else if (!sampleRecord1.equals(other.sampleRecord1))
			return false;
		if (sampleRecord10 == null) {
			if (other.sampleRecord10 != null)
				return false;
		} else if (!sampleRecord10.equals(other.sampleRecord10))
			return false;
		if (sampleRecord2 == null) {
			if (other.sampleRecord2 != null)
				return false;
		} else if (!sampleRecord2.equals(other.sampleRecord2))
			return false;
		if (sampleRecord3 == null) {
			if (other.sampleRecord3 != null)
				return false;
		} else if (!sampleRecord3.equals(other.sampleRecord3))
			return false;
		if (sampleRecord4 == null) {
			if (other.sampleRecord4 != null)
				return false;
		} else if (!sampleRecord4.equals(other.sampleRecord4))
			return false;
		if (sampleRecord5 == null) {
			if (other.sampleRecord5 != null)
				return false;
		} else if (!sampleRecord5.equals(other.sampleRecord5))
			return false;
		if (sampleRecord6 == null) {
			if (other.sampleRecord6 != null)
				return false;
		} else if (!sampleRecord6.equals(other.sampleRecord6))
			return false;
		if (sampleRecord7 == null) {
			if (other.sampleRecord7 != null)
				return false;
		} else if (!sampleRecord7.equals(other.sampleRecord7))
			return false;
		if (sampleRecord8 == null) {
			if (other.sampleRecord8 != null)
				return false;
		} else if (!sampleRecord8.equals(other.sampleRecord8))
			return false;
		if (sampleRecord9 == null) {
			if (other.sampleRecord9 != null)
				return false;
		} else if (!sampleRecord9.equals(other.sampleRecord9))
			return false;
		if (transformation == null) {
			if (other.transformation != null)
				return false;
		} else if (!transformation.equals(other.transformation))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (embeddedFieldDataType == null) {
			if (other.embeddedFieldDataType != null)
				return false;
		} else if (!embeddedFieldDataType.equals(other.embeddedFieldDataType))
			return false;
		return true;
	}*/
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;

import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;



/** Changed for MAX-101 By Rushikesh Raut on 15-Sept-2016 */
public class FileDataIngesterDetails implements Serializable {

  private static final long serialVersionUID = 1L;
  private Boolean availableToUse;
  private String containsHeader;
  private Long dataSourceId;
  private DataSourceType dataSourceType;
  private String delimiter;
  private Long headerStarting;
  private String headerTemplate;
  private Boolean isDelimiterRegEx = false;
  private Long recordLoadLimit;
  private Long recordStartLine;
  private Boolean recordStartsWith = false;
  private String recordsIgnore;
  private String serverFilePath;
  private String startingRegularExpre;
  private String webServerTempPath;
  private List<ExcelRange> excelRanges;
  private String inputFileType;
  private String inputCompressionType;
  private Boolean optionallyEnclosedInDoubleQuotes = false;
  private String topicName;
  private Long refreshInterval;

  public FileDataIngesterDetails() {}

  public String getTopicName() {
    return topicName;
  }

  public void setTopicName(String topicName) {
    this.topicName = topicName;
  }

  public Long getRefreshInterval() {
    return refreshInterval;
  }

  public void setRefreshInterval(Long refreshInterval) {
    this.refreshInterval = refreshInterval;
  }

  public String getWebServerTempPath() {
    return webServerTempPath;
  }

  public void setWebServerTempPath(String webServerTempPath) {
    this.webServerTempPath = webServerTempPath;
  }

  public Boolean getAvailableToUse() {
    return availableToUse;
  }

  public void setAvailableToUse(Boolean availableToUse) {
    this.availableToUse = availableToUse;
  }

  public String getContainsHeader() {
    return containsHeader;
  }

  public void setContainsHeader(String containsHeader) {
    this.containsHeader = containsHeader;
  }

  public Long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(Long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public DataSourceType getDataSourceType() {
    return dataSourceType;
  }

  public void setDataSourceType(DataSourceType dataSourceType) {
    this.dataSourceType = dataSourceType;
  }

  public String getDelimiter() {
    if (dataSourceType == DataSourceType.TSV_DATA_SOURCE) return "\t";
    else if (dataSourceType == DataSourceType.CSV_DATA_SOURCE) return ",";
    else if (dataSourceType == DataSourceType.REMOTE_DATA) return GlobalParams.MAXIQ_DEL;
    else if (dataSourceType == DataSourceType.RDBMS_DATA_SOURCE) return GlobalParams.MAXIQ_DEL;
    else return delimiter;
  }

  public void setDelimiter(String delimiter) {
    this.delimiter = delimiter;
  }

  public List<ExcelRange> getExcelRanges() {
    return excelRanges;
  }

  public void setExcelRanges(List<ExcelRange> excelRanges) {
    this.excelRanges = excelRanges;
  }

  public Long getHeaderStarting() {
    return headerStarting;
  }

  public void setHeaderStarting(Long headerStarting) {
    this.headerStarting = headerStarting;
  }

  public String getHeaderTemplate() {
	if (dataSourceType == DataSourceType.REMOTE_DATA && null != headerTemplate) {
		headerTemplate = headerTemplate.replaceAll("\t", "");
		headerTemplate = headerTemplate.replaceAll("\n", "");
		headerTemplate = headerTemplate.replaceAll("\r", "");
		headerTemplate = headerTemplate.replaceAll("\b", "");
		headerTemplate = headerTemplate.replaceAll("\f", "");
		headerTemplate = headerTemplate.replaceAll("\"", "");
		headerTemplate = headerTemplate.replaceAll("\\\\", "");
		headerTemplate = headerTemplate.replaceAll(" ", "");
	}
    return headerTemplate;
  }

  public void setHeaderTemplate(String headerTemplate) {
    this.headerTemplate = headerTemplate;
  }

  public Boolean getIsDelimiterRegEx() {
    return isDelimiterRegEx == null ? false : isDelimiterRegEx;
  }

  public void setIsDelimiterRegEx(Boolean isDelimiterRegEx) {
    this.isDelimiterRegEx = isDelimiterRegEx;
  }

  public Long getRecordLoadLimit() {
    return recordLoadLimit;
  }

  public void setRecordLoadLimit(Long recordLoadLimit) {
    this.recordLoadLimit = recordLoadLimit;
  }

  public Long getRecordStartLine() {
    return recordStartLine;
  }

  public void setRecordStartLine(Long recordStartLine) {
    this.recordStartLine = recordStartLine;
  }

  public Boolean getRecordStartsWith() {
    return recordStartsWith;
  }

  public void setRecordStartsWith(Boolean recordStartsWith) {
    this.recordStartsWith = recordStartsWith;
  }

  public String getRecordsIgnore() {
    return recordsIgnore;
  }

  public void setRecordsIgnore(String recordsIgnore) {
    this.recordsIgnore = recordsIgnore;
  }

  public String getServerFilePath() {
    return serverFilePath;
  }

  public void setServerFilePath(String serverFilePath) {
    this.serverFilePath = serverFilePath;
  }

  public String getStartingRegularExpre() {
    return startingRegularExpre;
  }

  public void setStartingRegularExpre(String startingRegularExpre) {
    this.startingRegularExpre = startingRegularExpre;
  }

  public String getInputFileType() {
    return inputFileType == null ? FileTypeEnum.TEXT.name() : inputFileType;
  }

  public void setInputFileType(String inputFileType) {
    this.inputFileType = inputFileType;
  }

  public String getInputCompressionType() {
    return inputCompressionType == null
        ? CompressionTypeEnum.UNCOMPRESSED.name()
        : inputCompressionType;
  }

  public void setInputCompressionType(String inputCompressionType) {
    this.inputCompressionType = inputCompressionType;
  }

  public Boolean getOptionallyEnclosedInDoubleQuotes() {
    return optionallyEnclosedInDoubleQuotes == null ? false : optionallyEnclosedInDoubleQuotes;
  }

  public void setOptionallyEnclosedInDoubleQuotes(Boolean optionallyEnclosedInDoubleQuotes) {
    this.optionallyEnclosedInDoubleQuotes = optionallyEnclosedInDoubleQuotes;
  }

  @Override
  public String toString() {
    return "FileDataIngesterDetails [availableToUse="
        + availableToUse
        + ", containsHeader="
        + containsHeader
        + ", dataSourceId="
        + dataSourceId
        + ", dataSourceType="
        + dataSourceType
        + ", delimiter="
        + delimiter
        + ", headerStarting="
        + headerStarting
        + ", headerTemplate="
        + headerTemplate
        + ", isDelimiterRegEx="
        + isDelimiterRegEx
        + ", recordLoadLimit="
        + recordLoadLimit
        + ", recordStartLine="
        + recordStartLine
        + ", recordStartsWith="
        + recordStartsWith
        + ", recordsIgnore="
        + recordsIgnore
        + ", serverFilePath="
        + serverFilePath
        + ", startingRegularExpre="
        + startingRegularExpre
        + ", webServerTempPath="
        + webServerTempPath
        + ", excelRanges="
        + excelRanges
        + ", inputFileType="
        + inputFileType
        + ", inputCompressionType="
        + inputCompressionType
        + ", optionallyEnclosedInDoubleQuotes="
        + optionallyEnclosedInDoubleQuotes
        + ", topicName="
        + topicName
        + ", refreshInterval="
        + refreshInterval
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((availableToUse == null) ? 0 : availableToUse.hashCode());
    result = prime * result + ((containsHeader == null) ? 0 : containsHeader.hashCode());
    result = prime * result + ((dataSourceId == null) ? 0 : dataSourceId.hashCode());
    result = prime * result + ((dataSourceType == null) ? 0 : dataSourceType.hashCode());
    result = prime * result + ((delimiter == null) ? 0 : delimiter.hashCode());
    result = prime * result + ((excelRanges == null) ? 0 : excelRanges.hashCode());
    result = prime * result + ((headerStarting == null) ? 0 : headerStarting.hashCode());
    result = prime * result + ((headerTemplate == null) ? 0 : headerTemplate.hashCode());
    result =
        prime * result + ((inputCompressionType == null) ? 0 : inputCompressionType.hashCode());
    result = prime * result + ((inputFileType == null) ? 0 : inputFileType.hashCode());
    result = prime * result + ((isDelimiterRegEx == null) ? 0 : isDelimiterRegEx.hashCode());
    result =
        prime * result
            + ((optionallyEnclosedInDoubleQuotes == null)
                ? 0
                : optionallyEnclosedInDoubleQuotes.hashCode());
    result = prime * result + ((recordLoadLimit == null) ? 0 : recordLoadLimit.hashCode());
    result = prime * result + ((recordStartLine == null) ? 0 : recordStartLine.hashCode());
    result = prime * result + ((recordStartsWith == null) ? 0 : recordStartsWith.hashCode());
    result = prime * result + ((recordsIgnore == null) ? 0 : recordsIgnore.hashCode());
    result = prime * result + ((refreshInterval == null) ? 0 : refreshInterval.hashCode());
    result = prime * result + ((serverFilePath == null) ? 0 : serverFilePath.hashCode());
    result =
        prime * result + ((startingRegularExpre == null) ? 0 : startingRegularExpre.hashCode());
    result = prime * result + ((topicName == null) ? 0 : topicName.hashCode());
    result = prime * result + ((webServerTempPath == null) ? 0 : webServerTempPath.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    FileDataIngesterDetails other = (FileDataIngesterDetails) obj;
    if (availableToUse == null) {
      if (other.availableToUse != null) return false;
    } else if (!availableToUse.equals(other.availableToUse)) return false;
    if (containsHeader == null) {
      if (other.containsHeader != null) return false;
    } else if (!containsHeader.equals(other.containsHeader)) return false;
    if (dataSourceId == null) {
      if (other.dataSourceId != null) return false;
    } else if (!dataSourceId.equals(other.dataSourceId)) return false;
    if (dataSourceType != other.dataSourceType) return false;
    if (delimiter == null) {
      if (other.delimiter != null) return false;
    } else if (!delimiter.equals(other.delimiter)) return false;
    if (excelRanges == null) {
      if (other.excelRanges != null) return false;
    } else if (!excelRanges.equals(other.excelRanges)) return false;
    if (headerStarting == null) {
      if (other.headerStarting != null) return false;
    } else if (!headerStarting.equals(other.headerStarting)) return false;
    if (headerTemplate == null) {
      if (other.headerTemplate != null) return false;
    } else if (!headerTemplate.equals(other.headerTemplate)) return false;
    if (inputCompressionType == null) {
      if (other.inputCompressionType != null) return false;
    } else if (!inputCompressionType.equals(other.inputCompressionType)) return false;
    if (inputFileType == null) {
      if (other.inputFileType != null) return false;
    } else if (!inputFileType.equals(other.inputFileType)) return false;
    if (isDelimiterRegEx == null) {
      if (other.isDelimiterRegEx != null) return false;
    } else if (!isDelimiterRegEx.equals(other.isDelimiterRegEx)) return false;
    if (optionallyEnclosedInDoubleQuotes == null) {
      if (other.optionallyEnclosedInDoubleQuotes != null) return false;
    } else if (!optionallyEnclosedInDoubleQuotes.equals(other.optionallyEnclosedInDoubleQuotes))
      return false;
    if (recordLoadLimit == null) {
      if (other.recordLoadLimit != null) return false;
    } else if (!recordLoadLimit.equals(other.recordLoadLimit)) return false;
    if (recordStartLine == null) {
      if (other.recordStartLine != null) return false;
    } else if (!recordStartLine.equals(other.recordStartLine)) return false;
    if (recordStartsWith == null) {
      if (other.recordStartsWith != null) return false;
    } else if (!recordStartsWith.equals(other.recordStartsWith)) return false;
    if (recordsIgnore == null) {
      if (other.recordsIgnore != null) return false;
    } else if (!recordsIgnore.equals(other.recordsIgnore)) return false;
    if (refreshInterval == null) {
      if (other.refreshInterval != null) return false;
    } else if (!refreshInterval.equals(other.refreshInterval)) return false;
    if (serverFilePath == null) {
      if (other.serverFilePath != null) return false;
    } else if (!serverFilePath.equals(other.serverFilePath)) return false;
    if (startingRegularExpre == null) {
      if (other.startingRegularExpre != null) return false;
    } else if (!startingRegularExpre.equals(other.startingRegularExpre)) return false;
    if (topicName == null) {
      if (other.topicName != null) return false;
    } else if (!topicName.equals(other.topicName)) return false;
    if (webServerTempPath == null) {
      if (other.webServerTempPath != null) return false;
    } else if (!webServerTempPath.equals(other.webServerTempPath)) return false;
    return true;
  }
}

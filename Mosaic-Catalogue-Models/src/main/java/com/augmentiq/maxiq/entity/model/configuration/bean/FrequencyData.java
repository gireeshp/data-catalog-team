package com.augmentiq.maxiq.entity.model.configuration.bean;
/** @author Rushi created on Jul 25, 2017 for MAX-1186 */
public class FrequencyData {

  private String data;
  private Long frequency;
  private Double relationFrequency;
  private Double percentage;

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public Long getFrequency() {
    return frequency;
  }

  public void setFrequency(Long frequency) {
    this.frequency = frequency;
  }

  public Double getRelationFrequency() {
    return relationFrequency;
  }

  public void setRelationFrequency(Double relationFrequency) {
    this.relationFrequency = relationFrequency;
  }

  public Double getPercentage() {
    return percentage;
  }

  public void setPercentage(Double percentage) {
    this.percentage = percentage;
  }
}

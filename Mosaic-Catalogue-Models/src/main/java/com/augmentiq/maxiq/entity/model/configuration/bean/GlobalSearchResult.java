package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.ArrayList;
import java.util.List;

import com.augmentiq.maxiq.model.globalsearch.response.DisplayFacades;

/** @author Kaushik Parmar */
public class GlobalSearchResult {

  List<Record> records = new ArrayList<Record>();
  private Long totalCount;
  private String message;
  private DisplayFacades dataSourceAndFlowType;

  public List<Record> getRecords() {
    return records;
  }

  public void setRecords(List<Record> records) {
    this.records = records;
  }

  public Long getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(Long totalCount) {
    this.totalCount = totalCount;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public DisplayFacades getDataSourceAndFlowType() {
    return dataSourceAndFlowType;
  }

  public void setDataSourceAndFlowType(DisplayFacades dataSourceAndFlowType) {
    this.dataSourceAndFlowType = dataSourceAndFlowType;
  }

  public GlobalSearchResult(List<Record> records, Long totalCount, String message) {
    super();
    this.records = records;
    this.totalCount = totalCount;
    this.message = message;
  }

  public GlobalSearchResult(
      List<Record> records, Long totalCount, String message, DisplayFacades dataSourceAndFlowType) {
    super();
    this.records = records;
    this.totalCount = totalCount;
    this.message = message;
    this.dataSourceAndFlowType = dataSourceAndFlowType;
  }

  public GlobalSearchResult() {
    super();
  }

  @Override
  public String toString() {
    return "GlobalSearchResult [records="
        + records
        + ", totalCount="
        + totalCount
        + ", message="
        + message
        + ", dataSourceAndFlowType="
        + dataSourceAndFlowType
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.LinkedList;
import java.util.List;

public class GlobalSearchWithObjectInstance {

  public GlobalSearchWithObjectInstance() {
    super();
    // TODO Auto-generated constructor stub
  }

  List<DataSourceInstance> records;
  Long totalCount;
  List<GlobalSearchWithObjectInstance.Item> requestIndicator;

  public GlobalSearchWithObjectInstance(
      LinkedList<DataSourceInstance> records,
      Long totalCount,
      LinkedList<Item> requestIndicator,
      LinkedList<ObjectRequestToApproveDTO> requestedObjects,
      GlobalSearchResult globalSearchResult) {
    super();
    this.records = records;
    this.totalCount = totalCount;
    this.requestIndicator = requestIndicator;
    this.requestedObjects = requestedObjects;
    this.globalSearchResult = globalSearchResult;
  }

  List<ObjectRequestToApproveDTO> requestedObjects;

  GlobalSearchResult globalSearchResult;

  public GlobalSearchResult getGlobalSearchResult() {
    return globalSearchResult;
  }

  public void setGlobalSearchResult(GlobalSearchResult globalSearchResult) {
    this.globalSearchResult = globalSearchResult;
  }

  public List<DataSourceInstance> getRecords() {
    return records;
  }

  public void setRecords(List<DataSourceInstance> dataSourceInstances) {
    this.records = dataSourceInstances;
  }

  public Long getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(Long totalCount) {
    this.totalCount = totalCount;
  }

  public List<GlobalSearchWithObjectInstance.Item> getRequestIndicator() {
    return requestIndicator;
  }

  public void setRequestIndicator(List<GlobalSearchWithObjectInstance.Item> requestIndicator) {
    this.requestIndicator = requestIndicator;
  }

  public List<ObjectRequestToApproveDTO> getRequestedObjects() {
    return requestedObjects;
  }

  public void setRequestedObjects(List<ObjectRequestToApproveDTO> requestedObjects) {
    this.requestedObjects = requestedObjects;
  }

  @Override
  public String toString() {
    return "GlobalSearchWithObjectInstance [records="
        + records
        + ", totalCount="
        + totalCount
        + ", requestIndicator="
        + requestIndicator
        + ", requestedObjects="
        + requestedObjects
        + ", globalSearchResult="
        + globalSearchResult
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((globalSearchResult == null) ? 0 : globalSearchResult.hashCode());
    result = prime * result + ((records == null) ? 0 : records.hashCode());
    result = prime * result + ((requestIndicator == null) ? 0 : requestIndicator.hashCode());
    result = prime * result + ((requestedObjects == null) ? 0 : requestedObjects.hashCode());
    result = prime * result + ((totalCount == null) ? 0 : totalCount.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GlobalSearchWithObjectInstance other = (GlobalSearchWithObjectInstance) obj;
    if (globalSearchResult == null) {
      if (other.globalSearchResult != null) return false;
    } else if (!globalSearchResult.equals(other.globalSearchResult)) return false;
    if (records == null) {
      if (other.records != null) return false;
    } else if (!records.equals(other.records)) return false;
    if (requestIndicator == null) {
      if (other.requestIndicator != null) return false;
    } else if (!requestIndicator.equals(other.requestIndicator)) return false;
    if (requestedObjects == null) {
      if (other.requestedObjects != null) return false;
    } else if (!requestedObjects.equals(other.requestedObjects)) return false;
    if (totalCount == null) {
      if (other.totalCount != null) return false;
    } else if (!totalCount.equals(other.totalCount)) return false;
    return true;
  }

  public class Item {
    String status;
    String category;
    String subCategory;

    public Item() {
      super();
    }

    public Item(String status, String category, String subCategory) {
      super();
      this.status = status;
      this.category = category;
      this.subCategory = subCategory;
    }

    public String getStatus() {
      return status;
    }

    public void setStatus(String status) {
      this.status = status;
    }

    public String getCategory() {
      return category;
    }

    public void setCategory(String category) {
      this.category = category;
    }

    public String getSubCategory() {
      return subCategory;
    }

    public void setSubCategory(String subCategory) {
      this.subCategory = subCategory;
    }

    @Override
    public String toString() {
      return "Item [status="
          + status
          + ", category="
          + category
          + ", subCategory="
          + subCategory
          + "]";
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + getOuterType().hashCode();
      result = prime * result + ((category == null) ? 0 : category.hashCode());
      result = prime * result + ((status == null) ? 0 : status.hashCode());
      result = prime * result + ((subCategory == null) ? 0 : subCategory.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) return true;
      if (obj == null) return false;
      if (getClass() != obj.getClass()) return false;
      Item other = (Item) obj;
      if (!getOuterType().equals(other.getOuterType())) return false;
      if (category == null) {
        if (other.category != null) return false;
      } else if (!category.equals(other.category)) return false;
      if (status == null) {
        if (other.status != null) return false;
      } else if (!status.equals(other.status)) return false;
      if (subCategory == null) {
        if (other.subCategory != null) return false;
      } else if (!subCategory.equals(other.subCategory)) return false;
      return true;
    }

    private GlobalSearchWithObjectInstance getOuterType() {
      return GlobalSearchWithObjectInstance.this;
    }
  }
}

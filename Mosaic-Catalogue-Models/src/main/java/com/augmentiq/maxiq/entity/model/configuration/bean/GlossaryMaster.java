package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*CREATE TABLE `glossary_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `shortDescription` varchar(100) DEFAULT NULL,
  `longDescription` text,
  `createdDate` varchar(45) NOT NULL,
  `createdBy` varchar(45) NOT NULL,
  `modifiedBy` varchar(45) DEFAULT NULL,
  `modifiedDate` varchar(45) DEFAULT NULL,
  `groupId` int(10) NOT NULL,
  `status` int(1) DEFAULT '1' COMMENT 'Default 1 is Active & 0 is Inactive',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;*/

@TableName(tableName = Sequences.GLOSSARY)
public class GlossaryMaster implements Serializable {
  @Id private Long id;
  private String name;
  private String shortDescription;
  private String longDescription;
  private Long createdDate;
  private Long createdBy;
  private Long modifiedBy;
  private Long modifiedDate;
  private Long groupId;
  private Integer status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getLongDescription() {
    return longDescription;
  }

  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  public Long getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Long getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Long modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((longDescription == null) ? 0 : longDescription.hashCode());
    result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
    result = prime * result + ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((shortDescription == null) ? 0 : shortDescription.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    GlossaryMaster other = (GlossaryMaster) obj;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdDate == null) {
      if (other.createdDate != null) return false;
    } else if (!createdDate.equals(other.createdDate)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (longDescription == null) {
      if (other.longDescription != null) return false;
    } else if (!longDescription.equals(other.longDescription)) return false;
    if (modifiedBy == null) {
      if (other.modifiedBy != null) return false;
    } else if (!modifiedBy.equals(other.modifiedBy)) return false;
    if (modifiedDate == null) {
      if (other.modifiedDate != null) return false;
    } else if (!modifiedDate.equals(other.modifiedDate)) return false;
    if (name == null) {
      if (other.name != null) return false;
    } else if (!name.equals(other.name)) return false;
    if (shortDescription == null) {
      if (other.shortDescription != null) return false;
    } else if (!shortDescription.equals(other.shortDescription)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "GlossaryMaster [id="
        + id
        + ", name="
        + name
        + ", shortDescription="
        + shortDescription
        + ", longDescription="
        + longDescription
        + ", createdDate="
        + createdDate
        + ", createdBy="
        + createdBy
        + ", modifiedBy="
        + modifiedBy
        + ", modifiedDate="
        + modifiedDate
        + ", groupId="
        + groupId
        + ", status="
        + status
        + "]";
  }
}

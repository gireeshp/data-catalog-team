package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;
public class GoogleAnalytic implements Serializable {

  private String p12FilePath;
  private String fromDt;
  private String toDt;
  private List<GoogleCollection> googleCollections;
  private String accesskeyid;
  private String connectionName;

  public String getP12FilePath() {
    return p12FilePath;
  }

  public void setP12FilePath(String p12FilePath) {
    this.p12FilePath = p12FilePath;
  }

  public String getFromDt() {
    return fromDt;
  }

  public void setFromDt(String fromDt) {
    this.fromDt = fromDt;
  }

  public String getToDt() {
    return toDt;
  }

  public void setToDt(String toDt) {
    this.toDt = toDt;
  }

  public List<GoogleCollection> getGoogleCollections() {
    return googleCollections;
  }

  public void setGoogleCollections(List<GoogleCollection> googleCollections) {
    this.googleCollections = googleCollections;
  }

  public String getAccesskeyid() {
    return accesskeyid;
  }

  public void setAccesskeyid(String accesskeyid) {
    this.accesskeyid = accesskeyid;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  @Override
  public String toString() {
    return "GoogleAnalytics [p12FilePath="
        + p12FilePath
        + ", fromDt="
        + fromDt
        + ", toDt="
        + toDt
        + ", googleCollections="
        + googleCollections
        + ", accesskeyid="
        + accesskeyid
        + ", connectionName="
        + connectionName
        + "]";
  }
}

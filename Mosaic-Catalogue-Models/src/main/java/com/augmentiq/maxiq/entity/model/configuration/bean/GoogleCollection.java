package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
public class GoogleCollection {
  String name;
  List<String> dimensions;
  List<String> metrics;
  List<Boolean> dimensionsChk;
  List<Boolean> metricsChk;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getDimensions() {
    return dimensions;
  }

  public void setDimensions(List<String> dimensions) {
    this.dimensions = dimensions;
  }

  public List<String> getMetrics() {
    return metrics;
  }

  public void setMetrics(List<String> metrics) {
    this.metrics = metrics;
  }

  public List<Boolean> getDimensionsChk() {
    return dimensionsChk;
  }

  public void setDimensionsChk(List<Boolean> dimensionsChk) {
    this.dimensionsChk = dimensionsChk;
  }

  public List<Boolean> getMetricsChk() {
    return metricsChk;
  }

  public void setMetricsChk(List<Boolean> metricsChk) {
    this.metricsChk = metricsChk;
  }

  @Override
  public String toString() {
    return "GoogleCollection [name="
        + name
        + ", dimensions="
        + dimensions
        + ", metrics="
        + metrics
        + ", dimensionsChk="
        + dimensionsChk
        + ", metricsChk="
        + metricsChk
        + "]";
  }
}

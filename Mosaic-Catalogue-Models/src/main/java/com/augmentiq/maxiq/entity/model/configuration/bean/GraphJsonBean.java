package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Arrays;

public class GraphJsonBean {

  private String name;
  private String id;
  private GraphJsonBean[] _parents;
  private GraphJsonBean[] _children;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public GraphJsonBean[] get_parents() {
    return _parents;
  }

  public void set_parents(GraphJsonBean[] _parents) {
    this._parents = _parents;
  }

  public GraphJsonBean[] get_children() {
    return _children;
  }

  public void set_children(GraphJsonBean[] _children) {
    this._children = _children;
  }

  @Override
  public String toString() {
    return "GraphJsonBean [name="
        + name
        + ", id="
        + id
        + ", _parents="
        + Arrays.toString(_parents)
        + ", _children="
        + Arrays.toString(_children)
        + "]";
  }
}

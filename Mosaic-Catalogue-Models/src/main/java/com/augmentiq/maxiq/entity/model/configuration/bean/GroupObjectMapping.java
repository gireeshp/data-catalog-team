package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * CREATE TABLE `maxiq`.`group_object_mapping` ( `id` int(11) NOT NULL AUTO_INCREMENT, `groupId`
 * int(11) NOT NULL, `objectId` int(11) NOT NULL, `objectType` varchar(100) NOT NULL, `accessType`
 * int(11) NOT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
 */
/** @author Rushi created on Jul 10, 2017 for MAX-1058 */
@TableName(tableName = Sequences.GROUP_OBJECT_MAPPING)
public class GroupObjectMapping {

  @Id private Long id;
  private Long groupId;
  private Long objectId;
  private String objectType;
  private Integer accessType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getObjectId() {
    return objectId;
  }

  public void setObjectId(Long objectId) {
    this.objectId = objectId;
  }

  public String getObjectType() {
    return objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  public Integer getAccessType() {
    return accessType;
  }

  public void setAccessType(Integer accessType) {
    this.accessType = accessType;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * CREATE TABLE `maxiq`.`group_project_mapping` ( `id` int(11) NOT NULL AUTO_INCREMENT, `groupId`
 * int(11) NOT NULL, `projectId` int(11) NOT NULL, `accessType` int(11) NOT NULL, PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
 */
/** @author Rushi created on Jul 10, 2017 for MAX-1058 */
@TableName(tableName = Sequences.GROUP_PROJECT_MAPPING)
public class GroupProjectMapping {

  @Id private Long id;
  private Long groupId;
  private Long projectId;
  private Integer accessType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public Integer getAccessType() {
    return accessType;
  }

  public void setAccessType(Integer accessType) {
    this.accessType = accessType;
  }
}

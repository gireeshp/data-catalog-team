package com.augmentiq.maxiq.entity.model.configuration.bean;

/** @author 10641134 */
public class Hackyl {

  private String connectionName;
  private String ipUrl;
  private String companyDetails;
  private String asset;
  private String entityType;
  private String entityCode;
  private String storagePath;

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  public String getIpUrl() {
    return ipUrl;
  }

  public void setIpUrl(String ipUrl) {
    this.ipUrl = ipUrl;
  }

  public String getCompanyDetails() {
    return companyDetails;
  }

  public void setCompanyDetails(String companyDetails) {
    this.companyDetails = companyDetails;
  }

  public String getAsset() {
    return asset;
  }

  public void setAsset(String asset) {
    this.asset = asset;
  }

  public String getEntityType() {
    return entityType;
  }

  public void setEntityType(String entityType) {
    this.entityType = entityType;
  }

  public String getEntityCode() {
    return entityCode;
  }

  public void setEntityCode(String entityCode) {
    this.entityCode = entityCode;
  }

  public String getStoragePath() {
    return storagePath;
  }

  public void setStoragePath(String storagePath) {
    this.storagePath = storagePath;
  }
}

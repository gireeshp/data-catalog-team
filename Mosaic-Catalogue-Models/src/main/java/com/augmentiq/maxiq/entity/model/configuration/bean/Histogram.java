package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.HashMap;
/** @author Rushi created on Jul 25, 2017 for MAX-1186 */
public class Histogram {
  private String xLabel;
  private String yLabel;
  private Integer noOfBeans;
  private HashMap<Double, Long> dataPointsList;

  public String getxLabel() {
    return xLabel;
  }

  public void setxLabel(String xLabel) {
    this.xLabel = xLabel;
  }

  public String getyLabel() {
    return yLabel;
  }

  public void setyLabel(String yLabel) {
    this.yLabel = yLabel;
  }

  public Integer getNoOfBeans() {
    return noOfBeans;
  }

  public void setNoOfBeans(Integer noOfBeans) {
    this.noOfBeans = noOfBeans;
  }

  public HashMap<Double, Long> getDataPointsList() {
    return dataPointsList;
  }

  public void setDataPointsList(HashMap<Double, Long> dataPointsList) {
    this.dataPointsList = dataPointsList;
  }
}

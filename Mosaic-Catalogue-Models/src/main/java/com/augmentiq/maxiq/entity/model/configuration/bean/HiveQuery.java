package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * * Data repository is the main configuration bean. It is based on the Composite Design pattern.
 *
 * @author anant
 */
@TableName(tableName = Sequences.HIVE_QUERY)
public class HiveQuery {
  @Id private Long id;
  private String queryName;
  private String delimiter;
  private String query;
  private String userId;
  private Long groupId;
  private Boolean hbase;

  public String getDelimiter() {
    return delimiter;
  }

  public void setDelimiter(String delimiter) {
    this.delimiter = delimiter;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getQueryName() {
    return queryName;
  }

  public void setQueryName(String queryName) {
    this.queryName = queryName;
  }

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public void setHbase(Boolean hbase) {
    this.hbase = hbase;
  }

  public Boolean getHbase() {
    return hbase;
  }

  @Override
  public String toString() {
    return "HiveQuery [id="
        + id
        + ", queryName="
        + queryName
        + ", delimiter="
        + delimiter
        + ", query="
        + query
        + ", userId="
        + userId
        + ", groupId="
        + groupId
        + ", hbase="
        + hbase
        + "]";
  }
}

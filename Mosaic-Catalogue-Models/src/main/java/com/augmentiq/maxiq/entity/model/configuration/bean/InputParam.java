package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.APPVIEWINSTANCE)
public class InputParam {
  @Id Long id;
  Long appId;
  Long jobId;
  List<InputParamValues> inputParametersValues;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

  public List<InputParamValues> getInputParametersValues() {
    return inputParametersValues;
  }

  public void setInputParametersValues(List<InputParamValues> inputParametersValues) {
    this.inputParametersValues = inputParametersValues;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result =
        prime * result + ((inputParametersValues == null) ? 0 : inputParametersValues.hashCode());
    result = prime * result + ((jobId == null) ? 0 : jobId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    InputParam other = (InputParam) obj;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (inputParametersValues == null) {
      if (other.inputParametersValues != null) return false;
    } else if (!inputParametersValues.equals(other.inputParametersValues)) return false;
    if (jobId == null) {
      if (other.jobId != null) return false;
    } else if (!jobId.equals(other.jobId)) return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("InputParam [id=");
    builder.append(id);
    builder.append(", appId=");
    builder.append(appId);
    builder.append(", jobId=");
    builder.append(jobId);
    builder.append(", inputParametersValues=");
    builder.append(inputParametersValues);
    builder.append("]");
    return builder.toString();
  }
}

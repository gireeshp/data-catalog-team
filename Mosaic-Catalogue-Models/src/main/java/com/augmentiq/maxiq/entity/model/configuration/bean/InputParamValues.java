package com.augmentiq.maxiq.entity.model.configuration.bean;

public class InputParamValues {
  String paramName;
  String paramType;
  String paramValue;
  String jobId;

  public String getParamName() {
    return paramName;
  }

  public void setParamName(String paramName) {
    this.paramName = paramName;
  }

  public String getParamType() {
    return paramType;
  }

  public void setParamType(String paramType) {
    this.paramType = paramType;
  }

  public String getParamValue() {
    return paramValue;
  }

  public void setParamValue(String paramValue) {
    this.paramValue = paramValue;
  }

  public String getJobId() {
    return jobId;
  }

  public void setJobId(String jobId) {
    this.jobId = jobId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((jobId == null) ? 0 : jobId.hashCode());
    result = prime * result + ((paramName == null) ? 0 : paramName.hashCode());
    result = prime * result + ((paramType == null) ? 0 : paramType.hashCode());
    result = prime * result + ((paramValue == null) ? 0 : paramValue.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    InputParamValues other = (InputParamValues) obj;
    if (jobId == null) {
      if (other.jobId != null) return false;
    } else if (!jobId.equals(other.jobId)) return false;
    if (paramName == null) {
      if (other.paramName != null) return false;
    } else if (!paramName.equals(other.paramName)) return false;
    if (paramType == null) {
      if (other.paramType != null) return false;
    } else if (!paramType.equals(other.paramType)) return false;
    if (paramValue == null) {
      if (other.paramValue != null) return false;
    } else if (!paramValue.equals(other.paramValue)) return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("InputParamValues [paramName=");
    builder.append(paramName);
    builder.append(", paramType=");
    builder.append(paramType);
    builder.append(", paramValue=");
    builder.append(paramValue);
    builder.append(", jobId=");
    builder.append(jobId);
    builder.append("]");
    return builder.toString();
  }
}

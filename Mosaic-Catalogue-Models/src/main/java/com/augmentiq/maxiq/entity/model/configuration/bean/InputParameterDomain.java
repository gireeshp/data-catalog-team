package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;

public class InputParameterDomain implements Serializable {
  private String paramName;
  private FieldDataTypes paramType;
  private String defaultVal;

  public InputParameterDomain() {
    super();
  }

  public InputParameterDomain(String paramName, FieldDataTypes paramType, String defaultVal) {
    super();
    this.paramName = paramName;
    this.paramType = paramType;
    this.defaultVal = defaultVal;
  }

  public String getParamName() {
    return paramName;
  }

  public void setParamName(String paramName) {
    this.paramName = paramName;
  }

  public FieldDataTypes getParamType() {
    return paramType;
  }

  public void setParamType(FieldDataTypes paramType) {
    this.paramType = paramType;
  }

  public String getDefaultVal() {
    return defaultVal;
  }

  public void setDefaultVal(String defaultVal) {
    this.defaultVal = defaultVal;
  }

  @Override
  public String toString() {
    return "InputParameterDomain [paramName="
        + paramName
        + ", paramType="
        + paramType
        + ", defaultVal="
        + defaultVal
        + "]";
  }
}

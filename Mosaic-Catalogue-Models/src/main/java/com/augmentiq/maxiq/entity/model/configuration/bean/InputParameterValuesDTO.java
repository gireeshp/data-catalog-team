package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.InputParamTypes;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.DATA_PARAM_INSTANCE)
public class InputParameterValuesDTO implements Serializable {

  @Id private Long id;
  private Long dsId;
  private Long drId;
  private Long appId;
  private String type;
  private InputParamTypes inputParamType;
  private Long baapJobInsId;
  private String paramName;
  private String val;
  private String dataSourceName;
  private String appName;
  
  public InputParameterValuesDTO(
      Long id,
      Long dsId,
      Long drId,
      Long baapJobInsId,
      String paramName,
      String val,
      String type,
      Long appId,
      InputParamTypes inputParamType,      
      String dataSourceName,
      String appName) {
    super();
    this.id = id;
    this.dsId = dsId;
    this.drId = drId;
    this.appId = appId;
    this.type = type;
    this.inputParamType = inputParamType;
    this.baapJobInsId = baapJobInsId;
    this.paramName = paramName;
    this.val = val;
    this.dataSourceName = dataSourceName;
    this.appName = appName;
  }

  public InputParamTypes getInputParamType() {
    return inputParamType;
  }

  public void setInputParamType(InputParamTypes inputParamType) {
    this.inputParamType = inputParamType;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getDsId() {
    return dsId;
  }

  public void setDsId(Long dsId) {
    this.dsId = dsId;
  }

  public Long getDrId() {
    return drId;
  }

  public void setDrId(Long drId) {
    this.drId = drId;
  }

  public Long getBaapJobInsId() {
    return baapJobInsId;
  }

  public void setBaapJobInsId(Long baapJobInsId) {
    this.baapJobInsId = baapJobInsId;
  }

  public String getParamName() {
    return paramName;
  }

  public void setParamName(String paramName) {
    this.paramName = paramName;
  }

  public String getVal() {
    return val;
  }

  public void setVal(String val) {
    this.val = val;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getDataSourceName() {
    return dataSourceName;
  }

  public void setDataSourceName(String dataSourceName) {
    this.dataSourceName = dataSourceName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((appName == null) ? 0 : appName.hashCode());
    result = prime * result + ((baapJobInsId == null) ? 0 : baapJobInsId.hashCode());
    result = prime * result + ((drId == null) ? 0 : drId.hashCode());
    result = prime * result + ((dsId == null) ? 0 : dsId.hashCode());
    result = prime * result + ((dataSourceName == null) ? 0 : dataSourceName.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((inputParamType == null) ? 0 : inputParamType.hashCode());
    result = prime * result + ((paramName == null) ? 0 : paramName.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    result = prime * result + ((val == null) ? 0 : val.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    InputParameterValuesDTO other = (InputParameterValuesDTO) obj;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (appName == null) {
      if (other.appName != null) return false;
    } else if (!appName.equals(other.appName)) return false;
    if (baapJobInsId == null) {
      if (other.baapJobInsId != null) return false;
    } else if (!baapJobInsId.equals(other.baapJobInsId)) return false;
    if (drId == null) {
      if (other.drId != null) return false;
    } else if (!drId.equals(other.drId)) return false;
    if (dsId == null) {
      if (other.dsId != null) return false;
    } else if (!dsId.equals(other.dsId)) return false;
    if (dataSourceName == null) {
      if (other.dataSourceName != null) return false;
    } else if (!dataSourceName.equals(other.dataSourceName)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (inputParamType != other.inputParamType) return false;
    if (paramName == null) {
      if (other.paramName != null) return false;
    } else if (!paramName.equals(other.paramName)) return false;
    if (type == null) {
      if (other.type != null) return false;
    } else if (!type.equals(other.type)) return false;
    if (val == null) {
      if (other.val != null) return false;
    } else if (!val.equals(other.val)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "InputParameterValuesDTO [id="
        + id
        + ", dsId="
        + dsId
        + ", drId="
        + drId
        + ", appId="
        + appId
        + ", type="
        + type
        + ", inputParamType="
        + inputParamType
        + ", baapJobInsId="
        + baapJobInsId
        + ", paramName="
        + paramName
        + ", val="
        + val
        + ", dsName="
        + dataSourceName
        + ", appName="
        + appName
        + "]";
  }
}

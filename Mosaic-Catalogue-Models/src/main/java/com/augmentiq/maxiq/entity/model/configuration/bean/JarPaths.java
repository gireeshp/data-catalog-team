package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.MAXIQ_COMMONS)
public class JarPaths {
  @Id Long id;
  String group_name;
  String keyOf;
  String value;
  String status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getGroup_name() {
    return group_name;
  }

  public void setGroup_name(String group_name) {
    this.group_name = group_name;
  }

  public String getKeyOf() {
    return keyOf;
  }

  public void setKeyOf(String keyOf) {
    this.keyOf = keyOf;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((group_name == null) ? 0 : group_name.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((keyOf == null) ? 0 : keyOf.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    JarPaths other = (JarPaths) obj;
    if (group_name == null) {
      if (other.group_name != null) return false;
    } else if (!group_name.equals(other.group_name)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (keyOf == null) {
      if (other.keyOf != null) return false;
    } else if (!keyOf.equals(other.keyOf)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    if (value == null) {
      if (other.value != null) return false;
    } else if (!value.equals(other.value)) return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("JarPaths [id=");
    builder.append(id);
    builder.append(", group_name=");
    builder.append(group_name);
    builder.append(", keyOf=");
    builder.append(keyOf);
    builder.append(", value=");
    builder.append(value);
    builder.append(", status=");
    builder.append(status);
    builder.append("]");
    return builder.toString();
  }
}

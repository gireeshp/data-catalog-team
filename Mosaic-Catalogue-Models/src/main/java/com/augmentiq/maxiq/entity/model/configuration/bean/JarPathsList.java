package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.MAXIQ_COMMONS)
public class JarPathsList {
  @Id List<JarPaths> jarPaths;

  public List<JarPaths> getJarPaths() {
    return jarPaths;
  }

  public void setJarPaths(List<JarPaths> jarPaths) {
    this.jarPaths = jarPaths;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((jarPaths == null) ? 0 : jarPaths.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    JarPathsList other = (JarPathsList) obj;
    if (jarPaths == null) {
      if (other.jarPaths != null) return false;
    } else if (!jarPaths.equals(other.jarPaths)) return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("JarPathsList [jarPaths=");
    builder.append(jarPaths);
    builder.append("]");
    return builder.toString();
  }
}

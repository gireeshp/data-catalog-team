package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
public class JobSchedular {
  private boolean manual;
  private List<TimeObject> fetchTimes;
  private List<String> weekDays;
  private List<String> dates;
  private boolean repeat;

  public boolean isManual() {
    return manual;
  }

  public void setManual(boolean manual) {
    this.manual = manual;
  }

  public List<TimeObject> getFetchTimes() {
    return fetchTimes;
  }

  public void setFetchTimes(List<TimeObject> fetchTimes) {
    this.fetchTimes = fetchTimes;
  }

  public List<String> getWeekDays() {
    return weekDays;
  }

  public void setWeekDays(List<String> weekDays) {
    this.weekDays = weekDays;
  }

  public List<String> getDates() {
    return dates;
  }

  public void setDates(List<String> dates) {
    this.dates = dates;
  }

  public boolean isRepeat() {
    return repeat;
  }

  public void setRepeat(boolean repeat) {
    this.repeat = repeat;
  }

  public JobSchedular() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "JobSchedular [manual="
        + manual
        + ", fetchTimes="
        + fetchTimes
        + ", weekDays="
        + weekDays
        + ", dates="
        + dates
        + ", repeat="
        + repeat
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dates == null) ? 0 : dates.hashCode());
    result = prime * result + ((fetchTimes == null) ? 0 : fetchTimes.hashCode());
    result = prime * result + (manual ? 1231 : 1237);
    result = prime * result + (repeat ? 1231 : 1237);
    result = prime * result + ((weekDays == null) ? 0 : weekDays.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    JobSchedular other = (JobSchedular) obj;
    if (dates == null) {
      if (other.dates != null) return false;
    } else if (!dates.equals(other.dates)) return false;
    if (fetchTimes == null) {
      if (other.fetchTimes != null) return false;
    } else if (!fetchTimes.equals(other.fetchTimes)) return false;
    if (manual != other.manual) return false;
    if (repeat != other.repeat) return false;
    if (weekDays == null) {
      if (other.weekDays != null) return false;
    } else if (!weekDays.equals(other.weekDays)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

public class Join implements Cloneable{
	private String leftTable;
	private String rightTable;
	private List<String> leftColumn;
	private List<String> rightColumn;
	private String keyAlias;
	public String getLeftTable() {
		return leftTable;
	}
	public void setLeftTable(String leftTable) {
		this.leftTable = leftTable;
	}
	public String getRightTable() {
		return rightTable;
	}
	public void setRightTable(String rightTable) {
		this.rightTable = rightTable;
	}
	public List<String> getLeftColumn() {
		return leftColumn;
	}
	public void setLeftColumn(List<String> leftColumn) {
		this.leftColumn = leftColumn;
	}
	public List<String> getRightColumn() {
		return rightColumn;
	}
	public void setRightColumn(List<String> rightColumn) {
		this.rightColumn = rightColumn;
	}
	public String getKeyAlias() {
		return keyAlias;
	}
	public void setKeyAlias(String keyAlias) {
		this.keyAlias = keyAlias;
	}

	public boolean equals(Object o) {

		if (o == this) return true;
		if (!(o instanceof Join)) {
			return false;
		}

		Join join = (Join) o;

		return (join.leftTable.equals(leftTable) ||join.leftTable.equals(rightTable))&&
				(join.rightTable.equals(leftTable) ||join.rightTable.equals(rightTable));
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + rightTable.hashCode();
		result = 31 * result + leftTable.hashCode();
		return result;
	}
	@Override
	public Join clone()
	{
		Join join=null;
		try
		{
			join = (Join) super.clone();
		}
		catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}
		return join;
	}
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * @author Balkrushna Patil create table jupyter(id int NOT NULL AUTO_INCREMENT,notebookName
 *     varchar(40),createdOn varchar(40),createdBy varchar(40),groupId int(10), ownerProjectId
 *     int(10), PRIMARY KEY (id));
 */
@TableName(tableName = Sequences.JUPYTER)
public class JupyterBean implements Serializable {
  @Id private int id;
  private String notebookName;
  private String createdOn;
  private String createdBy;
  private Long groupId;
  private Long ownerProjectId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNotebookName() {
    return notebookName;
  }

  public void setNotebookName(String notebookName) {
    this.notebookName = notebookName;
  }

  public String getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(String createdOn) {
    this.createdOn = createdOn;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + id;
    result = prime * result + ((notebookName == null) ? 0 : notebookName.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    JupyterBean other = (JupyterBean) obj;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdOn == null) {
      if (other.createdOn != null) return false;
    } else if (!createdOn.equals(other.createdOn)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (id != other.id) return false;
    if (notebookName == null) {
      if (other.notebookName != null) return false;
    } else if (!notebookName.equals(other.notebookName)) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "JupyterBean [id="
        + id
        + ", notebookName="
        + notebookName
        + ", createdOn="
        + createdOn
        + ", createdBy="
        + createdBy
        + ", groupId="
        + groupId
        + ", ownerProjectId="
        + ownerProjectId
        + "]";
  }
}

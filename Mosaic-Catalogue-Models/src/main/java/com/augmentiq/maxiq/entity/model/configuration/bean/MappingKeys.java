package com.augmentiq.maxiq.entity.model.configuration.bean;

/**
 * This will mantain the keys required for combining
 *
 * @author shiva
 */
public class MappingKeys {
  private Long leftKey;
  private Long rightKey;

  public Long getLeftKey() {
    return leftKey;
  }

  public void setLeftKey(Long leftKey) {
    this.leftKey = leftKey;
  }

  public Long getRightKey() {
    return rightKey;
  }

  public void setRightKey(Long rightKey) {
    this.rightKey = rightKey;
  }

  public MappingKeys() {}

  @Override
  public String toString() {
    return "MappingKeys [leftKey=" + leftKey + ", rightKey=" + rightKey + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((leftKey == null) ? 0 : leftKey.hashCode());
    result = prime * result + ((rightKey == null) ? 0 : rightKey.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MappingKeys other = (MappingKeys) obj;
    if (leftKey == null) {
      if (other.leftKey != null) return false;
    } else if (!leftKey.equals(other.leftKey)) return false;
    if (rightKey == null) {
      if (other.rightKey != null) return false;
    } else if (!rightKey.equals(other.rightKey)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Arrays;
import java.util.List;

import com.augmentiq.maxiq.constant.configuration.enums.Id;

public class MarketPlace {
  private String area;
  private List<SolutionShort> solutionShorts;
  private byte[] image;
  private String description;

  @Id private Long marketPlaceId;

  public MarketPlace(String area, List<SolutionShort> solutionShorts) {
    super();
    this.area = area;
    this.solutionShorts = solutionShorts;
  }

  public MarketPlace(
      String area,
      List<SolutionShort> solutionShorts,
      byte[] image,
      String description,
      Long marketPlaceId) {
    super();
    this.area = area;
    this.solutionShorts = solutionShorts;
    this.image = image;
    this.description = description;
    this.marketPlaceId = marketPlaceId;
  }

  public Long getMarketPlaceId() {
    return marketPlaceId;
  }

  public void setMarketPlaceId(Long marketPlaceId) {
    this.marketPlaceId = marketPlaceId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String discription) {
    this.description = discription;
  }

  public byte[] getImage() {
    return image;
  }

  public void setImage(byte[] image) {
    this.image = image;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public List<SolutionShort> getSolutionShorts() {
    return solutionShorts;
  }

  public void setSolutionShorts(List<SolutionShort> solutionShorts) {
    this.solutionShorts = solutionShorts;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((area == null) ? 0 : area.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + Arrays.hashCode(image);
    result = prime * result + ((marketPlaceId == null) ? 0 : marketPlaceId.hashCode());
    result = prime * result + ((solutionShorts == null) ? 0 : solutionShorts.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MarketPlace other = (MarketPlace) obj;
    if (area == null) {
      if (other.area != null) return false;
    } else if (!area.equals(other.area)) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (!Arrays.equals(image, other.image)) return false;
    if (marketPlaceId == null) {
      if (other.marketPlaceId != null) return false;
    } else if (!marketPlaceId.equals(other.marketPlaceId)) return false;
    if (solutionShorts == null) {
      if (other.solutionShorts != null) return false;
    } else if (!solutionShorts.equals(other.solutionShorts)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "MarketPlace [area="
        + area
        + ", solutionShorts="
        + solutionShorts
        + ", image="
        + Arrays.toString(image)
        + ", description="
        + description
        + ", marketPlaceId="
        + marketPlaceId
        + "]";
  }
}

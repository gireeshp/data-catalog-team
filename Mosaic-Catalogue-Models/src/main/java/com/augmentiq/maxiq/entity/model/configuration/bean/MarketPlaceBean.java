package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Arrays;

public class MarketPlaceBean implements Serializable {
  private Long mp_id;
  private String solution_name;
  private String shortDesc;
  private byte[] solution_image;

  public Long getMp_id() {
    return mp_id;
  }

  public void setMp_id(Long mp_id) {
    this.mp_id = mp_id;
  }

  public String getSolution_name() {
    return solution_name;
  }

  public void setSolution_name(String solution_name) {
    this.solution_name = solution_name;
  }

  public String getShortDesc() {
    return shortDesc;
  }

  public void setShortDesc(String shortDesc) {
    this.shortDesc = shortDesc;
  }

  public byte[] getSolution_image() {
    return solution_image;
  }

  public void setSolution_image(byte[] solution_image) {
    this.solution_image = solution_image;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((mp_id == null) ? 0 : mp_id.hashCode());
    result = prime * result + ((shortDesc == null) ? 0 : shortDesc.hashCode());
    result = prime * result + Arrays.hashCode(solution_image);
    result = prime * result + ((solution_name == null) ? 0 : solution_name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MarketPlaceBean other = (MarketPlaceBean) obj;
    if (mp_id == null) {
      if (other.mp_id != null) return false;
    } else if (!mp_id.equals(other.mp_id)) return false;
    if (shortDesc == null) {
      if (other.shortDesc != null) return false;
    } else if (!shortDesc.equals(other.shortDesc)) return false;
    if (!Arrays.equals(solution_image, other.solution_image)) return false;
    if (solution_name == null) {
      if (other.solution_name != null) return false;
    } else if (!solution_name.equals(other.solution_name)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "MarketPlaceBean [mp_id="
        + mp_id
        + ", solution_name="
        + solution_name
        + ", shortDesc="
        + shortDesc
        + ", solution_image="
        + Arrays.toString(solution_image)
        + "]";
  }
}

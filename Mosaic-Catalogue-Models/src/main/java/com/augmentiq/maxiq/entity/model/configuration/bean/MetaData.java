package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

public class MetaData {

  private List<String> metaDataParents;
  private List<MetaDataFields> metaDataFields;
  private List<MetaDataStructure> metaDataStructures;

  public List<String> getMetaDataParents() {
    return metaDataParents;
  }

  public void setMetaDataParents(List<String> metaDataParents) {
    this.metaDataParents = metaDataParents;
  }

  public List<MetaDataFields> getMetaDataFields() {
    return metaDataFields;
  }

  public void setMetaDataFields(List<MetaDataFields> metaDataFields) {
    this.metaDataFields = metaDataFields;
  }

  public List<MetaDataStructure> getMetaDataStructures() {
    return metaDataStructures;
  }

  public void setMetaDataStructures(List<MetaDataStructure> metaDataStructures) {
    this.metaDataStructures = metaDataStructures;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((metaDataFields == null) ? 0 : metaDataFields.hashCode());
    result = prime * result + ((metaDataParents == null) ? 0 : metaDataParents.hashCode());
    result = prime * result + ((metaDataStructures == null) ? 0 : metaDataStructures.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MetaData other = (MetaData) obj;
    if (metaDataFields == null) {
      if (other.metaDataFields != null) return false;
    } else if (!metaDataFields.equals(other.metaDataFields)) return false;
    if (metaDataParents == null) {
      if (other.metaDataParents != null) return false;
    } else if (!metaDataParents.equals(other.metaDataParents)) return false;
    if (metaDataStructures == null) {
      if (other.metaDataStructures != null) return false;
    } else if (!metaDataStructures.equals(other.metaDataStructures)) return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MetaData [metaDataParents=");
    builder.append(metaDataParents);
    builder.append(", metaDataFields=");
    builder.append(metaDataFields);
    builder.append(", metaDataStructures=");
    builder.append(metaDataStructures);
    builder.append("]");
    return builder.toString();
  }
}

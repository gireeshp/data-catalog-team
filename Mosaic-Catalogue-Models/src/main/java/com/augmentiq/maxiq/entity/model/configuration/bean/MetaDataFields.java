package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

public class MetaDataFields {
  private List<String> metaDataField;

  public List<String> getMetaDataField() {
    return metaDataField;
  }

  public void setMetaDataField(List<String> metaDataField) {
    this.metaDataField = metaDataField;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MetaDataFields [metaDataField=");
    builder.append(metaDataField);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((metaDataField == null) ? 0 : metaDataField.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MetaDataFields other = (MetaDataFields) obj;
    if (metaDataField == null) {
      if (other.metaDataField != null) return false;
    } else if (!metaDataField.equals(other.metaDataField)) return false;
    return true;
  }
}

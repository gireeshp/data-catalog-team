package com.augmentiq.maxiq.entity.model.configuration.bean;

public class MetaDataStructure {

  private String filePath;
  private String fileName;
  private String delimiter;
  private String fileContent;

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getDelimiter() {
    return delimiter;
  }

  public void setDelimiter(String delimiter) {
    this.delimiter = delimiter;
  }

  public String getFileContent() {
    return fileContent;
  }

  public void setFileContent(String fileContent) {
    this.fileContent = fileContent;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MetaDataStructure [filePath=");
    builder.append(filePath);
    builder.append(", fileName=");
    builder.append(fileName);
    builder.append(", delimiter=");
    builder.append(delimiter);
    builder.append(", fileContent=");
    builder.append(fileContent);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((delimiter == null) ? 0 : delimiter.hashCode());
    result = prime * result + ((fileContent == null) ? 0 : fileContent.hashCode());
    result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
    result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MetaDataStructure other = (MetaDataStructure) obj;
    if (delimiter == null) {
      if (other.delimiter != null) return false;
    } else if (!delimiter.equals(other.delimiter)) return false;
    if (fileContent == null) {
      if (other.fileContent != null) return false;
    } else if (!fileContent.equals(other.fileContent)) return false;
    if (fileName == null) {
      if (other.fileName != null) return false;
    } else if (!fileName.equals(other.fileName)) return false;
    if (filePath == null) {
      if (other.filePath != null) return false;
    } else if (!filePath.equals(other.filePath)) return false;
    return true;
  }
}

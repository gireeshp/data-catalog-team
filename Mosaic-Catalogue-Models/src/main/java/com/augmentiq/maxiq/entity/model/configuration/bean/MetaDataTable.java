package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.MAXIQ_METADATA)
public class MetaDataTable {

  @Id private Long id;
  private Long dataSourceId;
  private MetaData metaData;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getDataSourceId() {
    return dataSourceId;
  }

  public void setDataSourceId(Long dataSourceId) {
    this.dataSourceId = dataSourceId;
  }

  public MetaData getMetaData() {
    return metaData;
  }

  public void setMetaData(MetaData metaData) {
    this.metaData = metaData;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MetaDataTable [id=");
    builder.append(id);
    builder.append(", dataSourceId=");
    builder.append(dataSourceId);
    builder.append(", metaData=");
    builder.append(metaData);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dataSourceId == null) ? 0 : dataSourceId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((metaData == null) ? 0 : metaData.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MetaDataTable other = (MetaDataTable) obj;
    if (dataSourceId == null) {
      if (other.dataSourceId != null) return false;
    } else if (!dataSourceId.equals(other.dataSourceId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (metaData == null) {
      if (other.metaData != null) return false;
    } else if (!metaData.equals(other.metaData)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.LinkedHashMap;
import java.util.Map;

public class ModelBean {

  private String appId = "";
  private String modelType = "";
  private String modelPath = "";
  private String requestId = "";
  private Map<String, String> featureData = new LinkedHashMap<String, String>();

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public String getModelType() {
    return modelType;
  }

  public void setModelType(String modelType) {
    this.modelType = modelType;
  }

  public String getModelPath() {
    return modelPath;
  }

  public void setModelPath(String modelPath) {
    this.modelPath = modelPath;
  }

  public Map<String, String> getFeatureData() {
    return featureData;
  }

  public void setFeatureData(Map<String, String> featureData) {
    this.featureData = featureData;
  }

  @Override
  public String toString() {
    return "ModelBean [appId="
        + appId
        + ", modelType="
        + modelType
        + ", modelPath="
        + modelPath
        + ", requestId="
        + requestId
        + ", featureData="
        + featureData
        + "]";
  }
}

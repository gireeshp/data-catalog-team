package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;

public class ModelFieldMapping implements Serializable {
  private String name;
  private FieldDataTypes dataType;
  private String format;

  public ModelFieldMapping() {
    super();
  }

  public ModelFieldMapping(String name, FieldDataTypes dataType, String format) {
    super();
    this.name = name;
    this.dataType = dataType;
    this.format = format;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public FieldDataTypes getDataType() {
    return dataType;
  }

  public void setDataType(FieldDataTypes dataType) {
    this.dataType = dataType;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  @Override
  public String toString() {
    return "ModelFieldMapping [name="
        + name
        + ", dataType="
        + dataType
        + ", format="
        + format
        + "]";
  }
}

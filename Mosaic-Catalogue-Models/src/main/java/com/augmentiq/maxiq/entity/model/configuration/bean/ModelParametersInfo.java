package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Arrays;

public class ModelParametersInfo implements Serializable {
  private ModelFieldMapping inputFields[];
  private ModelFieldMapping outputFields[];

  public ModelFieldMapping[] getInputFields() {
    return inputFields;
  }

  public void setInputFields(ModelFieldMapping[] inputFields) {
    this.inputFields = inputFields;
  }

  public ModelFieldMapping[] getOutputFields() {
    return outputFields;
  }

  public void setOutputFields(ModelFieldMapping[] outputFields) {
    this.outputFields = outputFields;
  }

  @Override
  public String toString() {
    return "ModelParametersInfo [inputFields="
        + Arrays.toString(inputFields)
        + ", outputFields="
        + Arrays.toString(outputFields)
        + "]";
  }
}

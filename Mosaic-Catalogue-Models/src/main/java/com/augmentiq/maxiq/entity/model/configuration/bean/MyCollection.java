package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 * @author Ajinkya Marathe on 03-Mar-2017
 *

 CREATE TABLE `my_collection` (
  `id` int(15) NOT NULL,
  `objectId` int(15) DEFAULT '0',
  `objectType` varchar(100) DEFAULT '0',
  `userId` int(15) DEFAULT '0',
  `created_date` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT 'By defaullt status is 1 as active record \nif 0 then which is removed OR Inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */

@TableName(tableName = Sequences.MY_COLLECTION)
public class MyCollection implements Serializable {

  @Id private Long id;
  private Long objectId;
  private String objectType;
  private Long userId;
  private Long created_date;
  private Integer status;

  public MyCollection() {
    super();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getObjectId() {
    return objectId;
  }

  public void setObjectId(Long objectId) {
    this.objectId = objectId;
  }

  public String getObjectType() {
    return objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getCreated_date() {
    return created_date;
  }

  public void setCreated_date(Long created_date) {
    this.created_date = created_date;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((created_date == null) ? 0 : created_date.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
    result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    MyCollection other = (MyCollection) obj;
    if (created_date == null) {
      if (other.created_date != null) return false;
    } else if (!created_date.equals(other.created_date)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (objectId == null) {
      if (other.objectId != null) return false;
    } else if (!objectId.equals(other.objectId)) return false;
    if (objectType == null) {
      if (other.objectType != null) return false;
    } else if (!objectType.equals(other.objectType)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "MyCollection [id="
        + id
        + ", objectId="
        + objectId
        + ", objectType="
        + objectType
        + ", userId="
        + userId
        + ", created_date="
        + created_date
        + ", status="
        + status
        + "]";
  }
}

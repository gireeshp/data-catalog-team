package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
import java.util.Map;

public class NodeGraphInfo {

  private String appName;
  private String name;
  private String nodeType;
  private Long id;
  private Long nodeId;
  private String processType;
  private Double x;
  private Double y;
  private String datasourceId;
  private List<Map<String, String>> inputConnectors;
  private List<Map<String, String>> outputConnectors;
  private String refAppName;
  private String refNodeName;
  private String description;
  private String descritption;

  public String getDescritption() {
    return descritption;
  }

  public void setDescritption(String descritption) {
    this.descritption = descritption;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getRefAppName() {
    return refAppName;
  }

  public void setRefAppName(String refAppName) {
    this.refAppName = refAppName;
  }

  public String getRefNodeName() {
    return refNodeName;
  }

  public void setRefNodeName(String refNodeName) {
    this.refNodeName = refNodeName;
  }

  public Long getNodeId() {
    return nodeId;
  }

  public void setNodeId(Long nodeId) {
    this.nodeId = nodeId;
  }

  public String getDatasourceId() {
    return datasourceId;
  }

  public void setDatasourceId(String datasourceId) {
    this.datasourceId = datasourceId;
  }

  public NodeGraphInfo() {}

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNodeType() {
    return nodeType;
  }

  public void setNodeType(String nodeType) {
    this.nodeType = nodeType;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProcessType() {
    return processType;
  }

  public void setProcessType(String processType) {
    this.processType = processType;
  }

  public Double getX() {
    return x;
  }

  public void setX(Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(Double y) {
    this.y = y;
  }

  public List<Map<String, String>> getInputConnectors() {
    return inputConnectors;
  }

  public void setInputConnectors(List<Map<String, String>> inputConnectors) {
    this.inputConnectors = inputConnectors;
  }

  public List<Map<String, String>> getOutputConnectors() {
    return outputConnectors;
  }

  public void setOutputConnectors(List<Map<String, String>> outputConnectors) {
    this.outputConnectors = outputConnectors;
  }

  @Override
  public String toString() {
    return "NodeGraphInfo [appName="
        + appName
        + ", name="
        + name
        + ", nodeType="
        + nodeType
        + ", id="
        + id
        + ", nodeId="
        + nodeId
        + ", processType="
        + processType
        + ", x="
        + x
        + ", y="
        + y
        + ", datasourceId="
        + datasourceId
        + ", inputConnectors="
        + inputConnectors
        + ", outputConnectors="
        + outputConnectors
        + ", refAppName="
        + refAppName
        + ", refNodeName="
        + refNodeName
        + ", description="
        + description
        + ", descritption="
        + descritption
        + "]";
  }
}

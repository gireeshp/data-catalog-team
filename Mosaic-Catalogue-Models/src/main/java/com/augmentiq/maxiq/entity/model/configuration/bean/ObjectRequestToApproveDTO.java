package com.augmentiq.maxiq.entity.model.configuration.bean;

public class ObjectRequestToApproveDTO {

  private Long id;
  private String objectName;
  private Long objectId;
  private String objectType;
  private String requestedBy;
  private String fromGroup;
  private String justification;
  private Long requestedDate;
  private String requestedJustification;
  private String rejectedJustification;
  private String requestedUser;
  private String category;
  private String subcategory;

  public ObjectRequestToApproveDTO() {
    super();
  }

  public ObjectRequestToApproveDTO(
      Long id,
      String objectName,
      Long objectId,
      String objectType,
      String requestedBy,
      String fromGroup,
      String justification,
      Long requestedDate,
      String requestedJustification,
      String rejectedJustification,
      String requestedUser,
      String category,
      String subcategory) {
    super();
    this.id = id;
    this.objectName = objectName;
    this.objectId = objectId;
    this.objectType = objectType;
    this.requestedBy = requestedBy;
    this.fromGroup = fromGroup;
    this.justification = justification;
    this.requestedDate = requestedDate;
    this.requestedJustification = requestedJustification;
    this.rejectedJustification = rejectedJustification;
    this.requestedUser = requestedUser;
    this.category = category;
    this.subcategory = subcategory;
  }

  public String getRequestedJustification() {
    return requestedJustification;
  }

  public void setRequestedJustification(String requestedJustification) {
    this.requestedJustification = requestedJustification;
  }

  public String getRejectedJustification() {
    return rejectedJustification;
  }

  public void setRejectedJustification(String rejectedJustification) {
    this.rejectedJustification = rejectedJustification;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getObjectName() {
    return objectName;
  }

  public void setObjectName(String objectName) {
    this.objectName = objectName;
  }

  public Long getObjectId() {
    return objectId;
  }

  public void setObjectId(Long objectId) {
    this.objectId = objectId;
  }

  public String getObjectType() {
    return this.objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  public String getRequestedBy() {
    return requestedBy;
  }

  public void setRequestedBy(String requestedBy) {
    this.requestedBy = requestedBy;
  }

  public String getFromGroup() {
    return fromGroup;
  }

  public void setFromGroup(String fromGroupId) {
    this.fromGroup = fromGroupId;
  }

  public String getJustification() {
    return justification;
  }

  public void setJustification(String justification) {
    this.justification = justification;
  }

  public Long getRequestedDate() {
    return requestedDate;
  }

  public void setRequestedDate(Long requestedDate) {
    this.requestedDate = requestedDate;
  }

  public String getRequestedUser() {
    return requestedUser;
  }

  public void setRequestedUser(String requestedUser) {
    this.requestedUser = requestedUser;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getSubcategory() {
    return subcategory;
  }

  public void setSubcategory(String subcategory) {
    this.subcategory = subcategory;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((category == null) ? 0 : category.hashCode());
    result = prime * result + ((fromGroup == null) ? 0 : fromGroup.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((justification == null) ? 0 : justification.hashCode());
    result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
    result = prime * result + ((objectName == null) ? 0 : objectName.hashCode());
    result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
    result =
        prime * result + ((rejectedJustification == null) ? 0 : rejectedJustification.hashCode());
    result = prime * result + ((requestedBy == null) ? 0 : requestedBy.hashCode());
    result = prime * result + ((requestedDate == null) ? 0 : requestedDate.hashCode());
    result = prime * result + ((requestedUser == null) ? 0 : requestedUser.hashCode());
    result =
        prime * result + ((requestedJustification == null) ? 0 : requestedJustification.hashCode());
    result = prime * result + ((subcategory == null) ? 0 : subcategory.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    ObjectRequestToApproveDTO other = (ObjectRequestToApproveDTO) obj;
    if (category == null) {
      if (other.category != null) return false;
    } else if (!category.equals(other.category)) return false;
    if (fromGroup == null) {
      if (other.fromGroup != null) return false;
    } else if (!fromGroup.equals(other.fromGroup)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (justification == null) {
      if (other.justification != null) return false;
    } else if (!justification.equals(other.justification)) return false;
    if (objectId == null) {
      if (other.objectId != null) return false;
    } else if (!objectId.equals(other.objectId)) return false;
    if (objectName == null) {
      if (other.objectName != null) return false;
    } else if (!objectName.equals(other.objectName)) return false;
    if (objectType == null) {
      if (other.objectType != null) return false;
    } else if (!objectType.equals(other.objectType)) return false;
    if (rejectedJustification == null) {
      if (other.rejectedJustification != null) return false;
    } else if (!rejectedJustification.equals(other.rejectedJustification)) return false;
    if (requestedBy == null) {
      if (other.requestedBy != null) return false;
    } else if (!requestedBy.equals(other.requestedBy)) return false;
    if (requestedDate == null) {
      if (other.requestedDate != null) return false;
    } else if (!requestedDate.equals(other.requestedDate)) return false;
    if (requestedUser == null) {
      if (other.requestedUser != null) return false;
    } else if (!requestedUser.equals(other.requestedUser)) return false;
    if (requestedJustification == null) {
      if (other.requestedJustification != null) return false;
    } else if (!requestedJustification.equals(other.requestedJustification)) return false;
    if (subcategory == null) {
      if (other.subcategory != null) return false;
    } else if (!subcategory.equals(other.subcategory)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "ObjectRequestToApproveDTO [id="
        + id
        + ", objectName="
        + objectName
        + ", objectId="
        + objectId
        + ", objectType="
        + objectType
        + ", requestedBy="
        + requestedBy
        + ", fromGroup="
        + fromGroup
        + ", justification="
        + justification
        + ", requestedDate="
        + requestedDate
        + ", requestedJustification="
        + requestedJustification
        + ", rejectedJustification="
        + rejectedJustification
        + ", requestedGroup="
        + requestedUser
        + ", category="
        + category
        + ", subcategory="
        + subcategory
        + "]";
  }
}

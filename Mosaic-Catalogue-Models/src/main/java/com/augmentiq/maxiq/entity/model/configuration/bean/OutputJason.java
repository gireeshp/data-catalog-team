package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.Arrays;

public class OutputJason {

  private String name;
  private OutputJason[] children;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OutputJason[] getChildren() {
    return children;
  }

  public void setChildren(OutputJason[] children) {
    this.children = children;
  }

  @Override
  public String toString() {
    return "OutputJason [name=" + name + ", children=" + Arrays.toString(children) + "]";
  }
}

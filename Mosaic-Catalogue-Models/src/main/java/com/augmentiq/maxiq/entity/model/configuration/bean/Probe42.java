package com.augmentiq.maxiq.entity.model.configuration.bean;

public class Probe42 {

  private String probe42ApiKey;
  private String companyDetails;
  private String companyBaseData;
  private String companyFinancials;
  private String companyCompliance;
  private String companyIndustrySegment;
  private String companyHoldingSubsidiary;
  private String companyCINNumber;
  private String connectionName;
  private String storagePath;

  public String getProbe42ApiKey() {
    return probe42ApiKey;
  }

  public void setProbe42ApiKey(String probe42ApiKey) {
    this.probe42ApiKey = probe42ApiKey;
  }

  public String getCompanyDetails() {
    return companyDetails;
  }

  public void setCompanyDetails(String companyDetails) {
    this.companyDetails = companyDetails;
  }

  public String getCompanyBaseData() {
    return companyBaseData;
  }

  public void setCompanyBaseData(String companyBaseData) {
    this.companyBaseData = companyBaseData;
  }

  public String getCompanyFinancials() {
    return companyFinancials;
  }

  public void setCompanyFinancials(String companyFinancials) {
    this.companyFinancials = companyFinancials;
  }

  public String getCompanyCompliance() {
    return companyCompliance;
  }

  public void setCompanyCompliance(String companyCompliance) {
    this.companyCompliance = companyCompliance;
  }

  public String getCompanyIndustrySegment() {
    return companyIndustrySegment;
  }

  public void setCompanyIndustrySegment(String companyIndustrySegment) {
    this.companyIndustrySegment = companyIndustrySegment;
  }

  public String getCompanyHoldingSubsidiary() {
    return companyHoldingSubsidiary;
  }

  public void setCompanyHoldingSubsidiary(String companyHoldingSubsidiary) {
    this.companyHoldingSubsidiary = companyHoldingSubsidiary;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  public String getStoragePath() {
    return storagePath;
  }

  public void setStoragePath(String storagePath) {
    this.storagePath = storagePath;
  }

  public String getCompanyCINNumber() {
    return companyCINNumber;
  }

  public void setCompanyCINNumber(String companyCINNumber) {
    this.companyCINNumber = companyCINNumber;
  }

  @Override
  public String toString() {
    return "Probe42 [probe42ApiKey="
        + probe42ApiKey
        + ", companyDetails="
        + companyDetails
        + ", companyBaseData="
        + companyBaseData
        + ", companyFinancials="
        + companyFinancials
        + ", companyCompliance="
        + companyCompliance
        + ", companyIndustrySegment="
        + companyIndustrySegment
        + ", companyHoldingSubsidiary="
        + companyHoldingSubsidiary
        + ", connectionName="
        + connectionName
        + ", storagePath="
        + storagePath
        + ", companyCINNumber="
        + companyCINNumber
        + "]";
  }
}

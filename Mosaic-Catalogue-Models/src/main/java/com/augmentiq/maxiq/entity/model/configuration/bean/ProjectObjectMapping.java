package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * CREATE TABLE `maxiq`.`project_object_mapping` ( `id` int(11) NOT NULL AUTO_INCREMENT, `projectId`
 * int(11) NOT NULL, `objectId` int(11) NOT NULL, `objectType` varchar(100) NOT NULL, PRIMARY KEY
 * (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
 */
/** @author Rushi created on Jul 10, 2017 for MAX-1058 */
@TableName(tableName = Sequences.PROJECT_OBJECT_MAPPING)
public class ProjectObjectMapping {

  @Id private Long id;
  private Long projectId;
  private Long objectId;
  private String objectType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public Long getObjectId() {
    return objectId;
  }

  public void setObjectId(Long objectId) {
    this.objectId = objectId;
  }

  public String getObjectType() {
    return objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  @Override
  public String toString() {
    return "ProjectObjectMapping [id="
        + id
        + ", projectId="
        + projectId
        + ", objectId="
        + objectId
        + ", objectType="
        + objectType
        + "]";
  }
}

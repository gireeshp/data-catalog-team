package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * @author Mayuri on 13-June-2017 CREATE TABLE `projects` ( `id` int(11) NOT NULL AUTO_INCREMENT,
 *     `projectName` varchar(200) NOT NULL, `description` varchar(256) DEFAULT '', `type`
 *     varchar(50) NOT NULL, `createdDate` varchar(50) NOT NULL, `modifiedDate` varchar(50) DEFAULT
 *     NULL, `createdBy` varchar(20) NOT NULL, `modifiedBy` varchar(20) DEFAULT NULL, `imageIcon`
 *     blob, `status` varchar(45) DEFAULT NULL, PRIMARY KEY (`id`) );
 *     <p>Alter table `projects` drop `groupId`; Alter table `projects` drop `dataSourceCount`;
 *     Alter table `projects` drop `indexCount`; Alter table `projects` drop `analyticsCount`; Alter
 *     table `projects` drop `flowsCount`; Alter table `projects` drop `connectionsCount`; Alter
 *     table `projects` drop `customComponentsCount`; Alter table `projects` drop `repositoryCount`;
 */
@TableName(tableName = Sequences.PROJECTS)
public class ProjectsBean implements Serializable {

  @Id private Long id;
  private String projectName;
  private String description;
  private String type;
  private String createdDate;
  private String modifiedDate;
  private String createdBy;
  private String modifiedBy;
  private String imageIcon;
  private String status;

  public ProjectsBean(ProjectsBean projectBean) {
    this.id = projectBean.getId();
    this.projectName = projectBean.getProjectName();
    this.description = projectBean.getDescription();
    this.type = projectBean.getType();
    this.createdDate = projectBean.getCreatedDate();
    this.modifiedDate = projectBean.getModifiedDate();
    this.createdBy = projectBean.getCreatedBy();
    this.modifiedBy = projectBean.getModifiedBy();
    this.imageIcon = projectBean.getImageIcon();
    this.status = projectBean.getStatus();
  }

  public ProjectsBean() {
    super();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }

  public String getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(String modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public String getImageIcon() {
    return imageIcon;
  }

  public void setImageIcon(String imageIcon) {
    this.imageIcon = imageIcon;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((imageIcon == null) ? 0 : imageIcon.hashCode());
    result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
    result = prime * result + ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
    result = prime * result + ((projectName == null) ? 0 : projectName.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    ProjectsBean other = (ProjectsBean) obj;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdDate == null) {
      if (other.createdDate != null) return false;
    } else if (!createdDate.equals(other.createdDate)) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (imageIcon == null) {
      if (other.imageIcon != null) return false;
    } else if (!imageIcon.equals(other.imageIcon)) return false;
    if (modifiedBy == null) {
      if (other.modifiedBy != null) return false;
    } else if (!modifiedBy.equals(other.modifiedBy)) return false;
    if (modifiedDate == null) {
      if (other.modifiedDate != null) return false;
    } else if (!modifiedDate.equals(other.modifiedDate)) return false;
    if (projectName == null) {
      if (other.projectName != null) return false;
    } else if (!projectName.equals(other.projectName)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    if (type == null) {
      if (other.type != null) return false;
    } else if (!type.equals(other.type)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "ProjectsBean [id="
        + id
        + ", projectName="
        + projectName
        + ", description="
        + description
        + ", type="
        + type
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", createdBy="
        + createdBy
        + ", modifiedBy="
        + modifiedBy
        + ", imageIcon="
        + imageIcon
        + ", status="
        + status
        + "]";
  }
}

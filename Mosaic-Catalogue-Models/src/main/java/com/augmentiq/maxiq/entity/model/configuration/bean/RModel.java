package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.MACHINE_LEARNING_MODELS)
public class RModel implements Serializable {

  private String modelName;
  @Id private Long modelId;
  private String modelLocation;
  private String description;
  private Long groupId;
  private Long createdBy;
  private Long createdDate;
  private Long modifiedDate;
  private Long modifiedBy;
  private Long ownerProjectId;

  private String modelType;
  private String apiKey;
  private Integer apiAccessStatus;
  private RModelData data;
  private Long appId;
  private Long nodeId;
  private Long jobInstanceId;
  private String modelPackageName;

  public RModel() {
    super();
  }

  public RModel(
      Long modelId,
      String modelName,
      String modelLocation,
      String description,
      Long groupId,
      Long createdBy,
      Long createdDate,
      Long modifiedDate,
      Long modifiedBy,
      String modelType,
      String apiKey,
      Integer apiAccessStatus,
      RModelData data,
      Long ownerProjectId,
      Long appId,
      Long nodeId,
      Long jobInstanceId,
      String modelPackageName) {
    this.modelId = modelId;
    this.modelName = modelName;
    this.modelLocation = modelLocation;
    this.description = description;
    this.groupId = groupId;
    this.createdBy = createdBy;
    this.createdDate = createdDate;
    this.modifiedDate = modifiedDate;
    this.modifiedBy = modifiedBy;
    this.ownerProjectId = ownerProjectId;
    this.modelType = modelType;
    this.apiKey = apiKey;
    this.apiAccessStatus = apiAccessStatus;
    this.data = data;
    this.appId = appId;
    this.nodeId = nodeId;
    this.jobInstanceId = jobInstanceId;
    this.modelPackageName = modelPackageName;
  }

  public RModel(
      Long modelId2,
      String modelName2,
      String modelLocation2,
      String description2,
      Long groupId2,
      Long createdBy2,
      Long createdDate2,
      Long modifiedDate2,
      Long modifiedBy2,
      String modelType2,
      String apiKey2,
      Integer apiAccessStatus2,
      RModelData data2,
      Long appId2,
      Long nodeId2,
      Long jobInstanceId2,
      String modelPackageName2) {
    // TODO Auto-generated constructor stub
    this.modelId = modelId2;
    this.modelName = modelName2;
    this.modelLocation = modelLocation2;
    this.description = description2;
    this.groupId = groupId2;
    this.createdBy = createdBy2;
    this.createdDate = createdDate2;
    this.modifiedDate = modifiedDate2;
    this.modifiedBy = modifiedBy2;
    this.modelType = modelType2;
    this.apiKey = apiKey2;
    this.apiAccessStatus = apiAccessStatus2;
    this.data = data2;
    this.appId = appId2;
    this.nodeId = nodeId2;
    this.jobInstanceId = jobInstanceId2;
    this.modelPackageName = modelPackageName2;
  }

  public Long getModelId() {
    return modelId;
  }

  public void setModelId(Long modelId) {
    this.modelId = modelId;
  }

  public String getModelName() {
    return modelName;
  }

  public void setModelName(String name) {
    this.modelName = name;
  }

  public String getModelLocation() {
    return modelLocation;
  }

  public void setModelLocation(String location) {
    this.modelLocation = location;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Long getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
  }

  public Long getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Long modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  public String getModelType() {
    return modelType;
  }

  public void setModelType(String modelType) {
    this.modelType = modelType;
  }

  public RModelData getData() {
    return data;
  }

  public void setData(RModelData data) {
    this.data = data;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public Integer getApiAccessStatus() {
    return apiAccessStatus;
  }

  public void setApiAccessStatus(Integer apiAccessStatus) {
    this.apiAccessStatus = apiAccessStatus;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public Long getNodeId() {
    return nodeId;
  }

  public void setNodeId(Long nodeId) {
    this.nodeId = nodeId;
  }

  public Long getJobInstanceId() {
    return jobInstanceId;
  }

  public void setJobInstanceId(Long jobInstanceId) {
    this.jobInstanceId = jobInstanceId;
  }

  public String getModelPackageName() {
    return modelPackageName;
  }

  public void setModelPackageName(String modelPackageName) {
    this.modelPackageName = modelPackageName;
  }

  @Override
  public String toString() {
    return "RModel [modelId="
        + modelId
        + ", modelName="
        + modelName
        + ", modelLocation="
        + modelLocation
        + ", description="
        + description
        + ", groupId="
        + groupId
        + ", createdBy="
        + createdBy
        + ", createdDate="
        + createdDate
        + ", modifiedDate="
        + modifiedDate
        + ", modifiedBy="
        + modifiedBy
        + ", ownerProjectId="
        + ownerProjectId
        + ", modelType="
        + modelType
        + ", apiKey="
        + apiKey
        + ", apiAccessStatus="
        + apiAccessStatus
        + ", data="
        + data
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Map;

public class RModelData implements Serializable {

  /** */
  private static final long serialVersionUID = 1L;

  private String classOfModel;
  private String displayName;
  private ModelParametersInfo fieldMappings;
  private String numberOfFeatures;
  private String modelAccuracy;
  private String modelSummary;
  private String targetColumn;
  private String targetColumnDatatype;
  private Map<String, Object> featureImportance;

  public RModelData(
      String classOfModel,
      String displayName,
      ModelParametersInfo fieldMappings,
      String numberOfFeatures,
      String modelAccuracy,
      String modelSummary,
      String targetColumn,
      String targetColumnDatatype,
      Map<String, Object> featureImportance) {
    super();
    this.classOfModel = classOfModel;
    this.displayName = displayName;
    this.fieldMappings = fieldMappings;
    this.numberOfFeatures = numberOfFeatures;
    this.modelAccuracy = modelAccuracy;
    this.modelSummary = modelSummary;
    this.targetColumn = targetColumn;
    this.targetColumnDatatype = targetColumnDatatype;
    this.featureImportance = featureImportance;
  }

  public RModelData() {}

  public RModelData(
      String classOfModel,
      String displayName,
      ModelParametersInfo fieldMappings,
      String numberOfFeatures) {
    this.classOfModel = classOfModel;
    this.displayName = displayName;
    this.fieldMappings = fieldMappings;
    this.numberOfFeatures = numberOfFeatures;
  }

  public String getClassOfModel() {
    return classOfModel;
  }

  public void setClassOfModel(String classOfModel) {
    this.classOfModel = classOfModel;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public ModelParametersInfo getFieldMappings() {
    return fieldMappings;
  }

  public void setFieldMappings(ModelParametersInfo fieldMappings) {
    this.fieldMappings = fieldMappings;
  }

  public String getNumberOfFeatures() {
    return numberOfFeatures;
  }

  public void setNumberOfFeatures(String numberOfFeatures) {
    this.numberOfFeatures = numberOfFeatures;
  }

  public String getModelAccuracy() {
    return modelAccuracy;
  }

  public void setModelAccuracy(String modelAccuracy) {
    this.modelAccuracy = modelAccuracy;
  }

  public String getTargetColumn() {
    return targetColumn;
  }

  public void setTargetColumn(String targetColumn) {
    this.targetColumn = targetColumn;
  }

  public String getTargetColumnDatatype() {
    return targetColumnDatatype;
  }

  public void setTargetColumnDatatype(String targetColumnDatatype) {
    this.targetColumnDatatype = targetColumnDatatype;
  }

  public Map<String, Object> getFeatureImportance() {
    return featureImportance;
  }

  public void setFeatureImportance(Map<String, Object> featureImportance) {
    this.featureImportance = featureImportance;
  }

  public String getModelSummary() {
    return modelSummary;
  }

  public void setModelSummary(String modelSummary) {
    this.modelSummary = modelSummary;
  }

  @Override
  public String toString() {
    return "RModelData [classOfModel="
        + classOfModel
        + ", displayName="
        + displayName
        + ", fieldMappings="
        + fieldMappings
        + ", numberOfFeatures="
        + numberOfFeatures
        + ", modelAccuracy="
        + modelAccuracy
        + ", modelSummary="
        + modelSummary
        + ", targetColumn="
        + targetColumn
        + ", targetColumnDatatype="
        + targetColumnDatatype
        + ", featureImportance="
        + featureImportance
        + "]";
  }
}

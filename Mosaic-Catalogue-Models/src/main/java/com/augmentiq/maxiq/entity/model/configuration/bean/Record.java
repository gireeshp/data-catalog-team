package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

public class Record {
  private Long id_;
  private String name_;
  private SearchInType searchInTypes_;
  private List<String> matchingNodes_;
  private String createdBy_;
  private String createdDate_;
  private String category_;
  private String subcategory_;

  public Record(
      Long id,
      String name,
      SearchInType searchInTypes,
      List<String> matchingNodes,
      String createdBy) {
    super();
    id_ = id;
    name_ = name;
    searchInTypes_ = searchInTypes;
    matchingNodes_ = matchingNodes;
    createdBy_ = createdBy;
  }

  public Record(
      Long id, String name, List<String> matchingNodes, String createdBy, String createdDate) {
    super();
    id_ = id;
    name_ = name;
    matchingNodes_ = matchingNodes;
    createdBy_ = createdBy;
    createdDate_ = createdDate;
  }

  public Long getId() {
    return id_;
  }

  public void setId(Long id) {
    id_ = id;
  }

  public String getName() {
    return name_;
  }

  public void setName(String name) {
    name_ = name;
  }

  public SearchInType getSearchInTypes() {
    return searchInTypes_;
  }

  public void setSearchInTypes(SearchInType searchInTypes) {
    searchInTypes_ = searchInTypes;
  }

  public List<String> getMatchingNodes() {
    return matchingNodes_;
  }

  public void setMatchingNodes(List<String> matchingNodes) {
    matchingNodes_ = matchingNodes;
  }

  public String getCreatedBy() {
    return createdBy_;
  }

  public void setCreatedBy(String createdBy) {
    createdBy_ = createdBy;
  }

  public String getCreatedDate() {
    return createdDate_;
  }

  public void setCreatedDate_(String createdDate) {
    this.createdDate_ = createdDate;
  }

  public String getCategory() {
    return category_;
  }

  public void setCategory(String category) {
    this.category_ = category;
  }

  public String getSubcategory() {
    return subcategory_;
  }

  public void setSubcategory(String subcategory) {
    this.subcategory_ = subcategory;
  }

  @Override
  public String toString() {
    return "Record [id_="
        + id_
        + ", name_="
        + name_
        + ", searchInTypes_="
        + searchInTypes_
        + ", matchingNodes_="
        + matchingNodes_
        + ", createdBy_="
        + createdBy_
        + ", createdDate_="
        + createdDate_
        + ", category_="
        + category_
        + ", subcategory_="
        + subcategory_
        + "]";
  }
}

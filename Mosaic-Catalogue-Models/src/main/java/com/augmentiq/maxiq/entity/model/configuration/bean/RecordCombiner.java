package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;

/**
 * This will keep the information needed for Combining records
 *
 * @author shiva
 */
public class RecordCombiner {

  private Long leftDsId;
  private Long rightDsId;
  private List<MappingKeys> mappingKeys;
  private String queryString;

  public Long getLeftDsId() {
    return leftDsId;
  }

  public void setLeftDsId(Long leftDsId) {
    this.leftDsId = leftDsId;
  }

  public Long getRightDsId() {
    return rightDsId;
  }

  public void setRightDsId(Long rightDsId) {
    this.rightDsId = rightDsId;
  }

  public List<MappingKeys> getMappingKeys() {
    return mappingKeys;
  }

  public void setMappingKeys(List<MappingKeys> mappingKeys) {
    this.mappingKeys = mappingKeys;
  }

  public String getQueryString() {
    return queryString;
  }

  public void setQueryString(String queryString) {
    this.queryString = queryString;
  }

  public RecordCombiner() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "RecordCombiner [leftDsId="
        + leftDsId
        + ", rightDsId="
        + rightDsId
        + ", mappingKeys="
        + mappingKeys
        + ", queryString="
        + queryString
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((leftDsId == null) ? 0 : leftDsId.hashCode());
    result = prime * result + ((mappingKeys == null) ? 0 : mappingKeys.hashCode());
    result = prime * result + ((queryString == null) ? 0 : queryString.hashCode());
    result = prime * result + ((rightDsId == null) ? 0 : rightDsId.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    RecordCombiner other = (RecordCombiner) obj;
    if (leftDsId == null) {
      if (other.leftDsId != null) return false;
    } else if (!leftDsId.equals(other.leftDsId)) return false;
    if (mappingKeys == null) {
      if (other.mappingKeys != null) return false;
    } else if (!mappingKeys.equals(other.mappingKeys)) return false;
    if (queryString == null) {
      if (other.queryString != null) return false;
    } else if (!queryString.equals(other.queryString)) return false;
    if (rightDsId == null) {
      if (other.rightDsId != null) return false;
    } else if (!rightDsId.equals(other.rightDsId)) return false;
    return true;
  }
}

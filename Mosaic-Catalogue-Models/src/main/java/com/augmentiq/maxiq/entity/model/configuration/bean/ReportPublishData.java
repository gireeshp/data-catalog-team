package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
import java.util.Map;

public class ReportPublishData {

	private String reportName;
	private String packageName;
	private String searchPath;
//	private Map<String, List<String>> tables;
	private List<Join> joins;
	private Map<String, List<CognosReportPublishColumn>> tables;
	
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getSearchPath() {
		return searchPath;
	}
	public void setSearchPath(String searchPath) {
		this.searchPath = searchPath;
	}
	/*public Map<String, List<String>> getTables() {
		return tables;
	}
	public void setTables(Map<String, List<String>> tables) {
		this.tables = tables;
	}*/
	public List<Join> getJoins() {
		return joins;
	}
	public void setJoins(List<Join> joins) {
		this.joins = joins;
	}
	public Map<String, List<CognosReportPublishColumn>> getTables() {
		return tables;
	}
	public void setTables(Map<String, List<CognosReportPublishColumn>> tables) {
		this.tables = tables;
	}
	
	
}

package com.augmentiq.maxiq.entity.model.configuration.bean;
public class S3BucketInfo {
  private String name_;
  private String type_;
  private String storage_;
  private String size_;
  private String lastModified_;
  private String displayName_;

  public S3BucketInfo(String name_) {
    super();
    this.name_ = name_;
  }

  public S3BucketInfo() {}

  public String getDisplayName_() {
    return displayName_;
  }

  public void setDisplayName_(String displayName_) {
    this.displayName_ = displayName_;
  }

  public String getName_() {
    return name_;
  }

  public void setName_(String name_) {
    this.name_ = name_;
  }

  public String getType_() {
    return type_;
  }

  public void setType_(String type_) {
    this.type_ = type_;
  }

  public String getStorage_() {
    return storage_;
  }

  public void setStorage_(String storage_) {
    this.storage_ = storage_;
  }

  public String getSize_() {
    return size_;
  }

  public void setSize_(String size_) {
    this.size_ = size_;
  }

  public String getLastModified_() {
    return lastModified_;
  }

  public void setLastModified_(String lastModified_) {
    this.lastModified_ = lastModified_;
  }
}

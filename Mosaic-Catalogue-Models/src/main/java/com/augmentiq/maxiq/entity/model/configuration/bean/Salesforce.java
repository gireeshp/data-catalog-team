package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;

public class Salesforce implements Serializable {

  private String clientId;
  private String secretId;
  private String userName;
  private String passwordSecurityToken;
  private List<SalesforceCollection> salesforceCollection;
  private String connectionName;
  private int userToggleNumber;

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getPasswordSecurityToken() {
    return passwordSecurityToken;
  }

  public void setPasswordSecurityToken(String passwordSecurityToken) {
    this.passwordSecurityToken = passwordSecurityToken;
  }

  public String getSecretId() {
    return secretId;
  }

  public void setSecretId(String secretId) {
    this.secretId = secretId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public List<SalesforceCollection> getSalesforceCollection() {
    return salesforceCollection;
  }

  public void setSalesforceCollection(List<SalesforceCollection> salesforceCollection) {
    this.salesforceCollection = salesforceCollection;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  public int getUserToggleNumber() {
    return userToggleNumber;
  }

  public void setUserToggleNumber(int userToggleNumber) {
    this.userToggleNumber = userToggleNumber;
  }

  @Override
  public String toString() {
    return "Salesforce [clientId="
        + clientId
        + ", secretId="
        + secretId
        + " , userName="
        + userName
        + ", passwordSecurityToken="
        + passwordSecurityToken
        + ", salesforceCollection="
        + salesforceCollection
        + ", connectionName="
        + connectionName
        + ", userToggleNumber= "
        + userToggleNumber
        + "]";
  }
}

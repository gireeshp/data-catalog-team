package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
public class SalesforceCollection {

  String name;
  List<String> field;
  List<Boolean> fieldChk;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getField() {
    return field;
  }

  public void setField(List<String> field) {
    this.field = field;
  }

  public List<Boolean> getFieldChk() {
    return fieldChk;
  }

  public void setFieldChk(List<Boolean> fieldChk) {
    this.fieldChk = fieldChk;
  }

  @Override
  public String toString() {
    return "SalesforceCollection [ name="
        + name
        + ", field="
        + field
        + ", fieldChk="
        + fieldChk
        + "]";
  }
}

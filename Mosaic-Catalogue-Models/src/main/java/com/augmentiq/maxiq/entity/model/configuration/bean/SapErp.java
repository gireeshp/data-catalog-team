package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.util.List;
public class SapErp {

  private String hostName;
  private String instanceNo;
  private String client;
  private String user;
  private String password;
  private String language;
  private String connectionName;
  private String storagePath;
  private String functionModule;
  private String tableName;
  private String rowCount;
  private boolean selectColumn;
  private List<String> columnName;
  private String dataFilter;
  private String noOfSplit;
  private String splitRowCount;

  public String getHostName() {
    return hostName;
  }

  public void setHostName(String hostName) {
    this.hostName = hostName;
  }

  public String getInstanceNo() {
    return instanceNo;
  }

  public void setInstanceNo(String instanceNo) {
    this.instanceNo = instanceNo;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  public String getStoragePath() {
    return storagePath;
  }

  public void setStoragePath(String storagePath) {
    this.storagePath = storagePath;
  }

  public String getFunctionModule() {
    return functionModule;
  }

  public void setFunctionModule(String functionModule) {
    this.functionModule = functionModule;
  }

  public String getTableName() {
    return tableName;
  }

  public void setTableName(String tableName) {
    this.tableName = tableName;
  }

  public String getRowCount() {
    return rowCount;
  }

  public void setRowCount(String rowCount) {
    this.rowCount = rowCount;
  }

  public boolean isSelectColumn() {
    return selectColumn;
  }

  public void setSelectColumn(boolean selectColumn) {
    this.selectColumn = selectColumn;
  }

  public List<String> getColumnName() {
    return columnName;
  }

  public void setColumnName(List<String> columnName) {
    this.columnName = columnName;
  }

  public String getDataFilter() {
    return dataFilter;
  }

  public void setDataFilter(String dataFilter) {
    this.dataFilter = dataFilter;
  }

  public String getNoOfSplit() {
    return noOfSplit;
  }

  public void setNoOfSplit(String noOfSplit) {
    this.noOfSplit = noOfSplit;
  }

  public String getSplitRowCount() {
    return splitRowCount;
  }

  public void setSplitRowCount(String splitRowCount) {
    this.splitRowCount = splitRowCount;
  }

  @Override
  public String toString() {
    return "SapErp [hostName="
        + hostName
        + ", instanceNo="
        + instanceNo
        + ", client="
        + client
        + ", user="
        + user
        + ", password="
        + password
        + ", language="
        + language
        + ", connectionName="
        + connectionName
        + ", storagePath="
        + storagePath
        + ", functionModule="
        + functionModule
        + ", tableName="
        + tableName
        + ", rowCount="
        + rowCount
        + ", selectColumn="
        + selectColumn
        + ", columnName="
        + columnName
        + ", dataFilter="
        + dataFilter
        + ", noOfSplit="
        + noOfSplit
        + ", splitRowCount="
        + splitRowCount
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.SCHEDULED_APPLICATION_DETAILS)
public class ScheduleApplication implements Serializable {

  @Id private Long id;
  private Long appId;
  private String createdBy;
  private String cronExpression;
  private String oozieJobId;
  private Long groupId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getCronExpression() {
    return cronExpression;
  }

  public void setCronExpression(String cronExpression) {
    this.cronExpression = cronExpression;
  }

  public String getOozieJobId() {
    return oozieJobId;
  }

  public void setOozieJobId(String oozieJobId) {
    this.oozieJobId = oozieJobId;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  @Override
  public String toString() {
    return "ScheduleApplication [id="
        + id
        + ", appId="
        + appId
        + ", createdBy="
        + createdBy
        + ", cronExpression="
        + cronExpression
        + ", oozieJobId="
        + oozieJobId
        + ", groupId="
        + groupId
        + "]";
  }
}

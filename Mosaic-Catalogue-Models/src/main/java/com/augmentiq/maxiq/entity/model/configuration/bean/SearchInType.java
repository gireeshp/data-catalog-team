package com.augmentiq.maxiq.entity.model.configuration.bean;

public enum SearchInType {
  DATASOURCE("DATASOURCE"),
  FLOW("FLOW");

  private String searchTypes;

  public String getSearchTypes() {
    return searchTypes;
  }

  private SearchInType(String searchTypes) {
    this.searchTypes = searchTypes;
  }
}

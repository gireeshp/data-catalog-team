package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * CREATE TABLE `share_access_history` ( `shareaccHist_Id` int(11) NOT NULL AUTO_INCREMENT,
 * `assetId` int(11) NOT NULL, `assetType` varchar(50) NOT NULL, `groupId` varchar(20) DEFAULT NULL,
 * `userId` varchar(20) DEFAULT NULL, `access` varchar(50) NOT NULL, `accessType` varchar(50) NOT
 * NULL, `createdOn` datetime NOT NULL, PRIMARY KEY (`shareaccHist_Id`) ) ENGINE=InnoDB
 * AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
 */
/** @author anuj MAX-1058 */
@TableName(tableName = Sequences.SHARE_ACCESS_HISTORY)
public class ShareAccessHistory {

  @Id private Long shareaccHist_Id;
  private Long groupId;
  private Long assetId;
  private String assetType;
  private Long userId;
  private String access;
  private String accessType;
  private String createdOn;

  public String getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(String createdOn) {
    this.createdOn = createdOn;
  }

  public Long getShareaccHist_Id() {
    return shareaccHist_Id;
  }

  public void setShareaccHist_Id(Long shareaccHist_Id) {
    this.shareaccHist_Id = shareaccHist_Id;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getAccess() {
    return access;
  }

  public void setAccess(String access) {
    this.access = access;
  }

  public String getAccessType() {
    return accessType;
  }

  public void setAccessType(String accessType) {
    this.accessType = accessType;
  }

  public Long getAssetId() {
    return assetId;
  }

  public void setAssetId(Long assetId) {
    this.assetId = assetId;
  }

  public String getAssetType() {
    return assetType;
  }

  public void setAssetType(String assetType) {
    this.assetType = assetType;
  }
}

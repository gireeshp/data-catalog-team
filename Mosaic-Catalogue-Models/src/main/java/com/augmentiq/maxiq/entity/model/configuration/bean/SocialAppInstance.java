package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.DataStatus;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/** Each Social App instance save information about social media */
@TableName(tableName = Sequences.SOCIAL_APP_INSTANCE)
public class SocialAppInstance {
  @Id private Long id;
  private String name;
  private String type;
  private String appId;
  private String secretKey;
  private String owner;
  private Long createdDate;
  private Long updatedDate;
  private DataStatus status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public Long getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
  }

  public Long getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
  }

  public DataStatus getStatus() {
    return status;
  }

  public void setStatus(DataStatus status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "SocialAppInstance [id="
        + id
        + ", name="
        + name
        + ", type="
        + type
        + ", appId="
        + appId
        + ", secretKey="
        + secretKey
        + ", owner="
        + owner
        + ", createdDate="
        + createdDate
        + ", updatedDate="
        + updatedDate
        + ", status="
        + status
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((owner == null) ? 0 : owner.hashCode());
    result = prime * result + ((secretKey == null) ? 0 : secretKey.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    SocialAppInstance other = (SocialAppInstance) obj;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (createdDate == null) {
      if (other.createdDate != null) return false;
    } else if (!createdDate.equals(other.createdDate)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (name == null) {
      if (other.name != null) return false;
    } else if (!name.equals(other.name)) return false;
    if (owner == null) {
      if (other.owner != null) return false;
    } else if (!owner.equals(other.owner)) return false;
    if (secretKey == null) {
      if (other.secretKey != null) return false;
    } else if (!secretKey.equals(other.secretKey)) return false;
    if (status != other.status) return false;
    if (type == null) {
      if (other.type != null) return false;
    } else if (!type.equals(other.type)) return false;
    if (updatedDate == null) {
      if (other.updatedDate != null) return false;
    } else if (!updatedDate.equals(other.updatedDate)) return false;
    return true;
  }
}

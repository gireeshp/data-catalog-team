package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class SolutionDetails implements Serializable {

  private String solution_desc;
  private List<String> inputDs;
  private List<String> processing;
  private List<String> downstreamConsumption;
  private String demoUrl;
  private String catlogUrl;
  private byte[] imageInByte;
  private String zeppelinId;

  public SolutionDetails(
      String solution_desc,
      List<String> inputDs,
      List<String> processing,
      List<String> downstreamConsumption,
      String demoUrl,
      byte[] imageInByte,
      String zeppelinId) {
    super();
    this.solution_desc = solution_desc;
    this.inputDs = inputDs;
    this.processing = processing;
    this.downstreamConsumption = downstreamConsumption;
    this.demoUrl = demoUrl;
    this.imageInByte = imageInByte;
    this.zeppelinId = zeppelinId;
  }

  public String getZeppelinId() {
    return zeppelinId;
  }

  public void setZeppelinId(String zeppelinId) {
    this.zeppelinId = zeppelinId;
  }

  public byte[] getImageInByte() {
    return imageInByte;
  }

  public void setImageInByte(byte[] imageInByte) {
    this.imageInByte = imageInByte;
  }

  public String getSolution_desc() {
    return solution_desc;
  }

  public void setSolution_desc(String solution_desc) {
    this.solution_desc = solution_desc;
  }

  public List<String> getInputDs() {
    return inputDs;
  }

  public void setInputDs(List<String> inputDs) {
    this.inputDs = inputDs;
  }

  public List<String> getProcessing() {
    return processing;
  }

  public void setProcessing(List<String> processing) {
    this.processing = processing;
  }

  public List<String> getDownstreamConsumption() {
    return downstreamConsumption;
  }

  public void setDownstreamConsumption(List<String> downstreamConsumption) {
    this.downstreamConsumption = downstreamConsumption;
  }

  public String getDemoUrl() {
    return demoUrl;
  }

  public void setDemoUrl(String demoUrl) {
    this.demoUrl = demoUrl;
  }

  public String getCatlogUrl() {
    return catlogUrl;
  }

  public void setCatlogUrl(String catlogUrl) {
    this.catlogUrl = catlogUrl;
  }

  @Override
  public String toString() {
    return "SolutionDetails [solution_desc="
        + solution_desc
        + ", inputDs="
        + inputDs
        + ", processing="
        + processing
        + ", downstreamConsumption="
        + downstreamConsumption
        + ", demoUrl="
        + demoUrl
        + ", catlogUrl="
        + catlogUrl
        + ", imageInByte="
        + Arrays.toString(imageInByte)
        + ", zeppelinId="
        + zeppelinId
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((catlogUrl == null) ? 0 : catlogUrl.hashCode());
    result = prime * result + ((demoUrl == null) ? 0 : demoUrl.hashCode());
    result =
        prime * result + ((downstreamConsumption == null) ? 0 : downstreamConsumption.hashCode());
    result = prime * result + Arrays.hashCode(imageInByte);
    result = prime * result + ((inputDs == null) ? 0 : inputDs.hashCode());
    result = prime * result + ((processing == null) ? 0 : processing.hashCode());
    result = prime * result + ((solution_desc == null) ? 0 : solution_desc.hashCode());
    result = prime * result + ((zeppelinId == null) ? 0 : zeppelinId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    SolutionDetails other = (SolutionDetails) obj;
    if (catlogUrl == null) {
      if (other.catlogUrl != null) return false;
    } else if (!catlogUrl.equals(other.catlogUrl)) return false;
    if (demoUrl == null) {
      if (other.demoUrl != null) return false;
    } else if (!demoUrl.equals(other.demoUrl)) return false;
    if (downstreamConsumption == null) {
      if (other.downstreamConsumption != null) return false;
    } else if (!downstreamConsumption.equals(other.downstreamConsumption)) return false;
    if (!Arrays.equals(imageInByte, other.imageInByte)) return false;
    if (inputDs == null) {
      if (other.inputDs != null) return false;
    } else if (!inputDs.equals(other.inputDs)) return false;
    if (processing == null) {
      if (other.processing != null) return false;
    } else if (!processing.equals(other.processing)) return false;
    if (solution_desc == null) {
      if (other.solution_desc != null) return false;
    } else if (!solution_desc.equals(other.solution_desc)) return false;
    if (zeppelinId == null) {
      if (other.zeppelinId != null) return false;
    } else if (!zeppelinId.equals(other.zeppelinId)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/*
 * @author Mayuri Narawade on 4/1/2017
 * create table solution_objects(solution_obj_id int(20), solution_id int(20), mp_id int(50), object_type varchar(20), object_id varchar(40));*/

@TableName(tableName = Sequences.SOLUTION_OBJECTS)
public class SolutionObjectsBean implements Serializable {
  @Id private Long id = 0L;
  private String solution_id;
  private Long mp_id;
  private String object_type;
  private String object_id;
  private String installedBy;
  private Long installedDate;

  public String getInstalledBy() {
    return installedBy;
  }

  public void setInstalledBy(String installedBy) {
    this.installedBy = installedBy;
  }

  public Long getInstalledDate() {
    return installedDate;
  }

  public void setInstalledDate(Long installedDate) {
    this.installedDate = installedDate;
  }

  public String getSolution_id() {
    return solution_id;
  }

  public void setSolution_id(String solution_id) {
    this.solution_id = solution_id;
  }

  public Long getMp_id() {
    return mp_id;
  }

  public void setMp_id(Long mp_id) {
    this.mp_id = mp_id;
  }

  public String getObject_type() {
    return object_type;
  }

  public void setObject_type(String object_type) {
    this.object_type = object_type;
  }

  public String getObject_id() {
    return object_id;
  }

  public void setObject_id(String object_id) {
    this.object_id = object_id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((installedBy == null) ? 0 : installedBy.hashCode());
    result = prime * result + ((installedDate == null) ? 0 : installedDate.hashCode());
    result = prime * result + ((mp_id == null) ? 0 : mp_id.hashCode());
    result = prime * result + ((object_id == null) ? 0 : object_id.hashCode());
    result = prime * result + ((object_type == null) ? 0 : object_type.hashCode());
    result = prime * result + ((solution_id == null) ? 0 : solution_id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    SolutionObjectsBean other = (SolutionObjectsBean) obj;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (installedBy == null) {
      if (other.installedBy != null) return false;
    } else if (!installedBy.equals(other.installedBy)) return false;
    if (installedDate == null) {
      if (other.installedDate != null) return false;
    } else if (!installedDate.equals(other.installedDate)) return false;
    if (mp_id == null) {
      if (other.mp_id != null) return false;
    } else if (!mp_id.equals(other.mp_id)) return false;
    if (object_id == null) {
      if (other.object_id != null) return false;
    } else if (!object_id.equals(other.object_id)) return false;
    if (object_type == null) {
      if (other.object_type != null) return false;
    } else if (!object_type.equals(other.object_type)) return false;
    if (solution_id == null) {
      if (other.solution_id != null) return false;
    } else if (!solution_id.equals(other.solution_id)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "SolutionObjectsBean [id="
        + id
        + ", solution_id="
        + solution_id
        + ", mp_id="
        + mp_id
        + ", object_type="
        + object_type
        + ", object_id="
        + object_id
        + ", installedBy="
        + installedBy
        + ", installedDate="
        + installedDate
        + "]";
  }
}

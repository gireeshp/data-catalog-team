package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.Arrays;

import com.augmentiq.maxiq.constant.configuration.enums.Id;

public class SolutionShort implements Serializable {
  @Id private Long solution_id;
  private String icon_class;
  private String header;
  private String short_desc;
  private String installed;
  private byte[] imageInByte;

  public String getInstalled() {
    return installed;
  }

  public void setInstalled(String installed) {
    this.installed = installed;
  }

  public byte[] getImageInByte() {
    return imageInByte;
  }

  public void setImageInByte(byte[] imageInByte) {
    this.imageInByte = imageInByte;
  }

  public SolutionShort() {}

  public SolutionShort(Long solution_id, String icon_class, String header, String short_desc) {
    super();
    this.solution_id = solution_id;
    this.icon_class = icon_class;
    this.header = header;
    this.short_desc = short_desc;
  }

  public SolutionShort(
      Long solution_id,
      String icon_class,
      String header,
      String short_desc,
      String installed,
      byte[] imageInByte) {
    super();
    this.solution_id = solution_id;
    this.icon_class = icon_class;
    this.header = header;
    this.short_desc = short_desc;
    this.installed = installed;
    this.imageInByte = imageInByte;
  }

  public Long getSolution_id() {
    return solution_id;
  }

  public void setSolution_id(Long solution_id) {
    this.solution_id = solution_id;
  }

  public String getIcon_class() {
    return icon_class;
  }

  public void setIcon_class(String icon_class) {
    this.icon_class = icon_class;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getShort_desc() {
    return short_desc;
  }

  public void setShort_desc(String short_desc) {
    this.short_desc = short_desc;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((header == null) ? 0 : header.hashCode());
    result = prime * result + ((icon_class == null) ? 0 : icon_class.hashCode());
    result = prime * result + Arrays.hashCode(imageInByte);
    result = prime * result + ((installed == null) ? 0 : installed.hashCode());
    result = prime * result + ((short_desc == null) ? 0 : short_desc.hashCode());
    result = prime * result + ((solution_id == null) ? 0 : solution_id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    SolutionShort other = (SolutionShort) obj;
    if (header == null) {
      if (other.header != null) return false;
    } else if (!header.equals(other.header)) return false;
    if (icon_class == null) {
      if (other.icon_class != null) return false;
    } else if (!icon_class.equals(other.icon_class)) return false;
    if (!Arrays.equals(imageInByte, other.imageInByte)) return false;
    if (installed == null) {
      if (other.installed != null) return false;
    } else if (!installed.equals(other.installed)) return false;
    if (short_desc == null) {
      if (other.short_desc != null) return false;
    } else if (!short_desc.equals(other.short_desc)) return false;
    if (solution_id == null) {
      if (other.solution_id != null) return false;
    } else if (!solution_id.equals(other.solution_id)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "SolutionShort [solution_id="
        + solution_id
        + ", icon_class="
        + icon_class
        + ", header="
        + header
        + ", short_desc="
        + short_desc
        + ", installed="
        + installed
        + ", imageInByte="
        + Arrays.toString(imageInByte)
        + "]";
  }
}

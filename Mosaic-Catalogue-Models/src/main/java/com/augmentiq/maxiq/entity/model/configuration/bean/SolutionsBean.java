package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/** @author ajinkyamarathe */
@TableName(tableName = Sequences.MY_SOLUTIONS)
public class SolutionsBean implements Serializable {
  @Id private Long id = 0L;
  private String installation_id;
  private String solution_id;
  private String solution_name;
  private String solution_short_desc;
  private String solution_full_desc;
  private String created_by;
  private String created_date;
  private String version_id;
  private String version_name;
  private Boolean status;
  private String launch_url;
  private String group_id;
  private String project_id;

  public SolutionsBean() {
    super();
  }

  public SolutionsBean(
      String installation_id,
      String solution_id,
      String solution_name,
      String solution_short_desc,
      String solution_full_desc,
      String created_by,
      String created_date,
      String version_id,
      String version_name,
      Boolean status,
      String launch_url,
      String group_id,
      String project_id) {
    super();
    this.installation_id = installation_id;
    this.solution_id = solution_id;
    this.solution_name = solution_name;
    this.solution_short_desc = solution_short_desc;
    this.solution_full_desc = solution_full_desc;
    this.created_by = created_by;
    this.created_date = created_date;
    this.version_id = version_id;
    this.version_name = version_name;
    this.status = status;
    this.launch_url = launch_url;
    this.group_id = group_id;
    this.project_id = project_id;
  }

  public String getInstallation_id() {
    return installation_id;
  }

  public void setInstallation_id(String installation_id) {
    this.installation_id = installation_id;
  }

  public String getSolution_id() {
    return solution_id;
  }

  public void setSolution_id(String solution_id) {
    this.solution_id = solution_id;
  }

  public String getSolution_name() {
    return solution_name;
  }

  public void setSolution_name(String solution_name) {
    this.solution_name = solution_name;
  }

  public String getSolution_short_desc() {
    return solution_short_desc;
  }

  public void setSolution_short_desc(String solution_short_desc) {
    this.solution_short_desc = solution_short_desc;
  }

  public String getSolution_full_desc() {
    return solution_full_desc;
  }

  public void setSolution_full_desc(String solution_full_desc) {
    this.solution_full_desc = solution_full_desc;
  }

  public String getCreated_by() {
    return created_by;
  }

  public void setCreated_by(String created_by) {
    this.created_by = created_by;
  }

  public String getVersion_id() {
    return version_id;
  }

  public void setVersion_id(String version_id) {
    this.version_id = version_id;
  }

  public String getVersion_name() {
    return version_name;
  }

  public void setVersion_name(String version_name) {
    this.version_name = version_name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCreated_date() {
    return created_date;
  }

  public void setCreated_date(String created_date) {
    this.created_date = created_date;
  }

  public Boolean getStatus() {
    return status;
  }

  public void setStatus(Boolean status) {
    this.status = status;
  }

  public String getLaucn_url() {
    return launch_url;
  }

  public void setLaucn_url(String laucn_url) {
    this.launch_url = laucn_url;
  }

  public String getGroup_id() {
    return group_id;
  }

  public void setGroup_id(String group_id) {
    this.group_id = group_id;
  }

  public String getProject_id() {
    return project_id;
  }

  public void setProject_id(String project_id) {
    this.project_id = project_id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((created_by == null) ? 0 : created_by.hashCode());
    result = prime * result + ((created_date == null) ? 0 : created_date.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((installation_id == null) ? 0 : installation_id.hashCode());
    result = prime * result + ((solution_full_desc == null) ? 0 : solution_full_desc.hashCode());
    result = prime * result + ((solution_id == null) ? 0 : solution_id.hashCode());
    result = prime * result + ((solution_name == null) ? 0 : solution_name.hashCode());
    result = prime * result + ((solution_short_desc == null) ? 0 : solution_short_desc.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    result = prime * result + ((version_id == null) ? 0 : version_id.hashCode());
    result = prime * result + ((version_name == null) ? 0 : version_name.hashCode());
    result = prime * result + ((launch_url == null) ? 0 : launch_url.hashCode());
    result = prime * result + ((group_id == null) ? 0 : group_id.hashCode());
    result = prime * result + ((project_id == null) ? 0 : project_id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    SolutionsBean other = (SolutionsBean) obj;
    if (created_by == null) {
      if (other.created_by != null) return false;
    } else if (!created_by.equals(other.created_by)) return false;
    if (created_date == null) {
      if (other.created_date != null) return false;
    } else if (!created_date.equals(other.created_date)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (installation_id == null) {
      if (other.installation_id != null) return false;
    } else if (!installation_id.equals(other.installation_id)) return false;
    if (solution_full_desc == null) {
      if (other.solution_full_desc != null) return false;
    } else if (!solution_full_desc.equals(other.solution_full_desc)) return false;
    if (solution_id == null) {
      if (other.solution_id != null) return false;
    } else if (!solution_id.equals(other.solution_id)) return false;
    if (solution_name == null) {
      if (other.solution_name != null) return false;
    } else if (!solution_name.equals(other.solution_name)) return false;
    if (launch_url == null) {
      if (other.launch_url != null) return false;
    } else if (!launch_url.equals(other.launch_url)) return false;
    if (project_id == null) {
      if (other.project_id != null) return false;
    } else if (!project_id.equals(other.project_id)) return false;
    if (group_id == null) {
      if (other.group_id != null) return false;
    } else if (!group_id.equals(other.group_id)) return false;
    if (solution_short_desc == null) {
      if (other.solution_short_desc != null) return false;
    } else if (!solution_short_desc.equals(other.solution_short_desc)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    if (version_id == null) {
      if (other.version_id != null) return false;
    } else if (!version_id.equals(other.version_id)) return false;
    if (version_name == null) {
      if (other.version_name != null) return false;
    } else if (!version_name.equals(other.version_name)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "SolutionsBean [id="
        + id
        + ", installation_id="
        + installation_id
        + ", solution_id="
        + solution_id
        + ", solution_name="
        + solution_name
        + ", solution_short_desc="
        + solution_short_desc
        + ", solution_full_desc="
        + solution_full_desc
        + ", created_by="
        + created_by
        + ", created_date="
        + created_date
        + ", version_id="
        + version_id
        + ", version_name="
        + version_name
        + ", status="
        + status
        + ", laucn_url="
        + launch_url
        + ", group_id="
        + group_id
        + ", project_id="
        + project_id
        + "]";
  }
}

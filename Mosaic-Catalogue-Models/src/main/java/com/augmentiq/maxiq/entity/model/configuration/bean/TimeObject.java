package com.augmentiq.maxiq.entity.model.configuration.bean;

public class TimeObject {
  private int hour;
  private int minute;

  public TimeObject() {}

  /** @return the hour */
  public int getHour() {
    return hour;
  }

  /** @param hour the hour to set */
  public void setHour(int hour) {
    this.hour = hour;
  }

  /** @return the minute */
  public int getMinute() {
    return minute;
  }

  /** @param minute the minute to set */
  public void setMinute(int minute) {
    this.minute = minute;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "TimeObject [hour=" + hour + ", minute=" + minute + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + hour;
    result = prime * result + minute;
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    TimeObject other = (TimeObject) obj;
    if (hour != other.hour) return false;
    if (minute != other.minute) return false;
    return true;
  }
}

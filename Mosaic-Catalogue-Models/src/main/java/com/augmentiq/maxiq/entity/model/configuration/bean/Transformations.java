package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;

import com.augmentiq.maxiq.constant.configuration.enums.TransformationTypes;

public class Transformations implements Serializable {

  private List<Integer> inputFieldsPos_;
  private TransformationTypes type_;
  private String fromValue_;
  private String toValue_;
  private boolean replaceAll_;
  private APIDomain apiDomain_;
  private Long tranformationId_;

  public Long getTranformationId() {
    return tranformationId_;
  }

  public void setTranformationId(Long tranformationId) {
    tranformationId_ = tranformationId;
  }

  public List<Integer> getInputFieldsPos() {
    return inputFieldsPos_;
  }

  public void setInputFieldsPos(List<Integer> inputFieldsPos) {
    inputFieldsPos_ = inputFieldsPos;
  }

  public TransformationTypes getType() {
    return type_;
  }

  public void setType(TransformationTypes type) {
    type_ = type;
  }

  public String getFromValue() {
    return fromValue_;
  }

  public void setFromValue(String fromValue) {
    fromValue_ = fromValue;
  }

  public String getToValue() {
    return toValue_;
  }

  public void setToValue(String toValue) {
    toValue_ = toValue;
  }

  public boolean isReplaceAll() {
    return replaceAll_;
  }

  public void setReplaceAll(boolean replaceAll) {
    replaceAll_ = replaceAll;
  }

  public APIDomain getApiDomain() {
    return apiDomain_;
  }

  public void setApiDomain(APIDomain apiDomain) {
    apiDomain_ = apiDomain;
  }

  public Transformations() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Transformations [inputFieldsPos_="
        + inputFieldsPos_
        + ", type_="
        + type_
        + ", fromValue_="
        + fromValue_
        + ", toValue_="
        + toValue_
        + ", replaceAll_="
        + replaceAll_
        + ", apiDomain_="
        + apiDomain_
        + ", tranformationId_="
        + tranformationId_
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((apiDomain_ == null) ? 0 : apiDomain_.hashCode());
    result = prime * result + ((fromValue_ == null) ? 0 : fromValue_.hashCode());
    result = prime * result + ((inputFieldsPos_ == null) ? 0 : inputFieldsPos_.hashCode());
    result = prime * result + (replaceAll_ ? 1231 : 1237);
    result = prime * result + ((toValue_ == null) ? 0 : toValue_.hashCode());
    result = prime * result + ((tranformationId_ == null) ? 0 : tranformationId_.hashCode());
    result = prime * result + ((type_ == null) ? 0 : type_.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Transformations other = (Transformations) obj;
    if (apiDomain_ == null) {
      if (other.apiDomain_ != null) return false;
    } else if (!apiDomain_.equals(other.apiDomain_)) return false;
    if (fromValue_ == null) {
      if (other.fromValue_ != null) return false;
    } else if (!fromValue_.equals(other.fromValue_)) return false;
    if (inputFieldsPos_ == null) {
      if (other.inputFieldsPos_ != null) return false;
    } else if (!inputFieldsPos_.equals(other.inputFieldsPos_)) return false;
    if (replaceAll_ != other.replaceAll_) return false;
    if (toValue_ == null) {
      if (other.toValue_ != null) return false;
    } else if (!toValue_.equals(other.toValue_)) return false;
    if (tranformationId_ == null) {
      if (other.tranformationId_ != null) return false;
    } else if (!tranformationId_.equals(other.tranformationId_)) return false;
    if (type_ != other.type_) return false;
    return true;
  }
}

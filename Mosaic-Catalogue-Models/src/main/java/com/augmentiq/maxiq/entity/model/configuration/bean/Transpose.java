package com.augmentiq.maxiq.entity.model.configuration.bean;

public class Transpose {
  private String dateFormat;
  private String csv;
  private String delimeter;
  private String numType;
  private Long tempDsId;
  private String sysdate;

  public String getSysdate() {
    return sysdate;
  }

  public void setSysdate(String sysdate) {
    this.sysdate = sysdate;
  }

  public Long getTempDsId() {
    return tempDsId;
  }

  public void setTempDsId(Long tempDsId) {
    this.tempDsId = tempDsId;
  }

  public String getDateFormat() {
    return dateFormat;
  }

  public void setDateFormat(String dateFormat) {
    this.dateFormat = dateFormat;
  }

  public String getCsv() {
    return csv;
  }

  public void setCsv(String csv) {
    this.csv = csv;
  }

  public String getDelimeter() {
    return delimeter;
  }

  public void setDelimeter(String delimeter) {
    this.delimeter = delimeter;
  }

  public String getNumType() {
    return numType;
  }

  public void setNumType(String numType) {
    this.numType = numType;
  }

  @Override
  public String toString() {
    return "Transpose [dateFormat="
        + dateFormat
        + ", csv="
        + csv
        + ", delimeter="
        + delimeter
        + ", numType="
        + numType
        + ", tempDsId="
        + tempDsId
        + ", sysdate="
        + sysdate
        + "]";
  }
}

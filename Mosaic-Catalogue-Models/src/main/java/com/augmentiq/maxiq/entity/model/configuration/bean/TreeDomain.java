package com.augmentiq.maxiq.entity.model.configuration.bean;

/**
 * This is nuild for making tree structure perform well. It takes a lot of time to iterate through
 * all the fields via reflection. So, here we will iterate only 2 needed fields rather than all
 * fields of a table.
 */
public class TreeDomain {

  private long id;
  private String label;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return "ExploreTreeHighPerformanceBean [id=" + id + ", label=" + label + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (id ^ (id >>> 32));
    result = prime * result + ((label == null) ? 0 : label.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    TreeDomain other = (TreeDomain) obj;
    if (id != other.id) return false;
    if (label == null) {
      if (other.label != null) return false;
    } else if (!label.equals(other.label)) return false;
    return true;
  }
}

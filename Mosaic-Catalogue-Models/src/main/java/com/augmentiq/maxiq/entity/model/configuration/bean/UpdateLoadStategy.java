package com.augmentiq.maxiq.entity.model.configuration.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.constant.configuration.enums.LoadStategyInputType;
import com.augmentiq.maxiq.constant.configuration.enums.LoadStrategyMergeType;

/** Created by shivanand on 8/4/2015. */
public class UpdateLoadStategy implements Serializable {

  //For Append
  private boolean appendenabled;
  private String appendSplitKey;
  private LoadStategyInputType appendInputType;
  private String appendInputValue;

  //For Merge
  private boolean mergeenabled;
  private String mergeSplitKey;
  private LoadStategyInputType mergeType;
  private String mergePath;
  private List<FieldMapping> fieldMappingList;
  private LoadStrategyMergeType loadStrategyMergeType;
  private Long customComponentId;

  //For Delete
  private boolean deleteenabled;
  private String deleteSplitKey;
  private LoadStategyInputType deleteType;
  private String deletepath;
  private String headerTemplate;
  private String delimiter;

  public String getAppendSplitKey() {
    return appendSplitKey;
  }

  public void setAppendSplitKey(String appendSplitKey) {
    this.appendSplitKey = appendSplitKey;
  }

  public String getMergeSplitKey() {
    return mergeSplitKey;
  }

  public void setMergeSplitKey(String mergeSplitKey) {
    this.mergeSplitKey = mergeSplitKey;
  }

  public String getDeleteSplitKey() {
    return deleteSplitKey;
  }

  public void setDeleteSplitKey(String deleteSplitKey) {
    this.deleteSplitKey = deleteSplitKey;
  }

  private Map<Integer, FieldMapping> deleteFieldMappings;

  public Map<Integer, FieldMapping> getDeleteFieldMappings() {
    return deleteFieldMappings;
  }

  public void setDeleteFieldMappings(Map<Integer, FieldMapping> deleteFieldMappings) {
    this.deleteFieldMappings = deleteFieldMappings;
  }

  public List<FieldMapping> getFieldMappingList() {
    return fieldMappingList;
  }

  public void setFieldMappingList(List<FieldMapping> fieldMappingList) {
    this.fieldMappingList = fieldMappingList;
  }

  public LoadStrategyMergeType getLoadStrategyMergeType() {
    return loadStrategyMergeType;
  }

  public void setLoadStrategyMergeType(LoadStrategyMergeType loadStrategyMergeType) {
    this.loadStrategyMergeType = loadStrategyMergeType;
  }

  public Long getCustomComponentId() {
    return customComponentId;
  }

  public void setCustomComponentId(Long customComponentId) {
    this.customComponentId = customComponentId;
  }

  public boolean isAppendenabled() {
    return appendenabled;
  }

  public void setAppendenabled(boolean appendenabled) {
    this.appendenabled = appendenabled;
  }

  public LoadStategyInputType getAppendInputType() {
    return appendInputType;
  }

  public void setAppendInputType(LoadStategyInputType appendInputType) {
    this.appendInputType = appendInputType;
  }

  public String getAppendInputValue() {
    return appendInputValue;
  }

  public void setAppendInputValue(String appendInputValue) {
    this.appendInputValue = appendInputValue;
  }

  public boolean isMergeenabled() {
    return mergeenabled;
  }

  public void setMergeenabled(boolean mergeenabled) {
    this.mergeenabled = mergeenabled;
  }

  public LoadStategyInputType getMergeType() {
    return mergeType;
  }

  public void setMergeType(LoadStategyInputType mergeType) {
    this.mergeType = mergeType;
  }

  public boolean isDeleteenabled() {
    return deleteenabled;
  }

  public void setDeleteenabled(boolean deleteenabled) {
    this.deleteenabled = deleteenabled;
  }

  public LoadStategyInputType getDeleteType() {
    return deleteType;
  }

  public void setDeleteType(LoadStategyInputType deleteType) {
    this.deleteType = deleteType;
  }

  public String getHeaderTemplate() {
    return headerTemplate;
  }

  public void setHeaderTemplate(String headerTemplate) {
    this.headerTemplate = headerTemplate;
  }

  public String getDelimiter() {
    return delimiter;
  }

  public void setDelimiter(String delimiter) {
    this.delimiter = delimiter;
  }

  public String getMergePath() {
    return mergePath;
  }

  public void setMergePath(String mergePath) {
    this.mergePath = mergePath;
  }

  public String getDeletepath() {
    return deletepath;
  }

  public void setDeletepath(String deletepath) {
    this.deletepath = deletepath;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appendInputType == null) ? 0 : appendInputType.hashCode());
    result = prime * result + ((appendInputValue == null) ? 0 : appendInputValue.hashCode());
    result = prime * result + ((appendSplitKey == null) ? 0 : appendSplitKey.hashCode());
    result = prime * result + (appendenabled ? 1231 : 1237);
    result = prime * result + ((customComponentId == null) ? 0 : customComponentId.hashCode());
    result = prime * result + ((deleteFieldMappings == null) ? 0 : deleteFieldMappings.hashCode());
    result = prime * result + ((deleteSplitKey == null) ? 0 : deleteSplitKey.hashCode());
    result = prime * result + ((deleteType == null) ? 0 : deleteType.hashCode());
    result = prime * result + (deleteenabled ? 1231 : 1237);
    result = prime * result + ((deletepath == null) ? 0 : deletepath.hashCode());
    result = prime * result + ((delimiter == null) ? 0 : delimiter.hashCode());
    result = prime * result + ((fieldMappingList == null) ? 0 : fieldMappingList.hashCode());
    result = prime * result + ((headerTemplate == null) ? 0 : headerTemplate.hashCode());
    result =
        prime * result + ((loadStrategyMergeType == null) ? 0 : loadStrategyMergeType.hashCode());
    result = prime * result + ((mergePath == null) ? 0 : mergePath.hashCode());
    result = prime * result + ((mergeSplitKey == null) ? 0 : mergeSplitKey.hashCode());
    result = prime * result + ((mergeType == null) ? 0 : mergeType.hashCode());
    result = prime * result + (mergeenabled ? 1231 : 1237);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    UpdateLoadStategy other = (UpdateLoadStategy) obj;
    if (appendInputType != other.appendInputType) return false;
    if (appendInputValue == null) {
      if (other.appendInputValue != null) return false;
    } else if (!appendInputValue.equals(other.appendInputValue)) return false;
    if (appendSplitKey == null) {
      if (other.appendSplitKey != null) return false;
    } else if (!appendSplitKey.equals(other.appendSplitKey)) return false;
    if (appendenabled != other.appendenabled) return false;
    if (customComponentId == null) {
      if (other.customComponentId != null) return false;
    } else if (!customComponentId.equals(other.customComponentId)) return false;
    if (deleteFieldMappings == null) {
      if (other.deleteFieldMappings != null) return false;
    } else if (!deleteFieldMappings.equals(other.deleteFieldMappings)) return false;
    if (deleteSplitKey == null) {
      if (other.deleteSplitKey != null) return false;
    } else if (!deleteSplitKey.equals(other.deleteSplitKey)) return false;
    if (deleteType != other.deleteType) return false;
    if (deleteenabled != other.deleteenabled) return false;
    if (deletepath == null) {
      if (other.deletepath != null) return false;
    } else if (!deletepath.equals(other.deletepath)) return false;
    if (delimiter == null) {
      if (other.delimiter != null) return false;
    } else if (!delimiter.equals(other.delimiter)) return false;
    if (fieldMappingList == null) {
      if (other.fieldMappingList != null) return false;
    } else if (!fieldMappingList.equals(other.fieldMappingList)) return false;
    if (headerTemplate == null) {
      if (other.headerTemplate != null) return false;
    } else if (!headerTemplate.equals(other.headerTemplate)) return false;
    if (loadStrategyMergeType != other.loadStrategyMergeType) return false;
    if (mergePath == null) {
      if (other.mergePath != null) return false;
    } else if (!mergePath.equals(other.mergePath)) return false;
    if (mergeSplitKey == null) {
      if (other.mergeSplitKey != null) return false;
    } else if (!mergeSplitKey.equals(other.mergeSplitKey)) return false;
    if (mergeType != other.mergeType) return false;
    if (mergeenabled != other.mergeenabled) return false;
    return true;
  }

  @Override
  public String toString() {
    return "UpdateLoadStategy [appendenabled="
        + appendenabled
        + ", appendSplitKey="
        + appendSplitKey
        + ", appendInputType="
        + appendInputType
        + ", appendInputValue="
        + appendInputValue
        + ", mergeenabled="
        + mergeenabled
        + ", mergeSplitKey="
        + mergeSplitKey
        + ", mergeType="
        + mergeType
        + ", mergePath="
        + mergePath
        + ", fieldMappingList="
        + fieldMappingList
        + ", loadStrategyMergeType="
        + loadStrategyMergeType
        + ", customComponentId="
        + customComponentId
        + ", deleteenabled="
        + deleteenabled
        + ", deleteSplitKey="
        + deleteSplitKey
        + ", deleteType="
        + deleteType
        + ", deletepath="
        + deletepath
        + ", headerTemplate="
        + headerTemplate
        + ", delimiter="
        + delimiter
        + ", deleteFieldMappings="
        + deleteFieldMappings
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Actions;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.ACTIVITY_LOG)
public class UserActivity {

  @Id public Long id;
  public String ownerProjectId;
  public ObjectTypes objectType;
  public String objectId;
  public String userId;
  public String timeDate;
  public Actions action;
  public String objectName;

  public UserActivity() {}

  public UserActivity(
      String ownerProjectId,
      ObjectTypes objectType,
      String objectId,
      String userId,
      String timeDate,
      Actions action,
      String objectName) {
    super();
    this.ownerProjectId = ownerProjectId;
    this.objectType = objectType;
    this.objectId = objectId;
    this.userId = userId;
    this.timeDate = timeDate;
    this.action = action;
    this.objectName = objectName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(String ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  public ObjectTypes getObjectType() {
    return objectType;
  }

  public void setObjectType(ObjectTypes objectType) {
    this.objectType = objectType;
  }

  public String getObjectId() {
    return objectId;
  }

  public void setObjectId(String objectId) {
    this.objectId = objectId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getTimeDate() {
    return timeDate;
  }

  public void setTimeDate(String timeDate) {
    this.timeDate = timeDate;
  }

  public Actions getAction() {
    return action;
  }

  public void setAction(Actions action) {
    this.action = action;
  }

  public String getObjectName() {
    return objectName;
  }

  public void setObjectName(String objectName) {
    this.objectName = objectName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((action == null) ? 0 : action.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
    result = prime * result + ((objectName == null) ? 0 : objectName.hashCode());
    result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    result = prime * result + ((timeDate == null) ? 0 : timeDate.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    UserActivity other = (UserActivity) obj;
    if (action != other.action) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (objectId == null) {
      if (other.objectId != null) return false;
    } else if (!objectId.equals(other.objectId)) return false;
    if (objectName == null) {
      if (other.objectName != null) return false;
    } else if (!objectName.equals(other.objectName)) return false;
    if (objectType != other.objectType) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    if (timeDate == null) {
      if (other.timeDate != null) return false;
    } else if (!timeDate.equals(other.timeDate)) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "LogActivityBean [id="
        + id
        + ", ownerProjectId="
        + ownerProjectId
        + ", objectType="
        + objectType
        + ", objectId="
        + objectId
        + ", userId="
        + userId
        + ", timeDate="
        + timeDate
        + ", action="
        + action
        + ", objectName="
        + objectName
        + "]";
  }
}

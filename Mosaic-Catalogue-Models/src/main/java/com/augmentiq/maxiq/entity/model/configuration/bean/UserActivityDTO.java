package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.ACTIVITY_LOG)
public class UserActivityDTO {

  private String timeDate;

  public String getTimeDate() {
    return timeDate;
  }

  public void setTimeDate(String timeDate) {
    this.timeDate = timeDate;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/** @author Rushi Created For flavour story of amitesh */
@TableName(tableName = Sequences.USER_CLIENT_MAPPING)
public class UserClientMapping {

  @Id private Long unqUserId;
  private Long clientId;

  public Long getUnqUserId() {
    return unqUserId;
  }

  public void setUnqUserId(Long unqUserId) {
    this.unqUserId = unqUserId;
  }

  public Long getClientId() {
    return clientId;
  }

  public void setClientId(Long clientId) {
    this.clientId = clientId;
  }
}

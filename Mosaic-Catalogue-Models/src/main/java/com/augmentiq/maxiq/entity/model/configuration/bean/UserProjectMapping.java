package com.augmentiq.maxiq.entity.model.configuration.bean;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * CREATE TABLE `maxiq`.`user_project_mapping` ( `id` int(11) NOT NULL AUTO_INCREMENT, `userId`
 * int(11) NOT NULL, `projectId` int(11) NOT NULL, `accessType` int(11) NOT NULL, PRIMARY KEY (`id`)
 * ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
 */
/** @author Rushi created on Jul 10, 2017 for MAX-1058 */
@TableName(tableName = Sequences.USER_PROJECT_MAPPING)
public class UserProjectMapping {

  @Id private Long id;
  private Long userId;
  private Long projectId;
  private Integer accessType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public Integer getAccessType() {
    return accessType;
  }

  public void setAccessType(Integer accessType) {
    this.accessType = accessType;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((accessType == null) ? 0 : accessType.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((projectId == null) ? 0 : projectId.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    UserProjectMapping other = (UserProjectMapping) obj;
    if (accessType == null) {
      if (other.accessType != null) return false;
    } else if (!accessType.equals(other.accessType)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (projectId == null) {
      if (other.projectId != null) return false;
    } else if (!projectId.equals(other.projectId)) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "UserProjectMapping [id="
        + id
        + ", userId="
        + userId
        + ", projectId="
        + projectId
        + ", accessType="
        + accessType
        + "]";
  }
}

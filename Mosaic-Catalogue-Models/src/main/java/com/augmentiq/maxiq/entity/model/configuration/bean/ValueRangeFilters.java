package com.augmentiq.maxiq.entity.model.configuration.bean;

public class ValueRangeFilters extends DataFilter {
  private Integer fromValue_;
  private Integer toValue_;

  public Integer getFromValue() {
    return fromValue_;
  }

  /**
   * * This is the lower bound to the range. Put -1 to avoid this check
   *
   * @param fromValue
   */
  public void setFromValue(Integer fromValue) {
    this.fromValue_ = fromValue;
  }

  public Integer getToValue() {
    return toValue_;
  }

  /**
   * * This is the upper bound to the range. Put -1 to avoid this check
   *
   * @param fromValue_
   */
  public void setToValue(Integer toValue) {
    this.toValue_ = toValue;
  }

  public ValueRangeFilters() {}

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ValueRangeFilters [fromValue_=" + fromValue_ + ", toValue_=" + toValue_ + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((fromValue_ == null) ? 0 : fromValue_.hashCode());
    result = prime * result + ((toValue_ == null) ? 0 : toValue_.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!super.equals(obj)) return false;
    if (getClass() != obj.getClass()) return false;
    ValueRangeFilters other = (ValueRangeFilters) obj;
    if (fromValue_ == null) {
      if (other.fromValue_ != null) return false;
    } else if (!fromValue_.equals(other.fromValue_)) return false;
    if (toValue_ == null) {
      if (other.toValue_ != null) return false;
    } else if (!toValue_.equals(other.toValue_)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.entity.model.configuration.bean;
/** @author Rushi created on Aug 1, 2017 for MAX-1186 */
public class WordFrequency {
  private String text;
  private Long size;

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Long getSize() {
    return size;
  }

  public void setSize(Long size) {
    this.size = size;
  }
}

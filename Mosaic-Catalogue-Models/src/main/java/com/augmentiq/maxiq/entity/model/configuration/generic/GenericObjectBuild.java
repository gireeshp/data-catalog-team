package com.augmentiq.maxiq.entity.model.configuration.generic;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.ConfigurationDataType;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.CONFIGURATION)
public class GenericObjectBuild {
  @Id private Long id;
  private String json;
  private ConfigurationDataType type;
  private String name;
  private Long userid;
  private Long addedtime;
  private Long updatetime;
  private String typeOfActiveInstances;
  private Integer noOfActiveInstances;

  public GenericObjectBuild() {}

  public GenericObjectBuild(
      Long id,
      ConfigurationDataType type,
      String json,
      String name,
      Long userid,
      Long addedtime,
      Long updatetime) {
    super();
    this.id = id;
    this.type = type;
    this.json = json;
    this.name = name;
    this.userid = userid;
    this.addedtime = addedtime;
    this.updatetime = updatetime;
  }

  public ConfigurationDataType getType() {
    return type;
  }

  public void setType(ConfigurationDataType type) {
    this.type = type;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getJson() {
    return json;
  }

  public void setJson(String json) {
    this.json = json;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getUserid() {
    return userid;
  }

  public void setUserid(Long userid) {
    this.userid = userid;
  }

  public Long getAddedtime() {
    return addedtime;
  }

  public void setAddedtime(Long addedtime) {
    this.addedtime = addedtime;
  }

  public Long getUpdatetime() {
    return updatetime;
  }

  public void setUpdatetime(Long updatetime) {
    this.updatetime = updatetime;
  }

  public String getTypeOfActiveInstances() {
    return typeOfActiveInstances;
  }

  public void setTypeOfActiveInstances(String typeOfActiveInstances) {
    this.typeOfActiveInstances = typeOfActiveInstances;
  }

  public Integer getNoOfActiveInstances() {
    return noOfActiveInstances;
  }

  public void setNoOfActiveInstances(Integer noOfActiveInstances) {
    this.noOfActiveInstances = noOfActiveInstances;
  }

  @Override
  public String toString() {
    return "GenericObjectBuild [id="
        + id
        + ", json="
        + json
        + ", type="
        + type
        + ", name="
        + name
        + ", userid="
        + userid
        + ", addedtime="
        + addedtime
        + ", updatetime="
        + updatetime
        + ", typeOfActiveInstances="
        + typeOfActiveInstances
        + ", noOfActiveInstances="
        + noOfActiveInstances
        + "]";
  }
}

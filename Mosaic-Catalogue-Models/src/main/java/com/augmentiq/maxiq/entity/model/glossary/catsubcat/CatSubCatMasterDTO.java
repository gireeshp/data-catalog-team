package com.augmentiq.maxiq.entity.model.glossary.catsubcat;

import java.io.Serializable;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.CATEGORY_SUBCATEGORY)
public class CatSubCatMasterDTO implements Serializable {

  private Long id;
  private String catOrSubCatName;
  private Long parentId;
  private Long createdDate;
  private Long createdBy;
  private Long modifiedDate = 0l;
  private Long modifiedBy = 0l;
  private Integer status;
  private String createdByName;
  private String modifiedByName;
  private String shortDescription;
  private String longDescription;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCatOrSubCatName() {
    return catOrSubCatName;
  }

  public void setCatOrSubCatName(String catOrSubCatName) {
    this.catOrSubCatName = catOrSubCatName;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public Long getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Long modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getCreatedByName() {
    return createdByName;
  }

  public void setCreatedByName(String createdByName) {
    this.createdByName = createdByName;
  }

  public String getModifiedByName() {
    return modifiedByName;
  }

  public void setModifiedByName(String modifiedByName) {
    this.modifiedByName = modifiedByName;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getLongDescription() {
    return longDescription;
  }

  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((catOrSubCatName == null) ? 0 : catOrSubCatName.hashCode());
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdByName == null) ? 0 : createdByName.hashCode());
    result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((longDescription == null) ? 0 : longDescription.hashCode());
    result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
    result = prime * result + ((modifiedByName == null) ? 0 : modifiedByName.hashCode());
    result = prime * result + ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
    result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
    result = prime * result + ((shortDescription == null) ? 0 : shortDescription.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    CatSubCatMasterDTO other = (CatSubCatMasterDTO) obj;
    if (catOrSubCatName == null) {
      if (other.catOrSubCatName != null) return false;
    } else if (!catOrSubCatName.equals(other.catOrSubCatName)) return false;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdByName == null) {
      if (other.createdByName != null) return false;
    } else if (!createdByName.equals(other.createdByName)) return false;
    if (createdDate == null) {
      if (other.createdDate != null) return false;
    } else if (!createdDate.equals(other.createdDate)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (longDescription == null) {
      if (other.longDescription != null) return false;
    } else if (!longDescription.equals(other.longDescription)) return false;
    if (modifiedBy == null) {
      if (other.modifiedBy != null) return false;
    } else if (!modifiedBy.equals(other.modifiedBy)) return false;
    if (modifiedByName == null) {
      if (other.modifiedByName != null) return false;
    } else if (!modifiedByName.equals(other.modifiedByName)) return false;
    if (modifiedDate == null) {
      if (other.modifiedDate != null) return false;
    } else if (!modifiedDate.equals(other.modifiedDate)) return false;
    if (parentId == null) {
      if (other.parentId != null) return false;
    } else if (!parentId.equals(other.parentId)) return false;
    if (shortDescription == null) {
      if (other.shortDescription != null) return false;
    } else if (!shortDescription.equals(other.shortDescription)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "CatSubCatMasterDTO [id="
        + id
        + ", catOrSubCatName="
        + catOrSubCatName
        + ", parentId="
        + parentId
        + ", createdDate="
        + createdDate
        + ", createdBy="
        + createdBy
        + ", modifiedDate="
        + modifiedDate
        + ", modifiedBy="
        + modifiedBy
        + ", status="
        + status
        + ", createdByName="
        + createdByName
        + ", modifiedByName="
        + modifiedByName
        + ", shortDescription="
        + shortDescription
        + ", longDescription="
        + longDescription
        + "]";
  }
}

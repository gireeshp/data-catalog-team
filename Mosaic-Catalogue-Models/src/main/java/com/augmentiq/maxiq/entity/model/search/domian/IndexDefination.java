package com.augmentiq.maxiq.entity.model.search.domian;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.elasticsearch.search.lookup.IndexField;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.IndexInputType;
import com.augmentiq.maxiq.constant.configuration.enums.RunningStatus;
import com.augmentiq.maxiq.constant.configuration.enums.SearchDataInputType;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * Created by shivanand on 7/3/15.
 *
 * <p>create table index_defination (id numeric, indexName varchar(500), indexFields blob, createdOn
 * varchar(40), lastRunOn varchar(40), runningStatus varchar(100), createdBy varchar(100), count
 * numeric, dsId numeric, appId numeric, nodeId numeric, searchInputType varchar(100),
 * indexInputType varchar(100),sampleJson blob, inputDataDilimiter varchar(40))
 */
@TableName(tableName = Sequences.INDEX_DEFINATIONS)
public class IndexDefination implements Serializable {

  @Id private Long id;
  private String indexName;
  private List<IndexField> indexFields;
  private Date lastRunOn;
  private Date createdOn;
  private RunningStatus runningStatus;
  private String createdBy;
  private Long count;
  private IndexInputType indexInputType;
  private String sampleJson;
  private Long dsId;
  private Long appId;
  private Long nodeId;
  private SearchDataInputType searchInputType;
  private String inputDataDilimiter;
  private Long groupId;
  private Long ownerProjectId;

  public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  public String getInputDataDilimiter() {
    return inputDataDilimiter;
  }

  public void setInputDataDilimiter(String inputDataDilimiter) {
    this.inputDataDilimiter = inputDataDilimiter;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIndexName() {
    return indexName;
  }

  public void setIndexName(String indexName) {
    this.indexName = indexName;
  }

  public List<IndexField> getIndexFields() {
    return indexFields;
  }

  public void setIndexFields(List<IndexField> indexFields) {
    this.indexFields = indexFields;
  }

  public Date getLastRunOn() {
    return lastRunOn;
  }

  public void setLastRunOn(Date lastRunOn) {
    this.lastRunOn = lastRunOn;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public RunningStatus getRunningStatus() {
    return runningStatus;
  }

  public void setRunningStatus(RunningStatus runningStatus) {
    this.runningStatus = runningStatus;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getCount() {

    if (null == count) return 0L;
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }

  public IndexInputType getIndexInputType() {
    return indexInputType;
  }

  public void setIndexInputType(IndexInputType indexInputType) {
    this.indexInputType = indexInputType;
  }

  public String getSampleJson() {
    return sampleJson;
  }

  public void setSampleJson(String sampleJson) {
    this.sampleJson = sampleJson;
  }

  public Long getDsId() {
    if (null == dsId) return 0L;
    return dsId;
  }

  public void setDsId(Long dsId) {
    this.dsId = dsId;
  }

  public Long getAppId() {

    if (null == appId) return 0L;
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public Long getNodeId() {

    if (null == nodeId) return 0L;
    return nodeId;
  }

  public void setNodeId(Long nodeId) {
    this.nodeId = nodeId;
  }

  public SearchDataInputType getSearchInputType() {
    return searchInputType;
  }

  public void setSearchInputType(SearchDataInputType searchInputType) {
    this.searchInputType = searchInputType;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("IndexDefination [id=");
    builder.append(id);
    builder.append(", indexName=");
    builder.append(indexName);
    builder.append(", indexFields=");
    builder.append(indexFields);
    builder.append(", lastRunOn=");
    builder.append(lastRunOn);
    builder.append(", createdOn=");
    builder.append(createdOn);
    builder.append(", runningStatus=");
    builder.append(runningStatus);
    builder.append(", createdBy=");
    builder.append(createdBy);
    builder.append(", count=");
    builder.append(count);
    builder.append(", indexInputType=");
    builder.append(indexInputType);
    builder.append(", sampleJson=");
    builder.append(sampleJson);
    builder.append(", dsId=");
    builder.append(dsId);
    builder.append(", appId=");
    builder.append(appId);
    builder.append(", nodeId=");
    builder.append(nodeId);
    builder.append(", searchInputType=");
    builder.append(searchInputType);
    builder.append(", inputDataDilimiter=");
    builder.append(inputDataDilimiter);
    builder.append(", groupId=");
    builder.append(groupId);
    builder.append(", ownerProjectId=");
    builder.append(ownerProjectId);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((count == null) ? 0 : count.hashCode());
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
    result = prime * result + ((dsId == null) ? 0 : dsId.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((indexFields == null) ? 0 : indexFields.hashCode());
    result = prime * result + ((indexInputType == null) ? 0 : indexInputType.hashCode());
    result = prime * result + ((indexName == null) ? 0 : indexName.hashCode());
    result = prime * result + ((inputDataDilimiter == null) ? 0 : inputDataDilimiter.hashCode());
    result = prime * result + ((lastRunOn == null) ? 0 : lastRunOn.hashCode());
    result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    result = prime * result + ((runningStatus == null) ? 0 : runningStatus.hashCode());
    result = prime * result + ((sampleJson == null) ? 0 : sampleJson.hashCode());
    result = prime * result + ((searchInputType == null) ? 0 : searchInputType.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    IndexDefination other = (IndexDefination) obj;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (count == null) {
      if (other.count != null) return false;
    } else if (!count.equals(other.count)) return false;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdOn == null) {
      if (other.createdOn != null) return false;
    } else if (!createdOn.equals(other.createdOn)) return false;
    if (dsId == null) {
      if (other.dsId != null) return false;
    } else if (!dsId.equals(other.dsId)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (indexFields == null) {
      if (other.indexFields != null) return false;
    } else if (!indexFields.equals(other.indexFields)) return false;
    if (indexInputType != other.indexInputType) return false;
    if (indexName == null) {
      if (other.indexName != null) return false;
    } else if (!indexName.equals(other.indexName)) return false;
    if (inputDataDilimiter == null) {
      if (other.inputDataDilimiter != null) return false;
    } else if (!inputDataDilimiter.equals(other.inputDataDilimiter)) return false;
    if (lastRunOn == null) {
      if (other.lastRunOn != null) return false;
    } else if (!lastRunOn.equals(other.lastRunOn)) return false;
    if (nodeId == null) {
      if (other.nodeId != null) return false;
    } else if (!nodeId.equals(other.nodeId)) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    if (runningStatus != other.runningStatus) return false;
    if (sampleJson == null) {
      if (other.sampleJson != null) return false;
    } else if (!sampleJson.equals(other.sampleJson)) return false;
    if (searchInputType != other.searchInputType) return false;
    return true;
  }
}
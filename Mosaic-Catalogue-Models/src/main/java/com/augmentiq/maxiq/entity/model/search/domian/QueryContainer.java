package com.augmentiq.maxiq.entity.model.search.domian;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;
import com.augmentiq.maxiq.model.search.domian.AggregationRules;
import com.augmentiq.maxiq.model.search.domian.OutputFields;
import com.augmentiq.maxiq.model.search.domian.QuerySingleTerm;

/**
 * Created by shivanand on 7/14/2015. create table search_query_def (id numeric, queryName
 * varchar(500), indexName varchar(500), queries blob, aggregations blob, createdOn varchar(40),
 * apiKey varchar(100), createBy varchar(100), outputFields blob)
 */
@TableName(tableName = Sequences.SEARCH_QUERY_DEFINATION)
public class QueryContainer {
  @Id private Long id;
  private String queryName;
  private String indexName;
  private List<QuerySingleTerm> queries;
  private Map<Integer, AggregationRules> aggregations;
  private String createBy;
  private Date createdOn;
  private String apiKey;
  private List<OutputFields> outputFields;
  private Long groupId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getQueryName() {
    return queryName;
  }

  public void setQueryName(String queryName) {
    this.queryName = queryName;
  }

  public String getIndexName() {
    return indexName;
  }

  public void setIndexName(String indexName) {
    this.indexName = indexName;
  }

  public List<QuerySingleTerm> getQueries() {
    return queries;
  }

  public void setQueries(List<QuerySingleTerm> queries) {
    this.queries = queries;
  }

  public Map<Integer, AggregationRules> getAggregations() {
    return aggregations;
  }

  public void setAggregations(Map<Integer, AggregationRules> aggregations) {
    this.aggregations = aggregations;
  }

  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public List<OutputFields> getOutputFields() {
    return outputFields;
  }

  public void setOutputFields(List<OutputFields> outputFields) {
    this.outputFields = outputFields;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("QueryContainer [id=");
    builder.append(id);
    builder.append(", queryName=");
    builder.append(queryName);
    builder.append(", indexName=");
    builder.append(indexName);
    builder.append(", queries=");
    builder.append(queries);
    builder.append(", aggregations=");
    builder.append(aggregations);
    builder.append(", createBy=");
    builder.append(createBy);
    builder.append(", createdOn=");
    builder.append(createdOn);
    builder.append(", apiKey=");
    builder.append(apiKey);
    builder.append(", outputFields=");
    builder.append(outputFields);
    builder.append(", groupId=");
    builder.append(groupId);
    builder.append("]");
    return builder.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((apiKey == null) ? 0 : apiKey.hashCode());
    result = prime * result + ((createBy == null) ? 0 : createBy.hashCode());
    result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((indexName == null) ? 0 : indexName.hashCode());
    result = prime * result + ((outputFields == null) ? 0 : outputFields.hashCode());
    result = prime * result + ((queries == null) ? 0 : queries.hashCode());
    result = prime * result + ((queryName == null) ? 0 : queryName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    QueryContainer other = (QueryContainer) obj;
    if (apiKey == null) {
      if (other.apiKey != null) return false;
    } else if (!apiKey.equals(other.apiKey)) return false;
    if (createBy == null) {
      if (other.createBy != null) return false;
    } else if (!createBy.equals(other.createBy)) return false;
    if (createdOn == null) {
      if (other.createdOn != null) return false;
    } else if (!createdOn.equals(other.createdOn)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (indexName == null) {
      if (other.indexName != null) return false;
    } else if (!indexName.equals(other.indexName)) return false;
    if (outputFields == null) {
      if (other.outputFields != null) return false;
    } else if (!outputFields.equals(other.outputFields)) return false;
    if (queries == null) {
      if (other.queries != null) return false;
    } else if (!queries.equals(other.queries)) return false;
    if (queryName == null) {
      if (other.queryName != null) return false;
    } else if (!queryName.equals(other.queryName)) return false;
    return true;
  }
}
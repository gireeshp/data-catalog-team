package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.ACTIONS_MASTER)
public class ActionsMaster {

  @Id private String actionsMasterId;
  private String action;
  private String status;
  private String subsetOf;
  private String actionCode;
  private String appId;

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getActionsMasterId() {
    return actionsMasterId;
  }

  public void setActionsMasterId(String actionsMasterId) {
    this.actionsMasterId = actionsMasterId;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getSubsetOf() {
    return subsetOf;
  }

  public void setSubsetOf(String subsetOf) {
    this.subsetOf = subsetOf;
  }

  public String getActionCode() {
    return actionCode;
  }

  public void setActionCode(String actionCode) {
    this.actionCode = actionCode;
  }

  @Override
  public String toString() {
    return "ActionsMaster [actionsMasterId="
        + actionsMasterId
        + ", action="
        + action
        + ", status="
        + status
        + ", subsetOf="
        + subsetOf
        + ", actionCode="
        + actionCode
        + "]";
  }
}

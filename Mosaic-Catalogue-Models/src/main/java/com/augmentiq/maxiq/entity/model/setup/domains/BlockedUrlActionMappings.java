package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * create table blocked_url_action_mappings (id int(10) not null auto_increment, actionsMasterId
 * int(11), url varchar(100), primary key(id));
 */
@TableName(tableName = Sequences.BLOCKED_URL_ACTION_MAPPINGS)
public class BlockedUrlActionMappings {

  @Id private String id;
  private String actionsMasterId;
  private String url;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getActionsMasterId() {
    return actionsMasterId;
  }

  public void setActionsMasterId(String actionsMasterId) {
    this.actionsMasterId = actionsMasterId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public String toString() {
    return "BlockedUrlActionMappings [id="
        + id
        + ", actionsMasterId="
        + actionsMasterId
        + ", url="
        + url
        + "]";
  }
}

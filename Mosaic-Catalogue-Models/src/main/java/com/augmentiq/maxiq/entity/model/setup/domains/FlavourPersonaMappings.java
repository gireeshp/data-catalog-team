package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * create table flavour (flavourId int(10) not null auto_increment, flavourName varchar(50),
 * flavourDesc varchar(50), primary key(flavourId)); insert into flavour (flavourName, flavourDesc)
 * values ('Basic', 'Basic Features');
 *
 * <p>create table client (clientId int(10) not null auto_increment, clientName varchar(50),
 * clientDesc varchar(50), flavourId int(10) not null, primary key(clientId)); insert into client
 * (clientName, clientDesc, flavourId) values ("HDFC", " HDFC Bank", 1);
 *
 * <p>create table flavour_action_mappings (flavourActionId int(10) not null auto_increment,
 * flavourId int(10) not null, actionsMasterId int(11), primary key(flavourActionId)); INSERT INTO
 * flavour_action_mappings (flavourId, actionsMasterId) SELECT 1, actionsMasterId from
 * actions_master;
 *
 * <p>create table flavour_persona_mappings (flavourPersonaId int(10) not null auto_increment,
 * flavourId int(10) not null, personaId int(11), primary key(flavourPersonaId)); INSERT INTO
 * flavour_persona_mappings (flavourId, personaId) values (1, 1); INSERT INTO
 * flavour_persona_mappings (flavourId, personaId) values (1, 2); INSERT INTO
 * flavour_persona_mappings (flavourId, personaId) values (1, 3);
 *
 * <p>create table user_client_mapping (unqUserId int(20) not null, clientId int(10) not null);
 * INSERT INTO user_client_mapping (unqUserId, clientId) SELECT unqUserId, 1 from application_user;
 *
 * <p>create table blocked_url_action_mappings (id int(10) not null auto_increment, actionsMasterId
 * int(11), url varchar(100), primary key(id)); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (75, "/dataSourceList"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (2, "/dataRepoView"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (14, "/appDetail"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (120, "/streamDetail"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (92, "/notebooks"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (58, "/modelsView"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (46, "/searchIndex"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (48, "/searchQuery"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (57, "/customComponent"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (21, "/taskTracker"); INSERT into blocked_url_action_mappings
 * (actionsMasterId, url) values (21, "/taskTrackerCompleted"); INSERT into
 * blocked_url_action_mappings (actionsMasterId, url) values (26, "/visualize"); INSERT into
 * blocked_url_action_mappings (actionsMasterId, url) values (27, "/hiveExplore"); INSERT into
 * blocked_url_action_mappings (actionsMasterId, url) values (28, "/statistical"); INSERT into
 * blocked_url_action_mappings (actionsMasterId, url) values (109, "/solutions"); INSERT into
 * blocked_url_action_mappings (actionsMasterId, url) values (126, "/boostMemory");
 *
 * <p>create table blocked_url_persona_mappings (id int(10) not null auto_increment, personaId
 * int(11), url varchar(100), primary key(id)); INSERT into blocked_url_persona_mappings (personaId,
 * url) values (1, "/businessUser"); INSERT into blocked_url_persona_mappings (personaId, url)
 * values (2, "/projectsDetails"); INSERT into blocked_url_persona_mappings (personaId, url) values
 * (3, "/operationUser");
 */
@TableName(tableName = Sequences.FLAVOUR_PERSONA_MAPPINGS)
public class FlavourPersonaMappings {

  @Id private Long flavourPersonaId;
  private Long flavourId;
  private String personaId;

  public Long getFlavourPersonaId() {
    return flavourPersonaId;
  }

  public void setFlavourPersonaId(Long flavourPersonaId) {
    this.flavourPersonaId = flavourPersonaId;
  }

  public Long getFlavourId() {
    return flavourId;
  }

  public void setFlavourId(Long flavourId) {
    this.flavourId = flavourId;
  }

  public String getPersonaId() {
    return personaId;
  }

  public void setPersonaId(String personaId) {
    this.personaId = personaId;
  }

  @Override
  public String toString() {
    return "FlavourPersonaMappings [flavourPersonaId="
        + flavourPersonaId
        + ", flavourId="
        + flavourId
        + ", personaId="
        + personaId
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.USER_GROUP)
public class Group {

  @Id private Long groupId;
  private String groupName;
  private String description;
  //	private String queueMasterID;
  private String insertedBy;
  private String insertTs;
  private String updatedBy;
  private String updateTs;

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
  /*	public String getQueueMasterID() {
  	return queueMasterID;
  }
  public void setQueueMasterID(String queueMasterID) {
  	this.queueMasterID = queueMasterID;
  }*/
  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getInsertTs() {
    return insertTs;
  }

  public void setInsertTs(String insertTs) {
    this.insertTs = insertTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdateTs() {
    return updateTs;
  }

  public void setUpdateTs(String updateTs) {
    this.updateTs = updateTs;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Group [groupId=");
    builder.append(groupId);
    builder.append(", groupName=");
    builder.append(groupName);
    builder.append(", description=");
    builder.append(description);
    //		builder.append(", queueMasterID=");
    builder.append(", insertedBy=");
    builder.append(insertedBy);
    builder.append(", insertTs=");
    builder.append(insertTs);
    builder.append(", updatedBy=");
    builder.append(updatedBy);
    builder.append(", updateTs=");
    builder.append(updateTs);
    builder.append("]");
    return builder.toString();
  }
}

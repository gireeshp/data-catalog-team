package com.augmentiq.maxiq.entity.model.setup.domains;

import java.util.Date;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.GROUP_LEVEL_USER)
public class GroupLevelUser {

  @Id private Long groupId;
  private String groupUserPassword;
  private Date createDate;

  public Boolean getPolicyStatus() {
    return policyStatus;
  }

  public void setPolicyStatus(Boolean policyStatus) {
    this.policyStatus = policyStatus;
  }

  private Boolean policyStatus;

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getGroupUserPassword() {
    return groupUserPassword;
  }

  public void setGroupUserPassword(String groupUserPassword) {
    this.groupUserPassword = groupUserPassword;
  }
}

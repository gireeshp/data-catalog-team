package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.PERSONA_MASTER)
public class PersonaMaster {
  @Id private Long persona_id;
  private String persona_name;

  public Long getPersona_id() {
    return persona_id;
  }

  public void setPersona_id(Long persona_id) {
    this.persona_id = persona_id;
  }

  public String getPersona_name() {
    return persona_name;
  }

  public void setPersona_name(String persona_name) {
    this.persona_name = persona_name;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((persona_id == null) ? 0 : persona_id.hashCode());
    result = prime * result + ((persona_name == null) ? 0 : persona_name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    PersonaMaster other = (PersonaMaster) obj;
    if (persona_id == null) {
      if (other.persona_id != null) return false;
    } else if (!persona_id.equals(other.persona_id)) return false;
    if (persona_name == null) {
      if (other.persona_name != null) return false;
    } else if (!persona_name.equals(other.persona_name)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "PersonaMaster [persona_id=" + persona_id + ", persona_name=" + persona_name + "]";
  }
}

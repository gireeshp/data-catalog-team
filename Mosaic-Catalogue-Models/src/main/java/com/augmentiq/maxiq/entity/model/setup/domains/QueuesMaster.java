package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.QUEUE_MASTER)
public class QueuesMaster {

  @Id private Long queueMasterId;
  private String queueName;
  private String status;
  private Long rangerPolicyId;

  public Long getRangerPolicyId() {
    return rangerPolicyId;
  }

  public void setRangerPolicyId(Long rangerPolicyId) {
    this.rangerPolicyId = rangerPolicyId;
  }

  public Long getQueueMasterId() {
    return queueMasterId;
  }

  public void setQueueMasterId(Long queueMasterId) {
    this.queueMasterId = queueMasterId;
  }

  public String getQueueName() {
    return queueName;
  }

  public void setQueueName(String queueName) {
    this.queueName = queueName;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((queueMasterId == null) ? 0 : queueMasterId.hashCode());
    result = prime * result + ((queueName == null) ? 0 : queueName.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    QueuesMaster other = (QueuesMaster) obj;
    if (queueMasterId == null) {
      if (other.queueMasterId != null) return false;
    } else if (!queueMasterId.equals(other.queueMasterId)) return false;
    if (queueName == null) {
      if (other.queueName != null) return false;
    } else if (!queueName.equals(other.queueName)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "QueuesMaster [queueMasterId="
        + queueMasterId
        + ", queueName="
        + queueName
        + ", status="
        + status
        + "]";
  }
}

package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.ROLE_ACTION_MAPPINGS)
public class RoleActionMappings {

  @Id private String roleActionId;
  private String roleMasterId;
  private String actionId;
  private String insertTs;
  private String insertedBy;
  private String updatedBy;
  private String updatedTs;

  public String getRoleActionId() {
    return roleActionId;
  }

  public void setRoleActionId(String roleActionId) {
    this.roleActionId = roleActionId;
  }

  public String getRoleMasterId() {
    return roleMasterId;
  }

  public void setRoleMasterId(String roleMasterId) {
    this.roleMasterId = roleMasterId;
  }

  public String getActionId() {
    return actionId;
  }

  public void setActionId(String actionId) {
    this.actionId = actionId;
  }

  public String getInsertTs() {
    return insertTs;
  }

  public void setInsertTs(String insertTs) {
    this.insertTs = insertTs;
  }

  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedTs() {
    return updatedTs;
  }

  public void setUpdatedTs(String updatedTs) {
    this.updatedTs = updatedTs;
  }

  @Override
  public String toString() {
    return "RoleActionMappings [roleActionId="
        + roleActionId
        + ", roleMasterId="
        + roleMasterId
        + ", actionId="
        + actionId
        + ", insertTs="
        + insertTs
        + ", insertedBy="
        + insertedBy
        + ", updatedBy="
        + updatedBy
        + ", updatedTs="
        + updatedTs
        + "]";
  }
}

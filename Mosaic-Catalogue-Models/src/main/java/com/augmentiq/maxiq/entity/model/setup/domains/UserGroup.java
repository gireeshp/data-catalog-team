package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.USER_GROUP)
public class UserGroup {

  @Id private Long groupId;
  private String appId;
  private String groupName;
  private String description;
  private String queueMasterID;
  private String insertedBy;
  private String insertTs;
  private String updatedBy;
  private String updateTs;
  private Long rangerGroupId;
  private Long rangerGroupUserId;
  private String rangerGroupName;

  public String getRangerGroupName() {
    return rangerGroupName;
  }

  public void setRangerGroupName(String rangerGroupName) {
    this.rangerGroupName = rangerGroupName;
  }

  public Long getRangerGroupUserId() {
    return rangerGroupUserId;
  }

  public void setRangerGroupUserId(Long rangerGroupUserId) {
    this.rangerGroupUserId = rangerGroupUserId;
  }

  public Long getRangerGroupId() {
    return rangerGroupId;
  }

  public void setRangerGroupId(Long rangerGroupId) {
    this.rangerGroupId = rangerGroupId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }
  //private String queueName;
  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getAppId() {
    return appId;
  }

  public void setAppName(String appId) {
    this.appId = appId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getQueueMasterID() {
    return queueMasterID;
  }

  public void setQueueMasterID(String queueMasterID) {
    this.queueMasterID = queueMasterID;
  }

  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getInsertTs() {
    return insertTs;
  }

  public void setInsertTs(String insertTs) {
    this.insertTs = insertTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdateTs() {
    return updateTs;
  }

  public void setUpdateTs(String updateTs) {
    this.updateTs = updateTs;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
    result = prime * result + ((insertTs == null) ? 0 : insertTs.hashCode());
    result = prime * result + ((insertedBy == null) ? 0 : insertedBy.hashCode());
    result = prime * result + ((queueMasterID == null) ? 0 : queueMasterID.hashCode());
    result = prime * result + ((updateTs == null) ? 0 : updateTs.hashCode());
    result = prime * result + ((updatedBy == null) ? 0 : updatedBy.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    UserGroup other = (UserGroup) obj;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (groupName == null) {
      if (other.groupName != null) return false;
    } else if (!groupName.equals(other.groupName)) return false;
    if (insertTs == null) {
      if (other.insertTs != null) return false;
    } else if (!insertTs.equals(other.insertTs)) return false;
    if (insertedBy == null) {
      if (other.insertedBy != null) return false;
    } else if (!insertedBy.equals(other.insertedBy)) return false;
    if (queueMasterID == null) {
      if (other.queueMasterID != null) return false;
    } else if (!queueMasterID.equals(other.queueMasterID)) return false;
    if (updateTs == null) {
      if (other.updateTs != null) return false;
    } else if (!updateTs.equals(other.updateTs)) return false;
    if (updatedBy == null) {
      if (other.updatedBy != null) return false;
    } else if (!updatedBy.equals(other.updatedBy)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "UserGroup [groupId="
        + groupId
        + ", groupName="
        + groupName
        + ", description="
        + description
        + ", queueMasterID="
        + queueMasterID
        + ", insertedBy="
        + insertedBy
        + ", insertTs="
        + insertTs
        + ", updatedBy="
        + updatedBy
        + ", updateTs="
        + updateTs
        + "]";
  }
}

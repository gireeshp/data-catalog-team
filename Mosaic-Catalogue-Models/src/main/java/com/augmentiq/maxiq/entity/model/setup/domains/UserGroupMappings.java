package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.USER_GROUP_MAPPINGS)
public class UserGroupMappings {

  @Id private Long unqUserGroupMapId;
  private Long userId;
  private Long groupId;
  private String inserteBy;
  private String insertTs;
  private String updatedBy;
  private String updatedTs;

  public Long getUnqUserGroupMapId() {
    return unqUserGroupMapId;
  }

  public void setUnqUserGroupMapId(Long unqUserGroupMapId) {
    this.unqUserGroupMapId = unqUserGroupMapId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getInserteBy() {
    return inserteBy;
  }

  public void setInserteBy(String inserteBy) {
    this.inserteBy = inserteBy;
  }

  public String getInsertTs() {
    return insertTs;
  }

  public void setInsertTs(String insertTs) {
    this.insertTs = insertTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedTs() {
    return updatedTs;
  }

  public void setUpdatedTs(String updatedTs) {
    this.updatedTs = updatedTs;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("UserGroupMappings [unqUserGroupMapId=");
    builder.append(unqUserGroupMapId);
    builder.append(", userId=");
    builder.append(userId);
    builder.append(", groupId=");
    builder.append(groupId);
    builder.append(", inserteBy=");
    builder.append(inserteBy);
    builder.append(", insertTs=");
    builder.append(insertTs);
    builder.append(", updatedBy=");
    builder.append(updatedBy);
    builder.append(", updatedTs=");
    builder.append(updatedTs);
    builder.append("]");
    return builder.toString();
  }
}

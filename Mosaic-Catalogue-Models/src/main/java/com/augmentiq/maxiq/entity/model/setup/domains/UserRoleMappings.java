package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.USER_ROLE_MAPPINGS)
public class UserRoleMappings {

  @Id private Long unqUserRoleMapId;
  private Long userId;
  private Long roleId;
  private String insertedBy;
  private String insertTs;
  private String updatedBy;
  private String updatedTs;

  public Long getUnqUserRoleMapId() {
    return unqUserRoleMapId;
  }

  public void setUnqUserRoleMapId(Long unqUserRoleMapId) {
    this.unqUserRoleMapId = unqUserRoleMapId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getRoleId() {
    return roleId;
  }

  public void setRoleId(Long roleId) {
    this.roleId = roleId;
  }

  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getInsertTs() {
    return insertTs;
  }

  public void setInsertTs(String insertTs) {
    this.insertTs = insertTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedTs() {
    return updatedTs;
  }

  public void setUpdatedTs(String updatedTs) {
    this.updatedTs = updatedTs;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("UserRoleMappings [unqUserRoleMapId=");
    builder.append(unqUserRoleMapId);
    builder.append(", userId=");
    builder.append(userId);
    builder.append(", roleId=");
    builder.append(roleId);
    builder.append(", insertedBy=");
    builder.append(insertedBy);
    builder.append(", insertTs=");
    builder.append(insertTs);
    builder.append(", updatedBy=");
    builder.append(updatedBy);
    builder.append(", updatedTs=");
    builder.append(updatedTs);
    builder.append("]");
    return builder.toString();
  }
}

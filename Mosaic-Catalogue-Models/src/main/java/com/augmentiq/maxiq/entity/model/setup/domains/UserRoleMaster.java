package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.ROLE_MASTER)
public class UserRoleMaster {

  @Id private Long unqRoleMasterId;
  private String roleName;
  private String status;
  private String roleDescription;
  private String createdBy;
  private String createdTs;
  private String updatedBy;
  private String updatedTs;
  private Integer appId;

  public Integer getAppId() {
    return appId;
  }

  public void setAppId(Integer appId) {
    this.appId = appId;
  }

  public Long getUnqRoleMasterId() {
    return unqRoleMasterId;
  }

  public void setUnqRoleMasterId(Long unqRoleMasterId) {
    this.unqRoleMasterId = unqRoleMasterId;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRoleDescription() {
    return roleDescription;
  }

  public void setRoleDescription(String roleDescription) {
    this.roleDescription = roleDescription;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(String createdTs) {
    this.createdTs = createdTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedTs() {
    return updatedTs;
  }

  public void setUpdatedTs(String updatedTs) {
    this.updatedTs = updatedTs;
  }

  @Override
  public String toString() {
    return "UserRoleMaster [unqRoleMasterId="
        + unqRoleMasterId
        + ", roleName="
        + roleName
        + ", status="
        + status
        + ", roleDescription="
        + roleDescription
        + ", createdBy="
        + createdBy
        + ", createdTs="
        + createdTs
        + ", updatedBy="
        + updatedBy
        + ", updatedTs="
        + updatedTs
        + ", appId="
        + appId
        + "]";
  }
}

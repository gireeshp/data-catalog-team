package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.USER_SUBGROUP_MAPPINGS)
public class UserSubGroupMappings {

  @Id private Long unqUserSubgrpMapId;
  private Long userId;
  private Long subgroupId;
  private String insertTs;
  private String insertedBy;
  private String updateTs;
  private String updatedBy;

  public Long getUnqUserSubgrpMapId() {
    return unqUserSubgrpMapId;
  }

  public void setUnqUserSubgrpMapId(Long unqUserSubgrpMapId) {
    this.unqUserSubgrpMapId = unqUserSubgrpMapId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getSubgroupId() {
    return subgroupId;
  }

  public void setSubgroupId(Long subgroupId) {
    this.subgroupId = subgroupId;
  }

  public String getInsertTs() {
    return insertTs;
  }

  public void setInsertTs(String insertTs) {
    this.insertTs = insertTs;
  }

  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getUpdateTs() {
    return updateTs;
  }

  public void setUpdateTs(String updateTs) {
    this.updateTs = updateTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("UserSubGroupMappings [unqUserSubgrpMapId=");
    builder.append(unqUserSubgrpMapId);
    builder.append(", userId=");
    builder.append(userId);
    builder.append(", subgroupId=");
    builder.append(subgroupId);
    builder.append(", insertTs=");
    builder.append(insertTs);
    builder.append(", insertedBy=");
    builder.append(insertedBy);
    builder.append(", updateTs=");
    builder.append(updateTs);
    builder.append(", updatedBy=");
    builder.append(updatedBy);
    builder.append("]");
    return builder.toString();
  }
}

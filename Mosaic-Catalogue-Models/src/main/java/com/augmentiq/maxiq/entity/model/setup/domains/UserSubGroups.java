package com.augmentiq.maxiq.entity.model.setup.domains;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.USER_SUB_GROUP)
public class UserSubGroups {

  @Id private Long subGroupId;
  private String subGroupName;
  //private String groupName;
  private String subGroupDescription;
  private Long usergroupId;
  private String insertedBy;
  private String insertTs;
  private String updatedBy;
  private String updateTs;
  private String queueMasterID;
  //	private String appId;
  //
  //	public String getAppId() {
  //		return appId;
  //	}
  //	public void setAppId(String appId) {
  //		this.appId = appId;
  //	}

  public Long getSubGroupId() {
    return subGroupId;
  }

  public void setSubGroupId(Long subGroupId) {
    this.subGroupId = subGroupId;
  }

  public String getSubGroupName() {
    return subGroupName;
  }

  public void setSubGroupName(String subGroupName) {
    this.subGroupName = subGroupName;
  }

  public String getSubGroupDescription() {
    return subGroupDescription;
  }

  public void setSubGroupDescription(String subGroupDescription) {
    this.subGroupDescription = subGroupDescription;
  }

  public Long getUsergroupId() {
    return usergroupId;
  }

  public void setUsergroupId(Long usergroupId) {
    this.usergroupId = usergroupId;
  }

  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }

  public String getInsertTs() {
    return insertTs;
  }

  public void setInsertTs(String insertTs) {
    this.insertTs = insertTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdateTs() {
    return updateTs;
  }

  public void setUpdateTs(String updateTs) {
    this.updateTs = updateTs;
  }

  public String getQueueMasterID() {
    return queueMasterID;
  }

  public void setQueueMasterID(String queueMasterID) {
    this.queueMasterID = queueMasterID;
  }

  @Override
  public String toString() {
    return "UserSubGroups [subGroupId="
        + subGroupId
        + ", subGroupName="
        + subGroupName
        + ", subGroupDescription="
        + subGroupDescription
        + ", usergroupId="
        + usergroupId
        + ", insertedBy="
        + insertedBy
        + ", insertTs="
        + insertTs
        + ", updatedBy="
        + updatedBy
        + ", updateTs="
        + updateTs
        + ", queueMasterID="
        + queueMasterID
        + "]";
  }
}

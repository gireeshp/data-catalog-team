package com.augmentiq.maxiq.entity.model.user.bean;

import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

@TableName(tableName = Sequences.APPLICATION_USER)
public class ApplicationUser implements Comparable<ApplicationUser> {

  @Id private Long unqUserId;
  private String emailId;
  private String userId;
  private Long userUniqueId;
  private String password;
  private String firstName;
  private String lastName;
  private String status;
  private Long userCreatedDate;
  private List<String> userBrowsers;
  private List<String> userPlatform;
  private List<String> userips;
  private Long passwordexpirydays;
  private Long groupId;
  private String groupName;
  private String createdBy;
  private String createdTs;
  private String updatedBy;
  private String updatedTs;
  private Long userRangerId;
  private String rangerUserName;
  private String lockInd;
  private String lastFailureAttemptTs;
  private Long loginFailureCount;

  public String getLockInd() {
    return lockInd;
  }

  public void setLockInd(String lockIndNew) {
    lockInd = lockIndNew;
  }

  public String getLastFailureAttemptTs() {
    return lastFailureAttemptTs;
  }

  public void setLastFailureAttemptTs(String lastFailureAttemptTs) {
    this.lastFailureAttemptTs = lastFailureAttemptTs;
  }

  public Long getLoginFailureCount() {
    return loginFailureCount;
  }

  public void setLoginFailureCount(Long loginFailureCount) {
    this.loginFailureCount = loginFailureCount;
  }

  public String getRangerUserName() {
    return rangerUserName;
  }

  public void setRangerUserName(String rangerUserName) {
    this.rangerUserName = rangerUserName;
  }

  public Long getUserRangerId() {
    return userRangerId;
  }

  public void setUserRangerId(Long userRangerId) {
    this.userRangerId = userRangerId;
  }

  private String passwordRefreshedDate;
  private Long lastUsedPersonaId;

  public Long getLastUsedPersonaId() {
    return lastUsedPersonaId;
  }

  public void setLastUsedPersonaId(Long lastUsedPersonaId) {
    this.lastUsedPersonaId = lastUsedPersonaId;
  }

  public String getPasswordRefreshedDate() {
    return passwordRefreshedDate;
  }

  public void setPasswordRefreshedDate(String passwordRefreshedDate) {
    this.passwordRefreshedDate = passwordRefreshedDate;
  }

  public Long getUnqUserId() {
    return unqUserId;
  }

  public void setUnqUserId(Long unqUserId) {
    this.unqUserId = unqUserId;
  }

  public String getEmailId() {
    return emailId;
  }

  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Long getUserUniqueId() {
    return userUniqueId;
  }

  public void setUserUniqueId(Long userUniqueId) {
    this.userUniqueId = userUniqueId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Long getUserCreatedDate() {
    return userCreatedDate;
  }

  public void setUserCreatedDate(Long userCreatedDate) {
    this.userCreatedDate = userCreatedDate;
  }

  public List<String> getUserBrowsers() {
    return userBrowsers;
  }

  public void setUserBrowsers(List<String> userBrowsers) {
    this.userBrowsers = userBrowsers;
  }

  public List<String> getUserPlatform() {
    return userPlatform;
  }

  public void setUserPlatform(List<String> userPlatform) {
    this.userPlatform = userPlatform;
  }

  public List<String> getUserips() {
    return userips;
  }

  public void setUserips(List<String> userips) {
    this.userips = userips;
  }

  public Long getPasswordexpirydays() {
    return passwordexpirydays;
  }

  public void setPasswordexpirydays(Long passwordexpirydays) {
    this.passwordexpirydays = passwordexpirydays;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(String createdTs) {
    this.createdTs = createdTs;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public String getUpdatedTs() {
    return updatedTs;
  }

  public void setUpdatedTs(String updatedTs) {
    this.updatedTs = updatedTs;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((createdTs == null) ? 0 : createdTs.hashCode());
    result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
    result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
    result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
    result = prime * result + ((lastUsedPersonaId == null) ? 0 : lastUsedPersonaId.hashCode());
    result = prime * result + ((password == null) ? 0 : password.hashCode());
    result =
        prime * result + ((passwordRefreshedDate == null) ? 0 : passwordRefreshedDate.hashCode());
    result = prime * result + ((passwordexpirydays == null) ? 0 : passwordexpirydays.hashCode());
    result = prime * result + ((rangerUserName == null) ? 0 : rangerUserName.hashCode());
    result = prime * result + ((status == null) ? 0 : status.hashCode());
    result = prime * result + ((unqUserId == null) ? 0 : unqUserId.hashCode());
    result = prime * result + ((updatedBy == null) ? 0 : updatedBy.hashCode());
    result = prime * result + ((updatedTs == null) ? 0 : updatedTs.hashCode());
    result = prime * result + ((userBrowsers == null) ? 0 : userBrowsers.hashCode());
    result = prime * result + ((userCreatedDate == null) ? 0 : userCreatedDate.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    result = prime * result + ((userPlatform == null) ? 0 : userPlatform.hashCode());
    result = prime * result + ((userRangerId == null) ? 0 : userRangerId.hashCode());
    result = prime * result + ((userUniqueId == null) ? 0 : userUniqueId.hashCode());
    result = prime * result + ((userips == null) ? 0 : userips.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    ApplicationUser other = (ApplicationUser) obj;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (createdTs == null) {
      if (other.createdTs != null) return false;
    } else if (!createdTs.equals(other.createdTs)) return false;
    if (emailId == null) {
      if (other.emailId != null) return false;
    } else if (!emailId.equals(other.emailId)) return false;
    if (firstName == null) {
      if (other.firstName != null) return false;
    } else if (!firstName.equals(other.firstName)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (groupName == null) {
      if (other.groupName != null) return false;
    } else if (!groupName.equals(other.groupName)) return false;
    if (lastName == null) {
      if (other.lastName != null) return false;
    } else if (!lastName.equals(other.lastName)) return false;
    if (lastUsedPersonaId == null) {
      if (other.lastUsedPersonaId != null) return false;
    } else if (!lastUsedPersonaId.equals(other.lastUsedPersonaId)) return false;
    if (password == null) {
      if (other.password != null) return false;
    } else if (!password.equals(other.password)) return false;
    if (passwordRefreshedDate == null) {
      if (other.passwordRefreshedDate != null) return false;
    } else if (!passwordRefreshedDate.equals(other.passwordRefreshedDate)) return false;
    if (passwordexpirydays == null) {
      if (other.passwordexpirydays != null) return false;
    } else if (!passwordexpirydays.equals(other.passwordexpirydays)) return false;
    if (rangerUserName == null) {
      if (other.rangerUserName != null) return false;
    } else if (!rangerUserName.equals(other.rangerUserName)) return false;
    if (status == null) {
      if (other.status != null) return false;
    } else if (!status.equals(other.status)) return false;
    if (unqUserId == null) {
      if (other.unqUserId != null) return false;
    } else if (!unqUserId.equals(other.unqUserId)) return false;
    if (updatedBy == null) {
      if (other.updatedBy != null) return false;
    } else if (!updatedBy.equals(other.updatedBy)) return false;
    if (updatedTs == null) {
      if (other.updatedTs != null) return false;
    } else if (!updatedTs.equals(other.updatedTs)) return false;
    if (userBrowsers == null) {
      if (other.userBrowsers != null) return false;
    } else if (!userBrowsers.equals(other.userBrowsers)) return false;
    if (userCreatedDate == null) {
      if (other.userCreatedDate != null) return false;
    } else if (!userCreatedDate.equals(other.userCreatedDate)) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    if (userPlatform == null) {
      if (other.userPlatform != null) return false;
    } else if (!userPlatform.equals(other.userPlatform)) return false;
    if (userRangerId == null) {
      if (other.userRangerId != null) return false;
    } else if (!userRangerId.equals(other.userRangerId)) return false;
    if (userUniqueId == null) {
      if (other.userUniqueId != null) return false;
    } else if (!userUniqueId.equals(other.userUniqueId)) return false;
    if (userips == null) {
      if (other.userips != null) return false;
    } else if (!userips.equals(other.userips)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "ApplicationUser [unqUserId="
        + unqUserId
        + ", emailId="
        + emailId
        + ", userId="
        + userId
        + ", userUniqueId="
        + userUniqueId
        + ", password="
        + password
        + ", firstName="
        + firstName
        + ", lastName="
        + lastName
        + ", status="
        + status
        + ", userCreatedDate="
        + userCreatedDate
        + ", userBrowsers="
        + userBrowsers
        + ", userPlatform="
        + userPlatform
        + ", userips="
        + userips
        + ", passwordexpirydays="
        + passwordexpirydays
        + ", groupId="
        + groupId
        + ", groupName="
        + groupName
        + ", createdBy="
        + createdBy
        + ", createdTs="
        + createdTs
        + ", updatedBy="
        + updatedBy
        + ", updatedTs="
        + updatedTs
        + ", userRangerId="
        + userRangerId
        + ", rangerUserName="
        + rangerUserName
        + ", passwordRefreshedDate="
        + passwordRefreshedDate
        + ", lastUsedPersonaId="
        + lastUsedPersonaId
        + "]";
  }

  @Override
  public int compareTo(ApplicationUser obj) {
    return this.userUniqueId > obj.userUniqueId ? 1 : this.userUniqueId > obj.userUniqueId ? -1 : 0;
  }
}

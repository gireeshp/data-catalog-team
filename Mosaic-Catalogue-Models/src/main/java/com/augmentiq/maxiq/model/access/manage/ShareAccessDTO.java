package com.augmentiq.maxiq.model.access.manage;

import java.util.Map;

import com.augmentiq.maxiq.constant.configuration.enums.AccessTypeEnum;
/** @author Rushi created on Jul 17, 2017 for MAX-1058 */
public class ShareAccessDTO {

  private Map<Long, AccessTypeEnum> groupAccessMap;
  private Map<Long, AccessTypeEnum> userAccessMap;

  public Map<Long, AccessTypeEnum> getGroupAccessMap() {
    return groupAccessMap;
  }

  public void setGroupAccessMap(Map<Long, AccessTypeEnum> groupAccessMap) {
    this.groupAccessMap = groupAccessMap;
  }

  public Map<Long, AccessTypeEnum> getUserAccessMap() {
    return userAccessMap;
  }

  public void setUserAccessMap(Map<Long, AccessTypeEnum> userAccessMap) {
    this.userAccessMap = userAccessMap;
  }
}

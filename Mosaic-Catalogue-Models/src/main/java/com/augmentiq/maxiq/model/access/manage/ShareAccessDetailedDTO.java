package com.augmentiq.maxiq.model.access.manage;

import java.util.Map;

public class ShareAccessDetailedDTO extends ShareAccessDTO {

  private Map<Long, String> userProjectMap;
  private Map<Long, String> groupProjectMap;

  public Map<Long, String> getUserProjectMap() {
    return userProjectMap;
  }

  public void setUserProjectMap(Map<Long, String> userProjectMap) {
    this.userProjectMap = userProjectMap;
  }

  public Map<Long, String> getGroupProjectMap() {
    return groupProjectMap;
  }

  public void setGroupProjectMap(Map<Long, String> groupProjectMap) {
    this.groupProjectMap = groupProjectMap;
  }
}

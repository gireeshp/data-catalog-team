package com.augmentiq.maxiq.model.apps.domain;

import java.io.Serializable;

public class FieldNameValue implements Serializable {
  private static final long serialVersionUID = 1L;
  String fieldName_;
  String fieldValue_;

  public String getFieldName() {
    return fieldName_;
  }

  public void setFieldName(String fieldName) {
    fieldName_ = fieldName;
  }

  public String getFieldValue() {
    return fieldValue_;
  }

  public void setFieldValue(String fieldValue) {
    fieldValue_ = fieldValue;
  }

  @Override
  public String toString() {
    return "FieldNameValue [fieldName_=" + fieldName_ + ", fieldValue_=" + fieldValue_ + "]";
  }
}

package com.augmentiq.maxiq.model.apps.domain;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GlobalParams;
import com.opencsv.CSVReader;
/** Changed for MAX-491 By Rushikesh Raut on 30-Sept-2016 */
public class StringRec implements Serializable {

  private static final Logger logger = LoggerFactory.getLogger(StringRec.class);

  private static final long serialVersionUID = 1L;

  private String rec_;
  private String[] splitRec_;

  public String getRec() {
    return rec_;
  }

  public void setRec(String rec) {
    rec_ = rec;
  }

  public StringRec() {}

  public StringRec(String[] strings) {
    splitRec_ = strings;
  }

  public StringRec(String rec, String del, Boolean optionallyEnclosedInDoubleQuotes) {
    if (optionallyEnclosedInDoubleQuotes) {
      splitRec_ = getTokenisedStringArrayFromStringUsingDelimiter(rec, del);
    } else {
      splitRec_ = StringUtils.splitByWholeSeparatorPreserveAllTokens(rec, del);
    }
  }

  public String getNthField(int index) {
    if (index < splitRec_.length) return splitRec_[index] + "";
    else {
      return "";
    }
  }

  public String getNthField(Long index) {
    if (index < splitRec_.length) return splitRec_[Integer.parseInt(index + "")] + "";
    else {
      return null;
    }
  }

  @Override
  public String toString() {

    StringBuilder builder = new StringBuilder();

    if (null != splitRec_ && splitRec_.length > 0) {
      for (String values : splitRec_) {
        builder.append(values).append(GlobalParams.MAXIQ_DEL);
      }
    }
    return builder.toString();
  }

  public String[] getSplitRec() {
    return splitRec_;
  }

  public void setSplitRec(String[] splitRec) {
    splitRec_ = splitRec;
  }

  public int getLength() {
    return splitRec_.length;
  }

  public String truncateLastField() {
    StringBuilder rec = new StringBuilder();

    for (int i = 0; i < getLength() - 1; i++) {
      rec.append(getNthField(i));
      if (i != getLength() - 2) rec.append(GlobalParams.MAXIQ_DEL);
    }
    return rec.toString();
  }
  /**
   * @param inputString
   * @param delimiter
   * @return tokenised array created by splitting the input string
   */
  public String[] getTokenisedStringArrayFromStringUsingDelimiter(
      String inputString, String delimiter) {
    String[] tokenisedString = null;
    if (!delimiter.equals(null)) {
      if (1 == delimiter.length() || delimiter.equalsIgnoreCase("\\t")) {
        char delimiterInChar = getCharFromString(delimiter);
        inputString = performPreOperations(inputString, delimiterInChar);
        CSVReader reader = new CSVReader(new StringReader(inputString), delimiterInChar);
        try {
          tokenisedString = reader.readNext();
          reader.close();

        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      } else {
        // If in case delimiter length is greater than 1
        tokenisedString =
            StringUtils.splitByWholeSeparatorPreserveAllTokens(inputString, delimiter);
      }
      if (tokenisedString == null) {
        inputString = StringUtils.replace(inputString, "\"", "");
        tokenisedString =
            StringUtils.splitByWholeSeparatorPreserveAllTokens(inputString, delimiter);
      }
    }
    return tokenisedString;
  }

  public static char getCharFromString(String InputString) {
    char CharToReturn = '\0';
    if (1 == InputString.length()) {
      // all the delimiters such as , | and \t are handled here
      CharToReturn = InputString.charAt(0);
    } else if (InputString.equalsIgnoreCase("\\t")) {
      // extra check for \t
      CharToReturn = '\t';
    }
    return CharToReturn;
  }

  private static String performPreOperations(String inputString, char delimiter) {
    if (inputString.contains("\"")) {
      for (int i = 1; i < inputString.length() - 1; i++) {
        // if " is in between two delimiters
        if ('\"' == inputString.charAt(i)) {
          if (delimiter != inputString.charAt(i - 1)
              && delimiter != inputString.charAt(i + 1)
              && '\"' != inputString.charAt(i - 1)) {
            // replace \" with \"\"
            inputString = inputString.substring(0, i) + "\"\"" + inputString.substring(i + 1);
          }
        }
      }
    }
    return inputString;
  }
}

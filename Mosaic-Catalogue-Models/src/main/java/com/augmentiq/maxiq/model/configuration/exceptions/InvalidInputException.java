package com.augmentiq.maxiq.model.configuration.exceptions;

public class InvalidInputException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public InvalidInputException() {
    super();
  }

  public InvalidInputException(String msg) {
    super(msg);
  }

  public InvalidInputException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

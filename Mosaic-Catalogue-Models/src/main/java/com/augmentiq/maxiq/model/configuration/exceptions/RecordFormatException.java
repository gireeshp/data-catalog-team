package com.augmentiq.maxiq.model.configuration.exceptions;

public class RecordFormatException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public RecordFormatException() {
    super();
  }

  public RecordFormatException(String msg) {
    super(msg);
  }

  public RecordFormatException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

package com.augmentiq.maxiq.model.configuration.exceptions;

import com.augmentiq.maxiq.constant.models.errors.enums.ErrorTypes;

public class RecordProcessorException extends Exception {
  /** */
  private static final long serialVersionUID = 1L;

  public ErrorTypes errorType;
  public String errorField;
  public String errorFieldValue;

  public RecordProcessorException() {}

  public RecordProcessorException(ErrorTypes errorType_) {
    this.errorType = errorType_;
  }

  public RecordProcessorException(
      String errorField_, ErrorTypes errorType_, String errorFieldValue) {
    this.errorField = errorField_;
    this.errorType = errorType_;
    this.errorFieldValue = errorFieldValue;
  }

  public RecordProcessorException(String errorField_) {
    this.errorField = errorField_;
  }

  public String getMessage() {
    return "RecordProcessorException [errorType="
        + errorType
        + ", errorField="
        + errorField
        + ", errorFieldValue="
        + errorFieldValue
        + "]";
  }
}

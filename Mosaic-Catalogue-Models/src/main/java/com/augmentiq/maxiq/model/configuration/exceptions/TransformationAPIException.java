package com.augmentiq.maxiq.model.configuration.exceptions;

public class TransformationAPIException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public TransformationAPIException() {
    super();
  }

  public TransformationAPIException(String msg) {
    super(msg);
  }

  public TransformationAPIException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

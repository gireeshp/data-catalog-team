package com.augmentiq.maxiq.model.configuration.exceptions;

public class TransformationSetupException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public TransformationSetupException() {
    super();
  }

  public TransformationSetupException(String msg) {
    super(msg);
  }

  public TransformationSetupException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

package com.augmentiq.maxiq.model.configuration.factory;

import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepository;

/**
 * This is the Factory for creating DataRepository Object
 *
 * @author shiva
 */
public class DataRepositoryFactory {

  public DataRepository getDataRepository() {
    return new DataRepository();
  }
}

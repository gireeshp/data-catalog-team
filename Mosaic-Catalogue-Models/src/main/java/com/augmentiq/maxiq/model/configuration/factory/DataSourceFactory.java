package com.augmentiq.maxiq.model.configuration.factory;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.RandomUtility;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;

/**
 * A factory design pattern to create DataSource Objects
 *
 * @author shiva
 */
public class DataSourceFactory {
  private static final Logger logger = LoggerFactory.getLogger(DataSourceFactory.class);

  public DataSource getDataSource() {
    return new DataSource();
  }

  public DataSource getDataSource(
      String dataSourceName, String userId, String ingection, Long groupId) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getDataSource()"
            + ParamUtils.getString(dataSourceName, userId, ingection, groupId));
    DataSource dataSource = getDataSource();

    dataSource.setCreatedBy(StringUtils.isNotBlank(userId) ? userId : "");

    dataSource.setDataSourceName(dataSourceName);

    dataSource.setIngection(ingection);

    dataSource.setDataSourceIdentifier(RandomUtility.getRandomString(5));
    dataSource.setDataSourceName(dataSourceName);

    dataSource.setId(null);
    dataSource.setDsLevelParams(
        InputParamsFactory.getListOfInputParamsByValue(CHAR_CONSTANTS.BLANK, null));
    dataSource.setNoOfActiveInstances(0);
    dataSource.setTypeOfActiveInstances(GENERAL_CONSTANTS.DEFAULT);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getDataSource()" + ParamUtils.getString(dataSource));
    return dataSource;
  }
}

package com.augmentiq.maxiq.model.configuration.factory;

import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;

/**
 * Factory pattern for creating the fieldmapping object
 *
 * @author shiva
 */
public class FieldMappingFactory {

  /*	private static final Logger logger = LoggerFactory.getLogger(FieldMappingFactory.class);
   */ public FieldMapping getFieldMapping() {
    return new FieldMapping();
  }

  public FieldMapping getFieldMapping(
      Long dataSourceId, int position, String fieldName, Long fieldId) {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getFieldMapping()" + ParamUtils.getString(dataSourceId,position,fieldName));

    FieldMapping fieldMapping = getFieldMapping();

    fieldMapping.setDataSourceId(dataSourceId);

    fieldMapping.setFieldName(fieldName);

    fieldMapping.setFieldDataType(FieldDataTypes.STRING);

    fieldMapping.setPosition((long) position);

    fieldMapping.setId(fieldId);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getFieldMapping()" + ParamUtils.getString(fieldMapping));
    return fieldMapping;
  }
}

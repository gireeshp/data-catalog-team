package com.augmentiq.maxiq.model.configuration.factory;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.constant.configuration.InputParamTypes;
import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.entity.model.configuration.bean.AppCombiner;
import com.augmentiq.maxiq.entity.model.configuration.bean.InputParameterDomain;
import com.augmentiq.maxiq.entity.model.configuration.bean.Transpose;

public class InputParamsFactory {

  private static final Logger logger = LoggerFactory.getLogger(InputParamsFactory.class);

  /**
   * Return list of AppCombiner for flow with default initialization of passed value
   *
   * @param updateRunDateTime
   * @param succeededTime - send null if no succeeded time means first time run or existing
   *     datasource without system generated parameters
   * @return
   */
  public static List<AppCombiner> getDefaultListOfAppCombinerByTime(
      String updateRunDateTime, String succeededTime) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getDefaultListOfAppCombinerByTime()"
            + ParamUtils.getString(updateRunDateTime, succeededTime));
    List<AppCombiner> listSystemGlobalParams = new LinkedList<AppCombiner>();
    listSystemGlobalParams.add(
        new AppCombiner(
            Constants.GP_CURRENT_RUN_DATETIME,
            FieldDataTypes.TIMESTAMP.toString(),
            InputParamTypes.SYSTEM,
            new Transpose(),
            updateRunDateTime));
    listSystemGlobalParams.add(
        new AppCombiner(
            Constants.GP_LAST_RUN_DATETIME,
            FieldDataTypes.TIMESTAMP.toString(),
            InputParamTypes.SYSTEM,
            new Transpose(),
            updateRunDateTime));
    listSystemGlobalParams.add(
        new AppCombiner(
            Constants.GP_LAST_SUCCESSFULLY_RUN_DATETIME,
            FieldDataTypes.TIMESTAMP.toString(),
            InputParamTypes.SYSTEM,
            new Transpose(),
            succeededTime != null ? succeededTime : CHAR_CONSTANTS.BLANK));
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getDefaultListOfAppCombinerByTime()"
            + ParamUtils.getString(listSystemGlobalParams));
    return listSystemGlobalParams;
  }

  /**
   * Return list of InputParameterDomain for Datasource with default initialization of passed value
   *
   * @param updateRunDateTime
   * @param succeededTime - send null if no succeeded time means first time run or existing
   *     datasource without system generated parameters
   * @return
   */
  public static List<InputParameterDomain> getListOfInputParamsByValue(
      final String updateRunDateTime, final String succeededTime) {
    List<InputParameterDomain> listSystemGlobalParams = new LinkedList<InputParameterDomain>();
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getListOfInputParamsByValue()"
            + ParamUtils.getString(updateRunDateTime, succeededTime));
    listSystemGlobalParams.add(
        new InputParameterDomain(
            Constants.GP_CURRENT_RUN_DATETIME, FieldDataTypes.TIMESTAMP, updateRunDateTime));
    listSystemGlobalParams.add(
        new InputParameterDomain(
            Constants.GP_LAST_RUN_DATETIME, FieldDataTypes.TIMESTAMP, updateRunDateTime));
    listSystemGlobalParams.add(
        new InputParameterDomain(
            Constants.GP_LAST_SUCCESSFULLY_RUN_DATETIME,
            FieldDataTypes.TIMESTAMP,
            succeededTime != null ? succeededTime : CHAR_CONSTANTS.BLANK));
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getListOfInputParamsByValue()"
            + ParamUtils.getString(listSystemGlobalParams));
    return listSystemGlobalParams;
  }
}

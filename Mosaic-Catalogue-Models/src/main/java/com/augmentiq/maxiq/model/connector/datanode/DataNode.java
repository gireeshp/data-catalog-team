package com.augmentiq.maxiq.model.connector.datanode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DataNode {

	/*
	 * Name of a Catalog data node - Could be a non leaf node like mysql schema
	 * node Or coudl be a leaf node like mysql table data source which can be
	 * published as Data Node in Catalog
	 */
	private String label;

	/* The connection details for this data node */
	// private ConnectorConfig config;

	/* true if the user has selected the checkbox to publish this node */
	private Boolean selected = false;
	private Boolean isDsNode = false;
	private Boolean __ivhTreeviewExpanded = false;
	private Boolean isPublished = false;
	private Boolean _genericField = false;
	
	private String dataSourceType;
	
	public String getDataSourceType() {
		return dataSourceType;
	}

	public void setDataSourceType(String dataSourceType) {
		this.dataSourceType = dataSourceType;
	}

	public Boolean getIsPublished() {
		return isPublished;
	}

	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	private Boolean __ivhTreeviewIndeterminate = false;

	private String ftpFileLocation;
	private String delimiter= null;
	private Boolean optionallyEnclosedInDoubleQuotes = false;
	
	// amazon s3 properties
	private String storagePath;
	private String bucketName;

	/* Collection of sub node, if the current node is not a leaf node */
	private Collection<DataNode> children = new HashSet<DataNode>();
	
	/*
	 * public static void main(String args[]) throws Exception { ConnectorDao
	 * dao = new ConnectorDao(); Gson gson = new Gson(); String str =
	 * gson.toJson(dao.getConnectors()); System.out.println(str); }
	 */
	
	public DataNode() {

	}

	/**
	 * Constructor to create Bucket S3 DataNode
	 * @param label
	 * @param isDsNode
	 * @param BucketName
	 */
	public DataNode(String label, Boolean isDsNode, String BucketName) {
		this.label = label;
		this.isDsNode = isDsNode;
		this.bucketName = BucketName;
	}
	
	public DataNode(String label, Boolean selected, Boolean isDsNode) {
		this.label = label;
		this.isDsNode = isDsNode;
		this.selected = selected;
	}

	public DataNode(String label, Boolean isDsNode) {
		this.label = label;
		this.isDsNode = isDsNode;

	}

	public Boolean get__ivhTreeviewExpanded() {
		return __ivhTreeviewExpanded;
	}

	public void set__ivhTreeviewExpanded(Boolean __ivhTreeviewExpanded) {
		this.__ivhTreeviewExpanded = __ivhTreeviewExpanded;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public Collection<DataNode> getChildren() {
		return children;
	}

	public void setChildren(Collection<DataNode> children) {
		this.children = children;
	}
	
	public Boolean getIsDsNode() {
		return isDsNode;
	}

	public void setIsDsNode(Boolean isDsNode) {
		this.isDsNode = isDsNode;
	}

	public Boolean get__ivhTreeviewIndeterminate() {
		return __ivhTreeviewIndeterminate;
	}

	public void set__ivhTreeviewIndeterminate(Boolean __ivhTreeviewIndeterminate) {
		this.__ivhTreeviewIndeterminate = __ivhTreeviewIndeterminate;
	}
	
	public void addChild(DataNode dataNode){
		synchronized (this) {
			this.getChildren().add(dataNode);
		}
	
	}
	
	public Boolean get_genericField() {
		return _genericField;
	}

	public void set_genericField(Boolean _genericField) {
		this._genericField = _genericField;
	}


	/* test case for function - getDsNodes */
	public static void main(String args[]) {
		DataNode r = new DataNode("root", false, false);
		DataNode f1 = new DataNode("f1", false, false);
		DataNode f2 = new DataNode("f2", false, false);
		DataNode f3 = new DataNode("f3", false, false);
		DataNode f1txt = new DataNode("f1text", true, true);
		DataNode f2txt = new DataNode("f2text", true, true);
		DataNode f3txt = new DataNode("f3text", true, true);
		DataNode schema = new DataNode("schema", false, false);
		DataNode database = new DataNode("maxiq", false, false);
		DataNode database1 = new DataNode("unitrax", false, false);
		DataNode child_tables = new DataNode("Tables", false, false);
		DataNode child_views = new DataNode("Views", false, false);
		DataNode child_tables1 = new DataNode("Tables", false, false);
		DataNode child_views1 = new DataNode("Views", false, false);
		DataNode Table1 = new DataNode("Table1", true, true);
		DataNode Table2 = new DataNode("Table2", false, true);
		DataNode Table3 = new DataNode("Table3", true, true);
		DataNode View1 = new DataNode("View1", false, true);
		DataNode View2 = new DataNode("View2", true, true);
		DataNode View3 = new DataNode("View3", false, true);
		DataNode Table4 = new DataNode("Table4", false, true);
		DataNode Table5 = new DataNode("Table5", true, true);
		DataNode Table6 = new DataNode("Table6", false, true);
		DataNode View4 = new DataNode("View4", false, true);
		DataNode View5 = new DataNode("View5", false, true);
		DataNode View6 = new DataNode("View6", false, true);

		child_tables.getChildren().add(Table1);
		child_tables.getChildren().add(Table2);
		child_tables.getChildren().add(Table3);

		child_views.getChildren().add(View1);
		child_views.getChildren().add(View2);
		child_views.getChildren().add(View3);
		
		child_tables1.getChildren().add(Table4);
		child_tables1.getChildren().add(Table5);
		child_tables1.getChildren().add(Table6);
		
		child_views1.getChildren().add(View4);
		child_views1.getChildren().add(View5);
		child_views1.getChildren().add(View6);
		
		database.getChildren().add(child_tables);
		database.getChildren().add(child_views);
		
		database1.getChildren().add(child_tables1);
		database1.getChildren().add(child_views1);
		
		
		schema.getChildren().add(database);
		schema.getChildren().add(database1);
		
		Collection<DataNode> t = new HashSet<DataNode>();
		t.add(f3txt);
		f3.setChildren(t);

		Collection<DataNode> t2 = new HashSet<DataNode>();
		t2.add(f2txt);
		t2.add(f3);
		f1.setChildren(t2);

		Collection<DataNode> t3 = new HashSet<DataNode>();
		t3.add(f1);
		t3.add(f2);
		t3.add(f1txt);
		r.setChildren(t3);
		Collection<DataNode> dn1 = new HashSet<DataNode>();
		dn1.add(r);
		dn1.add(schema);

		DataNode dn = new DataNode();
		DataNode filtereddn = dn.getSelectedNodes(schema);
		System.out.println("Json AFter Filteration : ");
		Collection<Map<String, List<String>>> dbWithTablesAsChild = dn.getDbWithTablesList(filtereddn);
		
		/*
		 * Collection<DataNode> cdn=dn.getSelectedNodes(dn1); Iterator<DataNode>
		 * it = cdn.iterator(); while(it.hasNext()){ printnodes(it.next()); }
		 */

		/*
		 * printnodes(f2); printnodes(f3); printnodes(f2txt); printnodes(r);
		 */

	}

	public static void printnodes(DataNode d) {
		Collection<DataNode> result = d.getDsNodes(d);
		System.out.println("-> " + d.getLabel());
		for (DataNode res : result) {
			System.out.println("==> " + res.getLabel());
		}
	}

	/**
	 * Test case: ftp folder structure -> folder1 -> file2.txt -> folder3 ->
	 * file3.txt -> folder2 -> file1.txt
	 * 
	 * DataNode to be publish => file1.txt, file2.txt and file3.txt
	 * 
	 * @param currentNode
	 * @return
	 */
	public Collection<DataNode> getDsNodes(DataNode currentNode) {

		Collection<DataNode> dataNodes = new HashSet<DataNode>();

		if (currentNode.isDsNode) {
			if (currentNode.selected) {
				// user selected data node to be published
				dataNodes.add(currentNode);
			}
		} else {
			Collection<DataNode> childNodes = currentNode.getChildren();
			for (DataNode dataNode : childNodes) {
				dataNodes.addAll(getDsNodes(dataNode));
			}
		}

		return dataNodes;
	}

	public DataNode getSelectedNodes(DataNode dataNode) {
		DataNode filterNodes = filterNodes(dataNode);
		return filterNodes;

	}

	public DataNode filterNodes(DataNode dataNode) {
		
		if (dataNode.selected) {// root selected
			return dataNode;

		} else if (dataNode.isDsNode && selected) {// single resource outside
														// any directory
			return dataNode;

		} else if (!dataNode.getChildren().isEmpty()) {

			DataNode currentNode = new DataNode();
			currentNode.setLabel(dataNode.getLabel());
			currentNode.setSelected(dataNode.getSelected());
			currentNode.set__ivhTreeviewExpanded(dataNode.get__ivhTreeviewExpanded());
			
			Collection<DataNode> childNodes = dataNode.getChildren();
			for (DataNode childNode : childNodes) {
				DataNode leafNode = filterNodes(childNode);
				if (leafNode != null) {
					
					currentNode.getChildren().add(leafNode);
					
				}
				
			}
			if (currentNode.getChildren().size() > 0) {
				return currentNode;
			}
			return null;
		}
		// if leaf node is not selected then return null to indicate that this
		// leaf node is not to be picked up
		return null;

	}

	public Collection<Map<String, List<String>>> getDbWithTablesList(DataNode dataNode) {
		Collection<DataNode> databaseList = dataNode.getChildren();// All
																	// databases
																	// node list
																	// will be
																	// stored
		Collection<Map<String, List<String>>> dbWithTablesAsChild = new ArrayList<Map<String, List<String>>>();
		if (databaseList != null && !databaseList.isEmpty()) {
			for (DataNode currentDatabase : databaseList) {
				Map<String, List<String>> currentDbWithtbldetails = new HashMap<>();
				String DatabaseName = currentDatabase.getLabel();// current db
				Collection<DataNode> TablesAndViewsList = currentDatabase.getChildren();// list
																						// of
																						// tables
																						// and
																						// vies
																						// in
																						// that
																						// db
				Iterator<DataNode> tblAndviewlistItr = TablesAndViewsList.iterator();
				List<String> tableNames = new ArrayList<>();
				while (tblAndviewlistItr.hasNext()) {
					Collection<DataNode> tableOrViewList = tblAndviewlistItr.next().getChildren();// list
																									// of
																									// table
																									// or
																									// view
					Iterator<DataNode> tbl0rviewItr = tableOrViewList.iterator();
					while (tbl0rviewItr.hasNext()) {
						DataNode currentTable = tbl0rviewItr.next();
						tableNames.add(currentTable.getLabel());
					}

				}
				currentDbWithtbldetails.put(DatabaseName, tableNames);
				dbWithTablesAsChild.add(currentDbWithtbldetails);
			}

		}
		return dbWithTablesAsChild;
	}

	public String getFtpFileLocation() {
		return ftpFileLocation;
	}

	public void setFtpFileLocation(String ftpFileLocation) {
		this.ftpFileLocation = ftpFileLocation;
	}

	public String getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public Boolean getOptionallyEnclosedInDoubleQuotes() {
		return optionallyEnclosedInDoubleQuotes;
	}

	public void setOptionallyEnclosedInDoubleQuotes(Boolean optionallyEnclosedInDoubleQuotes) {
		this.optionallyEnclosedInDoubleQuotes = optionallyEnclosedInDoubleQuotes;
	}

	public String getStoragePath() {
		return storagePath;
	}

	public void setStoragePath(String storagePath) {
		this.storagePath = storagePath;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
}

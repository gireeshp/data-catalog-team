package com.augmentiq.maxiq.model.databasefunctions.bean;

import java.sql.Timestamp;

public class DatabaseFunction {

  private int id;
  private String databaseFunctionsName;
  private String parameters;
  private String returnType;

  private int databaseFunctionTypeId;
  private String databaseFunctionType;

  private int databaseNameId;
  private String databaseName;
  private String className;
  private int databaseFunctionUdfsId;

  private int createdBy;
  private int updatedBy;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDatabaseFunctionsName() {
    return databaseFunctionsName;
  }

  public void setDatabaseFunctionsName(String databaseFunctionsName) {
    this.databaseFunctionsName = databaseFunctionsName;
  }

  public String getParameters() {
    return parameters;
  }

  public void setParameters(String parameters) {
    this.parameters = parameters;
  }

  public String getReturnType() {
    return returnType;
  }

  public void setReturnType(String returnType) {
    this.returnType = returnType;
  }

  public int getDatabaseFunctionTypeId() {
    return databaseFunctionTypeId;
  }

  public void setDatabaseFunctionTypeId(int databaseFunctionTypeId) {
    this.databaseFunctionTypeId = databaseFunctionTypeId;
  }

  public String getDatabaseFunctionType() {
    return databaseFunctionType;
  }

  public void setDatabaseFunctionType(String databaseFunctionType) {
    this.databaseFunctionType = databaseFunctionType;
  }

  public int getDatabaseNameId() {
    return databaseNameId;
  }

  public void setDatabaseNameId(int databaseNameId) {
    this.databaseNameId = databaseNameId;
  }

  public String getDatabaseName() {
    return databaseName;
  }

  public void setDatabaseName(String databaseName) {
    this.databaseName = databaseName;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public int getDatabaseFunctionUdfsId() {
    return databaseFunctionUdfsId;
  }

  public void setDatabaseFunctionUdfsId(int databaseFunctionUdfsId) {
    this.databaseFunctionUdfsId = databaseFunctionUdfsId;
  }

  public int getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(int createdBy) {
    this.createdBy = createdBy;
  }

  public int getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(int updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public String toString() {
    return "DatabaseFunction [id="
        + id
        + ", databaseFunctionsName="
        + databaseFunctionsName
        + ", parameters="
        + parameters
        + ", returnType="
        + returnType
        + ", databaseFunctionTypeId="
        + databaseFunctionTypeId
        + ", databaseFunctionType="
        + databaseFunctionType
        + ", databaseNameId="
        + databaseNameId
        + ", databaseName="
        + databaseName
        + ", className="
        + className
        + ", databaseFunctionUdfsId="
        + databaseFunctionUdfsId
        + ", createdBy="
        + createdBy
        + ", updatedBy="
        + updatedBy
        + "]";
  }
}

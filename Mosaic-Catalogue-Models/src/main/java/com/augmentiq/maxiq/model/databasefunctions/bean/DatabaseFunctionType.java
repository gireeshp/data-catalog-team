package com.augmentiq.maxiq.model.databasefunctions.bean;

import java.util.Date;

public class DatabaseFunctionType {

  private int id;

  private String name;
  private String createdBy;
  private String updatedBy;

  private Date createdOn;
  private Date updatedOn;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Date getUpdatedOn() {
    return updatedOn;
  }

  public void setUpdatedOn(Date updatedOn) {
    this.updatedOn = updatedOn;
  }

  @Override
  public String toString() {
    return "DatabaseFunctionType "
        + "[id="
        + id
        + ", name="
        + name
        + ", createdBy="
        + createdBy
        + ", updatedBy="
        + updatedBy
        + ", createdOn="
        + createdOn
        + ", updatedOn="
        + updatedOn
        + "]";
  }
}

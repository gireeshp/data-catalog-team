package com.augmentiq.maxiq.model.errors;

import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

import com.augmentiq.maxiq.enity.model.errors.LogObject;

public class SingeltonErrorLogger extends Thread {

  private static final Logger LOGGER = Logger.getLogger(LogObject.class.getName());

  private static SingeltonErrorLogger errorLogger;
  private static Queue<LogObject> logObjects;

  static {
    errorLogger = new SingeltonErrorLogger();
    logObjects = new LinkedList<LogObject>();
    errorLogger.start();
  }

  private SingeltonErrorLogger() {}

  public static void addToErrorQueue(LogObject logObject) {
    logObjects.add(logObject);
  }

  public void run() {

    while (true) {
      /*System.out.println("Started thread");*/

      while (logObjects.peek() != null) {
        LogObject logObject = logObjects.poll();

        
      }

      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      /*System.out.println("Re starting thread");*/
    }
  }
}

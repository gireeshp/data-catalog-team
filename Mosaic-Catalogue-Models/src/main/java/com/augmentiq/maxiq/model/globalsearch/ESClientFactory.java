package com.augmentiq.maxiq.model.globalsearch;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;

/**
 * @author Ajinkya Marathe ESClientFactory is SingleTon class which is required because avoid usage
 *     of unused sources
 */
public class ESClientFactory {

  /** The logger. */
  private static Logger logger = Logger.getLogger(ESClientFactory.class);

  private static final Integer DEFAULT_PORT = 0;
  private static final Integer DEFAULT_PORT_ELASTIC = 9300;
  private static final String ELASTIC_CLUSTER_NAME = "cluster.name";
  private static final Integer INTERVAL_TIMEOUT = 10;
  private static final Integer PING_TIMEOUT = 50;
  private static final String CLIENT_TRANSPORT_SNIFF = "client.transport.sniff";
  private static final String CLIENT_TRANSPORT_PING_TIMEOUT = "client.transport.ping_timeout";
  private static final String CLIENT_TRANSPORT_NODES_SAMPLER_INTERVAL =
      "client.transport.nodes_sampler_interval";

  /** The instance. */
  public static TransportClient instance;

  /** Instantiates a new eS client factory. */
  private ESClientFactory() {}

  /**
   * Gets the single instance of ESClientFactory.
   *
   * @return single instance of ESClientFactory
   */
  public static synchronized Client getInstance() {
    String ipAddress = "", elasticClusterName = "";
    int transportClientPort = DEFAULT_PORT;
    try {
      ipAddress = Cache.getProperty(CacheConstants.GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER);
      transportClientPort =
          Integer.parseInt(
              Cache.getProperty(CacheConstants.GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER_PORT));
      elasticClusterName =
          Cache.getProperty(CacheConstants.GLOBAL_SEARCH_ELASTIC_SEARCH_CLUSTER_NAME);
    } catch (Exception e) {
      transportClientPort = DEFAULT_PORT_ELASTIC;
      logger.error(
          "In config.properties Following properties not exist - GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER_PORT, GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER, GLOBAL_SEARCH_ELASTIC_SEARCH_CLUSTER_NAME "
              + Arrays.toString(e.getStackTrace()));
    }

    logger.debug("got the client ip as :" + ipAddress + " and port :" + transportClientPort);
    if (instance == null) {
      //logger.debug("the client instance is null, creating a new instance");
      Settings settings =
          ImmutableSettings.settingsBuilder()
              .put(ELASTIC_CLUSTER_NAME, elasticClusterName)
              .put(CLIENT_TRANSPORT_SNIFF, true)
              .put(CLIENT_TRANSPORT_PING_TIMEOUT, PING_TIMEOUT, TimeUnit.SECONDS)
              .put(CLIENT_TRANSPORT_NODES_SAMPLER_INTERVAL, INTERVAL_TIMEOUT, TimeUnit.SECONDS)
              .build();
      instance = new TransportClient(settings);
      instance.addTransportAddress(new InetSocketTransportAddress(ipAddress, transportClientPort));
      //logger.debug("returning the new created client instance...");
      return instance;
    }
    //logger.debug("returning the existing transport client object connection.");
    return instance;
  }

  public static synchronized void shutdown() {
    if (instance != null) {
      //logger.debug("shutdown started");
      instance.close();
      instance.threadPool().shutdown();
      instance = null;
      //logger.debug("shutdown complete");
    }
  }
}

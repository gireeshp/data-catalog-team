/** */
package com.augmentiq.maxiq.model.globalsearch;

import java.util.List;

import com.augmentiq.maxiq.entity.model.configuration.bean.SearchInType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/** @author Mayuri Narawade */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GlobalSearchInput {
  private List<SearchInType> searchInTypes_;
  private List<String> createdBy_;
  private List<String> name_;
  private String columnName_;
  private String queryString_;
  private String generalSearchString_;
  private Integer createdWithinDays_;
  private Integer lastRunWithinDays_;
  private List<String> category_;
  private List<String> subCategory_;
  private List<String> tags_;
  private List<String> sourceType_;
  private List<String> dataAtRest_;
  private List<String> loadStrategy_;
  private List<String> dataRepo_;
  private List<String> userGroup_;
  private List<String> projectsName_;
  private List<String> avgRating_;

  // This fields are must to set
  private Long groupBy_;
  private Integer fetchRecordsFrm_;
  private Integer sizeOfRecords;

  public GlobalSearchInput() {
    super();
  }

  public GlobalSearchInput(
      List<SearchInType> searchInTypes_,
      List<String> createdBy_,
      List<String> name_,
      String columnName_,
      String queryString_,
      String generalSearchString_,
      Integer createdWithinDays_,
      Integer lastRunWithinDays_,
      List<String> category_,
      List<String> subCategory_,
      List<String> tags_,
      List<String> sourceType,
      List<String> dataAtRest,
      List<String> loadStrategy,
      List<String> dataRepo,
      List<String> userGroup,
      List<String> projectsName,
      List<String> avgRating,
      Long groupBy_,
      Integer fetchRecordsFrm_,
      Integer sizeOfRecords) {
    super();
    this.searchInTypes_ = searchInTypes_;
    this.createdBy_ = createdBy_;
    this.name_ = name_;
    this.columnName_ = columnName_;
    this.queryString_ = queryString_;
    this.generalSearchString_ = generalSearchString_;
    this.createdWithinDays_ = createdWithinDays_;
    this.lastRunWithinDays_ = lastRunWithinDays_;
    this.category_ = category_;
    this.subCategory_ = subCategory_;
    this.tags_ = tags_;
    this.sourceType_ = sourceType;
    this.dataAtRest_ = dataAtRest;
    this.loadStrategy_ = loadStrategy;
    this.dataRepo_ = dataRepo;
    this.userGroup_ = userGroup;
    this.projectsName_ = projectsName;
    this.avgRating_ = avgRating;
    this.groupBy_ = groupBy_;
    this.fetchRecordsFrm_ = fetchRecordsFrm_;
    this.sizeOfRecords = sizeOfRecords;
  }

  public List<SearchInType> getSearchInTypes() {
    return searchInTypes_;
  }

  public void setSearchInTypes(List<SearchInType> searchInTypes_) {
    this.searchInTypes_ = searchInTypes_;
  }

  public List<String> getCreatedBy() {
    return createdBy_;
  }

  public void setCreatedBy(List<String> createdBy_) {
    this.createdBy_ = createdBy_;
  }

  public List<String> getName() {
    return name_;
  }

  public void setName(List<String> name_) {
    this.name_ = name_;
  }

  public String getColumnName() {
    return columnName_;
  }

  public void setColumnName(String columnName_) {
    this.columnName_ = columnName_;
  }

  public String getQueryString() {
    return queryString_;
  }

  public void setQueryString(String queryString_) {
    this.queryString_ = queryString_;
  }

  public String getGeneralSearchString() {
    return generalSearchString_;
  }

  public void setGeneralSearchString(String generalSearchString_) {
    this.generalSearchString_ = generalSearchString_;
  }

  public Integer getCreatedWithinDays() {
    return createdWithinDays_;
  }

  public void setCreatedWithinDays(Integer createdWithinDays_) {
    this.createdWithinDays_ = createdWithinDays_;
  }

  public Integer getLastRunWithinDays() {
    return lastRunWithinDays_;
  }

  public void setLastRunWithinDays(Integer lastRunWithinDays_) {
    this.lastRunWithinDays_ = lastRunWithinDays_;
  }

  public List<String> getCategory() {
    return category_;
  }

  public void setCategory(List<String> category_) {
    this.category_ = category_;
  }

  public List<String> getSubCategory() {
    return subCategory_;
  }

  public void setSubCategory(List<String> subCategory_) {
    this.subCategory_ = subCategory_;
  }

  public List<String> getTags() {
    return tags_;
  }

  public void setTags(List<String> tags_) {
    this.tags_ = tags_;
  }

  public List<String> getSourceType() {
    return sourceType_;
  }

  public void setSourceType(List<String> sourceType) {
    this.sourceType_ = sourceType;
  }

  public List<String> getDataAtRest() {
    return dataAtRest_;
  }

  public void setDataAtRest(List<String> dataAtRest) {
    this.dataAtRest_ = dataAtRest;
  }

  public List<String> getLoadStrategy() {
    return loadStrategy_;
  }

  public void setLoadStrategy(List<String> loadStrategy) {
    this.loadStrategy_ = loadStrategy;
  }

  public List<String> getDataRepo() {
    return dataRepo_;
  }

  public void setDataRepo(List<String> dataRepo) {
    this.dataRepo_ = dataRepo;
  }

  public List<String> getUserGroup() {
    return userGroup_;
  }

  public void setUserGroup(List<String> userGroup) {
    this.userGroup_ = userGroup;
  }

  public List<String> getProjectsName() {
    return projectsName_;
  }

  public void setProjectsName(List<String> projectsName) {
    this.projectsName_ = projectsName;
  }

  public List<String> getAvgRating() {
    return avgRating_;
  }

  public void setAvgRating(List<String> avgRating) {
    this.avgRating_ = avgRating;
  }

  public Long getGroupBy() {
    return groupBy_;
  }

  public void setGroupBy(Long groupBy_) {
    this.groupBy_ = groupBy_;
  }

  public Integer getFetchRecordsFrm() {
    return fetchRecordsFrm_;
  }

  public void setFetchRecordsFrm(Integer fetchRecordsFrm_) {
    this.fetchRecordsFrm_ = fetchRecordsFrm_;
  }

  public Integer getSizeOfRecords() {
    return sizeOfRecords;
  }

  public void setSizeOfRecords(Integer sizeOfRecords) {
    this.sizeOfRecords = sizeOfRecords;
  }

  @Override
  public String toString() {
    return "GlobalSearchInput [searchInTypes_="
        + searchInTypes_
        + ", createdBy_="
        + createdBy_
        + ", name_="
        + name_
        + ", columnName_="
        + columnName_
        + ", queryString_="
        + queryString_
        + ", generalSearchString_="
        + generalSearchString_
        + ", createdWithinDays_="
        + createdWithinDays_
        + ", lastRunWithinDays_="
        + lastRunWithinDays_
        + ", category_="
        + category_
        + ", subCategory_="
        + subCategory_
        + ", tags_="
        + tags_
        + ", sourceType_="
        + sourceType_
        + ", dataAtRest_="
        + dataAtRest_
        + ", loadStrategy_="
        + loadStrategy_
        + ", dataRepo_="
        + dataRepo_
        + ", userGroup_="
        + userGroup_
        + ", projectsName_="
        + projectsName_
        + ", avgRating_="
        + avgRating_
        + ", groupBy_="
        + groupBy_
        + ", fetchRecordsFrm_="
        + fetchRecordsFrm_
        + ", sizeOfRecords="
        + sizeOfRecords
        + "]";
  }
}

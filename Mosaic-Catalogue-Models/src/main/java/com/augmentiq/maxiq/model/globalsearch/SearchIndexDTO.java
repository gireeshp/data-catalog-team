package com.augmentiq.maxiq.model.globalsearch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.augmentiq.maxiq.entity.model.configuration.bean.AppCombiner;
import com.augmentiq.maxiq.entity.model.configuration.bean.SearchInType;
import com.augmentiq.maxiq.model.globalsearch.dto.NodeDTO;

public class SearchIndexDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  // App OR Flow ID
  private Long id;
  // App OR Flow Name
  private String name;
  // App OR Flow GlobalSearchCosntant.DATASOURCE OR GlobalSearchCosntant.FLOW
  private SearchInType type;
  private String createdBy;
  // For App Only
  private NodeDTO nodes;
  private Date createdDate;
  private List<AppCombiner> list = new ArrayList<>();
  private Long groupId;
  private String category;
  private String subCategory;
  private String sourceType;
  private String dataAtRest;
  private String loadStrategy;
  private String dataRepo;
  private String userGroup;
  private String avgRating;

  public SearchIndexDTO(
      Long id,
      String name,
      SearchInType type,
      String createdBy,
      Date createdDate,
      Long groupId,
      String category,
      String subCategory) {
    super();
    this.id = id;
    this.name = name;
    this.type = type;
    this.createdBy = createdBy;
    this.createdDate = createdDate;
    this.groupId = groupId;
    this.category = category;
    this.subCategory = subCategory;
  }

  public SearchIndexDTO(
      Long id,
      String name,
      SearchInType type,
      String createdBy,
      Date createdDate,
      Long groupId,
      String category,
      String subCategory,
      String sourceType,
      String dataAtRest,
      String loadStrategy,
      String dataRepo,
      String userGroup,
      String avgRating) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.createdBy = createdBy;
    this.createdDate = createdDate;
    this.groupId = groupId;
    this.category = category;
    this.subCategory = subCategory;
    this.sourceType = sourceType;
    this.dataAtRest = dataAtRest;
    this.loadStrategy = loadStrategy;
    this.dataRepo = dataRepo;
    this.userGroup = userGroup;
    this.avgRating = avgRating;
  }

  public SearchIndexDTO(
      Long id,
      String name,
      SearchInType type,
      String createdBy,
      Date createdDate,
      Long groupId,
      NodeDTO nodes,
      List<AppCombiner> list,
      String userGroup) {
    this(
        id,
        name,
        type,
        createdBy,
        createdDate,
        groupId,
        "",
        "",
        "",
        "",
        "",
        "",
        userGroup,
        "");
    this.nodes = nodes;
    this.list = list;
  }
  
  
  public SearchIndexDTO(
	      Long id,
	      String name,
	      SearchInType type,
	      String sourceType,
	      String createdBy,
	      Date createdDate,
	      Long groupId,
	      NodeDTO nodes,
	      List<AppCombiner> list,
	      String userGroup) {
	    this(
	        id,
	        name,
	        type,
	        createdBy,
	        createdDate,
	        groupId,
	        "",
	        "",
	       sourceType,
	        "",
	        "",
	        "",
	        userGroup,
	        "");
	    this.nodes = nodes;
	    this.list = list;
	  }
  

  public List<AppCombiner> getList() {
    return list;
  }

  public void setList(List<AppCombiner> list) {
    this.list = list;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SearchInType getType() {
    return type;
  }

  public void setType(SearchInType type) {
    this.type = type;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public String getDataAtRest() {
    return dataAtRest;
  }

  public void setDataAtRest(String dataAtRest) {
    this.dataAtRest = dataAtRest;
  }

  public String getLoadStrategy() {
    return loadStrategy;
  }

  public void setLoadStrategy(String loadStrategy) {
    this.loadStrategy = loadStrategy;
  }

  public String getDataRepo() {
    return dataRepo;
  }

  public void setDataRepo(String dataRepo) {
    this.dataRepo = dataRepo;
  }

  public String getUserGroup() {
    return userGroup;
  }

  public void setUserGroup(String userGroup) {
    this.userGroup = userGroup;
  }

  public String getAvgRating() {
    return avgRating;
  }

  public void setAvgRating(String avgRating) {
    this.avgRating = avgRating;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public NodeDTO getNodes() {
    return nodes;
  }

  public void setNodes(NodeDTO nodes) {
    this.nodes = nodes;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }
}

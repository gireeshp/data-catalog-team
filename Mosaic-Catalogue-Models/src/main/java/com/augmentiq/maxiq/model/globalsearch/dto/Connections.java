package com.augmentiq.maxiq.model.globalsearch.dto;

import java.io.Serializable;

public class Connections implements Serializable {
  private Dest dest;

  private Source source;

  public Dest getDest() {
    return dest;
  }

  public void setDest(Dest dest) {
    this.dest = dest;
  }

  public Source getSource() {
    return source;
  }

  public void setSource(Source source) {
    this.source = source;
  }

  @Override
  public String toString() {
    return "ClassPojo [dest = " + dest + ", source = " + source + "]";
  }
}

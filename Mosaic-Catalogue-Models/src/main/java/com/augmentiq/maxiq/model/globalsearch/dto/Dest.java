package com.augmentiq.maxiq.model.globalsearch.dto;

import java.io.Serializable;

public class Dest implements Serializable {
  private String nodeID;

  private String connectorIndex;

  public String getNodeID() {
    return nodeID;
  }

  public void setNodeID(String nodeID) {
    this.nodeID = nodeID;
  }

  public String getConnectorIndex() {
    return connectorIndex;
  }

  public void setConnectorIndex(String connectorIndex) {
    this.connectorIndex = connectorIndex;
  }

  @Override
  public String toString() {
    return "ClassPojo [nodeID = " + nodeID + ", connectorIndex = " + connectorIndex + "]";
  }
}

package com.augmentiq.maxiq.model.globalsearch.dto;

import java.io.Serializable;

public class InputConnectors implements Serializable {
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "ClassPojo [name = " + name + "]";
  }
}

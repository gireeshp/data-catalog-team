package com.augmentiq.maxiq.model.globalsearch.dto;

public class NodeDTO {
  private Nodes[] nodes;

  private Connections[] connections;

  public Nodes[] getNodes() {
    return nodes;
  }

  public void setNodes(Nodes[] nodes) {
    this.nodes = nodes;
  }

  public Connections[] getConnections() {
    return connections;
  }

  public void setConnections(Connections[] connections) {
    this.connections = connections;
  }

  @Override
  public String toString() {
    return "ClassPojo [nodes = " + nodes + ", connections = " + connections + "]";
  }
}

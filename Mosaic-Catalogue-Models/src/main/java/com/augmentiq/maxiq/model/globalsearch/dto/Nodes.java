package com.augmentiq.maxiq.model.globalsearch.dto;

import java.io.Serializable;

public class Nodes implements Serializable {
  private String processType;

  private String id;

  private String appName;

  private OutputConnectors[] outputConnectors;

  private String name;

  private InputConnectors[] inputConnectors;

  private String datasourceId;

  private String nodeType;

  private String y;

  private String x;

  public String getProcessType() {
    return processType;
  }

  public void setProcessType(String processType) {
    this.processType = processType;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public OutputConnectors[] getOutputConnectors() {
    return outputConnectors;
  }

  public void setOutputConnectors(OutputConnectors[] outputConnectors) {
    this.outputConnectors = outputConnectors;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public InputConnectors[] getInputConnectors() {
    return inputConnectors;
  }

  public void setInputConnectors(InputConnectors[] inputConnectors) {
    this.inputConnectors = inputConnectors;
  }

  public String getDatasourceId() {
    return datasourceId;
  }

  public void setDatasourceId(String datasourceId) {
    this.datasourceId = datasourceId;
  }

  public String getNodeType() {
    return nodeType;
  }

  public void setNodeType(String nodeType) {
    this.nodeType = nodeType;
  }

  public String getY() {
    return y;
  }

  public void setY(String y) {
    this.y = y;
  }

  public String getX() {
    return x;
  }

  public void setX(String x) {
    this.x = x;
  }

  @Override
  public String toString() {
    return "ClassPojo [processType = "
        + processType
        + ", id = "
        + id
        + ", appName = "
        + appName
        + ", outputConnectors = "
        + outputConnectors
        + ", name = "
        + name
        + ", inputConnectors = "
        + inputConnectors
        + ", datasourceId = "
        + datasourceId
        + ", nodeType = "
        + nodeType
        + ", y = "
        + y
        + ", x = "
        + x
        + "]";
  }
}

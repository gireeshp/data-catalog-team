package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseProcess {

  @SerializedName("process_type")
  @Expose
  private String processType;

  @SerializedName("custom_query")
  @Expose
  private CustomQuery customQuery;

  @SerializedName("process_join")
  @Expose
  private ProcessJoin processJoin;

  @SerializedName("filter")
  @Expose
  private Filter filter;

  @SerializedName("process_add_var_math")
  @Expose
  private ProcessAddVarMath processAddVarMath;

  @SerializedName("process_group_fields")
  @Expose
  private List<ProcessGroupField> processGroupFields = null;

  @SerializedName("process_group_defs")
  @Expose
  private List<ProcessGroupDef> processGroupDefs = null;

  @SerializedName("process_group_process_filter")
  @Expose
  private ProcessGroupProcessFilter processGroupProcessFilter;

  public String getProcessType() {
    return processType;
  }

  public void setProcessType(String processType) {
    this.processType = processType;
  }

  public CustomQuery getCustomQuery() {
    return customQuery;
  }

  public void setCustomQuery(CustomQuery customQuery) {
    this.customQuery = customQuery;
  }

  public ProcessJoin getProcessJoin() {
    return processJoin;
  }

  public void setProcessJoin(ProcessJoin processJoin) {
    this.processJoin = processJoin;
  }

  public Filter getFilter() {
    return filter;
  }

  public void setFilter(Filter filter) {
    this.filter = filter;
  }

  public ProcessAddVarMath getProcessAddVarMath() {
    return processAddVarMath;
  }

  public void setProcessAddVarMath(ProcessAddVarMath processAddVarMath) {
    this.processAddVarMath = processAddVarMath;
  }

  public List<ProcessGroupField> getProcessGroupFields() {
    return processGroupFields;
  }

  public void setProcessGroupFields(List<ProcessGroupField> processGroupFields) {
    this.processGroupFields = processGroupFields;
  }

  public List<ProcessGroupDef> getProcessGroupDefs() {
    return processGroupDefs;
  }

  public void setProcessGroupDefs(List<ProcessGroupDef> processGroupDefs) {
    this.processGroupDefs = processGroupDefs;
  }

  public ProcessGroupProcessFilter getProcessGroupProcessFilter() {
    return processGroupProcessFilter;
  }

  public void setProcessGroupProcessFilter(ProcessGroupProcessFilter processGroupProcessFilter) {
    this.processGroupProcessFilter = processGroupProcessFilter;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

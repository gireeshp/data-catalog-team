package com.augmentiq.maxiq.model.globalsearch.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bucket {

  @SerializedName("key")
  @Expose
  private String key;

  @SerializedName("doc_count")
  @Expose
  private Long docCount;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public Long getDocCount() {
    return docCount;
  }

  public void setDocCount(Long docCount) {
    this.docCount = docCount;
  }

  public Bucket(String key, Long docCount) {
    super();
    this.key = key;
    this.docCount = docCount;
  }

  @Override
  public String toString() {
    return "Bucket [key=" + key + ", docCount=" + docCount + "]";
  }
}
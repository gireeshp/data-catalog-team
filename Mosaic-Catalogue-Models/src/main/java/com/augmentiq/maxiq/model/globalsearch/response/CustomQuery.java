package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomQuery {

  @SerializedName("process_query")
  @Expose
  private String processQuery;

  public String getProcessQuery() {
    return processQuery;
  }

  public void setProcessQuery(String processQuery) {
    this.processQuery = processQuery;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatasourceGlobalInputParameter {

  @SerializedName("name")
  @Expose
  private String name;

  @SerializedName("type")
  @Expose
  private String type;

  @SerializedName("value")
  @Expose
  private String value;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

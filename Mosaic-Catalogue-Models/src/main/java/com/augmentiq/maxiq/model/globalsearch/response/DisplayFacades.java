package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisplayFacades {

  @SerializedName("doc_count_error_upper_bound")
  @Expose
  private Long docCountErrorUpperBound;

  @SerializedName("sum_other_doc_count")
  @Expose
  private Long sumOtherDocCount;

  @SerializedName("buckets")
  @Expose
  private Map<String, List<Bucket>> bucketsMap;

  public Long getDocCountErrorUpperBound() {
    return docCountErrorUpperBound;
  }

  public void setDocCountErrorUpperBound(Long docCountErrorUpperBound) {
    this.docCountErrorUpperBound = docCountErrorUpperBound;
  }

  public Long getSumOtherDocCount() {
    return sumOtherDocCount;
  }

  public void setSumOtherDocCount(Long sumOtherDocCount) {
    this.sumOtherDocCount = sumOtherDocCount;
  }

  public Map<String, List<Bucket>> getBucketsMap() {
    return bucketsMap;
  }

  public void setBucketsMap(Map<String, List<Bucket>> bucketsMap) {
    this.bucketsMap = bucketsMap;
  }

  public DisplayFacades(
      Long docCountErrorUpperBound, Long sumOtherDocCount, Map<String, List<Bucket>> bucketsMap) {
    super();
    this.docCountErrorUpperBound = docCountErrorUpperBound;
    this.sumOtherDocCount = sumOtherDocCount;
    this.bucketsMap = bucketsMap;
  }
}

package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DsField {

  @SerializedName("name")
  @Expose
  private String name;

  @SerializedName("type")
  @Expose
  private String type;

  @SerializedName("format")
  @Expose
  private Object format;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Object getFormat() {
    return format;
  }

  public void setFormat(Object format) {
    this.format = format;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

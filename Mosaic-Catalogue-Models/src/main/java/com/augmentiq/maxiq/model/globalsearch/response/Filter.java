package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Filter {

  @SerializedName("process_filter_expression")
  @Expose
  private String processFilterExpression;

  public String getProcessFilterExpression() {
    return processFilterExpression;
  }

  public void setProcessFilterExpression(String processFilterExpression) {
    this.processFilterExpression = processFilterExpression;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

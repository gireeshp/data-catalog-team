package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Input {

  @SerializedName("input_data_files")
  @Expose
  private List<InputDataFile> inputDataFiles = null;

  public List<InputDataFile> getInputDataFiles() {
    return inputDataFiles;
  }

  public void setInputDataFiles(List<InputDataFile> inputDataFiles) {
    this.inputDataFiles = inputDataFiles;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
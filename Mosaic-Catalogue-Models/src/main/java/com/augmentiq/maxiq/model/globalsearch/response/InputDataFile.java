package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InputDataFile {

  @SerializedName("input_data_file_in_connector")
  @Expose
  private String inputDataFileInConnector;

  @SerializedName("node_in_fields")
  @Expose
  private List<NodeInField> nodeInFields = null;

  public String getInputDataFileInConnector() {
    return inputDataFileInConnector;
  }

  public void setInputDataFileInConnector(String inputDataFileInConnector) {
    this.inputDataFileInConnector = inputDataFileInConnector;
  }

  public List<NodeInField> getNodeInFields() {
    return nodeInFields;
  }

  public void setNodeInFields(List<NodeInField> nodeInFields) {
    this.nodeInFields = nodeInFields;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
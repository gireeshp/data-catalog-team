package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Node {

  @SerializedName("node_name")
  @Expose
  private String nodeName;

  @SerializedName("input")
  @Expose
  private List<Input> input = null;

  @SerializedName("output")
  @Expose
  private List<Output> output = null;

  @SerializedName("process")
  @Expose
  private List<Process> process = null;

  public String getNodeName() {
    return nodeName;
  }

  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  public List<Input> getInput() {
    return input;
  }

  public void setInput(List<Input> input) {
    this.input = input;
  }

  public List<Output> getOutput() {
    return output;
  }

  public void setOutput(List<Output> output) {
    this.output = output;
  }

  public List<Process> getProcess() {
    return process;
  }

  public void setProcess(List<Process> process) {
    this.process = process;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Output {

  @SerializedName("output_data_files")
  @Expose
  private List<OutputDataFile> outputDataFiles = null;

  public List<OutputDataFile> getOutputDataFiles() {
    return outputDataFiles;
  }

  public void setOutputDataFiles(List<OutputDataFile> outputDataFiles) {
    this.outputDataFiles = outputDataFiles;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutputDataFile {

  @SerializedName("output_data_file_out_connector")
  @Expose
  private String outputDataFileOutConnector;

  @SerializedName("node_out_fields")
  @Expose
  private List<NodeOutField> nodeOutFields = null;

  public String getOutputDataFileOutConnector() {
    return outputDataFileOutConnector;
  }

  public void setOutputDataFileOutConnector(String outputDataFileOutConnector) {
    this.outputDataFileOutConnector = outputDataFileOutConnector;
  }

  public List<NodeOutField> getNodeOutFields() {
    return nodeOutFields;
  }

  public void setNodeOutFields(List<NodeOutField> nodeOutFields) {
    this.nodeOutFields = nodeOutFields;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
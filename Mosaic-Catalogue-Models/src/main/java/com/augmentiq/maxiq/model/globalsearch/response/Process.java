package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Process {

  @SerializedName("base_processes")
  @Expose
  private List<BaseProcess> baseProcesses = null;

  public List<BaseProcess> getBaseProcesses() {
    return baseProcesses;
  }

  public void setBaseProcesses(List<BaseProcess> baseProcesses) {
    this.baseProcesses = baseProcesses;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

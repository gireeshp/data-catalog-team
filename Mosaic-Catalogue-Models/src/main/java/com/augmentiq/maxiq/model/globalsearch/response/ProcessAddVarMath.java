package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessAddVarMath {

  @SerializedName("process_add_var_math_expression")
  @Expose
  private String processAddVarMathExpression;

  @SerializedName("process_add_var_math_op_var_field_type")
  @Expose
  private String processAddVarMathOpVarFieldType;

  @SerializedName("process_add_var_math_op_var")
  @Expose
  private String processAddVarMathOpVar;

  public String getProcessAddVarMathExpression() {
    return processAddVarMathExpression;
  }

  public void setProcessAddVarMathExpression(String processAddVarMathExpression) {
    this.processAddVarMathExpression = processAddVarMathExpression;
  }

  public String getProcessAddVarMathOpVarFieldType() {
    return processAddVarMathOpVarFieldType;
  }

  public void setProcessAddVarMathOpVarFieldType(String processAddVarMathOpVarFieldType) {
    this.processAddVarMathOpVarFieldType = processAddVarMathOpVarFieldType;
  }

  public String getProcessAddVarMathOpVar() {
    return processAddVarMathOpVar;
  }

  public void setProcessAddVarMathOpVar(String processAddVarMathOpVar) {
    this.processAddVarMathOpVar = processAddVarMathOpVar;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

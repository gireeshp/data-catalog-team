package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessGroupDef {

  @SerializedName("process_group_def_field")
  @Expose
  private ProcessGroupDefField processGroupDefField;

  @SerializedName("process_group_process")
  @Expose
  private Object processGroupProcess;

  @SerializedName("process_group_output_var_field_type")
  @Expose
  private String processGroupOutputVarFieldType;

  @SerializedName("process_group_output_var")
  @Expose
  private String processGroupOutputVar;

  public ProcessGroupDefField getProcessGroupDefField() {
    return processGroupDefField;
  }

  public void setProcessGroupDefField(ProcessGroupDefField processGroupDefField) {
    this.processGroupDefField = processGroupDefField;
  }

  public Object getProcessGroupProcess() {
    return processGroupProcess;
  }

  public void setProcessGroupProcess(Object processGroupProcess) {
    this.processGroupProcess = processGroupProcess;
  }

  public String getProcessGroupOutputVarFieldType() {
    return processGroupOutputVarFieldType;
  }

  public void setProcessGroupOutputVarFieldType(String processGroupOutputVarFieldType) {
    this.processGroupOutputVarFieldType = processGroupOutputVarFieldType;
  }

  public String getProcessGroupOutputVar() {
    return processGroupOutputVar;
  }

  public void setProcessGroupOutputVar(String processGroupOutputVar) {
    this.processGroupOutputVar = processGroupOutputVar;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessGroupProcessFilter {

  @SerializedName("process_group_process_filter_expression")
  @Expose
  private String processGroupProcessFilterExpression;

  public String getProcessGroupProcessFilterExpression() {
    return processGroupProcessFilterExpression;
  }

  public void setProcessGroupProcessFilterExpression(String processGroupProcessFilterExpression) {
    this.processGroupProcessFilterExpression = processGroupProcessFilterExpression;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessJoin {

  @SerializedName("process_join_type")
  @Expose
  private String processJoinType;

  @SerializedName("process_left_datasource")
  @Expose
  private String processLeftDatasource;

  @SerializedName("process_right_datasource")
  @Expose
  private String processRightDatasource;

  @SerializedName("process_join_filter")
  @Expose
  private ProcessJoinFilter processJoinFilter;

  @SerializedName("process_join_conditions")
  @Expose
  private List<ProcessJoinCondition> processJoinConditions = null;

  public String getProcessJoinType() {
    return processJoinType;
  }

  public void setProcessJoinType(String processJoinType) {
    this.processJoinType = processJoinType;
  }

  public String getProcessLeftDatasource() {
    return processLeftDatasource;
  }

  public void setProcessLeftDatasource(String processLeftDatasource) {
    this.processLeftDatasource = processLeftDatasource;
  }

  public String getProcessRightDatasource() {
    return processRightDatasource;
  }

  public void setProcessRightDatasource(String processRightDatasource) {
    this.processRightDatasource = processRightDatasource;
  }

  public ProcessJoinFilter getProcessJoinFilter() {
    return processJoinFilter;
  }

  public void setProcessJoinFilter(ProcessJoinFilter processJoinFilter) {
    this.processJoinFilter = processJoinFilter;
  }

  public List<ProcessJoinCondition> getProcessJoinConditions() {
    return processJoinConditions;
  }

  public void setProcessJoinConditions(List<ProcessJoinCondition> processJoinConditions) {
    this.processJoinConditions = processJoinConditions;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

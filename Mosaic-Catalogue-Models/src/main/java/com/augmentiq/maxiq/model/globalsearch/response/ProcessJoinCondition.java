package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessJoinCondition {

  @SerializedName("process_join_conditions_left_fields")
  @Expose
  private ProcessJoinConditionsLeftFields processJoinConditionsLeftFields;

  @SerializedName("process_join_conditions_right_fields")
  @Expose
  private ProcessJoinConditionsRightFields processJoinConditionsRightFields;

  public ProcessJoinConditionsLeftFields getProcessJoinConditionsLeftFields() {
    return processJoinConditionsLeftFields;
  }

  public void setProcessJoinConditionsLeftFields(
      ProcessJoinConditionsLeftFields processJoinConditionsLeftFields) {
    this.processJoinConditionsLeftFields = processJoinConditionsLeftFields;
  }

  public ProcessJoinConditionsRightFields getProcessJoinConditionsRightFields() {
    return processJoinConditionsRightFields;
  }

  public void setProcessJoinConditionsRightFields(
      ProcessJoinConditionsRightFields processJoinConditionsRightFields) {
    this.processJoinConditionsRightFields = processJoinConditionsRightFields;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

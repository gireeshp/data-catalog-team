package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessJoinConditionsRightFields {

  @SerializedName("name")
  @Expose
  private String name;

  @SerializedName("format")
  @Expose
  private Object format;

  @SerializedName("type")
  @Expose
  private String type;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Object getFormat() {
    return format;
  }

  public void setFormat(Object format) {
    this.format = format;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

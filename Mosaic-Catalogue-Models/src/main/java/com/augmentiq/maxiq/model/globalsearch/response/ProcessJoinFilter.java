package com.augmentiq.maxiq.model.globalsearch.response;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessJoinFilter {

  @SerializedName("process_join_filter_expression")
  @Expose
  private String processJoinFilterExpression;

  public String getProcessJoinFilterExpression() {
    return processJoinFilterExpression;
  }

  public void setProcessJoinFilterExpression(String processJoinFilterExpression) {
    this.processJoinFilterExpression = processJoinFilterExpression;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}

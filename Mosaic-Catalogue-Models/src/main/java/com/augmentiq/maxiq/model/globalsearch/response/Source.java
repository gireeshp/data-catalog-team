package com.augmentiq.maxiq.model.globalsearch.response;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Source {

  @SerializedName("ds_or_flow_id")
  @Expose
  private Integer dsOrFlowId;

  @SerializedName("name")
  @Expose
  private String name;

  @SerializedName("type")
  @Expose
  private String type;

  @SerializedName("category")
  @Expose
  private String category;

  @SerializedName("sub_category")
  @Expose
  private String subCategory;

  @SerializedName("created_by")
  @Expose
  private String createdBy;

  @SerializedName("source_type")
  @Expose
  private String sourceType;

  @SerializedName("data_at_rest")
  @Expose
  private String dataAtRest;

  @SerializedName("load_strategy")
  @Expose
  private String loadStrategy;

  @SerializedName("data_repo")
  @Expose
  private String dataRepo;

  @SerializedName("user_group")
  @Expose
  private String userGroup;

  @SerializedName("projects_name")
  @Expose
  private String projectsName;

  @SerializedName("avg_rating")
  @Expose
  private String avgRating;

  @SerializedName("created_date")
  @Expose
  private String createdDate;

  @SerializedName("group_id")
  @Expose
  private Integer groupId;

  @SerializedName("ds_type")
  @Expose
  private String dsType;

  @SerializedName("ds_fields")
  @Expose
  private List<DsField> dsFields = null;

  @SerializedName("ds_connector_type")
  @Expose
  private String dsConnectorType;

  @SerializedName("ds_config_connection_type")
  @Expose
  private String dsConfigConnectionType;

  @SerializedName("query")
  @Expose
  private String query;

  @SerializedName("ds_primary_key")
  @Expose
  private String dsPrimaryKey;

  @SerializedName("ip_address")
  @Expose
  private String ipAddress;

  @SerializedName("database_port")
  @Expose
  private String databasePort;

  @SerializedName("connection_name")
  @Expose
  private String connectionName;

  @SerializedName("database_name")
  @Expose
  private String databaseName;

  @SerializedName("db_username")
  @Expose
  private String dbUsername;

  @SerializedName("datasource_global_input_parameters")
  @Expose
  private List<DatasourceGlobalInputParameter> datasourceGlobalInputParameters = null;

  @SerializedName("nodes")
  @Expose
  private List<Node> nodes = null;

 

  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public String getDataAtRest() {
    return dataAtRest;
  }

  public void setDataAtRest(String dataAtRest) {
    this.dataAtRest = dataAtRest;
  }

  public String getLoadStrategy() {
    return loadStrategy;
  }

  public void setLoadStrategy(String loadStrategy) {
    this.loadStrategy = loadStrategy;
  }

  public String getDataRepo() {
    return dataRepo;
  }

  public void setDataRepo(String dataRepo) {
    this.dataRepo = dataRepo;
  }

  public String getUserGroup() {
    return userGroup;
  }

  public void setUserGroup(String userGroup) {
    this.userGroup = userGroup;
  }

  public String getProjectsName() {
    return projectsName;
  }

  public void setProjectsName(String projectsName) {
    this.projectsName = projectsName;
  }

  public String getAvgRating() {
    return avgRating;
  }

  public void setAvgRating(String avgRating) {
    this.avgRating = avgRating;
  }

  public Integer getDsOrFlowId() {
    return dsOrFlowId;
  }

  public void setDsOrFlowId(Integer dsOrFlowId) {
    this.dsOrFlowId = dsOrFlowId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }

  public Integer getGroupId() {
    return groupId;
  }

  public void setGroupId(Integer groupId) {
    this.groupId = groupId;
  }

  public String getDsType() {
    return dsType;
  }

  public void setDsType(String dsType) {
    this.dsType = dsType;
  }

  public List<DsField> getDsFields() {
    return dsFields;
  }

  public void setDsFields(List<DsField> dsFields) {
    this.dsFields = dsFields;
  }

  public String getDsConnectorType() {
    return dsConnectorType;
  }

  public void setDsConnectorType(String dsConnectorType) {
    this.dsConnectorType = dsConnectorType;
  }

  public String getDsConfigConnectionType() {
    return dsConfigConnectionType;
  }

  public void setDsConfigConnectionType(String dsConfigConnectionType) {
    this.dsConfigConnectionType = dsConfigConnectionType;
  }

  public String getQuery() {
    return query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public String getDsPrimaryKey() {
    return dsPrimaryKey;
  }

  public void setDsPrimaryKey(String dsPrimaryKey) {
    this.dsPrimaryKey = dsPrimaryKey;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public String getDatabasePort() {
    return databasePort;
  }

  public void setDatabasePort(String databasePort) {
    this.databasePort = databasePort;
  }

  public String getConnectionName() {
    return connectionName;
  }

  public void setConnectionName(String connectionName) {
    this.connectionName = connectionName;
  }

  public String getDatabaseName() {
    return databaseName;
  }

  public void setDatabaseName(String databaseName) {
    this.databaseName = databaseName;
  }

  public String getDbUsername() {
    return dbUsername;
  }

  public void setDbUsername(String dbUsername) {
    this.dbUsername = dbUsername;
  }

  public List<DatasourceGlobalInputParameter> getDatasourceGlobalInputParameters() {
    return datasourceGlobalInputParameters;
  }

  public void setDatasourceGlobalInputParameters(
      List<DatasourceGlobalInputParameter> datasourceGlobalInputParameters) {
    this.datasourceGlobalInputParameters = datasourceGlobalInputParameters;
  }

  public List<Node> getNodes() {
    return nodes;
  }

  public void setNodes(List<Node> nodes) {
    this.nodes = nodes;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
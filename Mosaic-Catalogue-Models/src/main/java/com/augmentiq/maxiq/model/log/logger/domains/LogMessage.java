package com.augmentiq.maxiq.model.log.logger.domains;

import com.augmentiq.maxiq.constant.log.logger.domains.LoggerMessaageConstants;

public class LogMessage {

  private String rootId_;
  private LoggerMessaageConstants type_;
  private String message_;

  public LogMessage() {}

  public LogMessage(String rootId_, LoggerMessaageConstants type_, String message_) {
    super();
    this.rootId_ = rootId_;
    this.type_ = type_;
    this.message_ = message_;
  }

  public String getRootId() {
    return rootId_;
  }

  public void setRootId(String rootId_) {
    this.rootId_ = rootId_;
  }

  public LoggerMessaageConstants getType() {
    return type_;
  }

  public void setType(LoggerMessaageConstants type_) {
    this.type_ = type_;
  }

  public String getMessage() {
    return message_;
  }

  public void setMessage(String message_) {
    this.message_ = message_;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((message_ == null) ? 0 : message_.hashCode());
    result = prime * result + ((rootId_ == null) ? 0 : rootId_.hashCode());
    result = prime * result + ((type_ == null) ? 0 : type_.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    LogMessage other = (LogMessage) obj;
    if (message_ == null) {
      if (other.message_ != null) return false;
    } else if (!message_.equals(other.message_)) return false;
    if (rootId_ == null) {
      if (other.rootId_ != null) return false;
    } else if (!rootId_.equals(other.rootId_)) return false;
    if (type_ != other.type_) return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("LogMessage [rootId_=");
    builder.append(rootId_);
    builder.append(", type_=");
    builder.append(type_);
    builder.append(", message_=");
    builder.append(message_);
    builder.append("]");
    return builder.toString();
  }
}

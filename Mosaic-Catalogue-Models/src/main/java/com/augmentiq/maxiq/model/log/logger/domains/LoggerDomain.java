package com.augmentiq.maxiq.model.log.logger.domains;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.maxiq.constant.log.logger.domains.LoggerMessaageConstants;

/**
 * *
 *
 * @author Mayuri Narawade
 * @since 11 Jan 2016
 */
public class LoggerDomain {
  public static Map<Long, LoggerError> errorLog;
  public static Map<Long, LoggerInfo> infoLog;

  private static final Logger logger_ = LoggerFactory.getLogger(LoggerDomain.class);

  public static LoggerError fetchLoggerError(Long baapId) {
    if (errorLog == null) return null;
    else return errorLog.get(baapId);
  }

  public static LoggerInfo fetchLoggerInfo(Long baapId) {
    if (infoLog == null) return null;
    else return infoLog.get(baapId);
  }

  public static void deleteLogger(Long baapId) {
    errorLog.remove(baapId);
    infoLog.remove(baapId);
  }

  public static void cleanUpLoggerErrorAndInfo() {
    errorLog.clear();
    infoLog.clear();
  }

  public static void addMessageForLogger(
      Long baapId, String logMessage, LoggerMessaageConstants loggerType) {
    if (loggerType == LoggerMessaageConstants.INFO) {
      addInfoMessage(baapId, logMessage);
    } else {
      addErrorMessage(baapId, logMessage);
    }
  }

  public static void addErrorMessage(Long baapId, String message) {
    if (errorLog == null) {
      errorLog = new Hashtable<Long, LoggerError>();
    }

    LoggerError loggerError = errorLog.get(baapId);
    if (loggerError != null) {
      List<String> errLogs = loggerError.getErrorLog();
      if (errLogs == null) errLogs = new ArrayList<String>();

      errLogs.add(message);
      loggerError.setErrorLog(errLogs);
      errorLog.put(baapId, loggerError);
    } else {
      loggerError = new LoggerError();
      List<String> errorLogs = new ArrayList<String>();
      errorLogs.add(message);
      loggerError.setErrorLog(errorLogs);
      errorLog.put(baapId, loggerError);
    }
  }

  public static void addInfoMessage(Long baapId, String message) {
    if (infoLog == null) {
      infoLog = new Hashtable<Long, LoggerInfo>();
    }
    LoggerInfo loggerInfo = infoLog.get(baapId);
    if (loggerInfo != null) {
      List<String> infoLogs = loggerInfo.getInfoLog();
      if (infoLogs == null) infoLogs = new ArrayList<String>();

      infoLogs.add(message);
      loggerInfo.setInfoLog(infoLogs);
      infoLog.put(baapId, loggerInfo);
    } else {
      loggerInfo = new LoggerInfo();
      List<String> infoLogs = new ArrayList<String>();
      infoLogs.add(message);
      loggerInfo.setInfoLog(infoLogs);
      infoLog.put(baapId, loggerInfo);
    }
  }

  @Override
  public String toString() {
    return "LoggerDomain [getClass()="
        + getClass()
        + ", hashCode()="
        + hashCode()
        + ", toString()="
        + super.toString()
        + "]";
  }
}

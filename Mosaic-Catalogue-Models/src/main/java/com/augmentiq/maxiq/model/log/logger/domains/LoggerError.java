package com.augmentiq.maxiq.model.log.logger.domains;

import java.util.List;

/**
 * *
 *
 * @author Mayuri Narawade
 * @since 11 Jan 2016
 */
public class LoggerError {
  List<String> errorLog;

  public List<String> getErrorLog() {
    return errorLog;
  }

  public void setErrorLog(List<String> errorLog) {
    this.errorLog = errorLog;
  }

  @Override
  public String toString() {
    return "LoggerError [errorLog=" + errorLog + "]";
  }
}

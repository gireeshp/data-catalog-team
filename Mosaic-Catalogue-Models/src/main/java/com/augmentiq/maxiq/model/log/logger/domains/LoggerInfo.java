package com.augmentiq.maxiq.model.log.logger.domains;

import java.util.List;

/**
 * *
 *
 * @author Mayuri Narawade
 * @since 11 Jan 2016
 */
public class LoggerInfo {
  List<String> infoLog;

  public List<String> getInfoLog() {
    return infoLog;
  }

  public void setInfoLog(List<String> infoLog) {
    this.infoLog = infoLog;
  }

  @Override
  public String toString() {
    return "LoggerInfo [infoLog=" + infoLog + "]";
  }
}

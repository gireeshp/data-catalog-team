package com.augmentiq.maxiq.model.machine.learning.bean;

import com.augmentiq.maxiq.entity.model.configuration.bean.RModel;

public class RModelDTO extends RModel {
  String CreatedByName;
  String modifiedByName;
  String groupName;

  public String getCreatedByName() {
    return CreatedByName;
  }

  public void setCreatedByName(String createdByName) {
    CreatedByName = createdByName;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getModifiedByName() {
    return modifiedByName;
  }

  public void setModifiedByName(String modifiedByName) {
    this.modifiedByName = modifiedByName;
  }

  public RModelDTO(String createdByName, String modifiedByName, String groupName, RModel rmodel) {

    super(
        rmodel.getModelId(),
        rmodel.getModelName(),
        rmodel.getModelLocation(),
        rmodel.getDescription(),
        rmodel.getGroupId(),
        rmodel.getCreatedBy(),
        rmodel.getCreatedDate(),
        rmodel.getModifiedDate(),
        rmodel.getModifiedBy(),
        rmodel.getModelType(),
        rmodel.getApiKey(),
        rmodel.getApiAccessStatus(),
        rmodel.getData(),
        rmodel.getOwnerProjectId(),
        rmodel.getAppId(),
        rmodel.getNodeId(),
        rmodel.getJobInstanceId(),
        rmodel.getModelPackageName());

    this.CreatedByName = createdByName;
    this.groupName = groupName;
    this.modifiedByName = modifiedByName;
  }

  @Override
  public String toString() {
    return "RModelDTO [CreatedByName="
        + CreatedByName
        + ", modifiedByName="
        + modifiedByName
        + ", groupName="
        + groupName
        + "]";
  }
}

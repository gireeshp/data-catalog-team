package com.augmentiq.maxiq.model.marketplace.bean;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Category {
  private ObjectId Id;
  private String CategoryName;
  private String CategoryDescription;
  private String IconImage;
  private Boolean Status;
  private String CreatedBy;
  private String LastModifiedBy;
  private ObjectId TypeId;
  private String idInString;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  Date ModifiedDate;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  Date CreatedDate;

  public Category() {
    super();
  }

  public Category(
      String categoryName,
      String categoryDescription,
      String iconImage,
      Boolean status,
      String createdBy,
      String lastModifiedBy,
      ObjectId typeId,
      Date modifiedDate,
      Date createdDate) {
    super();
    CategoryName = categoryName;
    CategoryDescription = categoryDescription;
    IconImage = iconImage;
    Status = status;
    CreatedBy = createdBy;
    LastModifiedBy = lastModifiedBy;
    TypeId = typeId;
    ModifiedDate = modifiedDate;
    CreatedDate = createdDate;
    idInString = Id.toHexString();
  }

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    Id = id;
  }

  public String getCategoryName() {
    return CategoryName;
  }

  public void setCategoryName(String categoryName) {
    CategoryName = categoryName;
  }

  public String getCategoryDescription() {
    return CategoryDescription;
  }

  public void setCategoryDescription(String categoryDescription) {
    CategoryDescription = categoryDescription;
  }

  public String getIconImage() {
    return IconImage;
  }

  public void setIconImage(String iconImage) {
    IconImage = iconImage;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  public String getCreatedBy() {
    return CreatedBy;
  }

  public void setCreatedBy(String createdBy) {
    CreatedBy = createdBy;
  }

  public String getLastModifiedBy() {
    return LastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    LastModifiedBy = lastModifiedBy;
  }

  public ObjectId getTypeId() {
    return TypeId;
  }

  public void setTypeId(ObjectId typeId) {
    TypeId = typeId;
  }

  public Date getModifiedDate() {
    return ModifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    ModifiedDate = modifiedDate;
  }

  public Date getCreatedDate() {
    return CreatedDate;
  }

  public void setCreatedDate(Date createdDate) {
    CreatedDate = createdDate;
  }

  public String getIdInString() {
    return Id.toHexString();
  }

  public void setIdInString(ObjectId idInString) {
    this.idInString = idInString.toHexString();
  }

  @Override
  public String toString() {
    return "Category [Id="
        + Id
        + ", CategoryName="
        + CategoryName
        + ", CategoryDescription="
        + CategoryDescription
        + ", IconImage="
        + IconImage
        + ", Status="
        + Status
        + ", CreatedBy="
        + CreatedBy
        + ", LastModifiedBy="
        + LastModifiedBy
        + ", TypeId="
        + TypeId
        + ", idInString="
        + idInString
        + ", ModifiedDate="
        + ModifiedDate
        + ", CreatedDate="
        + CreatedDate
        + "]";
  }
}

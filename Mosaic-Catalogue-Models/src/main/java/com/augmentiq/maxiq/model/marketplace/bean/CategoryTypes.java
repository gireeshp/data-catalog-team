package com.augmentiq.maxiq.model.marketplace.bean;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class CategoryTypes {
  private ObjectId Id;
  private String TypeName;
  private Boolean Status;
  private String CreatedBy;
  private String LastModifiedBy;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date ModifiedDate;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date CreatedDate;

  public CategoryTypes() {
    super();
  }

  public CategoryTypes(
      String typeName,
      Boolean status,
      String createdBy,
      String lastModifiedBy,
      Date modifiedDate,
      Date createdDate) {
    super();
    TypeName = typeName;
    Status = status;
    CreatedBy = createdBy;
    LastModifiedBy = lastModifiedBy;
    ModifiedDate = modifiedDate;
    CreatedDate = createdDate;
  }

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    Id = id;
  }

  public String getTypeName() {
    return TypeName;
  }

  public void setTypeName(String typeName) {
    TypeName = typeName;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  public String getCreatedBy() {
    return CreatedBy;
  }

  public void setCreatedBy(String createdBy) {
    CreatedBy = createdBy;
  }

  public String getLastModifiedBy() {
    return LastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    LastModifiedBy = lastModifiedBy;
  }

  public Date getModifiedDate() {
    return ModifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    ModifiedDate = modifiedDate;
  }

  public Date getCreatedDate() {
    return CreatedDate;
  }

  public void setCreatedDate(Date createdDate) {
    CreatedDate = createdDate;
  }

  @Override
  public String toString() {
    return "CategoryTypes [Id="
        + Id
        + ", TypeName="
        + TypeName
        + ", Status="
        + Status
        + ", CreatedBy="
        + CreatedBy
        + ", LastModifiedBy="
        + LastModifiedBy
        + ", ModifiedDate="
        + ModifiedDate
        + ", CreatedDate="
        + CreatedDate
        + "]";
  }
}

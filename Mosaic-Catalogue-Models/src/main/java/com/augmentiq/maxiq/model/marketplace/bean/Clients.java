package com.augmentiq.maxiq.model.marketplace.bean;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Clients {

  private ObjectId Id;
  private String ClientID;
  private String APIKey;
  private Boolean Status;
  private String CreatedBy;
  private String LastModifiedBy;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date CreatedDate;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date ModifiedDate;

  public Clients() {
    super();
  }

  public Clients(
      String clientID,
      String aPIKey,
      Boolean status,
      String createdBy,
      String lastModifiedBy,
      Date createdDate,
      Date modifiedDate) {
    super();
    ClientID = clientID;
    APIKey = aPIKey;
    Status = status;
    CreatedBy = createdBy;
    LastModifiedBy = lastModifiedBy;
    CreatedDate = createdDate;
    ModifiedDate = modifiedDate;
  }

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    this.Id = id;
  }

  public String getClientID() {
    return ClientID;
  }

  public void setClientID(String clientID) {
    ClientID = clientID;
  }

  public String getAPIKey() {
    return APIKey;
  }

  public void setAPIKey(String aPIKey) {
    APIKey = aPIKey;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  public String getCreatedBy() {
    return CreatedBy;
  }

  public void setCreatedBy(String createdBy) {
    CreatedBy = createdBy;
  }

  public String getLastModifiedBy() {
    return LastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    LastModifiedBy = lastModifiedBy;
  }

  public Date getCreatedDate() {
    return CreatedDate;
  }

  public void setCreatedDate(Date createdDate) {
    CreatedDate = createdDate;
  }

  public Date getModifiedDate() {
    return ModifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    ModifiedDate = modifiedDate;
  }

  @Override
  public String toString() {
    return "Clients [Id="
        + Id
        + ", ClientID="
        + ClientID
        + ", APIKey="
        + APIKey
        + ", Status="
        + Status
        + ", CreatedBy="
        + CreatedBy
        + ", LastModifiedBy="
        + LastModifiedBy
        + ", CreatedDate="
        + CreatedDate
        + ", ModifiedDate="
        + ModifiedDate
        + "]";
  }
}

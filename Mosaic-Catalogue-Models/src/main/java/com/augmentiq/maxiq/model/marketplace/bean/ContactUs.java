package com.augmentiq.maxiq.model.marketplace.bean;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class ContactUs {
  private ObjectId Id;
  private String Name;
  private String Email;
  private String Message;
  private Boolean Status;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date CreatedDate;

  public ContactUs() {
    super();
  }

  public ContactUs(String name, String email, String message, Boolean status, Date createdDate) {
    super();
    Name = name;
    Email = email;
    Message = message;
    Status = status;
    CreatedDate = createdDate;
  }

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    Id = id;
  }

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }

  public String getEmail() {
    return Email;
  }

  public void setEmail(String email) {
    Email = email;
  }

  public String getMessage() {
    return Message;
  }

  public void setMessage(String message) {
    Message = message;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  public Date getCreatedDate() {
    return CreatedDate;
  }

  public void setCreatedDate(Date createdDate) {
    CreatedDate = createdDate;
  }

  @Override
  public String toString() {
    return "ContactUs [Id="
        + Id
        + ", Name="
        + Name
        + ", Email="
        + Email
        + ", Message="
        + Message
        + ", Status="
        + Status
        + ", CreatedDate="
        + CreatedDate
        + "]";
  }
}

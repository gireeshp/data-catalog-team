package com.augmentiq.maxiq.model.marketplace.bean;

import org.bson.types.ObjectId;

public class InputConnectors {
  ObjectId id;
  ObjectId SolutionId;
  String FlowOrDsOrNoteId;
  ObjectId TypeOfSourcesId;
  Integer VersionId;
  Boolean Status;

  public InputConnectors() {
    super();
  }

  public InputConnectors(
      ObjectId solutionId,
      String flowOrDsOrNoteId,
      ObjectId typeOfSourcesId,
      Integer versionId,
      Boolean status) {
    super();
    SolutionId = solutionId;
    FlowOrDsOrNoteId = flowOrDsOrNoteId;
    TypeOfSourcesId = typeOfSourcesId;
    VersionId = versionId;
    Status = status;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public ObjectId getSolutionId() {
    return SolutionId;
  }

  public void setSolutionId(ObjectId solutionId) {
    SolutionId = solutionId;
  }

  public String getFlowOrDsOrNoteId() {
    return FlowOrDsOrNoteId;
  }

  public void setFlowOrDsOrNoteId(String flowOrDsOrNoteId) {
    FlowOrDsOrNoteId = flowOrDsOrNoteId;
  }

  public ObjectId getTypeOfSourcesId() {
    return TypeOfSourcesId;
  }

  public void setTypeOfSourcesId(ObjectId typeOfSourcesId) {
    TypeOfSourcesId = typeOfSourcesId;
  }

  public Integer getVersionId() {
    return VersionId;
  }

  public void setVersionId(Integer versionId) {
    VersionId = versionId;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  @Override
  public String toString() {
    return "InputConnectors [id="
        + id
        + ", SolutionId="
        + SolutionId
        + ", FlowOrDsOrNoteId="
        + FlowOrDsOrNoteId
        + ", TypeOfSourcesId="
        + TypeOfSourcesId
        + ", VersionId="
        + VersionId
        + ", Status="
        + Status
        + "]";
  }
}

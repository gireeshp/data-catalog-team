package com.augmentiq.maxiq.model.marketplace.bean;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class InstallationDetails {
  private ObjectId Id;
  private String SolutionId;
  private String ClientId;
  private Boolean Status;
  private String InstalledBy;
  private String UninstalledBy;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date installedDate;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date UninstalledDate;

  public InstallationDetails() {
    super();
  }

  public InstallationDetails(
      String solutionId,
      String clientId,
      Boolean status,
      String installedBy,
      String uninstalledBy,
      Date installedDate,
      Date uninstalledDate) {
    super();
    SolutionId = solutionId;
    ClientId = clientId;
    Status = status;
    InstalledBy = installedBy;
    UninstalledBy = uninstalledBy;
    this.installedDate = installedDate;
    UninstalledDate = uninstalledDate;
  }

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    Id = id;
  }

  public String getSolutionId() {
    return SolutionId;
  }

  public void setSolutionId(String solutionId) {
    SolutionId = solutionId;
  }

  public String getClientId() {
    return ClientId;
  }

  public void setClientId(String clientId) {
    ClientId = clientId;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  public String getInstalledBy() {
    return InstalledBy;
  }

  public void setInstalledBy(String installedBy) {
    InstalledBy = installedBy;
  }

  public String getUninstalledBy() {
    return UninstalledBy;
  }

  public void setUninstalledBy(String uninstalledBy) {
    UninstalledBy = uninstalledBy;
  }

  public Date getInstalledDate() {
    return installedDate;
  }

  public void setInstalledDate(Date installedDate) {
    this.installedDate = installedDate;
  }

  public Date getUninstalledDate() {
    return UninstalledDate;
  }

  public void setUninstalledDate(Date uninstalledDate) {
    UninstalledDate = uninstalledDate;
  }

  @Override
  public String toString() {
    return "InstallationDetails [Id="
        + Id
        + ", SolutionId="
        + SolutionId
        + ", ClientId="
        + ClientId
        + ", Status="
        + Status
        + ", InstalledBy="
        + InstalledBy
        + ", UninstalledBy="
        + UninstalledBy
        + ", installedDate="
        + installedDate
        + ", UninstalledDate="
        + UninstalledDate
        + "]";
  }
}

package com.augmentiq.maxiq.model.marketplace.bean;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Interest {
  ObjectId Id;
  String Client;
  String SolutionId;
  String UserId;
  Boolean Status;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  Date CreatedDate;

  public Interest() {
    super();
  }

  public Interest(
      String client, String solutionId, String userId, Boolean status, Date createdDate) {
    super();
    Client = client;
    SolutionId = solutionId;
    UserId = userId;
    Status = status;
    CreatedDate = createdDate;
  }

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    Id = id;
  }

  public String getClient() {
    return Client;
  }

  public void setClient(String client) {
    Client = client;
  }

  public String getSolutionId() {
    return SolutionId;
  }

  public void setSolutionId(String solutionId) {
    SolutionId = solutionId;
  }

  public String getUserId() {
    return UserId;
  }

  public void setUserId(String userId) {
    UserId = userId;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  public Date getCreatedDate() {
    return CreatedDate;
  }

  public void setCreatedDate(Date createdDate) {
    CreatedDate = createdDate;
  }

  @Override
  public String toString() {
    return "Interest [Id="
        + Id
        + ", ClientID="
        + Client
        + ", SolutionId="
        + SolutionId
        + ", UserId="
        + UserId
        + ", Status="
        + Status
        + ", CreatedDate="
        + CreatedDate
        + "]";
  }
}

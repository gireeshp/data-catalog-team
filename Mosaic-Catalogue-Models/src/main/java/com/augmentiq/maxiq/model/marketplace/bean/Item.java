package com.augmentiq.maxiq.model.marketplace.bean;

import org.bson.types.ObjectId;

/**
 * Needful for common used in any transaction
 *
 * @see used InstallationDetails
 * @author Ajinkya Marathe
 */
public class Item {
  private ObjectId Id;
  private String Name;

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    Id = id;
  }

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }

  public Item(ObjectId id, String name) {
    super();
    Id = id;
    Name = name;
  }

  @Override
  public String toString() {
    return "Item [Id=" + Id + ", Name=" + Name + "]";
  }
}

package com.augmentiq.maxiq.model.marketplace.bean;

import java.util.Date;
import java.util.List;

import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Solution {
  private ObjectId id;
  private String SolutionName;
  private String SolutionShortDescription;
  private String SolutionFullDescription;
  private Binary IconImage;
  private Binary Image;
  private String CatalogLink;
  private String DemoLink;
  private ObjectId CategoryId;
  private Boolean Status;
  private String CreatedBy;
  private String LastModifiedBy;
  private Integer VersionId;
  private String VersionName;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date ModifiedDate;

  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Date CreatedDate;

  private List<String> InputDataSources;
  private List<String> Processing;
  private List<String> Outcomes;
  private List<InputConnectors> listInputConnectors;
  private List<Version> listVersions;
  private String StringId;
  private Boolean installable;

  public Solution() {
    super();
  }

  public Solution(
      String SolutionName,
      String SolutionShortDescription,
      String SolutionFullDescription,
      Binary IconImage,
      Binary Image,
      String CatalogLink,
      String DemoLink,
      ObjectId CategoryId,
      Boolean Status,
      String CreatedBy,
      String LastModifiedBy,
      Integer VersionId,
      String VersionName,
      Date ModifiedDate,
      Date CreatedDate,
      List<String> InputDataSources,
      List<String> Processing,
      List<String> Outcomes,
      List<InputConnectors> listInputConnectors,
      List<Version> listVersions,
      Boolean installable) {
    super();
    this.SolutionName = SolutionName;
    this.SolutionShortDescription = SolutionShortDescription;
    this.SolutionFullDescription = SolutionFullDescription;
    this.IconImage = IconImage;
    this.Image = Image;
    this.CatalogLink = CatalogLink;
    this.DemoLink = DemoLink;
    this.CategoryId = CategoryId;
    this.Status = Status;
    this.CreatedBy = CreatedBy;
    this.LastModifiedBy = LastModifiedBy;
    this.VersionId = VersionId;
    this.VersionName = VersionName;
    this.ModifiedDate = ModifiedDate;
    this.CreatedDate = CreatedDate;
    this.InputDataSources = InputDataSources;
    this.Processing = Processing;
    this.Outcomes = Outcomes;
    this.listInputConnectors = listInputConnectors;
    this.listVersions = listVersions;
    this.installable = installable;
    ;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getSolutionName() {
    return SolutionName;
  }

  public void setSolutionName(String solutionName) {
    SolutionName = solutionName;
  }

  public String getSolutionShortDescription() {
    return SolutionShortDescription;
  }

  public void setSolutionShortDescription(String solutionShortDescription) {
    SolutionShortDescription = solutionShortDescription;
  }

  public String getSolutionFullDescription() {
    return SolutionFullDescription;
  }

  public void setSolutionFullDescription(String solutionFullDescription) {
    SolutionFullDescription = solutionFullDescription;
  }

  public Binary getIconImage() {
    return IconImage;
  }

  public void setIconImage(Binary iconImage) {
    IconImage = iconImage;
  }

  public Binary getImage() {
    return Image;
  }

  public void setImage(Binary image) {
    Image = image;
  }

  public String getCatalogLink() {
    return CatalogLink;
  }

  public void setCatalogLink(String catalogLink) {
    CatalogLink = catalogLink;
  }

  public String getDemoLink() {
    return DemoLink;
  }

  public void setDemoLink(String demoLink) {
    DemoLink = demoLink;
  }

  public ObjectId getCategoryId() {
    return CategoryId;
  }

  public void setCategoryId(ObjectId categoryId) {
    CategoryId = categoryId;
  }

  public Boolean getStatus() {
    return Status;
  }

  public void setStatus(Boolean status) {
    Status = status;
  }

  public String getCreatedBy() {
    return CreatedBy;
  }

  public void setCreatedBy(String createdBy) {
    CreatedBy = createdBy;
  }

  public String getLastModifiedBy() {
    return LastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    LastModifiedBy = lastModifiedBy;
  }

  public Integer getVersionId() {
    return VersionId;
  }

  public void setVersionId(Integer versionId) {
    VersionId = versionId;
  }

  public String getVersionName() {
    return VersionName;
  }

  public void setVersionName(String versionName) {
    VersionName = versionName;
  }

  public Date getModifiedDate() {
    return ModifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    ModifiedDate = modifiedDate;
  }

  public Date getCreatedDate() {
    return CreatedDate;
  }

  public void setCreatedDate(Date createdDate) {
    CreatedDate = createdDate;
  }

  public List<String> getInputDataSources() {
    return InputDataSources;
  }

  public void setInputDataSources(List<String> inputDataSources) {
    InputDataSources = inputDataSources;
  }

  public List<String> getProcessing() {
    return Processing;
  }

  public void setProcessing(List<String> processing) {
    Processing = processing;
  }

  public List<String> getOutcomes() {
    return Outcomes;
  }

  public void setOutcomes(List<String> outcomes) {
    Outcomes = outcomes;
  }

  public List<InputConnectors> getListInputConnectors() {
    return listInputConnectors;
  }

  public void setListInputConnectors(List<InputConnectors> listInputConnectors) {
    this.listInputConnectors = listInputConnectors;
  }

  public List<Version> getListVersions() {
    return listVersions;
  }

  public void setListVersions(List<Version> listVersions) {
    this.listVersions = listVersions;
  }

  public String getStringId() {
    StringId = id.toHexString();
    return StringId;
  }

  public void setStringId(String stringId) {
    StringId = stringId;
  }

  public Boolean getInstallable() {
    return installable;
  }

  public void setInstallable(Boolean installable) {
    this.installable = installable;
  }

  @Override
  public String toString() {
    return "Solution [id="
        + id
        + ", SolutionName="
        + SolutionName
        + ", SolutionShortDescription="
        + SolutionShortDescription
        + ", SolutionFullDescription="
        + SolutionFullDescription
        + ", IconImage="
        + IconImage
        + ", Image="
        + Image
        + ", CatalogLink="
        + CatalogLink
        + ", DemoLink="
        + DemoLink
        + ", CategoryId="
        + CategoryId
        + ", Status="
        + Status
        + ", CreatedBy="
        + CreatedBy
        + ", LastModifiedBy="
        + LastModifiedBy
        + ", VersionId="
        + VersionId
        + ", VersionName="
        + VersionName
        + ", ModifiedDate="
        + ModifiedDate
        + ", CreatedDate="
        + CreatedDate
        + ", InputDataSources="
        + InputDataSources
        + ", Processing="
        + Processing
        + ", Outcomes="
        + Outcomes
        + ", listInputConnectors="
        + listInputConnectors
        + ", listVersions="
        + listVersions
        + ", StringId="
        + StringId
        + ", installable="
        + installable
        + "]";
  }
}

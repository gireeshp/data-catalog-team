package com.augmentiq.maxiq.model.marketplace.bean;

import org.bson.types.ObjectId;

public class TypeOfSources {

  ObjectId Id;
  String Name;
  Boolean status;

  public TypeOfSources() {
    super();
  }

  public TypeOfSources(String name, Boolean status) {
    super();
    Name = name;
    this.status = status;
  }

  public ObjectId getId() {
    return Id;
  }

  public void setId(ObjectId id) {
    Id = id;
  }

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }

  public Boolean getStatus() {
    return status;
  }

  public void setStatus(Boolean status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "TypeOfSources [Id=" + Id + ", Name=" + Name + ", status=" + status + "]";
  }
}

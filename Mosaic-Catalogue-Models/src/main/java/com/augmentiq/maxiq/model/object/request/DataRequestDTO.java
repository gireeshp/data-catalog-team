package com.augmentiq.maxiq.model.object.request;

import com.augmentiq.maxiq.constant.configuration.enums.AccessTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataRequestEnum;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRequest;

/** @author Rushi created on Jul 14, 2017 for MAX-1058 */
public class DataRequestDTO {

  private Long id;
  private Long objectId;
  private String objectType;
  private Long requestedBy;
  private DataRequestEnum status;
  private AccessTypeEnum requestedAccess;
  private Long last_modified_by;
  private String justification;
  private Long created_date;
  private Long modified_date;
  private Long group_id;
  private Long requested_user_id;
  private String rejected_justification;

  public DataRequestDTO() {
    super();
  }

  public DataRequestDTO(DataRequest dataRequest) {
    if (null != dataRequest) {
      this.id = dataRequest.getId();
      this.objectId = dataRequest.getObjectId();
      this.objectType = dataRequest.getObjectType();
      this.requestedBy = dataRequest.getRequestedBy();
      this.status = DataRequestEnum.get(dataRequest.getStatus());
      this.requestedAccess = AccessTypeEnum.get(dataRequest.getRequestedAccess());
      this.last_modified_by = dataRequest.getLast_modified_by();
      this.justification = dataRequest.getJustification();
      this.created_date = dataRequest.getCreated_date();
      this.modified_date = dataRequest.getModified_date();
      this.group_id = dataRequest.getGroup_id();
      this.requested_user_id = dataRequest.getRequested_user_id();
      this.rejected_justification = dataRequest.getJustification();
    }
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getObjectId() {
    return objectId;
  }

  public void setObjectId(Long objectId) {
    this.objectId = objectId;
  }

  public String getObjectType() {
    return objectType;
  }

  public void setObjectType(String objectType) {
    this.objectType = objectType;
  }

  public Long getRequestedBy() {
    return requestedBy;
  }

  public void setRequestedBy(Long requestedBy) {
    this.requestedBy = requestedBy;
  }

  public DataRequestEnum getStatus() {
    return status;
  }

  public void setStatus(DataRequestEnum status) {
    this.status = status;
  }

  public AccessTypeEnum getRequestedAccess() {
    return requestedAccess;
  }

  public void setRequestedAccess(AccessTypeEnum requestedAccess) {
    this.requestedAccess = requestedAccess;
  }

  public Long getLastModifiedBy() {
    return last_modified_by;
  }

  public void setLastModifiedBy(Long lastModifiedBy) {
    this.last_modified_by = lastModifiedBy;
  }

  public String getJustification() {
    return justification;
  }

  public void setJustification(String justification) {
    this.justification = justification;
  }

  public Long getCreatedDate() {
    return created_date;
  }

  public void setCreatedDate(Long createdDate) {
    this.created_date = createdDate;
  }

  public Long getModifiedDate() {
    return modified_date;
  }

  public void setModifiedDate(Long modifiedDate) {
    this.modified_date = modifiedDate;
  }

  public Long getGroupId() {
    return group_id;
  }

  public void setGroupId(Long groupId) {
    this.group_id = groupId;
  }

  public Long getRequestedUserId() {
    return requested_user_id;
  }

  public void setRequestedUserId(Long requestedUserId) {
    this.requested_user_id = requestedUserId;
  }

  public String getRejectedJustification() {
    return rejected_justification;
  }

  public void setRejectedJustification(String rejectedJustification) {
    this.rejected_justification = rejectedJustification;
  }
}

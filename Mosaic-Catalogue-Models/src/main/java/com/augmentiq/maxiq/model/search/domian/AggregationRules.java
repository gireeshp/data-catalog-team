package com.augmentiq.maxiq.model.search.domian;

import com.augmentiq.maxiq.constant.configuration.enums.AggregationTypes;

/** Created by shivanand on 7/14/2015. */
public class AggregationRules {
  private AggregationTypes aggregationTypes;
  private String fieldName;

  public AggregationTypes getAggregationTypes() {
    return aggregationTypes;
  }

  public void setAggregationTypes(AggregationTypes aggregationTypes) {
    this.aggregationTypes = aggregationTypes;
  }

  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  @Override
  public String toString() {
    return "AggregationRules{"
        + "aggregationTypes="
        + aggregationTypes
        + ", fieldName='"
        + fieldName
        + '\''
        + '}';
  }
}
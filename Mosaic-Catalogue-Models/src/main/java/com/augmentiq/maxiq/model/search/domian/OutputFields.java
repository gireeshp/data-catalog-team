package com.augmentiq.maxiq.model.search.domian;

/** Created by shivanand on 7/14/2015. */
public class OutputFields {
  private boolean selected;
  private String fieldName;

  public OutputFields() {}

  public OutputFields(boolean selected, String fieldName) {
    this.selected = selected;
    this.fieldName = fieldName;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  @Override
  public String toString() {
    return "OutputFields{" + "selected=" + selected + ", fieldName='" + fieldName + '\'' + '}';
  }
}
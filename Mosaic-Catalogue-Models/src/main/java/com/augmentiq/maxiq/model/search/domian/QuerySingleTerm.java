package com.augmentiq.maxiq.model.search.domian;

import com.augmentiq.maxiq.constant.configuration.enums.SearchBoolCondition;
import com.augmentiq.maxiq.constant.configuration.enums.SearchMatchType;
import com.augmentiq.maxiq.constant.configuration.enums.SearchQueryType;

/** Created by shivanand on 7/14/2015. */
public class QuerySingleTerm {
  private String fieldName;
  private int boost;
  private SearchQueryType queryType;
  private SearchBoolCondition boolCondition;
  private SearchMatchType matchType;

  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  public int getBoost() {
    return boost;
  }

  public void setBoost(int boost) {
    this.boost = boost;
  }

  public SearchQueryType getQueryType() {
    return queryType;
  }

  public void setQueryType(SearchQueryType queryType) {
    this.queryType = queryType;
  }

  public SearchBoolCondition getBoolCondition() {
    return boolCondition;
  }

  public void setBoolCondition(SearchBoolCondition boolCondition) {
    this.boolCondition = boolCondition;
  }

  public SearchMatchType getMatchType() {
    return matchType;
  }

  public void setMatchType(SearchMatchType matchType) {
    this.matchType = matchType;
  }

  @Override
  public String toString() {
    return "QuerySingleTerm{"
        + "fieldName='"
        + fieldName
        + '\''
        + ", boost="
        + boost
        + ", queryType="
        + queryType
        + ", boolCondition="
        + boolCondition
        + ", matchType="
        + matchType
        + '}';
  }
}
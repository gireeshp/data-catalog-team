package com.augmentiq.maxiq.model.services.glossary;

import java.io.Serializable;

public class GlossaryDTO implements Serializable {
  private String createdByName;
  private String modifiedByName;
  private Long id;
  private String name;
  private String shortDescription;
  private String longDescription;
  private Long createdDate;
  private Long createdBy;
  private Long modifiedBy;
  private Long modifiedDate;
  private Long groupId;
  private Integer status;

  public String getCreatedByName() {
    return createdByName;
  }

  public void setCreatedByName(String createdByName) {
    this.createdByName = createdByName;
  }

  public String getModifiedByName() {
    return modifiedByName;
  }

  public void setModifiedByName(String modifiedByName) {
    this.modifiedByName = modifiedByName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getLongDescription() {
    return longDescription;
  }

  public void setLongDescription(String longDescription) {
    this.longDescription = longDescription;
  }

  public Long getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Long getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(Long modifiedBy) {
    this.modifiedBy = modifiedBy;
  }

  public Long getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Long modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "GlossaryDTO [createdByName="
        + createdByName
        + ", modifiedByName="
        + modifiedByName
        + ", id="
        + id
        + ", name="
        + name
        + ", shortDescription="
        + shortDescription
        + ", longDescription="
        + longDescription
        + ", createdDate="
        + createdDate
        + ", createdBy="
        + createdBy
        + ", modifiedBy="
        + modifiedBy
        + ", modifiedDate="
        + modifiedDate
        + ", groupId="
        + groupId
        + ", status="
        + status
        + "]";
  }
}

package com.augmentiq.maxiq.model.setup.domains;

import java.util.List;

public class ApplicationDetails {

  private List<String> selectedApps;
  private List<Long> selectedAppIds;
  private List<String> availableApps;

  public List<String> getSelectedApps() {
    return selectedApps;
  }

  public void setSelectedApps(List<String> selectedApps) {
    this.selectedApps = selectedApps;
  }

  public List<Long> getSelectedAppIds() {
    return selectedAppIds;
  }

  public void setSelectedAppIds(List<Long> selectedAppIds) {
    this.selectedAppIds = selectedAppIds;
  }

  public List<String> getAvailableApps() {
    return availableApps;
  }

  public void setAvailableApps(List<String> availableApps) {
    this.availableApps = availableApps;
  }
}

package com.augmentiq.maxiq.model.setup.domains;

import java.util.List;

public class GroupDetails {

  private List<String> selectedGroups;
  private List<Long> selectedGroupsIds;
  private List<String> availableGroups;

  public List<String> getSelectedGroups() {
    return selectedGroups;
  }

  public void setSelectedGroups(List<String> selectedGroups) {
    this.selectedGroups = selectedGroups;
  }

  public List<Long> getSelectedGroupsIds() {
    return selectedGroupsIds;
  }

  public void setSelectedGroupsIds(List<Long> selectedGroupsIds) {
    this.selectedGroupsIds = selectedGroupsIds;
  }

  public List<String> getAvailableGroups() {
    return availableGroups;
  }

  public void setAvailableGroups(List<String> availableGroups) {
    this.availableGroups = availableGroups;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("GroupDetails [selectedGroups=");
    builder.append(selectedGroups);
    builder.append(", selectedGroupsIds=");
    builder.append(selectedGroupsIds);
    builder.append(", availableGroups=");
    builder.append(availableGroups);
    builder.append("]");
    return builder.toString();
  }
}

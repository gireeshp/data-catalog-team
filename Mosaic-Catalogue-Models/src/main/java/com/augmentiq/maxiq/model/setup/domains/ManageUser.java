package com.augmentiq.maxiq.model.setup.domains;

import java.util.List;

import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class ManageUser {

  private ApplicationUser userDetails;
  private List<Solution> solutions;
  private List<PersonaMaster> personaMasters;

  public List<PersonaMaster> getPersonaMasters() {
    return personaMasters;
  }

  public void setPersonaMasters(List<PersonaMaster> personaMasters) {
    this.personaMasters = personaMasters;
  }

  public List<Solution> getSolutions() {
    return solutions;
  }

  public void setSolutions(List<Solution> solutions) {
    this.solutions = solutions;
  }
  //	private GroupDetails groupDetails;
  //	private SubGroupDetails subGroupDetails;
  //	private RoleDetails roleDetails;
  //	private ApplicationDetails applicationDetails;
  public ApplicationUser getUserDetails() {
    return userDetails;
  }

  public void setUserDetails(ApplicationUser userDetails) {
    this.userDetails = userDetails;
  }

  //	public GroupDetails getGroupDetails() {
  //		return groupDetails;
  //	}
  //	public void setGroupDetails(GroupDetails groupDetails) {
  //		this.groupDetails = groupDetails;
  //	}
  //	public SubGroupDetails getSubGroupDetails() {
  //		return subGroupDetails;
  //	}
  //	public void setSubGroupDetails(SubGroupDetails subGroupDetails) {
  //		this.subGroupDetails = subGroupDetails;
  //	}
  //	public RoleDetails getRoleDetails() {
  //		return roleDetails;
  //	}
  //	public void setRoleDetails(RoleDetails roleDetails) {
  //		this.roleDetails = roleDetails;
  //	}
  //	public ApplicationDetails getApplicationDetails() {
  //		return applicationDetails;
  //	}
  //	public void setApplicationDetails(ApplicationDetails applicationDetails) {
  //		this.applicationDetails = applicationDetails;
  //	}
  //	@Override
  //	public String toString() {
  //		StringBuilder builder = new StringBuilder();
  //		builder.append("ManageUser [userDetails=");
  //		builder.append(userDetails);
  //		builder.append(", groupDetails=");
  //	//	builder.append(groupDetails);
  //		builder.append(", subGroupDetails=");
  //	//	builder.append(subGroupDetails);
  //		builder.append(", roleDetails=");
  //	//	builder.append(roleDetails);
  //		builder.append("]");
  //		return builder.toString();
  //	}

}

package com.augmentiq.maxiq.model.setup.domains;

import java.util.List;

import com.augmentiq.maxiq.entity.model.setup.domains.ActionsMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.UserRoleMaster;

public class RoleActions {

  private UserRoleMaster roleObject;
  private List<ActionsMaster> actionsObject;

  public UserRoleMaster getRoleObject() {
    return roleObject;
  }

  public void setRoleObject(UserRoleMaster roleObject) {
    this.roleObject = roleObject;
  }

  public List<ActionsMaster> getActionsObject() {
    return actionsObject;
  }

  public void setActionsObject(List<ActionsMaster> actionsObject) {
    this.actionsObject = actionsObject;
  }

  @Override
  public String toString() {
    return "RoleActions [roleObject=" + roleObject + ", actionsObject=" + actionsObject + "]";
  }
}

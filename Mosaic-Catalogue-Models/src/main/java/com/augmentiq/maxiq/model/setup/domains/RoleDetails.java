package com.augmentiq.maxiq.model.setup.domains;

import java.util.List;

public class RoleDetails {

  private List<String> selectedRoles;
  private List<Long> selectedRolesIds;
  private List<String> availableRoles;

  public List<String> getSelectedRoles() {
    return selectedRoles;
  }

  public void setSelectedRoles(List<String> selectedRoles) {
    this.selectedRoles = selectedRoles;
  }

  public List<Long> getSelectedRolesIds() {
    return selectedRolesIds;
  }

  public void setSelectedRolesIds(List<Long> selectedRolesIds) {
    this.selectedRolesIds = selectedRolesIds;
  }

  public List<String> getAvailableRoles() {
    return availableRoles;
  }

  public void setAvailableRoles(List<String> availableRoles) {
    this.availableRoles = availableRoles;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RoleDetails [selectedRoles=");
    builder.append(selectedRoles);
    builder.append(", selectedRolesIds=");
    builder.append(selectedRolesIds);
    builder.append(", availableRoles=");
    builder.append(availableRoles);
    builder.append("]");
    return builder.toString();
  }
}

package com.augmentiq.maxiq.model.setup.domains;

import java.util.List;

public class Solution {
  private Long appId;
  //private String appName;
  private List<Long> selectedGroupsIds;
  private List<Long> selectedSubgroupsIds;
  private List<Long> selectedRolesIds;
  //	private GroupDetails groupDetails;
  //	private SubGroupDetails subGroupDetails;
  //	private RoleDetails roleDetails;

  public List<Long> getSelectedGroupsIds() {
    return selectedGroupsIds;
  }

  public List<Long> getSelectedSubgroupsIds() {
    return selectedSubgroupsIds;
  }

  public List<Long> getSelectedRolesIds() {
    return selectedRolesIds;
  }

  public void setSelectedGroupsIds(List<Long> selectedGroupsIds) {
    this.selectedGroupsIds = selectedGroupsIds;
  }

  public void setSelectedSubgroupsIds(List<Long> selectedSubgroupsIds) {
    this.selectedSubgroupsIds = selectedSubgroupsIds;
  }

  public void setSelectedRolesIds(List<Long> selectedRolesIds) {
    this.selectedRolesIds = selectedRolesIds;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  //	public GroupDetails getGroupDetails() {
  //		return groupDetails;
  //	}
  //	public void setGroupDetails(GroupDetails groupDetails) {
  //		this.groupDetails = groupDetails;
  //	}
  //	public SubGroupDetails getSubGroupDetails() {
  //		return subGroupDetails;
  //	}
  //	public void setSubGroupDetails(SubGroupDetails subGroupDetails) {
  //		this.subGroupDetails = subGroupDetails;
  //	}
  //	public RoleDetails getRoleDetails() {
  //		return roleDetails;
  //	}
  //	public void setRoleDetails(RoleDetails roleDetails) {
  //		this.roleDetails = roleDetails;
  //	}

}

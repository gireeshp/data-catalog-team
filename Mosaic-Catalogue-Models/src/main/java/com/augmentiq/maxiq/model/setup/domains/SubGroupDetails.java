package com.augmentiq.maxiq.model.setup.domains;

import java.util.List;

public class SubGroupDetails {

  private List<String> selectedSubgroups;
  private List<Long> selectedSubgroupsIds;
  private List<String> availableSubgroups;

  public List<String> getSelectedSubgroups() {
    return selectedSubgroups;
  }

  public void setSelectedSubgroups(List<String> selectedSubgroups) {
    this.selectedSubgroups = selectedSubgroups;
  }

  public List<Long> getSelectedSubgroupsIds() {
    return selectedSubgroupsIds;
  }

  public void setSelectedSubgroupsIds(List<Long> selectedSubgroupsIds) {
    this.selectedSubgroupsIds = selectedSubgroupsIds;
  }

  public List<String> getAvailableSubgroups() {
    return availableSubgroups;
  }

  public void setAvailableSubgroups(List<String> availableSubgroups) {
    this.availableSubgroups = availableSubgroups;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SubGroupDetails [selectedSubgroups=");
    builder.append(selectedSubgroups);
    builder.append(", selectedSubgroupsIds=");
    builder.append(selectedSubgroupsIds);
    builder.append(", availableSubgroups=");
    builder.append(availableSubgroups);
    builder.append("]");
    return builder.toString();
  }
}

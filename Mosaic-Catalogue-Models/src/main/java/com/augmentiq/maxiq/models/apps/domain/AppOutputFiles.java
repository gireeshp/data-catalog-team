package com.augmentiq.maxiq.models.apps.domain;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

//alter table app_output_files add column lastRun varchar(100);
//alter table app_output_files add column lastrefreshBy varchar(100);
//alter table app_output_files add column nodeName varchar(200);
//alter table app_output_files add column savedForExploration varchar(100);

@TableName(tableName = Sequences.APP_OUTPUT_FILES)
public class AppOutputFiles {

  @Id private Long id;
  private Long appId;
  private Long appInstanceId;
  private String fileType;
  private String compressionType;
  private String nodeId;
  private String path;
  private String appName;
  private String label;
  private String lastRun;
  private String lastrefreshBy;
  private String nodeName;
  private String savedForExploration;
  private Long groupId;
  private String keyName;
  private Long baapJobInstId;

  public String getFileType() {
    return fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public String getCompressionType() {
    return compressionType;
  }

  public void setCompressionType(String compressionType) {
    this.compressionType = compressionType;
  }

  public String getKeyName() {
    return keyName;
  }

  public void setKeyName(String keyName) {
    this.keyName = keyName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public Long getAppInstanceId() {
    return appInstanceId;
  }

  public void setAppInstanceId(Long appInstanceId) {
    this.appInstanceId = appInstanceId;
  }

  public String getNodeId() {
    return nodeId;
  }

  public void setNodeId(String nodeId) {
    this.nodeId = nodeId;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public String getLastRun() {
    return lastRun;
  }

  public void setLastRun(String lastRun) {
    this.lastRun = lastRun;
  }

  public String getLastrefreshBy() {
    return lastrefreshBy;
  }

  public void setLastrefreshBy(String lastrefreshBy) {
    this.lastrefreshBy = lastrefreshBy;
  }

  public String getNodeName() {
    return nodeName;
  }

  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  public String getSavedForExploration() {
    return savedForExploration;
  }

  public void setSavedForExploration(String savedForExploration) {
    this.savedForExploration = savedForExploration;
  }

  public Long getBaapJobInstId() {
    return baapJobInstId;
  }

  public void setBaapJobInstId(Long baapJobInstId) {
    this.baapJobInstId = baapJobInstId;
  }

  public AppOutputFiles(
      Long id,
      Long appId,
      Long appInstanceId,
      String nodeId,
      String path,
      String appName,
      String label,
      String keyName,
      Long baapJobInstId) {
    super();
    this.id = id;
    this.appId = appId;
    this.appInstanceId = appInstanceId;
    this.nodeId = nodeId;
    this.path = path;
    this.appName = appName;
    this.label = label;
    this.keyName = keyName;
    this.baapJobInstId = baapJobInstId;
  }

  public AppOutputFiles() {
    super();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((appInstanceId == null) ? 0 : appInstanceId.hashCode());
    result = prime * result + ((appName == null) ? 0 : appName.hashCode());
    result = prime * result + ((compressionType == null) ? 0 : compressionType.hashCode());
    result = prime * result + ((fileType == null) ? 0 : fileType.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((keyName == null) ? 0 : keyName.hashCode());
    result = prime * result + ((label == null) ? 0 : label.hashCode());
    result = prime * result + ((lastRun == null) ? 0 : lastRun.hashCode());
    result = prime * result + ((lastrefreshBy == null) ? 0 : lastrefreshBy.hashCode());
    result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
    result = prime * result + ((nodeName == null) ? 0 : nodeName.hashCode());
    result = prime * result + ((path == null) ? 0 : path.hashCode());
    result = prime * result + ((savedForExploration == null) ? 0 : savedForExploration.hashCode());
    result = prime * result + ((baapJobInstId == null) ? 0 : baapJobInstId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AppOutputFiles other = (AppOutputFiles) obj;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (appInstanceId == null) {
      if (other.appInstanceId != null) return false;
    } else if (!appInstanceId.equals(other.appInstanceId)) return false;
    if (appName == null) {
      if (other.appName != null) return false;
    } else if (!appName.equals(other.appName)) return false;
    if (compressionType == null) {
      if (other.compressionType != null) return false;
    } else if (!compressionType.equals(other.compressionType)) return false;
    if (fileType == null) {
      if (other.fileType != null) return false;
    } else if (!fileType.equals(other.fileType)) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (keyName == null) {
      if (other.keyName != null) return false;
    } else if (!keyName.equals(other.keyName)) return false;
    if (label == null) {
      if (other.label != null) return false;
    } else if (!label.equals(other.label)) return false;
    if (lastRun == null) {
      if (other.lastRun != null) return false;
    } else if (!lastRun.equals(other.lastRun)) return false;
    if (lastrefreshBy == null) {
      if (other.lastrefreshBy != null) return false;
    } else if (!lastrefreshBy.equals(other.lastrefreshBy)) return false;
    if (nodeId == null) {
      if (other.nodeId != null) return false;
    } else if (!nodeId.equals(other.nodeId)) return false;
    if (nodeName == null) {
      if (other.nodeName != null) return false;
    } else if (!nodeName.equals(other.nodeName)) return false;
    if (path == null) {
      if (other.path != null) return false;
    } else if (!path.equals(other.path)) return false;
    if (savedForExploration == null) {
      if (other.savedForExploration != null) return false;
    } else if (!savedForExploration.equals(other.savedForExploration)) return false;
    if (baapJobInstId == null) {
      if (other.baapJobInstId != null) return false;
    } else if (!baapJobInstId.equals(other.baapJobInstId)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "AppOutputFiles [id="
        + id
        + ", appId="
        + appId
        + ", appInstanceId="
        + appInstanceId
        + ", fileType="
        + fileType
        + ", compressionType="
        + compressionType
        + ", nodeId="
        + nodeId
        + ", path="
        + path
        + ", appName="
        + appName
        + ", label="
        + label
        + ", lastRun="
        + lastRun
        + ", lastrefreshBy="
        + lastrefreshBy
        + ", nodeName="
        + nodeName
        + ", savedForExploration="
        + savedForExploration
        + ", groupId="
        + groupId
        + ", keyName="
        + keyName
        + " , baapJobInstId="
        + baapJobInstId
        + "]";
  }
}

package com.augmentiq.maxiq.models.apps.domain;

import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.apps.AppStatusEnum;
import com.augmentiq.maxiq.constant.configuration.enums.FlowType;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.RunningStatus;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;
import com.augmentiq.maxiq.entity.model.configuration.bean.AppCombiner;

@TableName(tableName = Sequences.MAXIQ_APPS)
public class Application {
  @Id private Long appId;
  private String appName;
  private String description;
  private Long createSt;
  private AppStatusEnum statusEnum;
  private String userId;
  private String createdBy;
  // last run property
  private Long lastRun;
  private List<AppCombiner> appCombiner;
  private Long groupId;
  private String scheduled;
  private FlowType flowType;
  private Long pollInterval;
  private Long ownerProjectId;
  private RunningStatus runningStatus;

  public Long getOwnerProjectId() {
    return ownerProjectId;
  }

  public void setOwnerProjectId(Long ownerProjectId) {
    this.ownerProjectId = ownerProjectId;
  }

  public FlowType getFlowType() {
    return flowType;
  }

  public void setFlowType(FlowType flowType) {
    this.flowType = flowType;
  }

  public String getScheduled() {
    return scheduled;
  }

  public void setScheduled(String scheduled) {
    this.scheduled = scheduled;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<AppCombiner> getAppCombiner() {
    return appCombiner;
  }

  public void setAppCombiner(List<AppCombiner> appCombiner) {
    this.appCombiner = appCombiner;
  }

  public Long getAppId() {
    return appId;
  }

  public void setAppId(Long appId) {
    this.appId = appId;
  }

  public String getAppName() {
    return appName;
  }

  public void setAppName(String appName) {
    this.appName = appName;
  }

  public Long getCreateSt() {
    return createSt;
  }

  public void setCreateSt(Long createSt) {
    this.createSt = createSt;
  }

  public AppStatusEnum getStatusEnum() {
    return statusEnum;
  }

  public void setStatusEnum(AppStatusEnum statusEnum) {
    this.statusEnum = statusEnum;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Long getLastRun() {
    return lastRun;
  }

  public void setLastRun(Long l) {
    this.lastRun = l;
  }

  public Long getGroupId() {
    return groupId;
  }

  public void setGroupId(Long groupId) {
    this.groupId = groupId;
  }

  public Long getPollInterval() {
    return pollInterval == null ? 0l : pollInterval;
  }

  public void setPollInterval(Long pollInterval) {
    this.pollInterval = pollInterval;
  }

  public RunningStatus getRunningStatus() {
    return runningStatus;
  }

  public void setRunningStatus(RunningStatus runningStatus) {
    this.runningStatus = runningStatus;
  }

  @Override
  public String toString() {
    return "Application [appId="
        + appId
        + ", appName="
        + appName
        + ", description="
        + description
        + ", createSt="
        + createSt
        + ", statusEnum="
        + statusEnum
        + ", userId="
        + userId
        + ", createdBy="
        + createdBy
        + ", lastRun="
        + lastRun
        + ", appCombiner="
        + appCombiner
        + ", groupId="
        + groupId
        + ", scheduled="
        + scheduled
        + ", ownerProjectId="
        + ownerProjectId
        + ", flowType="
        + flowType
        + ", pollInterval="
        + pollInterval
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((appCombiner == null) ? 0 : appCombiner.hashCode());
    result = prime * result + ((appId == null) ? 0 : appId.hashCode());
    result = prime * result + ((appName == null) ? 0 : appName.hashCode());
    result = prime * result + ((createSt == null) ? 0 : createSt.hashCode());
    result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((flowType == null) ? 0 : flowType.hashCode());
    result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
    result = prime * result + ((lastRun == null) ? 0 : lastRun.hashCode());
    result = prime * result + ((ownerProjectId == null) ? 0 : ownerProjectId.hashCode());
    result = prime * result + ((scheduled == null) ? 0 : scheduled.hashCode());
    result = prime * result + ((statusEnum == null) ? 0 : statusEnum.hashCode());
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    Application other = (Application) obj;
    if (appCombiner == null) {
      if (other.appCombiner != null) return false;
    } else if (!appCombiner.equals(other.appCombiner)) return false;
    if (appId == null) {
      if (other.appId != null) return false;
    } else if (!appId.equals(other.appId)) return false;
    if (appName == null) {
      if (other.appName != null) return false;
    } else if (!appName.equals(other.appName)) return false;
    if (createSt == null) {
      if (other.createSt != null) return false;
    } else if (!createSt.equals(other.createSt)) return false;
    if (createdBy == null) {
      if (other.createdBy != null) return false;
    } else if (!createdBy.equals(other.createdBy)) return false;
    if (description == null) {
      if (other.description != null) return false;
    } else if (!description.equals(other.description)) return false;
    if (flowType != other.flowType) return false;
    if (groupId == null) {
      if (other.groupId != null) return false;
    } else if (!groupId.equals(other.groupId)) return false;
    if (lastRun == null) {
      if (other.lastRun != null) return false;
    } else if (!lastRun.equals(other.lastRun)) return false;
    if (ownerProjectId == null) {
      if (other.ownerProjectId != null) return false;
    } else if (!ownerProjectId.equals(other.ownerProjectId)) return false;
    if (scheduled == null) {
      if (other.scheduled != null) return false;
    } else if (!scheduled.equals(other.scheduled)) return false;
    if (statusEnum != other.statusEnum) return false;
    if (userId == null) {
      if (other.userId != null) return false;
    } else if (!userId.equals(other.userId)) return false;
    return true;
  }
}

package com.augmentiq.maxiq.orm.entity.model.connector;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "connection_config")
public class ConnectionConfig {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long connectionId;
	@Column
	private String dbUserName;
	@Column
	private String dbPassword;
	@Transient
	private String driverClassName;
	@Transient
	private String connectionUrl;
	@Column
	private String sid;
	@Column
	private Integer port;
	@Column(unique = true)
	private String conncetionName;
	@Column
	private String createdBy;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDate;
	@Transient
	private String finalQueryToExecute;
	@Transient
	private String queryToExecute;
	@Transient
	private String dbName;
	@Column
	private String ipAddress;
	@Column
	private String nameSpace;
	@Column
	private String defaultEndPointsProtocol;
	@Column
	private String accountName;
	@Column
	private String accountKey;
	
	@Column
	private String hdfsRootDirectory;

	@Column
	private String ftpfilePath;
	
	@Column
	private String amazons3AccessId;
	
	@Column
	private String amazons3SecretId;
	
	@Column
	private String s3BucketName;
	@Column
	private String s3folderName;
	
	public Long getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(Long connectionId) {
		this.connectionId = connectionId;
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getConnectionUrl() {
		return connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}
	
	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getFinalQueryToExecute() {
		return finalQueryToExecute;
	}

	public void setFinalQueryToExecute(String finalQueryToExecute) {
		this.finalQueryToExecute = finalQueryToExecute;
	}

	public String getQueryToExecute() {
		return queryToExecute;
	}

	public void setQueryToExecute(String queryToExecute) {
		this.queryToExecute = queryToExecute;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getConncetionName() {
		return conncetionName;
	}

	public void setConncetionName(String conncetionName) {
		this.conncetionName = conncetionName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getNameSpace() {
		return nameSpace;
	}

	public String getFtpfilePath() {
		return ftpfilePath;
	}

	public void setFtpfilePath(String ftpfilePath) {
		this.ftpfilePath = ftpfilePath;
	}

	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}
	
	public String getDefaultEndPointsProtocol() {
		return defaultEndPointsProtocol;
	}

	public void setDefaultEndPointsProtocol(String defaultEndPointsProtocol) {
		this.defaultEndPointsProtocol = defaultEndPointsProtocol;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountKey() {
		return accountKey;
	}

	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}


	public String getHdfsRootDirectory() {
		return hdfsRootDirectory;
	}

	public void setHdfsRootDirectory(String hdfsRootDirectory) {
		this.hdfsRootDirectory = hdfsRootDirectory;
	}

	public String getAmazons3AccessId() {
		return amazons3AccessId;
	}

	public void setAmazons3AccessId(String amazons3AccessId) {
		this.amazons3AccessId = amazons3AccessId;
	}

	public String getAmazons3SecretId() {
		return amazons3SecretId;
	}

	public void setAmazons3SecretId(String amazons3SecretId) {
		this.amazons3SecretId = amazons3SecretId;
	}

	public String getS3BucketName() {
		return s3BucketName;
	}

	public void setS3BucketName(String s3BucketName) {
		this.s3BucketName = s3BucketName;
	}

	public String getS3folderName() {
		return s3folderName;
	}

	public void setS3folderName(String s3folderName) {
		this.s3folderName = s3folderName;
	}

}

package com.augmentiq.maxiq.orm.entity.model.connector;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "connection_sources")
public class ConnectionSources {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long sourceId;
	@Column
	private String connectionType;
	@Column
	private String sourceImagePath;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "source_subsource_mapping", joinColumns = {
			@JoinColumn(name = "sourceId") }, inverseJoinColumns = { @JoinColumn(name = "subSourceId") })
	private List<ConnectionSubSources> connectionSubSources = new ArrayList<ConnectionSubSources>();

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public String getConnectionType() {
		return connectionType;
	}

	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}

	public String getSourceImagePath() {
		return sourceImagePath;
	}

	public void setSourceImagePath(String sourceImagePath) {
		this.sourceImagePath = sourceImagePath;
	}

	public List<ConnectionSubSources> getConnectionSubSources() {
		return connectionSubSources;
	}

	public void setConnectionSubSources(List<ConnectionSubSources> connectionSubSources) {
		this.connectionSubSources = connectionSubSources;
	}

}

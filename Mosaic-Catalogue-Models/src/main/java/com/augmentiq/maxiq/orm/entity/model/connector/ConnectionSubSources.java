package com.augmentiq.maxiq.orm.entity.model.connector;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "connection_subsources")
public class ConnectionSubSources {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long subSourceId;
	@Column
	private String subConnectionType;
	@Column
	private String sourceImagePath;
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinTable(name = "subsource_connection_mapping", joinColumns = {
			@JoinColumn(name = "subSourceId") }, inverseJoinColumns = { @JoinColumn(name = "connectionId") })
	private List<ConnectionConfig> connectionConfig = new ArrayList<ConnectionConfig>();

	public Long getSubSourceId() {
		return subSourceId;
	}

	public void setSubSourceId(Long subSourceId) {
		this.subSourceId = subSourceId;
	}

	public String getSubConnectionType() {
		return subConnectionType;
	}

	public void setSubConnectionType(String subConnectionType) {
		this.subConnectionType = subConnectionType;
	}

	public List<ConnectionConfig> getConnectionConfig() {
		return connectionConfig;
	}

	public void setConnectionConfig(List<ConnectionConfig> connectionConfig) {
		this.connectionConfig = connectionConfig;
	}

	public String getSourceImagePath() {
		return sourceImagePath;
	}

	public void setSourceImagePath(String sourceImagePath) {
		this.sourceImagePath = sourceImagePath;
	}
	
	
}

package com.augmentiq.maxiq.access.manage.dao;

import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.DATACONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.configuration.enums.AccessTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataRequestEnum;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRequest;
import com.augmentiq.maxiq.entity.model.configuration.bean.DatasourceApprovalMapper;
import com.augmentiq.maxiq.entity.model.configuration.bean.GroupObjectMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.GroupProjectMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.ShareAccessHistory;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserObjectMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserProjectMapping;

/** @author Rushi created on Jul 17, 2017 for MAX-1058 */
public class AccessManageDAO {

  private static final Logger logger = LoggerFactory.getLogger(AccessManageDAO.class);

  /*

  public static void grantAccessHistoryManagementForGroupUnderProjects(
      Long projectId, Long groupId, AccessTypeEnum accessType) throws SystemException {
    ShareAccessHistory accessHistory = new ShareAccessHistory();
    accessHistory.setGroupId(groupId);
    accessHistory.setAssetId(projectId);
    accessHistory.setAssetType(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.ASSET_TYPE[0]);
    accessHistory.setAccessType(accessType.getName());
    accessHistory.setAccess(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.GRANT);
    Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
    accessHistory.setCreatedOn(date.toString());

    MySqlOperations.insert(accessHistory, QueryConstants.DataRequest.SHAREACCHIST_ID);
  }

*/  /*public static void grantAccessHistoryManagementForUserUnderProjects(
      Long projectId, Long userId, AccessTypeEnum accessType) throws SystemException {
    ShareAccessHistory accessHistory = new ShareAccessHistory();
    accessHistory.setUserId(userId);
    accessHistory.setAssetId(projectId);
    accessHistory.setAssetType(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.ASSET_TYPE[0]);
    accessHistory.setAccessType(accessType.getName());
    accessHistory.setAccess(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.GRANT);
    Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
    accessHistory.setCreatedOn(date.toString());

    MySqlOperations.insert(accessHistory, QueryConstants.DataRequest.SHAREACCHIST_ID);
  }*/

  public static void grantAccessHistoryManagementForGroupUnderObjects(
      Long objectId, Long groupId, String objectType, AccessTypeEnum accessType)
      throws SystemException {
    ShareAccessHistory accessHistory = new ShareAccessHistory();
    accessHistory.setGroupId(groupId);
    accessHistory.setAssetId(objectId);
    accessHistory.setAssetType(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.ASSET_TYPE[1]);
    accessHistory.setAccessType(accessType.getName());
    accessHistory.setAccess(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.GRANT);
    Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
    accessHistory.setCreatedOn(date.toString());
    MySqlOperations.insert(accessHistory, QueryConstants.DataRequest.SHAREACCHIST_ID);
  }

  public static void grantAccessHistoryManagementForUserUnderObjects(
      Long objectId, Long userId, String objectType, AccessTypeEnum accessType)
      throws SystemException {
    ShareAccessHistory accessHistory = new ShareAccessHistory();
    accessHistory.setUserId(userId);
    accessHistory.setAssetId(objectId);
    accessHistory.setAssetType(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.ASSET_TYPE[1]);
    accessHistory.setAccessType(accessType.getName());
    accessHistory.setAccess(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.GRANT);
    Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
    accessHistory.setCreatedOn(date.toString());

    MySqlOperations.insert(accessHistory, QueryConstants.DataRequest.SHAREACCHIST_ID);
  }

  /*public static void revokeAccessHistoryManagementForGroupUnderProjects(
      Long projectId, Long groupId, AccessTypeEnum accessType) throws SystemException {
    ShareAccessHistory accessHistory = new ShareAccessHistory();
    accessHistory.setGroupId(groupId);
    accessHistory.setAssetId(projectId);
    accessHistory.setAssetType(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.ASSET_TYPE[0]);
    accessHistory.setAccessType(accessType.getName());
    accessHistory.setAccess(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.REVOKE);
    Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
    accessHistory.setCreatedOn(date.toString());

    MySqlOperations.insert(accessHistory, QueryConstants.DataRequest.SHAREACCHIST_ID);
  }*/

  /*public static void revokeAccessHistoryManagementForUserUnderProjects(
      Long projectId, Long userId, AccessTypeEnum accessType) throws SystemException {
    ShareAccessHistory accessHistory = new ShareAccessHistory();
    accessHistory.setUserId(userId);
    accessHistory.setAssetId(projectId);
    accessHistory.setAssetType(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.ASSET_TYPE[0]);
    accessHistory.setAccessType(accessType.getName());
    accessHistory.setAccess(DATACONSTANTS.SHARE_ACCESS_HISTORY_ACESS_ACTION.REVOKE);
    Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
    accessHistory.setCreatedOn(date.toString());

    MySqlOperations.insert(accessHistory, QueryConstants.DataRequest.SHAREACCHIST_ID);
  }*/

  /*public static void shareProjectWithUserWithAccessType(
      Long projectId, Long userId, AccessTypeEnum accessType) throws SystemException {
    UserProjectMapping userProjectMapping = new UserProjectMapping();
    userProjectMapping.setProjectId(projectId);
    userProjectMapping.setUserId(userId);
    userProjectMapping.setAccessType(accessType.getValue());

    MySqlOperations.insert(userProjectMapping, QueryConstants.DataRequest.ID);
  }*/

 /* public static void revokeAccessOfThisProjectFromGroup(
      Long projectId, Long groupId, AccessTypeEnum accessType) throws SystemException {

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.Project.PROJECT_ID, projectId);
    query.put(QueryConstants.DataRequest.GROUPID, groupId);
    query.put(QueryConstants.Project.ACCESS_TYPE, accessType.getValue());
    GroupProjectMapping groupProjectMapping =
        MySqlOperations.scanOneForQuery(GroupProjectMapping.class, query);

    if (null != groupProjectMapping) {
      MySqlOperations.deleteRow(groupProjectMapping);
    }
  }*/

 /* public static void revokeAccessOfThisProjectFromUser(
      Long projectId, Long userId, AccessTypeEnum accessType) throws SystemException {

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.Project.PROJECT_ID, projectId);
    query.put(QueryConstants.DataRequest.USERID, userId);
    query.put(QueryConstants.Project.ACCESS_TYPE, accessType.getValue());
    UserProjectMapping userProjectMapping =
        MySqlOperations.scanOneForQuery(UserProjectMapping.class, query);

    if (null != userProjectMapping) {
      MySqlOperations.deleteRow(userProjectMapping);
    }
  }*/

  /*public static List<UserProjectMapping> getListOfSharedUsersForThisProject(Long projectId)
      throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.Project.PROJECT_ID, projectId);
    List<UserProjectMapping> userProjectMappings =
        MySqlOperations.scan(UserProjectMapping.class, query);
    return userProjectMappings;
  }*/

  /*public static List<GroupProjectMapping> getListOfSharedGroupsForThisProject(Long projectId)
      throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.Project.PROJECT_ID, projectId);
    List<GroupProjectMapping> groupProjectMappings =
        MySqlOperations.scan(GroupProjectMapping.class, query);
    return groupProjectMappings;
  }*/

  public static void shareObjectWithGroupWithAccessType(
      Long objectId, String objectType, Long groupId, AccessTypeEnum accessType)
      throws SystemException {
    GroupObjectMapping groupObjectMapping = new GroupObjectMapping();

    groupObjectMapping.setGroupId(groupId);
    groupObjectMapping.setObjectId(objectId);
    groupObjectMapping.setObjectType(objectType);
    groupObjectMapping.setAccessType(accessType.getValue());

    MySqlOperations.insert(groupObjectMapping, QueryConstants.DataRequest.ID);
  }

  public static void shareObjectWithUserWithAccessType(
      Long objectId, String objectType, Long userId, AccessTypeEnum accessType)
      throws SystemException {

    UserObjectMapping userObjectMapping = new UserObjectMapping();
    userObjectMapping.setObjectId(objectId);
    userObjectMapping.setObjectType(objectType);
    userObjectMapping.setUserId(userId);
    userObjectMapping.setAccessType(accessType.getValue());

    MySqlOperations.insert(userObjectMapping, QueryConstants.DataRequest.ID);
  }

  /**
   * @param objectId
   * @param objectType
   * @param groupId
   * @param accessType Warn : dont pass LOAD_EDIT here
   * @throws SystemException
   */
  public static void revokeAccessOfThisObjectFromGroup(
      Long objectId, String objectType, Long groupId, AccessTypeEnum accessType)
      throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    query.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    query.put(QueryConstants.DataRequest.GROUPID, groupId);
    query.put(QueryConstants.Project.ACCESS_TYPE, accessType.getValue());
    GroupObjectMapping groupObjectMapping =
        MySqlOperations.scanOneForQuery(GroupObjectMapping.class, query);

    if (null != groupObjectMapping) {
      MySqlOperations.deleteRow(groupObjectMapping);
    }
  }

  public static void revokeAccessOfUserFromThisObjectWithAccessType(
      Long objectId, String objectType, Long userId, AccessTypeEnum accessType)
      throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    query.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    query.put(QueryConstants.DataRequest.USERID, userId);
    query.put(QueryConstants.Project.ACCESS_TYPE, accessType.getValue());
    UserObjectMapping userProjectMapping =
        MySqlOperations.scanOneForQuery(UserObjectMapping.class, query);

    if (null != userProjectMapping) {
      MySqlOperations.deleteRow(userProjectMapping);
    }
  }

  public static List<UserObjectMapping> getListOfSharedUsersForThisObject(
      Long objectId, String objectType) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    query.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    List<UserObjectMapping> userObjectMappings =
        MySqlOperations.scan(UserObjectMapping.class, query);
    return userObjectMappings;
  }

  public static List<GroupObjectMapping> getListOfSharedGroupsForThisObject(
      Long objectId, String objectType) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    query.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    List<GroupObjectMapping> groupObjectMappings =
        MySqlOperations.scan(GroupObjectMapping.class, query);
    return groupObjectMappings;
  }

  public static Long getPendingRequestCount(Class<?> tableName, Long userId)
      throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.DataRequest.REQUESTED_USER_ID, userId);
    query.put(QueryConstants.DataRequest.STATUS, DataRequestEnum.PENDING.getValue());
    Long count = MySqlOperations.count(tableName, query);
    return count;
  }

  public static DataRequest getObjectRequestedForGroupStatus(
      Long groupId, Long objectId, String objectType) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.DataRequest.GROUP_ID, groupId);
    query.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    query.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    DataRequest dataRequest = MySqlOperations.scanOneForQuery(DataRequest.class, query);
    return dataRequest;
  }

  public static DataRequest getObjectRequestedForUserStatus(
      Long userId, Long objectId, String objectType) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.DataRequest.REQUESTED_BY, userId);
    query.put(QueryConstants.ShareAccesstMapping.OBJECT_ID, objectId);
    query.put(QueryConstants.ShareAccesstMapping.OBJECT_TYPE, objectType);
    query.put(QueryConstants.DataRequest.GROUP_ID, -1L);
    DataRequest dataRequest = MySqlOperations.scanOneForQuery(DataRequest.class, query);
    return dataRequest;
  }

  /**
   * @param objectId
   * @param userId
   * @return
   * @throws SystemException
   */
  public static DatasourceApprovalMapper getDataSourceApprover(Long objectId, Long userId)
      throws SystemException {
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(QueryConstants.ShareAccesstMapping.OBJECT_ID, objectId);
    query.put(QueryConstants.ShareAccesstMapping.USER_ID, userId);
    DatasourceApprovalMapper datasourceApprovalMapper =
        MySqlOperations.scanOneForQuery(DatasourceApprovalMapper.class, query);
    return datasourceApprovalMapper;
  }

  /**
   * @param objectId
   * @param parentLevel
   * @return
   * @throws SystemException
   */
  public static DatasourceApprovalMapper getParentDataSourceApprover(
      Long objectId, Long parentLevel) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(QueryConstants.ShareAccesstMapping.OBJECT_ID, objectId);
    query.put(QueryConstants.DatasourceServiceApimanager.LEVEL, parentLevel);
    DatasourceApprovalMapper datasourceApprovalMapper =
        MySqlOperations.scanOneForQuery(DatasourceApprovalMapper.class, query);
    return datasourceApprovalMapper;
  }

  /**
   * @param tableName
   * @param userId
   * @return
   * @throws SystemException
   */
  public static Long getNotificationsOfUser(Class<?> tableName, Long userId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getNotificationsOfUser()"
            + ParamUtils.getString(userId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.Notification.USERID, userId);
    Long count = MySqlOperations.count(tableName, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getNotificationsOfUser()"
            + ParamUtils.getString(userId));
    return count;
  }

  /**
   * @param objectId
   * @return
   * @throws SystemException
   */
  public static List<DatasourceApprovalMapper> getDataSourceApproverListByObjectId(Long objectId)
      throws SystemException {
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(QueryConstants.ShareAccesstMapping.OBJECT_ID, objectId);
    List<DatasourceApprovalMapper> datasourceApprovalMapperList =
        MySqlOperations.scanForQuery(DatasourceApprovalMapper.class, query);
    return datasourceApprovalMapperList;
  }

  /**
   * @param datasourceApprovalMapper
   * @throws SystemException
   */
  public static void reFreshApproverDetails(DatasourceApprovalMapper datasourceApprovalMapper)
      throws SystemException {

    Map<String, Object> query = new LinkedHashMap<>();
    query.put(QueryConstants.DatasourceServiceApimanager.ID, datasourceApprovalMapper.getId());
    MySqlOperations.deleteRecords(datasourceApprovalMapper, query);
  }

  /**
   * @param datasourceApprovalMapper
   * @throws SystemException
   */
  public static void saveDataSourceApprover(DatasourceApprovalMapper datasourceApprovalMapper)
      throws SystemException {

    MySqlOperations.insert(datasourceApprovalMapper, QueryConstants.DataRequest.ID);
  }
}

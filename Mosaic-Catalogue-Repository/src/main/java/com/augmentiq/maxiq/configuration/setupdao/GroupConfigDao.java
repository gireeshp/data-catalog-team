package com.augmentiq.maxiq.configuration.setupdao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.MysqlConnection;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.setup.domains.GroupLevelUser;
import com.augmentiq.maxiq.entity.model.setup.domains.QueuesMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.UserGroup;
import com.augmentiq.maxiq.entity.model.setup.domains.UserSubGroups;
import com.augmentiq.maxiq.entity.model.usergroup.UserGroups;

public class GroupConfigDao {
	
	private static final Logger logger = LoggerFactory.getLogger(GroupConfigDao.class);

  // FETCHING AVAILABLE GROUPS-DATA
  public static List<Map<String, Object>> fetchGroups() throws SystemException {
    //List<UserGroup> userGroupsList = null;
    List<Map<String, Object>> executeQueryForResultSet = null;
    try {
      executeQueryForResultSet =
          MySqlOperations.executeQueryForResultSet(
              QueryConstants.GroupConfig.FETCH_AVAILABLE_GROUPS, new String[] {});
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return executeQueryForResultSet;
  }

  public static List<UserGroup> getUserGroupByIds(List<Object> groupIds)
      throws SQLException, SystemException {

    List<UserGroup> userGroupList = null;
    try {
      Map<String, List<Object>> query = new LinkedHashMap<String, List<Object>>();
      query.put(QueryConstants.Parameters.GROUP_ID, groupIds);
      userGroupList = MySqlOperations.getListPresentInArgumentList(UserGroup.class, query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return userGroupList;
  }

  // FETCHING AVAILABLE SUB-GROUPS-DATA
  public static List<Map<String, Object>> fetchSubGroups() throws SystemException {
    List<Map<String, Object>> executeQueryForResultSet = null;
    try {
      executeQueryForResultSet =
          MySqlOperations.executeQueryForResultSet(
              QueryConstants.GroupConfig.FETCH_AVAILABLE_SUBGROUPS, new String[] {});
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return executeQueryForResultSet;
  }

  // FETCHING AVAILABLE QUEUES
  public static List<QueuesMaster> fetchAvailableQueues() throws SystemException {
    List<QueuesMaster> userGroupsList = null;
    try {
      userGroupsList =
          MySqlOperations.scanWithSqlQuery(
              QueuesMaster.class, QueryConstants.GroupConfig.FETCH_AVAILABLE_QUEUES, null);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return userGroupsList;
  }

  // FETCHING AVAILABLE GROUPS
  public static List<Map<String, Object>> fetchAvailableGroups() throws SystemException {
    List<Map<String, Object>> userGroupsList = null;
    try {
      userGroupsList =
          MySqlOperations.executeQueryForResultSet(
              QueryConstants.GroupConfig.FETCH_AVAILABLE_GROUPS_LIST,
              new String[] {
                QueryConstants.Parameters.GROUP_ID,
                QueryConstants.Parameters.GROUP_NAME,
                QueryConstants.Parameters.APP_ID,
                QueryConstants.Parameters.DESCRIPTION
              });
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return userGroupsList;
  }

  // CREATE NEW GROUP
  public static Long createGroupAction(String createGroupQuery) throws Exception {
    Long generatedKey = 0L;
    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement(); ) {
      statement.executeUpdate(createGroupQuery);
      ResultSet rs = statement.getGeneratedKeys();
      if (rs.next()) {
        generatedKey = rs.getLong(1);
        //System.out.println("The primary key is:"+generatedKey);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return generatedKey;
  }

  // CREATE SUB-GROUP ACTION
  public static void createSubgroupAction(UserSubGroups subGroups) throws SystemException {
    try {
      MySqlOperations.insert(subGroups, "subGroupId");
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // CREATE SUB-GROUP ACTION
  public static void upadteSubgroupAction(UserSubGroups subGroups, Map<String, Object> query)
      throws SystemException {
    try {
      MySqlOperations.update(subGroups, query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // CREATE NEW GROUP
  public static void updateGroupAction(String updateGroupQuery) throws Exception {
    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement(); ) {
      statement.executeUpdate(updateGroupQuery);
    } catch (Exception e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // MAINTAIN GROUP HISTORY
  public static void maintainGroupHistory(String insertGroupHistory) throws Exception {
    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement(); ) {
      statement.executeUpdate(insertGroupHistory);
    } catch (Exception e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // DELETE GROUP
  public static void deleteGroup(UserGroup userGroup) throws Exception {
    try {
      MySqlOperations.deleteRow(userGroup);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // DELETE SUBGROUP
  public static void deleteSubgroup(UserSubGroups userSubGroups) throws Exception {
    try {
      MySqlOperations.deleteRow(userSubGroups);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  //CHECK IF GROUP EXISTS
  //mukut not in used
  /*public static List<Map<String, Object>> getQueryResult(String query) throws Exception {
  	    List<Map<String, Object>> executeQueryForResultSet=null;
  	    try {
  			 executeQueryForResultSet = MySqlOperations.executeQueryForResultSet(query, new String [] {});
  		} catch (SystemException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  			throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
  		}
  	    return executeQueryForResultSet;
  }*/

  //EXECUTE SQl STATEMENT
  public static void executeQuery(String sqlStmt, Map<String, Object> query) throws Exception {
    try {
      MySqlOperations.executeQuery(sqlStmt, query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  public static UserGroup getUserGroupByQueryParam(Map<String, Object> query) throws Exception {
    try {
      return MySqlOperations.scanOneForQuery(UserGroup.class, query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  public static void createGroupLevelUser(GroupLevelUser object) throws SystemException {

    try {
      MySqlOperations.insert(object);
    } catch (SystemException e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  /**
   * @param groupId
   * @return
   * @throws SystemException
   */
  public static GroupLevelUser fetchGroupLevelUserDetails(Object groupId) throws SystemException {

    try {
      Map<String, Object> query = new LinkedHashMap<>();
      query.put(QueryConstants.Parameters.GROUP_ID, groupId);
      return MySqlOperations.scanOneForQuery(GroupLevelUser.class, query);
    } catch (SystemException e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  /**
   * @param query
   * @return
   * @throws SystemException
   */
  public static List<GroupLevelUser> fetchGroupLevelUsersByQuery(Map<String, Object> query)
      throws SystemException {

    try {
      return MySqlOperations.scan(GroupLevelUser.class, query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }
  
  public static String getGroupName(Long groupId) throws SystemException {
	    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getGroupName()" + ParamUtils.getString(groupId));
	    UserGroups groups = getGroupNameById(groupId);
	    String groupName = "";
	    if (null != groups) groupName = groups.getGroupName();
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB + " << getGroupName()" + ParamUtils.getString(groupName));
	    return groupName;
	  }
  
  public static UserGroups getGroupNameById(Long groupId) throws SystemException {
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB + " >> getGroupName()" + ParamUtils.getString(groupId));

	    Map<String, Object> query = new LinkedHashMap<String, Object>();
	    query.put("groupId", groupId);

	    UserGroups groups = MySqlOperations.scanOneForQuery(UserGroups.class, query);
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB + " << getGroupName()" + ParamUtils.getString(groups));
	    return groups;
	  }
}

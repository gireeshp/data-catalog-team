package com.augmentiq.maxiq.core.dao.configuration.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;

import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;

public class RelationsDao {

	public static List<Map<String, Object>> executeQueryForResultSet(String querym, String[] params)
			throws FileNotFoundException, IOException, ParseException {

		// select dataSourceId, dataSourceName from datasourceinstance where
		// dataSourceId in (42790,42920);
		List<Map<String, Object>> executeQueryForResultSet = null;

		try {
			executeQueryForResultSet = MySqlOperations.executeQueryForResultSet(querym, params);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return executeQueryForResultSet;
	}

	public static void executeQuery(String querym, Map<String, Object> query)
			throws FileNotFoundException, IOException, ParseException {

		try {
			// MySqlOperations.executeQuery(querym,query);
			MySqlOperations.executeQuery(querym, query);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

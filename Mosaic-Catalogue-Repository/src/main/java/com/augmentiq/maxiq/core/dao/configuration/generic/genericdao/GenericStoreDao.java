package com.augmentiq.maxiq.core.dao.configuration.generic.genericdao;

import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.base.encryption.decription.userinfo.SecretService;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.ConfigurationDataType;
import com.augmentiq.maxiq.constant.configuration.enums.DataStatus;
import com.augmentiq.maxiq.core.dao.configuration.setupdao.UserManagementDao;
import com.augmentiq.maxiq.core.dao.userdao.ApplicationUserDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.CustomComponentInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.SocialAppInstance;
import com.augmentiq.maxiq.entity.model.configuration.generic.GenericObjectBuild;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class GenericStoreDao {
  /*
	 * private static final Logger logger =
	 * LoggerFactory.getLogger(GenericStoreDao.class);
	 */ public static final String CONFIGURATION_COL = "configuration";

  public static void insertUpdateUserProfile(ApplicationUser userProfile) throws SystemException {
    MySqlOperations.insert(
        userProfile, HbaseUtility.getIdAnnotationForClass(ApplicationUser.class));
  }

  public static Long insertNewConfiguration(GenericObjectBuild build) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >>
    // insertNewConfiguration()" + ParamUtils.getString(build));

    if (null != build) {

      if (checkIfAlreadyExists(build.getName(), build.getId())
          && !(build
              .getType()
              .toString()
              .equalsIgnoreCase(ConfigurationDataType.CONNECTION.toString()))) {
        build.setUpdatetime(new Date().getTime());
      } else {
        build.setAddedtime(new Date().getTime());
      }

      MySqlOperations.insert(build);

      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : <<
      // insertNewConfiguration()" + ParamUtils.getString(build.getId()));
      return build.getId();
    }

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : <<
    // insertNewConfiguration()" + ParamUtils.getString(0L));
    return 0L;
  }

  public static Long insertNewConfigurationWithAutoIncremented(GenericObjectBuild build)
      throws SystemException {

    if (null != build) {

      if (checkIfAlreadyExists(build.getName(), build.getId())
          && !(build
              .getType()
              .toString()
              .equalsIgnoreCase(ConfigurationDataType.CONNECTION.toString()))) {
        build.setUpdatetime(System.currentTimeMillis());
      } else {
        build.setAddedtime(System.currentTimeMillis());
      }

      Long id =
          MySqlOperations.insert(
              build, HbaseUtility.getIdAnnotationForClass(GenericObjectBuild.class));

      return id;
    }
    return 0L;
  }

  public static void deleteConfiguration(Long configurationId) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> deleteConfiguration()"
    // + ParamUtils.getString(configurationId));

    if (null != configurationId) {
      GenericObjectBuild build = new GenericObjectBuild();
      build.setId(configurationId);
      MySqlOperations.deleteRow(build);
    }

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << deleteConfiguration()"
    // + ParamUtils.getString(configurationId));
  }

  public static void deleteConfigurationFromDataSourceName(String dataSourceName)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> deleteConfiguration()" + ParamUtils.getString(configurationId));

    String sql = "delete from " + Sequences.CONFIGURATION + " where name= ?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("name", dataSourceName);
    MySqlOperations.executeQuery(sql, query);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << deleteConfiguration()" + ParamUtils.getString(configurationId));
  }

  public static void deleteDataSourceInstanceFromDataSourceName(String dataSourceName)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> deleteConfiguration()" + ParamUtils.getString(configurationId));

    String sql = "delete from " + Sequences.DATASOURCE_INSTANCE + " where dataSourceName= ?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("dataSourceName", dataSourceName);
    MySqlOperations.executeQuery(sql, query);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << deleteConfiguration()" + ParamUtils.getString(configurationId));
  }

  public static Boolean insertNewUserProfile(ApplicationUser user) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // insertNewUserProfile()" + ParamUtils.getString(user));
    if (null != user) {

      if (checkIfAlreadyExists(user.getEmailId())) {
        // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
        // insertNewUserProfile()" + ParamUtils.getString(true));
        return true;
      }
      MySqlOperations.insert(user, "unqUserId"); // insert info into
      // database if user is
      // not already exist.
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // insertNewUserProfile()" + ParamUtils.getString(false));
      return false;
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // insertNewUserProfile()" + ParamUtils.getString(true));
    return true;
  }

  private static boolean checkIfAlreadyExists(String email) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> checkIfAlreadyExists()" + ParamUtils.getString(email));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("emailId", email);

    ApplicationUser user = MySqlOperations.scanOneForQuery(ApplicationUser.class, query);

    if (null != user) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // checkIfAlreadyExists()" + ParamUtils.getString(true));
      return true;
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // checkIfAlreadyExists()" + ParamUtils.getString(false));
    return false;
  }

  public static boolean checkIfAlreadyExists(String paramName, Long id) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> : checkIfAlreadyExists()" + ParamUtils.getString(paramName, id));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    if (id != null) query.put("id", id);

    query.put("name", paramName);

    GenericObjectBuild builds = MySqlOperations.scanOneForQuery(GenericObjectBuild.class, query);

    if (null != builds) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : <<
      // checkIfAlreadyExists()" + ParamUtils.getString(true));
      return true;
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : <<
    // checkIfAlreadyExists()" + ParamUtils.getString(false));
    return false;
  }

  public static Object getConfiguration(String paramName) throws SystemException {

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> getConfiguration()" + ParamUtils.getString(paramName));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("name", paramName);

    GenericObjectBuild builds = MySqlOperations.scanOneForQuery(GenericObjectBuild.class, query);

    if (null != builds) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : <<
      // getConfiguration()" + ParamUtils.getString(builds.getJson()));
      return builds.getJson();
    }
    return null;
  }

  public static Boolean isDataSourceExist(String paramName) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> getConfigurationBasedOnType()" + ParamUtils.getString(paramName));

    Map<String, Object> query = new LinkedHashMap<String, Object>();

    query.put("name", paramName);

    GenericObjectBuild builds = MySqlOperations.scanOneForQuery(GenericObjectBuild.class, query);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : <<
    // getConfigurationBasedOnType()" + ParamUtils.getString(paramName));
    if (null != builds) return true;
    return false;
  }

  public static GenericObjectBuild getConfigurationObject(String paramName) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> getConfigurationObject()" + ParamUtils.getString(paramName));
    Map<String, Object> map = new LinkedHashMap<String, Object>();
    map.put("name", paramName);

    GenericObjectBuild builds = MySqlOperations.scanOneForQuery(GenericObjectBuild.class, map);
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : <<
    // getConfigurationObject()" + ParamUtils.getString(builds));
    return builds;
  }

  public static ApplicationUser getUserProfile(String email) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getUserProfile()" +
    // ParamUtils.getString(email));
    if (StringUtils.isNotBlank(email)) {
      Map<String, Object> query = new LinkedHashMap<String, Object>();
      query.put(Sequences.EMAILID, email);

      ApplicationUser userProfile = MySqlOperations.scanOneForQuery(ApplicationUser.class, query);
      if (null != userProfile) {
        // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
        // getUserProfile()" + ParamUtils.getString(userProfile));
        return userProfile;
      }
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getUserProfile()" +
    // ParamUtils.getString(email));
    return null;
  }

  public static ApplicationUser getUserProfile(String email, Long appId) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getUserProfile()" +
    // ParamUtils.getString(email));
    if (StringUtils.isNotBlank(email) && appId != null) {
      String query =
          "select A.* from application_user A left outer join userApplicationMapping B"
              + " on (A.unqUserId = B.userId)"
              + " where A.emailId = ? and B.appId = ?";
      Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
      objectMap.put("A.emailId", email);
      objectMap.put("B.appId", appId);
      List<ApplicationUser> userProfile =
          MySqlOperations.scanWithSqlQuery(ApplicationUser.class, query, objectMap);
      if (null != userProfile && userProfile.size() > 0) {

        return userProfile.get(0);
      }
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getUserProfile()" +
    // ParamUtils.getString(email));
    return null;
  }

  public static ApplicationUser getUserProfile(Long createdBy) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getUserProfile()" +
    // ParamUtils.getString(createdBy));
    if (!(null == createdBy)) {
      Map<String, Object> query = new LinkedHashMap<String, Object>();
      query.put("createdBy", createdBy);

      ApplicationUser userProfile = MySqlOperations.scanOneForQuery(ApplicationUser.class, query);
      if (null != userProfile) {
        // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
        // getUserProfile()" + ParamUtils.getString(userProfile));
        return userProfile;
      }
    }
    return null;
  }

  public static String getConfiguration(Long id) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getConfiguration()" +
    // ParamUtils.getString(id));

    GenericObjectBuild builds = getGenericStoreobject(id);

    if (null != builds) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // getConfiguration()" + ParamUtils.getString(id));
      return builds.getJson();
    }

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getConfiguration()" +
    // ParamUtils.getString(id));
    return null;
  }

  public static GenericObjectBuild getGenericStoreobject(Long id) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("id", id);

    GenericObjectBuild builds = MySqlOperations.scanOneForQuery(GenericObjectBuild.class, query);
    return builds;
  }

  public static String getConfigurationByName(String name) throws SystemException {

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // getConfigurationByName()" + ParamUtils.getString(name));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("name", name);

    GenericObjectBuild builds = MySqlOperations.scanOneForQuery(GenericObjectBuild.class, query);

    if (null != builds) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // getConfigurationByName()" + ParamUtils.getString(name));
      return builds.getJson();
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // getConfigurationByName()" + ParamUtils.getString(name));

    return null;
  }

  public static Long insertNewConfiguration(String name, String value) throws SystemException {

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // insertNewConfiguration()" + ParamUtils.getString(name,value));
    GenericObjectBuild build = getConfigurationObject(name);

    if (null != build) {
      build.setUpdatetime(new Date().getTime());
    } else {
      build = new GenericObjectBuild();
      //			build.setId(Sequences.getNextId(Sequences.CONFIGURATION));
      build.setAddedtime(System.currentTimeMillis());
      build.setName(name);
      build.setType(ConfigurationDataType.OTHER_CONFIGURATIONS);
    }
    build.setJson(value);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // insertNewConfiguration()" +
    // ParamUtils.getString(insertNewConfiguration(build)));
    return insertNewConfiguration(build);
  }

  // begining of custom component

  public static Boolean isCustomComponentPresentBasedOnName(String paramName)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // isCustomComponentPresentBasedOnName()" +
    // ParamUtils.getString(paramName));

    Map<String, Object> query = new LinkedHashMap<String, Object>();

    query.put("componentName", paramName);

    CustomComponentInstance componentInstance =
        MySqlOperations.scanOneForQuery(CustomComponentInstance.class, query);

    if (null != componentInstance) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // isCustomComponentPresentBasedOnName()" +
      // ParamUtils.getString(true));
      return true;
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // isCustomComponentPresentBasedOnName()" +
    // ParamUtils.getString(false));
    return false;
  }

  public static CustomComponentInstance getCustomComponentBasedOnId(Long paramName)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // getCustomComponentBasedOnId()" + ParamUtils.getString(paramName));

    Map<String, Object> query = new LinkedHashMap<String, Object>();

    query.put("componentId", paramName);

    CustomComponentInstance componentInstance =
        MySqlOperations.scanOneForQuery(CustomComponentInstance.class, query);

    if (null != componentInstance) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // getCustomComponentBasedOnId()" +
      // ParamUtils.getString(componentInstance));
      return componentInstance;
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // getCustomComponentBasedOnId()" + ParamUtils.getString(paramName));
    return null;
  }

  public static CustomComponentInstance getCustomComponentBasedOnName(String paramName)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // getCustomComponentBasedOnName()" + ParamUtils.getString(paramName));

    Map<String, Object> query = new LinkedHashMap<String, Object>();

    query.put("componentName", paramName);

    CustomComponentInstance componentInstance =
        MySqlOperations.scanOneForQuery(CustomComponentInstance.class, query);

    if (null != componentInstance) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // getCustomComponentBasedOnName()" +
      // ParamUtils.getString(componentInstance));
      return componentInstance;
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // getCustomComponentBasedOnName()" + ParamUtils.getString(paramName));
    return null;
  }

  private static boolean checkIfComponentAlreadyExists(String paramName, Long id)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // checkIfComponentAlreadyExists()" +
    // ParamUtils.getString(paramName,id));
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("componentId", id);
    query.put("componentName", paramName);

    CustomComponentInstance componentInstance =
        MySqlOperations.scanOneForQuery(CustomComponentInstance.class, query);

    if (null != componentInstance) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // checkIfComponentAlreadyExists()" + ParamUtils.getString(true));
      return true;
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // checkIfComponentAlreadyExists()" + ParamUtils.getString(false));
    return false;
  }

  public static String insertNewCustomComponent(CustomComponentInstance customComponentInstance)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // insertNewCustomComponent()" +
    // ParamUtils.getString(customComponentInstance));
    if (null != customComponentInstance) {
      String componentStatusMsg = "";
      if (getCustomComponentBasedOnId(customComponentInstance.getComponentId()) != null) {
        CustomComponentInstance initialCustomComponentInstance =
            getCustomComponentBasedOnId(customComponentInstance.getComponentId());
        customComponentInstance.setOwner(initialCustomComponentInstance.getOwner());
        customComponentInstance.setComponentUpdatedDate(new Date().getTime());
        customComponentInstance.setComponentStatus(DataStatus.UPDATED);
        componentStatusMsg = "Custom Component is updated successfully";
        MySqlOperations.insert(customComponentInstance);
      } else {
        customComponentInstance.setComponentCreatedDate(new Date().getTime());
        customComponentInstance.setComponentStatus(DataStatus.CREATED);
        componentStatusMsg = "Custom Component is created successfully";
        Long componentId =
            MySqlOperations.insert(
                customComponentInstance,
                HbaseUtility.getIdAnnotationForClass(CustomComponentInstance.class));
        customComponentInstance.setComponentId(componentId);
      }

      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // insertNewCustomComponent()" +
      // ParamUtils.getString(componentStatusMsg));
      return componentStatusMsg;
    }

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // insertNewCustomComponent()" +
    // ParamUtils.getString(customComponentInstance));
    return null;
  }

  public static SocialAppInstance getSocialAppAlreadyExistsBasedOnId(Long id)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // getSocialAppAlreadyExistsBasedOnId()" + ParamUtils.getString(id));
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("id", id);
    SocialAppInstance socialAppInstance =
        MySqlOperations.scanOneForQuery(SocialAppInstance.class, query);

    if (null != socialAppInstance) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // getSocialAppAlreadyExistsBasedOnId()" +
      // ParamUtils.getString(socialAppInstance));
      return socialAppInstance;
    }
    return null;
  }

  public static SocialAppInstance getSocialAppBasedOnName(String paramName) throws SystemException {

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // getSocialAppBasedOnName()" + ParamUtils.getString(paramName));
    Map<String, Object> query = new LinkedHashMap<String, Object>();

    query.put("name", paramName);

    SocialAppInstance socialAppInstance =
        MySqlOperations.scanOneForQuery(SocialAppInstance.class, query);

    if (null != socialAppInstance) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
      // getSocialAppBasedOnName()" +
      // ParamUtils.getString(socialAppInstance));
      return socialAppInstance;
    }
    return null;
  }

  public static void insertSocialAppInstance(SocialAppInstance socialAppInstance)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >>
    // insertSocialAppInstance()" +
    // ParamUtils.getString(socialAppInstance));
    if (null != socialAppInstance) {
      if (null == getSocialAppAlreadyExistsBasedOnId(socialAppInstance.getId())) {
        socialAppInstance.setCreatedDate(new Date().getTime());
        socialAppInstance.setStatus(DataStatus.CREATED);
      } else {
        socialAppInstance.setUpdatedDate(new Date().getTime());
        socialAppInstance.setStatus(DataStatus.UPDATED);
      }
      MySqlOperations.insert(socialAppInstance);
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" <<
    // insertSocialAppInstance()" +
    // ParamUtils.getString(socialAppInstance));
  }

  public static GenericObjectBuild getAllConfiguration(Long id) throws SystemException {
    GenericObjectBuild builds = getGenericStoreobject(id);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getAllConfiguration()"
    // + ParamUtils.getString(builds));
    return builds;
  }

  public static void resetPasswordUser(String emailId, Map<String, String> detailsOfPassword)
      throws Exception {
    // TODO reset password for user method
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(Sequences.EMAILID, emailId);
    ApplicationUser userDetails = MySqlOperations.scanOneForQuery(ApplicationUser.class, query);

    String password = detailsOfPassword.get(Sequences.NEWPASSWORD);

    String newPassword = checkPasswordIsFromLastThreePassword(emailId, detailsOfPassword);

    if (null == userDetails) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_142");
    } else {
      emailId = userDetails.getEmailId();
    }

    UserManagementDao.maintainUserHistory(userDetails.getUnqUserId() + "", emailId, "UPDATE");
    encodeAndDecodePasswordOperation(detailsOfPassword, userDetails);
    Long longDate = UserManagementDao.getLongDate(userDetails.getCreatedTs());
    Long todaysDate = System.currentTimeMillis();
    Long diff = todaysDate - longDate;
    String passwordExpiryDays = Cache.getProperty(CacheConstants.PASS_EXP_DAYS);
    userDetails.setPasswordexpirydays(
        TimeUnit.MILLISECONDS.toDays(diff)
            + userDetails.getPasswordexpirydays()
            + Long.parseLong(passwordExpiryDays));
    if (null == userDetails.getUpdatedTs()
        || StringUtils.equalsIgnoreCase(userDetails.getUpdatedTs(), "null")) {
      userDetails.setUpdatedTs("0000-00-00 00:00:00");
    }
    if (null == userDetails.getLastFailureAttemptTs()
        || StringUtils.equalsIgnoreCase(userDetails.getLastFailureAttemptTs(), "null")) {
      userDetails.setLastFailureAttemptTs("0000-00-00 00:00:00");
    }
    userDetails.setPasswordRefreshedDate(String.valueOf(DateUtils.getCurrentTimestamp()));
    userDetails.setPassword(newPassword);
    MySqlOperations.insert(userDetails);

    userDetails.setPassword(password);
    
  }

  public static String checkPasswordIsFromLastThreePassword(
      String emailId, Map<String, String> detailsOfPassword) throws Exception {
    // TODO Check password which are already register on not
    SecretService secretService = new SecretService();
    String newPassword = GenericStoreDao.encodedPassword(detailsOfPassword.get(Sequences.NEWPASSWORD));
    List<Map<String, Object>> passwordHistoryForUserInDb =
        ApplicationUserDao.getPasswordHistoryForUserInDb(emailId);
    for (Map<String, Object> map : passwordHistoryForUserInDb) {
      if (newPassword.equals(map.get(Sequences.HIS_PASSWORD))) {
        ExceptionsMessanger.throwException(new SystemException(), "ERR_143");
      }
    }
    return newPassword;
  }

  private static void encodeAndDecodePasswordOperation(
      Map<String, String> detailsOfPassword, ApplicationUser userDetails) throws Exception {
    // TODO Encode the password to compared in current password
    String currentPassword = userDetails.getPassword();
    String oldPassword = encodedPassword(detailsOfPassword.get(Sequences.OLDPASSWORD));
    if (!currentPassword.equalsIgnoreCase(oldPassword)) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_141");
    }
  }

  public static String encodedPassword(String userPassword) throws Exception {
    SecretService secretService = new SecretService();
    String newPassword = null;
    if (Cache.getProperty(CacheConstants.USE_PASSWORD_HASHING)
        .equalsIgnoreCase(Boolean.TRUE.toString())) {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
      byte[] hashCodedPass =
          messageDigest.digest(secretService.encrypt(userPassword).getBytes("UTF-8"));
      newPassword = org.apache.hadoop.util.StringUtils.byteToHexString(hashCodedPass);
    } else newPassword = secretService.encrypt(userPassword);
    return newPassword;
  }

  public static String updateConfiguration(GenericObjectBuild build) throws SystemException {
    String returnValue = GENERAL_CONSTANTS.VALIDATE_MSG_SUCCESS;
    if (null != build) {
      try {
        HashMap<String, Object> queryMap = new LinkedHashMap<String, Object>();
        queryMap.put(GENERAL_CONSTANTS.QUERY_PARAM_NAME, "'" + build.getName() + "'");
        MySqlOperations.update(build, queryMap);
      } catch (Exception e) {
        returnValue = e.getMessage();
      }
    }

    return returnValue;
  }
 
}

package com.augmentiq.maxiq.core.dao.configuration.setupdao;

import java.sql.Connection;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.MysqlConnection;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.setup.domains.ActionsMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.RoleActionMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.UserRoleMaster;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class AccessConfigDao {

  // FETCHING AVAILABLE ACCESS DATA
  public static List<Map<String, Object>> fetchAvailableAccessData() throws SystemException {
    List<Map<String, Object>> executeQueryForResultSet = null;
    try {
      executeQueryForResultSet =
          MySqlOperations.executeQueryForResultSet(
              QueryConstants.AccessConfig.FETCH_AVAILABLE_ACCESS_DATA, new String[] {});
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return executeQueryForResultSet;
  }

  // FETCHING AVAILABLE ACTIONS
  public static List<ActionsMaster> fetchAvailableActions() throws SystemException {
    List<ActionsMaster> scanWithSqlQuery = null;
    try {
      scanWithSqlQuery =
          MySqlOperations.scanWithSqlQuery(
              ActionsMaster.class, QueryConstants.AccessConfig.FETCH_AVAILABLE_ACTIONS, null);
    } catch (SystemException e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return scanWithSqlQuery;
  }

  //FETCHING APPLICATION LIST
  public static List<Map<String, Object>> fetchAllApps() throws SystemException {
    List<Map<String, Object>> scanWithSqlQuery = null;
    try {
      String query = "Select * from masterListOfApplication";
      String params[] = null;
      scanWithSqlQuery = MySqlOperations.executeQueryForResultSet(query, params);
    } catch (SystemException e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return scanWithSqlQuery;
  }

  // SAVE ROLE OBJECT
  /**
   * This method is to saved role object. MAX-1848 : In this method we generate id by using Hbase
   * utility, so we just add this method with class.
   *
   * @author Mayuri
   * @param roleObject
   * @return
   * @throws SystemException
   */
  public static String saveRoleObject(UserRoleMaster roleObject) throws SystemException {
    String roleMasterId = null;
    try {
      roleMasterId =
          String.valueOf(
              MySqlOperations.insert(
                  roleObject, HbaseUtility.getIdAnnotationForClass(UserRoleMaster.class)));
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
    return roleMasterId;
  }

  // SAVE ROLE-ACTION OBJECT
  public static void saveRoleActionObject(List<RoleActionMappings> roleActionMappingsObject)
      throws SystemException {

    for (RoleActionMappings roleActionMappings : roleActionMappingsObject) {
      try {
        MySqlOperations.insert(roleActionMappings, "roleActionId");
      } catch (SystemException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
      }
    }
  }

  // SAVE ROLE-ACTION OBJECT
  public static void updateRoleObject(UserRoleMaster userRoleMaster, Map<String, Object> query)
      throws SystemException {
    try {

      Map<String, Object> appQuery = new LinkedHashMap<String, Object>();
      appQuery.put(
          "firstName", StringUtils.splitPreserveAllTokens(userRoleMaster.getCreatedBy(), " ")[0]);
      appQuery.put(
          "lastName", StringUtils.splitPreserveAllTokens(userRoleMaster.getCreatedBy(), " ")[1]);

      ApplicationUser applicationUser =
          MySqlOperations.scanOneForQuery(ApplicationUser.class, appQuery);
      userRoleMaster.setCreatedBy(applicationUser.getUnqUserId() + "");
      MySqlOperations.update(userRoleMaster, query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // SAVE ROLE-ACTION OBJECT
  public static void deleteRoleActionObject(Map<String, Object> query) throws SystemException {
    try {
      MySqlOperations.deleteRecords(new RoleActionMappings(), query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // SAVE ROLE-ACTION OBJECT
  public static void deleteAccessDetails(Object object, Map<String, Object> query)
      throws SystemException {
    try {
      MySqlOperations.deleteRecords(object, query);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  // SAVE ROLE-ACTION OBJECT
  //mukut not accessed
  /*public static List<? extends Object> getQueryResult(String query) throws SystemException{
  	    try {
  	    	//System.out.println("the query is:"+query);
  			return MySqlOperations.executeQueryForResultSet(query, new String[]{});
  		} catch (SystemException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  			throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
  		}
  }
  */
  // MAINTAIN ROLE HISTORY
  public static void maintainRoleHistory(String insertGroupHistory) throws SystemException {
    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement(); ) {
      statement.executeUpdate(insertGroupHistory);
    } catch (Exception e) {
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }
}

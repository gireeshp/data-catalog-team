package com.augmentiq.maxiq.core.dao.configuration.setupdao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.configuration.enums.UserStatus;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserClientMapping;
import com.augmentiq.maxiq.entity.model.setup.domains.UserGroup;
import com.augmentiq.maxiq.entity.model.setup.domains.UserGroupMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.UserRoleMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.UserRoleMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.UserSubGroupMappings;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class UserManagementDao {

  private static final Logger logger = LoggerFactory.getLogger(UserManagementDao.class);

  public static List<Map<String, Object>> getUserDetails() throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserDetails()" + ParamUtils.getString(""));

    List<Map<String, Object>> executeQueryForResultSet = null;
    try {
      executeQueryForResultSet =
          MySqlOperations.executeQueryForResultSet(
              QueryConstants.UserManageMent.FETCH_AVAILABLE_USERS, new String[] {});
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " >> getUserDetails()" + ParamUtils.getString(""));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getUserDetails()"
            + ParamUtils.getString(executeQueryForResultSet));
    return executeQueryForResultSet;
  }

  public static List<Map<String, Object>> getConcatinatedSubroupList() throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getConcatinatedSubroupList()"
            + ParamUtils.getString(""));

    List<Map<String, Object>> executeQueryForResultSet = null;
    try {
      executeQueryForResultSet =
          MySqlOperations.executeQueryForResultSet(
              QueryConstants.UserManageMent.FETCH_GROUP_SUBGROUP_CONCAT, new String[] {});
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " >> getConcatinatedSubroupList()"
              + ParamUtils.getString(""));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getConcatinatedSubroupList()"
            + ParamUtils.getString(executeQueryForResultSet));
    return executeQueryForResultSet;
  }

  public static void deleteRow(Object object) throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteRow()" + ParamUtils.getString(object));

    try {
      MySqlOperations.deleteRow(object);
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteRow()" + ParamUtils.getString(object));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  public static void deleteRows(Object object, Map<String, Object> query) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> deleteRows()" + ParamUtils.getString(object, query));

    try {
      MySqlOperations.deleteRecords(object, query);
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " >> deleteRows()" + ParamUtils.getString(object, query));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  public static void update(Object object, Map<String, Object> query) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> update()" + ParamUtils.getString(object, query));

    try {
      MySqlOperations.update(object, query);
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " >> update()" + ParamUtils.getString(object, query));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  public static List<Map<String, Object>> executeQueryForResultSet(String query)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> executeQueryForResultSet()"
            + ParamUtils.getString(query));

    List<Map<String, Object>> executeQueryForResultSet = null;
    try {
      executeQueryForResultSet = MySqlOperations.executeQueryForResultSet(query, new String[] {});
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " >> executeQueryForResultSet()"
              + ParamUtils.getString(query));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << executeQueryForResultSet()"
            + ParamUtils.getString(executeQueryForResultSet));
    return executeQueryForResultSet;
  }

  public static void executeQuery(String sqlStmt, Map<String, Object> query)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> executeQuery()" + ParamUtils.getString(sqlStmt));

    try {
      MySqlOperations.executeQuery(sqlStmt, query);
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " >> executeQuery()" + ParamUtils.getString(sqlStmt));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  public static Long createUser(ApplicationUser applicationUser, String primaryKey)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createUser()"
            + ParamUtils.getString(applicationUser, primaryKey));

    Long userId = 0L;
    try {
      userId = MySqlOperations.insert(applicationUser, primaryKey);
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " >> createUser()"
              + ParamUtils.getString(applicationUser, primaryKey));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << createUser()" + ParamUtils.getString(userId));
    return userId;
  }

  /**
   * This method will allocate a newly created user to a client. This client will be same as the
   * client assigned to the logged in user who created the new user.
   *
   * @param newUserId
   * @param createdByUserId
   * @throws SystemException
   */
  public static void allocateUserToClient(Long newUserId, String createdByUserId)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> allocateUserToClient()"
            + ParamUtils.getString(newUserId, createdByUserId));

    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put(QueryConstants.Parameters.NEW_USER_ID, newUserId.toString());
    params.put(QueryConstants.Parameters.CREATED_BY_USER_ID, createdByUserId);

    try {
      MySqlOperations.executeQuery(QueryConstants.UserManageMent.ALLOCATE_USER_TO_CLIENT, params);
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " >> allocateUserToClient()"
              + ParamUtils.getString(newUserId, createdByUserId));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << allocateUserToClient()"
            + ParamUtils.getString(newUserId, createdByUserId));
  }

  public static void assignClientIdToUserIfNotAssigned(Long userId, Long clientId) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> checkIfClientIdIsAssignedToUser()"
            + ParamUtils.getString(userId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("unqUserId", userId);
    try {
      UserClientMapping userClientMapping =
          MySqlOperations.scanOneForQuery(UserClientMapping.class, query);
      if (null == userClientMapping) {
        UserClientMapping newUserClientMapping = new UserClientMapping();
        newUserClientMapping.setClientId(clientId);
        newUserClientMapping.setUnqUserId(userId);

        MySqlOperations.insert(newUserClientMapping);
      }
    } catch (SystemException e) {
      logger.error(
          LoggerConstants.LOG_MAXIQWEB + " << checkIfClientIdIsAssignedToUser()" + e.getMessage());
      e.printStackTrace();
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << checkIfClientIdIsAssignedToUser()"
            + ParamUtils.getString(userId));
  }

  public static void createUserGroupMappings(
      List<UserGroupMappings> userGroupMappingsList, String primaryKey) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createUserGroupMappings()"
            + ParamUtils.getString(userGroupMappingsList, primaryKey));

    try {
      for (UserGroupMappings userGroupMappings : userGroupMappingsList) {
        MySqlOperations.insert(userGroupMappings, primaryKey);
      }
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " >> createUserGroupMappings()"
              + ParamUtils.getString(userGroupMappingsList, primaryKey));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  public static void createUserSubGroupMappings(
      List<UserSubGroupMappings> userSubGroupMappingsList, String primaryKey)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createUserSubGroupMappings()"
            + ParamUtils.getString(userSubGroupMappingsList, primaryKey));

    try {
      for (UserSubGroupMappings userSubGroupMappings : userSubGroupMappingsList) {
        MySqlOperations.insert(userSubGroupMappings, primaryKey);
      }
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " >> createUserSubGroupMappings()"
              + ParamUtils.getString(userSubGroupMappingsList, primaryKey));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  public static void createUserRoleMappings(
      List<UserRoleMappings> userRoleMappingsList, String primaryKey) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createUserRoleMappings()"
            + ParamUtils.getString(userRoleMappingsList, primaryKey));

    try {
      for (UserRoleMappings userRoleMappings : userRoleMappingsList) {
        MySqlOperations.insert(userRoleMappings, primaryKey);
      }
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " >> createUserRoleMappings()"
              + ParamUtils.getString(userRoleMappingsList, primaryKey));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  public static void createUserSolutionMapping(Long userId, Long appId) throws SystemException {

    String sql = "INSERT INTO userApplicationMapping (userId, appId) VALUES (?,?)";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("userId", userId);
    query.put("appId", appId);
    MySqlOperations.executeQuery(sql, query);
  }

  public static void deleteUserSolutionMappings(Long unqUserId) throws SystemException {
    String sql = "DELETE FROM userApplicationMapping where userId= ?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("userId", unqUserId);
    MySqlOperations.executeQuery(sql, query);
  }

  public static List<Map<String, Object>> getAllSolutions() throws SystemException {
    List<Map<String, Object>> scanWithSqlQuery = null;
    String query = "Select appId,appName from masterListOfApplication";
    String params[] = null;
    try {
      scanWithSqlQuery = MySqlOperations.executeQueryForResultSet(query, params);
    } catch (SystemException e) {
      e.printStackTrace();
    }
    return scanWithSqlQuery;
  }

  public static List<Map<String, Object>> getSelectedSolutions(String unqUserId) {
    List<Map<String, Object>> scanWithSqlQuery = null;
    String query =
        "select distinct mloa.appName solutionName,mloa.appId solutionId from masterListOfApplication mloa "
            + "left outer join userApplicationMapping uam on (uam.appId=mloa.appId) where uam.userId = "
            + unqUserId;
    String params[] = null;
    try {
      scanWithSqlQuery = MySqlOperations.executeQueryForResultSet(query, params);
    } catch (SystemException e) {
      e.printStackTrace();
    }
    return scanWithSqlQuery;
  }

  public static void createUserPersonaMapping(Long userId, Long personaId) throws SystemException {

    String sql = "INSERT INTO user_persona VALUES (?, ?)";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.UserPersona.USER_ID, userId);
    query.put(QueryConstants.UserPersona.PERSONA_ID, personaId);

    MySqlOperations.executeQuery(sql, query);
  }

  public static void deleteUserPersonaMappings(Long unqUserId) throws SystemException {

    String sql = "DELETE FROM user_persona where unqUserId= ?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("unqUserId", unqUserId);
    MySqlOperations.executeQuery(sql, query);
  }

  public static List<Map<String, Object>> getUserFirstAndLastNameById(Long unqUserId) {
    List<Map<String, Object>> scanWithSqlQuery = null;
    String query = QueryConstants.UserManageMent.FETCH_FNAME_LNAME_OF_USER + unqUserId;
    String params[] = null;
    try {
      scanWithSqlQuery = MySqlOperations.executeQueryForResultSet(query, params);
    } catch (SystemException e) {
      e.printStackTrace();
    }
    return scanWithSqlQuery;
  }

  /**
   * This methods queries the database for the details of the user having {@code userId}
   *
   * @author 10642747
   * @param userId for which the details to be fetched
   * @return
   */
  public static ApplicationUser fetchUserDetails(Long userId) {
    try {
      Map<String, Object> query = new LinkedHashMap<>();
      query.put("unqUserId", userId);
      return MySqlOperations.scanOneForQuery(ApplicationUser.class, query);
    } catch (SystemException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static UserGroup getGroupDetailsByGroupId(String groupId) throws Exception {
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(QueryConstants.Parameters.GROUP_ID, groupId);
    UserGroup usergroup = GroupConfigDao.getUserGroupByQueryParam(query);
    return usergroup;
  }

  public static List<Long> getAllUsersbyGroup(Long groupId) throws SystemException {
    String query = "select userId FROM user_group_mappings where groupId='" + groupId + "'";
    List<Map<String, Object>> executeQueryForResultSet =
        MySqlOperations.executeQueryForResultSet(query, null);
    List<Long> userIds = new LinkedList<Long>();
    for (Map<String, Object> map : executeQueryForResultSet) {
      userIds.add(Long.valueOf((String) map.get("userId")));
    }
    return userIds;
  }

  /**
   * This method sets the status of user as deleted
   *
   * @param applicationUser
   * @throws SystemException
   */
  public static void softDeleteUser(ApplicationUser applicationUser) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> update()" + ParamUtils.getString(applicationUser));

    try {
      String query = "update application_user set status = ? where unqUserId = ?";
      Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
      objectMap.put(Constants.STATUS, UserStatus.DELETED.name());
      objectMap.put(Constants.UNQ_USER_ID, applicationUser.getUnqUserId());
      MySqlOperations.executeQuery(query, objectMap);
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " >> update()" + ParamUtils.getString(applicationUser));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
    }
  }

  /**
   * If currently assigned personaIdList, contains last used persona id, then fine, else update the
   * last used persona id.
   *
   * @param unqUserId
   * @throws SystemException
   */
  public static void updateLastUsedPersona(Long unqUserId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateLastUsedPersona()" + unqUserId);
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.Parameters.PARAM_1, unqUserId);
    query.put(QueryConstants.Parameters.PARAM_2, unqUserId);
    query.put(QueryConstants.Parameters.PARAM_3, unqUserId);
    MySqlOperations.executeQuery(QueryConstants.UserManageMent.UPDATE_LAST_USED_PERSONA_SQL, query);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateLastUsedPersona()" + unqUserId);
  }
  /**
   * This method return sigin user role details
   *
   * @author Sunil Jagtap date 11-01-2018
   * @param unqUserId
   * @return
   * @throws SystemException
   */
  public static List<UserRoleMaster> getUserRoleDetails(Long unqUserId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserRoleDetails()" + unqUserId);
    List<UserRoleMaster> userRole = new ArrayList<UserRoleMaster>();
    if (unqUserId != null) {
      String query = QueryConstants.UserManageMent.FETCH_USER_ROLE_DETAILS;
      Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
      objectMap.put(QueryConstants.ShareAccesstMapping.USER_ID, unqUserId);
      userRole = MySqlOperations.scanWithSqlQuery(UserRoleMaster.class, query, objectMap);
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserRoleDetails()" + userRole);
    return userRole;
  }
  /**
   * This method return sigin user group info
   *
   * @author Sunil Jagtap date 11-01-2018
   * @param unqUserId
   * @return
   * @throws SystemException
   */
  public static List<UserGroup> getUserGroupDetails(Long unqUserId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserGroupDetails()" + unqUserId);
    List<UserGroup> userGroup = new ArrayList<UserGroup>();
    if (unqUserId != null) {
      String query = QueryConstants.UserManageMent.FETCH_USER_GROUP_DETAILS;
      Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
      objectMap.put(QueryConstants.ShareAccesstMapping.USER_ID, unqUserId);
      userGroup = MySqlOperations.scanWithSqlQuery(UserGroup.class, query, objectMap);
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserGroupDetails()" + userGroup);
    return userGroup;
  }
  
  public static void maintainUserHistory(String userId, String email, String action) throws SystemException{
	  
	  logger.debug(
		        LoggerConstants.LOG_MAXIQWEB
		            + " >> maintainUserHistory()"
		            + ParamUtils.getString(userId, email, action));

		    String insertHisQuery =
		        "insert into application_user_history"
		            + "(his_unqUserId,his_lastName,his_status,his_userUniqueId,his_class,his_password,his_userBrowsers,his_emailId,"
		            + "his_userCreatedDate,his_userPlatform,his_userId,his_userips,his_firstName,his_passwordexpirydays,his_userGroup,"
		            + "his_groupId,his_groupName,his_createdBy,his_createdTs,his_updatedBy,his_updatedTs,groupIdList,groupNameList,"
		            + "subGroupIdList,subGroupNameList,roleIdList,roleNameList,action,insertedBy)select au.unqUserId,au.lastName,"
		            + "au.status,au.userUniqueId,au.class,au.password,au.userBrowsers,au.emailId,au.userCreatedDate,au.userPlatform,"
		            + "au.userId,au.userips,au.firstName,au.passwordexpirydays,au.userGroup,au.groupId,au.groupName,au.createdBy,"
		            + "au.createdTs,au.updatedBy,au.updatedTs,group_concat(distinct ' ', ugm.groupId) as groupIdList,"
		            + "group_concat(distinct ' ', ug.groupName) as groupNameList,"
		            + "group_concat(distinct ' ', usg.subgroupId) as subGroupIdList,"
		            + "group_concat(distinct ' ', usg.subgroupName) as subGroupNameList,"
		            + "group_concat(distinct ' ', urm.roleId) as roleIdList,group_concat(distinct ' ', rm.roleName) as roleNameList,'"
		            + action
		            + "','"
		            + email
		            + "' from application_user au left outer join user_group_mappings ugm on au.unqUserId=ugm.userId"
		            + " left outer join usergroup ug on ug.groupId=ugm.groupId"
		            + " left outer join user_subgroup_mappings usgm on au.unqUserId=usgm.userId"
		            + " left outer join user_subgroup usg on usgm.subgroupId=usg.subGroupId"
		            + " left outer join user_role_mappings urm on au.unqUserId=urm.userId"
		            + " left outer join role_master rm on urm.roleId=rm.unqRoleMasterId"
		            + " where au.unqUserId = ? group by au.unqUserId";

		    Map<String, Object> query = new LinkedHashMap<String, Object>();
		    /*query.put("", value);
		    query.put("", value);*/
		    query.put("au.unqUserId", userId);
		    UserManagementDao.executeQuery(insertHisQuery, query);
	}

	public static Long getLongDate(String stringDate) throws ParseException {
		if (StringUtils.isBlank(stringDate) || stringDate.equalsIgnoreCase("null")
				|| stringDate.equalsIgnoreCase("0000-00-00 00:00:00"))
			return null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		// dateFormat.setTimeZone(TimeZone.getTimeZone(GlobalParams.MAXIQ_STD_IST_FORMAT));
		Date date = dateFormat.parse(stringDate);
		return date.getTime();
	}

}

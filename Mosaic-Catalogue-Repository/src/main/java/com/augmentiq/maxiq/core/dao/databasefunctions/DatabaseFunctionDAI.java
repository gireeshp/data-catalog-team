package com.augmentiq.maxiq.core.dao.databasefunctions;

import java.io.IOException;
import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.model.databasefunctions.bean.DatabaseFunction;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface DatabaseFunctionDAI {
  public String fetchDatabaseFunctionsJsonByFunctionTypebyDatabaseName(
      int DatabaseFunctionTypeId, int DatabaseNameId) throws Exception;

  public List<DatabaseFunction> fetchDatabaseFunctionsObjectByDatabaseName(String databaseName)
      throws SystemException, JsonGenerationException, JsonMappingException, IOException;
}

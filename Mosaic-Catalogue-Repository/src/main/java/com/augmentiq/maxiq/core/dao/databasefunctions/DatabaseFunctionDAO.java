package com.augmentiq.maxiq.core.dao.databasefunctions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.configuration.enums.database.functions.DatabaseNames;
import com.augmentiq.maxiq.model.databasefunctions.bean.DatabaseFunction;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DatabaseFunctionDAO implements DatabaseFunctionDAI {

  // conversion and parsing functions

  private String databasesName;

  public String listOfMaptoJsonParser(List<Map<String, Object>> value)
      throws JsonGenerationException, JsonMappingException, IOException {

    Map<String, Map<String, Object>> mapValue = new HashMap<String, Map<String, Object>>();

    for (Map<String, Object> val : value) {
      mapValue.put(val.get("id").toString(), val);
    }

    ObjectMapper mapperObj = new ObjectMapper();
    String jsonValue = mapperObj.writeValueAsString(mapValue);

    return jsonValue;
  }

  public static Map<String, String> jsonObjectToMapOfStringParser(
      JSONObject json, Map<String, String> out) throws JSONException {

    @SuppressWarnings("unchecked")
    Iterator<String> keys = json.keys();

    while (keys.hasNext()) {
      String key = keys.next();
      String val = null;
      try {
        JSONObject value = json.getJSONObject(key);
        jsonObjectToMapOfStringParser(value, out);
      } catch (Exception e) {
        val = json.getString(key);
      }

      if (val != null) {
        out.put(key, val);
      }
    }
    return out;
  }

  // fetch functions
  @Override
  public List<DatabaseFunction> fetchDatabaseFunctionsObjectByDatabaseName(String databasesName)
      throws SystemException, JsonGenerationException, JsonMappingException, IOException {

    String sql =
        " SELECT df.id, df.name databaseFunctionsName, df.parameters, df.returnType, df.databaseFunctionTypeId, df.databaseNameId, df.createdBy, df.updatedBy,"
            + "dft.name AS databaseFunctionType,dn.name AS databaseName, dfu.id databaseFunctionUdfsId, dfu.className "
            + " FROM database_functions df "
            + " 	JOIN database_function_types dft ON dft.id = df.databaseFunctionTypeId "
            + " 	JOIN database_names dn ON dn.id = df.databaseNameId "
            + " JOIN database_function_udfs dfu ON dfu.id= df.databaseFunctionUDFId"
            + " WHERE dn.name = ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("dn.name", databasesName);
    List<DatabaseFunction> scanWithSqlQuery =
        MySqlOperations.scanWithSqlQuery(DatabaseFunction.class, sql, objectMap);
    return scanWithSqlQuery;
  }

  public String fetchDatabaseFunctionsJsonByFunctionTypebyDatabaseName(
      int DatabaseFunctionTypeId, int DatabaseNameId)
      throws SystemException, JsonGenerationException, JsonMappingException, IOException {

    /*String sql =
    " SELECT df.*,dft.name AS databaseFunctionType,dn.name AS databaseName "
    + " FROM database_functions df "
    + " 	JOIN database_function_types dft ON df.databaseFunctionTypeId = dft.id "
    + " 	JOIN database_names dn ON df.databaseNameId = dn.id "
    + " WHERE dn.id = " + DatabaseNameId
    + " 	AND dft.id = " + DatabaseFunctionTypeId + " ";*/

    String sql =
        " SELECT df.*,dft.name AS databaseFunctionType,dn.name AS databaseName "
            + " FROM database_functions df "
            + " 	JOIN database_function_types dft ON df.databaseFunctionTypeId = dft.id "
            + " 	JOIN database_names dn ON df.databaseNameId = dn.id "
            + " WHERE dn.id = ?	AND dft.id = ?";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("DatabaseNameId", DatabaseNameId);
    objectMap.put("DatabaseFunctionTypeId", DatabaseFunctionTypeId);

    List<Map<String, Object>> value =
        MySqlOperations.executeQueryForResultSetPrepStatement(sql, null, objectMap);

    return listOfMaptoJsonParser(value);
  }

  @SuppressWarnings("null")
  public String fetchAllParametersInPrintingFormatJson(int databaseNameId)
      throws JsonGenerationException, JsonMappingException, IOException, SystemException,
          JSONException {

    String whereClause = "";

    if (GENERAL_CONSTANTS.ZERO != databaseNameId && null != DatabaseNames.getName(databaseNameId)) {
      if (whereClause.equalsIgnoreCase("")) {
        whereClause += " WHERE ";
      }
      whereClause += " df.databaseNameId = ?";
    }

    String sql =
        " SELECT df.id,df.name,df.parameters " + " FROM database_functions df " + whereClause;

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("databaseNameId", databaseNameId);

    List<Map<String, Object>> values =
        MySqlOperations.executeQueryForResultSetPrepStatement(sql, null, objectMap);

    Map<String, List<String>> mappedOutput = new HashMap<String, List<String>>();
    JSONObject jsonObject = new JSONObject();
    String regexString = Pattern.quote("{") + "(.*?)" + Pattern.quote("}");
    Pattern pattern = Pattern.compile(regexString);

    for (Map<String, Object> value : values) {
      // text contains the full text that you want to extract data
      Matcher matcher = pattern.matcher(value.get("parameters").toString());
      String textInBetween = value.get("name").toString().toUpperCase() + "(";
      while (matcher.find()) {
        String str = "";
        str += textInBetween;
        int commaCount = StringUtils.countMatches(matcher.group(1).toString(), ",");
        for (int i = 0; i < commaCount; i++) {
          str += ",";
        }
        str += ")";
        jsonObject.put(value.get("id").toString(), str);
      }
    }
    return jsonObject.toString();
  }
}

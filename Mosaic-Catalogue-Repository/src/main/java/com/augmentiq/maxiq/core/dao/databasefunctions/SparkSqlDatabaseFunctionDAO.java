package com.augmentiq.maxiq.core.dao.databasefunctions;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.configuration.enums.database.functions.DatabaseNames;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class SparkSqlDatabaseFunctionDAO extends DatabaseFunctionDAO {

  public String fetchDatabaseFunctionsJson()
      throws SystemException, JsonGenerationException, JsonMappingException, IOException {

    String sql =
        " SELECT df.*,dft.name AS databaseFunctionType,dn.name AS databaseName "
            + " FROM database_functions df "
            + " 	JOIN database_function_types dft "
            + " 	JOIN database_names dn "
            + " WHERE dn.id = "
            + DatabaseNames.SPARK_SQL.getValue();

    List<Map<String, Object>> value = MySqlOperations.executeQueryForResultSet(sql, null);

    return listOfMaptoJsonParser(value);
  }
}

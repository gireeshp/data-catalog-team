package com.augmentiq.maxiq.core.dao.ds.apiexposure;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DatasourceServiceApimanager;

public class DataSourceExposeDAO {
	private static final Logger logger = LoggerFactory.getLogger(DataSourceExposeDAO.class);

	public static DatasourceServiceApimanager fetchDataSourceApiDetails(Long userId, Long dataSourceId,
			String dataSourceName) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> fetchDataSourceApiDetails()"
				+ ParamUtils.getString(userId, dataSourceId, dataSourceName));
		Map<String, Object> query = new LinkedHashMap<>();
		query.put(QueryConstants.DatasourceServiceApimanager.DATASOURCE_ID, dataSourceId);
		query.put(QueryConstants.DatasourceServiceApimanager.USER_ID, userId);
		query.put(QueryConstants.DatasourceServiceApimanager.DATASOURCE_NAME, dataSourceName);
		DatasourceServiceApimanager datasourceServiceApimanager = MySqlOperations
				.scanOneForQuery(DatasourceServiceApimanager.class, query);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << fetchDataSourceApiDetails()",
				ParamUtils.getString(datasourceServiceApimanager));
		return datasourceServiceApimanager;
	}

	public static Long persistApiInfo(DatasourceServiceApimanager datasourceServiceApimanager) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> persistApiInfo()"
				+ ParamUtils.getString(datasourceServiceApimanager));
		return MySqlOperations.insert(datasourceServiceApimanager, QueryConstants.DatasourceServiceApimanager.ID);
	}

	public static void updateApiInfo(DatasourceServiceApimanager datasourceServiceApimanager) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> updateApiInfo()"
				+ ParamUtils.getString(datasourceServiceApimanager));
		String query = "update datasource_service_apimanager set status = ? ,"
				+ " modifiedDate = ?,query=? where userId = ? AND id= ?";

		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(QueryConstants.DatasourceServiceApimanager.STATUS, datasourceServiceApimanager.getStatus());
		params.put(QueryConstants.DatasourceServiceApimanager.MODIFIED_DATE,
				datasourceServiceApimanager.getModifiedDate());
		params.put(QueryConstants.DatasourceServiceApimanager.QUERY, datasourceServiceApimanager.getQuery());
		params.put(QueryConstants.DatasourceServiceApimanager.USER_ID, datasourceServiceApimanager.getUserId());
		params.put(QueryConstants.DatasourceServiceApimanager.ID, datasourceServiceApimanager.getId());
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << updateApiInfo()",
				ParamUtils.getString(datasourceServiceApimanager));
		MySqlOperations.updateQuery(query, params);
	}

	public static DatasourceServiceApimanager fectApiDetailsByApiKeyAndApiId(Long id, String apiKey)
			throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> fectApiDetailsByApiKeyAndApiId()"
				+ ParamUtils.getString(id, apiKey));
		Map<String, Object> query = new LinkedHashMap<>();
		query.put(QueryConstants.DatasourceServiceApimanager.ID, id);
		query.put(QueryConstants.DatasourceServiceApimanager.APIKEY, apiKey);
		DatasourceServiceApimanager datasourceServiceApimanager = MySqlOperations
				.scanOneForQuery(DatasourceServiceApimanager.class, query);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << fectApiDetailsByApiKeyAndApiId()",
				ParamUtils.getString(datasourceServiceApimanager));
		return datasourceServiceApimanager;
	}

	public static List<Map<String, Object>> getDeployedDS(String userId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getDeployedDS()" + ParamUtils.getString(userId));
		String sqlStmt = "select T1.*,CONCAT(T2.firstName,' ',T2.lastName) AS createdBy from datasource_service_apimanager T1 LEFT JOIN application_user T2 on "
				+ "T1.userId =T2.unqUserId where T1.userId= ? ";
		Map<String, Object> sql = new LinkedHashMap<String, Object>();
		sql.put(QueryConstants.DatasourceServiceApimanager.USER_ID, userId);
		List<Map<String, Object>> deployedDS = MySqlOperations.executeQueryForResultSetPrepStatement(sqlStmt, null,
				sql);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getDeployedDS()", ParamUtils.getString(deployedDS));
		return deployedDS;
	}

	/**
	 * Changed for MAX-1900 Changed by Yogesh Lokhande Description : DatasourceId is
	 * long type so no need to put ? in single quote ('?' instead use ? )
	 * 
	 * @param userId
	 * @param dataSourceId
	 * @return
	 * @throws SystemException
	 */
	public static List<Map<String, Object>> getDeployedDSApiByDatasourceId(String userId, Long dataSourceId)
			throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getDeployedDSApiByDatasourceId()"
				+ ParamUtils.getString(userId, dataSourceId));
		String sqlStmt = "select T1.*,CONCAT(T2.firstName,' ',T2.lastName) AS createdBy from datasource_service_apimanager T1 LEFT JOIN application_user T2 on "
				+ "T1.userId =T2.unqUserId where T1.userId=? and T1.dataSourceId=?";
		Map<String, Object> sql = new LinkedHashMap<String, Object>();
		sql.put(QueryConstants.DatasourceServiceApimanager.USER_ID, userId);
		sql.put(QueryConstants.DatasourceServiceApimanager.DATASOURCE_ID, dataSourceId);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getDeployedDSApiByDatasourceId()");
		return MySqlOperations.executeQueryForResultSetPrepStatement(sqlStmt, null, sql);
	}
}

package com.augmentiq.maxiq.core.dao.glossarydao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlossaryMaster;
import com.augmentiq.maxiq.model.services.glossary.GlossaryDTO;

public class GlossaryDAO {
  private static final Logger logger = LoggerFactory.getLogger(GlossaryDAO.class);

  /**
   * @param glossaryMaster
   * @return inserted id
   * @throws SystemException
   */
  public static Long addTag(GlossaryMaster glossaryMaster) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> addTag()" + ParamUtils.getString(glossaryMaster));
    Long insertedId = MySqlOperations.insert(glossaryMaster, "");
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> addTag()"
            + ParamUtils.getString("glossary inserted at  " + insertedId));
    return insertedId;
  }

  /**
   * @return List of glossaries which are active means non deleted
   * @throws SystemException
   */
  public static List<GlossaryDTO> getAllTagsByParams(String status) throws SystemException {
    String query =
        QueryConstants.Query.SELECT
            + CHAR_CONSTANTS.ASTERISK
            + CHAR_CONSTANTS.COMMA
            + CHAR_CONSTANTS.OPENING_BRACES
            + QueryConstants.Query.SELECT
            + " concat(a.firstName,' ', a.lastName) "
            + QueryConstants.Query.FROM
            + " application_user a "
            + QueryConstants.Query.WHERE
            + " a.unqUserId = gm.createdBy "
            + CHAR_CONSTANTS.CLOSING_BRACES
            + " createdByName "
            + CHAR_CONSTANTS.COMMA
            + CHAR_CONSTANTS.OPENING_BRACES
            + QueryConstants.Query.SELECT
            + " concat(a.firstName,' ', a.lastName) "
            + QueryConstants.Query.FROM
            + " application_user a "
            + QueryConstants.Query.WHERE
            + " a.unqUserId = gm.modifiedBy "
            + CHAR_CONSTANTS.CLOSING_BRACES
            + " modifiedByName "
            + QueryConstants.Query.FROM
            + CHAR_CONSTANTS.SPACE
            + Sequences.GLOSSARY
            + CHAR_CONSTANTS.SPACE
            + " gm "
            + QueryConstants.Query.WHERE
            + QueryConstants.GlossaryMaster.STATUS
            + CHAR_CONSTANTS.EQUALS
            + " ? "
            + CHAR_CONSTANTS.SPACE
            + QueryConstants.Query.ORDER_BY
            + " id "
            + QueryConstants.Query.DESC;

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put(QueryConstants.GlossaryMaster.STATUS, status);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllTagsByParams()"
            + ParamUtils.getString("Query to get list=", query));
    List<GlossaryDTO> listOfGlobalMaster =
        MySqlOperations.scanWithSqlQuery(GlossaryDTO.class, query, objectMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllTagsByParams()"
            + ParamUtils.getString("List of Glossary  " + listOfGlobalMaster));
    return listOfGlobalMaster;
  }

  /**
   * Delete the record by id only changing status from 1-Active to 0-Inactive
   *
   * @param glossaryMaster
   * @throws SystemException
   */
  public static void softDeleteTag(GlossaryMaster glossaryMaster) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> softDeleteTag()"
            + ParamUtils.getString(glossaryMaster));
    String sql =
        QueryConstants.Query.UPDATE
            + Sequences.GLOSSARY
            + QueryConstants.Query.SET
            + QueryConstants.GlossaryMaster.STATUS
            + CHAR_CONSTANTS.EQUALS
            + "?"
            + QueryConstants.Query.WHERE
            + QueryConstants.GlossaryMaster.ID
            + CHAR_CONSTANTS.EQUALS
            + "?";

    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put(QueryConstants.GlossaryMaster.STATUS, QueryConstants.GlossaryMaster.INACTIVE);
    params.put(QueryConstants.GlossaryMaster.ID, glossaryMaster.getId());

    MySqlOperations.executeQuery(sql, params);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> softDeleteTag()" + ParamUtils.getString(sql));
  }

  /**
   * Delete the record by id
   *
   * @param glossaryMaster
   * @throws SystemException
   */
  public static void deleteTag(GlossaryMaster glossaryMaster) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> deleteTag()" + ParamUtils.getString(glossaryMaster));
    String query =
        QueryConstants.Query.DELETE
            + QueryConstants.Query.FROM
            + Sequences.GLOSSARY
            + QueryConstants.Query.WHERE
            + QueryConstants.GlossaryMaster.ID
            + CHAR_CONSTANTS.EQUALS
            + "?";

    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put(QueryConstants.GlossaryMaster.ID, glossaryMaster.getId());

    MySqlOperations.executeQuery(query, params);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteTag()" + ParamUtils.getString(query));
  }

  public static List<String> getAllDSWithWithTagUsed(GlossaryMaster glossaryMaster)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllDSWithWithTagUsed()"
            + ParamUtils.getString(glossaryMaster.getName()));
    String query =
        "SELECT name FROM configuration where json like '%\"tags\":[%\""
            + glossaryMaster.getName()
            + "\"%'";
    List<String> listOfDs = new ArrayList<String>();

    List<Map<String, Object>> listOfFieldsMap =
        MySqlOperations.executeQueryForResultSet(query, new String[] {"name"});

    for (Map<String, Object> mp : listOfFieldsMap) {
      listOfDs.add(String.valueOf(mp.get("name")));
    }
    return listOfDs;
  }
}

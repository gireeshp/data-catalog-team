package com.augmentiq.maxiq.core.dao.inputparam;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.InputParameterValues;

/** @author Balkrushna Patil Created for MAX-523 By Balkrushna Patil on 26-Sept-2016 */
public class InputParamDAO {

  private static final Logger logger = LoggerFactory.getLogger(InputParamDAO.class);

  /**
   * @param inputParameterValues
   * @param maxRootId
   * @return
   * @throws SystemException
   */
  public static List<InputParameterValues> getListLoadingSameInputParam(
      InputParameterValues inputParameterValues, Long maxRootId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getListLoadingSameInputParam()"
            + ParamUtils.getString(inputParameterValues, maxRootId));
    String sql =
        "select * from dataParamInstance where paramName=?,val=?, dsId=?, baapJobInsId=?, type=?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("paramName", inputParameterValues.getParamName());
    objectMap.put("val", inputParameterValues.getVal());
    objectMap.put("dsId", inputParameterValues.getDsId());
    objectMap.put("baapJobInsId", maxRootId);
    objectMap.put("type", inputParameterValues.getType());
    List<InputParameterValues> inputParamList =
        MySqlOperations.scanWithSqlQuery(InputParameterValues.class, sql, objectMap);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getListLoadingSameInputParam()"
            + ParamUtils.getString(inputParamList));
    return inputParamList;
  }
}

package com.augmentiq.maxiq.core.dao.jupyter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.JupyterBean;

/** @author Balkrushna Patil */
public class JupyterDao {

  private static final Logger logger = LoggerFactory.getLogger(JupyterDao.class);

  public static void saveJupyterDetails(JupyterBean jupyterBean) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> saveJupyterDetails()"
            + ParamUtils.getString(jupyterBean));
    if (null != jupyterBean) {
      MySqlOperations.insert(jupyterBean);
    }

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << saveJupyterDetails()");
  }

  public static List<JupyterBean> getJupyterNotebookList(Long groupId, Long projectId)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getJupyterNotebookList()"
            + ParamUtils.getString(groupId, projectId));
    String query =
        "select jc.id, jc.ownerProjectId, jc.notebookName, jc.createdOn,jc.groupId, IFNULL(CONCAT(user.firstName, ' ', user.lastName), jc.createdBy) "
            + " as createdBy, jc.ownerProjectId from "
            + Sequences.JUPYTER
            + " jc join application_user user on (jc.createdBy = user.unqUserId) "
            + " where jc.ownerProjectId=?";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("jc.ownerProjectId", projectId);
    List<JupyterBean> fetchNotebookList =
        MySqlOperations.scanWithSqlQuery(JupyterBean.class, query, objectMap);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getJupyterNotebookList()"
            + ParamUtils.getString(fetchNotebookList));
    return fetchNotebookList;
  }

  public static void deleteNotebook(String notebookName) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> deleteNotebook()" + ParamUtils.getString(notebookName));

    JupyterBean jupyterBean = new JupyterBean();
    jupyterBean.setNotebookName(notebookName);
    JupyterBean notebookDetailsByNotebookName =
        JupyterDao.getNotebookDetailsByNotebookName(notebookName);
    MySqlOperations.deleteRow(notebookDetailsByNotebookName);

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << deleteNotebook()");
  }

  public static JupyterBean getNotebookDetailsByNotebookName(String notebookName)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getNotebookDetailsByNotebookName()"
            + ParamUtils.getString(notebookName));
    Map<String, Object> map = new LinkedHashMap<>();
    map.put("notebookName", notebookName);

    JupyterBean jupyterBean = MySqlOperations.scanOneForQuery(JupyterBean.class, map);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getNotebookDetailsByNotebookName()"
            + ParamUtils.getString(jupyterBean));
    return jupyterBean;
  }
}

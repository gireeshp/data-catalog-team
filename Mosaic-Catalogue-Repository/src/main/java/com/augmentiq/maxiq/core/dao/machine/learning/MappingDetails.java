package com.augmentiq.maxiq.core.dao.machine.learning;

import java.io.Serializable;

public class MappingDetails implements Serializable {

  private String ClassOfModel;
  private String NumberOfFeatures;
  private String DisplayName;

  public MappingDetails() {
    super();
  }

  public String getClassOfModel() {
    return ClassOfModel;
  }

  public void setClassOfModel(String classOfModel) {
    ClassOfModel = classOfModel;
  }

  public String getNumberOfFeatures() {
    return NumberOfFeatures;
  }

  public void setNumberOfFeatures(String numberOfFeatures) {
    NumberOfFeatures = numberOfFeatures;
  }

  public String getDisplayName() {
    return DisplayName;
  }

  public void setDisplayName(String displayName) {
    DisplayName = displayName;
  }

  @Override
  public String toString() {
    return "MappingDetails [ClassOfModel="
        + ClassOfModel
        + ", NumberOfFeatures="
        + NumberOfFeatures
        + ", DisplayName="
        + DisplayName
        + "]";
  }
}

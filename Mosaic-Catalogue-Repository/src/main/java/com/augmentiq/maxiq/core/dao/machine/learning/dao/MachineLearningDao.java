package com.augmentiq.maxiq.core.dao.machine.learning.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.RModel;

/** @author Rushi created on Apr 21, 2017 for MAX-720 */
public class MachineLearningDao {
  private static final Logger logger = LoggerFactory.getLogger(MachineLearningDao.class);

  /**
   * @param rModel
   * @throws SystemException
   */
  public static void saveRModel(RModel rModel) throws SystemException {

    logger.info(LoggerConstants.LOG_MAXIQWEB + " >> saveRModel()" + ParamUtils.getString(rModel));

    MySqlOperations.insert(rModel);

    logger.info(LoggerConstants.LOG_MAXIQWEB + " << saveRModel()" + ParamUtils.getString(rModel));
  }

  /**
   * @param rModel
   * @throws SystemException
   */
  public static void deleteRModel(String modelId) throws SystemException {

    logger.info(LoggerConstants.LOG_MAXIQWEB + " >> saveRModel()" + ParamUtils.getString(modelId));

    String queryForRModelDelete =
        "delete from " + Sequences.MACHINE_LEARNING_MODELS + " where GENERAL_CONSTANTS.ModelId =?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(GENERAL_CONSTANTS.ModelId, modelId);
    MySqlOperations.executeQuery(queryForRModelDelete, query);

    logger.info(LoggerConstants.LOG_MAXIQWEB + " << saveRModel()" + ParamUtils.getString(modelId));
  }

  /**
   * @param modelId
   * @return location of model file on agent machine
   */
  public static String getRModelLocationFromModelId(Long modelId) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getRModelFromModelId()"
            + ParamUtils.getString(modelId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(GENERAL_CONSTANTS.ModelId, modelId);
    try {
      RModel rModel = MySqlOperations.scanOneForQuery(RModel.class, query);
      return rModel.getModelLocation();
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
    }
    return null;
  }

  /**
   * @param groupId
   * @return
   */
  public static List<RModel> getListOfRModelsForGroupFromGroupId(Long groupId) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getListOfRModelsForGroup()"
            + ParamUtils.getString(groupId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.Parameters.GROUP_ID, groupId);
    query.put(QueryConstants.Parameters.MODEL_TYPE, "SparkR");
    try {
      List<RModel> listOfRModels = MySqlOperations.scanForQuery(RModel.class, query);
      return listOfRModels;
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
    }
    return null;
  }

  /**
   * @param groupId
   * @return
   */
  public static List<RModel> getListOfAllModelsForGroupFromGroupId(Long groupId, Long projectId) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getListOfRModelsForGroup()"
            + ParamUtils.getString(groupId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    //query.put(QueryConstants.parameters.GROUP_ID, groupId);
    query.put(QueryConstants.Project.OWNER_PROJECT_ID, projectId);
    try {
      List<RModel> listOfRModels = MySqlOperations.scanForQuery(RModel.class, query);

      return listOfRModels;
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
    }
    return null;
  }

  public static List<RModel> getListOfSparkModelsForProjectFromProjectId(
      Long groupId, Long projectId) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getListOfRModelsForGroup()"
            + ParamUtils.getString(groupId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    //query.put(QueryConstants.parameters.GROUP_ID, groupId);
    query.put(QueryConstants.Project.OWNER_PROJECT_ID, projectId);
    query.put(QueryConstants.Parameters.MODEL_TYPE, "SparkMLlib");
    try {
      List<RModel> listOfRModels = MySqlOperations.scanForQuery(RModel.class, query);

      return listOfRModels;
    } catch (SystemException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
      throw new SystemException(LoggerConstants.LOG_MAXIQWEB + e);
    }
  }

  public static Long updateRModel(RModel rModel) throws SystemException {
    logger.info(LoggerConstants.LOG_MAXIQWEB + " >> updateRModel()" + ParamUtils.getString(rModel));
    return MySqlOperations.insert(rModel, "1");
  }

  /**
   * @param modelId
   * @return RModel object
   */
  public static RModel getModelFromModelId(String modelId) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getRModelFromModelId()"
            + ParamUtils.getString(modelId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(GENERAL_CONSTANTS.ModelId, modelId);
    try {
      RModel rModel = MySqlOperations.scanOneForQuery(RModel.class, query);
      return rModel;
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
    }
    return null;
  }
}

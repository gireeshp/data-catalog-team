package com.augmentiq.maxiq.core.dao.mycollection;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.MyCollection;

public class MyCollectionDAO {

  private static final Logger logger = LoggerFactory.getLogger(MyCollectionDAO.class);

  /**
   * @param objectId - object id selected by user
   * @param objectType
   * @param userId - unqUserId from @see ApplicationUser (userId) because this is new request
   * @throws Exception
   */
  public static void addToMyCollection(Long objectId, String objectType, Long userId)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createDataSourceRequest()"
            + ParamUtils.getString(objectId, objectType, userId));

    MyCollection myCollection = getMyCollectionById(userId, objectId, objectType);
    if (null != myCollection) {
      String sql =
          "update my_collection set status=1 "
              + "where "
              + QueryConstants.Collection.OBJECT_ID
              + "=?"
              + " and "
              + QueryConstants.Collection.USERID
              + "= ?"
              + " and "
              + QueryConstants.Collection.OBJECT_TYPE
              + "= ?";
      Map<String, Object> query = new LinkedHashMap<String, Object>();
      query.put(QueryConstants.Collection.OBJECT_ID, objectId);
      query.put(QueryConstants.Collection.USERID, userId);
      query.put(QueryConstants.Collection.OBJECT_TYPE, objectType);
      MySqlOperations.executeQuery(sql, query);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << Updated the record"
              + ParamUtils.getString(objectId, objectType, userId));

      return;
    }

    myCollection = new MyCollection();
    myCollection.setId(null);
    myCollection.setCreated_date(System.currentTimeMillis());
    myCollection.setObjectType(objectType);
    myCollection.setObjectId(objectId);
    myCollection.setUserId(userId);
    myCollection.setStatus(1);
    MySqlOperations.insert(myCollection, HbaseUtility.getIdAnnotationForClass(MyCollection.class));
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createDataSourceRequest()"
            + ParamUtils.getString(objectId, objectType, userId));
  }

  /**
   * @param objectId
   * @param objectType
   * @param userId
   * @throws Exception
   */
  public static void removeFromMyCollection(Long objectId, String objectType, Long userId)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createDataSourceRequest()"
            + ParamUtils.getString(objectId, objectType, userId));
    String sql =
        "update my_collection set status=0 where objectId=? and  userId= ? and objectType = ?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("objectId", objectId);
    query.put("userId", userId);
    query.put("objectType", objectType);
    MySqlOperations.executeQuery(sql, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createDataSourceRequest()"
            + ParamUtils.getString(objectId, objectType, userId));
  }

  /**
   * @param from - unqUserId from @see ApplicationUser (userId)Cookies OR session
   * @return - List of all MyCollections by userid
   * @throws Exception
   */
  public static List<MyCollection> getAllOfMyCollection(Long userId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllAcceptedDataSourceOfMyCollection(Long userId)"
            + ParamUtils.getString(userId));
    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.Collection.USERID, userId);
    queryMap.put(QueryConstants.Collection.STATUS, 1);
    List<MyCollection> result = MySqlOperations.scanForQuery(MyCollection.class, queryMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getAllAcceptedDataSourceOfMyCollection(Long userId)"
            + ParamUtils.getString(result));
    return result;
  }

  public static void main(String[] args) throws Exception {
    List<MyCollection> allOfMyCollection = getAllOfMyCollection(37l);
    System.out.println("Hi" + allOfMyCollection);
  }

  /**
   * @param userId
   * @param objectId
   * @param objectType
   * @return
   * @throws Exception
   */
  public static MyCollection getMyCollectionById(Long userId, Long objectId, String objectType)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getMyCollectionById(Long userId, Long objectId, String objectType)"
            + ParamUtils.getString(userId, objectId, objectType));
    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.Collection.USERID, userId);
    queryMap.put(QueryConstants.Collection.OBJECT_ID, objectId);
    queryMap.put(QueryConstants.Collection.OBJECT_TYPE, objectType);
    MyCollection myCollection = MySqlOperations.scanOneForQuery(MyCollection.class, queryMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getMyCollectionById(Long userId, Long objectId, String objectType)"
            + ParamUtils.getString(myCollection));
    return myCollection;
  }
}

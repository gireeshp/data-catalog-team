package com.augmentiq.maxiq.core.dao.searchdao;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.search.domian.IndexDefination;

/** Created by shivanand on 7/3/15. */
public class IndexDefinationDao {
  private static final Logger logger = LoggerFactory.getLogger(IndexDefinationDao.class);

  public static void insertIndexDefination(IndexDefination indexDefination) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> insertIndexDefination()"
            + ParamUtils.getString(indexDefination));
    MySqlOperations.insert(indexDefination);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << insertIndexDefination()"
            + ParamUtils.getString(indexDefination));
  }

  public static void insertIndexDefinationWithAutoIncrement(IndexDefination indexDefination)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> insertIndexDefinationWithAutoIncrement()"
            + ParamUtils.getString(indexDefination));
    Long indexId =
        MySqlOperations.insert(
            indexDefination, HbaseUtility.getIdAnnotationForClass(indexDefination.getClass()));
    if (indexDefination.getId() == null) {
      indexDefination.setId(indexId);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << insertIndexDefinationWithAutoIncrement()"
            + ParamUtils.getString(indexDefination));
  }

  public static void deleteIndexDefination(IndexDefination indexDefination)
      throws SystemException, SQLException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> deleteIndexDefination()"
            + ParamUtils.getString(indexDefination));
    MySqlOperations.deleteRow(indexDefination);
    ;
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << deleteIndexDefination()"
            + ParamUtils.getString(indexDefination));
  }

  public static List<IndexDefination> getListOfIndexDefinationForGroup(Long groupId, Long projectId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getListOfIndexDefinationForGroup()"
            + ParamUtils.getString(groupId));

    String sql =
        "select ind.id,ind.indexName, ind.indexFields, "
            + "ind.createdOn, ind.lastRunOn, ind.runningStatus, ind.count, "
            + "ind.dsId, ind.appId, ind.nodeId, ind.searchInputType, "
            + "ind.indexInputType, ind.sampleJson,ind.inputDataDilimiter,"
            + "ind.groupId,CONCAT(A.firstName,' ',A.lastName) "
            + "as createdBy, ind.ownerProjectId from index_defination ind left outer join "
            + "application_user A on(A.unqUserId = ind.createdBy) "
            + "where ind.groupId= ? and ind.ownerProjectId= ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("ind.groupId", groupId);
    objectMap.put("ind.ownerProjectId", projectId);
    List<IndexDefination> hiveQuery =
        MySqlOperations.scanWithSqlQuery(IndexDefination.class, sql, objectMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getInsertDefinations()"
            + ParamUtils.getString(hiveQuery));
    return hiveQuery;
  }

  public static IndexDefination getIndexDefination(Long definationId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getIndexDefination()"
            + ParamUtils.getString(definationId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(QueryConstants.IndexConstants.ID, definationId);

    IndexDefination indexDefination = MySqlOperations.scanOneForQuery(IndexDefination.class, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getIndexDefination()"
            + ParamUtils.getString(indexDefination));
    return indexDefination;
  }

  @Deprecated
  /**
   * Dont use this method as this searches on the basis of name and not id use method which takes
   * input as id
   *
   * @param indexName
   * @return
   * @throws SystemException
   */
  public static IndexDefination getIndexDefinationFromIndexName(String indexName)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getIndexDefination()"
            + ParamUtils.getString(indexName));
    Map<String, Object> query1 = new LinkedHashMap<String, Object>();
    query1.put(QueryConstants.IndexConstants.INDEX_NAME, indexName);

    IndexDefination indexDefination =
        MySqlOperations.scanOneForQuery(IndexDefination.class, query1);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getIndexDefination()"
            + ParamUtils.getString(indexDefination));
    return indexDefination;
  }
}

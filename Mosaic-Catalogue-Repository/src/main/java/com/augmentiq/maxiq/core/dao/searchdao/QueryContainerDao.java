package com.augmentiq.maxiq.core.dao.searchdao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.entity.model.search.domian.QueryContainer;

/** Created by shivanand on 7/14/2015. */
public class QueryContainerDao {
  private static final Logger logger = LoggerFactory.getLogger(QueryContainerDao.class);

  public static void insertQueryContainer(QueryContainer queryContainer) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> insertQueryContainer()"
            + ParamUtils.getString(queryContainer));

    MySqlOperations.insert(queryContainer);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << insertQueryContainer()"
            + ParamUtils.getString(queryContainer));
  }

  public static void insertQueryContainerForAutoIncrement(QueryContainer queryContainer)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> insertQueryContainerForAutoIncrement()"
            + ParamUtils.getString(queryContainer));

    MySqlOperations.insert(
        queryContainer, HbaseUtility.getIdAnnotationForClass(QueryContainer.class));

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << insertQueryContainerForAutoIncrement()"
            + ParamUtils.getString(queryContainer));
  }

  public static List<QueryContainer> getAllQueryContainer(Long groupId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllQueryContainer()"
            + ParamUtils.getString(groupId));

    String sql =
        "select qry.id, qry.queryName, qry.indexName, "
            + "qry.queries, qry.aggregations, qry.createdOn, qry.apiKey,"
            + " CONCAT(A.firstName,' ',A.lastName) as createBy, "
            + "qry.outputFields, qry.groupId from search_query_def qry "
            + "left outer join application_user A on(A.unqUserId = qry.createBy) "
            + "where qry.groupId=?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("qry.groupId", groupId);
    List<QueryContainer> hiveQuery =
        MySqlOperations.scanWithSqlQuery(QueryContainer.class, sql, objectMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getAllQueryContainer()"
            + ParamUtils.getString(hiveQuery));
    return hiveQuery;
  }

  public static QueryContainer getQueryContainer(Long queryId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getQueryContainer()" + ParamUtils.getString(queryId));

    Map<String, Object> query1 = new LinkedHashMap<String, Object>();
    query1.put("id", queryId);

    QueryContainer queryContainer = MySqlOperations.scanOneForQuery(QueryContainer.class, query1);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getQueryContainer()"
            + ParamUtils.getString(queryContainer));
    return queryContainer;
  }

  public static QueryContainer getQueryContainer(String queryName) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getQueryContainer()" + ParamUtils.getString(queryName));
    Map<String, Object> query1 = new LinkedHashMap<String, Object>();
    query1.put("queryName", queryName);

    QueryContainer queryContainer = MySqlOperations.scanOneForQuery(QueryContainer.class, query1);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getQueryContainer()"
            + ParamUtils.getString(queryContainer));
    return queryContainer;
  }
}

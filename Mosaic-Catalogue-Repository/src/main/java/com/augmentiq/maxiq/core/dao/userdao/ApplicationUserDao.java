package com.augmentiq.maxiq.core.dao.userdao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.configuration.enums.UserStatus;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.entity.model.usergroup.UserGroups;

public class ApplicationUserDao {

  private static final Logger logger = LoggerFactory.getLogger(ApplicationUserDao.class);

  public static ApplicationUser getUserEmailGroupId(String email, Long groupId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getUserEmailGroupId()"
            + ParamUtils.getString(email, groupId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("emailId", email);

    ApplicationUser applicationUser = MySqlOperations.scanOneForQuery(ApplicationUser.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getUserEmailGroupId()"
            + ParamUtils.getString(applicationUser));

    return applicationUser;
  }

 /* public static UserGroups getGroupName(Long groupId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getGroupName()" + ParamUtils.getString(groupId));

    	return GroupManagementUtil.getGroupNameById(groupId);
  }*/

  public static String getUserName(String unqUserId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getUserName()" + ParamUtils.getString(unqUserId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("unqUserId", unqUserId);

    ApplicationUser applicationUser = MySqlOperations.scanOneForQuery(ApplicationUser.class, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getUserName()" + ParamUtils.getString(applicationUser));
    if (applicationUser == null) {
      return null;
    } else {
      return applicationUser.getFirstName() + " " + applicationUser.getLastName();
    }
  }

  public static ApplicationUser getUserInfoWithoutGroupId(String email) throws SystemException {
    // TODO Auto-generated method stub
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(Sequences.EMAILID, email);

    ApplicationUser applicationUser = MySqlOperations.scanOneForQuery(ApplicationUser.class, query);
    return applicationUser;
  }

  public static List<Map<String, Object>> getPasswordHistoryForUserInDb(String emailId)
      throws SystemException {
    // TODO Auto-generated method stub
    String query =
        "select his_password from application_user_history where his_emailId = ? order by appUserHist_Id desc limit 2";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("emailId", emailId);

    List<Map<String, Object>> listOfLastThreePassword =
        MySqlOperations.executeQueryForResultSetPrepStatement(
            query, new String[] {Sequences.HIS_PASSWORD}, objectMap);
    ApplicationUser userProfile = GenericStoreDao.getUserProfile(emailId, 1l);
    Map<String, Object> currentPassword = new HashMap<String, Object>();
    currentPassword.put(Sequences.HIS_PASSWORD, userProfile.getPassword());
    listOfLastThreePassword.add(currentPassword);
    return listOfLastThreePassword;
  }

  public static List<String> getAllUsers(Long groupId) throws SystemException {

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("groupId", groupId);

    List<Map<String, Object>> listOfUsers =
        MySqlOperations.executeQueryForResultSetPrepStatement(
            QueryConstants.GlobalSearch.FETCH_ALL_APPLICATION_USERS_NAME,
            new String[] {"createdBy"},
            objectMap);
    List<String> list = new ArrayList<>();
    if (listOfUsers != null && !listOfUsers.isEmpty()) {
      for (Map<String, Object> map : listOfUsers) {
        list.add((String) map.entrySet().iterator().next().getValue());
      }
    }
    return list;
  }

  public static void main(String[] args) throws SystemException {
    getAllUsers(2l);
  }

  public static List<ApplicationUser> getListOfAllUsers(Long userId) throws SystemException {
    String sql =
        "select * from application_user where unqUserId > 0 and status != ? and unqUserId != ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("status", UserStatus.DELETED.name());
    objectMap.put("unqUserId", userId);
    List<ApplicationUser> listOfUsers =
        MySqlOperations.scanWithSqlQuery(ApplicationUser.class, sql, objectMap);
    return listOfUsers;
  }

  public static List<UserGroups> getListOfUserGroup(Long userId) throws SystemException {
    //String sql="select groupName,groupId from usergroup where groupId in(select groupId from user_group_mappings where userId="+ userId +")";
    String sql = "select groupName,groupId from usergroup";
    List<UserGroups> listOfUsers = MySqlOperations.scanWithSqlQuery(UserGroups.class, sql, null);
    return listOfUsers;
  }

  public static List<UserGroups> getListOfUserGroupForObjectSharing(Long userId)
      throws SystemException {
    String sql =
        "select groupName,groupId from usergroup where groupId in(select groupId from user_group_mappings where userId=?)";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("userId", userId);
    List<UserGroups> listOfUsers =
        MySqlOperations.scanWithSqlQuery(UserGroups.class, sql, objectMap);
    return listOfUsers;
  }

  public static void updateGroupNameInApplicationUser(Long userId, Long groupId)
      throws SystemException {
    // TODO Update group name against user unqId
    String query = "update application_user set groupId = ? where unqUserId= ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("groupId", groupId);
    objectMap.put("unqUserId", userId);
    MySqlOperations.updateQuery(query, objectMap);
  }

  public static void setLoginFailureCnt(String emailId, long l) throws SystemException {
    // TODO Auto-generated method stub
    String query =
        "update application_user set loginFailureCount = ? , lastFailureAttemptTs = NOW() where emailId=?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("loginFailureCount", l);
    objectMap.put("emailId", emailId);
    MySqlOperations.updateQuery(query, objectMap);
  }

  public static void resetLoginFailureCnt(String emailId) throws SystemException {
    // TODO Auto-generated method stub
    String query = "update application_user set loginFailureCount = 0 where emailId = ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("emailId", emailId);
    MySqlOperations.updateQuery(query, objectMap);
  }

  public static void lockUser(String emailId) throws SystemException {
    // TODO Auto-generated method stub
    String query =
        "update application_user set lockInd = 'LOCK',loginFailureCount = 0 where emailId = ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("emailId", emailId);
    MySqlOperations.updateQuery(query, objectMap);
  }

  public static void unlockLockUser(String emailId) throws SystemException {
    // TODO Auto-generated method stub
    String query =
        "update application_user set lockInd = 'UNLOCK',loginFailureCount = 0 where emailId =?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("emailId", emailId);
    MySqlOperations.updateQuery(query, objectMap);
  }

  public static void insertIntoAudit(ApplicationUser appUserProfile) throws SystemException {
    // TODO Auto-generated method stub

    if (appUserProfile.getLastFailureAttemptTs() == null
        || StringUtils.equalsIgnoreCase(appUserProfile.getLastFailureAttemptTs(), "null")) {
      appUserProfile.setLastFailureAttemptTs("0000-00-00 00:00:00");
    }
    /*String query =  "insert into application_user_history( his_unqUserId,his_lastName , his_password , his_userBrowsers , lockInd, loginFailureCount  ,"
    		+ "lastFailureAttemptTs,his_passwordexpirydays,his_emailId) "
    			+ "values(?,?,?,?,?,?,?,?,?)";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("his_unqUserId", appUserProfile.getUnqUserId());
    objectMap.put("his_lastName", appUserProfile.getLastName());
    objectMap.put("his_password", appUserProfile.getPassword());
    objectMap.put("his_userBrowsers", appUserProfile.getUserBrowsers());
    objectMap.put("lockInd", appUserProfile.getLockInd());
    objectMap.put("loginFailureCount", appUserProfile.getLoginFailureCount());
    objectMap.put("lastFailureAttemptTs", appUserProfile.getLastFailureAttemptTs());
    objectMap.put("his_passwordexpirydays", appUserProfile.getPasswordexpirydays());
    objectMap.put("his_emailId", appUserProfile.getEmailId());
    MySqlOperations.updateQuery(query,objectMap);*/

    String query =
        "insert into application_user_history( his_unqUserId,his_lastName , his_password , his_userBrowsers , lockInd, loginFailureCount  ,"
            + "lastFailureAttemptTs,his_passwordexpirydays,his_emailId) "
            + "values("
            + appUserProfile.getUnqUserId()
            + ",'"
            + appUserProfile.getLastName()
            + "','"
            + appUserProfile.getPassword()
            + "','"
            + appUserProfile.getUserBrowsers()
            + "','"
            + appUserProfile.getLockInd()
            + "',"
            + appUserProfile.getLoginFailureCount()
            + ",'"
            + appUserProfile.getLastFailureAttemptTs()
            + "',"
            + appUserProfile.getPasswordexpirydays()
            + ",'"
            + appUserProfile.getEmailId()
            + "')";
    /*Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("his_unqUserId", appUserProfile.getUnqUserId());
    objectMap.put("his_lastName", appUserProfile.getLastName());
    objectMap.put("his_password", appUserProfile.getPassword());
    objectMap.put("his_userBrowsers", appUserProfile.getUserBrowsers());
    objectMap.put("lockInd", appUserProfile.getLockInd());
    objectMap.put("loginFailureCount", appUserProfile.getLoginFailureCount());
    objectMap.put("lastFailureAttemptTs", appUserProfile.getLastFailureAttemptTs());
    objectMap.put("his_passwordexpirydays", appUserProfile.getPasswordexpirydays());
    objectMap.put("his_emailId", appUserProfile.getEmailId());*/
    MySqlOperations.updateQuery(query, null);
  }

  public static void recreateSoftDeletedUser(ApplicationUser applicationUser)
      throws SystemException {

    String query =
        "update application_user set password = ? ,status = ? , firstName = ? ,lastName = ? where unqUserId =?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("password", applicationUser.getPassword());
    objectMap.put("status", applicationUser.getStatus());
    objectMap.put("firstName", applicationUser.getFirstName());
    objectMap.put("lastName", applicationUser.getLastName());
    objectMap.put("unqUserId", applicationUser.getUnqUserId());
    MySqlOperations.updateQuery(query, objectMap);
  }
}

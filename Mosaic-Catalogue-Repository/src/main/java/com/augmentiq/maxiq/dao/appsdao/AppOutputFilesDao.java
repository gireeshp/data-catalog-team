package com.augmentiq.maxiq.dao.appsdao;
/*package com.augmentiq.maxiq.core.dao.appsdao;

import java.util.LinkedHashMap;
import java.util.Map;

import com.augmentiq.maxiq.canvas.workFlow.domain.constants.ProcessConstants;
import com.augmentiq.maxiq.core.dao.configuration.exceoption.SystemException;
import com.augmentiq.maxiq.core.dao.mysql.MySqlOperations;
import com.augmentiq.maxiq.core.models.apps.domain.AppOutputFiles;
import com.augmentiq.maxiq.message.MessageConstants;

*//** Created by shivanand on 8/19/2015. *//*
public class AppOutputFilesDao {
  	private static final Logger logger = LoggerFactory.getLogger(AppOutputFilesDao.class);
   

  *//**
   * @param appOutputFiles
   * @throws SystemException
   *//*
  public static void insert(AppOutputFiles appOutputFiles) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> insert(AppOutputFiles appOutputFiles)");
    MySqlOperations.insert(appOutputFiles);
    // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << insert(AppOutputFiles appOutputFiles)");
  }

  *//**
   * @param appId
   * @param nodeId
   * @return
   * @throws SystemException
   *//*
  public static AppOutputFiles fetchAppOutputFile(Long appId, Long nodeId) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchAppOutputFile(Long appId, Long nodeId)");

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    if (appId != null) query.put(MessageConstants.APP_ID, appId);

    query.put(MessageConstants.NODE_ID, nodeId);

    AppOutputFiles appOutputFiles = MySqlOperations.scanOneForQuery(AppOutputFiles.class, query);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << fetchAppOutputFile(Long appId, Long nodeId)");
    return appOutputFiles;
  }

  *//**
   * @param appId
   * @param nodeId
   * @param key
   * @return
   * @throws SystemException
   *//*
  public static AppOutputFiles fetchAppOutputFile(Long appId, Long nodeId, String key)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchAppOutputFile(Long
    // appId, Long nodeId)");

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    if (appId != null) query.put(MessageConstants.APP_ID, appId);

    query.put(MessageConstants.NODE_ID, nodeId);
    query.put(MessageConstants.KEYNAME, key);

    AppOutputFiles appOutputFiles = MySqlOperations.scanOneForQuery(AppOutputFiles.class, query);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB + " << fetchAppOutputFile(Long
    // appId, Long nodeId)");
    return appOutputFiles;
  }

  *//**
   * baapJobInstId introduced to uniquely identify output of appNodes running in same context
   *
   * @param appId
   * @param nodeId
   * @param key
   * @param baapJobInstId
   * @return
   * @throws SystemException
   *//*
  public static AppOutputFiles fetchAppOutputFile(Long appId, Long nodeId, Long baapJobInstId)
      throws SystemException {

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    if (appId != null) query.put(MessageConstants.APP_ID, appId);

    query.put(MessageConstants.NODE_ID, nodeId);
    query.put(ProcessConstants.BAAPJOBINSTANCE_ID, baapJobInstId);

    AppOutputFiles appOutputFiles = MySqlOperations.scanOneForQuery(AppOutputFiles.class, query);
    if (null == appOutputFiles) {
      appOutputFiles = fetchAppOutputFile(appId, nodeId);
    }
    return appOutputFiles;
  }

  *//**
   * baapJobInstId introduced to uniquely identify output of appNodes running in same context
   *
   * @param appId
   * @param nodeId
   * @param key
   * @param baapJobInstId
   * @return
   * @throws SystemException
   *//*
  // workflowId is equivalent to baapJobInstId--Anand Rai
  public static AppOutputFiles fetchAppOutputFile(
      Long appId, Long nodeId, String key, Long baapJobInstId) throws SystemException {

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    if (appId != null) query.put(ProcessConstants.appId, appId);
    query.put(ProcessConstants.nodeId, nodeId);
    query.put(MessageConstants.KEYNAME, key);
    query.put(ProcessConstants.BAAPJOBINSTANCE_ID, baapJobInstId);

    AppOutputFiles appOutputFiles = MySqlOperations.scanOneForQuery(AppOutputFiles.class, query);
    if (null == appOutputFiles) {
      appOutputFiles = fetchAppOutputFile(appId, nodeId, key);
    }

    return appOutputFiles;
  }
}
*/
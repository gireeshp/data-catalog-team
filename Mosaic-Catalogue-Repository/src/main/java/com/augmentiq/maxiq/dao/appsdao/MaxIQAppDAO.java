package com.augmentiq.maxiq.dao.appsdao;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.models.apps.domain.Application;

/**
 * Changed for MAX-523 By Balkrushna Patil on 01-OCT-2016 Changed for MAX-626 By Ajinkya Marathe on
 * 23-DEC-2016
 */
public class MaxIQAppDAO {

  private static final Logger logger = LoggerFactory.getLogger(MaxIQAppDAO.class);
  
  public static Long getApplicationsCount(Long groupId) throws SystemException {
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB
	            + " >> getApplicationsCount()"
	            + ParamUtils.getString(groupId));

	    Map<String, Object> query = new LinkedHashMap<>();
	    query.put("groupId", groupId);
	    Long count = MySqlOperations.count(Application.class, query);

	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB + " << getApplicationsCount()" + ParamUtils.getString(count));
	    return count;
	  }/*

  @Deprecated
  public static Long getNewAppId() throws Exception {
    return Sequences.getNextId(Constants.MAXIQ_APPS);
  }

  @Deprecated
  public static Long getNewId() throws Exception {
    return Sequences.getNextId(Constants.APPVIEWINSTANCE);
  }

  public static void deleteApp(Long appId) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteApp()" + ParamUtils.getString(appId));
    Application application = new Application();
    application.setAppId(appId);
    MySqlOperations.deleteRow(application);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << deleteApp()" + ParamUtils.getString(appId));
  }

  public static void createNodeCopy(
      Long oldAppId, Long newAppId, String newAppName, Map<Long, Long> nodeIds) throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createNodeCopy()"
            + ParamUtils.getString(newAppName, oldAppId, newAppName, nodeIds));

    Map<String, Object> query = new LinkedHashMap<>();
    query.put(MessageConstants.APP_ID, oldAppId);

    List<NodeInfo> nodes = MySqlOperations.scan(NodeInfo.class, query);

    if (nodes != null) {
      for (NodeInfo nodeInfo : nodes) {
        if (nodeIds.containsKey(nodeInfo.getId())) {

          nodeInfo.setAppId(newAppId);
          nodeInfo.setAppName(newAppName);
          nodeInfo.setId(nodeIds.get(nodeInfo.getId()));

          MySqlOperations.insert(nodeInfo);
        }
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createNodeCopy()"
            + ParamUtils.getString(newAppName, oldAppId, newAppName, nodeIds));
  }

  public static void createProcessCopy(
      Long oldAppId,
      Long newAppId,
      String newAppName,
      Map<Long, Long> nodeIds,
      Map<String, String> oldNameNewNameMap,
      Map<Long, Long> oldDsIdNewDsIdMap)
      throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createProcessCopy()"
            + ParamUtils.getString(
                newAppName, oldAppId, newAppName, nodeIds, oldNameNewNameMap, oldDsIdNewDsIdMap));

    Set<Long> nodeIdSet = nodeIds.keySet();
    if (null != nodeIdSet && nodeIdSet.size() > 0) {
      for (Long nodeId : nodeIdSet) {
        WorkflowProcessInfo workflowProcess =
            GraphDao.getWorkflowProcessByAppIdAndNodeId(oldAppId, nodeId);
        if (null != workflowProcess) {
          getWorkflowProcessData(
              newAppId,
              newAppName,
              nodeIds,
              oldNameNewNameMap,
              nodeId,
              workflowProcess,
              oldDsIdNewDsIdMap);
        }

        MySqlOperations.insert(
            workflowProcess, HbaseUtility.getIdAnnotationForClass(WorkflowProcessInfo.class));
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createProcessCopy()"
            + ParamUtils.getString(
                newAppName, oldAppId, newAppName, nodeIds, oldNameNewNameMap, oldDsIdNewDsIdMap));
  }

  public static void getWorkflowProcessData(
      Long newAppId,
      String newAppName,
      Map<Long, Long> oldNodeIdNewNodeIdMap,
      Map<String, String> oldNameNewNameMap,
      Long oldNodeId,
      WorkflowProcessInfo workflowProcess,
      Map<Long, Long> oldDsIdNewDsIdMap)
      throws SystemException {

    workflowProcess.setAppId(newAppId);
    workflowProcess.setId(null);
    workflowProcess.setNodeId(oldNodeIdNewNodeIdMap.get(oldNodeId));

    String json = workflowProcess.getData();
    ProcessData processData = CanvasWorkFlowFactory.build(json);
    processData.setAppId(newAppId);
    processData.setAppName(newAppName);
    processData.setNodeId(oldNodeIdNewNodeIdMap.get(oldNodeId));

    setInputAndOutPut(oldNodeIdNewNodeIdMap, oldNameNewNameMap, processData, oldDsIdNewDsIdMap);

    workflowProcess.setData(ObjectSerializationHandler.toString(processData));
  }

  public static void setInputAndOutPut(
      Map<Long, Long> nodeIds,
      Map<String, String> oldNameNewNameMap,
      ProcessData processData,
      Map<Long, Long> oldDsIdNewDsIdMap)
      throws SystemException {

    Input input = processData.getInput();
    if (null != input) {
      List<DataFile> sources = input.getSources();
      if (null != sources && sources.size() > 0) {
        for (DataFile dataFile : sources) {
          setDataFilesAndFieldMappings(nodeIds, oldNameNewNameMap, dataFile, oldDsIdNewDsIdMap);
        }
      }
    }

    OutPut outPut = processData.getOutPut();
    if (null != outPut) {
      List<DataFile> sources = outPut.getSources();
      if (null != sources && sources.size() > 0) {
        for (DataFile dataFile : sources) {
          setDataFilesAndFieldMappings(nodeIds, oldNameNewNameMap, dataFile, oldDsIdNewDsIdMap);
        }
      }
    }
  }

  public static void setDataFilesAndFieldMappings(
      Map<Long, Long> nodeIds,
      Map<String, String> oldNameNewNameMap,
      DataFile dataFile,
      Map<Long, Long> oldDsIdNewDsIdMap)
      throws SystemException {

    String copyOfName = dataFile.getAlias();
    try {
      dataFile.setAlias(nodeIds.get(Long.parseLong(copyOfName)) + "");

    } catch (NumberFormatException e) {
      String string = oldNameNewNameMap.get(copyOfName);
      if (StringUtils.isNotBlank(string)) {
        dataFile.setAlias(string);
      } else {
        dataFile.setAlias(oldNameNewNameMap.get(copyOfName.toLowerCase()));
      }
    }

    dataFile.setNodeId(
        (null != nodeIds.get(dataFile.getNodeId()))
            ? nodeIds.get(dataFile.getNodeId())
            : dataFile.getNodeId());
    dataFile.setRefNodeId(
        (null != nodeIds.get(dataFile.getRefNodeId()))
            ? nodeIds.get(dataFile.getRefNodeId())
            : dataFile.getRefNodeId());
    dataFile.setRefDataSourceId(
        (null != oldDsIdNewDsIdMap.get(dataFile.getRefDataSourceId()))
            ? oldDsIdNewDsIdMap.get(dataFile.getRefDataSourceId())
            : dataFile.getRefDataSourceId());

    // Check for select , MapReduce and for other custom nodes
    String displayAliasName =
        StringUtils.isBlank(oldNameNewNameMap.get(dataFile.getDisplayAlias()))
            ? dataFile.getDisplayAlias()
            : StringUtils.isNotBlank(oldNameNewNameMap.get(dataFile.getDisplayAlias()))
                ? oldNameNewNameMap.get(dataFile.getDisplayAlias())
                : oldNameNewNameMap.get(dataFile.getDisplayAlias().toLowerCase());

    if (StringUtils.isBlank(dataFile.getAlias())) {
      dataFile.setAlias(copyOfName);
    }

    dataFile.setDisplayAlias(displayAliasName);

    
     * Alias & display alias shouldn't be blank. Would like to know if it
     * happens at all.
     
    // checkForBlank(dataFile.getAlias(), "alias");
    // checkForBlank(dataFile.getDisplayAlias(), "display alias");

    List<FieldMapping> fields = dataFile.getFields();
    for (FieldMapping fieldMapping : fields) {
      if (StringUtils.isNotBlank(fieldMapping.getSource())) {
        if (fieldMapping.getPos() < 0) continue;

        String sourceName =
            StringUtils.isNotBlank(oldNameNewNameMap.get(fieldMapping.getSource()))
                ? oldNameNewNameMap.get(fieldMapping.getSource())
                : oldNameNewNameMap.get(fieldMapping.getSource().toLowerCase());
        fieldMapping.setSource(sourceName);

        String sourceLebelName =
            StringUtils.isNotBlank(oldNameNewNameMap.get(fieldMapping.getSourceLabel()))
                ? oldNameNewNameMap.get(fieldMapping.getSourceLabel())
                : oldNameNewNameMap.get(fieldMapping.getSourceLabel().toLowerCase());
        fieldMapping.setSourceLabel(sourceLebelName);

        FieldMapping sourceRefField = fieldMapping.getSourceRefField();
        if (null != sourceRefField) {
          sourceName =
              StringUtils.isNotBlank(oldNameNewNameMap.get(sourceRefField.getSource()))
                  ? oldNameNewNameMap.get(sourceRefField.getSource())
                  : oldNameNewNameMap.get(sourceRefField.getSource().toLowerCase());
          sourceRefField.setSource(sourceName);

          sourceLebelName =
              StringUtils.isNotBlank(oldNameNewNameMap.get(sourceRefField.getSourceLabel()))
                  ? oldNameNewNameMap.get(sourceRefField.getSourceLabel())
                  : oldNameNewNameMap.get(sourceRefField.getSourceLabel().toLowerCase());
          sourceRefField.setSourceLabel(sourceLebelName);
        }
      }
    }
  }

  private static void checkForBlank(String checkMe, String fieldName) throws SystemException {
    if (StringUtils.isBlank(checkMe)) {
      throw new SystemException("Critical error! Blank " + fieldName + " found.");
    }
  }

  public static void createFileTypeDsCopyFromApp(
      Long oldAppId,
      Long newAppId,
      String oldAppName,
      String newAppName,
      String userId,
      Map<String, String> oldNameNewNameMap,
      Application application)
      throws SystemException, ConnectionException {
    List<AppCombiner> appCombiners = application.getAppCombiner();
    if (null != appCombiners && appCombiners.size() > 0) {
      for (AppCombiner appCombiner : appCombiners) {
        Transpose transpose = appCombiner.getTranspase();
        if (StringUtils.equalsIgnoreCase(appCombiner.getType(), "FILE")) {
          transpose.getCsv();
          GenericStoreDao.deleteConfigurationFromDataSourceName(
              newAppName + "_" + appCombiner.getName());
          DataSource dataSource =
              new TempDataSourceBso()
                  .createTempDataSource(
                      transpose.getCsv(),
                      transpose.getDelimeter(),
                      newAppId,
                      newAppName + "_" + appCombiner.getName(),
                      "",
                      application.getGroupId(),
                      application.getOwnerProjectId());
          transpose.setTempDsId(dataSource.getId());

          String dsName = "TEMP_DS_" + newAppId + "_" + appCombiner.getName();
          GenericStoreDao.deleteDataSourceInstanceFromDataSourceName(dsName);
          new TempDataSourceBso()
              .createDataSource(
                  userId,
                  newAppId,
                  dataSource,
                  application.getGroupId(),
                  application.getFlowType().name(),
                  application.getOwnerProjectId());

          Map<String, Object> query = new LinkedHashMap<String, Object>();
          query.put("dataSourceId", dataSource.getId());
          DataSourceInstance instance =
              MySqlOperations.scanOneForQuery(DataSourceInstance.class, query);
          if (null != instance) {
            instance.setFileTypeIndicator(Constants.TRUE);
            MySqlOperations.insert(
                instance, HbaseUtility.getIdAnnotationForClass(DataSourceInstance.class));
          }

          oldNameNewNameMap.put(
              oldAppName + "_" + appCombiner.getName(), dataSource.getDataSourceName());
          oldNameNewNameMap.put(appCombiner.getName(), dataSource.getDataSourceName());
          oldNameNewNameMap.put(
              appCombiner.getName().toLowerCase(), dataSource.getDataSourceName());
        }
      }
    }
  }

  *//**
   * This method will create copy of application
   * Modified for MAX-1787
   * 
   * @author Preeti Tripathi
   * @param oldAppId
   * @param newAppName
   * @param userId
   * @throws Exception
   *//*
  public static void createAppCopy(Long oldAppId, String newAppName, String userId)
      throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createClone()"
            + ParamUtils.getString(newAppName, oldAppId));

    Map<Long, Long> oldNodeIdNewNodeIdMap = new HashMap<Long, Long>();
    Map<String, String> oldNameNewNameMap = new HashMap<String, String>();
    Map<Long, Long> oldDsIdNewDsIdMap = new HashMap<Long, Long>();

    WorkflowGraph graphData = GraphDao.getGraphData(oldAppId);

    Application application = MaxIQAppService.getApplication(oldAppId);

    Long newAppId = createMaxiqAppCopy(oldAppId, newAppName, userId);

    createFileTypeDsCopyFromApp(
        oldAppId,
        newAppId,
        application.getAppName(),
        newAppName,
        userId,
        oldNameNewNameMap,
        application);

    if (graphData != null) {
      String nodeInfoStr = graphData.getNodeInfo();
      List<Map<String, Object>> nodeInfoList =
          (List<Map<String, Object>>) ObjectSerializationHandler.toObject(nodeInfoStr, List.class);
      Long nodeId = System.currentTimeMillis();

      for (Map<String, Object> nodeInfo : nodeInfoList) {
        
         * Loop through all nodes inside the flow canvas. Each node can
         * be: 1. External data source 2. Data source created 3. Nodes
         * from other flows - used as data source 4. Process nodes 5.
         * Flow imported inside the flow
         

        String nodeName = (String) nodeInfo.get(ProcessConstants.label);
        if (((String) nodeInfo.get(ProcessConstants.tyep))
            .equalsIgnoreCase(NodeTypeEnumTest.node_datasource.name())) {
          
           * It is a data source
           
          DataSourceInstance dataSourceInstance =
              DataSourceInstanceDao.getDataSourceInstance(
                  (String) nodeInfo.get(ProcessConstants.label));
          if (null == dataSourceInstance) {
            
             * If dataSource is of fileType then we have to set
             * there dataSource Ids
             *
             
            dataSourceInstance =
                DataSourceInstanceDao.getDataSourceInstance(
                    Long.parseLong(nodeInfo.get(ProcessConstants.connectingAppId) + ""));
            DataSource dataSource =
                DataRepositoryFetchWithChild.getDataSource(dataSourceInstance.getDataSourceId());
            DataSource clonedDataSource =
                DataRepositoryFetchWithChild.getDataSourceByName(
                    oldNameNewNameMap.get(dataSource.getDataSourceName()));

            oldDsIdNewDsIdMap.put(dataSource.getId(), clonedDataSource.getId());
            nodeInfo.put(ProcessConstants.connectingAppId, clonedDataSource.getId());

          } else if (null != dataSourceInstance
              && null != dataSourceInstance.getAppId()
              && dataSourceInstance.getAppId().equals(oldAppId)) {
             
            DataSourceInstance clonedDataSourceInstance = dataSourceInstance;
            
             * Means this is a data source created from inside the
             * flow. Need to create a copy of it too.
             * 
             * Once DS copy is created there will be two seperate instances of DS-
             * one in original flow and one in cloned flow.
             * 
             * After calling createAppDsCopy(), dataSourceInstance
             * object will have the old data source information and
             * clonedDataSourceInstance will have new data source information
             

            createAppDsCopy(newAppId, newAppName, nodeName, clonedDataSourceInstance);

            oldNameNewNameMap.put(nodeName, clonedDataSourceInstance.getDataSourceName());
            oldDsIdNewDsIdMap.put(
                Long.parseLong(nodeInfo.get(ProcessConstants.connectingAppId) + ""),
                clonedDataSourceInstance.getDataSourceId());
            nodeInfo.put(ProcessConstants.connectingAppId, clonedDataSourceInstance.getDataSourceId());

          } else {
            
             * This means it is an external data source. Just keep
             * name and id as is. Just create a new node id.
             
            oldNameNewNameMap.put(nodeName, nodeName);
            oldDsIdNewDsIdMap.put(
                Long.parseLong(nodeInfo.get(ProcessConstants.connectingAppId) + ""),
                Long.parseLong(nodeInfo.get(ProcessConstants.connectingAppId) + ""));
            nodeInfo.put(
                ProcessConstants.connectingAppId, nodeInfo.get(ProcessConstants.connectingAppId));
          }

          oldNodeIdNewNodeIdMap.put((Long) nodeInfo.get(MessageConstants.NODE_ID), nodeId);

        } else if (((String) nodeInfo.get(ProcessConstants.tyep))
            .equalsIgnoreCase(NodeTypeEnumTest.node_appsNode.name())) {
          
           * Node from other flow used as data source. Use as is. No
           * need to create new.
           
          oldNodeIdNewNodeIdMap.put(
              (Long) nodeInfo.get(MessageConstants.NODE_ID),
              (Long) nodeInfo.get(MessageConstants.NODE_ID));
          oldNameNewNameMap.put(nodeName, nodeName);

        } else {
          
           * This means, it is a process node
           
          oldNameNewNameMap.put(nodeName, nodeName);
          oldNodeIdNewNodeIdMap.put((Long) nodeInfo.get(MessageConstants.NODE_ID), nodeId);
        }

        nodeInfo.put(ProcessConstants.label, oldNameNewNameMap.get(nodeName));
        nodeInfo.put(
            MessageConstants.NODE_ID,
            oldNodeIdNewNodeIdMap.get(nodeInfo.get(MessageConstants.NODE_ID)));

        nodeId++;
      }

      graphData.setNodeInfo(ObjectSerializationHandler.toString(nodeInfoList));
    }

    createNodeCopy(oldAppId, newAppId, newAppName, oldNodeIdNewNodeIdMap);
    createProcessCopy(
        oldAppId,
        newAppId,
        newAppName,
        oldNodeIdNewNodeIdMap,
        oldNameNewNameMap,
        oldDsIdNewDsIdMap);

    if (graphData != null) {
      createGraphCopy(
          graphData,
          oldNodeIdNewNodeIdMap,
          oldNameNewNameMap,
          oldDsIdNewDsIdMap,
          newAppName,
          newAppId);
    }
  }

  public static Long createMaxiqAppCopy(Long oldAppId, String newAppName, String userId)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createMaxiqAppCopy()"
            + ParamUtils.getString(oldAppId, newAppName, userId));

    Application application = MaxIQAppService.getApplication(oldAppId);
    application.setAppId(null);
    application.setAppName(newAppName);
    application.setCreateSt(System.currentTimeMillis());
    application.setCreatedBy(userId);

    Long appId =
        MySqlOperations.insert(
            application, HbaseUtility.getIdAnnotationForClass(Application.class));

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> createMaxiqAppCopy()" + ParamUtils.getString(appId));
    return appId;
  }

  public static void createGraphCopy(
      WorkflowGraph graphData,
      Map<Long, Long> nodeIds,
      Map<String, String> oldNameNewNameMap,
      Map<Long, Long> dsIds,
      String newAppName,
      Long newAppId)
      throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createGraphCopy()"
            + ParamUtils.getString(newAppName, newAppName, nodeIds, oldNameNewNameMap));

    getUpdatedLinkInfo(graphData, nodeIds);
    Map<String, Object> canvasGraphData =
        (Map<String, Object>)
            ObjectSerializationHandler.toObject(graphData.getCanvas_graphData(), Map.class);
    List<NodeGraphInfo> graphInfos =
        ObjectSerializationHandler.toObjectList(
            ObjectSerializationHandler.toString(canvasGraphData.get(ProcessConstants.nodes)),
            NodeGraphInfo.class,
            new NodeGraphInfo());
    canvasGraphData.put(
        ProcessConstants.nodes,
        getCanvasGraphNodeInfo(
            graphData, graphInfos, nodeIds, dsIds, oldNameNewNameMap, newAppName));
    canvasGraphData.put(
        ProcessConstants.connections,
        getCanvasGraphConnections(graphData, nodeIds, dsIds, oldNameNewNameMap, newAppName));
    graphData.setCanvas_graphData(ObjectSerializationHandler.toString(canvasGraphData));
    graphData.setId(newAppId);

    MySqlOperations.insert(graphData);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createGraphCopy()"
            + ParamUtils.getString(newAppName, newAppName, nodeIds, oldNameNewNameMap));
  }

  public static List<Map<String, Map<String, Object>>> getCanvasGraphConnections(
      WorkflowGraph graphData,
      Map<Long, Long> nodeIds,
      Map<Long, Long> dsIds,
      Map<String, String> oldNameNewNameMap,
      String newAppName)
      throws JsonParseException, JsonMappingException, IOException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getCanvasGraphNodeInfo()"
            + ParamUtils.getString(graphData, nodeIds, dsIds, oldNameNewNameMap, newAppName));

    Map<String, Object> canvasGraphData =
        (Map<String, Object>)
            ObjectSerializationHandler.toObject(graphData.getCanvas_graphData(), Map.class);
    List<Map<String, Map<String, Object>>> connections =
        (List<Map<String, Map<String, Object>>>)
            ObjectSerializationHandler.toObject(
                ObjectSerializationHandler.toString(
                    canvasGraphData.get(ProcessConstants.connections)),
                List.class);

    for (Map<String, Map<String, Object>> map : connections) {
      Map<String, Object> sources = map.get(ProcessConstants.source);
      Map<String, Object> destinations = map.get(ProcessConstants.dest);

      sources.put(
          ProcessConstants.nodeID,
          nodeIds.get(Long.parseLong(sources.get(ProcessConstants.nodeID) + "")));
      destinations.put(
          ProcessConstants.nodeID, nodeIds.get((Long) destinations.get(ProcessConstants.nodeID)));
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getCanvasGraphNodeInfo()"
            + ParamUtils.getString(graphData, nodeIds, dsIds, oldNameNewNameMap, newAppName));

    return connections;
  }

  public static List<NodeGraphInfo> getCanvasGraphNodeInfo(
      WorkflowGraph graphData,
      List<NodeGraphInfo> graphInfos,
      Map<Long, Long> nodeIds,
      Map<Long, Long> dsIds,
      Map<String, String> oldNameNewNameMap,
      String newAppName) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getCanvasGraphNodeInfo()"
            + ParamUtils.getString(
                graphData, graphInfos, nodeIds, dsIds, oldNameNewNameMap, newAppName));

    for (NodeGraphInfo nodeGraphInfo : graphInfos) {
      nodeGraphInfo.setAppName(newAppName);
      nodeGraphInfo.setId(nodeIds.get(nodeGraphInfo.getId()));
      nodeGraphInfo.setName(oldNameNewNameMap.get(nodeGraphInfo.getName()));
      if (nodeGraphInfo.getNodeType().equalsIgnoreCase(NodeTypeEnumTest.node_datasource.name())) {
        nodeGraphInfo.setDatasourceId(
            dsIds.get(Long.parseLong(nodeGraphInfo.getDatasourceId())) + "");
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getCanvasGraphNodeInfo()"
            + ParamUtils.getString(
                graphData, graphInfos, nodeIds, dsIds, oldNameNewNameMap, newAppName));

    return graphInfos;
  }

  public static void getUpdatedLinkInfo(WorkflowGraph graphData, Map<Long, Long> nodeIds) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getUpdatedLinkInfo()"
            + ParamUtils.getString(graphData, nodeIds));

    String linkInfo = graphData.getLinkInfo();
    String newLinkInfo = "";
    String[] linkInfoArrays = StringUtils.splitPreserveAllTokens(linkInfo, CHAR_CONSTANTS.COMMA);
    for (String singleLinkInfo : linkInfoArrays) {

      String singleLinkInfoTemp = StringUtils.replace(singleLinkInfo, " ", "");
      String delimeter = "";

      String newSingleLinkInfo = "";
      if (singleLinkInfoTemp.indexOf("->") != -1) {
        delimeter = " -> ";
        newSingleLinkInfo = StringUtils.replace(singleLinkInfoTemp, "->", " ");
        newLinkInfo =
            concatAndCreateTheLinkInfo(newSingleLinkInfo, newLinkInfo, delimeter, nodeIds);
      } else if (singleLinkInfoTemp.indexOf("<-") != -1) {
        delimeter = " <- ";
        newSingleLinkInfo = StringUtils.replace(singleLinkInfoTemp, "<-", " ");
        newLinkInfo =
            concatAndCreateTheLinkInfo(newSingleLinkInfo, newLinkInfo, delimeter, nodeIds);
      } else {
        newLinkInfo += ",";
      }
    }

    graphData.setLinkInfo(newLinkInfo);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getUpdatedLinkInfo()"
            + ParamUtils.getString(graphData, nodeIds));
  }

  public static String concatAndCreateTheLinkInfo(
      String singleLink, String newLinkInfo, String delimeter, Map<Long, Long> nodeIds) {

    String[] nodeIdArray = StringUtils.split(singleLink, " ");
    String newInfo = "";
    if (nodeIdArray.length > 1) {
      newInfo =
          nodeIds.get(Long.parseLong(nodeIdArray[0]))
              + delimeter
              + nodeIdArray[1]
              + delimeter
              + nodeIds.get(Long.parseLong(nodeIdArray[2]));
    } else if (StringUtils.isBlank(StringUtils.trim(singleLink))) {
      // blank string after last comma. This is okay.
      return "";
    } else {
      new SystemException("Node connections are invalid");
    }

    if (StringUtils.isBlank(newLinkInfo)) {
      newLinkInfo = newLinkInfo + newInfo;
    } else {
      newLinkInfo = newLinkInfo + "," + newInfo;
    }

    return newLinkInfo;
  }
  
  *//**
   * Modified for MAX-1787
   * 
   * @author Preeti Tripathi
   * @param newAppId
   * @param newAppName
   * @param nodeName
   * @param dataSourceInstance
   * @throws SystemException
   * @throws ConnectionException
   *//*
  public static void createAppDsCopy(
      Long newAppId, String newAppName, String nodeName, DataSourceInstance dataSourceInstance)
      throws SystemException, ConnectionException {

    DataSource dataSource =
        DataRepositoryFetchWithChild.getDataSource(dataSourceInstance.getDataSourceId());
    
    
     * Set default id as null,so new data source instance should be created in DB
     
    dataSource.setId(null);

    String newDsName = getNextPossibleDsName(nodeName + "_" + newAppId);

    
     * New data source instance and data source are ready. Make entry into
     * DB
     
    
    dataSource.setId(DataRepositoryDao.insertDataSource(dataSource));

    if (null != dataSource) {
      dataSource.setId(dataSource.getId());
      dataSource.setDataSourceName(newDsName);

      if (null != dataSource.getFileDataIngesterDetails()) {
        dataSource.getFileDataIngesterDetails().setDataSourceId(dataSource.getId());
      }
    }

    DataRepositoryDao.insertDataSource(dataSource);

    dataSourceInstance =
        (dataSourceInstance == null ? new DataSourceInstance() : dataSourceInstance);
    // Modifying id and name & other parameters
    dataSourceInstance.setDataSourceId(dataSource.getId());
    dataSourceInstance.setDataSourceName(newDsName);
    dataSourceInstance.setAppId(newAppId);
    dataSourceInstance.setLastRunBy("");
    dataSourceInstance.setSize(0l);
    dataSourceInstance.setTotalRecords(0);
    dataSourceInstance.setUpdatedDate(null);
    dataSourceInstance.setCreatedDate(new Date());
    dataSourceInstance.setLatestFilepath("");

    DataSourceInstanceDao.insertDataSourceInstance(dataSourceInstance);
  }

  private static String getNextPossibleDsName(String dsName) throws SystemException {

    String newDsName = dsName;
    int i = 1;
    while (true) {
      Boolean existingStatus = GenericStoreDao.isDataSourceExist(newDsName);

      if (existingStatus) {
        newDsName = dsName + "_" + i;
      } else {
        return newDsName;
      }
    }
  }

  public static void deleteNodes(Long appId) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteNodes()" + ParamUtils.getString(appId));

    NodeInfo nodeInfo = new NodeInfo();
    Map<String, Object> query = new LinkedHashMap<>();
    nodeInfo.setAppId(appId);
    query.put("appId", appId);

    MySqlOperations.deleteRecords(nodeInfo, query);

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << deleteNodes()" + ParamUtils.getString(appId));
  }

  public static void deleteGraph(Long appId) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteGraph()" + ParamUtils.getString(appId));

    WorkflowGraph workflowGraph = new WorkflowGraph();
    Map<String, Object> query = new LinkedHashMap<>();
    workflowGraph.setId(appId);
    query.put(ProcessConstants.id, appId);

    MySqlOperations.deleteRecords(workflowGraph, query);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << deleteGraph()" + ParamUtils.getString(appId));
  }

  public static Long getMaxOfBaap(Long componentId, String componentType) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getMaxOfBaap()"
            + ParamUtils.getString(componentId, componentType));
    Long value = null;
    try {
      Connection connection = MysqlConnection.getConnection();
      Statement statement = connection.createStatement();
      String query = null;
      if (StringUtils.equals(componentType, Constants.APP)) {
        query =
            "select max(baapId) from  (select cast(baapJobInsId as UNSIGNED) as baapId from dataParamInstance where appId = "
                + componentId
                + ") as dummy";
      } else if (StringUtils.equals(componentType, Constants.DS)) {
        query =
            "select max(baapId) from  (select cast(baapJobInsId as UNSIGNED) as baapId from dataParamInstance where dsId = "
                + componentId
                + ") as dummy";
      }
      ResultSet resultSet = statement.executeQuery(query);
      while (resultSet.next()) {
        value = resultSet.getLong(1);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getMaxOfBaap()" + ParamUtils.getString(value));
    return value;
  }

  public static void deleteProcess(Long appId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> deleteProcess()" + ParamUtils.getString(appId));

    WorkflowProcessInfo workflowGraph = new WorkflowProcessInfo();
    Map<String, Object> query = new LinkedHashMap<>();
    workflowGraph.setAppId(appId);
    query.put("appId", appId);

    MySqlOperations.deleteRecords(workflowGraph, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << deleteProcess()" + ParamUtils.getString(appId));
  }

  public static Long insertApp(Application application) throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> insertApp()" + ParamUtils.getString(application));
    Long applicationId = 0L;
    if (null != application.getAppId() && application.getAppId() > 0l) {
      applicationId = application.getAppId();
    } else {
      applicationId = null;
    }

    application.setAppId(applicationId);

    applicationId =
        MySqlOperations.insert(
            application, HbaseUtility.getIdAnnotationForClass(Application.class));

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << insertApp()" + ParamUtils.getString(applicationId));

    return applicationId;
  }

  public static Long saveInputParam(InputParam inputParam) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> saveInputParam()" + ParamUtils.getString(inputParam));

    inputParam.setId(null);

    MySqlOperations.insert(inputParam, HbaseUtility.getIdAnnotationForClass(InputParam.class));

    Long id = inputParam.getId();

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << saveInputParam()" + ParamUtils.getString(id));
    return id;
  }

  public static Application getApplication(String appName) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getApplication()" + ParamUtils.getString(appName));

    Map<String, Object> query = new LinkedHashMap<>();
    query.put(ApplicationConstants.appName, appName);
    Application applicationObj = getApplicationObj(query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getApplication()"
            + ParamUtils.getString(applicationObj));
    return applicationObj;
  }

  public static List<InputParameterValues> getLastRunParam(Long componentId, String componentType)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getLastRunParam()" + ParamUtils.getString(componentId));

    Map<String, Object> query = new LinkedHashMap<>();
    if (StringUtils.equals(componentType, Constants.APP)) {
      query.put("appId", componentId);
    } else if (StringUtils.equals(componentType, Constants.DS)) {
      query.put("dsId", componentId);
    }

    query.put("baapJobInsId", getMaxOfBaap(componentId, componentType));
    List<InputParameterValues> scan = MySqlOperations.scan(InputParameterValues.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getLastRunParam()" + ParamUtils.getString(scan));
    return scan;
  }

  *//**
   * @param componentId
   * @param componentType
   * @return
   * @throws SystemException
   *//*
  public static List<InputParameterValues> getLastSucceedRunParam(
      Long componentId, String componentType) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getLastSucceedRunParam()"
            + ParamUtils.getString(componentId));
    List<InputParameterValues> scannedInputParameterValuesList = null;
    WorkflowInstance lastWorkflowInstanceObjectByStatusByComponentId =
        JobInstanceBso.getLastJobinstanceObjectByStatusByComponentId(
            componentId, HadoopJobState.SUCCEEDED.toString());
    if (null != lastWorkflowInstanceObjectByStatusByComponentId) {
      Map<String, Object> query = new LinkedHashMap<>();
      if (StringUtils.equals(componentType, Constants.APP)) {
        query.put("appId", componentId);
      } else if (StringUtils.equals(componentType, Constants.DS)) {
        query.put("dsId", componentId);
      }

      query.put("baapJobInsId", lastWorkflowInstanceObjectByStatusByComponentId.getWorkflowId());
      scannedInputParameterValuesList = MySqlOperations.scan(InputParameterValues.class, query);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getLastRunParam()"
            + ParamUtils.getString(scannedInputParameterValuesList));
    return scannedInputParameterValuesList;
  }

  public static Application getApplication(Long appId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getApplication()" + ParamUtils.getString(appId));

    Map<String, Object> query = new LinkedHashMap<>();
    query.put(ApplicationConstants.appId, appId);
    Application applicationObj = getApplicationObj(query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getApplication()"
            + ParamUtils.getString(applicationObj));
    return applicationObj;
  }

  public static List<JarPaths> getJarPaths(String keyOf) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getJarPaths()" + ParamUtils.getString(keyOf));

    Map<String, Object> query = new LinkedHashMap<>();
    query.put(ApplicationConstants.keyOf, keyOf);
    List<JarPaths> jarPathsDetails = getJarPathsDetails(query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getJarPaths()"
            + ParamUtils.getString(jarPathsDetails));
    return jarPathsDetails;
  }

  public static List<Application> getApplications(String flowType, Long projectId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getApplications()" + ParamUtils.getString(projectId));

    String sql =
        "select apps.appName, apps.statusEnum, apps.appId, apps.userId, max(inst.start_at) lastRun, "
            + " apps.class, apps.createSt,"
            + " apps.description, apps.appCombiner, apps.groupId, apps.scheduled, "
            + " apps.class, apps.createSt,"
            + " apps.description, apps.appCombiner, apps.groupId, apps.scheduled"
            + " ,CONCAT(user.firstName,' ',user.lastName) as createdBy, apps.flowType, apps.pollInterval, apps.ownerProjectId, apps.runningStatus "
            + " from maxiq_apps apps"
            + " left outer join m_track_workflowInstance inst"
            + " on (apps.appId = inst.componentId)"
            + " left outer join application_user user"
            + " on (apps.createdBy = user.unqUserId)"
            + " where apps.flowType = ? ";

    if (0l < projectId) {
      sql = sql + " and apps.ownerProjectId='" + projectId + "' ";
    }

    sql =
        sql
            + " group by apps.appName, apps.statusEnum, apps.appId, apps.createdBy, apps.userId, "
            + " apps.class, apps.createSt, apps.description, apps.appCombiner, apps.groupId, apps.flowType, apps.ownerProjectId"
            + " order by apps.createSt desc";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("apps.flowType", flowType);
    List<Application> applications =
        MySqlOperations.scanWithSqlQuery(Application.class, sql, objectMap);
    if (applications != null && applications.size() > 0) {
      applications.removeAll(Collections.singleton(null));
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getApplications()"
            + ParamUtils.getString(applications));
    return applications;
  }

  *//**
   * @param flowType
   * @param projectId
   * @throws SystemException
   *//*
  public static List<Application> getAllApplicationByProject(Long projectId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllApplicationByProject()"
            + ParamUtils.getString(projectId));
    String sql = "select * from maxiq_apps where ownerProjectId=?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("ownerProjectId", projectId);
    List<Application> applications =
        MySqlOperations.scanWithSqlQuery(Application.class, sql, objectMap);
    if (applications != null && applications.size() > 0) {
      applications.removeAll(Collections.singleton(null));
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getApplications()"
            + ParamUtils.getString(applications));
    return applications;
  }

  public static List<TreeDomain> getFlows(String flowType, Long projectId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getFlows()"
            + ParamUtils.getString(flowType, projectId));

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();

    String sql =
        "select apps.appId as id, apps.appName as label "
            + " from maxiq_apps apps"
            + " where apps.flowType =?";

    objectMap.put("apps.flowType", flowType);
    if (0l < projectId) {
      sql = sql + " and apps.ownerProjectId=?";
      objectMap.put("ownerProjectId", projectId);
    }

    sql =
        sql
            + " group by apps.appName, apps.statusEnum, apps.appId, apps.createdBy, apps.userId, "
            + " apps.class, apps.createSt, apps.description, apps.appCombiner, apps.groupId, apps.flowType, apps.ownerProjectId"
            + " order by apps.createSt desc";

    List<TreeDomain> exploreTreeHighPerformanceBeanList =
        MySqlOperations.scanWithSqlQuery(TreeDomain.class, sql, objectMap);
    if (exploreTreeHighPerformanceBeanList != null
        && exploreTreeHighPerformanceBeanList.size() > 0) {
      exploreTreeHighPerformanceBeanList.removeAll(Collections.singleton(null));
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getFlows()"
            + ParamUtils.getString(exploreTreeHighPerformanceBeanList));
    return exploreTreeHighPerformanceBeanList;
  }

  public static List<Application> getLatestUpdatedFlowListByGroupId(Long groupId)
      throws SystemException {
    String query =
        "select * from (select app.appName, app.flowType, app.statusEnum,app.appId,app.createdBy,app.userId,app.lastRun,app.class, app.scheduled,"
            + " (Case when app.createSt > graph.lastUpdatedTime then app.createSt else graph.updatedTime end) as createSt, "
            + "app.description,app.appCombiner,app.groupId, app.ownerProjectId from maxiq_apps app join "
            + "(select *, (case when updatedTime = 'null' then 0 else updatedTime end) as lastUpdatedTime from workflow_graph) graph on "
            + "(app.appId = graph.id)) as appTable  where groupId=? order by createSt desc limit 10;";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("groupId", groupId);
    List<Application> flowListForUser =
        MySqlOperations.scanWithSqlQuery(Application.class, query, objectMap);
    return flowListForUser;
  }

  public static List<JobInstance> getApplicationInstances(Long appId, String type)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getApplicationInstances()"
            + ParamUtils.getString(appId, type));

    String query =
        "select * from job_instances where typeId=? and type=? order by "
            + Constants.JOB_INSTANCE_ID
            + " desc";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("typeId", appId);
    objectMap.put("type", type);
    List<JobInstance> jobInstnaces =
        MySqlOperations.scanWithSqlQuery(JobInstance.class, query, objectMap);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getApplicationInstances()"
            + ParamUtils.getString(jobInstnaces));
    return jobInstnaces;
  }

  public static List<JobInstance> getApplicationInstances(String appName) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getApplicationInstances()"
            + ParamUtils.getString(appName));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("jobName", ApplicationConstants.appPrefix + appName);

    List<JobInstance> jobInstnaces = MySqlOperations.scanForQuery(JobInstance.class, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getApplicationInstances()"
            + ParamUtils.getString(jobInstnaces));

    return jobInstnaces;
  }

  public static List<JobInstance> getJobInstByTypeAndTopic(Long typeId, String topic)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getJobInstByTypeAndTopic()"
            + ParamUtils.getString(typeId, topic));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("typeId", typeId);
    query.put("topic", topic);

    List<JobInstance> jobInstnaces = MySqlOperations.scanForQuery(JobInstance.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getJobInstByTypeAndTopic()"
            + ParamUtils.getString(jobInstnaces));
    return jobInstnaces;
  }

  public static List<JobInstance> getJobInstByTypeIdAndType(Long typeId, String type)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getJobInstByTypeIdAndType()"
            + ParamUtils.getString(typeId, type));

    List<JobInstance> list = null;
    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet =
            statement.executeQuery(
                "select * from job_instances where type like '%node_%' and baapJobInstId is not null and baapJobInstId !='0' and typeId='"
                    + typeId
                    + "' order by "
                    + Constants.JOB_INSTANCE_ID
                    + " desc")) {

      list = new ArrayList<>();

      while (resultSet.next()) {

        list.add(HbaseUtility.buildObject(JobInstance.class, resultSet));
      }

    } catch (SQLException | ClassNotFoundException | IOException e) {

      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getJobInstByTypeIdAndType()"
            + ParamUtils.getString(list));
    return list;
  }

  public static List<JobInstance> getApplicationInstances(Long appId) throws SystemException {

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("drId", appId);

    List<JobInstance> jobInstnaces = MySqlOperations.scanForQuery(JobInstance.class, query);

    return jobInstnaces;
  }

  public static Application getApplicationObj(Map<String, Object> query) throws SystemException {

    Application application = MySqlOperations.scanOneForQuery(Application.class, query);

    return application;
  }

  public static List<JarPaths> getJarPathsDetails(Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> getJarPathsDetails()" + ParamUtils.getString(query));

    List<JarPaths> jarPaths = MySqlOperations.scan(JarPaths.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getJarPathsDetails()"
            + ParamUtils.getString(jarPaths));
    return jarPaths;
  }

  public static List<AppOutputFiles> getFilePath(Long appId) throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getFilePath()" + ParamUtils.getString(appId));
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(ProcessConstants.appId, appId);

    List<AppOutputFiles> appOutputFiles = MySqlOperations.scan(AppOutputFiles.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getFilePath()" + ParamUtils.getString(appOutputFiles));
    return appOutputFiles;
  }

  public static List<AppOutputFiles> getAppComponentParamMapping(Long appId, Long componentId)
      throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getFilePath(Long appId)");
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(ProcessConstants.appId, appId);
    query.put(ProcessConstants.COMPONENT_ID, componentId);

    List<AppOutputFiles> appOutputFiles = MySqlOperations.scan(AppOutputFiles.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getFilePath()" + ParamUtils.getString(appOutputFiles));
    return appOutputFiles;
  }

  public static boolean getcheckAppNameIsAlreadyExist(String appName) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getcheckAppNameIsAlreadyExist()"
            + ParamUtils.getString(appName));
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(ProcessConstants.appName, appName);
    Long count = MySqlOperations.count(Application.class, query);
    if (count != null && count > 0) {
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << getcheckAppNameIsAlreadyExist()"
              + ParamUtils.getString(true));
      return true;
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getcheckAppNameIsAlreadyExist()"
            + ParamUtils.getString(false));
    return false;
  }

 

  public static void updateAppLastRun(Long appId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> updateAppLastRun()" + ParamUtils.getString(appId));

    Application app = getApplication(appId);
    app.setLastRun(System.currentTimeMillis());

    MySqlOperations.insert(app);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << updateAppLastRun()" + ParamUtils.getString(appId));
  }

  public static String getLatestPathForApp(Long appId, Long nodeId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getLatestPathForApp()" + ParamUtils.getString(appId));

    Long latestAppJobId = JobInstanceDao.getLatestAppJobId(appId);
    if (latestAppJobId != null && latestAppJobId > 0) {
      String path =
          GlobalParams.MAXIQ_DATA_PATH
              + "/apps/"
              + appId
              + "/"
              + latestAppJobId
              + "/"
              + nodeId
              + "/";
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " << getLatestPathForApp()" + ParamUtils.getString(path));
      return path;
    }

    ExceptionsMessanger.throwException(
        new SystemException(), "ERR_042", getApplication(appId).getAppName());
    return null;
  }

  public static void savePublishStatus(String appName) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> savePublishStatus()" + ParamUtils.getString(appName));

    Application application = MaxIQAppService.getApplication(appName);
    application.setStatusEnum(AppStatusEnum.active);

    MySqlOperations.insert(application);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << savePublishStatus()" + ParamUtils.getString(appName));
  }

  public static void deleteAppGlobal(AppSettingInstance appSettingInstance, Boolean delete)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> deleteAppGlobal()"
            + ParamUtils.getString(appSettingInstance, delete));
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("componantId", appSettingInstance.getComponantId());
    query.put("appId", appSettingInstance.getAppId());
    query.put("type", appSettingInstance.getType());
    MySqlOperations.deleteRecords(appSettingInstance, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << deleteAppGlobal()"
            + ParamUtils.getString(appSettingInstance, delete));
  }

  public static Long saveAppGlobal(AppSettingInstance appSettingInstance, Boolean delete)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> saveAppGlobal()"
            + ParamUtils.getString(appSettingInstance, delete));

    Long appGlobalId =
        MySqlOperations.insert(
            appSettingInstance, HbaseUtility.getIdAnnotationForClass(AppSettingInstance.class));
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << saveAppGlobal()"
            + ParamUtils.getString(appSettingInstance));

    return appGlobalId;
  }

  public static void setAppSatus(String appName) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> setAppSatus()" + ParamUtils.getString(appName));

    Application application = MaxIQAppService.getApplication(appName);

    application.setStatusEnum(AppStatusEnum.inactive);

    MySqlOperations.insert(application);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << setAppSatus()" + ParamUtils.getString(appName));
  }

  public static List<CustomComponentInstance> getCustComp(
      CustomComponentEnum componentType, Long projectId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getCustComp()"
            + ParamUtils.getString(componentType, projectId));
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(GENERAL_CONSTANTS.COMPONENT_TYPE, componentType);
    query.put(GENERAL_CONSTANTS.OWNER_PROJECT_ID, projectId);

    List<CustomComponentInstance> customComponentInstance =
        MySqlOperations.scan(CustomComponentInstance.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getCustComp()"
            + ParamUtils.getString(customComponentInstance));

    return customComponentInstance;
  }

  public static NodeInfo getNodeInfo(Map<String, String> nodeDescriptionData)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getNodeInfo()"
            + ParamUtils.getString(nodeDescriptionData));
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(
        MessageConstants.NODE_INFO_ID, nodeDescriptionData.get(MessageConstants.NODE_INFO_ID));

    NodeInfo nodeInfo = MySqlOperations.scanOneForQuery(NodeInfo.class, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getNodeInfo()" + ParamUtils.getString(nodeInfo));
    return nodeInfo;
  }

  public static boolean getDatasourceRunStatus(Long dsId, Long appId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getDatasourceRunStatus()"
            + ParamUtils.getString(dsId, appId));
    logger.info("AppId == " + appId + " ::: DsId == " + dsId);
    boolean refresh = false;
    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet =
            statement.executeQuery(
                "select * from app_global_param where appId = '"
                    + appId
                    + "' and componantId = '"
                    + dsId
                    + "'")) {

      DataSourceInstance datasourceInstance =
          DataSourceInstanceDao.getDataSourceInstanceForApp(appId, dsId);

      boolean appLevelDataSource = false;

      if (null != datasourceInstance
          && null != datasourceInstance.getDataSourceType()
          && !(datasourceInstance
              .getDataSourceType()
              .equalsIgnoreCase(InputParamTypes.RCP.name()))) {
        appLevelDataSource = true;
      }

      DataSource dataSource = DataRepositoryDao.getDataSource(dsId);
      if (dataSource != null && FileStoreObject.SOURCE == dataSource.getFileStoreObject()) {
        return false; //TODO navinSourceChanges
      }
      if (resultSet.next()) {
        refresh = true;
      } else refresh = false;
      if (!refresh && appLevelDataSource) {
        if (datasourceInstance.getFlowType().equals(FlowType.STREAM)) {
          return refresh;
        }
        ;
        refresh = !refresh;
      }

    } catch (SQLException e) {
      logger.error(e.getMessage(), e);
      throw e;
    }
    return refresh;
  }

  public static List<AppSettingInstance> getDSAppParam(Long appId, Long dataSourceId, String type)
      throws SystemException {
    // TODO Auto-generated method stub
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("componantId", dataSourceId);
    query.put("appId", appId);
    query.put("type", type);

    List<AppSettingInstance> appSettingInstance =
        MySqlOperations.scan(AppSettingInstance.class, query);
    return appSettingInstance;
  }

  public static void saveNodeStaus(Long appId, Long nodeId, String nodeStatus)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> saveNodeStaus() "
            + ParamUtils.getString(appId, nodeId, nodeStatus));
    WorkflowProcessInfo workflowProcessInfo = getWorkflowProcessInfo(appId, nodeId);
    if (null != workflowProcessInfo) {
      workflowProcessInfo.setNodeStatus(nodeStatus);
      Map<String, Object> query = new HashMap<String, Object>();
      query.put("appId", appId);
      query.put("nodeId", nodeId);
      MySqlOperations.insert(workflowProcessInfo);
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << saveNodeStaus()");
  }

  public static WorkflowProcessInfo getWorkflowProcessInfo(Long appId, Long nodeId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getWorkflowProcessInfo() "
            + ParamUtils.getString(appId, nodeId));
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(ProcessConstants.appId, appId);
    query.put(ProcessConstants.nodeId, nodeId);
    WorkflowProcessInfo workflowProcessInfo =
        MySqlOperations.scanOneForQuery(WorkflowProcessInfo.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getWorkflowProcessInfo() "
            + ParamUtils.getString(workflowProcessInfo));
    return workflowProcessInfo;
  }

  public static List<JarPaths> getMaxiqCommons(String groupName, String keyOf)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getMaxiqCommons()"
            + ParamUtils.getString(groupName, keyOf));

    Map<String, Object> query = new LinkedHashMap<>();
    query.put(ApplicationConstants.groupName, groupName);
    query.put(ApplicationConstants.status, "ACTIVE");

    if (keyOf != null && !keyOf.equalsIgnoreCase(null) && !keyOf.equalsIgnoreCase("null")) {
      query.put(ApplicationConstants.keyOf, keyOf);
    }
    List<JarPaths> jarPathsDetails = getJarPathsDetails(query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getMaxiqCommons()"
            + ParamUtils.getString(jarPathsDetails));
    return jarPathsDetails;
  }

  *//**
   * @param
   * @return List of all applications for Global Search
   * @throws SystemException
   *//*
  public static List<Application> getApplications() throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getApplications()");
    Map<String, Object> flowQueryMap = new LinkedHashMap<String, Object>();
    flowQueryMap.put(
        QueryConstants.GlobalSearch.IS_MODIFIED, QueryConstants.GlobalSearch.IS_MODIFIED_TRUE);

    List<Application> applications = MySqlOperations.scan(Application.class, flowQueryMap);
    if (applications != null) {
      ListIterator<Application> lstIterator = applications.listIterator();
      while (lstIterator.hasNext()) {
        Application app = lstIterator.next();
        if (app != null) {
          if (StringUtils.isNumeric(app.getCreatedBy())) {
            List<Map<String, Object>> userList =
                UserManagementDao.getUserFirstAndLastNameById(Long.valueOf(app.getCreatedBy()));
            if (userList.size() > 0) {
              for (Map<String, Object> ma : userList) {
                String fname = null, lname = null;
                for (Entry<String, Object> ent : ma.entrySet()) {
                  if ("firstName".equalsIgnoreCase(ent.getKey())) {
                    fname = (String) ent.getValue();
                  }

                  if ("lastName".equalsIgnoreCase(ent.getKey())) {
                    lname = (String) ent.getValue();
                  }
                }
                app.setCreatedBy(fname + " " + lname);
              }
            } else {
              app.setCreatedBy("NA");
            }
          }
        }
        lstIterator.set(app);
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getApplications()"
            + ParamUtils.getString(applications));
    return applications;
  }

  public static List<ScheduleApplication> getScheduleJobs(Long applicationId, Long groupId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getScheduleJobs()"
            + ParamUtils.getString(applicationId, groupId));

    String sql =
        "select schedApps.id, schedApps.appId, schedApps.cronExpression, schedApps.oozieJobId, schedApps.groupId"
            + ",CONCAT(user.firstName,' ',user.lastName) as createdBy"
            + " from scheduled_application_details schedApps"
            + " left outer join application_user user"
            + " on (schedApps.createdBy = user.unqUserId)"
            + " where schedApps.groupId =? and schedApps.appId= ?";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("schedApps.groupId", groupId);
    objectMap.put("schedApps.appId", applicationId);
    List<ScheduleApplication> scheduleApplicationList =
        MySqlOperations.scanWithSqlQuery(ScheduleApplication.class, sql, objectMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getScheduleJobs()"
            + ParamUtils.getString(scheduleApplicationList));
    return scheduleApplicationList;
  }

  public static List<Application> getSchedApplications(Long projectId, String schedJobsFlag)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getScheduleJobs()"
            + ParamUtils.getString(schedJobsFlag, projectId));
    String sql =
        "select apps.appName, apps.statusEnum, apps.appId, apps.userId, max(inst.start_at) lastRun, apps.class, apps.createSt, apps.description, apps.appCombiner, apps.groupId,apps.scheduled"
            + ",CONCAT(user.firstName,' ',user.lastName) as createdBy, apps.ownerProjectId from maxiq_apps apps"
            + " left outer join m_track_workflowInstance inst"
            + " on (apps.appId = inst.componentId)"
            + " left outer join application_user user"
            + " on (apps.createdBy = user.unqUserId)"
            + " where apps.ownerProjectId = ? and apps.scheduled=? group by apps.appName, apps.statusEnum, apps.appId, apps.createdBy, apps.userId, apps.class, apps.createSt, apps.description, apps.appCombiner, apps.groupId, apps.scheduled"
            + " order by apps.createSt desc";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("apps.ownerProjectId", projectId);
    objectMap.put("apps.scheduled", schedJobsFlag);
    List<Application> applications =
        MySqlOperations.scanWithSqlQuery(Application.class, sql, objectMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getScheduleJobs()"
            + ParamUtils.getString(applications));
    return applications;
  }

  public static void saveScheduleJob(ScheduleApplication scApplication) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> saveScheduleJob()"
            + ParamUtils.getString(scApplication));

    scApplication.setId(null);
    MySqlOperations.insert(
        scApplication, HbaseUtility.getIdAnnotationForClass(ScheduleApplication.class));

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << saveScheduleJob()"
            + ParamUtils.getString(scApplication));
  }

  public static void killOozieJobByJobId(String oozieJobId, String oozieServerUrl) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> killOozieJobByJobId()"
            + ParamUtils.getString(oozieJobId, oozieServerUrl));

    OozieJobScheduler jobScheduler = new OozieJobScheduler();
    jobScheduler.setOozieServerUrl(oozieServerUrl);
    jobScheduler.killOozieJob(oozieJobId);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << killOozieJobByJobId()"
            + ParamUtils.getString(oozieJobId, oozieServerUrl));
  }

  public static List<JobInstance> getOutPutPathOfNodesByRootId(Long rootId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getOutPutPathOfNodesByRootId()"
            + ParamUtils.getString(rootId));

    String query =
        "select * from " + Constants.JOB_INSTANCES_COLLECTION + " where baapJobInstId = ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("baapJobInstId", rootId);
    List<JobInstance> jobInstances =
        MySqlOperations.scanWithSqlQuery(JobInstance.class, query, objectMap);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getOutPutPathOfNodesByRootId()"
            + ParamUtils.getString(jobInstances));
    return jobInstances;
  }

  public static List<Application> getFlowDetails(String[] flows) throws SystemException {

    String[] flowsArray = flows;
    String inValueString = "";
    int i = 0;

    if (null == flows || flows.length <= 0) {
      return new ArrayList<Application>();
    }

    String query =
        "select ma.appName, ma.statusEnum , ma.userId , ma.appId , ma.lastRun, ma.createSt, ma.flowType, ma.pollInterval, "
            + " ma.class, ma.description,ma.appCombiner, ma.groupId, ma.scheduled, CONCAT(firstName,' ', lastName) as createdBy, ma.ownerProjectId, ma.runningStatus "
            + "from maxiq_apps ma join application_user au on ma.createdBy = au.unqUserId where appName in (";

    for (String flow : flowsArray) {
      if (i == 0) {
        inValueString = inValueString + "'" + flow + "'";
        i++;
      } else {
        inValueString = inValueString + "," + "'" + flow + "'";
      }
    }

    query = query + inValueString + ")";

    logger.info(
        LoggerConstants.LOG_MAXIQWEB + " >> getFlowDetails(query)" + ParamUtils.getString(query));

    List<Application> flowList = MySqlOperations.scanWithSqlQuery(Application.class, query, null);

    return flowList;
  }

  public static AppStatusEnum getJobStatusByRootId(Long rootId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getJobStatusByRootId()" + ParamUtils.getString(rootId));
    return JobInstanceDao.getRootJobInstanceByRootId(rootId);
  }

  public static List<UserActivity> getLatestUpdatedData(Long projectId, String activityDate)
      throws SystemException {
    logger.info(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getLatestUpdatedData()"
            + ParamUtils.getString(projectId)
            + " "
            + activityDate);
    // TODO Auto-generated method stub
    String query =
        "select log.id, log.ownerProjectId, log.objectType, log.objectId, CONCAT(firstName, ' ', lastName) as userId, "
            + "log.timeDate, log.action, log.objectName from activity_log log join application_user app on "
            + "(log.userId = app.unqUserId) where log.ownerProjectId = ?  and "
            + "date_format(from_unixtime(floor(log.timeDate/1000)),'%Y-%m-%d') = ? order by log.timeDate desc LIMIT 5";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("log.ownerProjectId", projectId);
    objectMap.put("2", activityDate);
    List<UserActivity> allListOfUserActivityLog =
        MySqlOperations.scanWithSqlQuery(UserActivity.class, query, objectMap);

    logger.info(
        LoggerConstants.LOG_MAXIQWEB
            + " << getLatestUpdatedData()"
            + ParamUtils.getString(allListOfUserActivityLog));
    return allListOfUserActivityLog;
  }

  public static List<UserActivityDTO> getLatestUpdatedDates(Long projectId) throws SystemException {
    logger.info(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getLatestUpdatedData()"
            + ParamUtils.getString(projectId));
    // TODO Auto-generated method stub
    String query =
        "select DISTINCT date_format(from_unixtime(floor(timeDate/1000)),'%Y-%m-%d') as timeDate from activity_log where ownerProjectId = ? order by timeDate desc LIMIT 200";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("ownerProjectId", projectId);

    List<UserActivityDTO> userActivityDTO =
        MySqlOperations.scanWithSqlQuery(UserActivityDTO.class, query, objectMap);
    logger.info(
        LoggerConstants.LOG_MAXIQWEB
            + " << getLatestUpdatedDates()"
            + ParamUtils.getString(userActivityDTO));
    return userActivityDTO;
  }

  
   * @Mayuri - MAX-1825
   * @Param - appId
   *
   * This method used to delete data source from flow
   * 
  public static void deleteFlowLevelDataSource(Long appId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> deleteDataSourceInFlow()"
            + ParamUtils.getString(appId));

    List<DataSourceInstance> dataSourceInstancesForApp =
        DataSourceInstanceDao.getDataSourceInstancesForApp(appId);
    for (DataSourceInstance dataSourceInstance : dataSourceInstancesForApp) {
      DataSourceInstanceDao.deleteDataSourceFromDataSourceInstance(dataSourceInstance);
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << deleteDataSourceInFlow()"
            + ParamUtils.getString(appId));
  }*/
}
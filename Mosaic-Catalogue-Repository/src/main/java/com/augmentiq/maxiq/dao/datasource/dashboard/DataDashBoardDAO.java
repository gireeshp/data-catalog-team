package com.augmentiq.maxiq.dao.datasource.dashboard;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DashBoardDTO;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class DataDashBoardDAO {

  private static final Logger logger = LoggerFactory.getLogger(DataDashBoardDAO.class);

  /**
   * Changed for MAX-1903
   * changed by - Mukund Tripathi
   * Description - Fixed sql query.
   * On dataSource dash board tab, when we click on Total users ,the user list displayed 
   * was invalid. Query has been fixed to display the correct user list for that data source.
   * 
   * @param dataSourceId
   * @return
   * @throws SystemException
   */
  public static List<ApplicationUser> getSharedObjectUsers(String dataSourceId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getSharedObjectUsers {}", dataSourceId);

/*    String sql =
        "select * from application_user where unqUserId in ( select userId from user_group_mappings where groupId = ? )";
*/    
    String sql =
        "select au.* from " + Sequences.APPLICATION_USER + " au ,"
        + " ( select userId from " + Sequences.USER_GROUP_MAPPINGS + " where groupId in "
                      + " ( select groupId from " + Sequences.GROUP_OBJECT_MAPPING 
                      + " where objectId = ? and objectType = 'DATA_SOURCE') "
        + " UNION "
        + " select createdBy as userId from " + Sequences.DATASOURCE_INSTANCE
        + " where dataSourceId = ? "
        + " UNION "
        + " select userId from " + Sequences.USER_OBJECT_MAPPING
        + " where objectId = ? and objectType = 'DATA_SOURCE' ) ut "
        + " where au.unqUserId = ut.userId";
        
    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put(GENERAL_CONSTANTS.DATASOURCEID, dataSourceId);
    params.put(QueryConstants.DataRequest.OBJECT_ID, dataSourceId);
    params.put(QueryConstants.Parameters.PARAM_1, dataSourceId);
    
    List<ApplicationUser> applicationUser =
        MySqlOperations.scanWithSqlQuery(ApplicationUser.class, sql, params);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getSharedObjectUsers {}", dataSourceId);
    return applicationUser;
  }

  /**
   * Changed for MAX-1902
   * changed by - Yogesh Lokhande
   * Description - added "and rating = 0" in where clause
   * while creating feedback, when we click on rating it directory save in rating in feedback table.
   * and when we submit actual feedback details, it takes rating value as 0 and insert data in feedback table.
   * so for each ds feedback count is increased with one value which is rating non zero value in feedback.  
   * 
   * @param dataSourceId
   * @return
   * @throws SystemException
   */
  public static List<DashBoardDTO> getDataDashboardCountDetails(String dataSourceId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> getDataDashboardCountDetails {} ", dataSourceId);

    String sql =
        "select * from (select count(*) + 1 as count, 'group' as name  from "
            + Sequences.APPLICATION_USER
            + " where unqUserId in (select userId from "
            + Sequences.USER_GROUP_MAPPINGS
            + " where groupId in "
            + " (select groupId from "
            + Sequences.GROUP_OBJECT_MAPPING
            + " where objectId = ? "
            + " and objectType = 'DATA_SOURCE')"
            + " union all select userId from "
            + Sequences.USER_OBJECT_MAPPING
            + " where objectId = ? and objectType = 'DATA_SOURCE') "
            + " union all select count(*) as count, 'feedback' as name from "
            + Sequences.FEEDBACK
            + " where dataSourceId = ? "
            + "and rating = 0 "
            + "union all select (count(*) + 1) as count, 'project' as name from "
            + Sequences.PROJECT_OBJECT_MAPPING
            + " where objectId = ? and objectType = 'DATA_SOURCE' "
            + "union all select count(*) AS count, 'api' as name "
            + " from "
            + Sequences.DATASOURCE_SERVICE_APIMANAGER
            + " where dataSourceId = ?) as tbl order by name desc";

    Map<String, Object> params = new LinkedHashMap<String, Object>();

    params.put(QueryConstants.DataRequest.OBJECT_ID, dataSourceId);
    params.put(QueryConstants.Parameters.PARAM_1, dataSourceId);
    params.put(QueryConstants.DatasourceServiceApimanager.DATASOURCE_ID, dataSourceId);
    params.put(QueryConstants.Parameters.PARAM_2, dataSourceId);
    params.put(QueryConstants.Parameters.PARAM_3, dataSourceId);

    List<DashBoardDTO> countList =
        MySqlOperations.scanWithSqlQuery(DashBoardDTO.class, sql, params);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> getDataDashboardCountDetails {}", dataSourceId);
    return countList;
  }
}

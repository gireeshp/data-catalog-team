package com.augmentiq.maxiq.dao.flavour;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.flavour.query.FlavourQuery;
import com.augmentiq.maxiq.entity.model.setup.domains.ActionsMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.BlockedUrlActionMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;

public class FlavourDAO {

  private static final Logger logger = LoggerFactory.getLogger(FlavourDAO.class);
  private static final String loggedInUserId = "loggedInUserId";

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<PersonaMaster> getPersonas(String userId) throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getPersonas(userId)" + userId);

    String sql =
        "	select c.* from (select b.* from application_user a join user_persona b on a.unqUserId = b.unqUserId) as d join persona_master c on d.persona_id = c.persona_id "
            + "		where d.unqUserId=? and c.persona_id in (			"
            + "			select personaId from flavour_persona_mappings where flavourId in ( "
            + "				select flavourId from client where clientId in ( 	"
            + "					select clientId from user_client_mapping where unqUserId =  ?"
            + "				)  	"
            + "			)	"
            + "		)	";

    Map<String, Object> query1 = new LinkedHashMap<String, Object>();
    query1.put("d.unqUserId", userId);
    query1.put("unqUserId", userId);
    List<PersonaMaster> personasList =
        MySqlOperations.scanWithSqlQuery(PersonaMaster.class, sql, query1);

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> actionsMasterList" + personasList);

    return personasList;
  }

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<ActionsMaster> getActions(String userId) throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getActions(userId)" + userId);

    String sql = FlavourQuery.ACTIONS_SQL.replaceAll(loggedInUserId, userId);
    List<ActionsMaster> actionsMasterList =
        MySqlOperations.scanWithSqlQuery(ActionsMaster.class, sql, null);

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> actionsMasterList" + actionsMasterList);

    return actionsMasterList;
  }

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<BlockedUrlActionMappings> getBlockedUrlList(String userId)
      throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getBlockedUrlList(userId)" + userId);

    String sql = FlavourQuery.BLOCKED_URL_SQL.replaceAll(loggedInUserId, userId);
    List<BlockedUrlActionMappings> blockedUrlActionMappingList =
        MySqlOperations.scanWithSqlQuery(BlockedUrlActionMappings.class, sql, null);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> blockedUrlActionMappingList"
            + blockedUrlActionMappingList);

    return blockedUrlActionMappingList;
  }
}

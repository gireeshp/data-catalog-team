package com.augmentiq.maxiq.dao.globalsearch;

import org.elasticsearch.common.xcontent.XContentBuilder;

import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchResult;
import com.augmentiq.maxiq.entity.model.configuration.bean.SearchInType;
import com.augmentiq.maxiq.model.globalsearch.GlobalSearchInput;

/** @author Ajinkya Marathe Interface plan for elastic search operations */
public interface GlobalSearchIndexDAO {
  public boolean createIndex(String indexName) throws Exception;

  public void loadAllDocuments() throws Exception;

  public boolean checkIndexExistORNot();

  public void createDocument(XContentBuilder json, String id, SearchInType type);

  public GlobalSearchResult searchInDocuments(GlobalSearchInput globalSearchInput);

  public GlobalSearchResult searchInDocumentsDiscovery(GlobalSearchInput globalSearchInput);
}


package com.augmentiq.maxiq.dao.globalsearch;

import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.DATE_FORMAT_FOR_GSON_LOAD;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.DATE_FORMAT_FOR_QUERY;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.INDEX_TYPE;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_ALL;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_CREATED_BY;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_CREATED_DATE;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_DS_FIELDS_NAME;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_DS_QUERY;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_FLOW_DS_NAME;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_FLOW_INPUT_FIELDS_NAME;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_FLOW_OUTPUT_FIELDS_NAME;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_FLOW_QUERY;
import static com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants.QUERY_FIELD_GROUP_ID;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.http.util.TextUtils;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryFilterBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.StackTraceReader;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.configuration.setupdao.GroupConfigDao;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileStoreObject;
import com.augmentiq.maxiq.constant.configuration.enums.LoadStategyType;
import com.augmentiq.maxiq.constant.globalsearch.GlobalSearchCosntants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchResult;
import com.augmentiq.maxiq.entity.model.configuration.bean.InputParameterDomain;
import com.augmentiq.maxiq.entity.model.configuration.bean.Record;
import com.augmentiq.maxiq.entity.model.configuration.bean.SearchInType;
import com.augmentiq.maxiq.model.globalsearch.ESClientFactory;
import com.augmentiq.maxiq.model.globalsearch.GlobalSearchInput;
import com.augmentiq.maxiq.model.globalsearch.SearchIndexDTO;
import com.augmentiq.maxiq.model.globalsearch.response.BaseProcess;
/*import com.augmentiq.maxiq.model.globalsearch.response.InputDataFile;
import com.augmentiq.maxiq.model.globalsearch.response.Node;
import com.augmentiq.maxiq.model.globalsearch.response.NodeInField;
import com.augmentiq.maxiq.model.globalsearch.response.NodeOutField;
import com.augmentiq.maxiq.model.globalsearch.response.Output;*/
//import com.augmentiq.maxiq.core.models.globalsearch.response.OutputDataFile;
import com.augmentiq.maxiq.model.globalsearch.response.Bucket;
import com.augmentiq.maxiq.model.globalsearch.response.DisplayFacades;
import com.augmentiq.maxiq.model.globalsearch.response.Input;
import com.augmentiq.maxiq.model.globalsearch.response.InputDataFile;
import com.augmentiq.maxiq.model.globalsearch.response.Node;
import com.augmentiq.maxiq.model.globalsearch.response.NodeInField;
import com.augmentiq.maxiq.model.globalsearch.response.NodeOutField;
import com.augmentiq.maxiq.model.globalsearch.response.Output;
import com.augmentiq.maxiq.model.globalsearch.response.OutputDataFile;
import com.augmentiq.maxiq.model.globalsearch.response.Source;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.model.globalsearch.response.Process;
//import com.augmentiq.maxiq.core.canvas.domain.io.DataFile;
//import com.augmentiq.maxiq.core.canvas.domain.io.Input;
//import com.augmentiq.maxiq.core.canvas.domain.io.OutPut;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/*
 * Author - Ajinkya Marathe
 * Operations on elastic search
 * */
public class GlobalSearchIndexDAOImpl implements GlobalSearchIndexDAO {

  private static final Logger logger = LoggerFactory.getLogger(GlobalSearchIndexDAOImpl.class);

  private static List<SearchIndexDTO> listOfFlowNDataSource = new ArrayList<SearchIndexDTO>();
  public static final String MAXIQ_HOME = System.getenv().get(CacheConstants.MAXIQ_HOME);
  public static final String MAPPING_FILE_PATH =
      MAXIQ_HOME + "/conf/global_search_document_mapping.json";
  private static final String EMPTY_SPACE = " ";
  private static String INDEX_NAME = "";

  static {
    try {
      INDEX_NAME = Cache.getProperty(CacheConstants.GLOBAL_SEARCH_ELASTIC_SEARCH_INDEX_NAME);
    } catch (Exception e) {
      //			 e.printStackTrace();
      logger.error(
          GlobalSearchIndexDAOImpl.class.getClass().getName()
              + " INDEX_NAME not configured in config.properties");
    }
  }

  /** Establishing connection with elastic search & Loading of file path from constants */
  {
    if (StringUtils.isBlank(MAXIQ_HOME)) {
      logger.error("Base Path $MAXIQ_HOME {} not configured ", MAXIQ_HOME);
    }
  }

  /** @return true if index is already created in elastic search */
  @Override
  public boolean createIndex(String indexName) throws Exception {
    JsonParser parser = new JsonParser();
    // CREATE MAPPING
    JsonElement obj = null;
    try {
      obj = parser.parse(new FileReader(MAPPING_FILE_PATH));
    } catch (JsonIOException | JsonSyntaxException | FileNotFoundException e1) {
      logger.error(
          LoggerConstants.LOG_MAXIQWEB
              + " While creating index of -> "
              + StackTraceReader.stringFromStackTrace(e1));
    }

    try {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << Checking Index
      // of -> " + GlobalSearchCosntants.INDEX_NAME);
      CreateIndexRequestBuilder createIndex =
          ESClientFactory.getInstance().admin().indices().prepareCreate(INDEX_NAME);
      createIndex.setSource(obj.toString());
      CreateIndexResponse response = createIndex.execute().actionGet();
      return response.isAcknowledged();
    } catch (Exception e) {
      logger.error(
          LoggerConstants.LOG_MAXIQWEB
              + " While creating index of -> "
              + StackTraceReader.stringFromStackTrace(e));
    } finally {
      ESClientFactory.shutdown();
    }
    return false;
  }

  /** @return If index is created or not in elastic search */
  @Override
  public boolean checkIndexExistORNot() {
    try {
      return ESClientFactory.getInstance()
          .admin()
          .indices()
          .prepareExists(INDEX_NAME)
          .execute()
          .actionGet()
          .isExists();
    } catch (NoNodeAvailableException e) {
      logger.error(
          "<< Elastic search service is not working or started NoNodeAvailableEx <<"
              + StackTraceReader.stringFromStackTrace(e));
    } finally {
      ESClientFactory.shutdown();
    }
    return false;
  }

  /*
   * (non-Javadoc)
   *
   * @params (create document with content Builder, unique id of datasource or
   * flow, type of record datasource or flow)
   *
   * @see
   * com.augmentiq.maxiq.GlobalSearch.GlobalSearchIndexDAO#createDocument(org.
   * elasticsearch.common.xcontent.XContentBuilder, java.lang.String,
   * com.augmentiq.maxiq.GlobalSearch.SearchInType)
   */
  @Override
  public void createDocument(XContentBuilder json, String id, SearchInType type) {
    LoadMetaDataDAOImpl loaddata = new LoadMetaDataDAOImpl();
    try {
      ESClientFactory.getInstance()
          .prepareIndex(INDEX_NAME, GlobalSearchCosntants.INDEX_TYPE, type + "-" + id)
          .setSource(json)
          .execute()
          .actionGet();
      loaddata.updateIsUpdatedInIndex(Long.valueOf(id), type);
    } catch (Exception e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + StackTraceReader.stringFromStackTrace(e));
      //			 e.printStackTrace();
    } finally {
      ESClientFactory.shutdown();
    }
  }

  @Override
  public GlobalSearchResult searchInDocumentsDiscovery(GlobalSearchInput globalSearchInput) {
    GlobalSearchResult globalSearchResult = new GlobalSearchResult();
    SearchResponse response = null;
    QueryBuilder mainQueryBuilder = null;

    try {
      // Match all Query

      // Advance Query
      BoolQueryBuilder globalQueryBuilder = QueryBuilders.boolQuery();

      // If Datasource or flows or others
      if ((globalSearchInput.getSearchInTypes() != null)
          && (globalSearchInput.getSearchInTypes().size() == 1)) {
        // Type of DOC like DATASOURCE
        if (globalSearchInput.getSearchInTypes().contains(SearchInType.DATASOURCE)) {
          // Type of DOC like DATASOURCE
          globalQueryBuilder.must(
              QueryBuilders.termQuery(
                  GlobalSearchCosntants.QUERY_FIELD_TYPE, SearchInType.DATASOURCE));
          // Category
          if ((globalSearchInput.getCategory() != null)
              && globalSearchInput.getCategory().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termQuery(
                    GlobalSearchCosntants.DOCUMENT_CATEGORY, globalSearchInput.getCategory()));
          }
          // SubCategory
          if ((globalSearchInput.getSubCategory() != null)
              && globalSearchInput.getSubCategory().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termQuery(
                    GlobalSearchCosntants.DOCUMENT_SUBCATEGORY,
                    globalSearchInput.getSubCategory()));
          }

          // Tags Field LEVEL
          if (globalSearchInput.getTags() != null && globalSearchInput.getTags().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_FIELD_LEVEL_TAGS,
                    globalSearchInput.getTags()));
          }

          // Tags DS LEVEL
          if (globalSearchInput.getTags() != null && globalSearchInput.getTags().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_TAGS, globalSearchInput.getTags()));
          }
        }
      }

      getQueryOnMustParameters(globalSearchInput, globalQueryBuilder);

      // Query string of datasource
      if (globalSearchInput.getQueryString() != null
          && !TextUtils.isEmpty(globalSearchInput.getQueryString())
          && globalSearchInput.getQueryString().length() >= 0) {
        BoolQueryBuilder dsAppQueryBuilder = QueryBuilders.boolQuery();
        getFilterOnQuery(globalSearchInput, dsAppQueryBuilder);
        globalQueryBuilder.must(dsAppQueryBuilder);
      }

      // Column Name
      if (globalSearchInput.getColumnName() != null
          && !TextUtils.isEmpty(globalSearchInput.getColumnName())
          && globalSearchInput.getColumnName().length() >= 0) {
        QueryFilterBuilder dsTFB =
            new QueryFilterBuilder(
                QueryBuilders.queryStringQuery(globalSearchInput.getColumnName())
                    .defaultField(QUERY_FIELD_DS_FIELDS_NAME));

        BoolFilterBuilder flBuilder = FilterBuilders.boolFilter();
        if (globalSearchInput.getSearchInTypes() != null
            && globalSearchInput.getSearchInTypes().contains(SearchInType.DATASOURCE)) {
          flBuilder.should(dsTFB);
        }

        mainQueryBuilder = QueryBuilders.filteredQuery(globalQueryBuilder, flBuilder);
      } else {
        mainQueryBuilder =
            QueryBuilders.filteredQuery(globalQueryBuilder, FilterBuilders.matchAllFilter());
      }
      try {
        response =
            ESClientFactory.getInstance()
                .prepareSearch(INDEX_NAME)
                .setTypes(INDEX_TYPE)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(mainQueryBuilder)
                .addAggregation(getTypeAggregationBuilder())
                .addAggregation(getCreatedByAggregationBuilder())
                .addAggregation(getCategoryAggregationBuilder())
                .addAggregation(getSubCategoryAggregationBuilder())
                .addAggregation(getCreatedDateAggregationBuilder())
                .addAggregation(getSourceTypeAggregationBuilder())
                .addAggregation(getDataAtRestAggregationBuilder())
                .addAggregation(getLoadStrategyAggregationBuilder())
                .addAggregation(getDataRepoAggregationBuilder())
                .addAggregation(getUserGroupAggregationBuilder())
                .addAggregation(getProjectsNameAggregationBuilder())
                .addAggregation(getAvgRatingAggregationBuilder())
                .setFrom(globalSearchInput.getFetchRecordsFrm())
                .setSize(globalSearchInput.getSizeOfRecords())
                .setExplain(true)
                .execute()
                .actionGet();
      } catch (NoNodeAvailableException e) {
        //				e.printStackTrace();
        logger.error(
            "Please check elastic search is working or not because No node available NoNodeAvailableException while creating document");
      }

      if (response != null && response.getHits().totalHits() >= 0) {
        getFinalReponseBySearchFilter(globalSearchInput, globalSearchResult, response);
      } else {
        globalSearchResult.setMessage(GlobalSearchCosntants.NO_RECORDS_FOUND);
      }
    } catch (Exception e) {
      logger.error(getClass().getName() + e.getStackTrace());
      //			 e.printStackTrace();
      globalSearchResult.setMessage(
          GlobalSearchCosntants.SOME_PROBLEM_IN_PROCESSING_RESPONSE + EMPTY_SPACE + e.getMessage());
    } finally {
      ESClientFactory.shutdown();
    }
    return globalSearchResult;
  }

  /** For loading all the documents from database to elastic search */
  @Override
  public void loadAllDocuments() throws Exception {
    LoadMetaDataDAOImpl loadMetaDataDAOImpl = new LoadMetaDataDAOImpl();
   listOfFlowNDataSource = loadMetaDataDAOImpl.getAllDatasourcesAndFlow();
    if (listOfFlowNDataSource != null) {
      try {
        for (SearchIndexDTO searchIndexDTO : listOfFlowNDataSource) { // Loop
          // for
          // SearchIndexDTO
          // START
          if (searchIndexDTO != null) { // Condition for index Not null
            // START
            XContentBuilder json;
            try {
              json = XContentFactory.jsonBuilder();
              json.startObject(); // Main Object Start
              json.field(GlobalSearchCosntants.DOCUMENT_ID, searchIndexDTO.getId());
              json.field(GlobalSearchCosntants.DOCUMENT_NAME, searchIndexDTO.getName());
              json.field(GlobalSearchCosntants.DOCUMENT_TYPE, searchIndexDTO.getType());
              json.field(GlobalSearchCosntants.DOCUMENT_SOURCETYPE, searchIndexDTO.getSourceType());
              json.field(GlobalSearchCosntants.DOCUMENT_CREATED_BY, searchIndexDTO.getCreatedBy());
              json.field(
                  GlobalSearchCosntants.DOCUMENT_CREATED_DATE, searchIndexDTO.getCreatedDate());
              json.field(GlobalSearchCosntants.DOCUMENT_GROUP_ID, searchIndexDTO.getGroupId());
              if (searchIndexDTO.getCategory() != null
                  && (!searchIndexDTO.getCategory().equalsIgnoreCase("null")
                      || !searchIndexDTO.getCategory().equalsIgnoreCase(""))) {
                json.field(GlobalSearchCosntants.DOCUMENT_CATEGORY, searchIndexDTO.getCategory());
              } else {
                json.field(GlobalSearchCosntants.DOCUMENT_CATEGORY, "Other");
              }

              if (searchIndexDTO.getSubCategory() != null
                  && (!searchIndexDTO.getSubCategory().equalsIgnoreCase("null")
                      || !searchIndexDTO.getSubCategory().equalsIgnoreCase(""))) {
                json.field(
                    GlobalSearchCosntants.DOCUMENT_SUBCATEGORY, searchIndexDTO.getSubCategory());
              } else {
                json.field(GlobalSearchCosntants.DOCUMENT_SUBCATEGORY, "Other");
              }

              if (searchIndexDTO.getType() == SearchInType.DATASOURCE) { // DataSource
                // Condition
                // START
                DataSource dataSource = null;
                try {
                  // Get the all information of DataSource by
                  // DataSource - ID
                  dataSource = DataRepositoryFetchWithChild.getDataSource(searchIndexDTO.getId());
                } catch (SystemException e) {
                  //									e.printStackTrace();
                  logger.error(
                      LoggerConstants.LOG_MAXIQWEB
                          + " <<  DataRepositoryFetchWithChild.getDataSource(id)>>"
                          + ParamUtils.getString(e.getMessage()));
                }

                /*json.field(GlobalSearchCosntants.DOCUMENT_CATEGORY, searchIndexDTO.getCategory());
                json.field(GlobalSearchCosntants.DOCUMENT_SUBCATEGORY, searchIndexDTO.getSubCategory());*/

                json.field(GlobalSearchCosntants.DOCUMENT_DATAREPO, searchIndexDTO.getDataRepo());
                //json.field( GlobalSearchCosntants.DOCUMENT_PROJECTSNAME, searchIndexDTO.getProjectsName());
                json.field(GlobalSearchCosntants.DOCUMENT_AVGRATING, searchIndexDTO.getAvgRating());

                if (dataSource != null && dataSource.getFieldMappings() != null) { // DataSource
                  // &
                  // FieldMapping
                  // Condition
                  // START

                  json.field(
                      GlobalSearchCosntants.DOCUMENT_SOURCETYPE,
                      dataSource.getDataSourceType() + "");
                  if (dataSource.getDataSourceLoadStrategy() != null
                      && dataSource.getDataSourceLoadStrategy().getLoadStategyType() != null
                      && !dataSource
                          .getDataSourceLoadStrategy()
                          .getLoadStategyType()
                          .toString()
                          .equalsIgnoreCase("null")
                      && StringUtils.isNotBlank(
                          dataSource.getDataSourceLoadStrategy().getLoadStategyType().toString())) {
                    json.field(
                        GlobalSearchCosntants.DOCUMENT_LOADSTRATEGY,
                        dataSource.getDataSourceLoadStrategy().getLoadStategyType().toString());
                  } else {
                    json.field(
                        GlobalSearchCosntants.DOCUMENT_LOADSTRATEGY,
                        LoadStategyType.REPLACEALL.toString());
                  }

                  String dataAtRest =
                      (dataSource.getFileStoreObject()).equals(FileStoreObject.HDFS)
                          ? FileStoreObject.HDFS.toString()
                          : FileStoreObject.HBASE.toString();
                  json.field(GlobalSearchCosntants.DOCUMENT_DATAATREST, dataAtRest);
                  json.field(
                      GlobalSearchCosntants.DOCUMENT_USERGROUP,
                      GroupConfigDao.getGroupName(searchIndexDTO.getGroupId()));
                  json.startArray(GlobalSearchCosntants.DOCUMENT_FIELD_TAGS);
                  if (dataSource.getTags() != null && dataSource.getTags().size() > 0) {
                    for (String tag : dataSource.getTags()) {
                      json.startObject()
                          .field(GlobalSearchCosntants.DOCUMENT_FIELD_TAG_NAME, tag)
                          .endObject();
                    }
                  }
                  json.endArray();

                  json.field(
                      GlobalSearchCosntants.DOCUMENT_DS_TYPE, dataSource.getDataSourceType());
                  ConnectionConfig connectionConfig = dataSource.getConfig();
                  if (connectionConfig != null) {
                    if (dataSource.getDataSourceType().equals(DataSourceType.FTP_DATA)) {
                     // json.field(GlobalSearchCosntants.DOCUMENT_DS_CONNECTOR_TYPE,connectionConfig.getConnectorType());
                      //json.field(GlobalSearchCosntants.DOCUMENT_DS_CONFIG_CONNECTION_TYPE,connectionConfig.getConfigConnectionType());
                      //json.field(GlobalSearchCosntants.DOCUMENT_DS_IP_ADDRESS, connectionConfig.getIp());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_DB_PORT, connectionConfig.getPort());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_CONNECTION_NAME,connectionConfig.getConncetionName());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_DB_USERNAME,connectionConfig.getDbUserName());
                    }

                    if (dataSource.getDataSourceType().equals(DataSourceType.RDBMS_DATA_SOURCE)) {
                      //json.field(GlobalSearchCosntants.DOCUMENT_DS_CONNECTOR_TYPE,connectionConfig.getConnectorType());
                      //json.field(GlobalSearchCosntants.DOCUMENT_DS_CONFIG_CONNECTION_TYPE,connectionConfig.getConfigConnectionType());
                      //json.field(GlobalSearchCosntants.DOCUMENT_DS_QUERY,connectionConfig.getQueryToExecute());
                      //json.field(GlobalSearchCosntants.DOCUMENT_DS_PRIMARY_KEY,connectionConfig.getDbPrimaryKey());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_IP_ADDRESS, connectionConfig.getIpAddress());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_DB_PORT, connectionConfig.getPort());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_CONNECTION_NAME,connectionConfig.getConncetionName());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_DATABASE_NAME,connectionConfig.getDbName());
                      json.field(GlobalSearchCosntants.DOCUMENT_DS_DB_USERNAME,connectionConfig.getDbUserName());
                    }
                  }

                  json.startArray(GlobalSearchCosntants.DOCUMENT_DS_FIELDS); // ArrayStart

                  // Field Mappings adding in indexing array
                  for (FieldMapping field : dataSource.getFieldMappings()) {
                    if (field != null
                        && field.getFieldName() != null
                        && !TextUtils.isEmpty(field.getFieldName())) {
                      json.startObject()
                          .field(GlobalSearchCosntants.DOCUMENT_FIELD_NAME, field.getFieldName())
                          .field(
                              GlobalSearchCosntants.DOCUMENT_FIELD_TYPE,
                              field.getFieldDataType() != null
                                  ? field.getFieldDataType().toString()
                                  : "" + null)
                          .field(GlobalSearchCosntants.DOCUMENT_FIELD_FORMAT, field.getFormat());
                      json.startArray(GlobalSearchCosntants.DOCUMENT_FIELD_TAGS);
                      if (field.getTags() != null && field.getTags().size() > 0) {
                        for (String tag : field.getTags()) {
                          json.startObject()
                              .field(GlobalSearchCosntants.DOCUMENT_FIELD_TAG_NAME, tag)
                              .endObject();
                        }
                      }
                      json.endArray();
                      json.endObject();
                    }
                  }
                  json.endArray(); // ArrayEnd
                  if (dataSource.getDsLevelParams() != null) {
                    json.startArray(
                        GlobalSearchCosntants
                            .DOCUMENT_DATASOURCE_GLOBAL_INPUT_PARAMETERS); // ArrayStart
                    // Field Mappings adding in indexing
                    // array
                    for (InputParameterDomain field : dataSource.getDsLevelParams()) {
                      if (field != null) {
                        json.startObject()
                            .field(GlobalSearchCosntants.DOCUMENT_FIELD_NAME, field.getParamName())
                            .field(
                                GlobalSearchCosntants.DOCUMENT_FIELD_TYPE,
                                field.getParamType() != null
                                    ? field.getParamType().toString()
                                    : null + "")
                            .field(
                                GlobalSearchCosntants
                                    .DOCUMENT_DATASOURCE_GLOBAL_INPUT_PARAMETER_VALUE,
                                field.getDefaultVal())
                            .endObject();
                      }
                    }
                    json.endArray(); // ArrayEnd
                  }

                  json.endObject(); // Main Object END
                } // DataSource & FieldMapping Condition END

                createDocument(
                    json, String.valueOf(searchIndexDTO.getId()), SearchInType.DATASOURCE);
              } // DataSource Condition END

              
            } catch (IOException e1) {
              logger.error("Problem while creating document " + e1.getMessage());
              e1.printStackTrace();
            }
          } // Condition for index Not null START
        } // Loop for SearchIndexDTO END
      } catch (Exception e) {
        //				 e.printStackTrace();
        logger.error(getClass().getName() + e.getStackTrace());
      } finally {
        ESClientFactory.shutdown();
      }
    }
  }

  private AggregationBuilder<?> getTypeAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.type").field("type").size(0);
  }

  private AggregationBuilder<?> getCreatedByAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.created_by").field("created_by").size(0);
  }

  private AggregationBuilder<?> getCategoryAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.category").field("category").size(0);
  }

  private AggregationBuilder<?> getSubCategoryAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.sub_category")
        .field("sub_category")
        .size(0);
  }

  private AggregationBuilder<?> getCreatedDateAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.created_date")
        .field("created_date")
        .size(0);
  }

  private AggregationBuilder<?> getSourceTypeAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.source_type")
        .field("source_type")
        .size(0);
  }

  private AggregationBuilder<?> getDataAtRestAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.data_at_rest")
        .field("data_at_rest")
        .size(0);
  }

  private AggregationBuilder<?> getLoadStrategyAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.load_strategy")
        .field("load_strategy")
        .size(0);
  }

  private AggregationBuilder<?> getDataRepoAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.data_repo").field("data_repo").size(0);
  }

  private AggregationBuilder<?> getUserGroupAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.user_group").field("user_group").size(0);
  }

  private AggregationBuilder<?> getProjectsNameAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.projects_name")
        .field("projects_name")
        .size(0);
  }

  private AggregationBuilder<?> getAvgRatingAggregationBuilder() {
    return AggregationBuilders.terms("datasource_and_flow.avg_rating").field("avg_rating").size(0);
  }

  @Override
  public GlobalSearchResult searchInDocuments(GlobalSearchInput globalSearchInput) {

    GlobalSearchResult globalSearchResult = new GlobalSearchResult();
    SearchResponse response = null;
    QueryBuilder mainQueryBuilder = null;

    try {
      // Match all Query
      if ((globalSearchInput.getGeneralSearchString() != null
              && !TextUtils.isEmpty(globalSearchInput.getColumnName())
              && globalSearchInput.getColumnName().length() >= 0)
          && (globalSearchInput.getCreatedBy() == null
              || (globalSearchInput.getCreatedBy() != null
                  && globalSearchInput.getCreatedBy().size() == 0))
          && (globalSearchInput.getName() == null
              && TextUtils.isEmpty(globalSearchInput.getColumnName()))
          && (globalSearchInput.getCreatedWithinDays() == null
              || globalSearchInput.getCreatedWithinDays() == 0)
          && (globalSearchInput.getSearchInTypes() == null
              || (globalSearchInput.getSearchInTypes() != null
                  && globalSearchInput.getSearchInTypes().size() == 0))
          && (globalSearchInput.getColumnName() == null
              && TextUtils.isEmpty(globalSearchInput.getColumnName()))
          && (globalSearchInput.getTags() == null
              || (globalSearchInput.getTags() != null
                  && globalSearchInput.getTags().size() == 0))) {
        // For general Search for all

        BoolQueryBuilder globalQueryBuilderMatchAll = QueryBuilders.boolQuery();
        QueryStringQueryBuilder builderMatchAll =
            new QueryStringQueryBuilder(globalSearchInput.getGeneralSearchString());
        builderMatchAll.defaultField(QUERY_ALL);
        globalQueryBuilderMatchAll.must(builderMatchAll);

        // GroupId
        if (globalSearchInput.getGroupBy() != null && globalSearchInput.getGroupBy() > 0) {
          QueryBuilder queryBuilderForGroupBy =
              QueryBuilders.termQuery(QUERY_FIELD_GROUP_ID, globalSearchInput.getGroupBy());
          globalQueryBuilderMatchAll.must(queryBuilderForGroupBy);
        }

        response =
            ESClientFactory.getInstance()
                .prepareSearch(INDEX_NAME)
                .setTypes(INDEX_TYPE)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(globalQueryBuilderMatchAll)
                .addAggregation(getTypeAggregationBuilder())
                .addAggregation(getCreatedByAggregationBuilder())
                .addAggregation(getCategoryAggregationBuilder())
                .addAggregation(getSubCategoryAggregationBuilder())
                .addAggregation(getCreatedDateAggregationBuilder())
                .addAggregation(getSourceTypeAggregationBuilder())
                .addAggregation(getDataAtRestAggregationBuilder())
                .addAggregation(getLoadStrategyAggregationBuilder())
                .addAggregation(getDataRepoAggregationBuilder())
                .addAggregation(getUserGroupAggregationBuilder())
                .addAggregation(getProjectsNameAggregationBuilder())
                .addAggregation(getAvgRatingAggregationBuilder())
                .setFrom(globalSearchInput.getFetchRecordsFrm())
                .setSize(globalSearchInput.getSizeOfRecords())
                .setExplain(true)
                .execute()
                .actionGet();
      } else {
        // Advance Query
        BoolQueryBuilder globalQueryBuilder = QueryBuilders.boolQuery();

        // If Datasource or flows or others
        BoolQueryBuilder boolQueryDsBuilder = QueryBuilders.boolQuery();

        // Condition 1- If globalSearchInput.getSearchInTypes() == null
        // then necessary to search in DATASOURCE & FLOW
        // So added condition for DS & FLOW
        if (globalSearchInput.getSearchInTypes() == null) {

          // Category
          if ((globalSearchInput.getCategory() != null)
              && globalSearchInput.getCategory().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.DOCUMENT_CATEGORY, globalSearchInput.getCategory()));
          }

          // SubCategory
          if ((globalSearchInput.getSubCategory() != null)
              && globalSearchInput.getSubCategory().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.DOCUMENT_SUBCATEGORY,
                    globalSearchInput.getSubCategory()));
          }

          // Tags DS LEVEL
          if (globalSearchInput.getTags() != null && globalSearchInput.getTags().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_TAGS, globalSearchInput.getTags()));
          }

          // Source type level
          if (globalSearchInput.getSourceType() != null
              && globalSearchInput.getSourceType().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_SOURCE_TYPE,
                    globalSearchInput.getSourceType()));
          }

          // Data At Rest level
          if (globalSearchInput.getDataAtRest() != null
              && globalSearchInput.getDataAtRest().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_DATA_AT_REST,
                    globalSearchInput.getDataAtRest()));
          }

          // Load Strategy level
          if (globalSearchInput.getLoadStrategy() != null
              && globalSearchInput.getLoadStrategy().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_LOAD_STRATEGY,
                    globalSearchInput.getLoadStrategy()));
          }
          // Data source repo level
          if (globalSearchInput.getDataRepo() != null
              && globalSearchInput.getDataRepo().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_DATA_REPO, globalSearchInput.getDataRepo()));
          }
          // User group level
          if (globalSearchInput.getUserGroup() != null
              && globalSearchInput.getUserGroup().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_USER_GROUP,
                    globalSearchInput.getUserGroup()));
          }
          // Projects Name level
          if (globalSearchInput.getProjectsName() != null
              && globalSearchInput.getProjectsName().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_PROJECTS_NAME,
                    globalSearchInput.getProjectsName()));
          }
          // Avg Rating level
          if (globalSearchInput.getAvgRating() != null
              && globalSearchInput.getAvgRating().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_AVG_RATING,
                    globalSearchInput.getAvgRating()));
          }

          // Tags Field LEVEL
          if (globalSearchInput.getTags() != null && globalSearchInput.getTags().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_FIELD_LEVEL_TAGS,
                    globalSearchInput.getTags()));
          }
          getQueryOnMustParameters(globalSearchInput, globalQueryBuilder);
        }

        // Condition-2
        // IF
        // globalSearchInput.getSearchInTypes().contains(SearchInType.DATASOURCE)
        // then
        if (globalSearchInput.getSearchInTypes() != null
            && globalSearchInput.getSearchInTypes().contains(SearchInType.DATASOURCE)) {
          // Type of DOC like DATASOURCE
          boolQueryDsBuilder.must(
              QueryBuilders.termQuery(
                  GlobalSearchCosntants.QUERY_FIELD_TYPE, SearchInType.DATASOURCE.toString()));

          // Category
          if ((globalSearchInput.getCategory() != null)
              && globalSearchInput.getCategory().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_CATEGORY, globalSearchInput.getCategory()));
          }
          // SubCategory
          if ((globalSearchInput.getSubCategory() != null)
              && globalSearchInput.getSubCategory().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_SUB_CATEGORY, globalSearchInput.getSubCategory()));
          }

          // Source type level
          if (globalSearchInput.getSourceType() != null
              && globalSearchInput.getSourceType().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_SOURCE_TYPE,
                    globalSearchInput.getSourceType()));
          }

          // Data At Rest level
          if (globalSearchInput.getDataAtRest() != null
              && globalSearchInput.getDataAtRest().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_DATA_AT_REST,
                    globalSearchInput.getDataAtRest()));
          }

          // Load Strategy level
          if (globalSearchInput.getLoadStrategy() != null
              && globalSearchInput.getLoadStrategy().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_LOAD_STRATEGY,
                    globalSearchInput.getLoadStrategy()));
          }
          // Data source repo level
          if (globalSearchInput.getDataRepo() != null
              && globalSearchInput.getDataRepo().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_DATA_REPO, globalSearchInput.getDataRepo()));
          }
          // User group level
          if (globalSearchInput.getUserGroup() != null
              && globalSearchInput.getUserGroup().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_USER_GROUP,
                    globalSearchInput.getUserGroup()));
          }
          // Projects Name level
          if (globalSearchInput.getProjectsName() != null
              && globalSearchInput.getProjectsName().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_PROJECTS_NAME,
                    globalSearchInput.getProjectsName()));
          }
          // Avg Rating level
          if (globalSearchInput.getAvgRating() != null
              && globalSearchInput.getAvgRating().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_AVG_RATING,
                    globalSearchInput.getAvgRating()));
          }

          // Tags DS LEVEL
          if (globalSearchInput.getTags() != null && globalSearchInput.getTags().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_TAGS, globalSearchInput.getTags()));
          }

          // Tags Field LEVEL
          if (globalSearchInput.getTags() != null && globalSearchInput.getTags().size() > 0) {
            globalQueryBuilder.must(
                QueryBuilders.termsQuery(
                    GlobalSearchCosntants.QUERY_FIELD_FIELD_LEVEL_TAGS,
                    globalSearchInput.getTags()));
          }

          getQueryOnMustParameters(globalSearchInput, boolQueryDsBuilder);

          globalQueryBuilder.should(boolQueryDsBuilder);
        }

        // Condition-2
        // IF
        // globalSearchInput.getSearchInTypes().contains(SearchInType.FLOW)
        // then
        BoolQueryBuilder boolQueryFlowBuilder = QueryBuilders.boolQuery();
        if (globalSearchInput.getSearchInTypes() != null
            && globalSearchInput.getSearchInTypes().contains(SearchInType.FLOW)) {
          boolQueryFlowBuilder.must(
              QueryBuilders.termQuery(
                  GlobalSearchCosntants.QUERY_FIELD_TYPE, SearchInType.FLOW.toString()));

          getQueryOnMustParameters(globalSearchInput, boolQueryFlowBuilder);
          globalQueryBuilder.should(boolQueryFlowBuilder);
        }

        globalQueryBuilder.minimumNumberShouldMatch(1);

        // Query string of datasource
        if (globalSearchInput.getQueryString() != null
            && !TextUtils.isEmpty(globalSearchInput.getQueryString())
            && globalSearchInput.getQueryString().length() >= 0) {
          BoolQueryBuilder dsAppQueryBuilder = QueryBuilders.boolQuery();
          getFilterOnQuery(globalSearchInput, dsAppQueryBuilder);
          globalQueryBuilder.must(dsAppQueryBuilder);
        }

        // Column Name
        if (globalSearchInput.getColumnName() != null
            && !TextUtils.isEmpty(globalSearchInput.getColumnName())
            && globalSearchInput.getColumnName().length() >= 0) {
          BoolFilterBuilder flBuilder = getColumnNameFilter(globalSearchInput);
          mainQueryBuilder = QueryBuilders.filteredQuery(globalQueryBuilder, flBuilder);
        } else {
          mainQueryBuilder =
              QueryBuilders.filteredQuery(globalQueryBuilder, FilterBuilders.matchAllFilter());
        }
        try {
          response =
              ESClientFactory.getInstance()
                  .prepareSearch(INDEX_NAME)
                  .setTypes(INDEX_TYPE)
                  .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                  .setQuery(mainQueryBuilder)
                  .addAggregation(getTypeAggregationBuilder())
                  .addAggregation(getCreatedByAggregationBuilder())
                  .addAggregation(getCategoryAggregationBuilder())
                  .addAggregation(getSubCategoryAggregationBuilder())
                  .addAggregation(getCreatedDateAggregationBuilder())
                  .addAggregation(getSourceTypeAggregationBuilder())
                  .addAggregation(getDataAtRestAggregationBuilder())
                  .addAggregation(getLoadStrategyAggregationBuilder())
                  .addAggregation(getDataRepoAggregationBuilder())
                  .addAggregation(getUserGroupAggregationBuilder())
                  .addAggregation(getProjectsNameAggregationBuilder())
                  .addAggregation(getAvgRatingAggregationBuilder())
                  .setFrom(globalSearchInput.getFetchRecordsFrm())
                  .setSize(globalSearchInput.getSizeOfRecords())
                  .setExplain(true)
                  .execute()
                  .actionGet();
        } catch (NoNodeAvailableException e) {
          logger.error(
              "Please check elastic search is working or not because No node available NoNodeAvailableException while creating document");
        }
      }

      if (response != null && response.getHits().totalHits() >= 0) {
        getFinalReponseBySearchFilter(globalSearchInput, globalSearchResult, response);
      } else {
        globalSearchResult.setMessage(GlobalSearchCosntants.NO_RECORDS_FOUND);
      }
    } catch (Exception e) {
      logger.error("{}", e);
      globalSearchResult.setMessage(
          GlobalSearchCosntants.SOME_PROBLEM_IN_PROCESSING_RESPONSE + EMPTY_SPACE + e.getMessage());
    } finally {
      ESClientFactory.shutdown();
    }
    return globalSearchResult;
  }

  private void getQueryOnMustParameters(
      GlobalSearchInput globalSearchInput, BoolQueryBuilder boolQueryFlowBuilder) {
    // GroupId
    if (globalSearchInput.getGroupBy() != null && globalSearchInput.getGroupBy() > 0) {
      boolQueryFlowBuilder.must(
          QueryBuilders.termQuery(QUERY_FIELD_GROUP_ID, globalSearchInput.getGroupBy()));
    }

    // Created By
    if ((globalSearchInput.getCreatedBy() != null)
        && (globalSearchInput.getCreatedBy().size() > 0)) {
      boolQueryFlowBuilder.must(
          QueryBuilders.termsQuery(QUERY_FIELD_CREATED_BY, globalSearchInput.getCreatedBy()));
    }

    // Name of flow & datasource
    if (globalSearchInput.getName() != null && globalSearchInput.getName().size() > 0) {
      boolQueryFlowBuilder.must(
          QueryBuilders.termsQuery(QUERY_FIELD_FLOW_DS_NAME, globalSearchInput.getName()));
    }

    // General search string
    if (globalSearchInput.getGeneralSearchString() != null
        && !TextUtils.isEmpty(globalSearchInput.getGeneralSearchString())) {
      boolQueryFlowBuilder.must(
          QueryBuilders.queryStringQuery(globalSearchInput.getGeneralSearchString())
              .defaultField(QUERY_ALL));
    }

    // Created within days
    if (globalSearchInput.getCreatedWithinDays() != null
        && globalSearchInput.getCreatedWithinDays() > 0) {
      String formattedDate = null;
      SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_FOR_QUERY);
      try {
        Date date = DateUtils.addDays(new Date(), -globalSearchInput.getCreatedWithinDays());
        formattedDate = dateFormat.format(date);
      } catch (Exception e) {
        //				e.printStackTrace();
        formattedDate = dateFormat.format(new Date());
      }
      boolQueryFlowBuilder.must(
          QueryBuilders.rangeQuery(QUERY_FIELD_CREATED_DATE)
              .gte(formattedDate)
              .includeLower(true)
              .includeUpper(false));
    }
  }

  private void getFilterOnQuery(
      GlobalSearchInput globalSearchInput, BoolQueryBuilder dsAppQueryBuilder) {
    Boolean isAdded = true;
    if (globalSearchInput.getSearchInTypes() == null
        || globalSearchInput.getSearchInTypes().size() == 2) {
      dsAppQueryBuilder.should(
          QueryBuilders.queryStringQuery(globalSearchInput.getQueryString())
              .defaultField(QUERY_FIELD_DS_QUERY));
      dsAppQueryBuilder.should(
          QueryBuilders.queryStringQuery(globalSearchInput.getQueryString())
              .defaultField(QUERY_FIELD_FLOW_QUERY));
      isAdded = false;
    }
    if (globalSearchInput.getSearchInTypes() != null
        && globalSearchInput.getSearchInTypes().contains(SearchInType.DATASOURCE)
        && isAdded) {
      dsAppQueryBuilder.should(
          QueryBuilders.queryStringQuery(globalSearchInput.getQueryString())
              .defaultField(QUERY_FIELD_DS_QUERY));
    }
    if (globalSearchInput.getSearchInTypes() != null
        && globalSearchInput.getSearchInTypes().contains(SearchInType.FLOW)
        && isAdded) {
      dsAppQueryBuilder.should(
          QueryBuilders.queryStringQuery(globalSearchInput.getQueryString())
              .defaultField(QUERY_FIELD_FLOW_QUERY));
    }
  }

  private BoolFilterBuilder getColumnNameFilter(GlobalSearchInput globalSearchInput) {
    QueryFilterBuilder flowlInTFB =
        new QueryFilterBuilder(
            QueryBuilders.queryStringQuery(globalSearchInput.getColumnName())
                .defaultField(QUERY_FIELD_FLOW_INPUT_FIELDS_NAME));
    QueryFilterBuilder flowOutTFB =
        new QueryFilterBuilder(
            QueryBuilders.queryStringQuery(globalSearchInput.getColumnName())
                .defaultField(QUERY_FIELD_FLOW_OUTPUT_FIELDS_NAME));
    QueryFilterBuilder dsTFB =
        new QueryFilterBuilder(
            QueryBuilders.queryStringQuery(globalSearchInput.getColumnName())
                .defaultField(QUERY_FIELD_DS_FIELDS_NAME));

    BoolFilterBuilder flBuilder = FilterBuilders.boolFilter();
    Boolean isAdded = true;
    if (globalSearchInput.getSearchInTypes() == null
        || (globalSearchInput.getSearchInTypes() != null
            && (globalSearchInput.getSearchInTypes().size() == 2
                || globalSearchInput.getSearchInTypes().size() == 0))) {
      flBuilder.should(dsTFB, flowlInTFB, flowOutTFB);
      isAdded = false;
    }
    if (globalSearchInput.getSearchInTypes() != null
        && globalSearchInput.getSearchInTypes().contains(SearchInType.DATASOURCE)
        && isAdded) {
      flBuilder.should(dsTFB);
    }
    if (globalSearchInput.getSearchInTypes() != null
        && globalSearchInput.getSearchInTypes().contains(SearchInType.FLOW)
        && isAdded) {
      flBuilder.should(flowlInTFB, flowOutTFB);
    }
    return flBuilder;
  }

  private void getFinalReponseBySearchFilter(
      GlobalSearchInput globalSearchInput,
      GlobalSearchResult globalSearchResult,
      SearchResponse response) {
    globalSearchResult.setTotalCount(response.getHits().totalHits());
    // int totalHits = (int)response.getHits().totalHits();
    List<Record> listOfRecords = new ArrayList<Record>();
    for (SearchHit hit : response.getHits().getHits()) {
      String sourceAsString = hit.getSourceAsString();
      if (sourceAsString != null) {
        try {
          Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT_FOR_GSON_LOAD).create();
          Source source = gson.fromJson(sourceAsString, Source.class);
          List<String> nodes = new ArrayList<>();

          if (globalSearchInput != null
              && source.getNodes() != null
              && source.getNodes().size() > 0) {
            // Filtration of nodes done here if column name
            // present in that node then only we add name in
            // list
            boolean isColumnNameMatched = false;

            boolean isColumnNameEmpty =
                globalSearchInput.getColumnName() != null
                    && !TextUtils.isEmpty(globalSearchInput.getColumnName())
                    && globalSearchInput.getColumnName().length() >= 0;
            boolean isQueryStringIsEmpty =
                globalSearchInput.getQueryString() != null
                    && !TextUtils.isEmpty(globalSearchInput.getQueryString())
                    && globalSearchInput.getQueryString().length() >= 0;
            if (isColumnNameEmpty || isQueryStringIsEmpty) {
              for (Node node : source.getNodes()) {
                if (isColumnNameEmpty) {
                  for (Input in : node.getInput()) {
                    for (InputDataFile inDatafile : in.getInputDataFiles()) {
                      for (NodeInField field : inDatafile.getNodeInFields()) {
                        if (field.getName().equalsIgnoreCase(globalSearchInput.getColumnName())) {
                          isColumnNameMatched = true;
                          break;
                        }
                      }
                    }
                  }
                  if (!isColumnNameMatched) {
                    for (Output in : node.getOutput()) {
                      for (OutputDataFile inDatafile : in.getOutputDataFiles()) {
                        for (NodeOutField field : inDatafile.getNodeOutFields()) {
                          if (field.getName().equalsIgnoreCase(globalSearchInput.getColumnName())) {
                            isColumnNameMatched = true;
                            break;
                          }
                        }
                      }
                    }
                  }
                }
                if (isQueryStringIsEmpty) {
                  for (Process process :node.getProcess()) {
                    for (BaseProcess baseProcess : process.getBaseProcesses()) {
                      if (baseProcess.getCustomQuery() != null
                          && baseProcess
                              .getCustomQuery()
                              .getProcessQuery()
                              .contains(globalSearchInput.getQueryString())) {
                        isColumnNameMatched = true;
                        break;
                      }
                    }
                  }
                }

                if (isColumnNameMatched) {
                  nodes.add(node.getNodeName());
                  isColumnNameMatched = false;
                }
              }
            } else {
              if (source.getNodes() != null && source.getNodes().size() > 0)
                for (Node node : source.getNodes()) {
                  nodes.add(node.getNodeName());
                }
            }
          }

          Record record =
              new Record(
                  Long.valueOf(source.getDsOrFlowId()),
                  source.getName(),
                  nodes,
                  source.getCreatedBy(),
                  source.getCreatedDate());

          if (source.getType().equalsIgnoreCase(SearchInType.DATASOURCE.getSearchTypes())) {
            record.setSearchInTypes(SearchInType.DATASOURCE);
            record.setCategory(source.getCategory());
            record.setSubcategory(source.getSubCategory());
          }

          
          listOfRecords.add(record);
        } catch (Exception e) {
          logger.error("Error in response parsing of global search " + e.getMessage());
          //					e.printStackTrace();
        }
      }
    }

    globalSearchResult.setRecords(listOfRecords);
    DisplayFacades datasourceAndFlowType;
    Map<String, List<Bucket>> bucketMap = new HashMap<String, List<Bucket>>();

    Terms termsType = response.getAggregations().get("datasource_and_flow.type");
    bucketMap.put("type", getBucket(termsType));

    Terms termsCreatedBy = response.getAggregations().get("datasource_and_flow.created_by");
    bucketMap.put("createdBy", getBucket(termsCreatedBy));

    Terms termsCategory = response.getAggregations().get("datasource_and_flow.category");
    bucketMap.put("category", getBucket(termsCategory));

    Terms termsSubCategory = response.getAggregations().get("datasource_and_flow.sub_category");
    bucketMap.put("sub_category", getBucket(termsSubCategory));

    Terms termsCreatedDate = response.getAggregations().get("datasource_and_flow.created_date");
    bucketMap.put("created_date", getBucket(termsCreatedDate));

    Terms termsSourceType = response.getAggregations().get("datasource_and_flow.source_type");
    bucketMap.put("source_type", getBucket(termsSourceType));

    Terms termsDataAtRest = response.getAggregations().get("datasource_and_flow.data_at_rest");
    bucketMap.put("data_at_rest", getBucket(termsDataAtRest));

    Terms termsLoadStrategy = response.getAggregations().get("datasource_and_flow.load_strategy");
    bucketMap.put("load_strategy", getBucket(termsLoadStrategy));

    Terms termsDataRepo = response.getAggregations().get("datasource_and_flow.data_repo");
    bucketMap.put("data_repo", getBucket(termsDataRepo));

    Terms termsUserGroup = response.getAggregations().get("datasource_and_flow.user_group");
    bucketMap.put("user_group", getBucket(termsUserGroup));

    Terms termsProjectsName = response.getAggregations().get("datasource_and_flow.projects_name");
    bucketMap.put("projects_name", getBucket(termsProjectsName));

    Terms termsAvgRating = response.getAggregations().get("datasource_and_flow.avg_rating");
    bucketMap.put("avg_rating", getBucket(termsAvgRating));

    datasourceAndFlowType =
        new DisplayFacades(
            termsType.getDocCountError(), termsType.getSumOfOtherDocCounts(), bucketMap);
    globalSearchResult.setDataSourceAndFlowType(datasourceAndFlowType);
  }

  private List<Bucket> getBucket(Terms terms) {
    Collection<Terms.Bucket> buckets;
    List<Bucket> bucketList = new ArrayList<Bucket>(1);
    if (terms != null && null != terms.getBuckets() && terms.getBuckets().size() > 0) {
      int sizeOfBuckets = terms.getBuckets().size();
      if (terms.getBuckets() != null && sizeOfBuckets > 0) {
        buckets = terms.getBuckets();
        bucketList = new ArrayList<Bucket>(sizeOfBuckets);
        Iterator<Terms.Bucket> iterator = buckets.iterator();
        while (iterator.hasNext()) {
          Terms.Bucket bucket = (Terms.Bucket) iterator.next();
          bucketList.add(new Bucket(bucket.getKeyAsText().string(), bucket.getDocCount()));
        }
      } else {
        bucketList = new ArrayList<>(1);
      }
    }
    return bucketList;
  }

}

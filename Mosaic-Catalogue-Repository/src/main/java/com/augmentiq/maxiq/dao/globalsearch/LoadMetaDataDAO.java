package com.augmentiq.maxiq.dao.globalsearch;

import java.util.List;

import com.augmentiq.maxiq.entity.model.configuration.bean.SearchInType;
import com.augmentiq.maxiq.model.globalsearch.SearchIndexDTO;

/** @author Ajinkya Marathe Required to fetch & update operations from mysql database */

public interface LoadMetaDataDAO {
  public List<SearchIndexDTO> getAllDataSources();

  //public List<SearchIndexDTO> getAllFlowsWithNodes();

  public List<SearchIndexDTO> getAllDatasourcesAndFlow();

  public void updateIsUpdatedInIndex(Long id, SearchInType type);
}

package com.augmentiq.maxiq.dao.globalsearch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.StackTraceReader;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.SearchInType;
import com.augmentiq.maxiq.model.globalsearch.SearchIndexDTO;
import com.augmentiq.maxiq.model.globalsearch.dto.NodeDTO;
import com.augmentiq.maxiq.models.apps.domain.Application;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.google.gson.Gson;

/** @author Ajinkya Marathe Loading records & updating from database */
public class LoadMetaDataDAOImpl implements LoadMetaDataDAO {

  private final Logger logger = LoggerFactory.getLogger(GlobalSearchIndexDAOImpl.class);

  @Override
  public List<SearchIndexDTO> getAllDataSources() {
    Collection<DataSourceInstance> domains = null;
    List<SearchIndexDTO> list = null;
    try {
      domains = DataSourceInstanceDao.getAllModifiedDataSourceInstances();
    } catch (ConnectionException | SystemException e) {
      // e.printStackTrace();
      logger.error("problem with LoadMetaDataDAIImpl while getting all datasources");
      logger.error(StackTraceReader.stringFromStackTrace(e));
    }
    if (domains != null) {
      list = new ArrayList<SearchIndexDTO>(domains.size());
      for (DataSourceInstance dataSourceInstance : domains) {
        if (dataSourceInstance != null && !StringUtils.isEmpty(dataSourceInstance.getDataSourceName())) {
         
          list.add(
              new SearchIndexDTO(
                  dataSourceInstance.getDataSourceId(),
                  dataSourceInstance.getDataSourceName(),
                  SearchInType.DATASOURCE,
                  dataSourceInstance.getCreatedBy(),
                  dataSourceInstance.getCreatedDate(),
                  dataSourceInstance.getGroupId(),
                  dataSourceInstance.getCategory(),
                  dataSourceInstance.getSubCategory(),
                  dataSourceInstance.getDataSourceType(),
                  "",
                  dataSourceInstance.getDataRepoName(),
                  "",
                  "",
                  DataSourceInstanceDao.getAvgRatingFromDataSourceId(
                          dataSourceInstance.getDataSourceId())
                      + ""));
        }
      }
    }
    return list;
  }

/*  @Override
  public List<SearchIndexDTO> getAllFlowsWithNodes() {
    Gson gson = new Gson();
    List<Application> applications = null;
    try {
      applications = MaxIQAppService.getApplications();
    } catch (SystemException e) {
      //	    e.printStackTrace();
      logger.error("problem with LoadMetaDataDAIImpl while getting all Flow with nodes");
      logger.error(StackTraceReader.stringFromStackTrace(e));
    }

    List<SearchIndexDTO> list = new ArrayList<SearchIndexDTO>();
    CanvasWorkFlowService canvasWFS = new CanvasWorkFlowService();

    if (applications != null) {
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " <<  getDateSourcesList() "
              + ParamUtils.getString(applications));
      for (Application a : applications) {
        if (a != null && !StringUtils.isEmpty(a.getAppName())) {
          String graphCanvasDataJSON = null;
          try {
            graphCanvasDataJSON = canvasWFS.getGraphCanvasData(a.getAppId());
          } catch (SystemException e) {
            //			e.printStackTrace();
            logger.error(
                "problem with LoadMetaDataDAIImpl while getting getGraphCanvasData "
                    + a.getAppId()
                    + e.getStackTrace());
          }
          String projectName = "";
          try {
            projectName = ProjectsBso.getProjectsDetails(a.getOwnerProjectId()).getProjectName();
          } catch (Exception e) {
          }

          Date date = null;
          if (a.getCreateSt() != null) {
            date = new Date(a.getCreateSt());
          }

          if (graphCanvasDataJSON != null) {
            NodeDTO nodeDTO = gson.fromJson(graphCanvasDataJSON, NodeDTO.class);

            if (nodeDTO != null && !StringUtils.isEmpty(a.getAppName())) {
              list.add(
                  new SearchIndexDTO(
                      a.getAppId(),
                      a.getAppName(),
                      SearchInType.FLOW,
                      a.getCreatedBy(),
                      date,
                      a.getGroupId(),
                      nodeDTO,
                      a.getAppCombiner(),
                      projectName,
                      ""));
            }
          } else {
            list.add(
                new SearchIndexDTO(
                    a.getAppId(),
                    a.getAppName(),
                    SearchInType.FLOW,
                    a.getCreatedBy(),
                    date,
                    a.getGroupId(),
                    new NodeDTO(),
                    a.getAppCombiner(),
                    projectName,
                    ""));
          }
        }
      }
    }
    return list;
  }*/

  @Override
  public List<SearchIndexDTO> getAllDatasourcesAndFlow() {
    List<SearchIndexDTO> listOfFLowNDataSource = getAllDataSources();
    if (listOfFLowNDataSource == null) {
      listOfFLowNDataSource = new ArrayList<SearchIndexDTO>();
    }
    //listOfFLowNDataSource.addAll(getAllFlowsWithNodes());
    return listOfFLowNDataSource;
  }

  @Override
  public void updateIsUpdatedInIndex(Long id, SearchInType type) {
    Map<String, String> values = new HashMap<String, String>();
    values.put(
        QueryConstants.GlobalSearch.IS_MODIFIED, QueryConstants.GlobalSearch.IS_MODIFIED_FALSE);
    try {
      if (type == SearchInType.DATASOURCE) {
        DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(id, values);
      }
      /*if (type == SearchInType.FLOW) {
        MaxIQAppService.updateApplicationDetails(id, values);
      }*/
    } catch (SystemException e) {
      //	    e.printStackTrace();
      logger.error(
          "problem with LoadMetaDataDAIImpl while update isModified status in "
              + type
              + " & Id -"
              + id
              + e.getStackTrace());
    }
  }
}
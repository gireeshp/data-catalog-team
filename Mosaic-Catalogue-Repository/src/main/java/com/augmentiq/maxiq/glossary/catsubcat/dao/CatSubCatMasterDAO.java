package com.augmentiq.maxiq.glossary.catsubcat.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.CategorySubCatMaster;
import com.augmentiq.maxiq.entity.model.glossary.catsubcat.CatSubCatMasterDTO;

public class CatSubCatMasterDAO {

  private static final Logger logger = LoggerFactory.getLogger(CatSubCatMasterDAO.class);

  /**
   * @param CategorySubCatMaster - object of CategorySubCatMaster with initialized by front end
   * @throws Exception
   */
  public void insertCategory(CategorySubCatMaster catOrSubCatMaster) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> insertCategory {} ", catOrSubCatMaster);
    MySqlOperations.insert(
        catOrSubCatMaster, HbaseUtility.getIdAnnotationForClass(CategorySubCatMaster.class));
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << insertCategory done");
  }

  /**
   * @param query - will soft delete the record
   * @param params
   * @throws Exception
   */
  public void deleteFromCategoryAndSubCat(String query, Map<String, Object> params)
      throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> deleteFromCategoryAndSubCat {}", query);
    MySqlOperations.executeQuery(query, params);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << deleteFromCategoryAndSubCat done");
  }

  /**
   * @param query - Any string query
   * @return List of all CatSubCatMasterDTO
   * @throws Exception
   */
  public List<CatSubCatMasterDTO> getAllOfCategorySubCatMasterDTO(String query) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getAllOfCategorySubCatMaster {}", query);
    return MySqlOperations.scanWithSqlQuery(CatSubCatMasterDTO.class, query, null);
  }

  /**
   * @param query - Any string query
   * @return List of all CategorySubCatMaster
   * @throws Exception
   */
  public List<CategorySubCatMaster> getAllOfCategorySubCatMaster(String query) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getAllOfCategorySubCatMaster {}", query);
    return MySqlOperations.scanWithSqlQuery(CategorySubCatMaster.class, query, null);
  }

  /**
   * @param queryMap - in which you can add your conditions
   * @return
   * @throws Exception
   */
  public CategorySubCatMaster getCategoryAndSubCategoryById(Map<String, Object> queryMap)
      throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getCategoryAndSubCategoryById {}", queryMap);
    return MySqlOperations.scanOneForQuery(CategorySubCatMaster.class, queryMap);
  }

  /**
   * @param queryMap - in which you can add your conditions
   * @return
   * @throws Exception
   */
  public boolean getCategoryAndSubCategoryByName(Map<String, Object> queryMap) throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getCategoryAndSubCategoryByName()", queryMap);
    CategorySubCatMaster catSubData =
        MySqlOperations.scanOneForQuery(CategorySubCatMaster.class, queryMap);
    if (catSubData != null
        && StringUtils.equalsIgnoreCase(
            queryMap.get(QueryConstants.CatSubCatMaster.CAT_OR_SUBCAT_NAME).toString(),
            catSubData.getCatOrSubCatName())) {
      return true;
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getCategoryAndSubCategoryByName()");
    return false;
  }
}

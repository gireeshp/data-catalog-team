package com.augmentiq.maxiq.object.request.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.configuration.enums.AccessTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataRequestEnum;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRequest;
import com.augmentiq.maxiq.entity.model.configuration.bean.GroupObjectMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.MyCollection;
import com.augmentiq.maxiq.entity.model.configuration.bean.Notification;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserObjectMapping;
import com.augmentiq.maxiq.model.object.request.DataRequestDTO;

public class ObjectRequestDAO {

  private static final Logger logger = LoggerFactory.getLogger(ObjectRequestDAO.class);

  /**
   * @param dataRequestFromUi
   * @param userId
   * @param groupId
   * @param isRequestedForGroup
   * @throws Exception
   */
  public static void createObjectRequest(
      DataRequestDTO dataRequestFromUi, Long userId, Boolean isRequestedForGroup) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> createDataSourceRequest()"
            + ParamUtils.getString(dataRequestFromUi, userId, isRequestedForGroup));
    if (dataRequestFromUi.getGroupId() != null) {
      String sql =
          "select * from data_request where "
              + QueryConstants.DataRequest.OBJECT_ID
              + " = "
              + dataRequestFromUi.getObjectId()
              + " AND "
              + QueryConstants.DataRequest.GROUP_ID
              + " = "
              + dataRequestFromUi.getGroupId()
              + "  AND "
              + QueryConstants.DataRequest.STATUS
              + " = 0 LIMIT 1";
      DataRequest dataRequest = MySqlOperations.scanOneForSqlQuery(DataRequest.class, sql);
      if (dataRequest != null) {
        ExceptionsMessanger.throwException(new SystemException(), "ERR_165");
      }
    }
    insertObjectRequestInDb(dataRequestFromUi, userId);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << createDataSourceRequest()"
            + ParamUtils.getString(dataRequestFromUi, userId, isRequestedForGroup));
  }

  public static void insertObjectRequestInDb(DataRequestDTO dataRequestFromUi, Long userId)
      throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> insertObjectRequestInDb() {}", userId);
    DataRequest dataRequest = new DataRequest();
    dataRequest.setObjectId(dataRequestFromUi.getObjectId());
    dataRequest.setObjectType(dataRequestFromUi.getObjectType());
    dataRequest.setRequestedBy(userId);
    dataRequest.setRequestedAccess(dataRequestFromUi.getRequestedAccess().getValue());
    dataRequest.setJustification(dataRequestFromUi.getJustification());
    dataRequest.setLast_modified_by(userId);
    dataRequest.setModified_date(0L);
    if (dataRequestFromUi.getGroupId() == null) {
      dataRequest.setGroup_id(-1L);
    } else {
      dataRequest.setGroup_id(dataRequestFromUi.getGroupId());
    }
    dataRequest.setCreated_date(System.currentTimeMillis());
    dataRequest.setStatus(DataRequestEnum.PENDING.getValue());
    dataRequest.setRequested_user_id(dataRequestFromUi.getRequestedUserId());
    MySqlOperations.insert(dataRequest, QueryConstants.DataRequest.ID);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << insertObjectRequestInDb() {}", userId);
  }

  /**
   * @param dataSourceId - datasource id selected by user
   * @param from - unqUserId from @see ApplicationUser (userId)Cookies OR session
   * @param status - @see DataRequestEnum
   * @param lastModifiedBy - unqUserId from @see ApplicationUser of that user who is
   *     Accepted/Rejected the request
   * @throws Exception
   */
  public static void updateRequestDataSourceRequest(
      Long dataSourceId,
      Long groupId,
      DataRequestEnum status,
      Long lastModifiedBy,
      String rejectJustification)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> updateRequestDataSourceRequest(Long dataSourceId, Long from, DataRequestEnum status, Long lastModifiedBy)"
            + ParamUtils.getString(dataSourceId, groupId, status, lastModifiedBy));
    String updateQuery = "";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    if (status.getValue() == -1) {
      updateQuery =
          "update data_request set status =?, last_modified_by=? , rejected_justification = ? where datasourceid=? and  group_id=?";
      query.put("status", status.getValue());
      query.put("last_modified_by", lastModifiedBy);
      query.put("rejected_justification", rejectJustification);
      query.put("datasourceid", dataSourceId);
      query.put("group_id", groupId);
    } else {
      /*updateQuery = "update data_request set status =" + status.getValue() + ", last_modified_by="
      + lastModifiedBy + " where datasourceid=" + dataSourceId + " and  group_id=" + groupId;*/
      updateQuery =
          "update data_request set status =?, last_modified_by=? where datasourceid=? and  group_id=?";
      query.put("status", status.getValue());
      query.put("last_modified_by", lastModifiedBy);
      //	query.put("rejected_justification", rejectJustification);
      query.put("datasourceid", dataSourceId);
      query.put("group_id", groupId);
    }

    MySqlOperations.executeQuery(updateQuery, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << updateRequestDataSourceRequest(Long dataSourceId, Long from, DataRequestEnum status, Long lastModifiedBy)"
            + ParamUtils.getString(dataSourceId, groupId, status, lastModifiedBy));
  }

  /**
   * @param userId
   * @return
   * @throws Exception
   */
  public static List<DataRequest> getAllAcceptedRequests(Long userId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllAcceptedDataSourceOfMyCollection(Long from)"
            + ParamUtils.getString(userId));
    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.DataRequest.REQUESTED_BY, userId);
    queryMap.put(QueryConstants.DataRequest.STATUS, DataRequestEnum.ACCEPT.getValue());
    List<DataRequest> result = MySqlOperations.scanForQuery(DataRequest.class, queryMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getAllAcceptedDataSourceOfMyCollection(Long from)"
            + ParamUtils.getString(result));
    return result;
  }

  /**
   * @param userId
   * @param groupId
   * @param objectId
   * @return
   * @throws Exception
   */
  public static DataRequestEnum getStausOfObjectRequest(
      Long userId, Long objectId, String objectType) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllDataSourceOfMyCollection(Long from, Long dataSourceId)"
            + ParamUtils.getString(userId, objectId, objectType));

    Map<String, Object> queryMapForUser = new LinkedHashMap<String, Object>();
    queryMapForUser.put(QueryConstants.DataRequest.REQUESTED_BY, userId);
    queryMapForUser.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    queryMapForUser.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    DataRequest dataRequestForUser =
        MySqlOperations.scanOneForQuery(DataRequest.class, queryMapForUser);

    if (null == dataRequestForUser) {
      return DataRequestEnum.get(2);
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getAllDataSourceOfMyCollection(Long from, Long dataSourceId)"
            + ParamUtils.getString(dataRequestForUser));
    return DataRequestEnum.get(dataRequestForUser.getStatus());
  }
  public static DataRequestEnum getAddedMycollectionDataSource(
	      Long userId, Long objectId, String objectType) throws Exception {
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB
	            + " >> getAddedMycollectionDataSource(Long from, Long dataSourceId)"
	            + ParamUtils.getString(userId, objectId, objectType));

	    Map<String, Object> queryMapForUser = new LinkedHashMap<String, Object>();
	    queryMapForUser.put(QueryConstants.DatasourceServiceApimanager.USER_ID, userId);
	    queryMapForUser.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
	    queryMapForUser.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
	    MyCollection myCollection =
	        MySqlOperations.scanOneForQuery(MyCollection.class, queryMapForUser);

	    if (null == myCollection) {
	      return DataRequestEnum.get(2);
	    }

	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB
	            + " << getAddedMycollectionDataSource(Long from, Long dataSourceId)"
	            + ParamUtils.getString(myCollection));
	    return DataRequestEnum.get(myCollection.getStatus());
	  }

  /**
   * @param requestId
   * @return
   * @throws Exception
   */
  public static DataRequest getDataRequestById(Long requestId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getDataRequestById(Long from, Long dataSourceId)"
            + ParamUtils.getString(requestId));
    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(Sequences.ID, requestId);
    DataRequest dataRequest = MySqlOperations.scanOneForQuery(DataRequest.class, queryMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getDataRequestById(Long from, Long dataSourceId)"
            + ParamUtils.getString(dataRequest));
    return dataRequest;
  }

  /**
   * @param userId
   * @param groupId
   * @return
   * @throws Exception
   */
  public static List<DataRequest> getObjectsForApproval(Long userId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllAcceptedDataSourceOfMyCollection(Long userId)"
            + ParamUtils.getString(userId));
    String query =
        "select * from data_request where "
            + QueryConstants.DataRequest.REQUESTED_USER_ID
            + " = ?"
            + " order by case when status = '0' "
            + "then '5' when status ='1' then '6' else '7' end, case when modified_date = 0 or modified_date "
            + "is null then created_date else modified_date end desc";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put(QueryConstants.DataRequest.REQUESTED_USER_ID, userId);
    List<DataRequest> result =
        MySqlOperations.scanWithSqlQuery(DataRequest.class, query, objectMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getAllAcceptedDataSourceOfMyCollection(Long userId)"
            + ParamUtils.getString(result));
    return result;
  }

  /**
   * @param userId
   * @param groupId
   * @return
   * @throws Exception
   */
  public static List<DataRequest> getAllRequestedObjects(Long userId, Long groupId)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllRequestedDataSource(Long userId,Long groupId)"
            + ParamUtils.getString(userId, groupId));
    String query =
        "select * from "
            + Sequences.DATA_REQUEST
            + " where "
            + QueryConstants.DataRequest.REQUESTED_BY
            + "= ? order by case when status = '0' "
            + "then '5' when status ='1' then '6' else '7' end, case when modified_date = 0 or modified_date "
            + "is null then created_date else modified_date end desc";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("QueryConstants.DataRequest.REQUESTED_BY", userId);
    List<DataRequest> result =
        MySqlOperations.scanWithSqlQuery(DataRequest.class, query, objectMap);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getAllRequestedDataSource(Long userId,Long groupId)"
            + ParamUtils.getString(result));
    return result;
  }

  public static void acceptDataSourceRequest(Long requestId, String justification)
      throws SystemException {
    String sql =
        "update data_request set status = '1', modified_date='"
            + System.currentTimeMillis()
            + "' , justification = ? where id=?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("justification", justification);
    query.put("id", requestId);
    MySqlOperations.executeQuery(sql, query);
  }

  public static void rejectObjectRequest(Long requestId, String justification)
      throws SystemException {
    String sql =
        "update data_request set status = '-1' , rejected_justification = ?, modified_date='"
            + System.currentTimeMillis()
            + "' where id= ?";
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("rejected_justification", justification);
    query.put("id", requestId);
    MySqlOperations.executeQuery(sql, query);
  }

  public static void insertGroupObjectMapping(GroupObjectMapping groupObjectMapping)
      throws SystemException {
    MySqlOperations.insert(groupObjectMapping, QueryConstants.DataRequest.ID);
  }

  public static void insertUserObjectMapping(UserObjectMapping userObjectMapping)
      throws SystemException {
    MySqlOperations.insert(userObjectMapping, QueryConstants.DataRequest.ID);
  }

  /**
   * @param userId
   * @param objectId
   * @param objectType
   * @param accessType
   * @return
   * @throws SystemException
   */
  public static Boolean isObjectSharedWithUserWithThisAccess(
      Long userId, Long objectId, String objectType, String accessType) throws SystemException {

    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.DataRequest.USERID, userId);
    queryMap.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    queryMap.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    queryMap.put(QueryConstants.Project.ACCESS_TYPE, AccessTypeEnum.valueOf(accessType).getValue());
    UserObjectMapping userObjectMapping =
        MySqlOperations.scanOneForQuery(UserObjectMapping.class, queryMap);
    if (null == userObjectMapping) {
      return false;
    }
    return true;
  }

  /**
   * @param groupId
   * @param objectId
   * @param objectType
   * @param accessType
   * @return
   * @throws SystemException
   */
  public static Boolean isObjectSharedWithGroupWithThisAccess(
      Long groupId, Long objectId, String objectType, String accessType) throws SystemException {

    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.DataRequest.GROUPID, groupId);
    queryMap.put(QueryConstants.DataRequest.OBJECT_ID, objectId);
    queryMap.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    queryMap.put(QueryConstants.Project.ACCESS_TYPE, AccessTypeEnum.valueOf(accessType).getValue());
    GroupObjectMapping groupObjectMapping =
        MySqlOperations.scanOneForQuery(GroupObjectMapping.class, queryMap);
    if (null == groupObjectMapping) {
      return false;
    }
    return true;
  }
  /**
   * @param objectType
   * @param groupId
   * @param accessType
   * @return
   * @throws SystemException
   */
  public static List<GroupObjectMapping> getObjectsSharedWithGroupWithAccessType(
      String objectType, Long groupId, String accessType) throws SystemException {

    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.DataRequest.GROUPID, groupId);
    queryMap.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    queryMap.put(QueryConstants.Project.ACCESS_TYPE, AccessTypeEnum.valueOf(accessType).getValue());
    List<GroupObjectMapping> groupObjectMappings =
        MySqlOperations.scan(GroupObjectMapping.class, queryMap);
    return groupObjectMappings;
  }

  /**
   * @param objectType
   * @param userId
   * @param accessType
   * @return
   * @throws SystemException
   */
  public static List<UserObjectMapping> getObjectsSharedWithUserWithAccessType(
      String objectType, Long userId, String accessType) throws SystemException {

    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.DataRequest.USERID, userId);
    queryMap.put(QueryConstants.DataRequest.OBJECT_TYPE, objectType);
    queryMap.put(QueryConstants.Project.ACCESS_TYPE, AccessTypeEnum.valueOf(accessType).getValue());
    List<UserObjectMapping> userObjectMappings =
        MySqlOperations.scan(UserObjectMapping.class, queryMap);
    return userObjectMappings;
  }

  /**
   * Add entry in Notification Table
   *
   * @param Notification bean as input
   * @throws SystemException
   */
  public static void setNotification(Notification notification) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> setNotification()");
    MySqlOperations.insert(notification, QueryConstants.Notification.ID);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << setNotification()");
  }

  /**
   * This method will delete the notifications of user from notification table
   *
   * @param userId - delete all object request acceptance/rejection notification for this particular
   *     userid
   * @throws SystemException
   */
  public static void deleteNotification(Long userId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> deleteNotification()" + ParamUtils.getString(userId));

    String query = "delete from notification where userId= ?";
    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put(QueryConstants.Notification.USERID, userId);

    MySqlOperations.executeQuery(query, params);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << deleteNotification()" + ParamUtils.getString(userId));
  }
}

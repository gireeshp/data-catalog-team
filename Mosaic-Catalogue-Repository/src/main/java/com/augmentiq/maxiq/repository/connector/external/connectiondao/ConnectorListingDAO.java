package com.augmentiq.maxiq.repository.connector.external.connectiondao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.core.dao.userdao.ApplicationUserDao;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;

@Repository
public class ConnectorListingDAO {
	private static final Logger logger = LoggerFactory.getLogger(ConnectorListingDAO.class);
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public List<ConnectionSources> listConnectionSource() {

		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(ConnectionSources.class);
		ProjectionList projectionList = Projections.projectionList();
		projectionList.add(Projections.property("sourceId"), "sourceId");
		projectionList.add(Projections.property("connectionType"), "connectionType");
		projectionList.add(Projections.property("sourceImagePath"), "sourceImagePath");
		criteria.setProjection(projectionList);
		criteria.setResultTransformer(Transformers.aliasToBean(ConnectionSources.class));
		List<ConnectionSources> connectionSourcesList = criteria.list();

		return connectionSourcesList;
	}

	public ConnectionSources listConnectionSourceAndSubSource(Long sourceId) {

		List<ConnectionSources> list = new ArrayList<>();
		Session session = sessionFactory.openSession();
		ConnectionSources connSources = (ConnectionSources) session.get(ConnectionSources.class, sourceId);
		Hibernate.initialize(connSources.getConnectionSubSources());
		session.close();
		ConnectionSources connectionSources = new ConnectionSources();
		connectionSources.setSourceId(connSources.getSourceId());
		connectionSources.setConnectionType(connSources.getConnectionType());
		connectionSources.setSourceImagePath(connSources.getSourceImagePath());
		for (ConnectionSubSources cs : connSources.getConnectionSubSources()) {
			ConnectionSubSources cstemp = new ConnectionSubSources();
			cstemp.setSubSourceId(cs.getSubSourceId());
			cstemp.setSubConnectionType(cs.getSubConnectionType());
			cstemp.setSourceImagePath(cs.getSourceImagePath());
			connectionSources.getConnectionSubSources().add(cstemp);

		}
		return connectionSources;
	}

	public ConnectionSources listSubSourceConnections(Long sourceId, Long subSourceId) throws SystemException {

		Session session = sessionFactory.openSession();
		ConnectionSources connectionSources = (ConnectionSources) session.get(ConnectionSources.class, sourceId);
		ConnectionSubSources connectionSubSources = (ConnectionSubSources) session.get(ConnectionSubSources.class,
				subSourceId);
		Hibernate.initialize(connectionSubSources.getConnectionConfig());
		session.close();
		ConnectionSources connSources = new ConnectionSources();
		connSources.setConnectionType(connectionSources.getConnectionType());
		connSources.setSourceId(connectionSources.getSourceId());
		if (connectionSubSources.getConnectionConfig() != null
				&& !connectionSubSources.getConnectionConfig().isEmpty()) {
			List<ConnectionConfig> connectionConfigList = connectionSubSources.getConnectionConfig();
			for (ConnectionConfig connectionConfig : connectionConfigList) {
				String userName = ApplicationUserDao.getUserName(connectionConfig.getCreatedBy());
				connectionConfig.setCreatedBy(userName);
			}
		}
		connSources.getConnectionSubSources().add(connectionSubSources);
		return connSources;
	}

	public Map<String, Object> saveNewSubSourceConnection(Long subSourceId, ConnectionConfig connectionConfig)
			throws ConstraintViolationException {

		Map<String, Object> connectionInfo = new HashMap<>();
		Session session = sessionFactory.getCurrentSession();

		Long generatedId = (Long) session.save(connectionConfig);
		ConnectionSubSources connectionSubSources = (ConnectionSubSources) session.get(ConnectionSubSources.class,
				subSourceId);
		Hibernate.isInitialized(connectionSubSources.getConnectionConfig());
		connectionSubSources.getConnectionConfig().add(connectionConfig);
		session.update(connectionSubSources);
		connectionInfo.put("connectionConfig", (ConnectionConfig) session.get(ConnectionConfig.class, generatedId));
		connectionInfo.put(TaskStatusEnums.SUCCESS.name(), "Connection Saved Successfully");
		return connectionInfo;

	}

	public Map<String, Object> deleteExistingSubSourceConnection(Long subSourceId, Long ConnectionId) {

		Map<String, Object> connectionInfo = new HashMap<String, Object>();
		connectionInfo.put(TaskStatusEnums.FAIL.name(), "Unable To Delete Connection");
		Session session = sessionFactory.getCurrentSession();
		Query query = session
				.createSQLQuery("delete from subsource_connection_mapping where connectionId = " + ConnectionId);
		query.executeUpdate();
		ConnectionConfig connectionConfig = (ConnectionConfig) session.get(ConnectionConfig.class, ConnectionId);
		if (connectionConfig != null) {
			session.delete(connectionConfig);
			connectionInfo.put("connectionConfig", connectionConfig);
			connectionInfo.put(TaskStatusEnums.SUCCESS.name(), "Connection Deleted Successfully");

		}
		return connectionInfo;
	}

	public ConnectionConfig getConnectionConfig(Long connectionId) {

		Session session = sessionFactory.openSession();
		ConnectionConfig connection = (ConnectionConfig) session.get(ConnectionConfig.class, connectionId);
		session.close();
		return connection;
	}

	public ConnectionSources getConnectionSourceById(Long sourceId) {

		Session session = sessionFactory.openSession();
		ConnectionSources connectionSources = (ConnectionSources) session.get(ConnectionSources.class, sourceId);
		session.close();
		return connectionSources;
	}

	public ConnectionSubSources getConnectionSubSources(Long subSourceId) {

		Session session = sessionFactory.openSession();
		ConnectionSubSources connectionSubSources = (ConnectionSubSources) session.get(ConnectionSubSources.class,
				subSourceId);
		session.close();
		return connectionSubSources;
	}

	public boolean isDataSourceExist(Long subSourceId, String connectionName) throws SystemException {
		logger.debug("Mosaic-Catalogue-Repository: ClassName : ConnectorListingDAO MethodName : isDataSourceExist : ");
		String sql = "select count(*) from connection_config " + "where " + QueryConstants.Connection.CONNECTION_NAME
				+ "=" + "'" + connectionName + "'" + " and " + QueryConstants.Connection.CONNECTION_ID
				+ " in (  select " + QueryConstants.Connection.CONNECTION_ID + " from subsource_connection_mapping "
				+ "where " + QueryConstants.Connection.SUBSOURCE_ID + "= " + subSourceId + ")";

		Long count = MySqlOperations.complexQueryCount(sql);

		if (count > 0) {
			return true;
		} else
			return false;

	}

}

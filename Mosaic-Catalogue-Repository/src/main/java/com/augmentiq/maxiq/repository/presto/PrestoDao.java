package com.augmentiq.maxiq.repository.presto;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ObjectSerializationHandler;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.generic.GenericObjectBuild;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;

@Repository
public class PrestoDao {

	public DataNode getPublishedDataSource() throws SystemException {
		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put("type", "DATA_SOURCE");
		List<GenericObjectBuild> buildObjectList = MySqlOperations.scan(GenericObjectBuild.class, query);
		List<DataSource> dataSourceList = new ArrayList<DataSource>();
		if (buildObjectList != null && !buildObjectList.isEmpty()) {
			for (GenericObjectBuild buildObject : buildObjectList) {
				DataSource dataSource = (DataSource) ObjectSerializationHandler
						.toObject(StringUtils.replace(buildObject.getJson(), "\\", "\\\\"), DataSource.class);
				if (dataSource != null) {
					dataSourceList.add(dataSource);
				}
			}
		}
		DataNode dataSources = new DataNode("Data Sources", false);

		if (dataSourceList != null && !dataSourceList.isEmpty()) {
			for (DataSource dataSourceObj : dataSourceList) {
				String dataSourceName = dataSourceObj.getDataSourceName();
				List<FieldMapping> fieldMappingList = dataSourceObj.getFieldMappings();
				DataNode dataSource = new DataNode(dataSourceName, true);
				DataNode fields = new DataNode("Columns", false);
				if (null != fieldMappingList) {
					if (fieldMappingList != null && fieldMappingList.size() > 0) {
						for (FieldMapping fieldMapping : fieldMappingList) {

							String name = fieldMapping.getFieldName();
							DataNode field = new DataNode(fieldMapping.getFieldName(), false);
							field.set_genericField(true);
							if (fields.getChildren() != null) {
								fields.getChildren().add(field);
							}
						}
					}
				}
				dataSource.getChildren().add(fields);
				dataSource.setDataSourceType(
						dataSourceObj.getDataSourceType() != null ? dataSourceObj.getDataSourceType().toString() : "");
				dataSources.getChildren().add(dataSource);
			}
		}

		return dataSources;
	}

}

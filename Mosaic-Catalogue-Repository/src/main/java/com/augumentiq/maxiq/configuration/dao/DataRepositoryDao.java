package com.augumentiq.maxiq.configuration.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ObjectSerializationHandler;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.constant.configuration.enums.ConfigurationDataType;
import com.augmentiq.maxiq.constant.configuration.enums.HadoopJobState;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepository;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.TreeDomain;
import com.augmentiq.maxiq.entity.model.configuration.generic.GenericObjectBuild;

/** @author root */
public class DataRepositoryDao {
  private static Logger logger = LoggerFactory.getLogger(DataRepositoryDao.class);

  public static Long insertDataSource(DataSource dataSource) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> insertDataSource()" + ParamUtils.getString(dataSource));
    GenericObjectBuild build = new GenericObjectBuild();
    build.setId(dataSource.getId());
    build.setJson(ObjectSerializationHandler.toString(dataSource));
    build.setName(dataSource.getDataSourceName());
    build.setType(ConfigurationDataType.DATA_SOURCE);
    build.setTypeOfActiveInstances(dataSource.getTypeOfActiveInstances());
    build.setNoOfActiveInstances(dataSource.getNoOfActiveInstances());

    Long dataSourceId = GenericStoreDao.insertNewConfigurationWithAutoIncremented(build);

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << insertDataSource()");
    return dataSourceId;
  }

  /*public static Long insertConnector(ConnectorConfig config) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> insertDataSource()" + ParamUtils.getString(config));
    GenericObjectBuild build = new GenericObjectBuild();
    build.setJson(ObjectSerializationHandler.toString(config));
    build.setName(config.getConncetionName());
    build.setType(ConfigurationDataType.CONNECTION);

    Long id = GenericStoreDao.insertNewConfigurationWithAutoIncremented(build);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << insertDataSource()" + ParamUtils.getString(id));
    return id;
  }*/

  public static List<TreeDomain> getDataSourceList(Long projectId) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >>  getDataSourceList()"
            + ParamUtils.getString(projectId));

    List<TreeDomain> dataSourceInstanceList = new ArrayList<>();

    String query =
        " select dataSourceId as id, dataSourceName as label from "
            + Sequences.DATASOURCE_INSTANCE
            + " where ownerProjectId= ? "
            + " or ownerProjectId in (select objectId as dataSourceId from "
            + Sequences.PROJECT_OBJECT_MAPPING
            + " where projectId=? and objectType='DATA_SOURCE')";

    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put("ownerProjectId", projectId);
    params.put("projectId", projectId);
    try {
      dataSourceInstanceList = MySqlOperations.scanWithSqlQuery(TreeDomain.class, query, params);
    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << getDataSourceList()"
              + ParamUtils.getString(projectId));
      e.printStackTrace();
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_008", "Data Source", "dataSourceId");
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getDataSourceList()" + ParamUtils.getString(projectId));
    return dataSourceInstanceList;
  }

  public static DataSource getDataSource(Long dataSourceId) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataSource()" +
    // ParamUtils.getString(dataSourceId));

    DataSource dataSource = null;
    try {

      String json = GenericStoreDao.getConfiguration(dataSourceId);

      if (StringUtils.isNotBlank(json)) {
        dataSource =
            (DataSource)
                ObjectSerializationHandler.toObject(
                    StringUtils.replace(json, "\\", "\\\\"), DataSource.class);
        if (dataSource == null) {
          dataSource = (DataSource) ObjectSerializationHandler.toObject(json, DataSource.class);
        }
      }
    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << getDataSource()"
              + ParamUtils.getString(dataSourceId));
      logger.info("Exception : " + e.getMessage());
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_008", "Data Source", "dataSourceId");
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataSource()" +
    // ParamUtils.getString(dataSource));
    return dataSource;
  }

  public static DataSource getDataSourceByName(String dataSourceName) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataSourceByName()" +
    // ParamUtils.getString(dataSourceName));
    DataSource dataSource = null;
    try {

      String json = GenericStoreDao.getConfigurationByName(dataSourceName);

      json = StringUtils.replace(json, "\\", "\\\\");

      if (StringUtils.isNotBlank(json))
        dataSource = (DataSource) ObjectSerializationHandler.toObject(json, DataSource.class);

    } catch (Exception e) {
      e.printStackTrace();
      // logger.warn(LoggerConstants.LOG_MAXIQWEB+e);
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataSourceByName()" +
      // ParamUtils.getString(dataSourceName));
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_008", "Data Source", dataSourceName);
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataSourceByName()" +
    // ParamUtils.getString(dataSource));
    return dataSource;
  }

  public static Long insertDataRepository(DataRepository dataRepository) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> insertDataRepository()" +
    // ParamUtils.getString(dataRepository));

    GenericObjectBuild build = new GenericObjectBuild();

    build.setId(dataRepository.getId());
    build.setJson(ObjectSerializationHandler.toString(dataRepository));
    build.setName(dataRepository.getRepoName());
    build.setType(ConfigurationDataType.DATA_REPOSITORY);
    build.setNoOfActiveInstances(0);
    build.setTypeOfActiveInstances(GENERAL_CONSTANTS.DEFAULT);
    Long id = GenericStoreDao.insertNewConfigurationWithAutoIncremented(build);
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << insertDataRepository()" + ParamUtils.getString(id));
    return id;
  }

  public static DataRepository getDataRepository(Long dataRepoId) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataRepository()" +
    // ParamUtils.getString(dataRepoId));
    DataRepository dataRepository = null;
    try {

      String json = GenericStoreDao.getConfiguration(dataRepoId);
      dataRepository =
          (DataRepository) ObjectSerializationHandler.toObject(json, DataRepository.class);

    } catch (Exception e) {

      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataRepository()" +
      // ParamUtils.getString(dataRepoId));
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_008", "Data Repository", dataRepoId);
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataRepository()" +
    // ParamUtils.getString(dataRepository));
    return dataRepository;
  }

  public static Collection<DataRepository> getAllDataRepository(Long userId, Long groupId)
      throws SystemException {

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getAllDataRepository()" +
    // ParamUtils.getString(userId,groupId));

    Collection<DataRepository> dataRepository = null;
    try {

      Map<String, Object> map = new LinkedHashMap<String, Object>();
      map.put("type", ConfigurationDataType.DATA_REPOSITORY);

      List<GenericObjectBuild> genricObjects =
          MySqlOperations.scanForQuery(GenericObjectBuild.class, map);
      if (null != genricObjects) {
        dataRepository = new ArrayList<DataRepository>();
        for (GenericObjectBuild build : genricObjects) {
          dataRepository.add(
              (DataRepository)
                  ObjectSerializationHandler.toObject(build.getJson(), DataRepository.class));
        }
      }

    } catch (Exception e) {

      // logger.warn(LoggerConstants.LOG_MAXIQWEB+e);
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_008", "Data Repository", userId);
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getAllDataRepository()" +
    // ParamUtils.getString(dataRepository));
    return dataRepository;
  }

  public static List<Map<String, Object>> getQueryForAllPipeLineOperationOfDataRepo(
      Long jobInstanceId) throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+ " >>
    // getQueryForAllPipeLineOperationOfDataRepo()" +
    // ParamUtils.getString(jobInstanceId));

    String sqlStmt =
        "select * from job_instances where baapJobInstId = ? and (type ='PIPELINE' or type ='CONVERSION_MR')";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();

    objectMap.put("baapJobInstId", jobInstanceId);

    List<Map<String, Object>> jobInstanceResultSet =
        MySqlOperations.executeQueryForResultSetPrepStatement(
            sqlStmt, new String[] {"offsetInfo", "typeId", "jobInstId", "type"}, objectMap);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+ " << getQueryForAllPipeLineOperationOfDataRepo()" + ParamUtils.getString(jobInstanceResultSet));
    return jobInstanceResultSet;
  }

  public static void updateJobStatusOfType(Long jobInstanceId, String type, Long endTime)
      throws SystemException {
    logger.info(
        LoggerConstants.LOG_MAXIQWEB
            + " >> updateJobStatusOfType()"
            + ParamUtils.getString(jobInstanceId, type, endTime));

    String sql =
        "update job_instances set jobStatus= ?"
            + " , endTime= ?"
            + " where  type =? and jobInstId = ?"
            + " and jobStatus <> ? and jobStatus <> ?";

    Map<String, Object> query = new LinkedHashMap<String, Object>();

    query.put("jobStatus", HadoopJobState.SUCCEEDED.toString());
    query.put("endTime", endTime);
    query.put("type", type);
    query.put("jobInstId", jobInstanceId);
    query.put("jobStatus1", HadoopJobState.FAILED.toString());
    query.put("jobStatus2", HadoopJobState.ABORTED.toString());

    MySqlOperations.executeQuery(sql, query);

    logger.info(
        LoggerConstants.LOG_MAXIQWEB
            + " << updateJobStatusOfType()"
            + ParamUtils.getString(jobInstanceId, type, endTime));
  }

 /* public static List<ConnectorConfig> getConnections() throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getConnections() : []");

    List<ConnectorConfig> dataRepository = null;
    try {

      Map<String, Object> map = new LinkedHashMap<String, Object>();
      map.put("type", ConfigurationDataType.CONNECTION);

      List<GenericObjectBuild> genricObjects =
          MySqlOperations.scanForQuery(GenericObjectBuild.class, map);
      if (null != genricObjects) {
        dataRepository = new ArrayList<ConnectorConfig>();
        for (GenericObjectBuild build : genricObjects) {

          try {

            ConnectorConfig object =
                (ConnectorConfig)
                    ObjectSerializationHandler.toObject(build.getJson(), ConnectorConfig.class);
            if (object != null) dataRepository.add(object);

          } catch (Throwable e) {
            logger.error(e.getMessage());
          }
        }
      }

    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + " : << getConnections() : [e {}]", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_008", "Data Repository");
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getConnections()"
            + ParamUtils.getString(dataRepository));
    return dataRepository;
  }
*/
  public static void upgradeLoadStrategy(DataSource dataSource) throws SystemException {

    String query = "update configuration set json =? where id = ?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("json", ObjectSerializationHandler.toString(dataSource));
    objectMap.put("id", dataSource.getId());
    MySqlOperations.updateQuery(query, objectMap);
  }

  /**
   * @param config
   * @return
   * @throws Exception
   */
  /*public static Boolean isConnectorAlreadyExist(ConnectorConfig config) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> isConnectorAlreadyExist()"
            + ParamUtils.getString(config));

    Map<String, Object> query = new LinkedHashMap<String, Object>();

    query.put(GENERAL_CONSTANTS.NAME, config.getConncetionName());
    query.put(GENERAL_CONSTANTS.TYPE, ConfigurationDataType.CONNECTION);

    GenericObjectBuild builds = MySqlOperations.scanOneForQuery(GenericObjectBuild.class, query);
    if (null != builds) {
      return true;
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << isConnectorAlreadyExist()"
            + ParamUtils.getString(false));
    return false;
  }*/
}
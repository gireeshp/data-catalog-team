package com.augumentiq.maxiq.configuration.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepository;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;

/**
 * This class will fethc the Data Repository object along with its child elements
 *
 * @author shiva
 */

public class DataRepositoryFetchWithChild {

	  /**
	   * Fetch the Data Repository object from the Repo ID
	   *
	   * @param repoId
	   * @return
	   * @throws HbaseDataFetchException
	   */
	  private static final Logger logger = LoggerFactory.getLogger(DataRepositoryFetchWithChild.class);

	  public static DataRepository getDataRepository(Long repoId) throws SystemException {

	    // // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataRepository()" + ParamUtils.getString(repoId));
	    DataRepository dataRepository = DataRepositoryDao.getDataRepository(repoId);

	    if (null != dataRepository) {
	      dataRepository.setDataSources(getDataSources(dataRepository.getDataSourcesStore()));
	    }
	    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataRepository()" + ParamUtils.getString(dataRepository));
	    return dataRepository;
	  }

	  /*
	   * private static final Logger logger =
	   * LoggerFactory.getLogger(DataRepositoryFetchWithChild.class);
	   */
	  public static DataRepository getDataRepositoryForSelectedSources(
	      Long repoId, Set<Long> dataSourceIdsSet) throws SystemException {

	    //logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataRepository()" + ParamUtils.getString(repoId));
	    DataRepository dataRepository = DataRepositoryDao.getDataRepository(repoId);

	    List<DataSource> dataSources = new ArrayList<>();
	    for (Long dataSourceId : dataSourceIdsSet) {
	      DataSource ds = getDataSource(dataSourceId);
	      if (ds != null) dataSources.add(ds);
	    }

	    if (null != dataRepository) {
	      dataRepository.setDataSources(dataSources);
	    }
	    return dataRepository;
	  }

	  /**
	   * List of datasources for the input datasource id list
	   *
	   * @param dataSourceIds
	   * @return
	   * @throws SystemException
	   */
	  public static List<DataSource> getDataSources(List<Long> dataSourceIds) throws SystemException {
	    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataSources()" + ParamUtils.getString(dataSourceIds));
	    List<DataSource> dataSources = new ArrayList<>();

	    if (null != dataSourceIds) {
	      for (long dataSourceId : dataSourceIds) {
	        DataSource ds = getDataSource(dataSourceId);

	        if (ds != null) dataSources.add(ds);
	      }
	    }
	    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataSources()" + ParamUtils.getString(dataSources));
	    return dataSources;
	  }

	  /**
	   * Return the DataSource object for the data source Id provided. It also fetches the child
	   * elements of the Data Source
	   *
	   * @param dataSourceId
	   * @return
	   * @throws SystemException
	   */
	  public static DataSource getDataSource(long dataSourceId) throws SystemException {
	    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataSource()" + ParamUtils.getString(dataSourceId));
	    DataSource dataSource = DataRepositoryDao.getDataSource(dataSourceId);

	    if (dataSource != null && null != dataSource.getFieldMappings()) {
	      Collections.sort(dataSource.getFieldMappings());
	    }
	    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataSource()" + ParamUtils.getString(dataSource));
	    return dataSource;
	  }

	  public static DataSource getDataSourceByName(String dataSourceName) throws SystemException {
	    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getDataSourceByName()" + ParamUtils.getString(dataSourceName));

	    DataSource dataSource = DataRepositoryDao.getDataSourceByName(dataSourceName);
	    if (dataSource != null && null != dataSource.getFieldMappings()) {
	      Collections.sort(dataSource.getFieldMappings());
	    }

	    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getDataSourceByName()" + ParamUtils.getString(dataSource));
	    return dataSource;
	  }
}

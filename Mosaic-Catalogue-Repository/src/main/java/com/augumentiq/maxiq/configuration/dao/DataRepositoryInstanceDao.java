package com.augumentiq.maxiq.configuration.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.DATACONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepositoryInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;

public class DataRepositoryInstanceDao {

  private static final Logger logger = LoggerFactory.getLogger(DataRepositoryDao.class);

  public static void insertRepositoryInstance(DataRepositoryInstance dataRepositoryInstance)
      throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> insertRepositoryInstance()"
            + ParamUtils.getString(dataRepositoryInstance));

    MySqlOperations.insert(dataRepositoryInstance);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << insertRepositoryInstance()"
            + ParamUtils.getString(dataRepositoryInstance));
  }

  public static List<DataRepositoryInstance> getDataRepoInstance(Long userId, Long projectId)
      throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getDataRepoInstance()"
            + ParamUtils.getString(userId, projectId));
    String sql =
        "select dri.dataRepoStatus, dri.repoUpdated, dri.repoCreated, "
            + "dri.class, dri.totalRecords, dri.dataSourceIds, "
            + "dri.filePaths, dri.refreshType, dri.size, "
            + "dri.repoName, dri.userId, dri.lastUpdateFile, "
            + "dri.repoId, dri.groupId,CONCAT(A.firstName,' ',A.lastName) as createdBy,"
            + " dri.ownerProjectId from datarepoinstance dri left outer join application_user A "
            + "on (A.unqUserId = dri.createdBy) where dri.ownerProjectId=?"
            + " order by dri.repoName asc";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("dri.ownerProjectId", projectId);

    List<DataRepositoryInstance> dataRepositoryInstances =
        MySqlOperations.scanWithSqlQuery(DataRepositoryInstance.class, sql, objectMap);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getDataRepoInstance()"
            + ParamUtils.getString(dataRepositoryInstances));
    return dataRepositoryInstances;
  }

  public static DataRepositoryInstance getDataRepoInstance(String repoName)
      throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getDataRepoInstance()"
            + ParamUtils.getString(repoName));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(DATACONSTANTS.DATA_REPOSITORY.REPO_NAME, repoName);

    DataRepositoryInstance dataRepositoryInstance =
        MySqlOperations.scanOneForQuery(DataRepositoryInstance.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getDataRepoInstance()"
            + ParamUtils.getString(dataRepositoryInstance));
    return dataRepositoryInstance;
  }

  public static DataRepositoryInstance getDataRepoInstanceByUsingId(Long repoId)
      throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getDataRepoInstanceByUsingId()"
            + ParamUtils.getString(repoId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(DATACONSTANTS.DATA_REPOSITORY.REPO_ID, repoId);

    DataRepositoryInstance dataRepositoryInstance =
        MySqlOperations.scanOneForQuery(DataRepositoryInstance.class, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getDataRepoInstanceByUsingId()"
            + ParamUtils.getString(dataRepositoryInstance));
    return dataRepositoryInstance;
  }

  public static Long getDataRepoIdFromName(String repoName)
      throws ConnectionException, SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getDataRepoIdFromName()"
            + ParamUtils.getString(repoName));

    DataRepositoryInstance dataRepositoryInstance = getDataRepoInstance(repoName);

    Long repoId = dataRepositoryInstance.getRepoId();

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getDataRepoIdFromName()"
            + ParamUtils.getString(repoId));

    return repoId;
  }

  public static void updateDsIds(long repoId, long dsId)
      throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> updateDsIds()" + ParamUtils.getString(repoId, dsId));

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(DATACONSTANTS.DATA_REPOSITORY.REPO_ID, repoId);

    DataRepositoryInstance dataRepositoryInstance =
        MySqlOperations.scanOneForQuery(DataRepositoryInstance.class, query);

    List<Long> dsIds = null;

    if (null != dataRepositoryInstance) {
      if (null != dataRepositoryInstance.getDataSourceIds()) {
        dsIds = dataRepositoryInstance.getDataSourceIds();
      } else {
        dsIds = new ArrayList<>();
      }
      dsIds.add(dsId);
      dataRepositoryInstance.setDataSourceIds(dsIds);

      insertRepositoryInstance(dataRepositoryInstance);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << updateDsIds()" + ParamUtils.getString(repoId, dsId));
  }

  public static void updateInformationAfterRun(
      long repoId, long fileCount, long spaceOccupied, String filePath)
      throws ConnectionException, SystemException, SQLException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put(DATACONSTANTS.DATA_REPOSITORY.REPO_ID, repoId);

    DataRepositoryInstance dataRepositoryInstance =
        MySqlOperations.scanOneForQuery(DataRepositoryInstance.class, query);

    if (null != dataRepositoryInstance) {

      dataRepositoryInstance.setSize(spaceOccupied);

      dataRepositoryInstance.setTotalRecords(fileCount);

      dataRepositoryInstance.setLastUpdateFile(filePath);

      dataRepositoryInstance.setRepoUpdated(new Date());

      insertRepositoryInstance(dataRepositoryInstance);
    }
  }

  public static Long getCountOfDataRepo(Long groupId) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getCountOfDataRepo()" + ParamUtils.getString(groupId));

    try {
      Map<String, Object> query = new LinkedHashMap<String, Object>();
      query.put("groupId", groupId);
      Long count = MySqlOperations.count(DataRepositoryInstance.class, query);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " << getCountOfDataRepo()" + ParamUtils.getString(count));
      return count;
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << getCountOfDataRepo()"
              + ParamUtils.getString(groupId));
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getCountOfDataRepo()" + ParamUtils.getString(groupId));
    return 0L;
  }

  public static Boolean checkDataSourceIsAlreadyExistInDataRepo(
      String repoName, String dataSourceName) throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> checkDataSourceIsAlreadyExistInDataRepo()"
            + ParamUtils.getString(repoName, dataSourceName));

    DataRepositoryInstance dataRepoInstance = getDataRepoInstance(repoName);

    if (dataRepoInstance.getDataSourceIds() != null
        && dataRepoInstance.getDataSourceIds().size() > 0) {

      DataSourceInstance dataSourceInstance =
          DataSourceInstanceDao.getDataSourceInstance(dataSourceName);
      List<Long> dataSourceIds = dataRepoInstance.getDataSourceIds();

      for (Long dataSourceId : dataSourceIds) {
        if (dataSourceId == dataSourceInstance.getDataSourceId()) {
          logger.debug(
              LoggerConstants.LOG_MAXIQWEB
                  + " << checkDataSourceIsAlreadyExistInDataRepo()"
                  + ParamUtils.getString(repoName, dataSourceName));
          return false;
        }
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << checkDataSourceIsAlreadyExistInDataRepo()"
            + ParamUtils.getString(repoName, dataSourceName));
    return true;
  }

  public static void updateValueStatsInDataRepo(Long drId)
      throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >>  updateValueStatsInDataRepo()"
            + ParamUtils.getString(drId));

    Long totalRecord = 0L;
    Long totalSize = 0L;
    DataRepositoryInstance repoInstance =
        DataRepositoryInstanceDao.getDataRepoInstanceByUsingId(drId);
    if (null != repoInstance
        && null != repoInstance.getDataSourceIds()
        && repoInstance.getDataSourceIds().size() > 0) {
      for (Long dsId : repoInstance.getDataSourceIds()) {
        DataSourceInstance dsInstance = DataSourceInstanceDao.getDataSourceInstance(dsId);
        if (dsInstance != null) {
          totalRecord += dsInstance.getTotalRecords();
          totalSize += dsInstance.getSize();
        }
      }
    }
    repoInstance.setTotalRecords(totalRecord);
    repoInstance.setSize(totalSize);

    String sql = "update datarepoinstance set totalRecords=?, size=? where  repoId =?";

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("totalRecords", totalRecord);
    query.put("size", totalSize);
    query.put("repoId", drId);
    MySqlOperations.executeQuery(sql, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " <<  updateValueStatsInDataRepo()"
            + ParamUtils.getString(drId));
  }

  public static void deleteRepo(Long id, Long groupId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> deleteRepo()" + ParamUtils.getString(id, groupId));

    DataRepositoryInstance dataRepositoryInstance = new DataRepositoryInstance();
    dataRepositoryInstance.setRepoId(id);

    DataRepositoryInstance dataRepoInstance =
        DataRepositoryInstanceDao.getDataRepoInstanceByUsingId(id);
    if (dataRepoInstance.getDataSourceIds() != null)
      for (Long ids : dataRepoInstance.getDataSourceIds()) {
        DataSourceInstanceDao.deleteDataSource(ids, groupId);
      }

    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("repoId", id);

    MySqlOperations.deleteRecords(dataRepoInstance, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << deleteRepo()" + ParamUtils.getString(id, groupId));
  }
}
package com.augumentiq.maxiq.configuration.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.DATACONSTANTS;
import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.MysqlConnection;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants.CategoryOrSubCategory;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants.GlobalSearch;
import com.augmentiq.maxiq.constant.configuration.InputParamTypes;
import com.augmentiq.maxiq.constant.configuration.enums.FlowType;
import com.augmentiq.maxiq.constant.configuration.enums.LoadStategyType;
import com.augmentiq.maxiq.core.dao.configuration.setupdao.UserManagementDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.CategorySubCatMaster;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepositoryInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceFeedback;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceFeedbackVoting;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstanceDTO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceLoadStrategy;
import com.augmentiq.maxiq.entity.model.configuration.bean.DatasourceServiceApimanager;
import com.augmentiq.maxiq.entity.model.configuration.bean.InputParameterValues;
import com.augmentiq.maxiq.entity.model.configuration.bean.ProjectObjectMapping;

public class DataSourceInstanceDao {
	private static final Logger logger = LoggerFactory.getLogger(DataSourceInstanceDao.class);

	public static void deleteDataSource(Long dataSourceId, Long projectId) throws Exception {

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> deleteDataSource()"
				+ ParamUtils.getString(dataSourceId, projectId));

		DataSourceInstance dataSourceInstance = DataSourceInstanceDao.getDataSourceInstance(dataSourceId);
		deleteDataSourceFromDataSourceInstance(dataSourceInstance);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << : deleteDataSource()"
				+ ParamUtils.getString(dataSourceId, projectId));
	}

	public static void insertDataSourceInstance(DataSourceInstance dataSourceInstance)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> insertDataSourceInstance()"
				+ ParamUtils.getString(dataSourceInstance));

		MySqlOperations.insert(dataSourceInstance);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << insertDataSourceInstance()"
				+ ParamUtils.getString(dataSourceInstance));
	}

	public static Long insertDataSourceInstanceForAutoIncrement(DataSourceInstance dataSourceInstance)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> insertDataSourceInstanceForAutoIncrement()"
				+ ParamUtils.getString(dataSourceInstance));

		Long dataSourceId = MySqlOperations.insert(dataSourceInstance,
				HbaseUtility.getIdAnnotationForClass(DataSourceInstance.class));

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << insertDataSourceInstanceForAutoIncrement()"
				+ ParamUtils.getString(dataSourceId));
		return dataSourceId;
	}

	public static void deleteDataSourceInstance(DataSourceInstance dataSourceInstance)
			throws ConnectionException, SystemException {

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteDataSourceInstance()"
				+ ParamUtils.getString(dataSourceInstance));

		MySqlOperations.deleteRow(dataSourceInstance);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << deleteDataSourceInstance()"
				+ ParamUtils.getString(dataSourceInstance));
	}

	public static List<DataSourceInstance> getDataSourceInstances(Long userId, Long groupId)
			throws ConnectionException, SystemException {
		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstances()" + ParamUtils.getString(userId, groupId));

		String sql = "select tbl.fieldMappingVersionId, tbl.dataSourceVersionId, tbl.flowType, tbl.ingection, tbl.dataSourceName, tbl.appId, tbl.createdDate,tbl.updatedDate, tbl.fileType, "
				+ " tbl.compressionType, tbl.dataSourceStatus, tbl.category, tbl.subCategory, tbl.totalRecords, tbl.size, tbl.dataSourceId,"
				+ " tbl.filePaths, tbl.latestFilepath, tbl.description, tbl.oldFilePath, tbl.dataRepoId, tbl.dataRepoName, tbl.dataSourceType, tbl.groupId,"
				+ " tbl.fileTypeIndicator, tbl.createdBy,tbl.lastRunBy,fb.avgRating, tbl.ownerProjectId,tbl.fileStoreObject from (select dataSourceId, round(IFNULL(avgRating, 0)) "
				+ " as avgRating from (select dataSourceId, avg(rating) avgRating from feedback where rating <> 0 and rating is not null"
				+ " and rating <> 'null' group by dataSourceId) as fbk) fb "
				+ " right outer join (select dsi.flowType, dsi.ingection, dsi.dataSourceName, dsi.appId, dsi.fieldMappingVersionId, dsi.dataSourceVersionId,"
				+ " dsi.createdDate,dsi.updatedDate, dsi.fileType, dsi.compressionType, dsi.dataSourceStatus, dsi.category, dsi.subCategory,"
				+ " dsi.totalRecords,dsi.size, dsi.dataSourceId, dsi.filePaths, dsi.latestFilepath, dsi.description, dsi.oldFilePath,"
				+ " dsi.dataRepoId, dsi.dataRepoName, dsi.dataSourceType, dsi.groupId, dsi.fileTypeIndicator, CONCAT(A.firstName,' ',A.lastName) "
				+ " as createdBy,CONCAT(B.firstName,' ',B.lastName) as lastRunBy, dsi.ownerProjectId,dsi.fileStoreObject from datasourceinstance dsi left outer join application_user "
				+ " A on (A.unqUserId = dsi.createdBy) left outer join application_user B on (B.unqUserId = dsi.lastRunBy) where "
				+ " dsi.groupId=?  and dsi.appId='null' "
				+ " union all select dsi.fieldMappingVersionId, dsi.dataSourceVersionId, dsi.flowType, dsi.ingection, dsi.dataSourceName, dsi.appId, "
				+ " dsi.createdDate, dsi.updatedDate, dsi.fileType, dsi.compressionType, dsi.dataSourceStatus, dsi.category, dsi.subCategory,"
				+ " dsi.totalRecords,dsi.size, dsi.dataSourceId, dsi.filePaths, dsi.latestFilepath, dsi.description, dsi.oldFilePath,"
				+ " dsi.dataRepoId, dsi.dataRepoName, dsi.dataSourceType,dsi.groupId, dsi.fileTypeIndicator, CONCAT(A.firstName,' ',A.lastName) "
				+ " as createdBy,CONCAT(B.firstName,' ',B.lastName) as lastRunBy, dsi.ownerProjectId,dsi.fileStoreObject  from datasourceinstance dsi left outer join application_user "
				+ " A on (A.unqUserId = dsi.createdBy) left outer join application_user B on (B.unqUserId = dsi.lastRunBy) inner join "
				+ " data_request dr on dr.group_id= ? and dsi.dataSourceId = dr.datasourceid where dr.status='1') "
				+ " as tbl on fb.dataSourceId = tbl.dataSourceId";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("dsi.groupId", groupId);
		objectMap.put("dr.group_id", groupId);
		List<DataSourceInstance> dataSourceInstances = MySqlOperations.scanWithSqlQuery(DataSourceInstance.class, sql,
				objectMap);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstances()"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}

	public static List<ProjectObjectMapping> getDataSourcesOutsideOfProject(Long projectId)
			throws SystemException, ConnectionException {
		String sql = "select * from project_object_mapping where projectId= ?";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("projectId", projectId);
		List<ProjectObjectMapping> projObjMapping = MySqlOperations.scanWithSqlQuery(ProjectObjectMapping.class, sql,
				objectMap);
		return projObjMapping;
	}

	public static List<DataSourceInstanceDTO> getDataSourceInstancesFromOutsideProject(List<Long> dataSouceId)
			throws SystemException, ConnectionException {
		String param = dataSouceId.toString();
		param = param.replaceAll("\\[|\\]", "");
		String sql = "select dsi.dataSourceId,CONCAT(app.firstName,' ',app.lastName) as createdBy,dsi.dataSourceName,dsi.dataRepoName,"
				+ "dsi.dataSourceType, dsi.category,dsi.subCategory, dsi.totalRecords,dsi.avgRating,dsi.groupId,'USE' as accessType,"
				+ " dsi.updatedDate,dsi.fileType, dsi.fileStoreObject from datasourceinstance dsi left outer join application_user app on (app.unqUserId = dsi.createdBy) "
				+ "where dsi.dataSourceId in (?) and dsi.flowType <> ?";

		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("dsi.dataSourceId", param);
		objectMap.put("dsi.flowType", FlowType.STREAM);
		List<DataSourceInstanceDTO> dataSourceInstance = MySqlOperations.scanWithSqlQuery(DataSourceInstanceDTO.class,
				sql, objectMap);
		return dataSourceInstance;
	}

	/*
	 * Sql Query changed in below method,created view datasource_view to save query
	 * compilation time for every call to this method as previous query had multiple
	 * joins
	 *
	 */

	public static List<DataSourceInstanceDTO> getDataSourceInstancesFromProjectIdGroupIdAndUserId(Long userId)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstances()" + ParamUtils.getString(userId));

		String sql = QueryConstants.DatasourceServiceApimanager.FETCH_DATASOURCE_BY_USERID;
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put(QueryConstants.DatasourceServiceApimanager.USER_ID, userId);
		objectMap.put(QueryConstants.DatasourceServiceApimanager.CREATED_BY, userId);

		List<DataSourceInstanceDTO> dataSourceInstances = MySqlOperations.scanWithSqlQuery(DataSourceInstanceDTO.class,
				sql, objectMap);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstances()"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}
	/*
	 * public static List<DataSourceInstanceDTO>
	 * getDataSourceInstancesFromProjectIdGroupIdAndUserId( Long projectId) throws
	 * ConnectionException, SystemException { logger.debug(
	 * LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstances()" +
	 * ParamUtils.getString(projectId));
	 * 
	 * String sql = "select * from datasource_view where createdBy != ? Limit ?,?";
	 * 
	 * Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
	 * objectMap.put("ownerProjectId", -1);
	 * 
	 * List<DataSourceInstanceDTO> dataSourceInstances =
	 * MySqlOperations.scanWithSqlQuery(DataSourceInstanceDTO.class, sql,
	 * objectMap);
	 * 
	 * logger.debug( LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstances()" +
	 * ParamUtils.getString(dataSourceInstances)); return dataSourceInstances; }
	 */

	public static DataSourceInstance getDataSourceInstanceById(Long dataSourceId)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstanceById(Long dataSourceId)"
				+ ParamUtils.getString(dataSourceId));

		String sql = "SELECT tbl.ingection, tbl.dataSourceName, tbl.appId, tbl.createdDate, tbl.updatedDate, tbl.fileType, "
				+ "tbl.compressionType, tbl.dataSourceStatus, tbl.category, tbl.subCategory, tbl.totalRecords, tbl.size, tbl.dataSourceId, "
				+ "tbl.filePaths, tbl.latestFilepath, tbl.description, tbl.oldFilePath, tbl.dataRepoId, tbl.dataRepoName, tbl.dataSourceType,"
				+ " tbl.groupId, tbl.fileTypeIndicator, tbl.createdBy, tbl.lastRunBy, fb.avgRating, tbl.ownerProjectId, tbl.flowType, "
				+ " tbl.fieldMappingVersionId,  tbl.dataSourceVersionId, tbl.fileStoreObject FROM (SELECT dataSourceId,"
				+ " ROUND(IFNULL(avgRating, 0)) AS avgRating FROM (SELECT dataSourceId, AVG(rating) avgRating FROM feedback WHERE rating <> 0 "
				+ "AND rating IS NOT NULL AND rating <> 'null' GROUP BY dataSourceId) AS fbk) fb RIGHT OUTER JOIN (SELECT dsi.ingection,"
				+ " dsi.dataSourceName, dsi.appId, dsi.createdDate, dsi.updatedDate, dsi.fileType, dsi.compressionType, dsi.dataSourceStatus, dsi.category,"
				+ " dsi.subCategory, dsi.totalRecords, dsi.size, dsi.dataSourceId, dsi.filePaths, dsi.latestFilepath, dsi.description, dsi.oldFilePath, "
				+ "dsi.dataRepoId, dsi.dataRepoName, dsi.dataSourceType, dsi.groupId, dsi.fileTypeIndicator, CONCAT(A.firstName, ' ', A.lastName) AS createdBy,"
				+ " CONCAT(B.firstName, ' ', B.lastName) AS lastRunBy, dsi.ownerProjectId, dsi.flowType, dsi.fieldMappingVersionId,  dsi.dataSourceVersionId,dsi.fileStoreObject FROM datasourceinstance dsi LEFT OUTER JOIN application_user A ON (A.unqUserId = dsi.createdBy) "
				+ "LEFT OUTER JOIN application_user B ON (B.unqUserId = dsi.lastRunBy)" + " WHERE dsi.dataSourceId = '"
				+ dataSourceId + "' AND dsi.appId = 'null') AS tbl ON fb.dataSourceId = tbl.dataSourceId";

		DataSourceInstance dataSourceInstances = MySqlOperations.scanOneForSqlQuery(DataSourceInstance.class, sql);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstanceById(Long dataSourceId)"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}

	public static DataSourceInstance getDataSourceInstance(String sourceName)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstance()" + ParamUtils.getString(sourceName));

		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put(DATACONSTANTS.DATA_SOURCE.DATA_SOURCE_NAME, sourceName);

		DataSourceInstance dataSourceInstance = MySqlOperations.scanOneForQuery(DataSourceInstance.class, query);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstance()"
				+ ParamUtils.getString(dataSourceInstance));
		return dataSourceInstance;
	}

	public static DataSourceInstance getDataSourceInstance(Long dsId) throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >>  getDataSourceInstance()" + ParamUtils.getString(dsId));

		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put(DATACONSTANTS.DATA_SOURCE.DATA_SOURCE_ID, dsId);
		DataSourceInstance dataSourceInstance = MySqlOperations.scanOneForQuery(DataSourceInstance.class, query);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " <<  getDataSourceInstance()"
				+ ParamUtils.getString(dataSourceInstance));
		return dataSourceInstance;
	}

	public static DataSourceInstanceDTO getSingleDSInstance(Long dsId) throws SystemException {

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >>  getSingleDSInstance()" + ParamUtils.getString(dsId));

		String sql = "select dto.groupId, CONCAT(app.firstName,' ',app.lastName) as createdBy, dto.dataSourceName, dto.dataRepoName, dto.dataSourceType,"
				+ "dto.updatedDate, dto.category, dto.subCategory, dto.totalRecords, fbk.avgRating, dto.datasourceid, 'LOAD_EDIT' as accessType,dto.fileType from "
				+ "datasourceinstance dto left outer join application_user app on (app.unqUserId = dto.createdBy) left outer join (select "
				+ "dataSourceId, IFNULL(round(IFNULL(avgRating, 0)), 0) as avgRating from (select dataSourceId, avg(rating) avgRating from feedback "
				+ "where rating <> 0 and rating is not null and rating <> 'null' group by dataSourceId) as fbk) fbk on "
				+ "(dto.dataSourceId = fbk.dataSourceId) where dto.datasourceid = '" + dsId + "'";
		DataSourceInstanceDTO DataSourceInstanceDTO = MySqlOperations.scanOneForSqlQuery(DataSourceInstanceDTO.class,
				sql);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >>  getSingleDSInstance()" + ParamUtils.getString(dsId));
		return DataSourceInstanceDTO;
	}

	public static List<DataSourceInstance> getDataSourceInstances(List<Long> dsIds)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >>  getDataSourceInstance()" + ParamUtils.getString(dsIds));
		List<DataSourceInstance> dataSourceInstances = new ArrayList<DataSourceInstance>();

		for (Long dsId : dsIds) {
			DataSourceInstance dataSourceInstance = getDataSourceInstance(dsId);
			if (dataSourceInstance != null)
				dataSourceInstances.add(dataSourceInstance);
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " <<  getDataSourceInstance()"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}

	public static DataSourceInstance getDataSourceInstanceByName(String dataSourceName)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >>  getDataSourceInstanceByName()"
				+ ParamUtils.getString(dataSourceName));
		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put(DATACONSTANTS.DATA_SOURCE.DATA_SOURCE_NAME, dataSourceName);

		DataSourceInstance dataSourceInstance = MySqlOperations.scanOneForQuery(DataSourceInstance.class, query);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " <<  getDataSourceInstanceByName()"
				+ ParamUtils.getString(dataSourceInstance));
		return dataSourceInstance;
	}

	public static String getDataSourceLatesFilePath(Long dsId) throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceLatesFilePath()" + ParamUtils.getString(dsId));

		DataSourceInstance dataSourceInstance = getDataSourceInstance(dsId);

		if (null != dataSourceInstance) {
			logger.debug(
					LoggerConstants.LOG_MAXIQWEB + " << getDataSourceLatesFilePath()" + ParamUtils.getString(dsId));

			return dataSourceInstance.getLatestFilepath();
		}

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceLatesFilePath()" + ParamUtils.getString(dsId));

		return null;
	}

	public static String getDataSourceLatesFilePath(String datasourceName) throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceLatesFilePath()"
				+ ParamUtils.getString(datasourceName));

		DataSourceInstance dataSourceInstance = getDataSourceInstance(datasourceName);

		if (null != dataSourceInstance) {

			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceLatesFilePath()"
					+ ParamUtils.getString(datasourceName));
			return dataSourceInstance.getLatestFilepath();
		}

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceLatesFilePath()"
				+ ParamUtils.getString(datasourceName));
		return null;
	}

	public static String getDataSourceLatesOfDataSouce(String dataSourceName)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceLatesOfDataSouce()"
				+ ParamUtils.getString(dataSourceName));
		DataSourceInstance dataSourceInstance = getDataSourceInstanceByName(dataSourceName);

		if (null != dataSourceInstance) {
			String latestFilepath = dataSourceInstance.getLatestFilepath();
			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceLatesOfDataSouce()"
					+ ParamUtils.getString(latestFilepath));
			return latestFilepath;
		}

		return null;
	}

	public static String getDataSourceLatesOfDataSouce(Long dataSourceId) throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceLatesOfDataSouce()"
				+ ParamUtils.getString(dataSourceId));
		DataSourceInstance dataSourceInstance = getDataSourceInstance(dataSourceId);

		if (null != dataSourceInstance) {
			String latestFilepath = dataSourceInstance.getLatestFilepath();
			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceLatesOfDataSouce()"
					+ ParamUtils.getString(latestFilepath));
			return latestFilepath;
		}

		return null;
	}

	public static List<DataSourceInstance> getDataSourcesInstance(Long userId)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourcesInstance()" + ParamUtils.getString(userId));
		List<DataSourceInstance> dataSourceInstances = MySqlOperations.scan(DataSourceInstance.class, null);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourcesInstance()"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}

	public static void updateInformationAfterRun(long dsId, long fileCount, long spaceOccupied, String filePath,
			String fileFormatType, String compressionCodec, DataSourceLoadStrategy dataSourceLoadStrategy,
			Long baapInstId) throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateInformationAfterRun()"
				+ ParamUtils.getString(dsId, fileCount, spaceOccupied, filePath));

		DataSourceInstance dataSourceInstance = getDataSourceInstance(dsId);

		if (null != dataSourceInstance) {
			dataSourceInstance.setOldFilePath(dataSourceInstance.getLatestFilepath());
			dataSourceInstance.setFileType(fileFormatType);
			// check LoadType and append path
			if (null != dataSourceLoadStrategy && null != dataSourceLoadStrategy.getLoadStategyType()
					&& dataSourceLoadStrategy.getLoadStategyType().name().equals(LoadStategyType.APPENDALL.name())) {
				dataSourceInstance.setLatestFilepath(findTheLatestPathInAppendAllLoad(dataSourceInstance, filePath));
				dataSourceInstance.setSize((dataSourceInstance.getSize() + spaceOccupied));
				dataSourceInstance.setTotalRecords((dataSourceInstance.getTotalRecords() + fileCount));
			} else {
				dataSourceInstance.setLatestFilepath(filePath);
				dataSourceInstance.setSize(spaceOccupied);
				dataSourceInstance.setTotalRecords(fileCount);
			}

			String latestJobInstanceCreateBy = getLatestJobInstanceCreateBy(dataSourceInstance.getDataRepoId());
			dataSourceInstance.setLastRunBy(latestJobInstanceCreateBy);
			dataSourceInstance.setUpdatedDate(new Date());

			/* Changes to include file format and compression type */
			dataSourceInstance.setFileType(fileFormatType);
			dataSourceInstance.setCompressionType(compressionCodec);
			/* End Changes */

			// If dataSource is created from flow and it is of fileType
			if (null != dataSourceInstance.getFileTypeIndicator()
					&& dataSourceInstance.getFileTypeIndicator().equalsIgnoreCase(Sequences.TRUE)) {
				saveFileTypeDataSourceDetailsInInputParamTables(baapInstId, dataSourceInstance);
			}

			insertDataSourceInstance(dataSourceInstance);
		}

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << updateInformationAfterRun()"
				+ ParamUtils.getString(dsId, fileCount, spaceOccupied, filePath));
	}

	public static Long getCountOfDataSource(Long groupId) {

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getCountOfDataSource()" + ParamUtils.getString(groupId));
		try {
			Map<String, Object> query = new LinkedHashMap<String, Object>();
			query.put("groupId", groupId);
			Long count = MySqlOperations.count(DataSourceInstance.class, query);
			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getCountOfDataSource()" + ParamUtils.getString(count));
			return count;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getCountOfDataSource()" + ParamUtils.getString(groupId));
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getCountOfDataSource()" + ParamUtils.getString(groupId));
		return 0L;
	}

	public static List<DataSourceInstance> getDataSourceInstancesForApp(Long appId)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstancesForApp()" + ParamUtils.getString(appId));
		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put("appId", appId);

		List<DataSourceInstance> dataSourceInstances = MySqlOperations.scan(DataSourceInstance.class, query);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstancesForApp()"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}

	public static DataSourceInstance getDataSourceInstanceForApp(Long appId, Long dsId)
			throws ConnectionException, SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstancesForApp()"
				+ ParamUtils.getString(appId, dsId));
		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put("appId", appId);
		query.put("dataSourceId", dsId);

		DataSourceInstance dataSourceInstance = MySqlOperations.scanOneForQuery(DataSourceInstance.class, query);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstancesForApp()"
				+ ParamUtils.getString(dataSourceInstance));
		return dataSourceInstance;
	}

	public static void updateDataRepoIdIntoDataSourceInstance(Long dataSourceId, Map<String, String> updatedValue)
			throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateDataRepoIdIntoDataSourceInstance()"
				+ ParamUtils.getString(dataSourceId, updatedValue));
		String queryGenerate = "";
		if (updatedValue != null && updatedValue.size() > 0) {

			Set<String> keySet = updatedValue.keySet();
			int i = 0;
			for (String key : keySet) {
				queryGenerate += i > 0 ? "," : "";
				queryGenerate += key + "='" + updatedValue.get(key) + "'";
				i++;
			}
			String sql = "update datasourceinstance set " + queryGenerate + " where dataSourceId= ?";
			Map<String, Object> query = new LinkedHashMap<String, Object>();
			query.put("dataSourceId", dataSourceId);
			// MySqlOperations.executeQuery(sql,query);
			MySqlOperations.updateQuery(sql, query);
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << updateDataRepoIdIntoDataSourceInstance()"
				+ ParamUtils.getString(dataSourceId, updatedValue));
	}

	public static String getLatestJobInstanceCreateBy(Long repoId) throws SystemException {

		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " >> getLatestJobInstanceCreateBy()" + ParamUtils.getString(repoId));

		String sql = "select createdBy from job_instances where typeId = ? and type = 'dataRepoRefresh' order by insertDate desc limit 1";

		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("typeId", repoId);

		List<Map<String, Object>> executeQueryForResultSet = MySqlOperations.executeQueryForResultSetPrepStatement(sql,
				new String[] { "createdBy" }, objectMap);
		if (executeQueryForResultSet != null && executeQueryForResultSet.size() > 0) {
			String query = (String) executeQueryForResultSet.get(0).get("createdBy");
			logger.debug(
					LoggerConstants.LOG_MAXIQWEB + " << getLatestJobInstanceCreateBy()" + ParamUtils.getString(query));
			return query;
		}
		return null;
	}

	public static String findTheLatestPathInAppendAllLoad(DataSourceInstance dataSourceInstance, String filePath) {

		if (null != dataSourceInstance.getFileType()) {

			String createFilesList = "";

			if (dataSourceInstance.getLatestFilepath() == null
					|| StringUtils.equalsIgnoreCase(dataSourceInstance.getLatestFilepath(), "null")
					|| StringUtils.trim(dataSourceInstance.getLatestFilepath()).length() == 0) {
				createFilesList = filePath;
			} else if (dataSourceInstance.getLatestFilepath() != null
					&& StringUtils.trim(dataSourceInstance.getLatestFilepath()).contains("_")) {
				createFilesList = filePath;
			} else {
				createFilesList = dataSourceInstance.getLatestFilepath() + GlobalParams.MAXIQ_DEL + filePath;
			}

			String[] combineAllFiles = StringUtils.split(createFilesList, GlobalParams.MAXIQ_DEL);
			Set<String> avoidDuplicates = new HashSet<>();
			for (String jobInstFilePath : combineAllFiles) {
				avoidDuplicates.add(jobInstFilePath);
			}
			return avoidDuplicates.toString().replace("[", "").replace("]", "").replace(",", GlobalParams.MAXIQ_DEL);
		}
		return filePath;
	}

	/**
	 * @param
	 * @param Total
	 *            the datasources
	 * @return List of DataSource intances
	 * @throws ConnectionException
	 * @throws SystemException
	 */
	public static List<DataSourceInstance> getAllDataSourceInstances() throws ConnectionException, SystemException {
		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " >> getAllDataSourceInstances() " + GlobalSearch.QUERY_FOR_DATASORUCE);
		return MySqlOperations.scanWithSqlQuery(DataSourceInstance.class, GlobalSearch.QUERY_FOR_DATASORUCE, null);
	}

	/**
	 * @param
	 * @param Development
	 *            - Created for global search need to fetch all the datasources
	 * @return List of DataSource instance without any condition
	 * @throws ConnectionException
	 * @throws SystemException
	 */
	public static List<DataSourceInstance> getAllModifiedDataSourceInstances()
			throws ConnectionException, SystemException {
		List<DataSourceInstance> dataSourceInstances = MySqlOperations.scanWithSqlQuery(DataSourceInstance.class,
				GlobalSearch.QUERY_FOR_MODIFIED_DATASORUCES, null);
		if (dataSourceInstances != null) {
			ListIterator<DataSourceInstance> lstIterator = dataSourceInstances.listIterator();
			try {
				while (lstIterator.hasNext()) {
					DataSourceInstance ds = lstIterator.next();
					if (isInteger(ds.getCreatedBy())) {
						List<Map<String, Object>> userList = UserManagementDao
								.getUserFirstAndLastNameById(Long.valueOf(ds.getCreatedBy()));
						if (userList.size() > 0) {
							for (Map<String, Object> ma : userList) {
								String fname = null, lname = null;
								for (Entry<String, Object> ent : ma.entrySet()) {
									if ("firstName".equalsIgnoreCase(ent.getKey())) {
										fname = (String) ent.getValue();
									}

									if ("lastName".equalsIgnoreCase(ent.getKey())) {
										lname = (String) ent.getValue();
									}
								}
								ds.setCreatedBy((fname != null && lname != null) ? fname + " " + lname : "");
							}
						} else {
							ds.setCreatedBy("Maxiq Bot");
						}
					}
					lstIterator.set(ds);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstances()"
					+ ParamUtils.getString(dataSourceInstances));
			return dataSourceInstances;
		} else {
			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstances() return empty or null");
			return new ArrayList<DataSourceInstance>();
		}
	}

	private static void saveFileTypeDataSourceDetailsInInputParamTables(Long baapInstId,
			DataSourceInstance dataSourceInstance) throws SystemException {
		InputParameterValues inputParameterValues = new InputParameterValues();
		inputParameterValues.setId(null);
		inputParameterValues.setDsId(dataSourceInstance.getDataSourceId());
		inputParameterValues.setDrId(dataSourceInstance.getDataRepoId());
		inputParameterValues.setBaapJobInsId(baapInstId);
		inputParameterValues.setType(Sequences.FILETYPEDS);
		inputParameterValues.setParamName(dataSourceInstance.getDataSourceName());
		inputParameterValues.setVal(dataSourceInstance.getLatestFilepath());
		inputParameterValues.setInputParamType(InputParamTypes.FILE);
		MySqlOperations.insert(inputParameterValues, HbaseUtility.getIdAnnotationForClass(InputParameterValues.class));
	}

	public static void saveDataSourceFeedback(DataSourceFeedback sourceFeedback) throws SystemException {
		MySqlOperations.insert(sourceFeedback, HbaseUtility.getIdAnnotationForClass(DataSourceFeedback.class));
	}

	public static List<DataSourceFeedback> fetchDataSourceFeedback(Long userId, Long groupId, String dataSourceId)
			throws SystemException {
		String query = "select fb.id, fb.dataSourceId, fb.groupId, fb.rating, fb.question, fb.answer,"
				+ " fb.insertTS, CONCAT(firstName, ' ', lastName) as unqUserId from feedback fb "
				+ " join application_user au on au.unqUserId = fb.unqUserId where fb.dataSourceId = ? and (rating = 0  or rating is null or rating = 'null')";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();

		objectMap.put("fb.dataSourceId", dataSourceId);
		List<DataSourceFeedback> feedbackDataSource = MySqlOperations.scanWithSqlQuery(DataSourceFeedback.class, query,
				objectMap);
		return feedbackDataSource;
	}

	public static List<Map<String, Object>> fetchDataSourceFeedbackRatingCount(String dataSourceId)
			throws SystemException {
		String query = "SELECT rating , AVG(rating) averageRating,count(rating) ratingCount FROM feedback where datasourceId=? and rating <> 0 GROUP BY rating;";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		String[] params = null;
		objectMap.put("dataSourceId", dataSourceId);
		List<Map<String, Object>> dataSourceFeedbackRatingCountList = MySqlOperations
				.executeQueryForResultSetPrepStatement(query, params, objectMap);
		return dataSourceFeedbackRatingCountList;
	}

	public static List<DataSourceFeedback> fetchDataSourceRating(Long userId, Long groupId, String dataSourceId)
			throws SystemException {

		String query = "select * from feedback where dataSourceId = ? and unqUserId = ?"
				+ " and groupId =? and rating <> 0 and rating is not null and rating <> 'null'";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();

		objectMap.put("dataSourceId", dataSourceId);
		objectMap.put("unqUserId", userId);
		objectMap.put("groupId", groupId);
		List<DataSourceFeedback> feedbackDataSource = MySqlOperations.scanWithSqlQuery(DataSourceFeedback.class, query,
				objectMap);

		return feedbackDataSource;
	}

	public static void updateDataSourceRating(Long userId, Long groupId, Long rating, String dataSourceId)
			throws SystemException {

		String query = "update feedback set rating = ? where groupId = ? and unqUserId = ? and dataSourceId = ?"
				+ " and rating <> 0 and rating is not null and rating <> 'null'";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("rating", rating);
		objectMap.put("groupId", groupId);
		objectMap.put("unqUserId", userId);
		objectMap.put("dataSourceId", dataSourceId);
		MySqlOperations.updateQuery(query, objectMap);
	}

	public static void updateCategoryAndSubCategoryDetails(Long datasourceId, String category, String subCategory,
			String descriptions) throws SystemException {
		Map<String, String> values = new LinkedHashMap<String, String>();
		values.put(QueryConstants.GlobalSearch.IS_MODIFIED, QueryConstants.GlobalSearch.IS_MODIFIED_TRUE);
		DataSource dataSource = DataRepositoryDao.getDataSource(datasourceId);
		if (null != dataSource) {
			dataSource.setCategory(category);
			dataSource.setSubCategory(subCategory);
			dataSource.setDescription(descriptions);
			DataRepositoryDao.insertDataSource(dataSource);
		}

		String queryGenerate = "";
		Set<String> keySet = values.keySet();
		int i = 0;
		for (String key : keySet) {
			queryGenerate += i > 0 ? "," : "";
			queryGenerate += key + "='" + values.get(key) + "'";
			i++;
		}

		String sql = "update datasourceinstance set " + queryGenerate
				+ ", category = ? , subCategory = ?, description = ? where dataSourceId= ?";
		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put("category", category);
		query.put("subCategory", subCategory);
		query.put("description", descriptions);
		query.put("dataSourceId", datasourceId);

		MySqlOperations.executeQuery(sql, query);
	}

	// Voting for datasource Feedback question
	public static String updateDataSourceVote(Long userId, Long dataSourceId, Integer vote, Long feedbackId)
			throws SystemException {

		String voteGiven = "";

		DataSourceFeedbackVoting dataSource = getDataSourceVoteObject(userId, dataSourceId, feedbackId);
		if (dataSource != null) {

			if (dataSource.getVote() == vote) {
				voteGiven = Sequences.ALREADYVOTED;
				return voteGiven;
			}

			if (dataSource.getVote() == 0) {
				String sql = "update " + Sequences.FEEDBACK_VOTING + " set vote = ? where  unqUserId = ?"
						+ " and dataSourceId = ? and feedbackId = ?";
				Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
				objectMap.put("vote", vote);
				objectMap.put("unqUserId", userId);
				objectMap.put("dataSourceId", dataSourceId);
				objectMap.put("feedbackId", feedbackId);
				MySqlOperations.updateQuery(sql, objectMap);
				voteGiven = vote + "";
			} else {
				String sql = "update " + Sequences.FEEDBACK_VOTING + " set vote = '0' where  unqUserId = ?"
						+ " and dataSourceId = ? and feedbackId = ?";
				Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
				objectMap.put("unqUserId", userId);
				objectMap.put("dataSourceId", dataSourceId);
				objectMap.put("feedbackId", feedbackId);
				MySqlOperations.updateQuery(sql, objectMap);
				voteGiven = Sequences.NO_VOTE;
			}

		} else {
			insertDataSourceVote(userId, dataSourceId, vote, feedbackId);
			voteGiven = vote + "";
		}
		return voteGiven;
	}

	public static Object getCountDataSourceVote(String dataSourceId, Long feedbackId) throws SystemException {
		String query = "select IFNULL(sum(vote), 0) as voteCount from " + Sequences.FEEDBACK_VOTING
				+ " where dataSourceId = ? and feedbackId= ?";

		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("dataSourceId", dataSourceId);
		objectMap.put("feedbackId", feedbackId);

		return MySqlOperations.executeQueryForResultSetPrepStatement(query, new String[] { "voteCount" }, objectMap)
				.get(0).get("voteCount");
	}

	public static Object getCountDataSourcePostiveORNegativeVote(String dataSourceId, Long feedbackId,
			Boolean isPositive) throws SystemException {
		String query = "";

		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();

		if (isPositive) {
			query = "select IFNULL(sum(vote), 0) as voteCount from " + Sequences.FEEDBACK_VOTING
					+ " where dataSourceId = ? and feedbackId= ? and vote='1'";
			objectMap.put("dataSourceId", dataSourceId);
			objectMap.put("feedbackId", feedbackId);

		} else {
			query = "select IFNULL(sum(vote), 0) as voteCount from " + Sequences.FEEDBACK_VOTING
					+ " where dataSourceId = ? and feedbackId= ? and vote='-1'";
			objectMap.put("dataSourceId", dataSourceId);
			objectMap.put("feedbackId", feedbackId);
		}
		return MySqlOperations.executeQueryForResultSetPrepStatement(query, new String[] { "voteCount" }, objectMap)
				.get(0).get("voteCount");
	}

	public static DataSourceFeedbackVoting getDataSourceVoteObject(Long userId, Long dataSourceId, Long feedbackId)
			throws SystemException {
		String query = "select * from " + Sequences.FEEDBACK_VOTING
				+ " where dataSourceId =? and feedbackId = ? and unqUserId=?";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("dataSourceId", dataSourceId);
		objectMap.put("feedbackId", feedbackId);
		objectMap.put("unqUserId", userId);
		return MySqlOperations.scanOneForSqlQuerywithData(DataSourceFeedbackVoting.class, query, objectMap);
	}

	public static void insertDataSourceVote(Long userId, Long dataSourceId, Integer vote, Long feedbackId)
			throws SystemException {
		DataSourceFeedbackVoting dataSourceFeedbackVoting = new DataSourceFeedbackVoting();
		dataSourceFeedbackVoting.setId(null);
		dataSourceFeedbackVoting.setCreated_date(System.currentTimeMillis());
		dataSourceFeedbackVoting.setDataSourceId(dataSourceId);
		dataSourceFeedbackVoting.setFeedbackId(feedbackId);
		dataSourceFeedbackVoting.setStatus(1);
		dataSourceFeedbackVoting.setType("");
		dataSourceFeedbackVoting.setUnqUserId(userId);
		dataSourceFeedbackVoting.setVote(vote);
		MySqlOperations.insert(dataSourceFeedbackVoting,
				HbaseUtility.getIdAnnotationForClass(DataSourceFeedbackVoting.class));
	}

	public static Collection<DataSourceInstance> getDataSourceListWhichNotOurGroup(Long groupId)
			throws SystemException {

		String sql = "select tbl.fieldMappingVersionId, tbl.dataSourceVersionId, tbl.flowType, tbl.ingection, tbl.dataSourceName, tbl.appId,"
				+ " tbl.createdDate,tbl.updatedDate, tbl.fileType,"
				+ " tbl.compressionType, tbl.dataSourceStatus, tbl.category, tbl.subCategory, tbl.totalRecords, tbl.size, tbl.dataSourceId,"
				+ " tbl.filePaths, tbl.latestFilepath, tbl.description, tbl.oldFilePath, tbl.dataRepoId, tbl.dataRepoName, tbl.dataSourceType, tbl.groupId,"
				+ " tbl.fileTypeIndicator, tbl.createdBy,tbl.lastRunBy, tbl.ownerProjectId, fb.avgRating from (select dataSourceId, round(IFNULL(avgRating, 0))"
				+ " as avgRating from (select dataSourceId, avg(rating) avgRating from feedback where rating <> 0 and rating is not null"
				+ " and rating <> 'null' group by dataSourceId) as fbk) fb" + " right outer join"
				+ " (select dsi.fieldMappingVersionId, dsi.dataSourceVersionId, dsi.flowType, dsi.ingection, dsi.dataSourceName, dsi.appId, dsi.createdDate,"
				+ " dsi.updatedDate, dsi.fileType, dsi.compressionType, dsi.dataSourceStatus, dsi.category, dsi.subCategory,"
				+ " dsi.totalRecords, dsi.size, dsi.dataSourceId, dsi.filePaths, dsi.latestFilepath, dsi.description,"
				+ " dsi.oldFilePath, dsi.dataRepoId, dsi.dataRepoName, dsi.dataSourceType,"
				+ " dsi.groupId, dsi.fileTypeIndicator, CONCAT(A.firstName,' ',A.lastName) as createdBy,CONCAT(B.firstName,' ',B.lastName)"
				+ " as lastRunBy, dsi.ownerProjectId from datasourceinstance dsi left outer join application_user A on (A.unqUserId = dsi.createdBy)"
				+ " left outer join application_user B on (B.unqUserId = dsi.lastRunBy)"
				+ " inner join data_request dr on dr.group_id= ? and"
				+ " dsi.dataSourceId = dr.objectId where dr.status='1' and dsi.flowType='" + FlowType.PERIODIC + "') "
				+ "as tbl on fb.dataSourceId = tbl.dataSourceId";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("dr.group_id", groupId);
		List<DataSourceInstance> dataSourceInstances = MySqlOperations.scanWithSqlQuery(DataSourceInstance.class, sql,
				objectMap);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstances()"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}

	public static List<DataSourceFeedback> getAvgRatingForDataSource(Long userId, Long groupId, String dataSourceId)
			throws SystemException {
		String query = "select id, dataSourceId, unqUserId, groupId, question, answer, insertTS, rating from feedback where "
				+ "dataSourceId = ? and unqUserId = ? and rating <> 0 and rating is not null and rating <> 'null'";
		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("dataSourceId", dataSourceId);
		objectMap.put("unqUserId", userId);
		List<DataSourceFeedback> fetchAvgRatingForDS = MySqlOperations.scanWithSqlQuery(DataSourceFeedback.class, query,
				objectMap);
		return fetchAvgRatingForDS;
	}

	public static List<DataSourceFeedback> getEachUserRatingForDataSource(Long groupId, String dataSourceId)
			throws SystemException {
		// fetch user and dataSourceId against rating data
		String query = "select fb.Id, fb.dataSourceId, fb.groupId, fb.answer, fb.question,fb.rating, "
				+ "fb.insertTS, CONCAT(app.firstName,' ',app.lastName) as unqUserId from feedback fb "
				+ "join application_user app on fb.unqUserId = app.unqUserId where fb.dataSourceId = ? and (fb.rating <> 0  and fb.rating is not null and fb.rating <> 'null')";

		Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
		objectMap.put("fb.dataSourceId", dataSourceId);
		List<DataSourceFeedback> fetchAllRatingDetails = MySqlOperations.scanWithSqlQuery(DataSourceFeedback.class,
				query, objectMap);
		return fetchAllRatingDetails;
	}

	public static Integer getAvgRatingFromDataSourceId(Long dataSourceId) {
		// TODO fetch avg rating for data source
		String query = "select round(IFNULL(avgRating, 0))  as avgRating from "
				+ "(select dataSourceId, avg(rating) avgRating from feedback where rating <> 0 "
				+ " and rating is not null  and rating <> 'null' and dataSourceId='" + dataSourceId
				+ "' group by dataSourceId)  as fbk";
		Integer nextValue = 0;
		try (Connection connection = MysqlConnection.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(query)) {

			while (resultSet.next()) {
				nextValue = resultSet.getInt(1);
			}
		} catch (Exception e) {
		}
		return nextValue;
	}

	public static boolean isInteger(String inputString) {
		try {
			Integer.valueOf(inputString);
			return true;
		} catch (Exception e) {
			try {
				Long.valueOf(inputString);
				return true;
			} catch (Exception e1) {
				return false;
			}
		}
	}

	/*
	 * Sql query which used to fetch pagination data for datasource_view
	 * 
	 * @Mayuri Narawade
	 */
	public static <T> List<T> getComponentsListPerPageFromPageNumberAndRecordPerPageForDataSource(Long projectId,
			Long pageNumber, Long numberOfRecordsPerPage, String searchContent, String tableName, Class<T> entityClass)
			throws ConnectionException, SystemException {

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getComponentsListPerPageFromPageNumberAndRecordPerPage()"
				+ ParamUtils.getString(projectId));

		String primaryKey = GENERAL_CONSTANTS.DATASOURCEID;
		long calNextRecord = (pageNumber - 1) * numberOfRecordsPerPage;
		String minOrMax = "min(dsId)";
		if (calNextRecord <= 0l) {
			calNextRecord = 8l;
			minOrMax = "max(dsId)";
		}

		String simpleSearch = "";
		if (StringUtils.isNotBlank(searchContent)) {
			simpleSearch = simpleSearch + " dataSourceName like '%" + searchContent + "%'";
		}

		String sql = "select * from (select * from " + tableName + "  where (select " + minOrMax
				+ " as minDsId from (select CONVERT(dataSourceId,  unsigned) as dsId " + "from " + tableName
				+ " where ownerProjectId = ? order by CONVERT(" + primaryKey + " ,  unsigned) desc limit "
				+ calNextRecord + ") as tbl) " + (minOrMax.equalsIgnoreCase("max(dsId)") ? ">=" : ">") + " CONVERT("
				+ primaryKey + ", unsigned) and ownerProjectId = ? " + " union all select dv.* from " + tableName
				+ " dv join (select * from " + Sequences.PROJECT_OBJECT_MAPPING + " where "
				+ "projectId= ? ) as tbl on (dv." + primaryKey
				+ " = tbl.objectId)) as final where (dataRepoName <> 'null' or dataRepoName <> null) ";

		Map<String, Object> query = new LinkedHashMap<>();
		query.put("ownerProjectId1", projectId);
		query.put("ownerProjectId2", projectId);
		query.put("projectId", projectId);

		if (StringUtils.isNotBlank(simpleSearch)) {
			sql = sql + " and " + simpleSearch;
		}

		sql = sql + " order by CONVERT(" + primaryKey + ",  unsigned) desc limit " + numberOfRecordsPerPage;

		List<T> dataSourceInstances = MySqlOperations.scanWithSqlQuery(entityClass, sql, query);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getComponentsListPerPageFromPageNumberAndRecordPerPage()"
				+ ParamUtils.getString(dataSourceInstances));
		return dataSourceInstances;
	}

	public static Long getDataSourceCount(Long projectId, String dataSourceName) throws Exception {
		// TODO Auto-generated method stub
		String query = "select count(*) from " + Sequences.DATASOURCE_VIEW + " where ";
		Long count = 0l;
		if (StringUtils.isNotBlank(dataSourceName)) {
			query = query + " dataSourceName like '%" + dataSourceName + "%' and ";
		}
		query = query + " ownerProjectId = '" + projectId + "'";

		String otherProject = "select count(*) from " + Sequences.DATASOURCE_VIEW
				+ " where dataSourceId in ( select objectId from " + Sequences.PROJECT_OBJECT_MAPPING
				+ " where projectId = " + projectId + " and objectType= 'DATA_SOURCE')";

		if (StringUtils.isNotBlank(dataSourceName)) {
			otherProject = otherProject + " and dataSourceName like '%" + dataSourceName + "%'";
		}

		try (Connection connection = MysqlConnection.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(query);) {

			while (resultSet.next()) {
				count = resultSet.getLong(1);
			}

			ResultSet resultSet1 = statement.executeQuery(otherProject);
			while (resultSet1.next()) {
				count = count + resultSet1.getLong(1);
			}

		} catch (Exception e) {
			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
			logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getNextId(String sequenceName)");
			e.printStackTrace();
		}

		return count;
	}

	/**
	 * @param
	 * @param Total
	 *            the datasources by category name
	 * @return List of DataSource instances
	 * @throws ConnectionException
	 * @throws SystemException
	 */
	public static List<DataSourceInstance> getAllDataSourceInstancesByCatOrSubCatName(
			CategorySubCatMaster categorySubCatMaster) throws ConnectionException, SystemException {
		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " >> getAllDataSourceInstancesByCategory() " + categorySubCatMaster);
		return MySqlOperations.scanWithSqlQuery(DataSourceInstance.class,
				(categorySubCatMaster.getParentId() == 0 ? CategoryOrSubCategory.GET_ALL_DATASOURCES_BY_CATEGORY
						: CategoryOrSubCategory.GET_ALL_DATASOURCES_BY_SUB_CATEGORY) + "'"
						+ categorySubCatMaster.getCatOrSubCatName() + "'",
				null);
	}

	/**
	 * @author Mayuri - MAX-1825
	 * @param dataSourceInstance
	 * @throws Exception
	 */
	public static void deleteDataSourceFromDataSourceInstance(DataSourceInstance dataSourceInstance) throws Exception {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> deleteDataSourceFromDataSourceInstance() "
				+ ParamUtils.getString(dataSourceInstance));

		if (null != dataSourceInstance && dataSourceInstance.getDataRepoId() != null) {
			DataRepositoryInstance dataRepositoryInstance = DataRepositoryInstanceDao
					.getDataRepoInstanceByUsingId(dataSourceInstance.getDataRepoId());

			if (null != dataRepositoryInstance) {
				List<Long> dataSourceIds = dataRepositoryInstance.getDataSourceIds();
				dataSourceIds.remove(dataSourceInstance.getDataSourceId());
				dataRepositoryInstance.setDataSourceIds(dataSourceIds);
				MySqlOperations.insert(dataRepositoryInstance);
			}
		}

		DataSource dataSource = new DataSource();
		dataSource.setId(dataSourceInstance.getDataSourceId());

		dataSourceInstance.setDataSourceId(dataSourceInstance.getDataSourceId());

		Map<String, Object> query = new LinkedHashMap<String, Object>();
		query.put("dataSourceId", dataSourceInstance.getDataSourceId());

		MySqlOperations.deleteRecords(dataSourceInstance, query);

		Map<String, Object> query1 = new LinkedHashMap<String, Object>();
		query1.put("id", dataSourceInstance.getDataSourceId());

		MySqlOperations.deleteRecords(dataSource, query1);

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << deleteDataSourceFromDataSourceInstance() ");
	}

	public static List<DataSourceInstanceDTO> getDataSourceWithoutProjectLayer(Long userId, Long groupId)
			throws SystemException {

		String sql = "select dsi.dataSourceId,CONCAT(app.firstName,' ',app.lastName) as createdBy,dsi.dataSourceName,dsi.dataRepoName,"
				+ "dsi.dataSourceType,dsi.category,dsi.subCategory,dsi.totalRecords,dsi.avgRating,dsi.groupId,'USE' as accessType,dsi.updatedDate,"
				+ "dsi.fileType,dsi.fileStoreObject from datasourceinstance dsi left outer join application_user app on (app.unqUserId = dsi.createdBy)"
				+ "where dsi.dataSourceId in (select dataSourceId from datasourceinstance where createdBy=? union all select distinct objectId"
				+ " as dataSourceId  from user_object_mapping where userId=? union all select distinct objectId as dataSourceId from "
				+ "group_object_mapping where groupId=?) and dsi.flowType <> 'STREAM'";
		Map<String, Object> query = new LinkedHashMap<>();
		query.put("createdBy", userId);
		query.put("userId", userId);
		query.put("groupId", groupId);

		return MySqlOperations.scanWithSqlQuery(DataSourceInstanceDTO.class, sql, query);

	}

}
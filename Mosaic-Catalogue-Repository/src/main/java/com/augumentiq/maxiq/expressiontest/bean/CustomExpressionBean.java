package com.augumentiq.maxiq.expressiontest.bean;

import java.util.LinkedHashMap;
import java.util.List;

public class CustomExpressionBean {

  private String _expression;

  private List<LinkedHashMap<String, String>> _params;

  public String getExpression() {
    return _expression;
  }

  public void setExpression(String expression) {
    _expression = expression;
  }

  public List<LinkedHashMap<String, String>> getParams() {
    return _params;
  }

  public void setParams(List<LinkedHashMap<String, String>> params) {
    _params = params;
  }
}

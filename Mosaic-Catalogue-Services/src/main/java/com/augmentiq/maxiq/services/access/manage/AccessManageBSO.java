package com.augmentiq.maxiq.services.access.manage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.access.manage.dao.AccessManageDAO;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.configuration.enums.AccessTypeEnum;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.core.dao.userdao.ApplicationUserDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRequest;
import com.augmentiq.maxiq.entity.model.configuration.bean.DatasourceApprovalMapper;
import com.augmentiq.maxiq.entity.model.configuration.bean.GroupObjectMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.Notification;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserObjectMapping;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.entity.model.usergroup.UserGroups;
import com.augmentiq.maxiq.model.access.manage.ShareAccessDTO;

/** @author Rushi created on Jul 17, 2017 for MAX-1058 */
public class AccessManageBSO {

  private static final Logger logger = LoggerFactory.getLogger(AccessManageBSO.class);

  /**
   * @param projectId
   * @param groupAccessMap
   * @throws SystemException
   */
  /*public static void shareProjectWithGroups(
      Long projectId, Map<Long, AccessTypeEnum> groupAccessMap) throws SystemException {
    if (null != groupAccessMap && null != projectId && groupAccessMap.size() > 0) {
      Set<Long> keySet = groupAccessMap.keySet();
      if (null != keySet) {
        for (Long groupId : keySet) {
          AccessManageDAO.shareProjectWithGroupWithAccessType(
              projectId, groupId, groupAccessMap.get(groupId));
          AccessManageDAO.grantAccessHistoryManagementForGroupUnderProjects(
              projectId, groupId, groupAccessMap.get(groupId));
        }
      }
    }
  }*/

  /**
   * @param projectId
   * @param userAccessMap
   * @throws SystemException
   */
 /* public static void shareProjectWithUsers(Long projectId, Map<Long, AccessTypeEnum> userAccessMap)
      throws SystemException {
    if (null != userAccessMap && null != projectId) {
      Set<Long> keySet = userAccessMap.keySet();
      if (null != keySet) {
        for (Long userId : keySet) {
          AccessManageDAO.shareProjectWithUserWithAccessType(
              projectId, userId, userAccessMap.get(userId));
          AccessManageDAO.grantAccessHistoryManagementForUserUnderProjects(
              projectId, userId, userAccessMap.get(userId));
        }
      }
    }
  }*/

  /**
   * @param projectId
   * @param groupAccessMap
   * @throws SystemException
   */
 /* public static void revokeAccessOfProjectFromGroups(
      Long projectId, Map<Long, AccessTypeEnum> groupAccessMap) throws SystemException {
    if (null != groupAccessMap && null != projectId) {
      Set<Long> keySet = groupAccessMap.keySet();
      if (null != keySet) {
        for (Long groupId : keySet) {
          AccessManageDAO.revokeAccessOfThisProjectFromGroup(
              projectId, groupId, groupAccessMap.get(groupId));
          AccessManageDAO.revokeAccessHistoryManagementForGroupUnderProjects(
              projectId, groupId, groupAccessMap.get(groupId));
        }
      }
    }
  }*/

  /**
   * @param projectId
   * @param userAccessMap
   * @throws SystemException
   */
 /* public static void revokeAccessOfProjectFromUsers(
      Long projectId, Map<Long, AccessTypeEnum> userAccessMap) throws SystemException {

    if (null != userAccessMap && null != projectId) {
      Set<Long> keySet = userAccessMap.keySet();
      if (null != keySet) {
        for (Long userId : keySet) {
          AccessManageDAO.revokeAccessOfThisProjectFromUser(
              projectId, userId, userAccessMap.get(userId));
          AccessManageDAO.revokeAccessHistoryManagementForUserUnderProjects(
              projectId, userId, userAccessMap.get(userId));
        }
      }
    }
  }*/

  /**
   * @param projectId
   * @return
   * @throws SystemException
   */
 /* public static ShareAccessDetailedDTO getSharedAccessForThisProjects(Long projectId)
      throws SystemException {

    ShareAccessDetailedDTO shareAccessDetailedDTO = new ShareAccessDetailedDTO();
    Map<Long, AccessTypeEnum> userAccessMap = new HashMap<Long, AccessTypeEnum>();
    Map<Long, String> userProjectMap = new HashMap<Long, String>();

    List<UserProjectMapping> sharedUserProjectMapping =
        AccessManageDAO.getListOfSharedUsersForThisProject(projectId);
    if (null != sharedUserProjectMapping) {
      for (UserProjectMapping userProjectMapping : sharedUserProjectMapping) {
        userAccessMap.put(
            userProjectMapping.getUserId(), AccessTypeEnum.get(userProjectMapping.getAccessType()));
        String userName =
            ApplicationUserDao.getUserName(String.valueOf(userProjectMapping.getUserId()));
        userProjectMap.put(userProjectMapping.getUserId(), userName);
      }
    }

    Map<Long, AccessTypeEnum> groupAccessMap = new HashMap<Long, AccessTypeEnum>();
    Map<Long, String> groupProjectMap = new HashMap<Long, String>();
    List<GroupProjectMapping> sharedGroupProjectMapping =
        AccessManageDAO.getListOfSharedGroupsForThisProject(projectId);
    if (null != sharedGroupProjectMapping) {
      for (GroupProjectMapping groupProjectMapping : sharedGroupProjectMapping) {
        groupAccessMap.put(
            groupProjectMapping.getGroupId(),
            AccessTypeEnum.get(groupProjectMapping.getAccessType()));
        UserGroups userGroups = GroupManagementUtil.getGroupNameById(groupProjectMapping.getGroupId());
        groupProjectMap.put(groupProjectMapping.getGroupId(), userGroups.getGroupName());
      }
    }
    shareAccessDetailedDTO.setGroupAccessMap(groupAccessMap);
    shareAccessDetailedDTO.setUserAccessMap(userAccessMap);
    shareAccessDetailedDTO.setGroupProjectMap(groupProjectMap);
    shareAccessDetailedDTO.setUserProjectMap(userProjectMap);
    return shareAccessDetailedDTO;
  }*/

  /**
   * @param objectId
   * @param objectType
   * @param groupAccessMap
   * @throws SystemException
   */
  public static void shareObjectWithGroups(
      Long objectId, String objectType, Map<Long, AccessTypeEnum> groupAccessMap)
      throws SystemException {
    if (null != groupAccessMap && null != objectId && null != objectType) {
      Set<Long> keySet = groupAccessMap.keySet();
      if (null != keySet) {
        for (Long groupId : keySet) {
          AccessManageDAO.shareObjectWithGroupWithAccessType(
              objectId, objectType, groupId, groupAccessMap.get(groupId));
          AccessManageDAO.grantAccessHistoryManagementForGroupUnderObjects(
              objectId, groupId, objectType, groupAccessMap.get(groupId));
        }
      }
    }
  }

  /**
   * @param objectId
   * @param objectType
   * @param userAccessMap
   * @throws SystemException
   */
  public static void shareObjectWithUsers(
      Long objectId, String objectType, Map<Long, AccessTypeEnum> userAccessMap)
      throws SystemException {
    if (null != userAccessMap && null != objectId && null != objectType) {
      Set<Long> keySet = userAccessMap.keySet();
      if (null != keySet) {
        for (Long userId : keySet) {
          AccessManageDAO.shareObjectWithUserWithAccessType(
              objectId, objectType, userId, userAccessMap.get(userId));
          //AccessManageDAO.grantAccessHistoryManagementForUserUnderObjects(objectId, userId, objectType, userAccessMap.get(userId));
        }
      }
    }
  }

 /**
  * 
  * @param objectId
  * @param objectType
  * @param groupAccessMap
  * @throws SystemException
  */
  public static void revokeAccessOfThisObjectFromGroups(
      Long objectId, String objectType, Map<Long, AccessTypeEnum> groupAccessMap)
      throws SystemException {
    if (null != groupAccessMap && null != objectId && null != objectType) {
      Set<Long> keySet = groupAccessMap.keySet();
      if (null != keySet) {
        for (Long groupId : keySet) {
          AccessManageDAO.revokeAccessOfThisObjectFromGroup(
              objectId, objectType, groupId, groupAccessMap.get(groupId));
        }
      }
    }
  }

  public static void revokeAccessOfThisObjectFromUsers(
      Long objectId, String objectType, Map<Long, AccessTypeEnum> userAccessMap)
      throws SystemException {
    if (null != userAccessMap && null != objectId && null != objectType) {
      Set<Long> keySet = userAccessMap.keySet();
      if (null != keySet) {
        for (Long userId : keySet) {
          AccessManageDAO.revokeAccessOfUserFromThisObjectWithAccessType(
              objectId, objectType, userId, userAccessMap.get(userId));
        }
      }
    }
  }

  public static Boolean isObjectRequestedForGroup(Long groupId, Long objectId, String objectType)
      throws SystemException {

    DataRequest dataRequest =
        AccessManageDAO.getObjectRequestedForGroupStatus(groupId, objectId, objectType);

    if (dataRequest == null) {
      return false;
    }
    return true;
  }

  public static Boolean isObjectRequestedForUser(Long userId, Long objectId, String objectType)
      throws SystemException {

    DataRequest dataRequest =
        AccessManageDAO.getObjectRequestedForUserStatus(userId, objectId, objectType);

    if (dataRequest == null) {
      return false;
    }
    return true;
  }

  public static ShareAccessDTO getSharedAccessForThisObject(Long objectId, String objectType)
      throws SystemException {
    ShareAccessDTO shareAccessDTO = new ShareAccessDTO();
    Map<Long, AccessTypeEnum> userAccessMap = new HashMap<Long, AccessTypeEnum>();
    List<UserObjectMapping> sharedUserObjectMappings =
        AccessManageDAO.getListOfSharedUsersForThisObject(objectId, objectType);
    if (null != sharedUserObjectMappings) {
      for (UserObjectMapping userObjectMapping : sharedUserObjectMappings) {
        userAccessMap.put(
            userObjectMapping.getUserId(), AccessTypeEnum.get(userObjectMapping.getAccessType()));
      }
    }

    Map<Long, AccessTypeEnum> groupAccessMap = new HashMap<Long, AccessTypeEnum>();
    List<GroupObjectMapping> sharedGroupObjectMappings =
        AccessManageDAO.getListOfSharedGroupsForThisObject(objectId, objectType);
    if (null != sharedGroupObjectMappings) {
      for (GroupObjectMapping groupObjectMapping : sharedGroupObjectMappings) {
        groupAccessMap.put(
            groupObjectMapping.getGroupId(),
            AccessTypeEnum.get(groupObjectMapping.getAccessType()));
      }
    }
    shareAccessDTO.setGroupAccessMap(groupAccessMap);
    shareAccessDTO.setUserAccessMap(userAccessMap);
    return shareAccessDTO;
  }

  public static List<ApplicationUser> getListOfApplicationUsers(Long userId)
      throws SystemException {
    List<ApplicationUser> userList = ApplicationUserDao.getListOfAllUsers(userId);
    return userList;
  }

  public static List<UserGroups> getListOfUserGroup(Long userId) throws SystemException {
    List<UserGroups> groupList = ApplicationUserDao.getListOfUserGroup(userId);
    return groupList;
  }

  public static List<UserGroups> getListOfUserGroupForObjectSharing(Long userId)
      throws SystemException {
    List<UserGroups> groupList = ApplicationUserDao.getListOfUserGroupForObjectSharing(userId);
    return groupList;
  }

  public static Long getPendingRequestCount(Long userId) throws SystemException {
    Long count = AccessManageDAO.getPendingRequestCount(DataRequest.class, userId);
    return count;
  }

  public static List<UserGroups> filterGroupList(
      List<UserGroups> userGroups, Long objectId, String objectType) throws SystemException {
    List<UserGroups> filterUserList = new ArrayList<>();
    List<GroupObjectMapping> groupObjectMapping =
        AccessManageDAO.getListOfSharedGroupsForThisObject(objectId, objectType);
    for (UserGroups userGroupsElement : userGroups) {
      for (GroupObjectMapping groupObjectMappingElement : groupObjectMapping) {
        if (userGroupsElement != null
            && groupObjectMappingElement != null
            && userGroupsElement.getGroupId() == groupObjectMappingElement.getGroupId()) {

          filterUserList.add(userGroupsElement);
        }
      }
    }
    return filterUserList;
  }

  public static DatasourceApprovalMapper getDataSourceApprover(Long objectId, Long userId)
      throws SystemException {
    DatasourceApprovalMapper datasourceApprovalMapper =
        AccessManageDAO.getDataSourceApprover(objectId, userId);
    return datasourceApprovalMapper;
  }

  public static DatasourceApprovalMapper getParentDataSourceApprover(
      Long objectId, Long parentLevel) throws SystemException {
    DatasourceApprovalMapper datasourceApprovalMapper =
        AccessManageDAO.getParentDataSourceApprover(objectId, parentLevel);
    return datasourceApprovalMapper;
  }

  /**
   * @param userId
   * @return
   * @throws Exception
   */
  public static Map<String, Long> getNotificationsOfUser(Long userId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getNotificationsOfUser" + ParamUtils.getString(userId));

    Map<String, Long> notificationMatrix = new HashMap<String, Long>();
    Long myApprovalsCount = AccessManageDAO.getPendingRequestCount(DataRequest.class, userId);
    Long myRequestsCount = AccessManageDAO.getNotificationsOfUser(Notification.class, userId);

    /*Notification count to be shown on each tab after clicking Notification Icon */
    notificationMatrix.put(GENERAL_CONSTANTS.TAB_MYAPPROVALS, myApprovalsCount);
    notificationMatrix.put(GENERAL_CONSTANTS.TAB_MYREQUESTS, myRequestsCount);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getNotificationsOfUser" + ParamUtils.getString(userId));

    return notificationMatrix;
  }

  /**
   * @param datasourceApprovalMapper
   * @return
   * @throws SystemException
   */
  public static String reFreshApproverDetails(
      List<DatasourceApprovalMapper> datasourceApprovalMapper) throws SystemException {

    List<DatasourceApprovalMapper> datasourceApprovalMapperList =
        AccessManageBSO.getDataSourceApproverListByObjectId(
            datasourceApprovalMapper.get(0).getObjectId());
    if (datasourceApprovalMapperList != null && !datasourceApprovalMapperList.isEmpty()) {
      for (DatasourceApprovalMapper approverX : datasourceApprovalMapperList) {
        AccessManageDAO.reFreshApproverDetails(approverX);
      }
    }
    for (DatasourceApprovalMapper approverX : datasourceApprovalMapper) {
      if (approverX.getLevel() < (datasourceApprovalMapper.size() - 1)) {
        approverX.setParent_level(approverX.getLevel() + 1);
      } else {
        approverX.setParent_level(-1L);
      }
      AccessManageBSO.saveDataSourceApprover(approverX);
    }

    return Constants.SUCCESS;
  }

  /**
   * @param datasourceApprovalMapper
   * @throws SystemException
   */
  public static void saveDataSourceApprover(DatasourceApprovalMapper datasourceApprovalMapper)
      throws SystemException {

    AccessManageDAO.saveDataSourceApprover(datasourceApprovalMapper);
  }

  /**
   * @param objectId
   * @return
   * @throws SystemException
   */
  public static List<DatasourceApprovalMapper> getDataSourceApproverListByObjectId(Long objectId)
      throws SystemException {
    List<DatasourceApprovalMapper> datasourceApprovalMapperList =
        AccessManageDAO.getDataSourceApproverListByObjectId(objectId);
    return datasourceApprovalMapperList;
  }
}

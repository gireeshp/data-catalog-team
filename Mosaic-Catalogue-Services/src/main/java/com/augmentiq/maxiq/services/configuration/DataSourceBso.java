package com.augmentiq.maxiq.services.configuration;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.configuration.enums.Actions;
import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceFeedback;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceFeedbackDTO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceFeedbackVoting;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.services.userservice.ApplicationUserServices;
import com.augmentiq.maxiq.util.component.configuration.util.ConfigurationCreateUtil;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import org.apache.commons.lang.StringUtils;

/**
 * This class is used to update data to data source object
 *
 * @author shiva Changed for MAX-502 By Balkrushna Patil on 21-Sept-2016 Changed
 *         for MAX-626 By Ajinkya Marathe on 02-Jan-2017
 */
@Service
public class DataSourceBso {

	/**
	 * This program takes the connection details and updates the same into the data
	 * source
	 *
	 * @param sourceType
	 * @param localFilePath
	 * @param containsHeader
	 * @param dilimter
	 * @param dataSourceId
	 * @param serverFilePath
	 *            TODO
	 * @param initateKafka
	 *            TODO
	 * @throws SystemException
	 */
	private static final Logger logger = LoggerFactory.getLogger(DataSourceBso.class);

	@Autowired
	private ConfigurationCreateUtil configUtil;

	public DataSource getDataSource(String dataSourceName, String userId) throws ConnectionException, SystemException {
		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " >> getDataSource()" + ParamUtils.getString(dataSourceName, userId));
		DataSource dataSource = null;

		DataSourceInstance dataSourceInstance = DataSourceInstanceDao.getDataSourceInstance(dataSourceName);

		if (null != dataSourceInstance) {
			dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceInstance.getDataSourceId());
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSource()" + ParamUtils.getString(dataSource));
		return dataSource;
	}

	public void saveDataSourceFeedback(Long userId, Long groupId, DataSourceFeedback sourceFeedback,
			String dataSourceId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> saveDataSourceFeedback()"
				+ ParamUtils.getString(userId, groupId, sourceFeedback, dataSourceId));
		sourceFeedback.setId(null);
		sourceFeedback.setDataSourceId(dataSourceId);
		sourceFeedback.setGroupId(groupId);
		sourceFeedback.setUnqUserId(String.valueOf(userId));
		sourceFeedback.setQuestion(sourceFeedback.getQuestion());
		sourceFeedback.setAnswer(sourceFeedback.getAnswer());
		sourceFeedback.setRating(00l);
		sourceFeedback.setInsertTS(System.currentTimeMillis() + "");
		DataSourceInstanceDao.saveDataSourceFeedback(sourceFeedback);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << saveDataSourceFeedback()" + ParamUtils.getString());
	}

	public void saveActivityData(Long projectId, String userId, ObjectTypes objectType, String objectId,
			String objectName, Actions action) throws SystemException {
		new ConfigurationCreateUtil().saveActivityData(projectId, userId, objectType, objectId, objectName, action);
	}

	public List<DataSourceFeedbackDTO> fetchDataSourceFeedback(Long userId, Long groupId, String dataSourceId)
			throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceFeedback()"
				+ ParamUtils.getString(userId, groupId, dataSourceId));
		List<DataSourceFeedbackDTO> liSourceFeedbackDTOs;
		List<DataSourceFeedback> fetchDataSourceFeedback = DataSourceInstanceDao.fetchDataSourceFeedback(userId,
				groupId, dataSourceId);
		if (fetchDataSourceFeedback != null && fetchDataSourceFeedback.size() > 0) {
			liSourceFeedbackDTOs = new ArrayList<>(fetchDataSourceFeedback.size());
			for (DataSourceFeedback d : fetchDataSourceFeedback) {
				DataSourceFeedbackDTO dto = new DataSourceFeedbackDTO();
				dto.setId(d.getId());
				dto.setDataSourceId(d.getDataSourceId());
				dto.setGroupId(d.getGroupId());
				dto.setRating(d.getRating());
				dto.setUnqUserId(d.getUnqUserId());
				dto.setQuestion(d.getQuestion());
				dto.setAnswer(d.getAnswer());
				dto.setInsertTS(d.getInsertTS());
				dto.setVotingCount(getCountDataSourceVote(dataSourceId, d.getId()));
				dto.setVotingPositiveCount(getCountDataSourcePostiveVote(dataSourceId, d.getId()));
				dto.setVotingNegativeCount(getCountDataSourceNegativeVote(dataSourceId, d.getId()));

				DataSourceFeedbackVoting dataSourceFeedbackVoting = getDataSourceVoteObject(userId,
						Long.parseLong(dataSourceId), d.getId());

				dto.setSelfVote(dataSourceFeedbackVoting == null ? 0 : dataSourceFeedbackVoting.getVote());
				liSourceFeedbackDTOs.add(dto);
			}
		} else {
			liSourceFeedbackDTOs = new ArrayList<>();
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << fetchDataSourceFeedback()"
				+ ParamUtils.getString(liSourceFeedbackDTOs));
		return liSourceFeedbackDTOs;
	}

	public List<Map<String, Object>> fetchDataSourceFeedbackRatingCount(String dataSourceId) throws SystemException {
		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " >> fetchDataSourceFeedback()" + ParamUtils.getString(dataSourceId));
		List<Map<String, Object>> dataSourceFeedbackRatingCountList = DataSourceInstanceDao
				.fetchDataSourceFeedbackRatingCount(dataSourceId);
		return dataSourceFeedbackRatingCountList;
	}

	public Object getCountDataSourcePostiveVote(String dataSourceId, Long feedbackId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getCountDataSourcePostiveVote()"
				+ ParamUtils.getString(dataSourceId, feedbackId));
		return DataSourceInstanceDao.getCountDataSourcePostiveORNegativeVote(dataSourceId, feedbackId, true);
	}

	public Object getCountDataSourceNegativeVote(String dataSourceId, Long feedbackId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getCountDataSourceNegativeVote()"
				+ ParamUtils.getString(dataSourceId, feedbackId));
		return DataSourceInstanceDao.getCountDataSourcePostiveORNegativeVote(dataSourceId, feedbackId, false);
	}

	public Object getCountDataSourceVote(String dataSourceId, Long feedbackId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getCountDataSourceVote()"
				+ ParamUtils.getString(dataSourceId, feedbackId));
		return DataSourceInstanceDao.getCountDataSourceVote(dataSourceId, feedbackId);
	}

	public static DataSourceFeedbackVoting getDataSourceVoteObject(Long userId, Long dataSourceId, Long feedbackId)
			throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceVoteObject()"
				+ ParamUtils.getString(userId, dataSourceId, feedbackId));
		return DataSourceInstanceDao.getDataSourceVoteObject(userId, dataSourceId, feedbackId);
	}

	public void checkUserAlreadyExist(Long userId, Long groupId, Long rating, String dataSourceId)
			throws SystemException {
		// TODO check user already exist
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> checkUserAlreadyExist()"
				+ ParamUtils.getString(userId, groupId, rating, dataSourceId));
		List<DataSourceFeedback> fetchDataSourceFeedback = DataSourceInstanceDao.fetchDataSourceRating(userId, groupId,
				dataSourceId);
		if (null != fetchDataSourceFeedback && fetchDataSourceFeedback.size() > 0) {
			DataSourceInstanceDao.updateDataSourceRating(userId, groupId, rating, dataSourceId);
		} else {
			DataSourceFeedback sourceFeedback = new DataSourceFeedback();
			sourceFeedback.setId(null);
			sourceFeedback.setDataSourceId(dataSourceId);
			sourceFeedback.setGroupId(groupId);
			sourceFeedback.setUnqUserId(String.valueOf(userId));
			sourceFeedback.setQuestion(sourceFeedback.getQuestion());
			sourceFeedback.setAnswer(sourceFeedback.getAnswer());
			sourceFeedback.setRating(rating);
			sourceFeedback.setInsertTS(System.currentTimeMillis() + "");
			DataSourceInstanceDao.saveDataSourceFeedback(sourceFeedback);
		}
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << checkUserAlreadyExist()" + ParamUtils.getString());
	}

	public List<DataSourceFeedback> getAvgRatingForDataSource(Long userId, Long groupId, String dataSourceId)
			throws SystemException {
		// TODO get avg rating from data source
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getAvgRatingForDataSource()"
				+ ParamUtils.getString(userId, groupId, dataSourceId));
		List<DataSourceFeedback> avgRatingForDataSource = DataSourceInstanceDao.getAvgRatingForDataSource(userId,
				groupId, dataSourceId);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getAvgRatingForDataSource()"
				+ ParamUtils.getString(avgRatingForDataSource));
		return avgRatingForDataSource;
	}

	public List<DataSourceFeedback> getEachUserRatingForDataSource(Long groupId, String dataSourceId)
			throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getEachUserRatingForDataSource()"
				+ ParamUtils.getString(groupId, dataSourceId));
		List<DataSourceFeedback> eachUserRatingForDataSource = DataSourceInstanceDao
				.getEachUserRatingForDataSource(groupId, dataSourceId);
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getEachUserRatingForDataSource()"
				+ ParamUtils.getString(eachUserRatingForDataSource));
		return eachUserRatingForDataSource;
	}

	public String updateDataSourceFeedbackVote(Long userId, Long groupId, Integer vote, Long dataSourceId,
			Long feedbackId) throws SystemException {
		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateDataSourceFeedbackVote()"
				+ ParamUtils.getString(userId, groupId, vote, dataSourceId, feedbackId));
		return DataSourceInstanceDao.updateDataSourceVote(userId, dataSourceId, vote, feedbackId);
	}

	/**
	 * Get the data source instance with the data source name
	 *
	 * @param dataSourceName
	 * @return
	 * @throws ConnectionException
	 * @throws SystemException
	 */
	public DataSourceInstance getDataSourceInstance(String dataSourceName) throws ConnectionException, SystemException {
		logger.debug(
				LoggerConstants.LOG_MAXIQWEB + " >> getDataSourceInstance()" + ParamUtils.getString(dataSourceName));

		DataSourceInstance dataSourcesInstance = DataSourceInstanceDao.getDataSourceInstance(dataSourceName);
		dataSourcesInstance.setCreatedBy(ApplicationUserServices.getUserName(dataSourcesInstance.getCreatedBy()));
		dataSourcesInstance.setLastRunBy(ApplicationUserServices.getUserName(dataSourcesInstance.getLastRunBy()));

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getDataSourceInstance()"
				+ ParamUtils.getString(dataSourcesInstance));
		return dataSourcesInstance;
	}

	public static DataSource getDSWithUpdatedDSInfo(Long datasourceId, String category, String subCategory,
			String descriptions) throws SystemException {
		Map<String, String> values = new LinkedHashMap<String, String>();
		values.put(QueryConstants.GlobalSearch.IS_MODIFIED, QueryConstants.GlobalSearch.IS_MODIFIED_TRUE);
		DataSource dataSource = DataRepositoryDao.getDataSource(datasourceId);
		if (null != dataSource) {
			dataSource.setCategory(category);
			dataSource.setSubCategory(subCategory);
			dataSource.setDescription(descriptions);
			return dataSource;
		}
		return null;
	}

	public static void checkAndMaintainTheVersion(DataSource newDataSource, String userId)
			throws SystemException, ConnectionException {
		DataSource existingDataSource = DataRepositoryFetchWithChild.getDataSource(newDataSource.getId());
		DataSourceInstance dataSourceInstance = DataSourceInstanceDao.getDataSourceInstance(newDataSource.getId());

		// checkAndMakeVersion(newDataSource, userId, existingDataSource,
		// dataSourceInstance);
	}
	// public static void checkAndMakeVersion(
	// DataSource newDataSource,
	// String userId,
	// DataSource existingDataSource,
	// DataSourceInstance dataSourceInstance)
	// throws SystemException {
	// if (!(existingDataSource.equals(newDataSource))) {
	// DataSourceVersion dataSourceVersion = new DataSourceVersion();
	// Long dsVersionId =
	// dataSourceInstance.getDataSourceVersionId() != null
	// ? dataSourceInstance.getDataSourceVersionId()
	// : 1l;
	// Long fieldMappingVersionId =
	// dataSourceInstance.getFieldMappingVersionId() != null
	// ? dataSourceInstance.getFieldMappingVersionId()
	// : 1l;
	//
	// if (dataSourceInstance.getDataSourceVersionId() != null) {
	// dsVersionId = dataSourceInstance.getDataSourceVersionId() + 1l;
	// if (CommunicationDetailsBSO.verifyCurrentDataSourceFieldMappingWithExisting(
	// newDataSource.getFieldMappings(), existingDataSource.getFieldMappings())) {
	// if (dataSourceInstance.getFieldMappingVersionId() != null
	// && dataSourceInstance.getFieldMappingVersionId() >= 1l) {
	// fieldMappingVersionId = dataSourceInstance.getFieldMappingVersionId() + 1l;
	// }
	// }
	// }
	//
	// dataSourceVersion.setDataSourceVersionId(dsVersionId);
	// dataSourceVersion.setDataSource(newDataSource);
	// dataSourceVersion.setFieldMappings(existingDataSource.getFieldMappings());
	// dataSourceVersion.setInsertedBy(userId);
	// dataSourceVersion.setInsertedOn(System.currentTimeMillis() + "");
	//
	// dataSourceVersion.setDataAtRestFileType(
	// StringUtils.isBlank(existingDataSource.getDataAtRestFileType())
	// ? FileTypeEnum.TEXT.name()
	// : existingDataSource.getDataAtRestFileType());
	// dataSourceVersion.setDataAtRestCompressionType(
	// StringUtils.isBlank(existingDataSource.getDataAtRestCompressionType())
	// ? CompressionTypeEnum.SNAPPY.name()
	// : existingDataSource.getDataAtRestCompressionType());
	//
	// dataSourceVersion.setFieldMappingVersionId(fieldMappingVersionId);
	// dataSourceVersion.setDataSourceId(newDataSource.getId());
	//
	// if (null != dataSourceInstance) {
	// dataSourceInstance.setDataSourceVersionId(dsVersionId);
	// dataSourceInstance.setFieldMappingVersionId(fieldMappingVersionId);
	//
	// if (newDataSource.getFileStoreObject() == FileStoreObject.SOURCE) {
	// dataSourceInstance.setFileType(newDataSource.getFileStoreObject().toString());
	// dataSourceInstance.setCompressionType(newDataSource.getFileStoreObject().toString());
	// } else {
	// dataSourceInstance.setFileType(newDataSource.getDataAtRestFileType());
	// dataSourceInstance.setCompressionType(newDataSource.getDataAtRestCompressionType());
	// }
	// MySqlOperations.insert(dataSourceInstance);
	// }
	//
	// MySqlOperations.insert(dataSourceVersion, "id");
	// }
	// }

}

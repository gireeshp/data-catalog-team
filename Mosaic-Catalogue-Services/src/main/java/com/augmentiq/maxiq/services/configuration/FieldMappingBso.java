package com.augmentiq.maxiq.services.configuration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionMessages;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;
import com.augmentiq.maxiq.constant.configuration.enums.TransformationTypes;
import com.augmentiq.maxiq.constant.globalparams.GlobalParams;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.Transformations;
import com.augmentiq.maxiq.entity.model.configuration.generic.GenericObjectBuild;
import com.augmentiq.maxiq.model.configuration.factory.FieldMappingFactory;
import com.augmentiq.maxiq.services.genericservices.HardCodeServices;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;

/** Changed for MAX-502 By Balkrushna Patil on 21-Sept-2016 */

public class FieldMappingBso {

  private static final Logger logger = LoggerFactory.getLogger(FieldMappingBso.class);

  public void extractFieldMappingforTransformation(
      List<Transformations> transformations, DataSource dataSource) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> extractFieldMappingforTransformation()"
            + ParamUtils.getString(transformations, dataSource));

    if (null != transformations && transformations.size() > 0) {
      for (Transformations transformation : transformations) {
        extractFieldMappingforTransformation(transformation, dataSource);
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << extractFieldMappingforTransformation()"
            + ParamUtils.getString(transformations, dataSource));
  }

  public void extractFieldMappingforTransformation(
      Transformations transformation, DataSource dataSource) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> extractFieldMappingforTransformation()"
            + ParamUtils.getString(transformation, dataSource));
    if (null != transformation && null != dataSource && null != dataSource.getFieldMappings()) {
      if (null == transformation.getTranformationId() || transformation.getTranformationId() == 0) {
        transformation.setTranformationId(
            Sequences.getMaxId(
                Sequences.CONFIGURATION,
                HbaseUtility.getIdAnnotationForClass(GenericObjectBuild.class)));

        List<FieldMapping> currentFields = dataSource.getFieldMappings();

        int currentCounter = currentFields.size();
        Long i = findOutMaxFieldId(dataSource);
        if (null != transformation.getType()) {

          FieldMappingFactory factory = new FieldMappingFactory();

          if (transformation.getType().toString().equals(TransformationTypes.OTHER.toString())
              && null != transformation.getApiDomain()) {

            List<String> geoTaggingFields =
                HardCodeServices.getGeoTaggingOutPutList(
                    transformation.getApiDomain().getOutputFieldsPos());

            for (String fieldName : geoTaggingFields) {
              FieldMapping fieldMapping =
                  factory.getFieldMapping(
                      dataSource.getId(),
                      currentCounter++,
                      HardCodeServices.DEFAULT_GEO_TAGGIN_PREFIX + fieldName.toUpperCase(),
                      i);

              currentFields.add(fieldMapping);
              i++;
            }
          } else {
            FieldMapping fieldMapping =
                factory.getFieldMapping(
                    dataSource.getId(),
                    currentCounter++,
                    "TRANFORMATION_FIELD_" + currentCounter,
                    i);

            currentFields.add(fieldMapping);
          }
        }
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << extractFieldMappingforTransformation()"
            + ParamUtils.getString(transformation, dataSource));
  }

  

  public Long findOutMaxFieldId(DataSource dataSource) {
    Long max = 1l;
    if (null != dataSource
        && dataSource.getFieldMappings() != null
        && dataSource.getFieldMappings().size() > 0) {
      List<FieldMapping> fieldMappings = dataSource.getFieldMappings();
      for (FieldMapping fieldMapping : fieldMappings) {
        if (max < fieldMapping.getId()) {
          max = fieldMapping.getId();
        }
      }
    }

    return max + 1l;
  }

  public static Boolean checkFieldForPrimaryKey(List<FieldMapping> primaryKeys, String fieldName) {
    for (FieldMapping fieldMapping : primaryKeys) {
      if (StringUtils.equalsIgnoreCase(fieldName, fieldMapping.getFieldName())) {
        return true;
      }
    }
    return false;
  }

  public static String getFieldMappingMerge(String delimeter, Integer count) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getFieldMappingMerge()"
            + ParamUtils.getString(delimeter, count));
    String inputRegexFormat = "";
    String outputString = "";
    for (int i = 0; i < count - 1; i++) {
      inputRegexFormat += "(.+)" + delimeter;
      Integer j = i + 1;
      outputString += "%" + j + "$s ";
    }
    inputRegexFormat += "(.+)'";
    String inputRegexFormat2 = inputRegexFormat + " , 'output.format.string' = '" + outputString;
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getFieldMappingMerge()"
            + ParamUtils.getString(inputRegexFormat2));
    return inputRegexFormat2;
  }

  public static String getFieldMappingListForPhoenix(DataSource dataSource) {

    String fieldNames = "";
    Boolean firstTime = false;

    for (FieldMapping fieldMapping : dataSource.getFieldMappings()) {
      String primaryKeyValue =
          checkFieldForPrimaryKey(dataSource.getPrimaryKeysForIndex(), fieldMapping.getFieldName())
              ? ""
              : "0.";

      if (!firstTime) {
        fieldNames += primaryKeyValue + fieldMapping.getFieldName();
        firstTime = true;
      } else {
        fieldNames += "," + primaryKeyValue + fieldMapping.getFieldName();
      }
    }
    return fieldNames.toUpperCase();
  }


  public static void insertDataSource(DataSource dataSource) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << insertDataSource()" + ParamUtils.getString(dataSource));
    DataRepositoryDao.insertDataSource(dataSource);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << insertDataSource()" + ParamUtils.getString(dataSource));
  }
  
  public String outPutFieldValidationForField(
	      List<FieldMapping> fieldMapping) {
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB
	            + " >> outPutFieldValidationForField()"
	            + ParamUtils.getString(fieldMapping));

	    HashSet<String> hashSet = GlobalParams.keyWords;
	    Iterator<FieldMapping> iterator =
	        fieldMapping.iterator();
	    FieldMapping fieldMappings =
	        new FieldMapping();
	    String errorMessage = null;
	    while (iterator.hasNext()) {
	      fieldMappings = iterator.next();
	      if (hashSet.contains(fieldMappings.getFieldName().toUpperCase())) {
	        // throw new Exception(iterator.next().getFieldNameAlias() +
	        // "Field Name is not allowed");
	        errorMessage =
	            ExceptionsMessanger.msg(
	                ExceptionMessages.KEYWORDS + " " + fieldMappings.getFieldName());
	        break;
	      }
	    }
	    if (errorMessage != null) {
	      logger.debug(
	          LoggerConstants.LOG_MAXIQWEB
	              + " << outPutFieldValidationForField()"
	              + ParamUtils.getString(errorMessage));
	      return errorMessage;
	    }
	    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << outPutFieldValidationForField()");
	    return null;
	  }
  public void insertFieldMapping(
	      FieldMapping inputFieldMapping, DataSource datasource, List<String> tags, String userId)
	      throws SystemException, ConnectionException {
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB
	            + " >> insertFieldMapping()"
	            + ParamUtils.getString(inputFieldMapping, datasource));

	    if (null != inputFieldMapping) {
	      if (inputFieldMapping.getFieldDataType() == FieldDataTypes.DATE
	          || inputFieldMapping.getFieldDataType() == FieldDataTypes.TIMESTAMP) {
	        if (!DateUtils.checkIfFormatIsParsable(inputFieldMapping.getFormat())) {

	          logger.debug(
	              LoggerConstants.LOG_MAXIQWEB
	                  + " << insertFieldMapping()"
	                  + ParamUtils.getString(inputFieldMapping, datasource));
	          ExceptionsMessanger.throwException(
	              new SystemException(),
	              "ERR_138",
	              inputFieldMapping.getFormat(),
	              inputFieldMapping.getFieldName());
	        }
	      }
	    }

	    if (null != datasource) {
	      List<FieldMapping> fieldMappings = datasource.getFieldMappings();
	      datasource.setTags(tags != null ? tags : new ArrayList<String>());

	      if (null != fieldMappings) {

	        Long inFieldId = inputFieldMapping.getId();

	        for (FieldMapping fm : fieldMappings) {

	          Long fieldId = fm.getId();

	          if (!inFieldId.equals(fieldId)
	              && StringUtils.equalsIgnoreCase(
	                  fm.getFieldName(), inputFieldMapping.getFieldName())) {
	            logger.debug(
	                LoggerConstants.LOG_MAXIQWEB
	                    + " << insertFieldMapping()"
	                    + ParamUtils.getString(inputFieldMapping, datasource));

	            ExceptionsMessanger.throwException(
	                new SystemException(), "ERR_021", inputFieldMapping.getFieldName());
	          }
	        }
	      } else {
	        fieldMappings = new ArrayList<>();
	      }

	      if (inputFieldMapping.getId() != null) {
	        int updatePos = -1;

	        int counter = 0;
	        for (FieldMapping fieldMapping2 : fieldMappings) {
	          if (fieldMapping2.getId().equals(inputFieldMapping.getId())) {
	            updatePos = counter;
	            break;
	          }
	          counter++;
	        }
	        if (updatePos > -1) fieldMappings.set(updatePos, inputFieldMapping);
	      } else {
	        Long i = findOutMaxFieldId(datasource);
	        inputFieldMapping.setId(i);
	        fieldMappings.add(inputFieldMapping);
	      }
	      datasource.setFieldMappings(fieldMappings);
	      DataSourceBso.checkAndMaintainTheVersion(datasource, userId);
	      insertDataSource(datasource);
	    }
	    logger.debug(
	        LoggerConstants.LOG_MAXIQWEB
	            + " << insertFieldMapping()"
	            + ParamUtils.getString(inputFieldMapping, datasource));
	  }


}

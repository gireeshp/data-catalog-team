package com.augmentiq.maxiq.services.configuration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.core.dao.configuration.dao.RelationsDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;


@Service
public class RelationsService {

  // BASE JSON CONSTANTS
  private static final String NODES = "nodes";
  private static final String LINKS = "links";

  // NODE'S CONSTANTS
  private static final String NODE_NAME = "name";
  private static final String NODE_RATING = "rating";
  private static final String NODE_ID = "id";
  private static final String NODE_KEYPAIRS = "keypairs";

  // LINK'S CONSTANT
  private static final String LINK_SOURCE = "source";
  private static final String LINK_TARGET = "target";
  private static final String LINK_VALUE = "value";
  private static final String LINK_LABEL = "label";
  private static final String LINK_KEYPAIRS = "keypairs";

  //MAPPING OBJECT FIELDS
  private static final String ALL_TARGET_FIELDS = "allTargetFields";
  private static final String MAPPING = "mapping";
  private static final String SOURCE_DS_FIELD = "sourceDsField";
  private static final String TARGET_DS_FIELD = "targetDsField";
  private static final String SOURCE_DS_ITEM = "sourceDsItem";
  private static final String TARGET_DS_ITEM = "targetDsItem";
  private static final String DATA_SUORCE = "dataSource";
  private static final String DATA_SUORCE_ID = "dataSourceId";
  public static final String USER_ID = "userId";
  private static final String STRATEGY_FORWARD = "FORWARD";
  private static final String STRATEGY_REVERSE = "REVERSE";

  @SuppressWarnings("unchecked")
  // METHOD CREATES COLLECTION GRAPH JSON
  public static JSONObject getCollectionDagData(String dsIdString)
      throws FileNotFoundException, IOException, ParseException {

    JSONObject graphJson = new JSONObject();
    JSONArray nodes = new JSONArray();
    JSONArray links = new JSONArray();
    Map<String, String> indexMap = new HashMap<String, String>();

    String baseNodesDataFetch = "";
    String fetchRelations = "";
    baseNodesDataFetch =
        "select dataSourceId, dataSourceName from datasourceinstance where dataSourceId in ("
            + dsIdString
            + ")";
    fetchRelations =
        "select dataSourceId, relatedDsId, relatedColumnsPair from datasource_relations where dataSourceId in ("
            + dsIdString
            + ") and relatedDsId in ("
            + dsIdString
            + ")";

    List<Map<String, Object>> baseNodesDataResult =
        RelationsDao.executeQueryForResultSet(baseNodesDataFetch, new String[] {});

    List<Map<String, Object>> fetchRelationResultSet =
        RelationsDao.executeQueryForResultSet(fetchRelations, new String[] {});

    // BUILD NODE ARRAY
    int count = 1;
    for (Map<String, Object> baseNodeData : baseNodesDataResult) {
      JSONObject baseNode = new JSONObject();
      baseNode.put(NODE_NAME, baseNodeData.get("dataSourceName"));
      baseNode.put(NODE_RATING, 5 * count);
      baseNode.put(NODE_ID, baseNodeData.get("dataSourceId"));
      baseNode.put(NODE_KEYPAIRS, "");
      nodes.add(baseNode);
      indexMap.put(baseNode.get(NODE_ID) + "", (count - 1) + "~" + baseNode.get(NODE_NAME));
      count++;
    }

    // BUILD LINK ARRAY
    if (nodes != null && nodes.size() > 0) {

      for (Map<String, Object> relation : fetchRelationResultSet) {
        JSONObject linkJson = new JSONObject();
        String[] sourceArray = StringUtils.split(indexMap.get(relation.get("dataSourceId")), "~");
        linkJson.put(LINK_SOURCE, Integer.parseInt(sourceArray[0]));
        String[] targerArray = StringUtils.split(indexMap.get(relation.get("relatedDsId")), "~");
        linkJson.put(LINK_TARGET, Integer.parseInt(targerArray[0]));
        linkJson.put(LINK_VALUE, 6);
        linkJson.put(LINK_LABEL, sourceArray[1] + "-" + targerArray[1]);
        linkJson.put(LINK_KEYPAIRS, relation.get("relatedColumnsPair"));
        links.add(linkJson);
      }
    }

    graphJson.put(NODES, nodes);
    graphJson.put(LINKS, links);

    return graphJson;
  }

  @Deprecated
  @SuppressWarnings("unused")
  private static Map<String, String> getIndexMap(JSONArray inputJsonArray) {
    int count = 0;
    Map<String, String> indexMap = new HashMap<String, String>();
    for (Object json : inputJsonArray) {
      JSONObject nodesJson = (JSONObject) json;
      indexMap.put(nodesJson.get("id") + "", count + "~" + nodesJson.get("name"));
      count++;
    }
    return indexMap;
  }

  @SuppressWarnings("unchecked")
  // METHOD CREATES SINGLE DS DEPENDANCY GRAPH JSON
  public static JSONObject getSingleRootDagData(Long dsId)
      throws FileNotFoundException, IOException, ParseException {

    String query =
        "select rel.unqDsRelationsId, rel.dataSourceId, rel.relatedDsId, rel.relatedColumnsPair, dsin.dataSourceName as rootDsName, dsin2.dataSourceName as childDsName from datasource_relations rel left outer join datasourceinstance dsin on rel.dataSourceId=dsin.dataSourceId left outer join datasourceinstance dsin2 on rel.relatedDsId=dsin2.dataSourceId where rel.dataSourceId="
            + dsId;

    List<Map<String, Object>> dsDagData =
        RelationsDao.executeQueryForResultSet(query, new String[] {});
    JSONObject builtGraphJson = new JSONObject();
    if (dsDagData != null && dsDagData.size() > 0) {
      builtGraphJson = buildSingleRootGraphJson(dsDagData);
    } else {
      String dsDetailsFetch =
          "select dataSourceId, dataSourceName from datasourceinstance where dataSourceId=" + dsId;
      List<Map<String, Object>> dsDetailsResult =
          RelationsDao.executeQueryForResultSet(dsDetailsFetch, new String[] {});
      JSONArray nodes = new JSONArray();
      if (null != dsDetailsResult && dsDetailsResult.size() > 0) {
        Map<String, Object> map = dsDetailsResult.get(0);
        JSONObject node = new JSONObject();
        node.put(NODE_NAME, map.get("dataSourceName"));
        node.put(NODE_RATING, 5);
        node.put(NODE_ID, map.get("dataSourceId"));
        node.put(NODE_KEYPAIRS, "");
        nodes.add(node);
      }
      builtGraphJson.put(NODES, nodes);
      builtGraphJson.put(LINKS, new JSONArray());
    }
    return builtGraphJson;
  }

  @SuppressWarnings("unchecked")
  private static JSONObject buildSingleRootGraphJson(List<Map<String, Object>> dsDagData) {

    int cout = 0;
    JSONObject graphJson = new JSONObject();
    JSONArray nodes = new JSONArray();
    JSONArray links = new JSONArray();
    JSONObject rootJson = new JSONObject();
    for (Map<String, Object> map : dsDagData) {
      // ADDING ROOT NODE ONCE
      if (cout == 0) {
        rootJson.put(NODE_NAME, map.get("rootDsName"));
        rootJson.put(NODE_RATING, 5);
        rootJson.put(NODE_ID, map.get("dataSourceId"));
        rootJson.put(NODE_KEYPAIRS, map.get("relatedColumnsPair"));
        nodes.add(rootJson);
      }
      // ADDING CHILDRENS
      JSONObject childJson = new JSONObject();
      childJson.put(NODE_NAME, map.get("childDsName"));
      childJson.put(NODE_RATING, 10 * (cout + 1));
      childJson.put(NODE_ID, map.get("relatedDsId"));
      childJson.put(NODE_KEYPAIRS, map.get("relatedColumnsPair"));
      nodes.add(childJson);

      // ADDING LINK DATA
      JSONObject linkJson = new JSONObject();
      linkJson.put(LINK_SOURCE, 0);
      linkJson.put(LINK_TARGET, cout + 1);
      linkJson.put(LINK_VALUE, 6);
      linkJson.put(LINK_LABEL, rootJson.get("name") + "-" + childJson.get("name"));
      linkJson.put(LINK_KEYPAIRS, childJson.get("keypairs"));
      links.add(linkJson);

      cout++;
    }
    graphJson.put(NODES, nodes);
    graphJson.put(LINKS, links);

    return graphJson;
  }

  public static DataSource getDataFields(Long dsId) throws Exception {
    DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(dsId);
    return dataSource;
  }

  @SuppressWarnings("unchecked")
  public static JSONObject fetchAvailableRelations() throws Exception {
    String fetchAvailableRel =
        "select dsinst.dataSourceId as datasourceinstance_ds_id, dsinst.dataSourceName, dsrel.relatedDsId, dsrel.relatedColumnsPair from datasource_relations dsrel inner join datasourceinstance dsinst on dsrel.dataSourceId=dsinst.dataSourceId";
    List<Map<String, Object>> availableRels =
        RelationsDao.executeQueryForResultSet(fetchAvailableRel, new String[] {});

    // BUILD NODE ARRAY
    int count = 1;
    JSONObject graphJson = new JSONObject();
    // JSONArray nodes=new JSONArray();
    Set<JSONObject> nodesSet = new HashSet<JSONObject>();
    List<JSONObject> nodes = new LinkedList<JSONObject>();
    JSONArray links = new JSONArray();
    Map<String, String> indexMap = new HashMap<String, String>();
    for (Map<String, Object> baseNodeData : availableRels) {
      JSONObject baseNode = new JSONObject();
      baseNode.put(NODE_NAME, baseNodeData.get("dataSourceName"));
      baseNode.put(NODE_ID, baseNodeData.get("datasourceinstance_ds_id"));
      baseNode.put(NODE_KEYPAIRS, "");
      boolean add = nodesSet.add(baseNode);
      if (add) {
        // baseNode.put(NODE_RATING, 5*count);
        nodes.add(baseNode);
        indexMap.put(baseNode.get(NODE_ID) + "", (count - 1) + "~" + baseNode.get(NODE_NAME));
        count++;
      }
    }

    // BUILD LINK ARRAY
    if (nodes != null && nodes.size() > 0) {

      for (Map<String, Object> relation : availableRels) {
        JSONObject linkJson = new JSONObject();
        String[] sourceArray =
            StringUtils.split(indexMap.get(relation.get("datasourceinstance_ds_id")), "~");
        linkJson.put(LINK_SOURCE, Integer.parseInt(sourceArray[0]));
        String[] targerArray = StringUtils.split(indexMap.get(relation.get("relatedDsId")), "~");
        linkJson.put(LINK_TARGET, Integer.parseInt(targerArray[0]));
        linkJson.put(LINK_VALUE, 6);
        linkJson.put(LINK_LABEL, sourceArray[1] + "-" + targerArray[1]);
        linkJson.put(LINK_KEYPAIRS, relation.get("relatedColumnsPair"));
        links.add(linkJson);
      }
    }
    graphJson.put(NODES, nodes);
    graphJson.put(LINKS, links);
    return graphJson;
  }

  public static List<Map<String, Object>> fetchDataSource()
      throws FileNotFoundException, IOException, ParseException {
    String fetchQuery = "select dataSourceId, dataSourceName from datasourceinstance";
    List<Map<String, Object>> executeQueryForResultSet =
        RelationsDao.executeQueryForResultSet(fetchQuery, new String[] {});
    return executeQueryForResultSet;
  }

  //builing field mapping object
  @SuppressWarnings("unchecked")
  public static JSONObject buildFieldMappingObject(String dsIds) throws SystemException {

    String[] inputDsIds = StringUtils.splitPreserveAllTokens(dsIds, "-");
    int length = inputDsIds.length;
    JSONObject finalJson = null;
    // if(length==2)
    if (StringUtils.equalsIgnoreCase(length + "", 2 + "")) {

      int count = 0;
      finalJson = new JSONObject();
      finalJson.put(ALL_TARGET_FIELDS, null);
      finalJson.put(MAPPING, null);

      Map<String, String> existingRelation;
      try {
        existingRelation = getExistingRelation(inputDsIds[0], inputDsIds[1]);
      } catch (IOException | ParseException e1) {
        e1.printStackTrace();
        throw new SystemException("Error while fetching existing relations..!");
      }

      for (String id : inputDsIds) {
        JSONArray mappingJsonlist = null;
        if (count == 0 && StringUtils.isNotEmpty(id)) {
          DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(Long.parseLong(id));
          if (null != dataSource) {
            // System.out.println("dataSource : "+dataSource);
            List<FieldMapping> fieldMappings = dataSource.getFieldMappings();
            if (fieldMappings != null
                && !StringUtils.equalsIgnoreCase(fieldMappings + "", "null")) {
              // System.out.println("fieldMappings :
              // "+fieldMappings);
              mappingJsonlist = new JSONArray();
              for (FieldMapping fieldMapping : fieldMappings) {
                JSONObject mappingJson = new JSONObject();
                mappingJson.put(SOURCE_DS_FIELD, fieldMapping.getFieldName());
                String targetValString = existingRelation.get(fieldMapping.getFieldName());
                mappingJson.put(
                    TARGET_DS_FIELD,
                    ((targetValString == null
                            || StringUtils.equalsIgnoreCase(targetValString, "null"))
                        ? ""
                        : targetValString));
                mappingJsonlist.add(mappingJson);
              }
              finalJson.put(MAPPING, mappingJsonlist);
            } else {
              finalJson.put(MAPPING, null);
            }
          }

        } else {
          if (StringUtils.isNotEmpty(id) && (finalJson.get(MAPPING) != null)) {
            DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(Long.parseLong(id));
            if (null != dataSource) {
              List<FieldMapping> fieldMappings = dataSource.getFieldMappings();
              if (fieldMappings == null
                  && StringUtils.equalsIgnoreCase(fieldMappings + "", "null")) {
                finalJson.put(ALL_TARGET_FIELDS, null);
                throw new SystemException("field Mapping is not exist for target data Source");
              } else {
                finalJson.put(ALL_TARGET_FIELDS, getAlltargetFieldArray(fieldMappings));
              }
            }
          } else {
            throw new SystemException("field Mapping is not exist for source data Source");
          }
        }

        count++;
      }
    }
    return finalJson;
  }

  @SuppressWarnings("unchecked")
  private static JSONArray getAlltargetFieldArray(List<FieldMapping> fieldMappings) {
    JSONArray targetFields = new JSONArray();
    for (FieldMapping fieldMapping : fieldMappings) {
      targetFields.add(fieldMapping.getFieldName());
    }
    return targetFields;
  }

  public static Map<String, String> getExistingRelation(String sourcedsId, String targetDsId)
      throws FileNotFoundException, IOException, ParseException {
    Map<String, String> existingDependencyMap = new HashMap<String, String>();
    String query =
        "select relatedColumnsPair from datasource_relations where dataSourceId="
            + sourcedsId
            + " and relatedDsId="
            + targetDsId;
    List<Map<String, Object>> executeQueryForResultSet =
        RelationsDao.executeQueryForResultSet(query, new String[] {});
    if (null != executeQueryForResultSet && executeQueryForResultSet.size() > 0) {
      String columnPairString = executeQueryForResultSet.get(0).get("relatedColumnsPair") + "";
      String[] dashedStrings = StringUtils.split(columnPairString, "~");
      for (String dashedString : dashedStrings) {
        String[] dashedArray = StringUtils.split(dashedString, ":");
        if (StringUtils.equalsIgnoreCase(dashedArray.length + "", 2 + "")) {
          existingDependencyMap.put(dashedArray[0], dashedArray[1]);
        }
      }
    }

    return existingDependencyMap;
  }

  // SAVE OBJECT
  public static void saveRelation(JSONObject inputData) throws SystemException {

    // delete Previous Entries
    JSONObject sourceDsItem = (JSONObject) inputData.get(SOURCE_DS_ITEM);
    JSONObject targetDsItem = (JSONObject) inputData.get(TARGET_DS_ITEM);
    JSONObject dataSource = (JSONObject) inputData.get(DATA_SUORCE);
    String sourceDsId = sourceDsItem.get(DATA_SUORCE_ID) + "";
    String targetDsId = targetDsItem.get(DATA_SUORCE_ID) + "";
    String userId = inputData.get(USER_ID) + "";
    JSONArray mapping = (JSONArray) dataSource.get(MAPPING);
    String buildKeyMappingString = buildKeyMappingString(mapping, STRATEGY_FORWARD);

    if (StringUtils.isNotEmpty(buildKeyMappingString)
        && !StringUtils.equalsIgnoreCase(buildKeyMappingString, "")) {
      try {
        deletePreviousEntries(sourceDsId, targetDsId);
      } catch (IOException | ParseException e) {
        e.printStackTrace();
      }

      // Insert New
      Map<String, String> insertParams = new HashMap<String, String>();
      insertParams.put("dataSourceId", sourceDsId);
      insertParams.put("relatedDsId", targetDsId);
      insertParams.put("insertedBy", userId);
      insertParams.put("relatedColumnsPair", buildKeyMappingString);

      try {
        insertRelation(insertParams);
      } catch (IOException | ParseException e) {
        e.printStackTrace();
      }

      // Insert Reverse
      insertParams.clear();
      insertParams.put("dataSourceId", targetDsId);
      insertParams.put("relatedDsId", sourceDsId);
      insertParams.put("insertedBy", userId);
      insertParams.put("relatedColumnsPair", buildKeyMappingString(mapping, STRATEGY_REVERSE));
      try {
        insertRelation(insertParams);
      } catch (IOException | ParseException e) {
        e.printStackTrace();
      }
    } else {
      throw new SystemException("Please map fields..!");
    }
  }

  public static void deletePreviousEntries(String sourceDsId, String targetDsId)
      throws FileNotFoundException, IOException, ParseException {
    String query =
        "delete from datasource_relations where (dataSourceId=? and relatedDsId=?) or (dataSourceId=? and relatedDsId=?)";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("dataSourceId", sourceDsId);
    objectMap.put("relatedDsId", targetDsId);
    objectMap.put("dataSourceId1", targetDsId);
    objectMap.put("relatedDsId1", sourceDsId);
    RelationsDao.executeQuery(query, objectMap);
  }

  private static void insertRelation(Map<String, String> params)
      throws FileNotFoundException, IOException, ParseException {
    String query =
        "insert into datasource_relations(dataSourceId, relatedDsId, relatedColumnsPair, insertedBy) values(?,?,?,?)";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("dataSourceId", params.get("dataSourceId"));
    objectMap.put("relatedDsId", params.get("relatedDsId"));
    objectMap.put("relatedColumnsPair", params.get("relatedColumnsPair"));
    objectMap.put("insertedBy", params.get("insertedBy"));
    RelationsDao.executeQuery(query, objectMap);
  }

  private static String buildKeyMappingString(JSONArray mapping, String strategy) {
    String keyValString = "";
    for (Object object : mapping) {
      JSONObject maping = (JSONObject) object;
      if (StringUtils.equalsIgnoreCase(strategy, STRATEGY_FORWARD)) {
        if (StringUtils.isNotEmpty(maping.get(TARGET_DS_FIELD) + ""))
          keyValString += maping.get(SOURCE_DS_FIELD) + ":" + maping.get(TARGET_DS_FIELD) + "~";
      } else {
        if (StringUtils.isNotEmpty(maping.get(TARGET_DS_FIELD) + ""))
          keyValString += maping.get(TARGET_DS_FIELD) + ":" + maping.get(SOURCE_DS_FIELD) + "~";
      }
    }
    return StringUtils.removeEnd(keyValString, "~");
  }

  /**
   * this function fetches dataSourceId also as we need Map<Id,Name> in UI
   * 
   * @param dataSourceId
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws ParseException
   * @throws SystemException
   */
  public static List<Map<String, Object>> fetchDataSourceFromId(Long dataSourceId)
      throws FileNotFoundException, IOException, ParseException, SystemException {
    //
    String fetchQuery =
        "select dataSourceId, dataSourceName from datasourceinstance where dataSourceId="
            + dataSourceId;
    List<Map<String, Object>> executeQueryForResultSet =
        RelationsDao.executeQueryForResultSet(fetchQuery, new String[] {});
    return executeQueryForResultSet;
  }
}

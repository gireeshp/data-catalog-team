package com.augmentiq.maxiq.services.datasource.dashboard;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.dao.datasource.dashboard.DataDashBoardDAO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DashBoardDTO;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class DataDasboardBSO {

  private static final Logger logger = LoggerFactory.getLogger(DataDasboardBSO.class);

  public static List<ApplicationUser> getSharedObjectUsers(String dataSourceId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getSharedObjectUsers {}", dataSourceId);
    List<ApplicationUser> applicationUser = DataDashBoardDAO.getSharedObjectUsers(dataSourceId);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getSharedObjectUsers {}", dataSourceId);
    return applicationUser;
  }

  public static List<DashBoardDTO> getDataDashboardCountDetails(String dataSourceId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> getDataDashboardCountDetails {}", dataSourceId);
    List<DashBoardDTO> countDetails = DataDashBoardDAO.getDataDashboardCountDetails(dataSourceId);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": << getDataDashboardCountDetails {}", dataSourceId);
    return countDetails;
  }
}

package com.augmentiq.maxiq.services.ds.apiexposure;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.core.dao.ds.apiexposure.DataSourceExposeDAO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DatasourceServiceApimanager;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.services.userservice.ApplicationUserServices;

public class DataSourceExposeBSO {

  private static final Logger logger = LoggerFactory.getLogger(DataSourceExposeBSO.class);

  public static DatasourceServiceApimanager fetchDataSourceApiDetails(
      Long userId, Long dataSourceId, String dataSourceName) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> fetchDataSourceApiDetails {} {}",
        userId,
        dataSourceId,
        dataSourceName);
    DatasourceServiceApimanager datasourceServiceApimanager =
        DataSourceExposeDAO.fetchDataSourceApiDetails(userId, dataSourceId, dataSourceName);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": << fetchDataSourceApiDetails {} {}",
        userId,
        dataSourceId,
        dataSourceName);
    return datasourceServiceApimanager;
  }

  public static Long persistApiInfo(DatasourceServiceApimanager dataSourceServiceApimanager)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> persistApiInfo {}", dataSourceServiceApimanager);
    return DataSourceExposeDAO.persistApiInfo(dataSourceServiceApimanager);
  }

  public static void updateApiInfo(DatasourceServiceApimanager dataSourceServiceApimanager)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> updateApiInfo {}", dataSourceServiceApimanager);
    DataSourceExposeDAO.updateApiInfo(dataSourceServiceApimanager);
  }

  public static DatasourceServiceApimanager fectApiDetailsByApiKeyAndApiId(Long id, String apiKey)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> fectApiDetailsByApiKeyAndApiId {} {}", id, apiKey);
    DatasourceServiceApimanager datasourceServiceApimanager =
        DataSourceExposeDAO.fectApiDetailsByApiKeyAndApiId(id, apiKey);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": << fectApiDetailsByApiKeyAndApiId {} {}", id, apiKey);
    return datasourceServiceApimanager;
  }

  public static List<Map<String, Object>> getDeployedDS(String userId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getDeployedDS {}", userId);

    List<Map<String, Object>> deployedDS = DataSourceExposeDAO.getDeployedDS(userId);

    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getDeployedDS {}", userId);
    return deployedDS;
  }

  /**
   * Changed for MAX-1900
   * Changed by Yogesh Lokhande
   * Description - Need to fetch data using DataSourceId
   * @param userId
   * @param dataSourceId
   * @return
   * @throws SystemException
   */
  public static List<Map<String, Object>> getDeployedDSApiByDatasourceId(
      String userId, Long dataSourceId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> getDeployedDSApiByDatasourceId {} {}",
        userId,
        dataSourceId);

    List<Map<String, Object>> deployedDS = DataSourceExposeDAO.getDeployedDSApiByDatasourceId(userId, dataSourceId);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": << getDeployedDSApiByDatasourceId {} {}",
        userId,
        dataSourceId);
    return deployedDS;
  }

  public static Map<String, String> getDSExposeApiMetaData() {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getDSExposeApiMetaData");
    Map<String, String> map = new HashMap<>();
    map.put("dsApiUrl", Cache.getProperty(CacheConstants.DATASOURCE_EXPOSE_API_URL));
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getDSExposeApiMetaData");
    return map;
  }

  public static Boolean isValidateUser(
      String emailId, String password, Long appId, String unqUserId) throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": >> isValidateUser {} {} {}", emailId, appId, unqUserId);

    Integer isUserExistsInSystem = null;
    // for hashCoded password
    ApplicationUser user = ApplicationUserServices.getUserBasedOnEmailId(emailId);
    String pass = GenericStoreDao.encodedPassword(password);

    // check if userid & password matches in database
    isUserExistsInSystem =
        ApplicationUserServices.checkIfUserAlreadyInSystem(emailId, pass, appId);
    if (user != null && isUserExistsInSystem == 0) {
      unqUserId = user.getUserUniqueId().toString();
      return true;
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + ": << isValidateUser {} {} {}", emailId, appId, unqUserId);

    return false;
  }
}

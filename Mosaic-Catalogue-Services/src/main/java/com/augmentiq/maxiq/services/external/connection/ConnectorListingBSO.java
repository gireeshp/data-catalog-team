package com.augmentiq.maxiq.services.external.connection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.augiq.external.source.rdbms.scanning.impl.RDBMSScanner;
import com.augiq.external.source.scanning.Scanner;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSources;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionSubSources;
import com.augmentiq.maxiq.repository.connector.external.connectiondao.ConnectorListingDAO;
import com.augmentiq.maxiq.util.app.context.MosaicAppContext;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
@Transactional
public class ConnectorListingBSO {

	@Autowired
	private ConnectorListingDAO connectorListingDAO;

	private Scanner scanner;

	@Autowired
	private MosaicAppContext mosaicAppContext;

	@Transactional
	public List<ConnectionSources> listConnectionSource() {

		return connectorListingDAO.listConnectionSource();
	}

	@Transactional
	public ConnectionSources listConnectionSourceAndSubSource(Long sourceId) {

		return connectorListingDAO.listConnectionSourceAndSubSource(sourceId);
	}

	@Transactional
	public ConnectionSources listSubSourceConnections(Map<String, Object> reqParam) throws SystemException {

		Long sourceId = Long.valueOf((Integer) reqParam.get("sourceId"));
		Long subSourceId = Long.valueOf((Integer) reqParam.get("subSourceId"));
		ConnectionSources connectionSources = connectorListingDAO.listSubSourceConnections(sourceId, subSourceId);
		return connectionSources;
	}

	@Transactional
	public Map<String, Object> saveNewSubSourceConnection(Map<String, Object> reqParam, String userId)
			throws ConstraintViolationException, SystemException {

		Object connConfig = null;
		String message = null;
		String sourceType = null;
		String subSourceType = null;
		Long subSourceId = null;

		ConnectionConfig connectionConfig = new ConnectionConfig();
		Map<String, Object> connectionInfo = new HashMap<>();
		if (reqParam != null & !reqParam.isEmpty()) {
			subSourceType = reqParam.get("subSourceType").toString();
			connConfig = (Object) reqParam.get("connectionConfig");
			sourceType = reqParam.get("sourceType").toString();
			subSourceId = Long.valueOf((Integer) reqParam.get("subSourceId"));
		}
		if (connConfig != null && sourceType != null && subSourceType != null && subSourceId != null) {
			sourceType = StringUtils.upperCase(sourceType);
			subSourceType = StringUtils.upperCase(subSourceType);
			try {
				connectionConfig = RDBMSScanner.objectParserHandler(connConfig, ConnectionConfig.class);
				reqParam.put("connectionConfig", connectionConfig);
				reqParam.put("subSourceType", subSourceType);
				reqParam.put("connectionName", connectionConfig.getConncetionName());
				Scanner scanner = (Scanner) getApplicationContext().getBean(sourceType);
				message = scanner.testConnection(reqParam);
				if (message.equals(TaskStatusEnums.SUCCESS.name())) {
					connectionConfig.setCreatedBy(userId);
					connectionConfig.setCreatedDate(new Date());
					if (!isDataSourceExist(subSourceId, connectionConfig.getConncetionName()))
						return connectorListingDAO.saveNewSubSourceConnection(subSourceId, connectionConfig);
					else {
						connectionInfo.put(TaskStatusEnums.FAIL.name(),
								"Connection Name '" + connectionConfig.getConncetionName() + "' Already exits..");
						connectionInfo.put("connectionConfig", null);
						return connectionInfo;
					}
				}

			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		connectionInfo.put(TaskStatusEnums.FAIL.name(), "Unable to Save Connection");
		connectionInfo.put("connectionConfig", null);
		return connectionInfo;
	}

	@Transactional
	public void deleteExistingSubSourceConnection(Map<String, Object> reqParam) {
		Long subSourceId = Long.valueOf((Integer) reqParam.get("connectionId"));
		Long connectionId = Long.valueOf((Integer) reqParam.get("connectionId"));
		connectorListingDAO.deleteExistingSubSourceConnection(subSourceId, connectionId);
	}

	public String testConnection(Map<String, Object> reqParam) {
		String message = TaskStatusEnums.FAIL.name();
		String sourceType = null;
		String subSourceType = null;
		Object object = null;
		if (reqParam != null) {
			sourceType = reqParam.get("sourceType").toString();
			subSourceType = reqParam.get("subSourceType").toString();
			object = reqParam.get("connectionConfig");
		}
		ConnectionConfig connectionConfig = new ConnectionConfig();
		if (object != null && sourceType != null && subSourceType != null) {

			try {
				connectionConfig = RDBMSScanner.objectParserHandler(object, ConnectionConfig.class);
				Scanner scanner = (Scanner) getApplicationContext().getBean(sourceType);
				reqParam.put("connectionConfig", connectionConfig);
				reqParam.put("subSourceType", subSourceType);
				message = scanner.testConnection(reqParam);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return message;
		}
		return message;
	}

	/*
	 * @Transactional public ConnectionConfig getConnection(Integer connectionId) {
	 * 
	 * return connectorListingDAO.getConnection(connectionId); }
	 */

	/*
	 * @Transactional public List<DataNode> listAllSchema(ConnectionSources
	 * connectionSources) throws Exception {
	 * 
	 * List<DataNode> dataNodeList = listAllSchema(); return dataNodeList;
	 * 
	 * }
	 */
	@Transactional
	public Map<String, Object> listAllSchema(Map<String, Object> reqParam) throws Exception {

		Long sourceId = null;
		Long subSourceId = null;
		Long connectionId = null;
		Map<String, Object> nodeConnConfigDetails = new HashMap<>();
		if (reqParam != null && !reqParam.isEmpty()) {
			sourceId = Long.valueOf((Integer) reqParam.get("sourceId"));
			subSourceId = Long.valueOf((Integer) reqParam.get("subSourceId"));
			connectionId = Long.valueOf((Integer) reqParam.get("connectionId"));

		}
		// String connectionType =
		// StringUtils.upperCase(connectionSources.getConnectionType());
		if (sourceId != null && subSourceId != null && connectionId != null) {
			List<ConnectionSubSources> connSubSources = new ArrayList<ConnectionSubSources>();
			List<ConnectionConfig> connConfig = new ArrayList<ConnectionConfig>();
			ConnectionSources connectionSources = connectorListingDAO.getConnectionSourceById(sourceId);
			ConnectionSubSources connectionSubSources = connectorListingDAO.getConnectionSubSources(subSourceId);
			ConnectionConfig connectionConfig = connectorListingDAO.getConnectionConfig(connectionId);
			connSubSources.add(connectionSubSources);
			connConfig.add(connectionConfig);
			connectionSubSources.setConnectionConfig(connConfig);
			;
			connectionSources.setConnectionSubSources(connSubSources);
			;
			String sourceType = connectionSources.getConnectionType();
			ApplicationContext applicationContext = getApplicationContext();
			Scanner scanner = (Scanner) applicationContext.getBean(sourceType);
			Map<String, Object> args = new HashMap<String, Object>();
			args.put("ConnectionSources", connectionSources);
			DataNode dataNode = scanner.scan(args);
			nodeConnConfigDetails.put("connectionSources", connectionSources);

			markPublishedNodes(dataNode);

			nodeConnConfigDetails.put("dataNode", dataNode);
			return nodeConnConfigDetails;
		}
		return null;

	}

	// mark published data sources based on datasource name
	public void markPublishedNodes(DataNode dataNode) throws ConnectionException, SystemException {

		List<String> allExistingDSNames = new ArrayList<String>();
		List<DataSourceInstance> allDataSources = DataSourceInstanceDao.getAllDataSourceInstances();
		for (DataSourceInstance datasource : allDataSources) {
			allExistingDSNames.add(datasource.getDataSourceName());
		}

		if (dataNode.getIsDsNode() && allExistingDSNames.contains(dataNode.getLabel())) {
			dataNode.setIsPublished(true);
		}

		markDSNodes(allExistingDSNames, dataNode.getChildren());
	}

	// mark published childs
	public void markDSNodes(List<String> existingNames, Collection<DataNode> dataNodes) {

		for (DataNode dataNode : dataNodes) {
			if (dataNode.getIsDsNode()) {
				if (Pattern.compile(Pattern.quote(dataNode.getLabel()), Pattern.CASE_INSENSITIVE)
						.matcher(existingNames.toString()).find()) {
					dataNode.setIsPublished(true);
				}
			}
			markDSNodes(existingNames, dataNode.getChildren());
		}
	}

	@Transactional
	public ConnectionConfig getConnectionConfigById(Long connectionId) {

		return connectorListingDAO.getConnectionConfig(connectionId);
	}

	public Map<String, List<String>> publishDataSources(Map<String, Object> reqData, String userId, Long groupId)
			throws Exception {

		ConnectionSources connectionSources = RDBMSScanner
				.objectParserHandler((Object) reqData.get("connectionSources"), ConnectionSources.class);
		reqData.put("userId", userId);
		reqData.put("groupId", groupId);
		String connectionType = StringUtils.upperCase(connectionSources.getConnectionType());
		ApplicationContext applicationContext = getApplicationContext();
		Scanner scanner = (Scanner) applicationContext.getBean(connectionType);
		return scanner.publishDataSources(reqData);

	}

	public Map<String, List<String>> checkDuplicateDSNames(List<DataNode> dataNodes) {
		Map<String, List<String>> res = new HashMap<String, List<String>>();
		List<String> existingDSName = new ArrayList<String>();

		List<String> dsNamesSelected = new ArrayList<String>();
		for (DataNode dataNode : dataNodes) {
			if (dataNode.getIsDsNode() && dataNode.getSelected())
				dsNamesSelected.add(dataNode.getLabel());
		}

		try {
			List<DataSourceInstance> allDataSources = DataSourceInstanceDao.getAllDataSourceInstances();
			for (DataSourceInstance datasource : allDataSources) {
				if (dsNamesSelected.contains(datasource.getDataSourceName())) {
					existingDSName.add(datasource.getDataSourceName());
					dsNamesSelected.remove(datasource.getDataSourceName());
				}
			}
		} catch (ConnectionException | SystemException e) {
			e.printStackTrace();
		}

		if (existingDSName.isEmpty()) {
			res.put("SUCCESS", null);
			return res;
		}

		res.put("FAIL", existingDSName);
		return res;
	}

	public ApplicationContext getApplicationContext() {
		return mosaicAppContext.getContext();
	}

	public ConnectorListingDAO getConnectorListingDAO() {
		return connectorListingDAO;
	}

	public void setConnectorListingDAO(ConnectorListingDAO connectorListingDAO) {
		this.connectorListingDAO = connectorListingDAO;
	}

	public boolean isDataSourceExist(Long subSourceId, String ConncetionName) throws SystemException {
		return connectorListingDAO.isDataSourceExist(subSourceId, ConncetionName);
	}

}

package com.augmentiq.maxiq.services.flavour;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.dao.flavour.FlavourDAO;
import com.augmentiq.maxiq.entity.model.setup.domains.ActionsMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.BlockedUrlActionMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;

public class FlavourBso {

  private static final Logger logger = LoggerFactory.getLogger(FlavourBso.class);

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<String> getPersonas(String userId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getPersonas() " + userId);

    List<PersonaMaster> personasList = FlavourDAO.getPersonas(userId);
    if (null != personasList) {
      List<String> personasIdList = new ArrayList<>(personasList.size());
      for (PersonaMaster flavourActionMappings : personasList) {
        String id = flavourActionMappings.getPersona_id().toString();
        if (null != id) personasIdList.add(id);
      }
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << actionCodeList " + personasIdList);
      return personasIdList;
    } else {
      throw new SystemException("Personas not found.");
    }
  }

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<String> getActions(String userId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getActions() " + userId);

    List<ActionsMaster> actionsMasterList = FlavourDAO.getActions(userId);
    if (null != actionsMasterList) {
      List actionCodeList = new ArrayList<>(actionsMasterList.size());
      for (ActionsMaster actionsMaster : actionsMasterList) {
        String actionCode = actionsMaster.getActionCode();
        if (null != actionCode) actionCodeList.add(actionCode);
      }
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << actionCodeList " + actionCodeList);
      return actionCodeList;
    } else {
      throw new SystemException("Actions not found.");
    }
  }

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<String> getBlockedUrlList(String userId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getBlockedUrlList() " + userId);

    List<BlockedUrlActionMappings> blockedUrlActionMappingList =
        FlavourDAO.getBlockedUrlList(userId);
    if (null != blockedUrlActionMappingList) {
      List<String> boackedUrlList = new ArrayList<>(blockedUrlActionMappingList.size());
      for (BlockedUrlActionMappings blockedUrlActionMappings : blockedUrlActionMappingList) {
        String url = blockedUrlActionMappings.getUrl();
        if (null != url) boackedUrlList.add(url);
      }
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << boackedUrlList " + boackedUrlList);
      return boackedUrlList;
    } else {
      throw new SystemException("BlockedUrl not found.");
    }
  }
}

package com.augmentiq.maxiq.services.flavour.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.services.flavour.FlavourBso;

public class FlavourService {

  private static final Logger logger = LoggerFactory.getLogger(FlavourService.class);

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<String> getPersonas(String userId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getPersonas():" + userId);
    List<String> personasList = FlavourBso.getPersonas(userId);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getPersonas():personasList" + personasList);
    return personasList;
  }

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<String> getActions(String userId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getActions():" + userId);
    List<String> actions = FlavourBso.getActions(userId);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getActions():actions" + actions);
    return actions;
  }

  /**
   * @param userId
   * @return
   * @throws SystemException
   */
  public static List<String> getBlockedUrlList(String userId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getBlockedUrlList():" + userId);
    List<String> bloackedUrlList = FlavourBso.getBlockedUrlList(userId);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getBlockedUrlList():bloackedUrlList" + bloackedUrlList);
    return bloackedUrlList;
  }
}

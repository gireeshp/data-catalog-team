package com.augmentiq.maxiq.services.genericservices;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;

/** Created by shivanand on 7/3/15. */
public class InputOutputService {

  public static List<String> readFromFile(String fileName) throws SystemException {

    List<String> list = new ArrayList<>();
    BufferedReader br = null;

    try {

      String sCurrentLine;

      br = new BufferedReader(new FileReader(fileName));

      while ((sCurrentLine = br.readLine()) != null) {
        list.add(sCurrentLine);
      }

    } catch (IOException e) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
    } finally {
      try {
        if (br != null) br.close();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }

    return list;
  }

  public static void writeFile(String fileName, boolean append, List<String> list)
      throws IOException {
    if (null != list && list.size() > 0) {
      FileWriter fileWriter = new FileWriter(fileName, append);
      BufferedWriter bw = new BufferedWriter(fileWriter);
      for (String str : list) {
        bw.append(str + "\n");
      }
      bw.close();
    }
  }
}

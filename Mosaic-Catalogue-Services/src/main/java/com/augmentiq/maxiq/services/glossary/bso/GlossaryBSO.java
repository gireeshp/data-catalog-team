package com.augmentiq.maxiq.services.glossary.bso;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.core.dao.glossarydao.GlossaryDAO;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlossaryMaster;
import com.augmentiq.maxiq.model.services.glossary.GlossaryDTO;
public class GlossaryBSO {
  private static final Logger logger = LoggerFactory.getLogger(GlossaryBSO.class);

  public Long addTag(GlossaryMaster glossaryMaster) throws SystemException {
    return GlossaryDAO.addTag(glossaryMaster);
  }

  /**
   * @return List of active tags only
   * @throws SystemException
   */
  public List<GlossaryDTO> getAllActiveTags() throws SystemException {
    List<GlossaryDTO> listOfGlossary =
        GlossaryDAO.getAllTagsByParams(QueryConstants.GlossaryMaster.ACTIVE);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllActiveTags()"
            + ParamUtils.getString("List = " + listOfGlossary));
    return (listOfGlossary == null || listOfGlossary.size() == 0)
        ? new ArrayList<GlossaryDTO>(1)
        : listOfGlossary;
  }

  /**
   * @return List of used tags only
   * @throws SystemException
   */
  public List<String> getAllDSWithWithTagUsed(GlossaryMaster glossaryMaster)
      throws SystemException {
    List<String> listOfGlossary = GlossaryDAO.getAllDSWithWithTagUsed(glossaryMaster);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getAllActiveTags()"
            + ParamUtils.getString("List = " + listOfGlossary));
    return (listOfGlossary == null || listOfGlossary.size() == 0)
        ? new ArrayList<String>(1)
        : listOfGlossary;
  }

  public void deleteTag(GlossaryMaster glossaryMaster) throws SystemException {
    GlossaryDAO.deleteTag(glossaryMaster);
  }
}

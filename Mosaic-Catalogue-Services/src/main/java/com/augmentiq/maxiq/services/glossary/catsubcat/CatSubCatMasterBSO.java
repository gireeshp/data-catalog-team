package com.augmentiq.maxiq.services.glossary.catsubcat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.entity.model.configuration.bean.CategorySubCatMaster;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.glossary.catsubcat.CatSubCatMasterDTO;
import com.augmentiq.maxiq.glossary.catsubcat.dao.CatSubCatMasterDAO;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

public class CatSubCatMasterBSO {

  private static final Logger logger = LoggerFactory.getLogger(CatSubCatMasterBSO.class);

  public void insertCatSubCatMaster(CategorySubCatMaster catSubCatmaster) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> insertCatSubCatMaster()"
            + ParamUtils.getString(catSubCatmaster));
    CategorySubCatMaster catMaster = getCategoryAndSubCategoryById(catSubCatmaster.getId());
    if (null != catMaster) {
      String query =
          "update "
              + Sequences.CATEGORY_SUBCATEGORY
              + " set status=1, "
              + QueryConstants.CatSubCatMaster.MODIFIED_BY
              + " = ? , "
              + QueryConstants.CatSubCatMaster.MODIFIED_DATE
              + " = ? where "
              + QueryConstants.CatSubCatMaster.ID
              + " = ? ";

      Map<String, Object> params = new LinkedHashMap<String, Object>();
      params.put(QueryConstants.CatSubCatMaster.MODIFIED_BY, catSubCatmaster.getCreatedBy());
      params.put(QueryConstants.CatSubCatMaster.MODIFIED_DATE, System.currentTimeMillis());
      params.put(QueryConstants.CatSubCatMaster.ID, catSubCatmaster.getId());

      MySqlOperations.executeQuery(query, params);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << Updated the record"
              + ParamUtils.getString(catSubCatmaster));
      return;
    }
    catSubCatmaster.setId(null);
    catSubCatmaster.setStatus(Integer.parseInt(QueryConstants.GlossaryMaster.ACTIVE));
    catSubCatmaster.setCreatedDate(System.currentTimeMillis());
    new CatSubCatMasterDAO().insertCategory(catSubCatmaster);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << insertCatSubCatMaster()"
            + ParamUtils.getString(catSubCatmaster));
  }

  /**
   * @param objectId
   * @param objectType
   * @param userId
   * @throws Exception
   */
  public void deleteFromCategoryAndSubCat(Long id, Long userId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> deleteFromCategoryAndSubCat()"
            + ParamUtils.getString(id, userId));
    //Check is used in datasources or not
    CategorySubCatMaster categoryOrSubCatObj = getCategoryAndSubCategoryById(id);
    if (isCatOrSubCatUsedInDatasource(categoryOrSubCatObj)) {
      ExceptionsMessanger.throwException(
          new SystemException(),
          "ERR_179",
          categoryOrSubCatObj.getParentId() == 0 ? "Category" : "SubCategory");
    }

    if (categoryOrSubCatObj.getParentId() == 0
        && !getAllOfActiveSubCategoriesByCategoryId(id).isEmpty()) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_180");
    }

    String query =
        "update "
            + Sequences.CATEGORY_SUBCATEGORY
            + " set status=0,  "
            + QueryConstants.CatSubCatMaster.MODIFIED_BY
            + " = ? , "
            + QueryConstants.CatSubCatMaster.MODIFIED_DATE
            + " = ? where "
            + QueryConstants.CatSubCatMaster.ID
            + " = ? ";

    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put(QueryConstants.CatSubCatMaster.MODIFIED_BY, userId);
    params.put(QueryConstants.CatSubCatMaster.MODIFIED_DATE, System.currentTimeMillis());
    params.put(QueryConstants.CatSubCatMaster.ID, id);

    new CatSubCatMasterDAO().deleteFromCategoryAndSubCat(query, params);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << deleteFromCategoryAndSubCat()"
            + ParamUtils.getString(id, userId, query));
  }

  /**
   * This method updates the datasourceinstance table
   *
   * @param categorySubCatMaster
   * @throws SystemException
   */
  public void removeCategoryOrSubCategoryFromDataSourceInstanceForAllDataSources(
      CategorySubCatMaster categorySubCatMaster) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> removeCategoryOrSubCategoryFromDataSourceInstanceForAllDataSources() {}",
        categorySubCatMaster);

    String query = "";
    if (categorySubCatMaster.getParentId() == 0) {
      query =
          "update "
              + Sequences.DATASOURCE_INSTANCE
              + " set "
              + QueryConstants.DataSourceInstance.CATEGORY
              + " = ?, "
              + QueryConstants.GlobalSearch.IS_MODIFIED
              + " = ? where "
              + QueryConstants.DataSourceInstance.CATEGORY
              + " = ?";
    } else {

      query =
          "update "
              + Sequences.DATASOURCE_INSTANCE
              + " set "
              + QueryConstants.DataSourceInstance.SUBCATEGORY
              + " = ?, "
              + QueryConstants.GlobalSearch.IS_MODIFIED
              + " = ? where "
              + QueryConstants.DataSourceInstance.SUBCATEGORY
              + " = ?";
    }
    Map<String, Object> params = new LinkedHashMap<String, Object>();
    params.put(QueryConstants.DataSourceInstance.CATEGORY, null);
    params.put(
        QueryConstants.GlobalSearch.IS_MODIFIED, QueryConstants.GlobalSearch.IS_MODIFIED_TRUE);
    params.put(QueryConstants.Parameters.PARAM_1, categorySubCatMaster.getCatOrSubCatName());

    MySqlOperations.executeQuery(query, params);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << removeCategoryOrSubCategoryFromDataSourceInstanceForAllDataSources() {}",
        categorySubCatMaster);
  }

  /**
   * @return - List of all CategorySubCatMaster which are not deleted
   * @throws Exception
   */
  public List<CatSubCatMasterDTO> getAllOfActiveCategorySubCatMaster() throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getAllOfActiveCategorySubCatMaster()");
    String query =
        "SELECT *, "
            + "(select concat(au.firstName,\" \",au.LastName) from  application_user au  where au.unqUserId=csm.createdBy) as createdByName, "
            + "(select concat(au.firstName,\" \",au.LastName) from  application_user au where au.unqUserId=csm.modifiedBy) as modifiedByName "
            + " FROM category_subcategory_master csm where status='1'";
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getAllOfActiveCategorySubCatMaster()");
    return new CatSubCatMasterDAO().getAllOfCategorySubCatMasterDTO(query);
  }

  /**
   * @return - List of all CategorySubCatMaster
   * @throws Exception
   */
  public Map<String, List<String>> getAllOfCategorySubCatMaster() throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getAllOfCategorySubCatMaster()");
    List<CatSubCatMasterDTO> list = getAllOfActiveCategorySubCatMaster();
    Map<String, List<String>> categories = new HashMap<String, List<String>>();

    if(list != null && list.size() > 0) {
      for (CatSubCatMasterDTO cat : list) {
        if (cat.getParentId() == 0) {
          categories.put(cat.getCatOrSubCatName(), getListOfAllSubCategory(cat, list));
        }
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": << getAllOfCategorySubCatMaster()"
            + ParamUtils.getString(categories));
    return categories;
  }

  private List<String> getListOfAllSubCategory(
      CatSubCatMasterDTO cat, List<CatSubCatMasterDTO> listOfCatsub) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": >> getListOfAllSubCategory()"
            + ParamUtils.getString(cat, listOfCatsub));
    List<String> list = new ArrayList<>();
    for (CatSubCatMasterDTO category : listOfCatsub) {
      if (cat.getId().equals(category.getParentId())) {
        list.add(category.getCatOrSubCatName());
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": << getListOfAllSubCategory()"
            + ParamUtils.getString(list));
    return list;
  }

  /**
   * @param id
   * @return
   * @throws Exception
   */
  public CategorySubCatMaster getCategoryAndSubCategoryById(Long id) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": >> getCategoryAndSubCategoryById()"
            + ParamUtils.getString(id));
    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.CatSubCatMaster.ID, id);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getCategoryAndSubCategoryById()");
    return new CatSubCatMasterDAO().getCategoryAndSubCategoryById(queryMap);
  }

  /**
   * @param name
   * @return
   * @throws Exception
   */
  public boolean getCategoryAndSubCategoryByName(String name) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": >> getCategoryAndSubCategoryByName()"
            + ParamUtils.getString(name));
    Map<String, Object> queryMap = new LinkedHashMap<String, Object>();
    queryMap.put(QueryConstants.CatSubCatMaster.CAT_OR_SUBCAT_NAME, name);
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getCategoryAndSubCategoryByName()");
    return new CatSubCatMasterDAO().getCategoryAndSubCategoryByName(queryMap);
  }

  /** Delete category with subcategories only */
  public void deleteAllSubCategoryByCategory(Long id, Long userId) throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": >> deleteAllSubCategoryByCategory()"
            + ParamUtils.getString(id, userId));
    List<CategorySubCatMaster> listOfSubCategories = getAllOfActiveSubCategoriesByCategoryId(id);
    if (null != listOfSubCategories) {
      for (CategorySubCatMaster subCategory : listOfSubCategories)
        deleteFromCategoryAndSubCat(subCategory.getId(), userId);
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << deleteAllSubCategoryByCategory()");
  }

  /**
   * @return - List of all CategorySubCatMaster which are not deleted
   * @throws Exception
   */
  public List<CategorySubCatMaster> getAllOfActiveSubCategoriesByCategoryId(Long categoryId)
      throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": >> getAllOfActiveCategorySubCatMaster()");
    String query =
        QueryConstants.Query.SELECT_STAR_FROM
            + Sequences.CATEGORY_SUBCATEGORY
            + QueryConstants.Query.WHERE
            + " status='1' and parentId="
            + categoryId;
    logger.debug(LoggerConstants.LOG_MAXIQWEB + ": << getAllOfActiveCategorySubCatMaster()");
    return new CatSubCatMasterDAO().getAllOfCategorySubCatMaster(query);
  }

  /**
   * @return - true if datasources found by category else false
   * @throws Exception
   */
  public boolean isCatOrSubCatUsedInDatasource(CategorySubCatMaster categorySubCatMaster)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": >> isUsedInDatasource()"
            + ParamUtils.getString(categorySubCatMaster));
    List<DataSourceInstance> listOfDatasourceIntancesByCat =
        DataSourceInstanceDao.getAllDataSourceInstancesByCatOrSubCatName(categorySubCatMaster);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + ": << isUsedInDatasource()"
            + ParamUtils.getString(
                null != listOfDatasourceIntancesByCat ? listOfDatasourceIntancesByCat : ""));
    return (null == listOfDatasourceIntancesByCat || listOfDatasourceIntancesByCat.isEmpty())
        ? false
        : true;
  }
}

package com.augmentiq.maxiq.services.index.kibana;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.entity.model.search.domian.IndexDefination;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

/** Created by Rushikesh Raut for MAX-521 on 01-Oct-2016 */
public class KibanaServices {

  private static Logger logger = LoggerFactory.getLogger(KibanaServices.class);

  public static void createIndexInKibana(IndexDefination indexDefination, String xsrfToken)
      throws IOException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << resisterIndexInKibana()"
            + ParamUtils.getString(indexDefination));

    String kibanaPath = Cache.getProperty(CacheConstants.KIBANA_SERVER_PATH);

    OkHttpClient client = new OkHttpClient();

    if (null == xsrfToken || xsrfToken.equalsIgnoreCase("null")) {
      xsrfToken = Constants.XSRF_TOKEN;
    }

    MediaType mediaType = MediaType.parse("application/octet-stream");
    RequestBody body =
        RequestBody.create(mediaType, "{\"title\":\"" + indexDefination.getIndexName() + "\"}");
    Request request =
        new Request.Builder()
            .url(
                kibanaPath
                    + "/elasticsearch/.kibana/index-pattern/"
                    + indexDefination.getIndexName()
                    + "?op_type=create")
            .post(body)
            .addHeader("kbn-xsrf-token", xsrfToken)
            .addHeader("cache-control", "no-cache")
            .build();

    Response response = client.newCall(request).execute();
  }

  public static void deleteIndexFromKibana(IndexDefination indexDefination, String xsrfToken)
      throws IOException {

    String kibanaPath = Cache.getProperty(CacheConstants.KIBANA_SERVER_PATH);
    OkHttpClient client = new OkHttpClient();

    if (null == xsrfToken || xsrfToken.equalsIgnoreCase("null")) {
      xsrfToken = Constants.XSRF_TOKEN;
    }

    Request request =
        new Request.Builder()
            .url(
                kibanaPath
                    + "/elasticsearch/.kibana/index-pattern/"
                    + indexDefination.getIndexName())
            .delete(null)
            .addHeader("kbn-xsrf-token", xsrfToken)
            .addHeader("cache-control", "no-cache")
            .build();

    Response response = client.newCall(request).execute();
  }
}

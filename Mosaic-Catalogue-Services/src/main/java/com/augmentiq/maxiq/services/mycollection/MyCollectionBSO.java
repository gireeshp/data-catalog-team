package com.augmentiq.maxiq.services.mycollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.core.dao.mycollection.MyCollectionDAO;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.MyCollection;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

public class MyCollectionBSO {

  public static void addToMyCollection(Long objectId, String objectType, Long userId)
      throws Exception {
    MyCollectionDAO.addToMyCollection(objectId, objectType, userId);
  }

  public static void removeFromMyCollection(Long objectId, String objectType, Long userId)
      throws Exception {
    MyCollectionDAO.removeFromMyCollection(objectId, objectType, userId);
  }

  public static Collection<DataSourceInstance> getAllOfMyCollection(Long userId) throws Exception {
    List<MyCollection> myCollectionList = MyCollectionDAO.getAllOfMyCollection(userId);
    Collection<DataSourceInstance> domains = null;
    Integer count = 0;
    if (myCollectionList != null) {
      count = myCollectionList.size();
      domains = new ArrayList<DataSourceInstance>(count);
      for (MyCollection myCollection : myCollectionList) {
        DataSourceInstance dataSourceInstance =
            DataSourceInstanceDao.getDataSourceInstanceById(myCollection.getObjectId());
        if (dataSourceInstance != null) {
          domains.add(dataSourceInstance);
        }
      }
    }
    return domains;
  }

  public static MyCollection getMyCollectionDetailsFromDataSourceId(Long dataSourceId, Long userId)
      throws Exception {
    return MyCollectionDAO.getMyCollectionById(
        userId, dataSourceId, ObjectTypes.DATA_SOURCE.toString());
  }
}
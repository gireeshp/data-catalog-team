package com.augmentiq.maxiq.services.object.request;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.configuration.enums.AccessTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataRequestEnum;
import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.core.dao.searchdao.IndexDefinationDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRequest;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchResult;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchWithObjectInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.GroupObjectMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.MyCollection;
import com.augmentiq.maxiq.entity.model.configuration.bean.Notification;
import com.augmentiq.maxiq.entity.model.configuration.bean.ObjectRequestToApproveDTO;
import com.augmentiq.maxiq.entity.model.configuration.bean.ProjectsBean;
import com.augmentiq.maxiq.entity.model.configuration.bean.Record;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserObjectMapping;
import com.augmentiq.maxiq.entity.model.search.domian.IndexDefination;
import com.augmentiq.maxiq.model.object.request.DataRequestDTO;
import com.augmentiq.maxiq.object.request.dao.ObjectRequestDAO;
import com.augmentiq.maxiq.services.access.manage.AccessManageBSO;
import com.augmentiq.maxiq.services.mycollection.MyCollectionBSO;
import com.augmentiq.maxiq.services.userservice.ApplicationUserServices;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

public class ObjectRequestBSO {

  private static final Logger logger = LoggerFactory.getLogger(ObjectRequestBSO.class);

  public static void createObjectRequest(
      DataRequestDTO datarequestFromUi, Long userId, Boolean isRequestedForGroup) throws Exception {
    ObjectRequestDAO.createObjectRequest(datarequestFromUi, userId, isRequestedForGroup);
  }

  public static void updateRequestDataSourceRequest(
      Long dataSourceId,
      Long from,
      DataRequestEnum status,
      Long lastModifiedBy,
      String rejectJustification)
      throws Exception {
    ObjectRequestDAO.updateRequestDataSourceRequest(
        dataSourceId, from, status, lastModifiedBy, rejectJustification);
  }

  public static List<DataRequest> getAllAcceptedRequests(Long userId) throws Exception {
    return ObjectRequestDAO.getAllAcceptedRequests(userId);
  }

  public static GlobalSearchWithObjectInstance getAllRequestedObjects(Long userId, Long groupId)
      throws Exception {
    List<DataRequest> dataRequestList = ObjectRequestDAO.getAllRequestedObjects(userId, groupId);
    return getGlobalSearchWithThereStatusByDataRequestList(dataRequestList);
  }

  public static GlobalSearchWithObjectInstance getObjectsForApproval(Long userId) throws Exception {
    List<DataRequest> dataRequestList = ObjectRequestDAO.getObjectsForApproval(userId);
    return getGlobalSearchWithThereStatusByDataRequestList(dataRequestList);
  }

  public static GlobalSearchWithObjectInstance getGlobalSearchWithThereStatusByDataRequestList(
      List<DataRequest> dataRequestList) throws ConnectionException, SystemException {
    List<ObjectRequestToApproveDTO> domains = null;
    GlobalSearchWithObjectInstance globlSearchWithDSInstance = new GlobalSearchWithObjectInstance();
    List<GlobalSearchWithObjectInstance.Item> listOfStatus =
        new ArrayList<GlobalSearchWithObjectInstance.Item>();
    Integer count = 0;
    if (dataRequestList != null) {
      count = dataRequestList.size();
      domains = new ArrayList<ObjectRequestToApproveDTO>(count);
      for (DataRequest dataRequest : dataRequestList) {
        if (dataRequest != null) {
          String rejectedJustification = dataRequest.getRejected_justification();
          if (StringUtils.isBlank(rejectedJustification)
              || StringUtils.equalsIgnoreCase(rejectedJustification, GENERAL_CONSTANTS.NULL)) {
            rejectedJustification = "";
          }
          String justification = dataRequest.getJustification();
          if (StringUtils.isBlank(justification)
              || StringUtils.equalsIgnoreCase(justification, GENERAL_CONSTANTS.NULL)) {
            justification = "";
          }
          ObjectRequestToApproveDTO objectRequestToApproveDTO = new ObjectRequestToApproveDTO();

          objectRequestToApproveDTO.setId(dataRequest.getId());
          objectRequestToApproveDTO.setObjectId(dataRequest.getObjectId());
          objectRequestToApproveDTO.setObjectType(dataRequest.getObjectType());
          objectRequestToApproveDTO.setRequestedBy(
              ApplicationUserServices.getUserName(String.valueOf(dataRequest.getRequestedBy())));
          objectRequestToApproveDTO.setFromGroup(
              ApplicationUserServices.getGroupName(dataRequest.getGroup_id()));
          objectRequestToApproveDTO.setRequestedDate(dataRequest.getCreated_date());
          //below two lines will explanation
          //below will check if user is hard deleted then will set requested user to MAXIQ BOT
          String requestedUser =
              ApplicationUserServices.getUserName(
                  String.valueOf(dataRequest.getRequested_user_id()));
          objectRequestToApproveDTO.setRequestedUser(
              requestedUser != null && StringUtils.isNotBlank(requestedUser)
                  ? requestedUser
                  : "Maxiq Bot");
          objectRequestToApproveDTO.setRejectedJustification(rejectedJustification);
          objectRequestToApproveDTO.setJustification(justification);

          //if (dataRequest.getObjectType() == ObjectTypes.DATA_SOURCE.toString()) {
          if (dataRequest.getObjectType() != null
              && dataRequest.getObjectType().equalsIgnoreCase(ObjectTypes.DATA_SOURCE.toString())) {

            DataSourceInstance dataSourceInstance =
                DataSourceInstanceDao.getDataSourceInstanceById(dataRequest.getObjectId());
            String category = "", subCategory = "";
            if (dataSourceInstance != null) {
              category = dataSourceInstance.getCategory();
              subCategory = dataSourceInstance.getSubCategory();
              objectRequestToApproveDTO.setObjectName(dataSourceInstance.getDataSourceName());
              objectRequestToApproveDTO.setCategory(category);
              objectRequestToApproveDTO.setSubcategory(subCategory);
              domains.add(objectRequestToApproveDTO);
            }
            GlobalSearchWithObjectInstance gi = new GlobalSearchWithObjectInstance();

            listOfStatus.add(
                gi
                .new Item(
                    DataRequestEnum.get(dataRequest.getStatus()).getName(), category, subCategory));
          } else if (dataRequest.getObjectType() != null
              && dataRequest.getObjectType().equalsIgnoreCase(ObjectTypes.INDEX.toString())) {
            IndexDefination indexDefination =
                IndexDefinationDao.getIndexDefination(dataRequest.getObjectId());
            objectRequestToApproveDTO.setObjectName(indexDefination.getIndexName());
            domains.add(objectRequestToApproveDTO);
            GlobalSearchWithObjectInstance gi = new GlobalSearchWithObjectInstance();
            listOfStatus.add(
                gi.new Item(DataRequestEnum.get(dataRequest.getStatus()).getName(), "", ""));
          }
        }
      }
    }
    globlSearchWithDSInstance.setRequestedObjects(domains);
    globlSearchWithDSInstance.setTotalCount(Long.valueOf(count));
    globlSearchWithDSInstance.setRequestIndicator(listOfStatus);
    return globlSearchWithDSInstance;
  }
  //required for object request
  public static DataRequestEnum getStausOfObjectRequestAndCollectionForObject(
      String userId, String groupId, String objectId, String objectType) throws Exception {
    if (objectType.equals(ObjectTypes.DATA_SOURCE.toString())) {

      DataSourceInstance dataSourceInstance =
          DataSourceInstanceDao.getDataSourceInstance(Long.valueOf(objectId));
      return getStatusFromRequestsAndMyCollection(
          userId, Long.valueOf(groupId), dataSourceInstance);
    }
    return DataRequestEnum.NA;
  }
  public static DataRequestEnum getAddedMycollectionDataSource(
	      Long userId, Long  objectId, String objectType) throws Exception {
	    if (objectType.equals(ObjectTypes.DATA_SOURCE.toString())) {

	    	return ObjectRequestDAO.getAddedMycollectionDataSource(userId,objectId,objectType);
	    }
	    return DataRequestEnum.NA;
	  }

  public static void acceptObjectRequest(Long requestId, String justification) throws Exception {
    ObjectRequestDAO.acceptDataSourceRequest(requestId, justification);
    DataRequest dataRequest = ObjectRequestDAO.getDataRequestById(requestId);
    if (null != dataRequest && -1L != dataRequest.getGroup_id()) {
      AccessTypeEnum requestedAccess = AccessTypeEnum.get(dataRequest.getRequestedAccess());

      GroupObjectMapping groupObjectMapping = new GroupObjectMapping();
      groupObjectMapping.setGroupId(dataRequest.getGroup_id());
      groupObjectMapping.setObjectId(dataRequest.getObjectId());
      groupObjectMapping.setObjectType(dataRequest.getObjectType());
      groupObjectMapping.setAccessType(AccessTypeEnum.USE.getValue());

      ObjectRequestDAO.insertGroupObjectMapping(groupObjectMapping);

      if (requestedAccess == AccessTypeEnum.LOAD || requestedAccess == AccessTypeEnum.EDIT) {
        groupObjectMapping.setAccessType(requestedAccess.getValue());

        ObjectRequestDAO.insertGroupObjectMapping(groupObjectMapping);
      } else if (requestedAccess == AccessTypeEnum.LOAD_EDIT) {
        groupObjectMapping.setAccessType(AccessTypeEnum.LOAD.getValue());
        ObjectRequestDAO.insertGroupObjectMapping(groupObjectMapping);

        groupObjectMapping.setAccessType(AccessTypeEnum.EDIT.getValue());
        ObjectRequestDAO.insertGroupObjectMapping(groupObjectMapping);
      }
    } else if (null != dataRequest && (-1L == dataRequest.getGroup_id())) {
      AccessTypeEnum requestedAccess = AccessTypeEnum.get(dataRequest.getRequestedAccess());

      UserObjectMapping userObjectMapping = new UserObjectMapping();
      userObjectMapping.setObjectId(dataRequest.getObjectId());
      userObjectMapping.setObjectType(dataRequest.getObjectType());
      userObjectMapping.setUserId(dataRequest.getRequestedBy());
      userObjectMapping.setAccessType(AccessTypeEnum.USE.getValue());

      ObjectRequestDAO.insertUserObjectMapping(userObjectMapping);

      if (requestedAccess == AccessTypeEnum.LOAD || requestedAccess == AccessTypeEnum.EDIT) {
        userObjectMapping.setAccessType(requestedAccess.getValue());

        ObjectRequestDAO.insertUserObjectMapping(userObjectMapping);
      } else if (requestedAccess == AccessTypeEnum.LOAD_EDIT) {
        userObjectMapping.setAccessType(AccessTypeEnum.LOAD.getValue());
        ObjectRequestDAO.insertUserObjectMapping(userObjectMapping);

        userObjectMapping.setAccessType(AccessTypeEnum.EDIT.getValue());
        ObjectRequestDAO.insertUserObjectMapping(userObjectMapping);
      }
    }
  }

  public static void rejectObjectRequest(Long requestId, String justification)
      throws SystemException {
    ObjectRequestDAO.rejectObjectRequest(requestId, justification);
  }

  //required for elastic search
  public static GlobalSearchWithObjectInstance getDataSourcesFromGlobalSearchResult(
      GlobalSearchResult searchForGivenInput, String userId, Long groupId) throws Exception {
    GlobalSearchWithObjectInstance globalSearchWithDsInstance =
        new GlobalSearchWithObjectInstance();
    if (null != searchForGivenInput) {
      List<Record> records = searchForGivenInput.getRecords();
      if (null != records && records.size() > 0) {
        String query = createQueryFromListOfRecords(records);

        List<DataSourceInstance> dsInstances =
            MySqlOperations.scanWithSqlQuery(DataSourceInstance.class, query, null);
        List<GlobalSearchWithObjectInstance.Item> statusList =
            getStatusListFromDataSourceInstanceList(userId, dsInstances, groupId, records);

        globalSearchWithDsInstance.setRecords(dsInstances);
        globalSearchWithDsInstance.setRequestIndicator(statusList);
        globalSearchWithDsInstance.setTotalCount(searchForGivenInput.getTotalCount());
        globalSearchWithDsInstance.setGlobalSearchResult(searchForGivenInput);

        return globalSearchWithDsInstance;
      } else {
        globalSearchWithDsInstance.setRecords(new ArrayList<DataSourceInstance>());
        globalSearchWithDsInstance.setTotalCount(0l);

        return globalSearchWithDsInstance;
      }
    } else {
      globalSearchWithDsInstance.setRecords(new ArrayList<DataSourceInstance>());
      globalSearchWithDsInstance.setTotalCount(0l);
    }

    return globalSearchWithDsInstance;
  }

  //required for elastic search
  public static DataRequestEnum getStatusFromRequestsAndMyCollection(
      String userId, Long groupId, DataSourceInstance dataSourceInstance)
      throws SystemException, Exception {
    dataSourceInstance.setCreatedBy(
        ApplicationUserServices.getUserName(dataSourceInstance.getCreatedBy()));
    dataSourceInstance.setLastRunBy(
        ApplicationUserServices.getUserName(dataSourceInstance.getLastRunBy()));

    DataRequestEnum dataRequestStatus =
        ObjectRequestDAO.getStausOfObjectRequest(
            Long.valueOf(userId),
            dataSourceInstance.getDataSourceId(),
            ObjectTypes.DATA_SOURCE.toString());
    if (dataRequestStatus == null
        || AccessManageBSO.isObjectRequestedForGroup(
                groupId, dataSourceInstance.getDataSourceId(), ObjectTypes.DATA_SOURCE.toString())
            && dataRequestStatus == DataRequestEnum.PENDING) {
      dataRequestStatus = DataRequestEnum.NA;
      return dataRequestStatus;
    } else if (AccessManageBSO.isObjectRequestedForUser(
            Long.valueOf(userId),
            dataSourceInstance.getDataSourceId(),
            ObjectTypes.DATA_SOURCE.toString())
        && dataRequestStatus == DataRequestEnum.PENDING) {
      dataRequestStatus = DataRequestEnum.PENDING;
      return dataRequestStatus;
    } 
    if (dataRequestStatus == DataRequestEnum.ACCEPT) {
      MyCollection myCollection =
          MyCollectionBSO.getMyCollectionDetailsFromDataSourceId(
              dataSourceInstance.getDataSourceId(), Long.parseLong(userId));
      if (null != myCollection && myCollection.getStatus() == 1) {
        dataRequestStatus = DataRequestEnum.REMOVE;
      }
    }
    return dataRequestStatus;
  }
  
  
  //TODO
  /*public static DataRequestEnum getStatusFromMyCollection(String userId, Long groupId,
  		DataSourceInstance dataSourceInstance) throws SystemException, Exception {
  	dataSourceInstance.setCreatedBy(ApplicationUserServices.getUserName(dataSourceInstance.getCreatedBy()));
  	dataSourceInstance.setLastRunBy(ApplicationUserServices.getUserName(dataSourceInstance.getLastRunBy()));
  	DataRequestEnum dataRequestStatus = DataSourceRequestDAO.getStausOfDataSourceRequest(groupId, dataSourceInstance.getDataSourceId());
  	if(groupId == dataSourceInstance.getGroupId()) {
  		dataRequestStatus = DataRequestEnum.ACCEPT;
  	}
  	if(dataRequestStatus == DataRequestEnum.ACCEPT) {
  		MyCollection myCollection = MyCollectionBSO.getMyCollectionDetailsFromDataSourceId(dataSourceInstance.getDataSourceId(),
  				Long.parseLong(userId));
  		if(null != myCollection && myCollection.getStatus() == 1) {
  			dataRequestStatus = DataRequestEnum.REMOVE;
  		}
  	}
  	return dataRequestStatus;
  }*/

  //required for elastic search
  private static List<GlobalSearchWithObjectInstance.Item> getStatusListFromDataSourceInstanceList(
      String userId, List<DataSourceInstance> dsInstances, Long groupId, List<Record> records)
      throws SystemException, Exception {
    List<GlobalSearchWithObjectInstance.Item> statusList =
        new ArrayList<GlobalSearchWithObjectInstance.Item>();
    if (null != dsInstances && dsInstances.size() > 0) {
      int i = 0;
      while (i < dsInstances.size()) {
        DataSourceInstance dataSourceInstance = dsInstances.get(i);
        Record r = records.get(i);
        DataRequestEnum dataRequestStatus =
            getStatusFromRequestsAndMyCollection(userId, groupId, dataSourceInstance);
        GlobalSearchWithObjectInstance gs = new GlobalSearchWithObjectInstance();
        statusList.add(
            gs.new Item(String.valueOf(dataRequestStatus), r.getCategory(), r.getSubcategory()));
        i++;
      }
    }
    return statusList;
  }

  private static String createQueryFromListOfRecords(List<Record> records) {
    String query =
        "select dsi.ownerProjectId , dsi.fieldMappingVersionId, dsi.dataSourceVersionId, dsi.ingection, dsi.dataSourceName,"
            + " dsi.appId, dsi.createdDate,dsi.updatedDate,dsi.flowType,"
            + " dsi.fileType, dsi.compressionType, dsi.dataSourceStatus, dsi.category, dsi.subCategory, dsi.totalRecords,"
            + " dsi.size, dsi.dataSourceId, dsi.filePaths, dsi.latestFilepath, dsi.description, dsi.oldFilePath,"
            + " dsi.dataRepoId, dsi.dataRepoName, dsi.dataSourceType, dsi.groupId, dsi.fileTypeIndicator, dsi.createdBy,"
            + " dsi.lastRunBy, dsi.fileStoreObject, fb.avgRating,  fb.avgRating from (select dataSourceId, round(IFNULL(avgRating, 0)) as"
            + " avgRating from (select dataSourceId, avg(rating) avgRating from feedback where rating <> 0 and rating is not null"
            + " and rating <> 'null' group by dataSourceId) as fbk)"
            + " as fb"
            + " right outer join"
            + " datasourceinstance dsi on dsi.dataSourceId = fb.dataSourceId where"
            + " dsi.dataSourceId in (";
    int cnt = 0;
    for (Record record : records) {
      if (cnt == 0) {
        query = query + "'" + record.getId() + "'";
        cnt++;
      } else {
        query = query + "," + "'" + record.getId() + "'";
      }
    }
    query = query + ")";
    return query;
  }

  public static List<Long> getObjectIdsSharedWithGroupWithAccessType(
      String objectType, Long groupId, String accessType) throws SystemException {

    List<Long> objectIds = new ArrayList<Long>();
    List<GroupObjectMapping> objectsSharedWithGroupWithAccessType =
        ObjectRequestDAO.getObjectsSharedWithGroupWithAccessType(objectType, groupId, accessType);
    if (null != objectsSharedWithGroupWithAccessType) {
      for (GroupObjectMapping groupObjectMapping : objectsSharedWithGroupWithAccessType) {
        objectIds.add(groupObjectMapping.getObjectId());
      }
    }
    return objectIds;
  }

  public static List<Long> getObjectIdsSharedWithUserWithAccessType(
      String objectType, Long userId, String accessType) throws SystemException {

    List<Long> objectIds = new ArrayList<Long>();
    List<UserObjectMapping> objectsSharedWithUserWithAccessType =
        ObjectRequestDAO.getObjectsSharedWithUserWithAccessType(objectType, userId, accessType);
    if (null != objectsSharedWithUserWithAccessType) {
      for (UserObjectMapping userObjectMapping : objectsSharedWithUserWithAccessType) {
        objectIds.add(userObjectMapping.getObjectId());
      }
    }
    return objectIds;
  }
  //When multiple Request to object is accepted or rejected

  public static void acceptMultiObjectRequest(List<Long> requestIdList) throws Exception {
    if (requestIdList == null) {
      return;
    }
    for (Long requestId : requestIdList) {
      if (requestId != null) {
        acceptObjectRequest(requestId, null);
      }
    }
  }

  public static void rejectMultiObjectRequest(Map<Long, String> requestIdAndJustification)
      throws SystemException {
    if (requestIdAndJustification == null || requestIdAndJustification.isEmpty()) {
      return;
    }
    Set<Entry<Long, String>> set = requestIdAndJustification.entrySet();
    for (Entry<Long, String> entry : set) {
      ObjectRequestDAO.rejectObjectRequest(entry.getKey(), entry.getValue());
    }
  }

  public static DataRequest getDataRequestById(Long requestId) throws Exception {
    DataRequest dataRequest = ObjectRequestDAO.getDataRequestById(requestId);
    return dataRequest;
  }

  /**
   * @param datarequestFromUi
   * @param userId
   * @throws Exception
   */
  public static void createObjectRequest(DataRequestDTO dataRequestFromUi, Long userId)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> createObjectRequest {} {}", dataRequestFromUi, userId);

    ObjectRequestDAO.insertObjectRequestInDb(dataRequestFromUi, userId);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << createObjectRequest {} {}", dataRequestFromUi, userId);
  }

  /**
   * @param userId
   * @param ObjectType
   * @param actionId
   * @param notificationType
   * @throws Exception
   */
  public static void setNotification(
      Long userId, ObjectTypes ObjectType, Long actionId, DataRequestEnum notificationType)
      throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> setNotification" + ParamUtils.getString(userId));

    Notification notification = new Notification();

    notification.setUserId(userId);
    notification.setObjectType(0); // only zero added to represent data sources as of now
    notification.setActionId(actionId);
    notification.setNotificationType(notificationType.getValue());

    ObjectRequestDAO.setNotification(notification);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << setNotification" + ParamUtils.getString(userId));
  }

  /**
   * @param userId
   * @throws SystemException
   */
  public static void deleteNotification(Long userId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> deleteNotification" + ParamUtils.getString(userId));

    ObjectRequestDAO.deleteNotification(userId);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << deleteNotification" + ParamUtils.getString(userId));
  }
}

package com.augmentiq.maxiq.services.presto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.model.connector.datanode.DataNode;
import com.augmentiq.maxiq.repository.presto.PrestoDao;

@Service
public class PrestoServices {
    
    @Autowired
    private PrestoDao prestoDao;
    
   public DataNode getPublishedDataSource() throws SystemException{
       return prestoDao.getPublishedDataSource();
   }

}

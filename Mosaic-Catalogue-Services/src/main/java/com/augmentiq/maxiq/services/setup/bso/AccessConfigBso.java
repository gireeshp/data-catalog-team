package com.augmentiq.maxiq.services.setup.bso;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;

import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.RoleNameAlreadyExistsException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.core.dao.configuration.setupdao.AccessConfigDao;
import com.augmentiq.maxiq.entity.model.setup.domains.ActionsMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.RoleActionMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.UserRoleMaster;
import com.augmentiq.maxiq.model.setup.domains.RoleActions;
import com.augmentiq.maxiq.util.session.registry.SessionRegistry;

public class AccessConfigBso {
  // FETCHING AVAILABLE ACCESSES DATA -req
  public static List<Map<String, Object>> fetchAccessData() throws Exception {
    List<Map<String, Object>> accessMap = AccessConfigDao.fetchAvailableAccessData();
    for (Map<String, Object> map : accessMap) {
      map.put("updatedTs", UserManagementBso.getLongDate(map.get("updatedTs") + ""));
      map.put("createdTs", UserManagementBso.getLongDate(map.get("createdTs") + ""));
    }
    return accessMap;
  }

  // FETCHING AVAILABLE ACCESSES DATA

  public static List<ActionsMaster> fetchAvailableActions() throws SystemException {
    return AccessConfigDao.fetchAvailableActions();
  }

  //FETCHING ALL APPLICATION LIST-req
  public static List<Map<String, Object>> fetchAllApps() throws SystemException {
    return AccessConfigDao.fetchAllApps();
  }

  // CREATE ROLE ACTIONS -req
  public static void createRole(RoleActions roleActions, String email, Integer appId)
      throws SystemException, RoleNameAlreadyExistsException {
    UserRoleMaster roleObject = roleActions.getRoleObject();
    checkIfAccessNameAlreadyExists(roleObject, appId);
    List<ActionsMaster> actionsObject = roleActions.getActionsObject();
    roleObject.setCreatedBy(email);
    roleObject.setCreatedTs(String.valueOf(DateUtils.getCurrentTimestamp()));
    roleObject.setAppId(appId);
    roleObject.setUpdatedTs("0000-00-00 00:00:00");

    String roleMasterId = AccessConfigDao.saveRoleObject(roleObject);

    List<RoleActionMappings> buildRoleActionMappingsObject =
        buildRoleActionMappingsObject(roleMasterId, actionsObject, email);
    AccessConfigDao.saveRoleActionObject(buildRoleActionMappingsObject);
  }

  // UPDATE ROLE ACTIONS-req

  /**
   * This method is to update the role MAX-1848 : In that we want string parameter, the value passed
   * is Integer.
   *
   * @author Mayuri
   * @param roleActions
   * @param email
   * @param appId
   * @throws Exception
   */
  public static void updateRole(RoleActions roleActions, String email, Integer appId)
      throws Exception {
    UserRoleMaster roleObject = roleActions.getRoleObject();
    checkIfAccessNameAlreadyExists(roleObject, appId);
    List<ActionsMaster> actionsObject = roleActions.getActionsObject();
    roleObject.setUpdatedBy(email);
    roleObject.setUpdatedTs(String.valueOf(DateUtils.getCurrentTimestamp()));
    roleObject.setCreatedTs(new Timestamp(Long.parseLong(roleObject.getCreatedTs())).toString());
    String roleMasterId = String.valueOf(roleObject.getUnqRoleMasterId());
    Map<String, Object> updateRolemaster = new LinkedHashMap<String, Object>();
    updateRolemaster.put("unqRoleMasterId", roleMasterId);

    maintainRoleHistory(roleMasterId, "UPDATE", email);

    AccessConfigDao.updateRoleObject(roleObject, updateRolemaster);
    Map<String, Object> deleteRoleActions = new LinkedHashMap<String, Object>();

    deleteRoleActions.put("roleMasterId", roleMasterId);

    AccessConfigDao.deleteRoleActionObject(deleteRoleActions);
    List<RoleActionMappings> buildRoleActionMappingsObject =
        buildRoleActionMappingsObject(roleMasterId, actionsObject, email);

    AccessConfigDao.saveRoleActionObject(buildRoleActionMappingsObject);
    removeFromRoles(Long.parseLong(roleMasterId));
  }

  public static void removeFromRoles(Long roleMasterId) throws SystemException {
    List<Map<String, String>> emailIds = fetchEmailIdsFromRoleId(roleMasterId);
    if (null != emailIds) {
      for (Map<String, String> emailMap : emailIds) {
        SessionRegistry.roles.remove(emailMap.get("emailId"));
      }
    }
  }

  public static List<Map<String, String>> fetchEmailIdsFromRoleId(Long roleMasterId)
      throws SystemException {
    String query =
        "select a.emailId from user_role_mappings b join application_user a on b.userId = a.unqUserId where b.roleId = ?";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("roleMasterId", roleMasterId);
    List<? extends Object> queryResult =
        MySqlOperations.executeQueryForResultSetPrepStatement(query, null, objectMap);

    //List<? extends Object> queryResult = AccessConfigDao.getQueryResult(query);
    return (List<Map<String, String>>) queryResult;
  }

  public static List<RoleActionMappings> buildRoleActionMappingsObject(
      String roleMasterId, List<ActionsMaster> actionsObject, String email) {
    List<RoleActionMappings> roleActionMappingsList = new LinkedList<RoleActionMappings>();

    String currentTimestamp = String.valueOf(DateUtils.getCurrentTimestamp());
    for (ActionsMaster actionsMaster : actionsObject) {
      RoleActionMappings roleActionMappings = new RoleActionMappings();
      roleActionMappings.setRoleMasterId(roleMasterId);
      roleActionMappings.setActionId(actionsMaster.getActionsMasterId());
      roleActionMappings.setInsertTs(currentTimestamp);
      roleActionMappings.setInsertedBy(email);
      roleActionMappings.setUpdatedTs("0000-00-00 00:00:00");
      roleActionMappingsList.add(roleActionMappings);
    }
    return roleActionMappingsList;
  };

  // CHECK IF ACCESS NAME ALREADY EXISTS
  public static void checkIfAccessNameAlreadyExists(UserRoleMaster userRoleMaster, Integer appId)
      throws SystemException, RoleNameAlreadyExistsException {
    String query = "";
    String accessName = userRoleMaster.getRoleName();
    String unqRoleMasterId = String.valueOf(userRoleMaster.getUnqRoleMasterId());
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    if (StringUtils.isEmpty(unqRoleMasterId)
        || StringUtils.equalsIgnoreCase(unqRoleMasterId, "null")) {
      query = "select count(*) as count from role_master where roleName= ? " + "and appId= ?";

      objectMap.put("accessName", accessName);
      objectMap.put("appId", appId);

    } else {
      query =
          "select count(*) as count from role_master where roleName= ? and unqRoleMasterId!= ? and appId= ?";

      //Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
      objectMap.put("accessName", accessName);
      objectMap.put("unqRoleMasterId", unqRoleMasterId);
      objectMap.put("appId", appId);
    }
    List<? extends Object> queryResult =
        MySqlOperations.executeQueryForResultSetPrepStatement(query, null, objectMap);
    // = AccessConfigDao.getQueryResult(query);

    // System.out.println("The result is:"+queryResult);

    if (queryResult != null && queryResult.size() > 0) {

      @SuppressWarnings("unchecked")
      Map<String, String> rs = (Map<String, String>) queryResult.get(0);

      if (Long.parseLong(rs.get("count")) > 0) {
        throw new RoleNameAlreadyExistsException(QueryConstants.DbExceptions.ROLE_ALREADY_EXIST);
      }
    }
  };

  // DELETE ROLE ACTIONS-req

  public static void deleteRole(String accessId, String email) throws Exception {
    Map<String, Object> rolemaster = new LinkedHashMap<String, Object>();
    rolemaster.put("unqRoleMasterId", accessId);
    maintainRoleHistory(accessId, "DELETE", email);
    AccessConfigDao.deleteAccessDetails(new UserRoleMaster(), rolemaster);
    Map<String, Object> roleActionsmapping = new HashMap<String, Object>();
    roleActionsmapping.put("roleMasterId", accessId);
    AccessConfigDao.deleteAccessDetails(new RoleActionMappings(), roleActionsmapping);
  }

  public static void maintainRoleHistory(String roleMasterId, String action, String email)
      throws Exception {
    String insertIntoHistory =
        "insert into roleActionHistory(his_unqRoleMasterId, his_roleName, his_roleDescription, his_createdBy, his_createdTs, his_updatedBy, his_updatedTs, his_roleActionIdList, his_actionIdList, action, insertBy, insertTs) select rm.unqRoleMasterId, rm.roleName, rm.roleDescription, rm.createdBy, rm.createdTs, rm.updatedBy, rm.updatedTs, group_concat(distinct ' ', ram.roleActionId) as roleActionIdList,group_concat(distinct ' ', ram.actionId) as actionIdList,'"
            + action
            + "','"
            + email
            + "',now() from role_master rm  left outer join role_action_mappings ram  on ram.roleMasterId=rm.unqRoleMasterId where rm.unqRoleMasterId='"
            + roleMasterId
            + "'";

    AccessConfigDao.maintainRoleHistory(insertIntoHistory);
  }

  public static Map<String, String> readCookieValue(String[] cookieNames, Cookie[] cookies) {

    Map<String, String> userCookie = new HashMap<String, String>();

    if (cookies != null) {

      for (Cookie cookie : cookies) {

        for (String cookieName : cookieNames) {

          if (cookie.getName().equals(cookieName)) {

            userCookie.put(cookie.getName(), cookie.getValue());
          }
        }
      }
    }

    return userCookie;
  }

  
  //req
  public static List<? extends Object> getGroupSubgroupRoleDetails(String emailId, Long appId)
      throws SystemException {

    String getGroupSubgroupRoleDetails =
        "select au.unqUserId, GROUP_CONCAT(DISTINCT ugm.groupId) AS groupIdList,"
            + " GROUP_CONCAT(DISTINCT ug.groupName) AS groupNameList, GROUP_CONCAT(DISTINCT usgm.subGroupId) AS subGroupIdList,"
            + " GROUP_CONCAT(DISTINCT usg.subGroupName) AS subGroupNameList, GROUP_CONCAT(DISTINCT urm.roleId) AS roleIdList,"
            + " GROUP_CONCAT(DISTINCT rm.roleName) AS roleNameList, GROUP_CONCAT(DISTINCT ram.actionId) AS actionIdList,"
            + " GROUP_CONCAT(DISTINCT am.actionCode) AS actionCodeList"
            + " from application_user au JOIN user_group_mappings ugm on au.unqUserId = ugm.userId"
            + " 	JOIN usergroup ug on ugm.groupId=ug.groupId"
            + "		JOIN user_subgroup_mappings usgm on  au.unqUserId = usgm.userId"
            + "		JOIN user_subgroup usg on usgm.subGroupId=usg.subGroupId"
            + "		JOIN user_role_mappings urm on  au.unqUserId = urm.userId"
            + "		JOIN role_master rm on urm.roleId=rm.unqRoleMasterId"
            + "		JOIN role_action_mappings ram on  urm.roleId = ram.roleMasterId"
            + "		JOIN actions_master am on ram.actionId=am.actionsMasterId"
            + " 	where au.emailId= ?"
            + "		AND ug.appId = ? AND rm.appId = ?";

    //List<? extends Object> queryResult = AccessConfigDao.getQueryResult(getGroupSubgroupRoleDetails);

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("emailId", emailId);
    objectMap.put("appId", appId);
    objectMap.put("appId1", appId);

    List<? extends Object> queryResult =
        MySqlOperations.executeQueryForResultSetPrepStatement(
            getGroupSubgroupRoleDetails, null, objectMap);

    // System.out.println("The query result is:"+queryResult);

    return queryResult;
  }

  @SuppressWarnings("unchecked")
  /**
   * @param groupId
   * @return
   * @throws SystemException
   */
  
  //req
  public static String getQueueNameByGroupId(Long groupId) throws SystemException {
    String query =
        "SELECT qm.queueName FROM user_subgroup  ug "
            + " JOIN queue_master qm ON qm.queueMasterId= ug.queueMasterID WHERE ug.usergroupId =?";

    List<? extends Object> queueName = null;

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("groupId", groupId);

    try {
      queueName = MySqlOperations.executeQueryForResultSetPrepStatement(query, null, objectMap);
      //queueName = AccessConfigDao.getQueryResult(query);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    if (queueName != null && queueName.size() > 0) {
      Map<String, String> mappedQueueName = (Map<String, String>) queueName.get(0);
      return mappedQueueName.get("queueName").toString();
    }
    return null;
  }

  //req
  public static List<Map<String, Object>> fetchAllMasterRolesBasedDetails(Long unqUserId)
      throws SystemException {
    // fetch all master roles based Details
    /*String query = "SELECT lastUsedPersonaId, actionCode FROM (SELECT case when user.lastUsedPersonaId = 0 or user.lastUsedPersonaId is null then persona.persona_id"
    + " else user.lastUsedPersonaId end AS lastUsedPersonaId FROM application_user user LEFT OUTER JOIN (SELECT MAX(persona_id) persona_id,"
    + " up.unqUserId FROM user_persona up WHERE unqUserId = "+ unqUserId + " ) persona ON (user.unqUserId = persona.unqUserId) "
    + "WHERE user.unqUserId = " + unqUserId + " ) A LEFT OUTER join persona_action_mapping B ON (A.lastUsedPersonaId = B.persona_id) "
    + "JOIN actions_master C ON (B.actionsMasterId = C.actionsMasterId);";*/

    String query =
        "SELECT lastUsedPersonaId, actionCode FROM (SELECT case when user.lastUsedPersonaId = 0 or user.lastUsedPersonaId is null then persona.persona_id"
            + " else user.lastUsedPersonaId end AS lastUsedPersonaId FROM application_user user LEFT OUTER JOIN (SELECT MAX(persona_id) persona_id,"
            + " up.unqUserId FROM user_persona up WHERE unqUserId = ? ) persona ON (user.unqUserId = persona.unqUserId) "
            + "WHERE user.unqUserId = ? ) A LEFT OUTER join persona_action_mapping B ON (A.lastUsedPersonaId = B.persona_id) "
            + "JOIN actions_master C ON (B.actionsMasterId = C.actionsMasterId);";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("unqUserId", unqUserId);
    objectMap.put("unqUserId1", unqUserId);

    List<Map<String, Object>> masterBasedRoles =
        MySqlOperations.executeQueryForResultSetPrepStatement(query, null, objectMap);
    return masterBasedRoles;
  }

  //req
  public static List<Map<String, Object>> fetchAllMasterRolesUsingNameDetails(String masterRoleName)
      throws SystemException {
    // fetch all master roles based Details
    String query =
        "select actionCode from persona_master a LEFT OUTER JOIN persona_action_mapping b "
            + "on (a.persona_id = b.persona_id) LEFT OUTER JOIN actions_master c on(b.actionsMasterId = c.actionsMasterId) "
            + "where a.persona_name = ?";

    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("masterRoleName", masterRoleName);

    List<Map<String, Object>> masterBasedRoles =
        MySqlOperations.executeQueryForResultSetPrepStatement(query, null, objectMap);
    return masterBasedRoles;
  }

  
  //req
  public static void updateLastUpdatePersonaId(String masterRoleName, Long unqUserId)
      throws SystemException {
    String query =
        "update application_user set lastUsedPersonaId = (select persona_id from persona_master "
            + " where persona_name = ?) where unqUserId=?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("persona_name", masterRoleName);
    objectMap.put("unqUserId", unqUserId);
    MySqlOperations.updateQuery(query, objectMap);
  }

  
  //req
  public static PersonaMaster fetchLastUpdatePersonaName(Long masterRoleId) throws SystemException {
    String query = "select * from persona_master where persona_id = " + masterRoleId;
    PersonaMaster fetchPersonaNameByPersonaId =
        MySqlOperations.scanOneForSqlQuery(PersonaMaster.class, query);
    return fetchPersonaNameByPersonaId;
  }

  //req
  public static PersonaMaster fetchMaxFromPersonaId() throws SystemException {
    String query = "select * from persona_master order by persona_id desc limit 1";
    PersonaMaster fetchPersonaNameByPersonaId =
        MySqlOperations.scanOneForSqlQuery(PersonaMaster.class, query);
    return fetchPersonaNameByPersonaId;
  }
}

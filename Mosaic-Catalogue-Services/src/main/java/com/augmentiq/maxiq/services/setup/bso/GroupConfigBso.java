package com.augmentiq.maxiq.services.setup.bso;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.RandomUtility;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.GroupNameAlreadyExistsException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SubgroupNameAlreadyExistsException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.encryption.decription.userinfo.SecretService;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.core.dao.configuration.setupdao.GroupConfigDao;
import com.augmentiq.maxiq.entity.model.setup.domains.GroupLevelUser;
import com.augmentiq.maxiq.entity.model.setup.domains.QueuesMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.UserGroup;
import com.augmentiq.maxiq.entity.model.setup.domains.UserSubGroups;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class GroupConfigBso {
  private static final Logger logger = LoggerFactory.getLogger(UserManagementBso.class);

  // FETCHING AVAILABLE GROUPS-DATA -req
  public static List<Map<String, Object>> fetchGroups() throws Exception {
    List<Map<String, Object>> groups = GroupConfigDao.fetchGroups();
    for (Map<String, Object> groupData : groups) {
      groupData.put("updateTs", UserManagementBso.getLongDate(groupData.get("updateTs") + ""));
      groupData.put("insertTs", UserManagementBso.getLongDate(groupData.get("insertTs") + ""));
    }
    return groups;
  }

  // FETCHING AVAILABLE SUB-GROUPS-DATA -req
  public static List<Map<String, Object>> fetchSubGroups() throws Exception {
    List<Map<String, Object>> subgroups = GroupConfigDao.fetchSubGroups();
    for (Map<String, Object> groupData : subgroups) {
      groupData.put("updateTs", UserManagementBso.getLongDate(groupData.get("updateTs") + ""));
      groupData.put("insertTs", UserManagementBso.getLongDate(groupData.get("insertTs") + ""));
    }
    return subgroups;
  }

  // FETCHING AVAILABLE QUEUES -req
  public static List<QueuesMaster> fetchAvailableQueues() throws SystemException {
    return GroupConfigDao.fetchAvailableQueues();
  }

  // FETCHING AVAILABLE GROUPS -req
  public static List<Map<String, Object>> fetchAvailableGroups() throws SystemException {
    return GroupConfigDao.fetchAvailableGroups();
  }

  // CREATE NEW SUB-GROUP req
  public static void createSubgroupAction(UserSubGroups subGroups) throws Exception {
    checkIfSubgroupNameAlreadyExists(subGroups);
    GroupConfigDao.createSubgroupAction(subGroups);
  }

  // UPDATE NEW SUB-GROUP-req
  public static void upadteSubgroupAction(UserSubGroups subGroups) throws Exception {
    checkIfSubgroupNameAlreadyExists(subGroups);
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    Long subGroupId = subGroups.getSubGroupId();
    query.put("subGroupId", subGroupId);
    maintainSubgroupHistory("UPDATE", String.valueOf(subGroupId), subGroups.getUpdatedBy());
    subGroups.setSubGroupId(null);
    GroupConfigDao.upadteSubgroupAction(subGroups, query);
  }

  // DELETE GROUP -req
  public static void deleteGroup(UserGroup userGroup, String email) throws Exception {
    Long groupId = userGroup.getGroupId();
    maintainGroupHistory("DELETE", String.valueOf(groupId), email);
    GroupConfigDao.deleteGroup(userGroup);
    String deleteSubGroup = "delete from user_subgroup where usergroupId=?";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("usergroupId", groupId);
    GroupConfigDao.executeQuery(deleteSubGroup, objectMap);
  }

  // DELETE SUB-GROUP -req
  public static void deleteSubGroup(UserSubGroups userSubGroup, String email) throws Exception {
    maintainSubgroupHistory("DELETE", String.valueOf(userSubGroup.getSubGroupId()), email);
    GroupConfigDao.deleteSubgroup(userSubGroup);
  }

  // CREATE NEW GROUP -req
  public static Long createGroupAction(UserGroup userGroup) throws Exception {

    checkIfGroupNameAlreadyExists(userGroup);

    // group creation logic
    Long groupId = null;

    try {

      String createGroupQuery =
          "insert into usergroup(appId,groupName,description,insertedBy) VALUES ( '"
              + userGroup.getAppId()
              + "', '"
              + userGroup.getGroupName()
              + "', '"
              + userGroup.getDescription()
              + "', '"
              + userGroup.getInsertedBy()
              + "')";
      groupId = GroupConfigDao.createGroupAction(createGroupQuery);

      

    } catch (Exception e) {
      logger.error("deleling entry for already insert group : ", e.getMessage());
      if (groupId != null && groupId > 0) {
        GroupConfigDao.deleteGroup(userGroup);
      }
      throw new Exception(e);
    }
    return groupId;
  }

  // UPDATE GROUP - req
  public static void updateGroupAction(UserGroup userGroup) throws Exception {
    checkIfGroupNameAlreadyExists(userGroup);
    maintainGroupHistory(
        "UPDATE", String.valueOf(userGroup.getGroupId()), userGroup.getUpdatedBy());

    
    String updateGroupQuery =
        "UPDATE usergroup SET groupName = '"
            + userGroup.getGroupName()
            + "', description = '"
            + userGroup.getDescription()
            + "', updatedBy='"
            + userGroup.getUpdatedBy()
            + "',updateTs=now() where groupId = '"
            + userGroup.getGroupId()
            + "'";
    GroupConfigDao.updateGroupAction(updateGroupQuery);
  }

  // MAINTAIN GROUP HISTORY
  private static void maintainGroupHistory(String action, String groupId, String actionUser)
      throws Exception {
    String historyInsertQuery =
        "insert into usergroup_history(his_groupId,his_groupName,his_description,his_queueMasterID,his_queueName,his_insertedBy,his_insertTs,his_updatedBy,his_updateTs,action,insertedBy,insertTs) select ug.groupId, ug.groupName, ug.description, ug.queueMasterID, qm.queueName, ug.insertedBy, ug.insertTs, ug.updatedBy,ug.updateTs,'"
            + action
            + "','"
            + actionUser
            + "',now() from usergroup ug left outer join queue_master qm on ug.queueMasterID=qm.queueMasterId where ug.groupId='"
            + groupId
            + "'";
    GroupConfigDao.maintainGroupHistory(historyInsertQuery);
  }

  // MAINTAIN SUB-GROUP HISTORY
  private static void maintainSubgroupHistory(String action, String subgroupId, String actionUser)
      throws Exception {
    String historyInsertQuery =
        "insert into user_subgroup_history(his_subGroupId,his_subGroupName,his_subGroupDescription,his_usergroupId,his_groupName,his_insertedBy,his_insertTs,his_updatedBy,his_updateTs,action,insertedBy,insertTs) select usg.subGroupId, usg.subGroupName, usg.subGroupDescription, usg.usergroupId, ug.groupName, usg.insertedBy, usg.insertTs, usg.updatedBy,usg.updateTs, '"
            + action
            + "','"
            + actionUser
            + "',now() from user_subgroup usg left outer join usergroup ug on ug.groupId=usg.usergroupId where usg.subGroupId='"
            + subgroupId
            + "'";
    GroupConfigDao.maintainGroupHistory(historyInsertQuery);
  }

  // CHECK WEATHER SUBGROUP NAME IS ALREADY EXISTS
  private static void checkIfSubgroupNameAlreadyExists(UserSubGroups subGroups) throws Exception {

    String checkQuery = "";
    String subgroupId = subGroups.getSubGroupId() + "";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    if (StringUtils.isEmpty(subgroupId) || StringUtils.equalsIgnoreCase(subgroupId, "null")) {
      checkQuery = "select * from user_subgroup where subGroupName=? and usergroupId=?";

      objectMap.put("subGroupName", subGroups.getSubGroupName());
      objectMap.put("usergroupId", subGroups.getUsergroupId());

    } else {
      checkQuery =
          "select * from user_subgroup where subGroupName=? and usergroupId=? and subGroupId!=?";

      objectMap.put("subGroupName", subGroups.getSubGroupName());
      objectMap.put("usergroupId", subGroups.getUsergroupId());
      objectMap.put("subGroupId", subgroupId);
    }

    List<Map<String, Object>> scanResult =
        MySqlOperations.executeQueryForResultSetPrepStatement(checkQuery, null, objectMap);
    //List<Map<String, Object>> scanResult = GroupConfigDao.getQueryResult(checkQuery);

    if (scanResult != null) {
      if (scanResult.size() > 0) {
        throw new SubgroupNameAlreadyExistsException(
            QueryConstants.DbExceptions.SUBGROUP_ALREADY_EXIST);
      }
    }
  }

  // CHECK WEATHER GROUP NAME IS ALREADY EXISTS
  private static void checkIfGroupNameAlreadyExists(UserGroup group) throws Exception {

    String checkQuery = "";
    String groupId = group.getGroupId() + "";
    String appId = group.getAppId();
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    if (StringUtils.isEmpty(groupId) || StringUtils.equalsIgnoreCase(groupId, "null")) {

      checkQuery = "select * from usergroup where groupName=? and appId=?";
      objectMap.put("groupName", group.getGroupName());
      objectMap.put("appId", appId);

    } else {

      checkQuery = "select * from usergroup where groupName=? and groupId!=? and appId=?";
      objectMap.put("groupName", group.getGroupName());
      objectMap.put("groupId", groupId);
      objectMap.put("appId", appId);
    }

    List<Map<String, Object>> checkIfGroupNameAlreadyExists =
        MySqlOperations.executeQueryForResultSetPrepStatement(checkQuery, null, objectMap);
    //List<Map<String, Object>> checkIfGroupNameAlreadyExists = GroupConfigDao.getQueryResult(checkQuery);

    if (checkIfGroupNameAlreadyExists != null && checkIfGroupNameAlreadyExists.size() > 0) {
      throw new GroupNameAlreadyExistsException(QueryConstants.DbExceptions.GROUP_ALREADY_EXIST);
    }
  }

  /**
   * @param userGroup
   * @return
   * @throws Exception
   */
  public static ApplicationUser creatingGroupLevelUserInMosaic(UserGroup userGroup)
      throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> creatingGroupLevelUserInMosaic() for group"
            + userGroup);
    SecretService secretService;
    try {

      checkGroupLevelUserIsAlreadyExist(userGroup.getGroupId());
      secretService = new SecretService();
      String groupPassowrd = RandomUtility.getRandomString(10);
      ApplicationUser applicationUser = new ApplicationUser();
      applicationUser.setUnqUserId(userGroup.getGroupId());
      applicationUser.setFirstName(userGroup.getGroupName());
      applicationUser.setLastName(userGroup.getGroupName());
      applicationUser.setPassword(groupPassowrd);
      applicationUser.setEmailId(userGroup.getGroupName());

      GroupLevelUser groupLevelUser = new GroupLevelUser();
      groupLevelUser.setGroupId(userGroup.getGroupId());
      groupLevelUser.setCreateDate(new Date());
      groupLevelUser.setPolicyStatus(false);
      groupLevelUser.setGroupUserPassword(secretService.encrypt(groupPassowrd));
      GroupConfigDao.createGroupLevelUser(groupLevelUser);

      logger.info("created group Level user for group " + userGroup.getGroupName());
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << creatingGroupLevelUserInMosaic() for group"
              + userGroup);

      return applicationUser;
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      logger.error("problem on creating group level user", e);
      throw new Exception(e.getMessage());
    }
  }

  /**
   * @param groupId
   * @return
   * @throws Exception
   */
  public static String fetchGroupLevelUserPassword(Object groupId) throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> fetchGroupLevelUserPassword() for group" + groupId);

    try {
      GroupLevelUser fetchGroupLevelUserDetails =
          GroupConfigDao.fetchGroupLevelUserDetails(groupId);
      SecretService secretService = new SecretService();
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " << fetchGroupLevelUserPassword() for group" + groupId);
      return secretService.decrypt(fetchGroupLevelUserDetails.getGroupUserPassword());
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      logger.error("problem on generating group level user password", e);
      throw new Exception(e.getMessage());
    }
  }

  /**
   * @return
   * @throws SystemException
   */
  public static List<GroupLevelUser> fetchAllUnAssignedPolicyUsers() throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchAllUnAssignedPolicyUsers() for group");
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(QueryConstants.GroupLevelUser.POLICY_STATUS, "0");
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << fetchAllUnAssignedPolicyUsers() for group" + query);
    return GroupConfigDao.fetchGroupLevelUsersByQuery(query);
  }

  /**
   * @param groupId
   * @throws Exception
   */
  private static void checkGroupLevelUserIsAlreadyExist(Long groupId) throws Exception {

    String checkQuery = "";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    if (null != groupId) {
      checkQuery = "select * from group_level_user where groupId=?";
      objectMap.put("groupId", groupId);
      List<Map<String, Object>> checkIfGroupNameAlreadyExists =
          MySqlOperations.executeQueryForResultSetPrepStatement(checkQuery, null, objectMap);
      //List<Map<String, Object>> checkIfGroupNameAlreadyExists = GroupConfigDao.getQueryResult(checkQuery);
      if (checkIfGroupNameAlreadyExists != null && checkIfGroupNameAlreadyExists.size() > 0) {
        throw new GroupNameAlreadyExistsException(QueryConstants.DbExceptions.GROUP_ALREADY_EXIST);
      }
    }
  }
}

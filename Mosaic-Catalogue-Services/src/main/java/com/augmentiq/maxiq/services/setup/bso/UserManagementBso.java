package com.augmentiq.maxiq.services.setup.bso;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.constant.maxiq.UserManagementConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.EntityNameAlreadyExistsException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.base.encryption.decription.userinfo.SecretService;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.UserStatus;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.core.dao.configuration.setupdao.UserManagementDao;
import com.augmentiq.maxiq.core.dao.userdao.ApplicationUserDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserFeedback;
import com.augmentiq.maxiq.entity.model.setup.domains.PersonaMaster;
import com.augmentiq.maxiq.entity.model.setup.domains.UserGroupMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.UserRoleMappings;
import com.augmentiq.maxiq.entity.model.setup.domains.UserSubGroupMappings;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.model.setup.domains.ManageUser;
import com.augmentiq.maxiq.model.setup.domains.Solution;

import scala.reflect.internal.Trees.This;

/** @see Ajinkya Marathe changed for MAX-626 updateUser(,,) */
public class UserManagementBso {

  private static final Logger logger = LoggerFactory.getLogger(UserManagementBso.class);

  public static List<Map<String, Object>> getUserDetails() throws Exception {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getUserDetails()" + ParamUtils.getString(""));

    List<Map<String, Object>> userDetails = UserManagementDao.getUserDetails();
    for (Map<String, Object> userDetail : userDetails) {
      userDetail.put(
          UserManagementConstants.PASSWORD,
          String.valueOf(userDetail.get(UserManagementConstants.PASSWORD)));
      userDetail.put(
          UserManagementConstants.UPDATED_TS,
          UserManagementBso.getLongDate(userDetail.get(UserManagementConstants.UPDATED_TS) + ""));
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getUserDetails()" + ParamUtils.getString(userDetails));
    return userDetails;
  }

  public static List<Map<String, Object>> fetchGroupSubgroupRoles() throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> fetchGroupSubgroupRoles()" + ParamUtils.getString(""));

    List<Map<String, Object>> solutions = new LinkedList<Map<String, Object>>();

    List<Map<String, Object>> allSolutions = UserManagementDao.getAllSolutions();
    Integer SolutionId;
    String SolutionName;
    for (int i = 0; i < allSolutions.size(); i++) {
      // The Map is declared inside because the map was not getting added
      // in the list
      Map<String, Object> groupSubgroupsRoles = new HashMap<String, Object>();

      SolutionId = Integer.parseInt(allSolutions.get(i).get("appId") + "");
      SolutionName = (String) allSolutions.get(i).get("appName");
      String query = QueryConstants.UserManageMent.FETCH_AVAILABLE_GROUPS + SolutionId;
      groupSubgroupsRoles.put("GROUP", UserManagementDao.executeQueryForResultSet(query));
      query =
          QueryConstants.UserManageMent.FETCH_AVAILABLE_SUBGROUPS
              + SolutionId
              + " group by usg.subGroupId";
      groupSubgroupsRoles.put("SUBGROUP", UserManagementDao.executeQueryForResultSet(query));
      query = QueryConstants.UserManageMent.FETCH_AVAILABLE_ROLES + SolutionId;
      groupSubgroupsRoles.put("ROLES", UserManagementDao.executeQueryForResultSet(query));
      groupSubgroupsRoles.put("SOLUTIONID", SolutionId);
      groupSubgroupsRoles.put("SOLUTIONNAME", SolutionName);

      solutions.add(groupSubgroupsRoles);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << fetchGroupSubgroupRoles()"
              + ParamUtils.getString(groupSubgroupsRoles));
    }
    return solutions;
  }

	public static Long getLongDate(String stringDate) throws ParseException {
		return UserManagementDao.getLongDate(stringDate);
	}

  // New Method to fetch solution details for specific user

  public static List<Map<String, Object>> fetchSolutionsForUser(String unqUserId)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> fetchSolutionsForUser()" + ParamUtils.getString(""));

    List<Map<String, Object>> solutions = new LinkedList<Map<String, Object>>();

    List<Map<String, Object>> selectedSolutions = UserManagementDao.getSelectedSolutions(unqUserId);
    Integer SolutionId;
    String SolutionName;
    for (int i = 0; i < selectedSolutions.size(); i++) {
      // The Map is declared inside because the map was not getting added
      // in the list
      Map<String, Object> groupSubgroupsRoles = new HashMap<String, Object>();

      SolutionId = Integer.parseInt(selectedSolutions.get(i).get("solutionId") + "");
      SolutionName = (String) selectedSolutions.get(i).get("solutionName");
      String query =
          QueryConstants.UserManageMent.FETCH_SELECTED_GROUPS
              + SolutionId
              + " and ugm.userId = "
              + unqUserId;
      groupSubgroupsRoles.put(
          QueryConstants.constants.GROUP, UserManagementDao.executeQueryForResultSet(query));
      query =
          QueryConstants.UserManageMent.FETCH_SELECTED_SUBGROUPS
              + SolutionId
              + " and usm.userId = "
              + unqUserId
              + " group by usg.subGroupId";
      groupSubgroupsRoles.put(
          QueryConstants.constants.SUBGROUP, UserManagementDao.executeQueryForResultSet(query));
      query =
          QueryConstants.UserManageMent.FETCH_SELECTED_ROLES
              + SolutionId
              + " and urm.userId = "
              + unqUserId;
      groupSubgroupsRoles.put(
          QueryConstants.constants.ROLES, UserManagementDao.executeQueryForResultSet(query));
      query =
          QueryConstants.UserManageMent.GET_USER_PERSONA_MAPPING.replaceAll(
              QueryConstants.Parameters.SELECTED_USER_ID, unqUserId);
      groupSubgroupsRoles.put(
          QueryConstants.constants.PERSONA_DETAILS,
          UserManagementDao.executeQueryForResultSet(query));
      groupSubgroupsRoles.put(QueryConstants.constants.SOLUTION_ID, SolutionId);
      groupSubgroupsRoles.put(QueryConstants.constants.SOLUTIONNAME, SolutionName);
      solutions.add(groupSubgroupsRoles);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << fetchSolutionsForUser()"
            + ParamUtils.getString(solutions));
    return solutions;
  }

  public static void createUser(String createdByUserId, ManageUser manageUser, Long clientId)
      throws SystemException, EntityNameAlreadyExistsException {

    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> createUser()" + ParamUtils.getString(""));

    ApplicationUser userDetails = manageUser.getUserDetails();

    ApplicationUser fromDbApplicationUser =
        GenericStoreDao.getUserProfile(userDetails.getEmailId());

    if (null != fromDbApplicationUser) {
      if (StringUtils.equalsIgnoreCase(
          fromDbApplicationUser.getStatus(), UserStatus.DELETED.name())) {
        userDetails.setUnqUserId(fromDbApplicationUser.getUnqUserId());
        manageUser.setUserDetails(userDetails);
        updateUser(userDetails.getEmailId(), manageUser, "true", clientId);
        return;

      } else {
        logger.debug(
            LoggerConstants.LOG_MAXIQWEB
                + " << createUser()"
                + ParamUtils.getString(createdByUserId, manageUser));
        throw new EntityNameAlreadyExistsException(QueryConstants.DbExceptions.USER_ALREADY_EXISTS);
      }
    }

    // 1)CREATE USER for MaxIQ as well as Ranger

    userDetails.setLastUsedPersonaId(0l);
    String passwordForLdap = userDetails.getPassword();
    try {
      buildApplicationUserObject(userDetails, createdByUserId);
    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << createUser()"
              + ParamUtils.getString(createdByUserId, manageUser));
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.PASS_ENCRYPTION);
    }
    userDetails.setUserRangerId(0l);

    if (userDetails.getLastFailureAttemptTs() == null
        || StringUtils.equalsIgnoreCase(userDetails.getLastFailureAttemptTs(), "null")) {
      userDetails.setLastFailureAttemptTs("0000-00-00 00:00:00");
      userDetails.setLoginFailureCount(0L);
    }

    Long userId = UserManagementDao.createUser(userDetails, "unqUserId");
    UserManagementDao.allocateUserToClient(userId, createdByUserId);

    // For each Solution
    List<UserGroupMappings> builduserGroupMappingsObject = null;
    List<Solution> solutions = manageUser.getSolutions();
    for (Solution solution : solutions) {

      // 2)CREATE USER-GROUP MAPPING
      builduserGroupMappingsObject =
          builduserGroupMappingsObject(userId, createdByUserId, solution.getSelectedGroupsIds());

      // TO-DO update user groups
      UserManagementDao.createUserGroupMappings(builduserGroupMappingsObject, "unqUserGroupMapId");

      // 3)CREATE USER SUBGROUP MAPPING
      List<UserSubGroupMappings> builduserSubGroupMappingsObject =
          builduserSubGroupMappingsObject(
              userId, createdByUserId, solution.getSelectedSubgroupsIds());
      UserManagementDao.createUserSubGroupMappings(
          builduserSubGroupMappingsObject, "unqUserSubgrpMapId");

      // 4)CREATE USER ROLE MAPPING
      List<UserRoleMappings> buildUserRoleMappingsObject =
          buildUserRoleMappingsObject(userId, createdByUserId, solution.getSelectedRolesIds());
      UserManagementDao.createUserRoleMappings(buildUserRoleMappingsObject, "unqUserRoleMapId");

      // 5)CREATE USER APPLICATION MAPPING
      Long appId = solution.getAppId();
      UserManagementDao.createUserSolutionMapping(userId, appId);
    }

    List<PersonaMaster> personaMasters = manageUser.getPersonaMasters();
    if (null != personaMasters) {
      for (PersonaMaster personaMaster : personaMasters) {
        UserManagementDao.createUserPersonaMapping(userId, personaMaster.getPersona_id());
      }
    }


  }

 

  private static void buildApplicationUserObject(ApplicationUser userDetails, String email)
      throws Exception {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> buildApplicationUserObject()"
            + ParamUtils.getString(userDetails, email));

    userDetails.setGroupId(0l);
    userDetails.setGroupName(null);
    userDetails.setUserBrowsers(null);

    userDetails.setPassword(GenericStoreDao.encodedPassword(userDetails.getPassword()));

    userDetails.setPasswordexpirydays(
        Long.valueOf(Cache.getProperty(CacheConstants.PASS_EXP_DAYS)));
    userDetails.setCreatedTs(String.valueOf(DateUtils.getCurrentTimestamp()));
    userDetails.setCreatedBy(email);
    userDetails.setUpdatedTs(QueryConstants.DEFAULT_TIMESTAMP);
  }

  public static void updateUser(
      String email, ManageUser manageUser, String indicator, Long clientId)
      throws SystemException, EntityNameAlreadyExistsException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> updateUser()"
            + ParamUtils.getString(email, manageUser));

    ApplicationUser userDetails = manageUser.getUserDetails();
    Long unqUserId = userDetails.getUnqUserId();
    maintainUserHistory(unqUserId + "", email, "UPDATE");
    try {
      checkIfUserAlreadyExists(userDetails.getEmailId(), unqUserId + "");
    } catch (EntityNameAlreadyExistsException e) {
      // TODO Auto-generated catch block
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << updateUser()"
              + ParamUtils.getString(email, manageUser));
      e.printStackTrace();
      throw new EntityNameAlreadyExistsException(QueryConstants.DbExceptions.USER_ALREADY_EXISTS);
    }

    // Check firstName & lastName is changed or not
    List<Map<String, Object>> existNames = UserManagementDao.getUserFirstAndLastNameById(unqUserId);
    boolean isChanged = false;
    if (existNames != null && existNames.get(0).entrySet() != null) {
      for (Entry<String, Object> entry : existNames.get(0).entrySet()) {
        if (entry.getKey().equalsIgnoreCase("firstName")
            && entry.getValue() != null
            && !userDetails.getFirstName().equalsIgnoreCase(entry.getValue().toString())) {
          isChanged = true;
        }
        if (entry.getKey().equalsIgnoreCase("lastName")
            && entry.getValue() != null
            && !userDetails.getLastName().equalsIgnoreCase(entry.getValue().toString())) {
          isChanged = true;
        }
      }
    }
    // Updating all applications and datasources - isModified flag
    if (isChanged) updateAppsAndDatasourceOfUser(unqUserId);

    String passwordChange = userDetails.getPassword();
    userDetails.setUpdatedBy(email);
    if (!indicator.equalsIgnoreCase("true")) {
      try {
        userDetails.setPassword(GenericStoreDao.encodedPassword(userDetails.getPassword()));
        userDetails.setPasswordRefreshedDate("0000-00-00 00:00:00");
      } catch (Exception e) {
        logger.debug(
            LoggerConstants.LOG_MAXIQWEB
                + " << updateUser()"
                + ParamUtils.getString(email, manageUser));
        // TODO Auto-generated catch block
        e.printStackTrace();
        throw new SystemException(QueryConstants.DbExceptions.PASS_ENCRYPTION);
      }
    }

    userDetails.setUpdatedTs(DateUtils.getCurrentTimestamp() + "");
    // 1 UPDATE APPLICATION USER
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("unqUserId", unqUserId);

    UserManagementDao.update(userDetails, query);


    //Insert Client mapping if does not exist
    UserManagementDao.assignClientIdToUserIfNotAssigned(unqUserId, clientId);

    // 2 DELETE ALL USER MAPPINGS
    query.clear();
    query.put("userId", unqUserId);
    UserManagementDao.deleteRows(new UserGroupMappings(), query);
    UserManagementDao.deleteRows(new UserSubGroupMappings(), query);
    UserManagementDao.deleteRows(new UserRoleMappings(), query);
    UserManagementDao.deleteUserSolutionMappings(unqUserId);
    UserManagementDao.deleteUserPersonaMappings(unqUserId);

    // For all solutions
    List<UserGroupMappings> builduserGroupMappingsObject = null;
    List<Solution> solutions = manageUser.getSolutions();
    for (Solution solution : solutions) {

      // 3)RE-CREATE USER-GROUP MAPPING
      builduserGroupMappingsObject =
          builduserGroupMappingsObject(unqUserId, email, solution.getSelectedGroupsIds());
      UserManagementDao.createUserGroupMappings(builduserGroupMappingsObject, "unqUserGroupMapId");

      // 4)RE-CREATE USER SUBGROUP MAPPING
      List<UserSubGroupMappings> builduserSubGroupMappingsObject =
          builduserSubGroupMappingsObject(unqUserId, email, solution.getSelectedSubgroupsIds());
      UserManagementDao.createUserSubGroupMappings(
          builduserSubGroupMappingsObject, "unqUserSubgrpMapId");

      // 5)RE-CREATE USER ROLE MAPPING
      List<UserRoleMappings> buildUserRoleMappingsObject =
          buildUserRoleMappingsObject(unqUserId, email, solution.getSelectedRolesIds());
      UserManagementDao.createUserRoleMappings(buildUserRoleMappingsObject, "unqUserRoleMapId");

      // 6)RE-CREATE USER SOLUTION MAPPING
      Long appId = solution.getAppId();
      UserManagementDao.createUserSolutionMapping(unqUserId, appId);
    }

    List<PersonaMaster> personaMasters = manageUser.getPersonaMasters();
    for (PersonaMaster personaMaster : personaMasters) {
      UserManagementDao.createUserPersonaMapping(unqUserId, personaMaster.getPersona_id());
    }

    //If currently assigned personaIdList, contains last used persona id, then fine, else update the last used persona id.
    UserManagementDao.updateLastUsedPersona(unqUserId);

    
  }

  public static Object removeAttributeFromSession(HttpServletRequest httpRequest, String key) {
    HttpSession httpSession = httpRequest.getSession();
    Object value = null;
    if (httpSession != null) {
      httpSession.removeAttribute(key);
    }
    return value;
  }

  public static List<Long> fetchRoleIdsForUser(Long userId) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    List<Long> roleIds = new ArrayList<Long>();
    query.put("userId", userId);
    List<UserRoleMappings> scan = MySqlOperations.scan(UserRoleMappings.class, query);
    if (null != scan) {
      for (UserRoleMappings userRoleMappings : scan) {
        roleIds.add(userRoleMappings.getRoleId());
      }
    }
    return roleIds;
  }

  private static void checkIfUserAlreadyExists(String email, String userId)
      throws SystemException, EntityNameAlreadyExistsException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> checkIfUserAlreadyExists()"
            + ParamUtils.getString(email, userId));

    String ifUserAlreadyExistsQuery = "";
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    if (StringUtils.isBlank(userId) || StringUtils.equalsIgnoreCase(userId, "null")) {
      ifUserAlreadyExistsQuery =
          "select count(emailId) as emailCount from application_user where emailId= ?";
      objectMap.put("emailId", email);
    } else {
      ifUserAlreadyExistsQuery =
          "select count(emailId) as emailCount from application_user where emailId= ? and unqUserId!=?";
      objectMap.put("emailId", email);
      objectMap.put("unqUserId", userId);
      //

    }
    List<Map<String, Object>> executeQueryForResultSet =
        MySqlOperations.executeQueryForResultSetPrepStatement(
            ifUserAlreadyExistsQuery, null, objectMap);
    //List<Map<String, Object>> executeQueryForResultSet = MySqlOperations.executeQueryForResultSetPrepStatement(ifUserAlreadyExistsQuery);
    if (executeQueryForResultSet != null && executeQueryForResultSet.size() > 0) {
      Long emailCount = Long.parseLong(executeQueryForResultSet.get(0).get("emailCount") + "");
      if (emailCount > 0) {
        throw new EntityNameAlreadyExistsException(QueryConstants.DbExceptions.USER_ALREADY_EXISTS);
      }
    }
  }

  private static List<UserGroupMappings> builduserGroupMappingsObject(
      Long userId, String email, List<Long> groupIds) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> builduserGroupMappingsObject()"
            + ParamUtils.getString(userId, email, groupIds));

    List<UserGroupMappings> userGroupMapList = new LinkedList<UserGroupMappings>();
    for (Long groupId : groupIds) {
      UserGroupMappings userGroupMappings = new UserGroupMappings();
      userGroupMappings.setUserId(userId);
      userGroupMappings.setInserteBy(email);
      userGroupMappings.setInsertTs(String.valueOf(DateUtils.getCurrentTimestamp()));
      userGroupMappings.setUpdatedTs(QueryConstants.DEFAULT_TIMESTAMP);
      userGroupMappings.setGroupId(groupId);
      userGroupMapList.add(userGroupMappings);
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << builduserGroupMappingsObject()"
            + ParamUtils.getString(userGroupMapList));

    return userGroupMapList;
  }

  private static List<UserSubGroupMappings> builduserSubGroupMappingsObject(
      Long userId, String email, List<Long> subGroupIds) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> builduserSubGroupMappingsObject()"
            + ParamUtils.getString(userId, email, subGroupIds));

    List<UserSubGroupMappings> userSubgroupMapList = new LinkedList<UserSubGroupMappings>();
    for (Long subGroupId : subGroupIds) {
      UserSubGroupMappings userSubGroupMappings = new UserSubGroupMappings();
      userSubGroupMappings.setUserId(userId);
      userSubGroupMappings.setInsertedBy(email);
      userSubGroupMappings.setInsertTs(String.valueOf(DateUtils.getCurrentTimestamp()));
      userSubGroupMappings.setUpdateTs(QueryConstants.DEFAULT_TIMESTAMP);
      userSubGroupMappings.setSubgroupId(subGroupId);
      userSubgroupMapList.add(userSubGroupMappings);
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << builduserSubGroupMappingsObject()"
            + ParamUtils.getString(userId, email, subGroupIds));

    return userSubgroupMapList;
  }

  private static List<UserRoleMappings> buildUserRoleMappingsObject(
      Long userId, String email, List<Long> roleIds) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> buildUserRoleMappingsObject()"
            + ParamUtils.getString(userId, email, roleIds));

    List<UserRoleMappings> userRoleMappings = new LinkedList<UserRoleMappings>();
    for (Long roleId : roleIds) {
      UserRoleMappings userRoleMapping = new UserRoleMappings();
      userRoleMapping.setUserId(userId);
      userRoleMapping.setInsertedBy(email);
      userRoleMapping.setInsertTs(String.valueOf(DateUtils.getCurrentTimestamp()));
      userRoleMapping.setUpdatedTs(QueryConstants.DEFAULT_TIMESTAMP);
      userRoleMapping.setRoleId(roleId);
      userRoleMappings.add(userRoleMapping);
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << buildUserRoleMappingsObject()"
            + ParamUtils.getString(userRoleMappings));

    return userRoleMappings;
  }

  public static void deleteUser(Long userId, String email) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> deleteUser()" + ParamUtils.getString(userId, email));

    maintainUserHistory(userId + "", email, "DELETE");

    // 1 DELETE FROM APPLICATION USER
    ApplicationUser applicationUser = new ApplicationUser();
    applicationUser.setUnqUserId(userId);

    //UserManagementDao.deleteRow(applicationUser);
    UserManagementDao.softDeleteUser(applicationUser);

    // 2 DELETE FROM USER GROUP MAPPING
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("userId", userId);
    UserManagementDao.deleteRows(new UserGroupMappings(), query);

    // 3DELETE FROM USER SUBGROUP MAPPING
    UserManagementDao.deleteRows(new UserSubGroupMappings(), query);

    // 4DELETE FROM USER ROLE MAPPINGS
    UserManagementDao.deleteRows(new UserRoleMappings(), query);

    // 5DELETE USER SOLUTION MAPPINGS
    UserManagementDao.deleteUserSolutionMappings(userId);
  }

  public static void maintainUserHistory(String userId, String email, String action)
			throws SystemException {

		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> maintainUserHistory()"
				+ ParamUtils.getString(userId, email, action));

		UserManagementDao.maintainUserHistory(userId, email, action);
	}

  public static void saveConatctUsDetails(UserFeedback userFeedback) throws SystemException {
    MySqlOperations.insert(userFeedback, HbaseUtility.getIdAnnotationForClass(UserFeedback.class));
  }

  public static void updateRangerUserIdAndRangerUserNameByUniqueUserId(
      String rangerUserName, Long rangerId, Long userId) throws SQLException, SystemException {

    try {
      Map<String, Object> params = new LinkedHashMap<String, Object>();
      params.put("rangerId", rangerId.toString());
      params.put("rangerUserName", rangerUserName);
      params.put("userId", userId.toString());

      MySqlOperations.executeQuery(
          QueryConstants.UserManageMent.UPDATE_USER_RANGER_USER_DETAILS, params);
    } catch (SystemException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new SystemException(QueryConstants.DbExceptions.DATA_BASE_EXCEPTION);
    }
  }

  public static List<PersonaMaster> fetchAllPersonaDetails(String userId)
      throws SystemException { // fetch presona data from db
    String query = QueryConstants.UserManageMent.GET_ALL_DETAILS_OF_PERSONA;
    Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
    objectMap.put("unqUserId", userId);
    List<PersonaMaster> personaInfo =
        MySqlOperations.scanWithSqlQuery(PersonaMaster.class, query, objectMap);
    return personaInfo;
  }

  public static List<String> listOfUsers(Long groupId) {
    try {
      List<String> list = ApplicationUserDao.getAllUsers(groupId);
      return list;
    } catch (SystemException e) {
      e.printStackTrace();
      return new ArrayList<String>();
    }
  }

  /**
   * @see This method will update all applications & datasoruces for Indexing of global search
   * @param createdBy - UserId OR CreatedBy Id
   * @throws SystemException
   */
  private static void updateAppsAndDatasourceOfUser(Long createdBy) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("createdBy", createdBy);

    MySqlOperations.executeQuery(
        QueryConstants.GlobalSearch.UPDATE_ALL_APPS_OF_USER_IS_MODIEFIED, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << update All Application isModified status  = 0  ");

    MySqlOperations.executeQuery(
        QueryConstants.GlobalSearch.UPDATE_ALL_DATASOURCE_OF_USER_AS_MODIFIED, query);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << update All Datasources isModified status  = 0  ");
  }

  /**
   * This method queries the database for the password of the user with {@code userId} userId
   *
   * @author 10642747
   * @param userId the user for which the password to be fetched
   * @return
   */
  public static String fetchUserPassword(Object userId) {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> fetchUserPassword() for user" + userId);

    try {
      ApplicationUser fetchUserDetails =
          UserManagementDao.fetchUserDetails(Long.parseLong((String) userId));
      SecretService secretService = new SecretService();
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " << fetchGroupLevelUserPassword() for user " + userId);
      return secretService.decrypt(fetchUserDetails.getPassword());
    } catch (Exception e) {
      e.printStackTrace();
      logger.error("problem on generating group level user password" + e);
      try {
        throw new Exception(e.getMessage());
      } catch (Exception e1) {
        e1.printStackTrace();
      }
    }
    return null;
  }

  /**
   * @return list of all users
   * @throws SystemException
   */
  public static List<ApplicationUser> getAllUserDetails() throws SystemException {
    Map<String, Object> map = new LinkedHashMap<String, Object>();
    map.put(Constants.STATUS, UserStatus.ACTIVE.toString());
    List<ApplicationUser> userData = MySqlOperations.scan(ApplicationUser.class, map);
    return userData;
  }
}

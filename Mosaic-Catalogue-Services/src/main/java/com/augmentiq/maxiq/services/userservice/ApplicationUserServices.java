package com.augmentiq.maxiq.services.userservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MysqlConnection;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.configuration.setupdao.GroupConfigDao;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.UserStatus;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.core.dao.userdao.ApplicationUserDao;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;
import com.augmentiq.maxiq.services.setup.bso.UserManagementBso;

public class ApplicationUserServices {

  private static final Logger logger = LoggerFactory.getLogger(ApplicationUserServices.class);
  public static Map<String, List<String>> categories = new HashMap<String, List<String>>();

  static {
    Properties categoriesProperties = new Properties();
    String base = System.getenv().get(CacheConstants.MAXIQ_HOME);

    String basePath = base + "/conf/categories.properties";

    try (FileInputStream fis = FileUtils.openInputStream(new File(basePath)); ) {
      categoriesProperties.load(fis);
      for (final String name : categoriesProperties.stringPropertyNames()) {
        String value = categoriesProperties.getProperty(name);
        List<String> subCategories = new ArrayList<String>();
        String[] subcategories = StringUtils.split(value, ",");
        for (String subcategory : subcategories) {
          subCategories.add(StringUtils.replace(subcategory, "_", " "));
        }

        categories.put(StringUtils.replace(name, "_", " "), subCategories);
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void lockUserIp(HttpServletRequest request, ApplicationUser applicationUser) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> lockUserIp()"
            + ParamUtils.getString(request, applicationUser));

    List<String> userips = applicationUser.getUserips();

    try {
      String ipAddress = request.getHeader("X-FORWARDED-FOR");
      if (ipAddress == null) {

        InetAddress ip = InetAddress.getLocalHost();
        ipAddress = ip.getHostAddress();
        // ipAddress = request.getRemoteAddr();
      }

      if (null != userips && userips.size() > 0) {
        Boolean isIpAvailable = userips.contains(ipAddress);
        if (!isIpAvailable) {
          userips.add(ipAddress);
        }
        applicationUser.setUserips(userips);
      } else {
        List<String> userIpList = new ArrayList<String>();
        userIpList.add(ipAddress);
        applicationUser.setUserips(userIpList);
      }

    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);

      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << lockUserIp()"
              + ParamUtils.getString(request, applicationUser));

      e.printStackTrace();
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << lockUserIp()"
            + ParamUtils.getString(request, applicationUser));
  }

  public static void lockUserBrowser(HttpServletRequest request, ApplicationUser applicationUser)
      throws IOException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> lockUserBrowser()"
            + ParamUtils.getString(request, applicationUser));

    String userAgent = StringUtils.lowerCase(request.getHeader("User-Agent"));

    String browser = "";

    if (userAgent.contains("MSIE")) {

      String substring = userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
      browser = substring.split(" ")[0].replace("MSIE", "IE") + "-" + substring.split(" ")[1];

    } else if (userAgent.contains("safari") && userAgent.contains("version")) {

      browser =
          (userAgent.substring(userAgent.indexOf("safari")).split(" ")[0]).split("/")[0]
              + "-"
              + (userAgent.substring(userAgent.indexOf("version")).split(" ")[0]).split("/")[1];

    } else if (userAgent.contains("opr") || userAgent.contains("opera")) {
      if (userAgent.contains("opera"))
        browser =
            (userAgent.substring(userAgent.indexOf("opera")).split(" ")[0]).split("/")[0]
                + "-"
                + (userAgent.substring(userAgent.indexOf("version")).split(" ")[0]).split("/")[1];
      else if (userAgent.contains("opr"))
        browser =
            ((userAgent.substring(userAgent.indexOf("opr")).split(" ")[0]).replace("/", "-"))
                .replace("opr", "opera");
    } else if (userAgent.contains("chrome")) {
      browser = (userAgent.substring(userAgent.indexOf("chrome")).split(" ")[0]).replace("/", "-");
    } else if ((userAgent.indexOf("mozilla/7.0") > -1)
        || (userAgent.indexOf("netscape6") != -1)
        || (userAgent.indexOf("mozilla/4.7") != -1)
        || (userAgent.indexOf("mozilla/4.78") != -1)
        || (userAgent.indexOf("mozilla/4.08") != -1)
        || (userAgent.indexOf("mozilla/3") != -1)) {

      browser = "Netscape-?";

    } else if (userAgent.contains("firefox")) {
      browser = (userAgent.substring(userAgent.indexOf("firefox")).split(" ")[0]).replace("/", "-");
    }

    if (StringUtils.isNotBlank(browser)) {
      List<String> userBrowsers = applicationUser.getUserBrowsers();

      if (null != userBrowsers && userBrowsers.size() > 0) {
        if (!userBrowsers.contains(browser)) {
          userBrowsers.add(browser);
          applicationUser.setUserBrowsers(userBrowsers);
        }
      } else {

        List<String> browsers = new ArrayList<String>();
        browsers.add(browser);
        applicationUser.setUserBrowsers(browsers);
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << lockUserBrowser()"
            + ParamUtils.getString(request, applicationUser));
  }

  public static void lockUserOs(HttpServletRequest request, ApplicationUser applicationUser) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> lockUserOs()"
            + ParamUtils.getString(request, applicationUser));
    String userAgent = StringUtils.lowerCase(request.getHeader("User-Agent"));

    String os = "";

    if (userAgent.indexOf("windows") >= 0) {
      os = "Windows";
    } else if (userAgent.indexOf("mac") >= 0) {
      os = "Mac";
    } else if (userAgent.indexOf("x11") >= 0) {
      os = "Unix";
    } else if (userAgent.indexOf("android") >= 0) {
      os = "Android";
    } else if (userAgent.indexOf("iphone") >= 0) {
      os = "IPhone";
    }

    List<String> userPlatform = applicationUser.getUserPlatform();

    if (null != userPlatform && userPlatform.size() > 0) {
      if (!userPlatform.contains(os)) {
        userPlatform.add(os);
        applicationUser.setUserPlatform(userPlatform);
      }

    } else {
      List<String> platforms = new ArrayList<String>();
      platforms.add(os);
      applicationUser.setUserPlatform(platforms);
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << lockUserOs()"
            + ParamUtils.getString(request, applicationUser));
  }

  public static Integer checkIfPasswordExpiredOrNot(String emailId) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> checkIfPasswordExpiredOrNot()"
            + ParamUtils.getString(emailId));
    int ispasswordExpired = 0;

    try {

      ApplicationUser appUserProfile = GenericStoreDao.getUserProfile(emailId);

      Long userCreatedDate = Timestamp.valueOf(appUserProfile.getCreatedTs()).getTime();

      if (userCreatedDate != null) {
        if (null == UserManagementBso.getLongDate(appUserProfile.getPasswordRefreshedDate())) {
          // If newly created user
          logger.debug(
              LoggerConstants.LOG_MAXIQWEB
                  + " << checkIfPasswordExpiredOrNot()"
                  + ParamUtils.getString(2));
          return 2;
        }

        if ((findRemainingDaysForExpiry(appUserProfile)) <= 0) {
          // If password of the user is expired

          logger.debug(
              LoggerConstants.LOG_MAXIQWEB
                  + " << checkIfPasswordExpiredOrNot()"
                  + ParamUtils.getString(1));
          return 1;
        }
      }

    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << checkIfPasswordExpiredOrNot()"
              + ParamUtils.getString(emailId));
      e.printStackTrace();
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << checkIfPasswordExpiredOrNot()"
            + ParamUtils.getString(ispasswordExpired));

    // Every thing is good
    return ispasswordExpired;
  }

  public static Integer checkIfUserAlreadyInSystem(
      String emailId, String password, Long appId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >>  checkIfUserAlreadyInSystem()"
            + ParamUtils.getString(emailId, password, appId));

    try {

      ApplicationUser appUserProfile = GenericStoreDao.getUserProfile(emailId, appId);

      if (null != appUserProfile) {

        String lockInd = appUserProfile.getLockInd();
        if (lockInd.equals("UNLOCK")) {
          String storedPassword = appUserProfile.getPassword();
          Long loginFailureCnt = appUserProfile.getLoginFailureCount();
          if (StringUtils.equalsIgnoreCase(password, storedPassword)) {
            logger.debug(
                LoggerConstants.LOG_MAXIQWEB
                    + " << checkIfUserAlreadyInSystem()"
                    + ParamUtils.getString(true));
            return 0;
          } else if (loginFailureCnt
              >= Long.parseLong(Cache.getProperty(CacheConstants.LOGIN_FAILURE_CNT))) {
            ApplicationUserDao.lockUser(emailId);
            return 3;
          } else {
            return 2;
          }
        } else {
          Long lastFailureDiffInMinutes = getTimeStampDiffFromLastFailure(appUserProfile);
          if (lastFailureDiffInMinutes
              > Long.parseLong(Cache.getProperty(CacheConstants.LOGIN_FAILURE_CNT_RESET_TS))) {
            appUserProfile.setLockInd("UNLOCK");
            ApplicationUserDao.unlockLockUser(emailId);
            ApplicationUserServices.insertIntoAudit(appUserProfile);
            return checkIfUserAlreadyInSystem(emailId, password, appId);
          }
          return 4;
        }

      } else {
        return 1;
      }

    } catch (Exception e) {
      e.printStackTrace();

      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << checkIfUserAlreadyInSystem()"
              + ParamUtils.getString(emailId, password));
      ExceptionsMessanger.throwException(new SystemException(), "ERR_145");
      return 2;
    }
  }

  public static Boolean checkIfUserActive(String emailId) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >>  checkIfUserActive()" + ParamUtils.getString(emailId));
    Boolean result = false;
    try {

      ApplicationUser appUserProfile = GenericStoreDao.getUserProfile(emailId);

      if (StringUtils.equalsIgnoreCase(UserStatus.ACTIVE.name(), appUserProfile.getStatus())) {
        result = true;
      }

    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " << checkIfUserActive()" + ParamUtils.getString(emailId));
      e.printStackTrace();
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << checkIfUserActive()" + ParamUtils.getString(result));
    return result;
  }

  public static ApplicationUser getUserBasedOnEmailId(String emailId) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getuserBasedOnEmailId()"
            + ParamUtils.getString(emailId));
    ApplicationUser applicationUser = null;
    try {

      applicationUser = GenericStoreDao.getUserProfile(emailId);

    } catch (SystemException e) {
      e.printStackTrace();
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << getuserBasedOnEmailId()"
              + ParamUtils.getString(emailId));
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getuserBasedOnEmailId()"
            + ParamUtils.getString(applicationUser));
    return applicationUser;
  }

  public static ApplicationUser getUserEmailGroupId(String email, Long groupId)
      throws SystemException, ParseException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getUserEmailGroupId()"
            + ParamUtils.getString(email, groupId));

    ApplicationUser applicationUser = null;
    if (null != email && null != groupId) {
      applicationUser = ApplicationUserDao.getUserEmailGroupId(email, groupId);
      String createdTs = applicationUser.getCreatedTs();
      applicationUser.setPasswordexpirydays(findRemainingDaysForExpiry(applicationUser));
      String passwordRefreshedDate = applicationUser.getPasswordRefreshedDate();
      if (StringUtils.isNotEmpty(createdTs)) {
        long time = Timestamp.valueOf(applicationUser.getCreatedTs()).getTime();
        applicationUser.setCreatedTs(time + "");
      }

      if (StringUtils.isNotBlank(passwordRefreshedDate)) {
        long time = UserManagementBso.getLongDate(passwordRefreshedDate);
        applicationUser.setPasswordRefreshedDate(time + "");
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getUserEmailGroupId()"
            + ParamUtils.getString(applicationUser));
    return applicationUser;
  }

  public static String getUserName(String unqUserId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getGroupName()" + ParamUtils.getString(unqUserId));

    if (null == unqUserId) {
      return null;
    }

    String userName = ApplicationUserDao.getUserName(unqUserId);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getGroupName()" + ParamUtils.getString(userName));
    return userName;
  }

  public static String getGroupName(Long groupId) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getGroupName()" + ParamUtils.getString(groupId));
    String groupName = GroupConfigDao.getGroupName(groupId);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << getGroupName()" + ParamUtils.getString(groupName));
    return groupName;
  }

  public static List<Map<String, Object>> getGroupMap(
      String groupNames, String groupIds, ApplicationUser user) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> groupNames()" + ParamUtils.getString(groupNames));
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> groupIds()" + ParamUtils.getString(groupIds));

    String[] groupNameArray = StringUtils.splitPreserveAllTokens(groupNames, ",");
    String[] groupIdArray = StringUtils.splitPreserveAllTokens(groupIds, ",");

    List<Map<String, Object>> groupMapList = new LinkedList<Map<String, Object>>();

    Boolean indicator = true;
    for (int i = 0; i < groupIdArray.length; i++) {
      Map<String, Object> groupMap = new LinkedHashMap<String, Object>();
      groupMap.put(GENERAL_CONSTANTS.HIVE_QUERY_GROUP_ID, groupIdArray[i]);
      groupMap.put(GENERAL_CONSTANTS.GROUPNAME, groupNameArray[i]);
      if (user.getGroupId() != null
          && user.getGroupId() > 0
          && user.getGroupId()
              == Long.parseLong(groupMap.get(GENERAL_CONSTANTS.HIVE_QUERY_GROUP_ID) + "")) {
        groupMap.put(GENERAL_CONSTANTS.ISSELECTED, true);
        indicator = false;
      } else {
        groupMap.put(GENERAL_CONSTANTS.ISSELECTED, false);
      }

      if (i == groupIdArray.length - 1 && indicator) {
        groupMap.put(GENERAL_CONSTANTS.ISSELECTED, true);
      }
      groupMapList.add(groupMap);
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getGroupName()" +
    // ParamUtils.getString(groupName));
    return groupMapList;
  }

  public static void resetPasswordUser(String emailId, Map<String, String> detailsOfPassword)
      throws Exception {
    // TODO Auto-generated method stub
    if (detailsOfPassword
        .get(Sequences.NEWPASSWORD)
        .equals(detailsOfPassword.get(Sequences.CONFIRMPASSWORD))) {
    	
      GenericStoreDao.resetPasswordUser(emailId, detailsOfPassword);
    }
  }

  public static Long getRemainingDaysUsingEmail(String email) throws Exception {
    ApplicationUser applicationUser = getUserInfoWithoutGroupId(email);
    return findRemainingDaysForExpiry(applicationUser);
  }

  public static ApplicationUser getUserInfoWithoutGroupId(String email) throws SystemException {
    // TODO Auto-generated method stub
    ApplicationUser applicationUser = null;
    if (null != email) {
      applicationUser = ApplicationUserDao.getUserInfoWithoutGroupId(email);
    }
    return applicationUser;
  }

  //Get Client id

  public static Long getClientId(Long userId) throws SystemException {
    String sql =
        "select clientId from " + Sequences.USER_CLIENT_MAPPING + " where unqUserId = " + userId;

    Long nextValue = 0L;

    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql)) {

      while (resultSet.next()) {
        nextValue = resultSet.getLong(1);
      }

    } catch (SQLException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getClientId(Long userId)");
      e.printStackTrace();
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getClientId(Long userId)");
      e.printStackTrace();
    }

    return nextValue;
  }

  // LDAP

  @Deprecated
  public static boolean ldapAuth(String email, String password) throws SystemException {

    String base =
        Cache.getProperty(CacheConstants.OU)
            + "="
            + Cache.getProperty(CacheConstants.USERS)
            + ","
            + Cache.getProperty(CacheConstants.DC)
            + "="
            + Cache.getProperty(CacheConstants.AGUMENT)
            + ","
            + Cache.getProperty(CacheConstants.DC)
            + "="
            + Cache.getProperty(CacheConstants.COM);
    logger.debug("base ", base);
    String dn = "cn=" + email + "," + base;
    Hashtable<String, String> env = new Hashtable<String, String>();

    env.put(
        Context.INITIAL_CONTEXT_FACTORY, Cache.getProperty(CacheConstants.LDAP_CONTEXT_FACTORY));
    env.put(Context.PROVIDER_URL, Cache.getProperty(CacheConstants.LDAP_SERVER_PATH));
    env.put(Context.SECURITY_AUTHENTICATION, Cache.getProperty(CacheConstants.LDAP_SECURITY_AUTH));
    env.put(Context.SECURITY_PRINCIPAL, dn);
    env.put(Context.SECURITY_CREDENTIALS, password);

    try {
      DirContext ctx = new InitialDirContext(env);
      logger.debug("connected");
      logger.debug(" Enviornment ", ctx.getEnvironment());
      ctx.close();
      return true;

    } catch (AuthenticationException ex) {
      logger.debug("incorrect password or username");
    } catch (Exception ex) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + ex);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_146");
    }
    return false;
  }

  private static Long findRemainingDaysForExpiry(ApplicationUser appUserProfile) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> findRemainingDaysForExpiry()"
            + ParamUtils.getString(appUserProfile));

    Long passwordexpirydays = 0l;
    Long elapsedDays = 0l;
    try {
      Long userCreatedDate = Timestamp.valueOf(appUserProfile.getCreatedTs()).getTime();
      Long currentTimeMillis = System.currentTimeMillis();
      long elapsedTime = 0;
      if (userCreatedDate != null) {
        Long refreshedDate =
            UserManagementBso.getLongDate(appUserProfile.getPasswordRefreshedDate());

        if (null == refreshedDate) {
          return 0l;
        }

        elapsedTime = currentTimeMillis - refreshedDate;

        passwordexpirydays = Long.parseLong(Cache.getProperty(CacheConstants.PASS_EXP_DAYS));

        elapsedDays = elapsedTime / (1000 * 60 * 60 * 24);
      }

    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << findRemainingDaysForExpiry()"
              + ParamUtils.getString(passwordexpirydays - elapsedDays));
      e.printStackTrace();
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << findRemainingDaysForExpiry()"
            + ParamUtils.getString(passwordexpirydays - elapsedDays));

    return passwordexpirydays - elapsedDays;
  }

  public static void resetLoginFailureCnt(String emailId) throws SystemException {
    // TODO Auto-generated method stub
    ApplicationUserDao.resetLoginFailureCnt(emailId);
  }

  public static void setLoginFailureCnt(String emailId, long l) throws SystemException {
    // TODO Auto-generated method stub
    ApplicationUserDao.setLoginFailureCnt(emailId, l);
  }

  public static void insertIntoAudit(ApplicationUser appUserProfile) throws SystemException {
    // TODO Auto-generated method stub
    ApplicationUserDao.insertIntoAudit(appUserProfile);
  }

  public static Long getTimeStampDiffFromLastFailure(ApplicationUser appUserProfile)
      throws ParseException {
    long diffMinure = 0;
    try {
      String LastFailureAttemptTs = appUserProfile.getLastFailureAttemptTs();
      if (null == LastFailureAttemptTs) {
        LastFailureAttemptTs =
            new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date());
      } else {
        Date rowdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(LastFailureAttemptTs);
        LastFailureAttemptTs = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(rowdate);
      }
      String todaysDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date());
      SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

      Date d1 = null;
      Date d2 = null;
      d1 = format.parse(LastFailureAttemptTs);
      d2 = format.parse(todaysDate);

      // in milliseconds
      long diff = d2.getTime() - d1.getTime();
      // diffHours = diff / (60 * 60 * 1000) % 24;
      diffMinure = TimeUnit.MILLISECONDS.toMinutes(diff);

      System.out.print(diffMinure + " minutes, ");

    } catch (Exception e) {
      e.printStackTrace();
    }

    return diffMinure;
  }

  public static void recreateSoftDeletedUser(ApplicationUser applicationUser)
      throws SystemException {
    ApplicationUserDao.recreateSoftDeletedUser(applicationUser);
  }
}

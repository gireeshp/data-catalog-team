package com.augmentiq.maxiq.services.userservice;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.entity.model.user.bean.ApplicationUser;

public class UploadUserProfilesUtils {

  private static final Logger logger = LoggerFactory.getLogger(UploadUserProfilesUtils.class);

  private UploadUserProfilesUtils() {}

  public static void uploadUserProfiles(String filePath) throws SystemException {
    uploadUserProfiles_(filePath);
  }

  private static void uploadUserProfiles_(String filePath) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> uploadUserProfiles_()"
            + ParamUtils.getString(filePath));
    try {

      if (StringUtils.isBlank(filePath)) {
        return;
      }

      @SuppressWarnings("unchecked")
      List<String> lines = FileUtils.readLines(new File(filePath), "UTF-8");
      int count = 0;
      if (null != lines && lines.size() > 0) {
        ApplicationUser appUser = null;
        for (String line : lines) {
          if (StringUtils.isNotBlank(line)) {

            String[] temp = StringUtils.splitPreserveAllTokens(line, "|");

            if (count == 0) {
              count++;
              continue;
            }

            appUser = GenericStoreDao.getUserProfile(temp[0]);
            //TODO Modify it if already exists; Create a new domain (new UserProfile()) if doesn't exist.

            if (null == appUser) {
              appUser = new ApplicationUser();
              appUser.setUserId(temp[0]);
              appUser.setPassword(temp[1]);
              appUser.setFirstName(temp[2]);
              appUser.setLastName(temp[3]);
              appUser.setEmailId(temp[4]);
              appUser.setStatus("111");
              appUser.setUserUniqueId(null);
            } else {
              appUser.setUserId(temp[0]);
              appUser.setPassword(temp[1]);
              appUser.setFirstName(temp[2]);
              appUser.setLastName(temp[3]);
              appUser.setEmailId(temp[4]);
              appUser.setStatus("111");
            }

            GenericStoreDao.insertUpdateUserProfile(appUser);
          }
        }
      }

    } catch (IOException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << uploadUserProfiles_()"
            + ParamUtils.getString(filePath));
  }
}

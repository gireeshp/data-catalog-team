package com.augmentiq.maxiq.util.common;

import org.apache.commons.lang.StringUtils;

public class StringUtil {
  public static String joinString(String[] str, String delimiter) {
    String output = "";

    if (null != str) {
      for (String string : str) {
        output += string + delimiter;
      }
    }

    return output;
  }

  public static String escapeSpaceForShellScript(String input) {
    input = StringUtils.replace(input, " ", "\\ ");
    return input;
  }

  public static boolean isNumeric(String value) {
    try {
      Double double1 = Double.valueOf(value);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public static boolean isNotNull(String str) {
    if (null != str) {
      if (StringUtils.isNotBlank(str)) {
        if (!"null".equalsIgnoreCase(str)) {
          return true;
        }
      }
    }
    return false;
  }
}

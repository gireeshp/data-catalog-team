package com.augmentiq.maxiq.util.component.configuration.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ObjectSerializationHandler;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.configuration.setupdao.GroupConfigDao;
import com.augmentiq.maxiq.constant.configuration.enums.Actions;
import com.augmentiq.maxiq.constant.configuration.enums.DataStatus;
import com.augmentiq.maxiq.constant.configuration.enums.FileStoreObject;
import com.augmentiq.maxiq.constant.configuration.enums.FlowType;
import com.augmentiq.maxiq.constant.configuration.enums.ObjectTypes;
import com.augmentiq.maxiq.constant.configuration.generic.GenericConstants;
import com.augmentiq.maxiq.constant.external.source.task.status.enums.TaskStatusEnums;
import com.augmentiq.maxiq.core.dao.configuration.generic.genericdao.GenericStoreDao;
import com.augmentiq.maxiq.core.dao.searchdao.IndexDefinationDao;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepository;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepositoryInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.UserActivity;
import com.augmentiq.maxiq.entity.model.configuration.generic.GenericObjectBuild;
import com.augmentiq.maxiq.model.configuration.factory.DataSourceFactory;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryInstanceDao;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

@Component
public class ConfigurationCreateUtil {

    Logger logger = LoggerFactory.getLogger(ConfigurationCreateUtil.class);

    public Long createDataSources(Map<String, Object> args) throws Exception {

	String datasourcename = args.get(GenericConstants.DATA_SOURCE.DATASOURCENAME) + "";
	// String repoName = args.get(GenericConstants.DATA_SOURCE.REPONAME) +
	// "";
	// String ingection = args.get(GenericConstants.DATA_SOURCE.INGECTION) +
	// "";
	String repoName = "globalRepo";
	String ingection = "quick";
	String userId = (String) args.get("userId");
	Long groupId = (Long) args.get("groupId");
	Map<String, List<String>> existingDSNotifier = (Map<String, List<String>>) args.get("existingDataSourceNotifier");

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> createDataSources() " + ParamUtils.getString(userId, datasourcename, repoName, ingection, groupId));

	if (null != IndexDefinationDao.getIndexDefinationFromIndexName(datasourcename)) {
	    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << createDataSources()" + ParamUtils.getString(datasourcename));
	    ExceptionsMessanger.throwException(new SystemException(), "ERR_086", null);
	}

	if (repoName.equalsIgnoreCase("globalRepo")) {
	    String groupName = GroupConfigDao.getGroupName(groupId);
	    repoName = groupName + "_GlobalRepo";
	}

	HashSet<String> hashSet = GlobalParams.keyWords;

	if (hashSet.contains(datasourcename.toUpperCase())) {

	    ExceptionsMessanger.throwException(new SystemException(), "ERR_003", null);
	}

	// long dsId
	// =configurationCreateService.createDataSource(datasourcename,
	// repoName, userId, ingection, groupId, projectId);
	// DataSourceBso.saveActivityData(projectId, userId,
	// ObjectTypes.DATA_SOURCE, dsId + "", datasourcename, Actions.CREATE);

	Long dsId = createDataSource(datasourcename, repoName, userId, ingection, groupId, -1L,existingDSNotifier);
	if (dsId == null) {
	    return null;
	}
	saveActivityData(-1L, userId, ObjectTypes.DATA_SOURCE, dsId + "", datasourcename, Actions.CREATE);

	/*
	 * logger.debug( LoggerConstants.LOG_MAXIQWEB +
	 * " << createDataSourceAndAttach() " + ParamUtils.getString(dsId));
	 */
	return dsId;

    }

    public Long createDataSource(String dataSourceName, String repoName, String userId, String ingection, Long groupId, Long projectId,Map<String, List<String>> existingDSNotifier) throws ConnectionException, SystemException {

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> createDataSource()" + ParamUtils.getString(dataSourceName, repoName, userId, ingection, groupId));

	Long dataSourceId = createDataSource(dataSourceName, userId, ingection, groupId, projectId,existingDSNotifier);
	if (dataSourceId == null) {
	    return null;
	}
	String groupName = GroupConfigDao.getGroupName(groupId);

	DataRepositoryInstance dataRepoInstance = DataRepositoryInstanceDao.getDataRepoInstance(repoName);

	if (repoName.equalsIgnoreCase(groupName + "_GlobalRepo")) {
	    GenericObjectBuild genericObjectBuild = GenericStoreDao.getConfigurationObject(repoName);
	    if (null == dataRepoInstance && null != genericObjectBuild) {
		// Use repoId from configuration

		DataRepository dataRepository = (DataRepository) ObjectSerializationHandler.toObject(genericObjectBuild.getJson(), DataRepository.class);
		List<Long> dataSourcesStore = dataRepository.getDataSourcesStore();

		// create repoinstance with id same as configuration
		createDataRepoInstance(repoName, userId, groupId, projectId, genericObjectBuild.getId(), dataSourcesStore);

	    } else if (null == genericObjectBuild && null != dataRepoInstance) {

		DataRepository dataRepository = new DataRepository();

		dataRepository.setUserId(userId);
		dataRepository.setRepoName(repoName);
		dataRepository.setId(dataRepoInstance.getRepoId());
		dataRepository.setDataSourcesStore(dataRepoInstance.getDataSourceIds());

		DataRepositoryDao.insertDataRepository(dataRepository);
	    } else {
		DataRepository dataRepository = new DataRepository();

		dataRepository.setUserId(userId);
		dataRepository.setRepoName(repoName);
		dataRepository.setId(null);
		Long dataRepoId = DataRepositoryDao.insertDataRepository(dataRepository);
		dataRepository.setId(dataRepoId);
		// This insert is must because the first time in json id is
		// still null
		DataRepositoryDao.insertDataRepository(dataRepository);

		createDataRepoInstance(repoName, userId, groupId, projectId, dataRepoId, null);
	    }
	}
	Long repoId = dataRepoInstance.getRepoId();

	addDataSourceToRepository(repoId, dataSourceId);

	Map<String, String> map = new HashMap<String, String>();

	map.put("dataRepoName", dataRepoInstance.getRepoName());
	map.put("dataRepoId", dataRepoInstance.getRepoId() + "");
	DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(dataSourceId, map);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << createDataSource()" + ParamUtils.getString(dataSourceId));
	return dataSourceId;
    }

    public Long createDataSource(String dataSourceName, String userId, String ingection, Long groupId, Long projectId,Map<String, List<String>> existingDSNotifier) throws ConnectionException, SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> createDataSource()" + ParamUtils.getString(dataSourceName, userId, ingection, groupId));

	if (null != dataSourceName && dataSourceName.length() > 0) {
	    dataSourceName = dataSourceName.toLowerCase();
	}
	Boolean existingStatus = GenericStoreDao.isDataSourceExist(dataSourceName);

	if (existingStatus) {
	    List<String> existingDSNames = null;
	    if (existingDSNotifier.containsKey(TaskStatusEnums.SUCCESS.name())) {
		existingDSNames = existingDSNotifier.get(TaskStatusEnums.SUCCESS.name());
		existingDSNames.add(dataSourceName);
		existingDSNotifier.remove(TaskStatusEnums.SUCCESS.name());
		existingDSNotifier.put(TaskStatusEnums.FAIL.name(), existingDSNames);
		return null;
	    } else if (existingDSNotifier.containsKey(TaskStatusEnums.FAIL.name())) {
		existingDSNames = existingDSNotifier.get(TaskStatusEnums.FAIL.name());
		existingDSNames.add(dataSourceName);
		existingDSNotifier.put(TaskStatusEnums.FAIL.name(), existingDSNames);
		return null;
	    }
	}

	DataSourceFactory dataSourceFactory = new DataSourceFactory();
	DataSource dataSource = dataSourceFactory.getDataSource(dataSourceName, userId, ingection, groupId);
	String fileType = "";
	String compressionType = "";
	if (dataSource.getFileStoreObject().equals(FileStoreObject.SOURCE.name())) {
	    fileType = FileStoreObject.SOURCE.name();
	    compressionType = FileStoreObject.SOURCE.name();
	}

	dataSource.setId(null);
	Long dataSourceId = DataRepositoryDao.insertDataSource(dataSource);

	// Dont delete this insert. It is neccessary because id is still null in
	// the json in db
	dataSource.setId(dataSourceId);
	DataRepositoryDao.insertDataSource(dataSource);

	createDataSourceInstance(dataSourceName, userId, ingection, groupId, projectId, fileType, compressionType, dataSourceId);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << createDataSource()" + ParamUtils.getString(dataSourceId));
	return dataSourceId;
    }

    public Long createDataRepoInstance(String repoName, String userId, Long groupId, Long projectId, Long dataRepoId, List<Long> dataSourcesStore) throws ConnectionException, SystemException {

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> createDataRepoInstance()" + ParamUtils.getString(repoName, groupId));

	DataRepositoryInstance dataRepositoryInstance = new DataRepositoryInstance();

	dataRepositoryInstance.setRepoId(dataRepoId);

	dataRepositoryInstance.setCreatedBy(userId);

	dataRepositoryInstance.setDataRepoStatus(DataStatus.CREATED);

	dataRepositoryInstance.setRepoCreated(new Date());

	dataRepositoryInstance.setRepoName(repoName);

	dataRepositoryInstance.setSize(0L);

	dataRepositoryInstance.setTotalRecords(0L);

	dataRepositoryInstance.setGroupId(groupId);

	dataRepositoryInstance.setOwnerProjectId(projectId);

	dataRepositoryInstance.setDataSourceIds(dataSourcesStore);

	Long repoId = MySqlOperations.insert(dataRepositoryInstance, HbaseUtility.getIdAnnotationForClass(DataRepositoryInstance.class));

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << createDataRepoInstance()" + ParamUtils.getString(repoId));

	return repoId;
    }

    public void saveActivityData(Long projectId, String userId, ObjectTypes objectType, String objectId, String objectName, Actions action) throws SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> saveActivityData()" + ParamUtils.getString(projectId, userId, objectType, objectId, objectName, action));
	UserActivity activity = new UserActivity(projectId + "", objectType, objectId, userId, System.currentTimeMillis() + "", action, objectName);
	MySqlOperations.insert(activity, HbaseUtility.getIdAnnotationForClass(UserActivity.class));
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << saveActivityData()" + ParamUtils.getString());
    }

    /**
     * This takes the RepoId and DataSourceId and adds the datasource to the
     * data repository.
     *
     * @param repoId
     * @param dataSourceId
     * @throws ConnectionException
     * @throws SystemException
     */
    public void addDataSourceToRepository(long repoId, long dataSourceId) throws ConnectionException, SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> addDataSourceToRepository()" + ParamUtils.getString(repoId, dataSourceId));

	DataRepositoryInstanceDao.updateDsIds(repoId, dataSourceId);
	DataRepository dataRepository = DataRepositoryDao.getDataRepository(repoId);

	List<Long> dsIds = null;

	if (null != dataRepository) {
	    if (dataRepository.getDataSourcesStore() != null) {
		dsIds = dataRepository.getDataSourcesStore();
	    } else {
		dsIds = new ArrayList<>();
	    }
	}

	dsIds.add(dataSourceId);

	dataRepository.setDataSourcesStore(dsIds);

	// GenericObjectBuild allConfiguration =
	// GenericStoreDao.getAllConfiguration(repoId);
	DataRepositoryDao.insertDataRepository(dataRepository);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << addDataSourceToRepository()" + ParamUtils.getString(repoId, dataSourceId));
    }

    private void createDataSourceInstance(String dataSourceName, String userId, String ingestion, Long groupId, Long projectId, String fileType, String compressionType, Long dataSourceId) throws ConnectionException, SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> createDataSourceInstance()" + ParamUtils.getString(dataSourceName, userId, ingestion, groupId, fileType, compressionType, projectId, dataSourceId));

	DataSourceInstance dataSourceInstance = new DataSourceInstance();

	dataSourceInstance.setCreatedDate(new Date());

	dataSourceInstance.setIngection(ingestion);

	dataSourceInstance.setDataSourceName(dataSourceName);

	dataSourceInstance.setDataSourceStatus(DataStatus.CREATED);

	dataSourceInstance.setDataSourceId(dataSourceId);

	dataSourceInstance.setSize(0L);

	dataSourceInstance.setTotalRecords(0L);

	dataSourceInstance.setFlowType(FlowType.PERIODIC);

	dataSourceInstance.setGroupId(groupId);

	dataSourceInstance.setCreatedBy(StringUtils.isNotBlank(userId) ? userId : "");
	dataSourceInstance.setOwnerProjectId(projectId);

	dataSourceInstance.setFileType(fileType);

	dataSourceInstance.setCompressionType(compressionType);
	dataSourceInstance.setCategory(GENERAL_CONSTANTS.DEFAULT);
	dataSourceInstance.setSubCategory(GENERAL_CONSTANTS.DEFAULT);

	DataSourceInstanceDao.insertDataSourceInstance(dataSourceInstance);
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " << createDataSourceInstance()" + ParamUtils.getString(dataSourceName, userId, ingestion, groupId, fileType, compressionType, projectId, dataSourceId));
    }

    public Long createDataRepository(String dataRepoName, String userId, Long groupId, Long projectId) throws ConnectionException, SystemException {
	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >>  createDataRepository()" + ParamUtils.getString(dataRepoName, userId, groupId));

	if (dataRepoName != null && dataRepoName.length() > 0) {
	    dataRepoName = dataRepoName.toLowerCase();
	}
	Boolean existingStatus = GenericStoreDao.isDataSourceExist(dataRepoName);

	if (existingStatus) {
	    ExceptionsMessanger.throwException(new SystemException(), "ERR_019", null);
	}

	DataRepository dataRepository = new DataRepository();

	// Create Data Repository id
	// Long dataRepoId = Sequences.getNextId(Sequences.CONFIGURATION);

	dataRepository.setUserId(userId);
	dataRepository.setId(null);
	Long dataRepoId = DataRepositoryDao.insertDataRepository(dataRepository);

	dataRepository.setId(dataRepoId);
	DataRepositoryDao.insertDataRepository(dataRepository);

	// Assign repo name
	dataRepository.setRepoName(dataRepoName);

	// Insert data repo
	createDataRepoInstance(dataRepoName, userId, groupId, projectId, dataRepoId, null);
	dataRepository.setId(dataRepoId);

	logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << createDataRepository()" + ParamUtils.getString(dataRepoId));
	return dataRepoId;
    }

}

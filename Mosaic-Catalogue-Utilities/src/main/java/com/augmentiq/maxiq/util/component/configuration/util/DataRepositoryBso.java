package com.augmentiq.maxiq.util.component.configuration.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepository;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataRepositoryInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.JobSchedular;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryInstanceDao;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;

/**
 * The service class for serving and modifying the DataRepository
 *
 * @author shiva
 */
public class DataRepositoryBso {

  /**
   * Returns the Data Repo Instance from the repo Name
   *
   * @param repoName
   * @return
   * @throws FileMissingException
   * @throws ConnectionException
   * @throws SystemException
   */
  private static final Logger logger = LoggerFactory.getLogger(DataRepositoryBso.class);

  public DataRepositoryInstance getDataReposirotyInstance(String repoName)
      throws ConnectionException, SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getDataReposirotyInstance()"
            + ParamUtils.getString(repoName));

    DataRepositoryInstance dataRepositoryInstance =
        DataRepositoryInstanceDao.getDataRepoInstance(repoName);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getDataReposirotyInstance()"
            + ParamUtils.getString(dataRepositoryInstance));
    return dataRepositoryInstance;
  }

  /**
   * Gets the DataRepository instance for the RepoName
   *
   * @param repoName
   * @return
   * @throws ConnectionException
   * @throws SystemException
   */
  public DataRepository getDataReposiroty(String repoName)
      throws ConnectionException, SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> getDataReposiroty()" + ParamUtils.getString(repoName));

    DataRepositoryInstance dataRepositoryInstance =
        DataRepositoryInstanceDao.getDataRepoInstance(repoName);

    DataRepository dataRepository =
        DataRepositoryFetchWithChild.getDataRepository(dataRepositoryInstance.getRepoId());

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getDataReposiroty()"
            + ParamUtils.getString(dataRepository));
    return dataRepository;
  }

  /**
   * Updates the JobSchedular object for the repoId
   *
   * @param jobSchedular
   * @param repoId
   * @throws SystemException
   */
  public void updateJobSchedular(JobSchedular jobSchedular, Long repoId) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> updateJobSchedular()"
            + ParamUtils.getString(jobSchedular, repoId));

    DataRepository dataRepository = DataRepositoryDao.getDataRepository(repoId);

    dataRepository.setSchedular(jobSchedular);

    DataRepositoryDao.insertDataRepository(dataRepository);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << updateJobSchedular()"
            + ParamUtils.getString(jobSchedular, repoId));
  }

  /**
   * Takes the data repo id and returns the count of rows in the data repo. The current approach is
   * to sum up all the rows of the containing data sources
   *
   * @return
   * @throws ConnectionException
   * @throws SystemException
   */
  public Long getTotalRecordsInDataRepo(Long dataRepoId)
      throws ConnectionException, SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getTotalRecordsInDataRepo()"
            + ParamUtils.getString(dataRepoId));
    DataRepository dataRepository = DataRepositoryDao.getDataRepository(dataRepoId);

    Long count = 0L;
    if (null != dataRepository && null != dataRepository.getDataSourcesStore()) {
      for (Long datasourceId : dataRepository.getDataSourcesStore()) {
        DataSourceInstance dataSourceInstance =
            DataSourceInstanceDao.getDataSourceInstance(datasourceId);

        if (null != dataSourceInstance) count += dataSourceInstance.getTotalRecords();
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getTotalRecordsInDataRepo()"
            + ParamUtils.getString(count));
    return count;
  }

  /**
   * Takes the data repo id and returns the size of data repo. The current approach is to sum up all
   * the sizes of the containing data sources
   *
   * @return
   * @throws ConnectionException
   * @throws SystemException
   */
  public Long getSizeOfDataRepo(Long dataRepoId) throws ConnectionException, SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getSizeOfDataRepo()"
            + ParamUtils.getString(dataRepoId));
    DataRepository dataRepository = DataRepositoryDao.getDataRepository(dataRepoId);

    Long datarepoSize = 0L;
    if (null != dataRepository && null != dataRepository.getDataSourcesStore()) {
      for (Long datasourceId : dataRepository.getDataSourcesStore()) {
        DataSourceInstance dataSourceInstance =
            DataSourceInstanceDao.getDataSourceInstance(datasourceId);

        if (null != dataSourceInstance) datarepoSize += dataSourceInstance.getSize();
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << checkDataSourceIsQuickType()"
            + ParamUtils.getString(datarepoSize));
    return datarepoSize;
  }

  public static boolean checkDataSourceIsQuickType(Long dataSourceId) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> checkDataSourceIsQuickType()"
            + ParamUtils.getString(dataSourceId));
    try {
      DataSource dataSource = DataRepositoryDao.getDataSource(dataSourceId);
      if (dataSource != null
          && null != dataSource.getIngection()
          && dataSource.getIngection().equals("quick")) {
        return true;
      }
    } catch (HbaseDataFetchException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << checkDataSourceIsQuickType()"
              + ParamUtils.getString(dataSourceId));
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << checkDataSourceIsQuickType()"
            + ParamUtils.getString(false));
    return false;
  }
}


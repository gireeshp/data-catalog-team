package com.augmentiq.maxiq.util.component.configuration.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.StackTraceReader;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.sforce.ws.ConnectorConfig;
/*import com.augmentiq.maxiq.Cache.Cache;
import com.augmentiq.maxiq.Cache.CacheConstants;
import com.augmentiq.maxiq.Configuration.Enums.CompressionTypeEnum;
import com.augmentiq.maxiq.Configuration.Enums.DataSourceType;
import com.augmentiq.maxiq.Configuration.Enums.FileTypeEnum;
import com.augmentiq.maxiq.Configuration.Exceptions.MessageHandler.ExceptionsMessanger;
import com.augmentiq.maxiq.Configuration.Exceptions.MessageHandler.StackTraceReader;
import com.augmentiq.maxiq.Configuration.Utility.ParamUtils;
import com.augmentiq.maxiq.apps.constant.setup.QueryConstants;
import com.augmentiq.maxiq.canvas.workFlow.general.Constants;
import com.augmentiq.maxiq.core.configurationdao.DataRepositoryDao;
import com.augmentiq.maxiq.core.configurationdao.DataRepositoryFetchWithChild;
import com.augmentiq.maxiq.core.configurationdao.DataSourceInstanceDao;
import com.augmentiq.maxiq.core.dao.configuration.exceoption.SystemException;
import com.augmentiq.maxiq.core.dao.mysql.MySqlOperations;
import com.augmentiq.maxiq.core.models.configuration.Bean.DataSource;
import com.augmentiq.maxiq.core.models.configuration.Bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.core.models.configuration.Bean.FieldMapping;
import com.augmentiq.maxiq.core.models.configuration.Bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.core.models.connector.ConnectorConfig;
import com.augmentiq.maxiq.core.services.communicationdetails.CommunicationDetailsBSO;
import com.augmentiq.maxiq.core.workontopics.SampleRecordExtractor;
import com.augmentiq.maxiq.hbase.HbaseUtility;*/

/**
 * Changed for MAX-101 By Rushikesh Raut on 15-Sept-2016 Changed for MAX-502 By Balkrushna Patil on
 * 21-Sept-2016
 */
public class ExternalDataSourceService {

  private static Logger logger_ = LoggerFactory.getLogger(ExternalDataSourceService.class);

  private AmazonS3 getAmazonS3Conn(Map<String, String> properties) {
    String accesskeyid = properties.get("accesskeyid");
    String secretkey = properties.get("secretkey");

    try {
      AWSCredentials credentials = null;
      credentials = new BasicAWSCredentials(accesskeyid, secretkey);

      logger_.info(
          "getting the credentials"
              + credentials.getAWSAccessKeyId()
              + " AND "
              + credentials.getAWSSecretKey());

      AmazonS3 s3 = new AmazonS3Client(credentials);

      return s3;
    } catch (Exception e) {
      logger_.info(StackTraceReader.stringFromStackTrace(e));
    }

    return null;
  }

  private String getFileName(String path) {
    path = StringUtils.removeEnd(path, "/");
    String[] chunks = StringUtils.split(path, "/");
    if (chunks != null) {
      return chunks[chunks.length - 1];
    }

    return null;
  }

  public static ExternalDataSourceDetails fetchExternalDataDetails(Long id) throws SystemException {
    Map<String, Object> query = new LinkedHashMap<String, Object>();
    query.put("id", id);
    return MySqlOperations.scanOneForQuery(ExternalDataSourceDetails.class, query);
  }

  public String downloadData(Map<String, String> connProperties, Map<String, String> param)
      throws FileNotFoundException, IOException {

    AmazonS3 s3 = getAmazonS3Conn(connProperties);
    String bucketName = param.get("bucketName");
    String prefix = param.get("key");
    String fileExtension = param.get("fileExtension");
    String localStorage = param.get("storagePath");
    String downloadObjectType = param.get("downloadObjectType");

    String delimiter = "/";
    if (prefix != null
        && !prefix.endsWith(delimiter)
        && downloadObjectType.equalsIgnoreCase("FOLDER")) {
      prefix += delimiter;
    }

    ListObjectsRequest listObjectsRequest =
        new ListObjectsRequest()
            .withBucketName(bucketName)
            // .withBucketName(bucket.getName())
            .withPrefix(prefix);

    ObjectListing objectListing;

    do {
      objectListing = s3.listObjects(listObjectsRequest);

      for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
        String keyName = objectSummary.getKey();

        if ("FILE".equals(downloadObjectType)) {
          if (!prefix.equalsIgnoreCase(keyName)) {
            continue;
          } else {
            downloadFile(s3, bucketName, keyName, localStorage, getFileName(keyName));
            break;
          }
        }
        downloadFile(s3, bucketName, keyName, localStorage, keyName);
      }
      listObjectsRequest.setMarker(objectListing.getNextMarker());
    } while (objectListing.isTruncated());

    return null;
  }

  private boolean downloadFile(
      AmazonS3 s3, String bucketName, String keyName, String localStorage, String displayName)
      throws FileNotFoundException, IOException {
    logger_.info("Download start ..." + keyName);
    GetObjectRequest request = new GetObjectRequest(bucketName, keyName);
    S3Object object = s3.getObject(request);
    String objectType = object.getObjectMetadata().getContentType();

    if (!(localStorage.lastIndexOf("/") == localStorage.length() - 1))
      localStorage = localStorage + "/";

    File tempDirInWebServer = new File(localStorage);
    if (!tempDirInWebServer.exists()) {
      tempDirInWebServer.mkdirs();
    }

    if ("binary/octet-stream".equalsIgnoreCase(objectType)) {

      String[] splitPreserveAllTokens = StringUtils.splitPreserveAllTokens(keyName, "/");
      String folderIs = localStorage;

      for (String folderName : splitPreserveAllTokens) {
        if (!(folderIs.lastIndexOf("/") == folderIs.length() - 1))
          folderIs = folderIs + "/" + folderName;
        else folderIs = folderIs + folderName;

        tempDirInWebServer = new File(folderIs);
        if (!tempDirInWebServer.exists()) {
          tempDirInWebServer.mkdirs();
        }
      }
      return true;
    }

    S3ObjectInputStream objectContent = object.getObjectContent();

    IOUtils.copy(objectContent, new FileOutputStream(localStorage + displayName));

    logger_.info("Download end ..." + keyName);
    return false;
  }

  /**
   * @param data
   * @param headerAt
   * @param dataSource
   * @param dsId
   * @param headerTemplate
   * @param indicator
   * @return
   * @throws SystemException
   */
  public static List<FieldMapping> fetchFieldMapping(
      List<String> data,
      Long headerAt,
      DataSource dataSource,
      Long dsId,
      String headerTemplate,
      Boolean indicator)
      throws SystemException {
    String headerRecordFields = "";

    List<String> sampleRecordsList = new ArrayList<String>();

    Long count = headerAt;
    while (headerAt + 10L > count && data.size() > count) {
      sampleRecordsList.add(data.get(Integer.parseInt(count + "")));
      count++;
    }

    if (headerAt == 1L) headerRecordFields = data.get(Integer.parseInt((headerAt - 1L) + ""));
    else {
      int index = 0;
      int size = sampleRecordsList.size();
      while (StringUtils.isBlank(headerRecordFields) && index < size) {
        headerRecordFields = StringUtils.upperCase(sampleRecordsList.get(index));
        index++;
      }
    }

    Map<Integer, FieldMapping> fieldMapppingMap = null;
    if (null != dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()
        && false != dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
      fieldMapppingMap =
          SampleRecordExtractor.generateFieldMappingFromHeader(
              SampleRecordExtractor.getTokenisedStringArrayFromStringUsingDelimiter(
                  headerRecordFields, dataSource.getFileDataIngesterDetails().getDelimiter()),
              headerTemplate,
              dsId,
              !indicator,
              dataSource);
    } else {
      fieldMapppingMap =
          SampleRecordExtractor.generateFieldMappingFromHeader(
              StringUtils.splitPreserveAllTokens(
                  headerRecordFields, dataSource.getFileDataIngesterDetails().getDelimiter()),
              headerTemplate,
              dsId,
              !indicator,
              dataSource);
    }

    if (sampleRecordsList.size() > 0) {

      int counter = 1;

      for (String sampleRecord : sampleRecordsList) {

        String[] fieldValues = null;
        if (null == dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()
            || false
                == dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
          sampleRecord = sampleRecord.replaceAll("\"", "");
        }

        int position = 0;

        if (dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
          fieldValues =
              SampleRecordExtractor.getTokenisedStringArrayFromStringUsingDelimiter(
                  sampleRecord, dataSource.getFileDataIngesterDetails().getDelimiter());
        } else {
          fieldValues =
              StringUtils.splitByWholeSeparatorPreserveAllTokens(
                  sampleRecord, dataSource.getFileDataIngesterDetails().getDelimiter());
        }
        if (null != fieldValues && fieldValues.length > 0) {
          for (String fieldValue : fieldValues) {
            if (fieldMapppingMap.containsKey(position)) {

              FieldMapping fieldMapping = fieldMapppingMap.get(position);
              List<String> rows = fieldMapping.getSampleRecords();
              rows.add(fieldValue);
            }
            position++;
          }
          counter++;
        }
      }

      List<FieldMapping> fieldMappings = new ArrayList<>();

      for (Entry<Integer, FieldMapping> entry : fieldMapppingMap.entrySet()) {

        String string = SampleRecordExtractor.outPutFieldValidation(entry.getValue());
        if (string != null) {
          entry.getValue().setFieldName(entry.getValue().getFieldName() + "_1");
        }
        fieldMappings.add(entry.getValue());
      }

      return fieldMappings;
    }
    return null;
  }
  /**
   * @param dataSourceId
   * @param indicator
   * @param dataSource
   * @param fields
   * @return
   * @throws SystemException
   */
  public static DataSource saveExternalDataSource(
      Long dataSourceId, Boolean indicator, DataSource dataSource, List<String> fields)
      throws SystemException {
	  logger_.info("saveExternalDataSource method starts");
    List<FieldMapping> fieldMappings;

    List<FieldMapping> existingFieldMappings = dataSource.getFieldMappings();
    if (indicator) {
      fieldMappings = fetchFieldMapping(fields, 1L, dataSource, dataSourceId, null, indicator);
    } else {
      fieldMappings = fetchFieldMapping(fields, 0L, dataSource, dataSourceId, null, indicator);
    }

    fieldMappings = SampleRecordExtractor.dataTypeGenerator(fieldMappings);

    dataSource.setFieldMappings(fieldMappings);

   /* boolean isSchemaChanged =
        CommunicationDetailsBSO.verifyCurrentDataSourceFieldMappingWithExisting(
            fieldMappings, existingFieldMappings);

    logger_.info("Is field Mapping schema has changes :" + isSchemaChanged);

    if (isSchemaChanged) {

      String uuid =
          CommunicationDetailsBSO.saveCommunicationDetails(
              dataSource.getFileDataIngesterDetails(),
              dataSource.getFieldMappings(),
              Constants.FIELDMAPPING_WARNING);
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_134", ParamUtils.getString(uuid));
    }*/

    dataSource.setFileStoreObject(dataSource.getFileStoreObject());
    dataSource.setDataAtRestCompressionType(CompressionTypeEnum.SNAPPY.name());
    dataSource.setDataAtRestFileType(FileTypeEnum.PARQUET.name());

    DataRepositoryDao.insertDataSource(dataSource);
    logger_.info("saveExternalDataSource method ends");
    return dataSource;
  }

  public static DataSource getDataSourceWithBasicDetails(
      Long dataSourceId, Boolean indicator, ExternalDataSourceDetails eDetails)
      throws SystemException {
    DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceId);
    FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();

    dataSource.setDataSourceType(DataSourceType.EXTERNAL_DATA);
    if (eDetails.getId() == null) eDetails.setId(null);

    dataSource.setExternalDataId(eDetails.getId());
    ConnectionConfig config = null;
    if (null != dataSource.getConfig()) {
      config = dataSource.getConfig();
    }

    if (null == config) {
      config = new ConnectionConfig();
    }

   //config.setInputFileType(FileTypeEnum.TEXT.name());
    //config.setInputCompressionType(CompressionTypeEnum.UNCOMPRESSED.name());

    dataSource.setConfig(config);

    if (fileDataIngesterDetails == null) fileDataIngesterDetails = new FileDataIngesterDetails();

    fileDataIngesterDetails.setContainsHeader(indicator + "");
    fileDataIngesterDetails.setDelimiter(eDetails.getDelimeter());
    fileDataIngesterDetails.setDataSourceType(DataSourceType.EXTERNAL_DATA);
    //fileDataIngesterDetails.setServerFilePath(
      //  Cache.getProperty(CacheConstants.UPLOAD_PATH) + System.currentTimeMillis());
    eDetails.setStoragePath(fileDataIngesterDetails.getServerFilePath());
    fileDataIngesterDetails.setDataSourceId(dataSourceId);
    dataSource.setFileDataIngesterDetails(fileDataIngesterDetails);

    Map<String, String> updatedValue = new HashMap<String, String>();
    updatedValue.put("dataSourceType", fileDataIngesterDetails.getDataSourceType().toString());
    DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(
        fileDataIngesterDetails.getDataSourceId(), updatedValue);

    if (dataSource.getExternalDataId() == null) {
      Long externalDsId =
          MySqlOperations.insert(
              eDetails, HbaseUtility.getIdAnnotationForClass(ExternalDataSourceDetails.class));
      dataSource.setExternalDataId(externalDsId);
    } else {
      ExternalDataSourceService.updateExternalDataSourceDetails(
          eDetails, dataSource.getExternalDataId());
    }

    return dataSource;
  }

  public static void updateExternalDataSourceDetails(ExternalDataSourceDetails eDetails, Long id)
      throws SystemException {
    // TODO Auto-generated method stub
    logger_.info("update ExternalDataSourceDetails method starts");
    Map<String, Object> query = new LinkedHashMap<>();
    query.put(QueryConstants.DataRequest.ID, id);
    MySqlOperations.update(eDetails, query);

    logger_.info("update ExternalDataSourceDetails method complete");
  }
}

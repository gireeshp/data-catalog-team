package com.augmentiq.maxiq.util.component.configuration.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;

import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;

/**
 * Utility class for commonly used File Operations
 *
 * @author shiva
 */
public class FileUtilities {
  /**
   * Checks if the input path given is Directory
   *
   * @param filePath
   * @return
   */
  /*	private static final Logger logger = LoggerFactory.getLogger(FileUtilities.class);
   */
  public static Boolean isDirectory(String filePath) {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> isDirectory()" + ParamUtils.getString(filePath));

    if (StringUtils.isBlank(filePath)) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << isDirectory()" + ParamUtils.getString(false));
      return false;
    }
    File file = new File(filePath);
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << isDirectory()" + ParamUtils.getString(file.isDirectory()));
    return file.isDirectory();
  }

  /**
   * Returns the files in the folder
   *
   * @param folderName
   * @return
   */
  public static List<String> getFiles(String folderName) {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> getFiles()" + ParamUtils.getString(folderName));
    if (StringUtils.isBlank(folderName)) {
      // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getFiles()" + ParamUtils.getString(null));
      return null;
    }

    File folder = new File(folderName);
    File[] listOfFiles = folder.listFiles();

    List<String> list = new ArrayList<>();

    for (int i = 0; i < listOfFiles.length; i++) {
      if (listOfFiles[i].isFile()) {
        list.add(folderName + listOfFiles[i].getName());
      }
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" << getFiles()" + ParamUtils.getString(list));
    return list;
  }

  /**
	 * Reads the file Line by line and returns the list of strings. You can
	 * limit the return list by specifying the limit.
	 * 
	 * @param fileName
	 * @param limit
	 * @param ds
	 * @return
	 * @throws SystemException
	 */
	public static List<String> readFile(String fileName, Integer limit,
			DataSource ds) throws SystemException {
		// logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> readFile()" + ParamUtils.getString(fileName,limit,ds));
		
		BufferedReader br = null;

		List<String> stringlist = new ArrayList<String>();
		if (null != ds) {
			FileDataIngesterDetails fileDataIngesterDetails = ds
					.getFileDataIngesterDetails();

			Long headerStarting = fileDataIngesterDetails.getHeaderStarting();
			String containsHeader = fileDataIngesterDetails.getContainsHeader();
			
			/*System.out.println("Contains Header: "+containsHeader);*/
			// logger.info(LoggerConstants.LOG_MAXIQWEB+"Contains Header: "+containsHeader);

			Boolean recordStartsWithARegExp = fileDataIngesterDetails
					.getRecordStartsWith();
			String startRegularExp = fileDataIngesterDetails
					.getStartingRegularExpre();
			Long recordStartLine = fileDataIngesterDetails.getRecordStartLine();

			String recordsToIgnoreRegEx = fileDataIngesterDetails
					.getRecordsIgnore();

			try {

				if (StringUtils.isBlank(fileName)) {
					// logger.debug(LoggerConstants.LOG_MAXIQWEB+" << readFile()" + ParamUtils.getString(fileName,limit,ds));
					return null;
				}

				String sCurrentLine;

				br = new BufferedReader(new FileReader(fileName));

				Integer counter = 0;
				Long fetchCounter = 0L;

				Boolean foundStartLine = false;

				if (recordStartsWithARegExp) {
					if (StringUtils.isBlank(startRegularExp))
						foundStartLine = true;
				} else {
					if (recordStartLine == null)
						foundStartLine = true;
				}

				while ((sCurrentLine = br.readLine()) != null) {

					fetchCounter++;

					if (!foundStartLine) {
						if (recordStartsWithARegExp) {
							if (Pattern.matches(startRegularExp,sCurrentLine)) {
								foundStartLine = true;
								fileDataIngesterDetails
										.setRecordStartLine(fetchCounter);
								stringlist.add(sCurrentLine);
							}
							continue;
						} else {
							if (fetchCounter >= recordStartLine) {
								foundStartLine = true;
							}
							else
								continue;

						}
					}
					if (StringUtils.equalsIgnoreCase(containsHeader, "true")
							&& headerStarting != null
							&& headerStarting >= fetchCounter)
						continue;

					if (StringUtils.isNotBlank(recordsToIgnoreRegEx)
							&& Pattern.matches(recordsToIgnoreRegEx,sCurrentLine))
						continue;

					if (counter < limit) {
						/**
						 * if (counter < limit && StringUtils.isNotBlank(sCurrentLine) ) 
						 * above condition will break the loop as soon as it gets blank record hence will not traverse datasource completely.
						 */
						if (StringUtils.isNotBlank(sCurrentLine)) {
							stringlist.add(sCurrentLine);
							counter++;
						}
					} else
						break;
				}
				if(stringlist.isEmpty()){
					ExceptionsMessanger.throwException(new SystemException(), "ERR_182", fileName);
				}
				return stringlist;

			} catch (IOException e) {
				
				// logger.warn(LoggerConstants.LOG_MAXIQWEB+e);
				
				ExceptionsMessanger.throwException(new SystemException(), "ERR_010", fileName);
			} finally {
				try {
					if (br != null)
						br.close();
				} catch (IOException ex) {
					// logger.warn(LoggerConstants.LOG_MAXIQWEB+ex);
					ex.printStackTrace();
				}
				// logger.debug(LoggerConstants.LOG_MAXIQWEB+" << readFile()" + ParamUtils.getString(fileName,limit,ds));
			}
		}
		// logger.debug(LoggerConstants.LOG_MAXIQWEB+" << readFile()" + ParamUtils.getString(stringlist));
		return stringlist;

	}
	
  /**
   * Reads line number 'n' from file
   *
   * @param fileName
   * @param lineNo
   * @return
   * @throws SystemException
   */
  public static String readNthLine(String fileName, Long lineNo) throws SystemException {

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> readNthLine()" + ParamUtils.getString(fileName,lineNo));
    BufferedReader br = null;

    try {

      if (StringUtils.isBlank(fileName)) {
        // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : << readNthLine()" + ParamUtils.getString(null));
        return null;
      }

      String sCurrentLine;

      br = new BufferedReader(new FileReader(fileName));

      Long counter = 0L;

      while ((sCurrentLine = br.readLine()) != null) {
        counter++;

        if (counter == lineNo) return sCurrentLine;
      }

      //return null;

    } catch (IOException e) {
      // logger.warn(LoggerConstants.LOG_MAXIQWEB+e);
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_010", fileName, e.getMessage());
    } finally {
      try {
        if (br != null) br.close();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : << readNthLine()" + ParamUtils.getString(null));
    return null;
  }

  public static String takeCareOfHeaderOptions(
      String inputFilePath,
      Long recordStartLine,
      Long headerStartsAt,
      Long jobInstId,
      Long dsId,
      String containsHeader)
      throws SystemException {

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> takeCareOfHeaderOptions()" + ParamUtils.getString(inputFilePath,recordStartLine,headerStartsAt,jobInstId,dsId,containsHeader));

    String offsetInfo = "";

    if (recordStartLine == null) recordStartLine = 1L;

    //		System.out.println("recordStartLine: " + recordStartLine);
    //		System.out.println("headerStartsAt: " + headerStartsAt);
    //		System.out.println("inputFilePath: " + inputFilePath);

    File in = new File(inputFilePath);
    List<File> inFiles = new ArrayList<File>();

    if (in.isDirectory()) inFiles = Arrays.asList(in.listFiles());
    else inFiles.add(in);

    for (File inFile : inFiles) {
      String info =
          handleOneFile(inFile, recordStartLine, headerStartsAt, jobInstId, dsId, containsHeader);

      if (StringUtils.isNotBlank(info)) {
        if (StringUtils.isBlank(offsetInfo)) offsetInfo = info;
        else offsetInfo = offsetInfo + "," + info;
      }
    }

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : << takeCareOfHeaderOptions()" + ParamUtils.getString(offsetInfo));
    return offsetInfo;
  }

  private static String handleOneFile(
      File inFile,
      Long recordStartLine,
      Long headerStartsAt,
      Long jobInstId,
      Long dsId,
      String containsHeader)
      throws SystemException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> handleOneFile()" + ParamUtils.getString(inFile,recordStartLine,headerStartsAt,jobInstId,dsId,containsHeader));

    LineIterator li = null;

    Long start = 1L;

    if (recordStartLine != null) start = recordStartLine;

    if (StringUtils.equalsIgnoreCase(containsHeader, "true")
        && headerStartsAt != null
        && headerStartsAt >= recordStartLine) start = headerStartsAt + 1L;

    String offSetInfo = "";

    try {
      li = FileUtils.lineIterator(inFile);

      Long noOfLinesTouched = 0L;
      Long offset = 0L;

      while (li.hasNext()) {
        noOfLinesTouched++;

        String rec = li.nextLine();

        //				System.out.println("Record no: " + noOfLinesTouched
        //						+ ". Record: " + rec);
        // logger.info(LoggerConstants.LOG_MAXIQWEB+" : << handleOneFile() Record no: " + ParamUtils.getString(noOfLinesTouched) + ". Record: " + ParamUtils.getString(rec));

        if (rec == null || noOfLinesTouched < start) {

          //					System.out.println("Ignore him.");
          // logger.info(LoggerConstants.LOG_MAXIQWEB+" : << handleOneFile() Ignore him.");

          int length = StringUtils.length(rec) + 1;
          offset += length;

          System.out.println(inFile.getName() + " offset so far: " + offset);

          // logger.info(LoggerConstants.LOG_MAXIQWEB+" : << handleOneFile()" + ParamUtils.getString(inFile.getName(),offset));
          continue;
        }

        if (DataRepositoryBso.checkDataSourceIsQuickType(dsId)) {
          offSetInfo =
              GlobalParams.MAXIQ_DATA_PATH
                  + "/ds/stage2/"
                  + dsId
                  + "/"
                  + jobInstId
                  + "/"
                  + inFile.getName()
                  + ":"
                  + offset;
        } else {
          offSetInfo =
              GlobalParams.MAXIQ_DATA_PATH
                  + "/ds/stage1/"
                  + dsId
                  + "/"
                  + jobInstId
                  + "/"
                  + inFile.getName()
                  + ":"
                  + offset;
        }

        System.out.println("Found offset: " + offSetInfo + ". Exiting.");
        // logger.info(LoggerConstants.LOG_MAXIQWEB+" : << handleOneFile() Found offset: " + ParamUtils.getString(offSetInfo) + ". Exiting.");
        break;
      }

    } catch (IOException e) {
      // logger.warn(LoggerConstants.LOG_MAXIQWEB+e);
      e.printStackTrace();
    } finally {
      if (li != null) li.close();
    }
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : << handleOneFile()" + ParamUtils.getString(offSetInfo));
    return offSetInfo;
  }

 /* public static String fileForExtraction(String localPath) throws SystemException, IOException {
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : >> fileForExtraction()" + ParamUtils.getString(localPath));
    File file = new File(localPath);
    // logger.info("Local File Path at: "+ParamUtils.getString(localPath));
    if (!file.exists()) {
      FileUtils.forceMkdir(file);
      //			ExceptionsMessanger.throwException(new SystemException(), "ERR_104", localPath);

      //			file.mkdirs();

    }
    if (FileUtilities.isDirectory(localPath)) {
      if (!localPath.endsWith("/")) localPath = localPath + "/";
      localPath += "*";
    }
    ExecuteScript executeScript = new ExecuteScript("chmod 777 " + localPath);
    executeScript.execute();
    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : << fileForExtraction()" + ParamUtils.getString(localPath));
    return localPath;
  }*/
}


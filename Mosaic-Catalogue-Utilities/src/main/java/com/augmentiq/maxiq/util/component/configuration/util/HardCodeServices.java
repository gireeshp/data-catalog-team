package com.augmentiq.maxiq.util.component.configuration.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.RandomUtility;


/**
 * This class is used to generate some hardcoded values. Will have to remove this file as we go on
 * with our development.
 *
 * @author shiva
 */
public class HardCodeServices {
  public static final String PARSE_ADDRESS = "parseAddress";
  public static final String INIT_ADDRESS = "init";
  public static final String DEFAULT_GEO_TAGGIN_PREFIX = "GEO_TAGGING_";
  public static final String ROWNUM = "rownum";
  public static String VERSION = "V1";
  public static String RECORDID = "RecordId";
  public static Long USER_ID = 1L;
  public static String DEFAULT_FIELDNAME = "Field";

  public static String getHardCoded(int lenght) {
    return RandomUtility.getRandomString(lenght);
  }

  public static List<String> getRandomStringList(int size) {
    List<String> list = new ArrayList<>();

    for (int i = 0; i < size; i++) {
      list.add(getHardCoded(5));
    }
    return list;
  }

  private static Random random = new Random();

  public static List<Long> getRandomLongList(int size) {
    List<Long> list = new ArrayList<>();

    for (int i = 0; i < size; i++) {
      list.add(random.nextLong());
    }
    return list;
  }

  static Map<Integer, String> geoTagginOutput = new HashMap<Integer, String>();

  public static List<String> getGeoTaggingOutPutList(List<Integer> outputFields) {

    geoTagginOutput.put(0, "uId");
    geoTagginOutput.put(1, "State");
    geoTagginOutput.put(2, "District");
    geoTagginOutput.put(3, "City");
    geoTagginOutput.put(4, "Pin");
    geoTagginOutput.put(5, "Locality1");
    geoTagginOutput.put(6, "Locality2");
    geoTagginOutput.put(7, "Locality3");
    geoTagginOutput.put(8, "Locality4");
    geoTagginOutput.put(9, "TokenBasedLocalityToken");
    geoTagginOutput.put(10, "TokenBasedLocality");
    geoTagginOutput.put(11, "Street1Token");
    geoTagginOutput.put(12, "Street1");
    geoTagginOutput.put(13, "Street2Token");
    geoTagginOutput.put(14, "Street2");
    geoTagginOutput.put(15, "Street3Token");
    geoTagginOutput.put(16, "Street3");
    geoTagginOutput.put(17, "OldNo");
    geoTagginOutput.put(18, "NewNo");
    geoTagginOutput.put(19, "Floor");
    geoTagginOutput.put(20, "Plot");
    geoTagginOutput.put(21, "Pocket");
    geoTagginOutput.put(22, "Sector");
    geoTagginOutput.put(23, "HouseNo");
    geoTagginOutput.put(24, "Ward");
    geoTagginOutput.put(25, "UnIdNo");
    geoTagginOutput.put(26, "HouseNameToken");
    geoTagginOutput.put(27, "HouseName");
    geoTagginOutput.put(28, "Commercial");
    geoTagginOutput.put(29, "UnIdNoTokenValue");
    geoTagginOutput.put(30, "FirstNumber");
    geoTagginOutput.put(31, "BlockNo");
    geoTagginOutput.put(32, "Wing");
    geoTagginOutput.put(33, "Phase");
    geoTagginOutput.put(34, "Flat");
    geoTagginOutput.put(35, "Main");
    geoTagginOutput.put(36, "Cross");
    geoTagginOutput.put(37, "Landmark1");
    geoTagginOutput.put(38, "Landmark2");
    geoTagginOutput.put(39, "Landmark3");
    geoTagginOutput.put(40, "Residual");
    geoTagginOutput.put(41, "DerPin1");
    geoTagginOutput.put(42, "DerPin2");
    geoTagginOutput.put(43, "StartTimeTaken");
    geoTagginOutput.put(44, "EndTimeTaken");
    geoTagginOutput.put(45, "TimeTaken");
    geoTagginOutput.put(46, "Relation");

    List<String> geoTagginFields = new ArrayList<>();

    if (null != outputFields) {
      for (int fields : outputFields) {
        geoTagginFields.add(geoTagginOutput.get(fields - 1));
      }
    }

    return geoTagginFields;
  }
}

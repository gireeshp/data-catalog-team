package com.augmentiq.maxiq.util.component.configuration.util;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;

public class HbaseDataFetchException extends SystemException {
  /** */
  private static final long serialVersionUID = 1L;

  public HbaseDataFetchException(String msg) {
    super(msg);
  }

  public HbaseDataFetchException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

package com.augmentiq.maxiq.util.component.configuration.util;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.DateUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.ConnectionException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionMessages;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;
import com.augmentiq.maxiq.constant.workFlow.general.Constants;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSourceInstance;
import com.augmentiq.maxiq.entity.model.configuration.bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.model.configuration.factory.FieldMappingFactory;
import com.augmentiq.maxiq.orm.entity.model.connector.ConnectionConfig;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
/*import com.augmentiq.maxiq.CONSTATS.GlobalParams;
import com.augmentiq.maxiq.CONSTATS.LoggerConstants;
import com.augmentiq.maxiq.Configuration.Enums.DataSourceType;
import com.augmentiq.maxiq.Configuration.Enums.FieldDataTypes;
import com.augmentiq.maxiq.Configuration.Exceptions.ConnectionException;
import com.augmentiq.maxiq.Configuration.Exceptions.MessageHandler.ExceptionMessages;
import com.augmentiq.maxiq.Configuration.Exceptions.MessageHandler.ExceptionsMessanger;
import com.augmentiq.maxiq.Configuration.Utility.DateUtils;
import com.augmentiq.maxiq.Configuration.Utility.ParamUtils;
import com.augmentiq.maxiq.Configuration.factory.FieldMappingFactory;
import com.augmentiq.maxiq.Utils.FileUtilities;
import com.augmentiq.maxiq.canvas.workFlow.general.Constants;
import com.augmentiq.maxiq.core.configurationdao.DataRepositoryDao;
import com.augmentiq.maxiq.core.configurationdao.DataSourceInstanceDao;
import com.augmentiq.maxiq.core.dao.configuration.exceoption.SystemException;
import com.augmentiq.maxiq.core.dao.mysql.MySqlOperations;
import com.augmentiq.maxiq.core.dataExtractor.XlsFileReader;
import com.augmentiq.maxiq.core.models.configuration.Bean.DataSource;
import com.augmentiq.maxiq.core.models.configuration.Bean.DataSourceInstance;
import com.augmentiq.maxiq.core.models.configuration.Bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.core.models.configuration.Bean.FieldMapping;
import com.augmentiq.maxiq.core.models.configuration.Bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.core.models.connector.ConnectorConfig;
import com.augmentiq.maxiq.core.services.communicationdetails.CommunicationDetailsBSO;
import com.augmentiq.maxiq.core.services.externaldsservice.ExternalDataSourceService;
import com.augmentiq.maxiq.core.services.externaldsservice.SapErpService;
import com.augmentiq.maxiq.core.services.genericservices.HardCodeServices;
import com.augmentiq.maxiq.hbase.HbaseUtility;*/
import com.opencsv.CSVReader;
import com.sap.conn.jco.JCoException;

/**
 * Will take the DataSource object and creates the Field Mapping from that
 *
 * @author shiva Changed for MAX-101 By Rushikesh Raut on 15-Sept-2016 Changed for MAX-502 By
 *     Balkrushna Patil on 21-Sept-2016
 */
@Deprecated
public class SampleRecordExtractor {

  private static final Logger logger = LoggerFactory.getLogger(SampleRecordExtractor.class);

  static List<String> dateFormats = new ArrayList<String>();

  static {
    dateFormats.add("dd-MM-yyyy");
    dateFormats.add("MM-dd-yyyy");
    dateFormats.add("dd-MM-yy");
    dateFormats.add("MM-dd-yy");
  }

  /**
   * This will take the datasource id and fetch the DataSource object from the database and will
   * create the field mappings
   *
   * @param dataSourceId
   * @param localFilePath TODO
   * @throws SystemException
   * @throws ConnectionException
   * @throws JCoException
   * @throws IOException
   */
  public static void sampleRecordExtractor(Long dataSourceId)
      throws SystemException, ConnectionException, JCoException {

    DataSource dataSource = DataRepositoryDao.getDataSource(dataSourceId);

    if (dataSource.getFileDataIngesterDetails().getDataSourceType()
        == DataSourceType.EXCEL_DATA_SOURCE) {
      dataSource.getFileDataIngesterDetails().setDelimiter(GlobalParams.MAXIQ_DEL);
    }

    sampleRecordExtractor(dataSource, false);
  }

  /**
   * This will take the DataSource object and will create the field mappings
   *
   * @param dataSourceId
   * @param localFilePath
   * @throws SystemException
   * @throws ConnectionException
   * @throws JCoException
   * @throws IOException
   */
  public static void sampleRecordExtractor(DataSource dataSource, Boolean local)
      throws SystemException, ConnectionException, JCoException {

    if (null != dataSource && null != dataSource.getDataSourceType()) {
      if (DataSourceType.FILE_DATA_SOURCE.equals(dataSource.getDataSourceType())) {
        sampleRecordExtractorFile(dataSource, local);
      } else if (DataSourceType.CSV_DATA_SOURCE.equals(dataSource.getDataSourceType())) {
        sampleRecordExtractorFile(dataSource, local);
      } else if (DataSourceType.TSV_DATA_SOURCE.equals(dataSource.getDataSourceType())) {
        sampleRecordExtractorFile(dataSource, local);
      } else if (DataSourceType.EXCEL_DATA_SOURCE.equals(dataSource.getDataSourceType())) {
        excelExtractor(dataSource);
      } else if (DataSourceType.FIXED_LENGTH_FORMAT.equals(dataSource.getDataSourceType())) {
        sampleRecordExtractorFile(dataSource, local);
      } else if (DataSourceType.REMOTE_DATA.equals(dataSource.getDataSourceType())) {
        sampleRecordExtractorForRemoteData(dataSource, local);
      } else if (DataSourceType.EXTERNAL_DATA.equals(dataSource.getDataSourceType())) {
        sampleRecordExtractorForExternalData(dataSource, local);
      }

    } else {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_009", 0L);
    }
  }

  /**
   * This will take the Datasource object of data source type and fetch data from exteranl
   * connectors and create field mappings
   *
   * @param dataSource
   * @param local
   * @throws SystemException
   * @throws JCoException
   */
  private static void sampleRecordExtractorForExternalData(DataSource dataSource, Boolean local)
      throws SystemException, JCoException {
    // TODO Auto-generated method stub
    logger.info("sampleRecordExtractorForExternalData starts");
    logger.debug("dataSource : "+dataSource +" , local : "+local);
    if (null != dataSource) {
      ExternalDataSourceDetails externalDataSourceDetails =
          dataSource.getExternalDataSourceDetails();
      List<String> headerWithData = new ArrayList<String>();

      headerWithData = getHeaderWithData(externalDataSourceDetails, dataSource.getConfig());

      if (headerWithData.size() > 1) {
        if (dataSource.getExternalDataId() == null) {
          Long externalDsId =
              MySqlOperations.insert(
                  externalDataSourceDetails,
                  HbaseUtility.getIdAnnotationForClass(ExternalDataSourceDetails.class));
          dataSource.setExternalDataId(externalDsId);
        } else {
          ExternalDataSourceService.updateExternalDataSourceDetails(
              externalDataSourceDetails, dataSource.getExternalDataId());
        }
      }
      logger.info("ExternalDataSourceDetails insert or update done");
      ExternalDataSourceService.saveExternalDataSource(
          externalDataSourceDetails.getDataSourceId(), true, dataSource, headerWithData);
      
      logger.info("sampleRecordExtractorForExternalData ends");
    }
  }

  /**
   * @param externalDataSourceDetails
   * @param config
   * @return
   * @throws JCoException
   */
  private static List<String> getHeaderWithData(
      ExternalDataSourceDetails externalDataSourceDetails, ConnectionConfig config)
      throws JCoException {
    // TODO Auto-generated method stub
    List<String> headerWithData = new ArrayList<>();
    logger.info("getHeaderWithData method starts");
    logger.debug("externalDataSourceDetails : "+externalDataSourceDetails +" , ConnectorConfig : "+config);
    if (externalDataSourceDetails.getConnectorType().equalsIgnoreCase(Constants.SAPERP)) {
      SapErpService sapErpService = new SapErpService();
      headerWithData = sapErpService.getFields(externalDataSourceDetails);
    }
    logger.info("getHeaderWithData method ends");
    return headerWithData;
  }

  private static String excelExtractor(DataSource dataSource) throws SystemException {
    XlsFileReader fileReader = new XlsFileReader();
    List<FieldMapping> fieldMappingsBk = dataSource.getFieldMappings();

    String readXlxs = "";
    try {
      dataSource.getFileDataIngesterDetails().setRecordStartLine(1L);
      readXlxs =
          fileReader.readXlxs(
              dataSource,
              dataSource.getFileDataIngesterDetails().getServerFilePath(),
              null,
              null,
              false,
              null);
      dataSource.getFileDataIngesterDetails().setDelimiter(GlobalParams.MAXIQ_DEL);

    } catch (Exception e) {
      return null;
    }

    List<FieldMapping> fieldMappings = dataSource.getFieldMappings();
   
    // @TODO
    //SampleRecordExtractor.checkIfFieldMappingChangesHappens(
      //  dataSource, fieldMappingsBk, fieldMappings);

    DataRepositoryDao.insertDataSource(dataSource);

    return readXlxs;
  }

  /**
   * This will take the Datasource object of data source type File and process the file and create
   * field mappings
   *
   * @param dataSource
   * @param local
   * @param localFilePath
   * @throws SystemException
   */
  private static void sampleRecordExtractorFile(DataSource dataSource, Boolean local)
      throws SystemException {

    if (null != dataSource) {

      FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();

      List<FieldMapping> fieldMappingsBkp = dataSource.getFieldMappings();

      String containsHeader = fileDataIngesterDetails.getContainsHeader();

      String sampleFileToBeFetched = null;

      String filePath = "";
      if (local) {
        filePath = fileDataIngesterDetails.getWebServerTempPath();
      } else {
        filePath = fileDataIngesterDetails.getServerFilePath();
      }

      File check = new File(filePath);
      checkPathIsValidOrNot(fileDataIngesterDetails.getHeaderTemplate(), filePath, check);

      if (FileUtilities.isDirectory(filePath)) {
        if (!filePath.trim().endsWith("/")) filePath = filePath + "/";

        List<String> files = FileUtilities.getFiles(filePath);

        if (null != files && files.size() > 0) sampleFileToBeFetched = files.get(0);

      } else {
        sampleFileToBeFetched = filePath;
      }

      List<String> sampleRecordsList =
          FileUtilities.readFile(sampleFileToBeFetched, 10, dataSource);

      String headerTemplate = fileDataIngesterDetails.getHeaderTemplate();
      Long headerLineNo = fileDataIngesterDetails.getHeaderStarting();

      String headerRecord = "";

      Boolean noHeaderInd = false;
    //check only if file contains header ,no need to check if headerTemplate in blank or not
      if (StringUtils.equalsIgnoreCase(containsHeader, "true")
				&& headerLineNo != null && headerLineNo > 0){
        headerRecord = FileUtilities.readNthLine(sampleFileToBeFetched, headerLineNo);
        headerTemplate="";
      }
      else {
        noHeaderInd = true;

        int index = 0;
        int size = sampleRecordsList.size();
        while (StringUtils.isBlank(headerRecord) && index < size) {
          headerRecord = StringUtils.upperCase(sampleRecordsList.get(index));
          index++;
        }
      }

      String[] headerRecordFields = null;

      if (fileDataIngesterDetails.getIsDelimiterRegEx() != null
          && fileDataIngesterDetails.getIsDelimiterRegEx()) {
        String delimiter = validateDelimiterRegex(fileDataIngesterDetails.getDelimiter());

        headerRecordFields = headerRecord.split(delimiter, -1);
      } else {
        if (dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
          headerRecordFields =
              getTokenisedStringArrayFromStringUsingDelimiter(
                  headerRecord, fileDataIngesterDetails.getDelimiter());
        } else {
          headerRecordFields =
              StringUtils.splitByWholeSeparatorPreserveAllTokens(
                  headerRecord, fileDataIngesterDetails.getDelimiter());
        }
      }

      Map<Integer, FieldMapping> fieldMapppingMap =
          generateFieldMappingFromHeader(
              headerRecordFields, headerTemplate, dataSource.getId(), noHeaderInd, dataSource);

      if (null != sampleRecordsList) {

        // If record start line is greater than 0, sample record must
        // have been taken from there onwards.
        Long recordStartLine = fileDataIngesterDetails.getRecordStartLine();
        if (recordStartLine == null) recordStartLine = 1L;

        if (StringUtils.equalsIgnoreCase(containsHeader, "true")
            && headerLineNo != null
            && headerLineNo > recordStartLine) recordStartLine = headerLineNo;

        int counter = 1;

        for (String sampleRecord : sampleRecordsList) {

          String[] fieldValues = null;

          if (null == fileDataIngesterDetails.getOptionallyEnclosedInDoubleQuotes()
              || false == fileDataIngesterDetails.getOptionallyEnclosedInDoubleQuotes()) {
            sampleRecord = StringUtils.replace(sampleRecord, "\"", "");
          }

          if (dataSource.getDataSourceType().equals(DataSourceType.FIXED_LENGTH_FORMAT)) {
            fieldValues = generateValuesForFixedLength(headerTemplate, sampleRecord);
          } else {
            if (fileDataIngesterDetails.getIsDelimiterRegEx() != null
                && fileDataIngesterDetails.getIsDelimiterRegEx()) {
              String delimiter = validateDelimiterRegex(fileDataIngesterDetails.getDelimiter());
              fieldValues = sampleRecord.split(delimiter, -1);
            } else {
              if (dataSource.getFileDataIngesterDetails().getOptionallyEnclosedInDoubleQuotes()) {
                fieldValues =
                    getTokenisedStringArrayFromStringUsingDelimiter(
                        sampleRecord, fileDataIngesterDetails.getDelimiter());
              } else {
                fieldValues =
                    StringUtils.splitByWholeSeparatorPreserveAllTokens(
                        sampleRecord, fileDataIngesterDetails.getDelimiter());
              }
            }
          }
          int position = 0;
          addSampleRecordToFieldMapping(fieldMapppingMap, counter, fieldValues, position);
          counter++;
        }

        List<FieldMapping> fieldMappings =
            generateFieldMappingFromMap(dataSource, headerTemplate, fieldMapppingMap);
        // @TODO
        //checkIfFieldMappingChangesHappens(dataSource, fieldMappingsBkp, fieldMappings);

        DataRepositoryDao.insertDataSource(dataSource);
      }
    }
  }

  public static String validateDelimiterRegex(String delimiter) {
    // TODO Auto-generated method stub
    Pattern validPattern = Pattern.compile("^[|$^]*.");
    Matcher m = validPattern.matcher(delimiter);
    if (m.matches()) {
      return Pattern.quote(delimiter);
    }
    return delimiter;
  }

  public static List<FieldMapping> generateFieldMappingFromMap(
      DataSource dataSource, String headerTemplate, Map<Integer, FieldMapping> fieldMapppingMap) {
    List<FieldMapping> fieldMappings = new ArrayList<>();

    for (Entry<Integer, FieldMapping> entry : fieldMapppingMap.entrySet()) {

      String string = outPutFieldValidation(entry.getValue());
      if (string != null) {
        entry.getValue().setFieldName(entry.getValue().getFieldName() + "_1");
      }
      fieldMappings.add(entry.getValue());
    }

    // To check if headerTemplate is exist or not.

    if (StringUtils.isBlank(headerTemplate)) fieldMappings = dataTypeGenerator(fieldMappings);

    dataSource.setFieldMappings(fieldMappings);
    return fieldMappings;
  }

  public static void addSampleRecordToFieldMapping(
      Map<Integer, FieldMapping> fieldMapppingMap,
      int counter,
      String[] fieldValues,
      int position) {
    for (String fieldValue : fieldValues) {
      if (fieldMapppingMap.containsKey(position)) {

        FieldMapping fieldMapping = fieldMapppingMap.get(position);

        List<String> rows = fieldMapping.getSampleRecords();
        rows.add(fieldValue);
        
        /*if (counter == 1) fieldMapping.setSampleRecord1(fieldValue);
        else if (counter == 2) fieldMapping.setSampleRecord2(fieldValue);
        else if (counter == 3) fieldMapping.setSampleRecord3(fieldValue);
        else if (counter == 4) fieldMapping.setSampleRecord4(fieldValue);
        else if (counter == 5) fieldMapping.setSampleRecord5(fieldValue);
        else if (counter == 6) fieldMapping.setSampleRecord6(fieldValue);
        else if (counter == 7) fieldMapping.setSampleRecord7(fieldValue);
        else if (counter == 8) fieldMapping.setSampleRecord8(fieldValue);
        else if (counter == 9) fieldMapping.setSampleRecord9(fieldValue);
        else if (counter == 10) fieldMapping.setSampleRecord10(fieldValue);*/
      }
      position++;
    }
  }

  /**
   * @param inputString
   * @param delimiter
   * @return Array of string tokens generated from input String over delimiter
   */
  public static String[] getTokenisedStringArrayFromStringUsingDelimiter(
      String inputString, String delimiter) {
    String[] tokenisedString = null;
    if (!delimiter.equals(null)) {
      if (1 == delimiter.length() || delimiter.equalsIgnoreCase("\\t")) {
        char delimiterInChar = getCharFromString(delimiter);
        //inputString = performPreOperations(inputString, delimiterInChar);
        CSVReader reader = new CSVReader(new StringReader(inputString), delimiterInChar);
        try {
          tokenisedString = reader.readNext();
          reader.close();

        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      } else {
        // If in case delimiter length is greater than 1
        tokenisedString =
            StringUtils.splitByWholeSeparatorPreserveAllTokens(inputString, delimiter);
      }
      if (tokenisedString == null) {
        inputString = StringUtils.replace(inputString, "\"", "");
        tokenisedString =
            StringUtils.splitByWholeSeparatorPreserveAllTokens(inputString, delimiter);
      }
    }
    return tokenisedString;
  }

  /**
   * @param delimiter
   * @return Char corresponding to InputString
   */
  public static char getCharFromString(String InputString) {
    char CharToReturn = '\0';
    if (1 == InputString.length()) {
      // all the delimiters such as , | and \t are handled here
      CharToReturn = InputString.charAt(0);
    } else if (InputString.equalsIgnoreCase("\\t")) {
      //extra check for \t
      CharToReturn = '\t';
    }
    return CharToReturn;
  }

  public static void checkPathIsValidOrNot(String headerTemplate, String filePath, File check)
      throws SystemException {
    if (!check.exists()) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_129", filePath);
    } else {
      if (check.length() <= 0 && StringUtils.isBlank(headerTemplate)) {
        String errorMessage = "File/Folder does not contains any data.";
        throw new SystemException(errorMessage);
      }
    }
  }

  public static String[] generateValuesForFixedLength(String headerTemplate, String sampleRecord) {

    List<String> fieldValues = new ArrayList<String>();
    String[] cloumns = StringUtils.split(headerTemplate, ',');

    Integer lastInteger = 0;
    for (String string : cloumns) {

      int startIndex = string.lastIndexOf("[");
      int lastIndex = string.lastIndexOf("]");

      Integer length = null;
      length = Integer.parseInt(StringUtils.substring(string, startIndex + 1, lastIndex));
      fieldValues.add(StringUtils.substring(sampleRecord, lastInteger, (lastInteger + length)));
      lastInteger = lastInteger + length;
    }

    return fieldValues.toArray(new String[fieldValues.size()]);
  }

  public static Map<Integer, FieldMapping> generateFieldMappingFromHeader(
      String[] headerRecordFields,
      String headerTemplate,
      Long dsId,
      Boolean noHeaderInd,
      DataSource dataSource)
      throws SystemException {

    Map<Integer, FieldMapping> fieldMapppingMap = new HashMap<Integer, FieldMapping>();

    HashMap<String, Integer> fieldNameCounterMap = new HashMap<String, Integer>();
    fieldNameCounterMap.put(HardCodeServices.DEFAULT_FIELDNAME, 1);

    FieldMappingFactory factory = new FieldMappingFactory();

    int position = 0;

    if (StringUtils.isNotBlank(headerTemplate)) {
      String[] columns = StringUtils.splitPreserveAllTokens(headerTemplate, ",");

      Set<String> templateColumnNamesSet = new HashSet<String>();

      for (String column : columns) {

        if (null != column && column.contains("decimal(")) {
          ExceptionsMessanger.throwException(new SystemException(), "ERR_178");
        }

        int typeStartsAt = StringUtils.indexOf(column, "[");
        int typeEndsAt = StringUtils.indexOf(column, "]");

        if (typeStartsAt == -1
            || typeEndsAt == -1
            || typeStartsAt >= typeEndsAt
            || typeStartsAt == 0)
          ExceptionsMessanger.throwException(new SystemException(), "ERR_107");

        String fieldName = StringUtils.upperCase(StringUtils.mid(column, 0, typeStartsAt));

        fieldName = avoidNuisanceInFieldName(fieldName);

        String dataType =
            StringUtils.trim(
                StringUtils.upperCase(
                    StringUtils.mid(column, typeStartsAt + 1, typeEndsAt - typeStartsAt - 1)));

        FieldDataTypes type = null;
        try {
          type = FieldDataTypes.valueOf(dataType);
        } catch (IllegalArgumentException e) {
          ExceptionsMessanger.throwException(new SystemException(), "ERR_108", null);
        }

        if (templateColumnNamesSet.contains(fieldName))
          ExceptionsMessanger.throwException(new SystemException(), "ERR_109", null);

        if (fieldName.toLowerCase().indexOf("gp_") == 0)
          ExceptionsMessanger.throwException(new SystemException(), "ERR_110", fieldName);

        templateColumnNamesSet.add(fieldName);

        FieldMapping fieldMapping =
            factory.getFieldMapping(
                dsId, position, fieldName, Long.parseLong(String.valueOf(position + 1)));
        fieldMapping.setFieldDataType(type);

        fieldMapppingMap.put(position, fieldMapping);

        position++;
      }
    } else if (headerRecordFields != null && headerRecordFields.length > 0) {

      for (String column : headerRecordFields) {

        String newFieldName = column;

        if (noHeaderInd) {
          newFieldName = HardCodeServices.DEFAULT_FIELDNAME + position;
        } else {
          column = StringUtils.replace(column, " ", "");

          if (StringUtils.startsWithIgnoreCase(column, "gp_"))
            column = StringUtils.replaceOnce(column, "gp_", "");

          column = checkStringContainSpecialCharcter(column);

          column = avoidNuisanceInFieldName(column);

          if (StringUtils.isBlank(column)) column = HardCodeServices.DEFAULT_FIELDNAME;

          if (!fieldNameCounterMap.containsKey(column.toUpperCase()))
            fieldNameCounterMap.put(column.toUpperCase(), 1);
          else
            fieldNameCounterMap.put(
                column.toUpperCase(), fieldNameCounterMap.get(column.toUpperCase()) + 1);

          if (fieldNameCounterMap.get(column.toUpperCase()) > 1)
            newFieldName = column + (fieldNameCounterMap.get(column.toUpperCase()) - 1);
          else newFieldName = column;
        }

        FieldMapping fieldMapping =
            factory.getFieldMapping(
                dsId, position, newFieldName, Long.parseLong(String.valueOf(position + 1)));

        fieldMapppingMap.put(position, fieldMapping);

        position++;
      }
    }

    return fieldMapppingMap;
  }

  public static List<FieldMapping> dataTypeGenerator(List<FieldMapping> fields) {

    if (fields != null && fields.size() > 0) {
      for (FieldMapping field : fields) {

        if (!checkTimeStamp(field)) {
          if (!checkDate(field)) {
            if (!checkInteger(field)) {
              if (!checkNumeric(field)) {
                if (!checkDouble(field)) {
                  if (!checkBoolean(field)) {
                    field.setFieldDataType(FieldDataTypes.STRING);
                  }
                }
              }
            }
          }
        }
      }
    }

    return fields;
  }

  public static String checkStringContainSpecialCharcter(String toCheck) {

    if (toCheck != null && !toCheck.equals("")) {
      Matcher m = Pattern.compile("[\\W]").matcher(toCheck);
      while (m.find()) {
        String group = m.group();
        if (group.equals("_")) {
          continue;
        } else {
          toCheck = toCheck.replace(group, "");
        }
      }
      toCheck = toCheck.replace("__", "_");
    }

    return toCheck;
  }

  public static Boolean checkTimeStamp(FieldMapping field) {

    Map<String, Integer> formatCounter = getSuitableFromats(field, FieldDataTypes.TIMESTAMP);

    if (formatCounter.size() > 0) {
      String maxFormat = "";
      Integer max = 0;

      for (Entry<String, Integer> e : formatCounter.entrySet()) {
        String format = e.getKey();
        Integer counter = e.getValue();

        if (counter > max) {
          max = counter;
          maxFormat = format;
        }
      }

      if (StringUtils.isNotBlank(maxFormat)) {
        field.setFormat(maxFormat);
        field.setFieldDataType(FieldDataTypes.TIMESTAMP);
        return true;
      }
    }

    if (field.getFieldDataType() == FieldDataTypes.TIMESTAMP) {
      return true; // If header template says it is a TimeStamp field, then it
      // is, even if there is no valid dates found
    }

    return false;
  }

  public static Boolean checkDate(FieldMapping field) {

    Map<String, Integer> formatCounter = getSuitableFromats(field, FieldDataTypes.DATE);

    if (formatCounter.size() > 0) {
      String maxFormat = "";
      Integer max = 0;

      for (Entry<String, Integer> e : formatCounter.entrySet()) {
        String format = e.getKey();
        Integer counter = e.getValue();

        if (counter > max) {
          max = counter;
          maxFormat = format;
        }
      }

      if (StringUtils.isNotBlank(maxFormat)) {
        field.setFormat(maxFormat);
        field.setFieldDataType(FieldDataTypes.DATE);
        return true;
      }
    }

    if (field.getFieldDataType() == FieldDataTypes.DATE) {
      return true; // If header template says it is a date field, then it
      // is, even if there is no valid dates found
    }

    return false;
  }

  public static Map<String, Integer> getSuitableFromats(FieldMapping field, FieldDataTypes type) {

    List<String> allPossibleFormats = new ArrayList<String>();

    /*allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord1(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord2(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord3(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord4(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord5(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord6(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord7(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord8(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord9(), type));
    allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(field.getSampleRecord10(), type));*/
    
    List<String> sampleN = field.getSampleRecords();
    for(String sampleData : sampleN) {
    	allPossibleFormats.addAll(DateUtils.findMyDateAndTSFormat(sampleData, type));
    }

    Map<String, Integer> formatCounter = new HashMap<String, Integer>();
    if (allPossibleFormats.size() > 0) {
      for (String format : allPossibleFormats) {
        if (!formatCounter.containsKey(format)) {
          formatCounter.put(format, 0);
        }
        formatCounter.put(format, formatCounter.get(format) + 1);
      }
    }

    return formatCounter;
  }

  public static Boolean checkDouble(FieldMapping field) {

    Integer counter = 0;
    Integer nonBlank = 0;

/*    if (StringUtils.isNotBlank(field.getSampleRecord1())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord2())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord3())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord4())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord5())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord6())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord7())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord8())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord9())) nonBlank++;
    if (StringUtils.isNotBlank(field.getSampleRecord10())) nonBlank++;*/

    List<String> rows = field.getSampleRecords();
    for(String rowData : rows) {
    	if (StringUtils.isNotBlank(rowData)) nonBlank++;
    }
    
    if (nonBlank == 0) return false;

 /*   if (checkDouble(field.getSampleRecord1())) counter++;
    if (checkDouble(field.getSampleRecord2())) counter++;
    if (checkDouble(field.getSampleRecord3())) counter++;
    if (checkDouble(field.getSampleRecord4())) counter++;
    if (checkDouble(field.getSampleRecord5())) counter++;
    if (checkDouble(field.getSampleRecord6())) counter++;
    if (checkDouble(field.getSampleRecord7())) counter++;
    if (checkDouble(field.getSampleRecord8())) counter++;
    if (checkDouble(field.getSampleRecord9())) counter++;
    if (checkDouble(field.getSampleRecord10())) counter++;*/

    for(String rowData : rows) {
    	if (checkDouble(rowData)) counter++;
    }
    
    if (nonBlank == counter) {
      field.setFieldDataType(FieldDataTypes.DOUBLE);
      return true;
    }

    return false;
  }

  public static Boolean checkInteger(FieldMapping field) {

    Integer counter = 0;
    Integer nonBlank = 0;

/*    if (StringUtils.isNotBlank(field.getSampleRecord1())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord2())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord3())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord4())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord5())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord6())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord7())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord8())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord9())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord10())) nonBlank++;*/

    List<String> rows = field.getSampleRecords();
    for(String rowData : rows) {
    	if (StringUtils.isNotBlank(rowData)) nonBlank++;
    }
    
    if (nonBlank == 0) return false;

/*    if (checkInteger(field.getSampleRecord1())) counter++;

    if (checkInteger(field.getSampleRecord2())) counter++;

    if (checkInteger(field.getSampleRecord3())) counter++;

    if (checkInteger(field.getSampleRecord4())) counter++;

    if (checkInteger(field.getSampleRecord5())) counter++;

    if (checkInteger(field.getSampleRecord6())) counter++;

    if (checkInteger(field.getSampleRecord7())) counter++;

    if (checkInteger(field.getSampleRecord8())) counter++;

    if (checkInteger(field.getSampleRecord9())) counter++;

    if (checkInteger(field.getSampleRecord10())) counter++;*/

    
    for(String rowData : rows) {
    	if (checkInteger(rowData)) counter++;
    }
    
    if (nonBlank == counter) {
      field.setFieldDataType(FieldDataTypes.INTEGER);
      return true;
    }

    return false;
  }

  public static Boolean checkNumeric(FieldMapping field) {

    Integer counter = 0;
    Integer nonBlank = 0;

   /* if (StringUtils.isNotBlank(field.getSampleRecord1())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord2())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord3())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord4())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord5())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord6())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord7())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord8())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord9())) nonBlank++;

    if (StringUtils.isNotBlank(field.getSampleRecord10())) nonBlank++;*/

    List<String> rows = field.getSampleRecords();
    for(String rowData : rows) {
    	if (StringUtils.isNotBlank(rowData)) nonBlank++;
    }
    
    if (nonBlank == 0) return false;

   /* if (checkNumeric(field.getSampleRecord1())) counter++;

    if (checkNumeric(field.getSampleRecord2())) counter++;

    if (checkNumeric(field.getSampleRecord3())) counter++;

    if (checkNumeric(field.getSampleRecord4())) counter++;

    if (checkNumeric(field.getSampleRecord5())) counter++;

    if (checkNumeric(field.getSampleRecord6())) counter++;

    if (checkNumeric(field.getSampleRecord7())) counter++;

    if (checkNumeric(field.getSampleRecord8())) counter++;

    if (checkNumeric(field.getSampleRecord9())) counter++;

    if (checkNumeric(field.getSampleRecord10())) counter++;*/

    for(String rowData : rows) {
    	if (checkNumeric(rowData)) counter++;
    }
    
    if (nonBlank == counter) {
      field.setFieldDataType(FieldDataTypes.LONG);
      return true;
    }

    return false;
  }

  private static boolean checkDouble(String value) {

    try {
      Double.valueOf(value);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean checkNumeric(String value) {
    try {
      Long.valueOf(value);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean checkInteger(String value) {
    try {
      Integer.valueOf(value);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public static Boolean validateDataType(String value, FieldDataTypes type, String formatStr) {

    if (FieldDataTypes.BOOLEAN.equals(type)) return checkBoolean(value);
    else if (FieldDataTypes.LONG.equals(type)) return checkNumeric(value);
    else if (FieldDataTypes.INTEGER.equals(type)) return checkInteger(value);
    else if (FieldDataTypes.DOUBLE.equals(type)) return checkDouble(value);
    else if (FieldDataTypes.DATE.equals(type)) {
      try {
        return DateUtils.validateFormat(new SimpleDateFormat(formatStr), value);
      } catch (Exception e) {
        return false;
      }
    } else if (FieldDataTypes.TIMESTAMP.equals(type)) {
      try {
        return DateUtils.validateFormat(new SimpleDateFormat(formatStr), value);
      } catch (Exception e) {
        return false;
      }
    }

    return true;
  }

  public static boolean checkBoolean(FieldMapping field) {

    //if (checkBoolean(field.getSampleRecord1())) {
if (checkBoolean(field.getSampleRecords().get(0))) {
      field.setFieldDataType(FieldDataTypes.BOOLEAN);
      return true;
    }

    return false;
  }

  private static boolean checkBoolean(String value) {
    if (StringUtils.equalsIgnoreCase(value, "true") || StringUtils.equalsIgnoreCase(value, "false"))
      return true;
    else return false;
  }

  public static String avoidNuisanceInFieldName(String fieldName) {

    String newName = "";

    if (StringUtils.isNotBlank(fieldName)) {
      newName = fieldName.replaceAll("[^a-zA-z_0-9]", "");
      newName = newName.replaceAll("^[0-9_]{0,}", "");
    }
    return newName;
  }

  public static String outPutFieldValidation(FieldMapping fieldMappin) {
    HashSet<String> hashSet = GlobalParams.keyWords;
    if (hashSet.contains(fieldMappin.getFieldName().toUpperCase())) {
      return ExceptionMessages.KEYWORDS + " " + fieldMappin.getFieldName();
    }
    return null;
  };

  private static String performPreOperations(String inputString, char delimiter) {
    if (inputString.contains("\"")) {
      for (int i = 1; i < inputString.length() - 1; i++) {
        //if " is in between two delimiters
        if ('\"' == inputString.charAt(i)) {
          if (delimiter != inputString.charAt(i - 1)
              && delimiter != inputString.charAt(i + 1)
              && '\"' != inputString.charAt(i - 1)) {
            //replace \" with \"\"
            inputString = inputString.substring(0, i) + "\"\"" + inputString.substring(i + 1);
          }
        }
      }
    }
    return inputString;
  }

  /*public static void checkIfFieldMappingChangesHappens(
      DataSource dataSource, List<FieldMapping> fieldMappingsBkp, List<FieldMapping> fieldMappings)
      throws SystemException {
    boolean isSchemaChanged =
        CommunicationDetailsBSO.verifyCurrentDataSourceFieldMappingWithExisting(
            fieldMappings, fieldMappingsBkp);

    logger.info("Is field Mapping schema has changes :" + isSchemaChanged);

    if (isSchemaChanged) {

      String uuid =
          CommunicationDetailsBSO.saveCommunicationDetails(
              dataSource.getFileDataIngesterDetails(),
              dataSource.getFieldMappings(),
              Constants.FIELDMAPPING_WARNING);
      logger.info(ParamUtils.getString(Constants.FIELDMAPPING_WARNING + "$$" + uuid));
      ExceptionsMessanger.throwException(
          new SystemException(), "ERR_134", ParamUtils.getString(uuid));
    }
  }*/

  public static void sampleRecordExtractorForRemoteData(DataSource dataSource, Boolean local)
      throws SystemException, ConnectionException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + "sampleRecordExtractorForRemoteData >> " + dataSource);
    FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();
    List<FieldMapping> fieldMappingsBkp = null;
    if (dataSource != null) {
      fieldMappingsBkp = dataSource.getFieldMappings();
    }
    Map<Integer, FieldMapping> fieldMappingMap =
        generateFieldMappingFromHeader(
            null,
            fileDataIngesterDetails.getHeaderTemplate(),
            dataSource.getId(),
            false,
            dataSource);
    List<FieldMapping> fieldMappings =
        generateFieldMappingFromMap(
            dataSource, fileDataIngesterDetails.getHeaderTemplate(), fieldMappingMap);
    
    //  @TODO
    //checkIfFieldMappingChangesHappens(dataSource, fieldMappingsBkp, fieldMappings);
    
    dataSource.getFileDataIngesterDetails().setDelimiter(GlobalParams.MAXIQ_DEL);
    DataSourceInstance dataSourceInstance =
        DataSourceInstanceDao.getDataSourceInstance(dataSource.getId());
    dataSourceInstance.setDataSourceType(DataSourceType.REMOTE_DATA.name());
    DataRepositoryDao.insertDataSource(dataSource);
    MySqlOperations.insert(dataSourceInstance);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + "sampleRecordExtractorForRemoteData << " + dataSource);
  }
}

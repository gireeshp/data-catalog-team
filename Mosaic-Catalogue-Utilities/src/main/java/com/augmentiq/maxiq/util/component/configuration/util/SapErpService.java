package com.augmentiq.maxiq.util.component.configuration.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.StackTraceReader;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.CompressionTypeEnum;
import com.augmentiq.maxiq.constant.configuration.enums.DataSourceType;
import com.augmentiq.maxiq.constant.configuration.enums.FileTypeEnum;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.ExternalDataSourceDetails;
import com.augmentiq.maxiq.entity.model.configuration.bean.FileDataIngesterDetails;
import com.augmentiq.maxiq.entity.model.configuration.bean.SapErp;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryDao;
import com.augumentiq.maxiq.configuration.dao.DataRepositoryFetchWithChild;
import com.augumentiq.maxiq.configuration.dao.DataSourceInstanceDao;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoRepository;
import com.sap.conn.jco.JCoTable;
import com.sap.conn.jco.ext.DestinationDataProvider;
import com.sforce.ws.ConnectorConfig;

public class SapErpService {

  private static final Logger logger = LoggerFactory.getLogger(SapErpService.class);
  private static JCoDestination destination = null;
  private static String DESTINATION_NAME1 = "mySAPSystem";

 /* public DataSource saveDataSourceDetails(
      Long dataSourceId,
      SapErp sapErp,
      Boolean containsHeader,
      ExternalDataSourceDetails externalDSDetails)
      throws Exception {
    // TODO Auto-generated method stub

    logger.info("SapErp saveDataSourceDetails method starts");

    externalDSDetails.setDelimeter(CHAR_CONSTANTS.PIPE);

    DataSource dataSource = DataRepositoryDao.getDataSource(dataSourceId);

    saveFileDataIngesterDetailsForDataSource(dataSource, externalDSDetails, sapErp);

    DataRepositoryDao.insertDataSource(dataSource);

    String id = MessageHandler.generateId();

    MessageHandler.setMessage(id, false);
    MessageHandler.executeForOthers(id, dataSourceId + "", MessageTypes.SAMPLE_RECORD2);

    String msg = MessageHandler.getError(id);
    MessageHandler.remove(id);

    if (StringUtils.isNotBlank(msg)) {
      throw new SystemException(msg);
    }

    dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceId);

    logger.info("SapErp saveDataSourceDetails method ends");

    return dataSource;
  }
*/
 /* private void saveFileDataIngesterDetailsForDataSource(
      DataSource dataSource, ExternalDataSourceDetails externalDSDetails, SapErp sapErp)
      throws Exception {
    // TODO Auto-generated method stub

    FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();

    dataSource.setDataSourceType(DataSourceType.EXTERNAL_DATA);
    if (externalDSDetails.getId() == null) externalDSDetails.setId(null);

    dataSource.setExternalDataId(externalDSDetails.getId());
    ConnectorConfig config = new ConnectorConfig();
    if (null != dataSource.getConfig()) {
      config = dataSource.getConfig();
    }

    config.setInputFileType(FileTypeEnum.TEXT.name());
    config.setInputCompressionType(CompressionTypeEnum.UNCOMPRESSED.name());

    // update Connection From Connections in DS
    List<ConnectorConfig> conn = new ConfigurationCreateService().getConn();
    if (conn != null) {
      for (ConnectorConfig connectorConfig : conn) {

        if (connectorConfig.getConncetionName().equals(sapErp.getConnectionName())) {
          sapErp.setHostName(connectorConfig.getSapJcoHostName());
          sapErp.setInstanceNo(connectorConfig.getSapJcoInstanceNo());
          sapErp.setClient(connectorConfig.getSapJcoClient());
          sapErp.setUser(connectorConfig.getSapJcoUser());
          sapErp.setPassword(connectorConfig.getSapJcoPasswd());
          sapErp.setLanguage(connectorConfig.getSapJcoLang());
        }
      }

      config.setConncetionName(sapErp.getConnectionName());
      config.setSapJcoHostName(sapErp.getHostName());
      config.setSapJcoInstanceNo(sapErp.getInstanceNo());
      config.setSapJcoClient(sapErp.getClient());
      config.setSapJcoUser(sapErp.getUser());
      config.setSapJcoPasswd(sapErp.getPassword());
      config.setSapJcoLang(sapErp.getLanguage());

      dataSource.setConfig(config);
      dataSource.setExternalDataType(GENERAL_CONSTANTS.ExternalConnector.SAP_ERP);

      if (fileDataIngesterDetails == null) fileDataIngesterDetails = new FileDataIngesterDetails();

      fileDataIngesterDetails.setContainsHeader(true + "");
      fileDataIngesterDetails.setDelimiter(externalDSDetails.getDelimeter());
      fileDataIngesterDetails.setHeaderStarting(1L);
      fileDataIngesterDetails.setRecordStartsWith(false);
      fileDataIngesterDetails.setRecordStartLine(2L);
      fileDataIngesterDetails.setDataSourceType(DataSourceType.EXTERNAL_DATA);
      fileDataIngesterDetails.setServerFilePath(
          Cache.getProperty(CacheConstants.UPLOAD_PATH) + System.currentTimeMillis());
      externalDSDetails.setStoragePath(fileDataIngesterDetails.getServerFilePath());
      fileDataIngesterDetails.setDataSourceId(externalDSDetails.getDataSourceId());
      dataSource.setFileDataIngesterDetails(fileDataIngesterDetails);

      //save ExternalDataSourceDetails to ds
      dataSource.setExternalDataSourceDetails(externalDSDetails);

      Map<String, String> updatedValue = new HashMap<String, String>();
      updatedValue.put("dataSourceType", fileDataIngesterDetails.getDataSourceType().toString());
      DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(
          fileDataIngesterDetails.getDataSourceId(), updatedValue);
    }
  }*/

 /* public static DataSource getDataSourceWithBasicDetails(
      Long dataSourceId, Boolean indicator, ExternalDataSourceDetails eDetails)
      throws SystemException {
    DataSource dataSource = DataRepositoryFetchWithChild.getDataSource(dataSourceId);
    FileDataIngesterDetails fileDataIngesterDetails = dataSource.getFileDataIngesterDetails();

    dataSource.setDataSourceType(DataSourceType.EXTERNAL_DATA);
    if (eDetails.getId() == null) eDetails.setId(null);

    dataSource.setExternalDataId(eDetails.getId());
    ConnectorConfig config = null;
    if (null != dataSource.getConfig()) {
      config = dataSource.getConfig();
    }

    if (null == config) {
      config = new ConnectorConfig();
    }

    config.setInputFileType(FileTypeEnum.TEXT.name());
    config.setInputCompressionType(CompressionTypeEnum.UNCOMPRESSED.name());

    dataSource.setConfig(config);

    if (fileDataIngesterDetails == null) fileDataIngesterDetails = new FileDataIngesterDetails();

    fileDataIngesterDetails.setContainsHeader(indicator + "");
    fileDataIngesterDetails.setDelimiter(eDetails.getDelimeter());
    fileDataIngesterDetails.setHeaderStarting(1L);
    fileDataIngesterDetails.setRecordStartsWith(false);
    fileDataIngesterDetails.setRecordStartLine(2L);
    fileDataIngesterDetails.setDataSourceType(DataSourceType.EXTERNAL_DATA);
    fileDataIngesterDetails.setServerFilePath(
        Cache.getProperty(CacheConstants.UPLOAD_PATH) + System.currentTimeMillis());
    eDetails.setStoragePath(fileDataIngesterDetails.getServerFilePath());
    fileDataIngesterDetails.setDataSourceId(dataSourceId);
    dataSource.setFileDataIngesterDetails(fileDataIngesterDetails);

    Map<String, String> updatedValue = new HashMap<String, String>();
    updatedValue.put("dataSourceType", fileDataIngesterDetails.getDataSourceType().toString());
    DataSourceInstanceDao.updateDataRepoIdIntoDataSourceInstance(
        fileDataIngesterDetails.getDataSourceId(), updatedValue);

    if (dataSource.getExternalDataId() == null) {
      Long externalDsId =
          MySqlOperations.insert(
              eDetails, HbaseUtility.getIdAnnotationForClass(ExternalDataSourceDetails.class));
      dataSource.setExternalDataId(externalDsId);
    } else {
      ExternalDataSourceService.updateExternalDataSourceDetails(
          eDetails, dataSource.getExternalDataId());
    }

    return dataSource;
  }*/

  public List<String> getFields(ExternalDataSourceDetails externalDataSourceDetails)
      throws JCoException {
    // TODO Auto-generated method stub
    logger.info("getFields method of saperp started");
    SapErp sapErp = externalDataSourceDetails.getSapErp();

    String functionModule = sapErp.getFunctionModule();
    String tableName = sapErp.getTableName();
    String rowCount = "10";
    String filter = sapErp.getDataFilter();
    List<String> columnNames = sapErp.getColumnName();

    List<String> fields = new ArrayList<String>();

    logger.info("Sap connection Started");

    JCoDestination connection = getConnection(sapErp);

    logger.info("Sap connection Success");
    JCoRepository repository = connection.getRepository();

    JCoFunction function = repository.getFunction(functionModule);

    if (function == null) throw new RuntimeException("Function Module Not found in SAP.");

    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.QUERY_TABLE, tableName);
    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.DELIMITER, CHAR_CONSTANTS.COMMA);
    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.ROW_COUNT, rowCount);

    JCoTable table =
        function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.FIELDS);
    if (columnNames != null && columnNames.size() > 0) {

      for (String columnName : columnNames) {
        table.appendRow();
        table.setValue(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME, columnName);
      }
    } else {
      for (int i = 0; i < table.getNumRows(); i++) {
        table.appendRow();
        table.setValue(
            GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME,
            table.getString(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME));
      }
    }

    if (filter != null && filter != "") {
      List<String> elementList = Arrays.asList(filter.split("\\|"));
      JCoTable table1 =
          function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.OPTIONS);
      for (String data : elementList) {

        table1.appendRow();
        table1.setValue(GENERAL_CONSTANTS.ExternalConnector.TEXT, data);
      }
    }

    function.execute(destination);

    final JCoTable codes =
        function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.FIELDS);

    String header = "";
    for (int i = 0; i < codes.getNumRows(); i++) {
      codes.setRow(i);
      header +=
          codes.getString(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME) + CHAR_CONSTANTS.PIPE;
    }
    logger.info("header ---->>" + header);
    header = header.substring(0, header.lastIndexOf(CHAR_CONSTANTS.PIPE));
    
    fields.add(header); // adding headers to field list

    final JCoTable rows =
        function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.SAP_DATA);

    String dataString = "";

    for (int i = 0; i < rows.getNumRows(); i++) {
      rows.setRow(i);

      String rowData = rows.getString(GENERAL_CONSTANTS.ExternalConnector.WA);

      List<String> elementList = Arrays.asList(rowData.split("\\,"));

      for (String data : elementList) {
        dataString += data.trim() + CHAR_CONSTANTS.PIPE;
      }

      dataString = dataString.substring(0, dataString.lastIndexOf(CHAR_CONSTANTS.PIPE));

      fields.add(dataString);

      dataString = ""; // make dataString empty for new row
    }
    logger.info("getFields method of saperp ends");
    return fields;
  }

  /**
   * @param sapErp
   * @return destination
   */
  public static JCoDestination getConnection(SapErp sapErp) {
    // TODO Auto-generated method stub
    if (null == destination) {
      try {
        logger.info("initConnection Starting : ");
        initConnection(sapErp);
        logger.info("initConnection complete : ");
        destination = JCoDestinationManager.getDestination(DESTINATION_NAME1);

        destination.ping();

      } catch (JCoException e) {
        logger.error("get destination after ping : " + e.getMessage());
        logger.error("get destination after ping : " + destination);
      }
    }
    logger.info("Connection return : " + destination);
    return destination;
  }

  /** @param sapErp */
  private static void initConnection(SapErp sapErp) {
    // TODO Auto-generated method stub

    Properties connectProperties = new Properties();
    connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, sapErp.getHostName());
    connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR, sapErp.getInstanceNo());
    connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, sapErp.getClient());
    connectProperties.setProperty(DestinationDataProvider.JCO_USER, sapErp.getUser());
    connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, sapErp.getPassword());
    connectProperties.setProperty(DestinationDataProvider.JCO_LANG, sapErp.getLanguage());
    createDestinationDataFile(DESTINATION_NAME1, connectProperties);
    logger.info("Destination created successfully");
  }

  public static JCoDestination getConnection(Map<String, String> confProperties) {
    // This will use that destination file to connect to SAP

    if (null == destination) {
      try {
        logger.info("initConnection Starting : ");
        initConnection(confProperties);
        logger.info("initConnection complete : ");
        destination = JCoDestinationManager.getDestination(DESTINATION_NAME1);

        destination.ping();

      } catch (JCoException e) {
        logger.error("get destination after ping : " + e.getMessage());
        logger.error("get destination after ping : " + destination);
      }
    }
    logger.info("Connection return : " + destination);
    return destination;
  }

  private static void initConnection(Map<String, String> confProperties) {

    String hostName = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.SAP_HOST_NAME);
    String instanceNo = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.SAP_INSTANCE_NO);
    String client = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.SAP_CLIENT);
    String user = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.SAP_USER);
    String password = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.SAP_PASSWORD);
    String language = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.SAP_LANGUAGE);

    Properties connectProperties = new Properties();
    connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, hostName);
    connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR, instanceNo);
    connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, client);
    connectProperties.setProperty(DestinationDataProvider.JCO_USER, user);
    connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, password);
    connectProperties.setProperty(DestinationDataProvider.JCO_LANG, language);

    createDestinationDataFile(DESTINATION_NAME1, connectProperties);
  }

  private static void createDestinationDataFile(
      String destinationName, Properties connectProperties) {

    File destCfg = new File(destinationName + ".jcoDestination");
    try {
      FileOutputStream fos = new FileOutputStream(destCfg, false);
      connectProperties.store(fos, "for tests only !");
      logger.info("create Destination Data File done");
      fos.close();
    } catch (Exception e) {
      logger.error(StackTraceReader.stringFromStackTrace(e));
      throw new RuntimeException("Unable to create the destination files", e);
    }
  }

  public List<String> getCustomColumns(ExternalDataSourceDetails externalDSDetails)
      throws JCoException {
    // TODO Auto-generated method stub
    SapErp sapErp = externalDSDetails.getSapErp();

    List<String> fields = new ArrayList<String>();

    Map<String, String> param = new HashMap<String, String>();

    param.put(GENERAL_CONSTANTS.ExternalConnector.FUNCTION_MODULE, sapErp.getFunctionModule());
    param.put(GENERAL_CONSTANTS.ExternalConnector.TABLE_NAME, sapErp.getTableName());
    param.put(GENERAL_CONSTANTS.ExternalConnector.ROW_COUNT, sapErp.getRowCount());
    param.put(GENERAL_CONSTANTS.ExternalConnector.DATA_FILTER, sapErp.getDataFilter());
    param.put(GENERAL_CONSTANTS.CONNECTIONNAME, sapErp.getConnectionName());
    param.put(GENERAL_CONSTANTS.STORAGEPATH, externalDSDetails.getStoragePath());
    param.put(GENERAL_CONSTANTS.ExternalConnector.SAP_HOST_NAME, sapErp.getHostName());
    param.put(GENERAL_CONSTANTS.ExternalConnector.SAP_INSTANCE_NO, sapErp.getInstanceNo());
    param.put(GENERAL_CONSTANTS.ExternalConnector.SAP_CLIENT, sapErp.getClient());
    param.put(GENERAL_CONSTANTS.ExternalConnector.SAP_USER, sapErp.getUser());
    param.put(GENERAL_CONSTANTS.ExternalConnector.SAP_PASSWORD, sapErp.getPassword());
    param.put(GENERAL_CONSTANTS.ExternalConnector.SAP_LANGUAGE, sapErp.getLanguage());

    String functionModule = param.get(GENERAL_CONSTANTS.ExternalConnector.FUNCTION_MODULE);
    String tableName = param.get(GENERAL_CONSTANTS.ExternalConnector.TABLE_NAME);

    JCoDestination connection = getConnection(param);

    JCoRepository repository = connection.getRepository();

    JCoFunction function = repository.getFunction(functionModule);

    if (function == null) throw new RuntimeException("Function Module Not found in SAP.");

    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.QUERY_TABLE, tableName);
    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.DELIMITER, CHAR_CONSTANTS.COMMA);
    function
        .getImportParameterList()
        .setValue(
            GENERAL_CONSTANTS.ExternalConnector.ROW_COUNT,
            GENERAL_CONSTANTS.ExternalConnector.ROW_CONSTANT);
    function
        .getImportParameterList()
        .setValue(
            GENERAL_CONSTANTS.ExternalConnector.NO_DATA,
            GENERAL_CONSTANTS.ExternalConnector.NO_DATA_CONSTANT);

    function.execute(destination);

    final JCoTable codes =
        function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.FIELDS);

    String header = "";
    for (int i = 0; i < codes.getNumRows(); i++) {
      codes.setRow(i);
      header = codes.getString(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME);
      fields.add(header);
    }

    return fields;
  }

  @Deprecated
  public void getData(SapErp sapErp, Map<String, String> confProperties)
      throws JCoException, IOException {
    // TODO Auto-generated method stub
    List<String> columnNames = sapErp.getColumnName();
    boolean selectedColumn = sapErp.isSelectColumn();
    String functionModule = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.FUNCTION_MODULE);
    String tableName = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.TABLE_NAME);
    String rowCount = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.ROW_COUNT);
    String filter = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.DATA_FILTER);
    String outFileLocation = confProperties.get(GENERAL_CONSTANTS.STORAGEPATH);

    File tempDirInWebServer = new File(outFileLocation);
    if (!tempDirInWebServer.exists()) {
      tempDirInWebServer.mkdirs();
    }

    File file = new File(outFileLocation + "/SapErpSample.txt");
    if (!file.exists()) {
      file.createNewFile();
    }

    FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
    logger.info("Sap connection Started");

    JCoDestination connection = getConnection(confProperties);

    logger.info("Sap connection Done");
    JCoRepository repository = connection.getRepository();

    JCoFunction function = repository.getFunction(functionModule);

    if (function == null) throw new RuntimeException("Function Module Not found in SAP.");

    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.QUERY_TABLE, tableName);
    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.DELIMITER, CHAR_CONSTANTS.COMMA);
    function
        .getImportParameterList()
        .setValue(GENERAL_CONSTANTS.ExternalConnector.ROW_COUNT, rowCount);

    if (columnNames != null && columnNames.size() > 0 && selectedColumn == true) {
      JCoTable table =
          function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.FIELDS);
      for (String columnName : columnNames) {
        table.appendRow();
        table.setValue(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME, columnName);
      }
    }

    if (filter != null && filter != "") {
      List<String> elephantList = Arrays.asList(filter.split("\\|"));
      JCoTable table1 =
          function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.OPTIONS);
      for (String data : elephantList) {
        table1.appendRow();
        table1.setValue(GENERAL_CONSTANTS.ExternalConnector.TEXT, data);
      }
    }

    function.execute(destination);

    final JCoTable codes =
        function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.FIELDS);

    String header = "";
    for (int i = 0; i < codes.getNumRows(); i++) {
      codes.setRow(i);
      header +=
          codes.getString(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME) + GlobalParams.MAXIQ_DEL;
    }
    header = header.substring(0, header.lastIndexOf(GlobalParams.MAXIQ_DEL));

    fileWriter.write(header); // write file headers
    logger.info("header info - " + header);

    final JCoTable rows =
        function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.SAP_DATA);

    String dataString = "";
    logger.info("File Downloading Started..");
    for (int i = 0; i < rows.getNumRows(); i++) {
      rows.setRow(i);

      String rowData = rows.getString(GENERAL_CONSTANTS.ExternalConnector.WA);

      if (rowData.endsWith(",")) {
        rowData = rowData + " ,";
      }
      List<String> elephantList = Arrays.asList(rowData.split("\\,"));

      for (String data : elephantList) {
        dataString += data.trim() + GlobalParams.MAXIQ_DEL;
      }

      dataString = dataString.substring(0, dataString.lastIndexOf(GlobalParams.MAXIQ_DEL));

      fileWriter.write(CHAR_CONSTANTS.NEW_LINE + dataString);

      dataString = ""; // make dataString empty for new row
    }
    logger.info("File Downloading complete..");
    fileWriter.flush();
    fileWriter.close();
  }

  public void getDataInBath(SapErp sapErp, Map<String, String> confProperties)
      throws JCoException, IOException {
    // TODO Auto-generated method stub
    List<String> columnNames = sapErp.getColumnName();
    boolean selectedColumn = sapErp.isSelectColumn();
    String functionModule = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.FUNCTION_MODULE);
    String tableName = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.TABLE_NAME);
    String originalRowCount = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.ROW_COUNT);
    String filter = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.DATA_FILTER);
    String outFileLocation = confProperties.get(GENERAL_CONSTANTS.STORAGEPATH);
    String noOfSplit = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.NO_OF_SPLIT);
    String splitRowCount = confProperties.get(GENERAL_CONSTANTS.ExternalConnector.SPLIT_ROW_COUNT);

    File tempDirInWebServer = new File(outFileLocation);
    if (!tempDirInWebServer.exists()) {
      tempDirInWebServer.mkdirs();
    }

    File file = new File(outFileLocation + "/SapErpSample.txt");
    if (!file.exists()) {
      file.createNewFile();
    }

    FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
    logger.info("Sap connection Started");

    JCoDestination connection = getConnection(confProperties);

    logger.info("Sap connection Done");
    JCoRepository repository = connection.getRepository();

    long rowSkips = 0;
    long batchCount = 0;

    if (originalRowCount != null
        && originalRowCount.equals(GENERAL_CONSTANTS.ExternalConnector.FETCH_ALL_RECORDS)) {
      batchCount = Long.parseLong(splitRowCount);
    } else {
      batchCount = Long.parseLong(originalRowCount) / Long.parseLong(noOfSplit);
    }
    int count = Integer.parseInt(originalRowCount);
    while (true) {

      JCoFunction function = repository.getFunction(functionModule);

      if (function == null) throw new RuntimeException("Function Module Not found in SAP.");

      function
          .getImportParameterList()
          .setValue(GENERAL_CONSTANTS.ExternalConnector.QUERY_TABLE, tableName);
      function
          .getImportParameterList()
          .setValue(GENERAL_CONSTANTS.ExternalConnector.DELIMITER, CHAR_CONSTANTS.COMMA);
      function
          .getImportParameterList()
          .setValue(GENERAL_CONSTANTS.ExternalConnector.ROW_COUNT, batchCount);
      function
          .getImportParameterList()
          .setValue(GENERAL_CONSTANTS.ExternalConnector.ROW_SKIPS, rowSkips);

      JCoTable table =
          function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.FIELDS);

      if (columnNames != null && columnNames.size() > 0) {

        for (String columnName : columnNames) {
          table.appendRow();
          table.setValue(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME, columnName);
        }
      } else {
        for (int i = 0; i < table.getNumRows(); i++) {
          table.appendRow();
          table.setValue(
              GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME,
              table.getString(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME));
        }
      }
      if (filter != null && filter != "") {
        List<String> elementList = Arrays.asList(filter.split("\\|"));
        JCoTable table1 =
            function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.OPTIONS);
        for (String data : elementList) {
          table1.appendRow();
          table1.setValue(GENERAL_CONSTANTS.ExternalConnector.TEXT, data);
        }
      }

      function.execute(destination);

      if (rowSkips == 0) {
        final JCoTable codes =
            function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.FIELDS);

        String header = "";
        for (int i = 0; i < codes.getNumRows(); i++) {
          codes.setRow(i);
          header +=
              codes.getString(GENERAL_CONSTANTS.ExternalConnector.FIELD_NAME)
                  + GlobalParams.MAXIQ_DEL;
        }
        header = header.substring(0, header.lastIndexOf(GlobalParams.MAXIQ_DEL));

        fileWriter.write(header); // write file headers
        logger.info("header info - " + header);
      }
      final JCoTable rows =
          function.getTableParameterList().getTable(GENERAL_CONSTANTS.ExternalConnector.SAP_DATA);

      int numRows = rows.getNumRows();

      if (numRows == 0
          || (!originalRowCount.equals(GENERAL_CONSTANTS.ExternalConnector.FETCH_ALL_RECORDS)
              && count == rowSkips)) {
        logger.info("Table rows finished : " + rowSkips);
        break;
      }

      String dataString = "";
      logger.info("File Downloading Started..");
      for (int i = 0; i < numRows; i++) {
        rows.setRow(i);

        String rowData = rows.getString(GENERAL_CONSTANTS.ExternalConnector.WA);

        if (rowData.endsWith(CHAR_CONSTANTS.COMMA)) {
          rowData = rowData + CHAR_CONSTANTS.SPACE + CHAR_CONSTANTS.COMMA;
        }
        List<String> elephantList = Arrays.asList(rowData.split("\\,"));

        for (String data : elephantList) {
          dataString += data.trim() + GlobalParams.MAXIQ_DEL;
        }

        dataString = dataString.substring(0, dataString.lastIndexOf(GlobalParams.MAXIQ_DEL));

        fileWriter.write(CHAR_CONSTANTS.NEW_LINE + dataString);

        dataString = ""; // make dataString empty for new row
      }

      rowSkips = rowSkips + batchCount;
    }
    logger.info("File Downloading complete..");
    fileWriter.flush();
    fileWriter.close();
  }
}


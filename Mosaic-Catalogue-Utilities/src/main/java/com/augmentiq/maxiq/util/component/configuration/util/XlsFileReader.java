package com.augmentiq.maxiq.util.component.configuration.util;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;
import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.entity.model.configuration.bean.DataSource;
import com.augmentiq.maxiq.entity.model.configuration.bean.ExcelRange;
import com.augmentiq.maxiq.entity.model.configuration.bean.FieldMapping;

public class XlsFileReader {

  private static final Logger logger = LoggerFactory.getLogger(XlsFileReader.class);

  public String readXlxs(
      DataSource dataSource,
      String filePath,
      String source,
      String destination,
      Boolean isConvert,
      String outPutFilePath)
      throws Exception {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> readXlxs()"
            + ParamUtils.getString(filePath, source, destination));
    if (StringUtils.isBlank(filePath)) {
      ExceptionsMessanger.throwException(new SystemException(), "ERR_079", filePath);
    }

    List<ExcelRange> excelRanges = dataSource.getFileDataIngesterDetails().getExcelRanges();

    File tempDirInWebServer = new File(filePath);

    if (!tempDirInWebServer.exists()) {
      tempDirInWebServer.mkdirs();
    }

    List<String> files = new ArrayList<String>();
    if (FileUtilities.isDirectory(filePath)) {
      if (!filePath.trim().endsWith(CHAR_CONSTANTS.FORWARD_SLASH))
        filePath = filePath + CHAR_CONSTANTS.FORWARD_SLASH;

      files = FileUtilities.getFiles(filePath);
    } else {
      files.add(filePath);
    }
    if (StringUtils.isNotBlank(outPutFilePath)) {
      File file = new File(outPutFilePath);
      if (!file.exists()) {
        file.mkdirs();
      }
      BufferedWriter output =
          new BufferedWriter(
              new FileWriter(
                  outPutFilePath
                      + CHAR_CONSTANTS.FORWARD_SLASH
                      + GENERAL_CONSTANTS.EXCEL_FILE_NAME));
      output.close();
    }

    readExcelFileUsingPath(dataSource, filePath, isConvert, excelRanges, files, outPutFilePath);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << readXlxs()"
            + ParamUtils.getString(
                filePath + CHAR_CONSTANTS.FORWARD_SLASH + GENERAL_CONSTANTS.EXCEL_FILE_NAME));
    return filePath + CHAR_CONSTANTS.FORWARD_SLASH + GENERAL_CONSTANTS.EXCEL_FILE_NAME;
  }

  public void readExcelFileUsingPath(
      DataSource dataSource,
      String filePath,
      Boolean isConvert,
      List<ExcelRange> excelRanges,
      List<String> files,
      String outPutFilePath)
      throws FileNotFoundException, IOException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> readExcelFileUsingPath()"
            + ParamUtils.getString(
                dataSource, filePath, isConvert, excelRanges, files, outPutFilePath));

    String source;
    String destination;
    List<String> data = new ArrayList<>();
    List<String> header = new ArrayList<>();
    String excelFileExtension;
    for (String excelFiles : files) {

      Iterator<ExcelRange> iterator = excelRanges.iterator();
      excelFileExtension = FilenameUtils.getExtension(excelFiles);

      while (iterator.hasNext()) {
        FileInputStream fileInputStream = new FileInputStream(new File(excelFiles));

        ExcelRange excelRange = iterator.next();
        source = excelRange.getXlsFilePathFrom();
        destination = excelRange.getXlsFilePathTo();
        String name = excelRange.getXlsSheetName();
        Boolean indicator = excelRange.getXlsHeaderOptions();

        readXlxsFile(
            name,
            fileInputStream,
            source,
            destination,
            indicator,
            filePath,
            dataSource,
            data,
            header,
            isConvert,
            outPutFilePath,
            excelFileExtension);
        if (!isConvert && data.size() > 10) {
          break;
        }
      }

      if (!isConvert) {
        break;
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << readExcelFileUsingPath()"
            + ParamUtils.getString(
                dataSource, filePath, isConvert, excelRanges, files, outPutFilePath));
  }

  private String readXlxsFile(
      String name,
      FileInputStream fileInputStream,
      String source,
      String destination,
      Boolean header,
      String filePath,
      DataSource dataSource,
      List<String> data,
      List<String> headerData,
      Boolean isConvert,
      String outPutFilePath,
      String excelFileExtension)
      throws IOException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> readXlxsFile()"
            + ParamUtils.getString(
                name,
                fileInputStream,
                source,
                destination,
                header,
                filePath,
                dataSource,
                data,
                headerData,
                excelFileExtension));

    String newFilePath = "";
    int sourceColumn = 0;
    int sourceRow = 0;
    int destinationColumn = 0;
    int destinationRow = 0;
    BufferedWriter output = null;
    if (isConvert) {
      output =
          new BufferedWriter(
              new FileWriter(
                  outPutFilePath + CHAR_CONSTANTS.FORWARD_SLASH + GENERAL_CONSTANTS.EXCEL_FILE_NAME,
                  true));
    }

    DataFormatter formatter = new DataFormatter();
    try (FileInputStream stream = fileInputStream) {

      String[] sRow = StringUtils.splitPreserveAllTokens(source, "_");
      sourceColumn = StringUtils.isNotBlank(sRow[0]) ? columnIndexer(sRow[0]) - 1 : 0;
      sourceRow = StringUtils.isNotBlank(sRow[1]) ? Integer.parseInt(sRow[1]) - 1 : 0;

      String[] dRow = StringUtils.splitPreserveAllTokens(destination, "_");
      destinationColumn = columnIndexer(dRow[0]);
      destinationRow = StringUtils.isNotBlank(dRow[1]) ? Integer.parseInt(dRow[1]) - 1 : 0;
      Workbook hssfWorkbook = null;

      try {
        logger.debug(
            LoggerConstants.LOG_MAXIQWEB
                + " : << readXlxsFile()"
                + ParamUtils.getString(
                    name,
                    fileInputStream,
                    source,
                    destination,
                    header,
                    filePath,
                    dataSource,
                    data,
                    headerData,
                    excelFileExtension));
        if (excelFileExtension.equals(GENERAL_CONSTANTS.EXCEL_FILE_EXTENSION_XLSX)) {
          hssfWorkbook = new XSSFWorkbook(stream);
        } else if (excelFileExtension.equals(GENERAL_CONSTANTS.EXCEL_FILE_EXTENSION_XLS)) {
          hssfWorkbook = new HSSFWorkbook(stream);
        }
      } catch (Exception e) {
        logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      }

      Sheet sheetAt = null;
      for (int i = 0; i < hssfWorkbook.getNumberOfSheets(); i++) {
        if (hssfWorkbook.getSheetName(i).equalsIgnoreCase(name)) {
          sheetAt = hssfWorkbook.getSheetAt(i);
          break;
        }
      }

      if (sheetAt == null) {
        throw new SystemException("No sheet found with name : " + name);
      }

      Iterator<Row> rows = sheetAt.iterator();
      Map<Integer, FieldMapping> fieldMappingMap = new HashMap<Integer, FieldMapping>();

      Long rowItrCount = 0L;

      for (Iterator<Row> rowIterator = rows; rowIterator.hasNext(); ) {
        Row row = rowIterator.next();

        if (!((destinationRow - sourceRow) > 0)) {
          ExceptionsMessanger.throwException(
              new SystemException(), "ERR_081", (sourceRow + 1), (destinationRow + 1));
        }

        if (!((destinationColumn - sourceColumn) > 0)) {
          ExceptionsMessanger.throwException(
              new SystemException(), "ERR_082", sourceColumn, sourceColumn);
        }

        if (row.getRowNum() >= sourceRow && row.getRowNum() <= destinationRow) {

          StringBuilder builder = new StringBuilder();

          for (int cellItrCounter = sourceColumn;
              row.getLastCellNum() > cellItrCounter;
              cellItrCounter++) {

            Cell cell = row.getCell(cellItrCounter);

            if (cell == null) {
              builder.append(GlobalParams.MAXIQ_DEL);
              continue;
            }

            if (cell.getColumnIndex() >= sourceColumn
                && cell.getColumnIndex() <= destinationColumn - 1) {

              if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                builder.append(formatter.formatCellValue(cell));
              } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
                builder.append(convertExcelTypeToString(cell.getBooleanCellValue()));
              } else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                builder.append(GlobalParams.MAXIQ_DEL);
              } else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
                builder.append(convertExcelTypeToString(cell.getStringCellValue()));
              } else if (cell.getCellType() == Cell.CELL_TYPE_ERROR) {
                builder.append(convertExcelTypeToString(cell.getStringCellValue()));
              } else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                String convertExcelTypeToString =
                    convertExcelTypeToString(cell.getStringCellValue());
                if (convertExcelTypeToString.contains(GENERAL_CONSTANTS.BREAK_LINE)
                    || convertExcelTypeToString.contains(GENERAL_CONSTANTS.BACK_SLASH_R)) {
                  convertExcelTypeToString =
                      convertExcelTypeToString.replaceAll(GENERAL_CONSTANTS.BREAK_LINE, " ");
                  convertExcelTypeToString =
                      convertExcelTypeToString.replaceAll(GENERAL_CONSTANTS.BACK_SLASH_R, " ");
                }
                builder.append(convertExcelTypeToString);
              }
              if (cell.getColumnIndex() < destinationColumn - 1)
                builder.append(GlobalParams.MAXIQ_DEL);
            }
          }
          rowItrCount++;

          if (header && row.getRowNum() == sourceRow) {
            headerData.add(builder.toString());
          } else {
            data.add(builder.toString());
          }
        }

        if (data.size() > 10 && !isConvert) {
          break;
        } else if (isConvert && data.size() >= 1000) {
          writeDataInfile(
              header, dataSource, data, headerData, output, filePath, outPutFilePath, !isConvert);
          data.clear();
        }
      }

      if (!isConvert) {
        generateFieldMappingFromExcelFile(dataSource, data, headerData);
      }

    } catch (IOException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();

    } catch (Exception e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      e.printStackTrace();
    }

    if (isConvert) {
      writeDataInfile(
          header, dataSource, data, headerData, output, filePath, outPutFilePath, !isConvert);
      data.clear();
      output.close();
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << readXlxsFile()" + ParamUtils.getString(newFilePath));
    return newFilePath;
  }

  private String convertExcelTypeToString(Object cell) {

    if (null == cell) {
      return "";
    }

    return cell + "";
  }

  private void generateFieldMappingFromExcelFile(
      DataSource dataSource, List<String> data, List<String> headerData) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> generateFieldMappingFromExcelFile()"
            + ParamUtils.getString(dataSource, data, headerData));

    Map<Integer, FieldMapping> fieldMappingMap;
    String headerTemplate = "";
    Boolean isHeaderTemplate = false;
    dataSource.getFileDataIngesterDetails().setRecordStartLine(1l);
    if (StringUtils.isNoneBlank(dataSource.getFileDataIngesterDetails().getHeaderTemplate())) {
      headerTemplate = dataSource.getFileDataIngesterDetails().getHeaderTemplate();
      isHeaderTemplate = true;
      dataSource.getFileDataIngesterDetails().setRecordStartLine(2l);
    }
    String[] headerRecords = StringUtils.split(data.get(0), GlobalParams.MAXIQ_DEL);

    if (headerData.size() > 0) {
      headerRecords = StringUtils.split(headerData.get(0), GlobalParams.MAXIQ_DEL);
    }

    fieldMappingMap =
        SampleRecordExtractor.generateFieldMappingFromHeader(
            headerRecords, headerTemplate, dataSource.getId(), isHeaderTemplate, dataSource);
    int counter = 1;
    for (String record : data) {
      int position = 0;
      SampleRecordExtractor.addSampleRecordToFieldMapping(
          fieldMappingMap,
          counter,
          StringUtils.splitPreserveAllTokens(record, GlobalParams.MAXIQ_DEL),
          position);
      counter++;
    }

    List<FieldMapping> fieldMappings =
        createFinalFieldMappingWithType(fieldMappingMap, headerTemplate);

    dataSource.setFieldMappings(fieldMappings);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << generateFieldMappingFromExcelFile()"
            + ParamUtils.getString(dataSource));
  }

  private List<FieldMapping> createFinalFieldMappingWithType(
      Map<Integer, FieldMapping> fieldMappingMap, String headerTemplate) {
    List<FieldMapping> fieldMappings = new ArrayList<>();
    for (Entry<Integer, FieldMapping> entry : fieldMappingMap.entrySet()) {

      String string = SampleRecordExtractor.outPutFieldValidation(entry.getValue());
      if (string != null) {
        entry
            .getValue()
            .setFieldName(
                entry.getValue().getFieldName()
                    + GENERAL_CONSTANTS.POST_FIX_FOR_DUPLICATE_FIELD_NAME);
      }
      fieldMappings.add(entry.getValue());
    }

    if (StringUtils.isBlank(headerTemplate))
      fieldMappings = SampleRecordExtractor.dataTypeGenerator(fieldMappings);
    return fieldMappings;
  }

  private static void writeDataInfile(
      boolean header,
      DataSource dataSource,
      List<String> data,
      List<String> headerData,
      BufferedWriter output,
      String filePath,
      String outPutFilePath,
      boolean isConvert)
      throws IOException {

    if (header && null != dataSource && null != dataSource.getFileDataIngesterDetails()) {
      dataSource.getFileDataIngesterDetails().setRecordStartLine(2L);
      dataSource.getFileDataIngesterDetails().setHeaderStarting(1L);
      dataSource.getFileDataIngesterDetails().setContainsHeader(GENERAL_CONSTANTS.TRUE);

      if (headerData.size() == 1
          && StringUtils.equalsIgnoreCase(
              dataSource.getIngection(), GENERAL_CONSTANTS.INJECTION_TYPE_QUICK)) {
        for (String string : headerData) {
          output.append(string + GENERAL_CONSTANTS.BREAK_LINE);
        }
      }
    }

    if (data.size() > 0) {
      for (String string : data) {
        try {
          output.append(string + GENERAL_CONSTANTS.BREAK_LINE);
        } catch (IOException e) {
          output =
              new BufferedWriter(
                  new FileWriter(
                      outPutFilePath
                          + CHAR_CONSTANTS.FORWARD_SLASH
                          + GENERAL_CONSTANTS.EXCEL_FILE_NAME,
                      true));
          output.append(string + GENERAL_CONSTANTS.BREAK_LINE);
        }
      }
    }

    //		output.close();
    output =
        new BufferedWriter(
            new FileWriter(
                outPutFilePath + CHAR_CONSTANTS.FORWARD_SLASH + GENERAL_CONSTANTS.EXCEL_FILE_NAME,
                true));
  }

  private int columnIndexer(String source) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> columnIndexer()" + ParamUtils.getString(source));
    int sum = 0;

    if (StringUtils.isNotBlank(source)) {

      source = StringUtils.upperCase(source);

      char[] charArray = source.toCharArray();
      final int asciiOffset = 64;

      int length = charArray.length;

      for (int i = 1; i < length; i++) {
        sum += Math.pow(26, i);
      }

      int counter = 1;
      for (int i = 0; i < length; i++) {
        if (counter == length) {
          sum += (int) charArray[i] - asciiOffset;
        } else {
          sum += (((int) charArray[i] - asciiOffset - 1) * (Math.pow(26, (length - counter))));
        }
        counter++;
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << columnIndexer()" + ParamUtils.getString(sum));
    return sum;
  }
}

package com.augmentiq.maxiq.util.configuration.massages;
/** Changed for MAX-101 By Rushikesh Raut on 15-Sept-2016 */
public class ExplanationMassages {
  /** Changed for MAX-101 By Rushikesh Raut on 15-Sept-2016 */
  public static class DataSourceMassages {
    public String isDelimete = "If the delimiter is defined as a regular expression, check this.";
    public String dataIngestion =
        "Select if the data should be a straight through (Basic) or with validations (Advance).";
    public String inputParameters =
        "Define Input parameters based on which the data would be made available in data source.";
    public String dataSourceType = "Please select the type of source to be connected.";
    public String connHeader =
        "In this section all the parameters required to connect and retrieve data are defined.";
    public String serverFilePath = "Please enter the file location.";
    public String containsHeader = "Please mark if a header is	present.";
    public String recordStarts =
        " Records starting position can be defined from a fixed line or by an expression.";
    public String headerStarting = "Please enter the line position of header.";
    public String recordStartLine = "Please mention the exact line number from where data starts.";
    public String startingRegularExpre =
        "Please mention the exact	regular	expression to identify the starting position of record.";
    public String recordsIgnore = "Records to be excluded can be defined by a regular expression.";
    public String recordLoadLimit =
        "If only few limited records are to be loaded then please enter the number.";
    public String delimiter =
        "Define the delimiter i.e ,  \", \" for comma,  \";\" for semicolon, \"|\" for Pipe and so on. Incase of regular expression , please mark the checkbox.";
    public String ingestion = "In this section the method to ingest data is set.";
    public String advance =
        "If Advanced is selected, data can be validated and transformed while ingesting the data";
    public String basic =
        "Incase data is retrieved from reliable source and it is certain that no validation or transformation is needed then basic can be selected.Note: this would be faster.";
    /*public String inputParamHeader  = "In this section additional input parameters can be added. These parameters can be used for conditional, dynamic data load. Note: These fields can be viewed in field mapping. These parameters have to be provided during data load.";*/
    public String inputParamHeader =
        "These parameters can be used for conditional, dynamic data load. Note: These fields can be viewed in field mapping. These parameters have to be provided during data load.";
    public String addEditField =
        "Additional fields can be created once and used in multiple places. These can be calculated or fixed fields.";
    public String saveAllField = "All the definitions, mapping of fields are saved.";
    public String fieldNames =
        "Column names i.e header is displayed here. Incase the header is not present ,  header template is referred here.Note: the column names are editable.";
    public String validations =
        "This can be set to enable data validation during ingestion.The entire record or field can be rejected if validation fails.";
    public String records =
        "Few sample records are displayed for verification before	actual ingestion.";
    public String valFrequency =
        "Value frequency can be viewed by setting this. To enable this pre/post profiling option needs to be set under advance setting.";
    public String tranformations =
        "The data can be transformed and standardized as per requirement.";
    public String dataTypes =
        "The data types suggested are based on few sample records. Possible values are date, string, numeric boolean.  Note: User can reset the data type. Incase a field's data type does not match as per definition, the entire record gets rejected.";
    public String profiling =
        "Advance profiling can be set for numeric fields. This can be enabled by setting the pre/post profiling option under advance setting.";
    public String profilingHeaderOnAdvTab =
        "This helps to create a basic profiling for all the fields.Profiling can be done pre ingestion and post ingestion. For advance validation please check under Field mapping. Note: Profiling can be viewed from statistics under Explore( main menu)";
    public String profilingCheck =
        "Please set the profiling option.This will create basic profiling of all fields and enable the pre /post check box under field mapping section.";
    public String accessHeader =
        "&#8208; Select if the access is public or only to the group which creates it.";
    public String addAdvanceVal =
        "&#8208; Add new validation can be used to performs various checks on multiple fields.<br/>&#8208; Validations like length check, valid values etc";
    public String loadStratergyHeader =
        "In this section the data load strategy to be applied in every data refresh can be defined.";
    public String headerTemplate =
        "Please enter the header format if the file does not have a header. Note: This would be used as column names under field mapping.";
    public String excelRange =
        "Please enter the cell range as per excel.  Note: Format to be followed for cell is column_number i.e. for A1 define as A_1.";
    public String replaceAll = "Replace all - replaces all the data every time.";
    public String updateLoad =
        "Update is used if the data is incremental i.e. data can be merged,  appended,  deleted selectively.";
    public String appendInputType =
        "The data in this path would be appended as new records to the data source.";
    public String ReplaceLatest =
        "Incase there is specific logic to merge or update custom components can be coded and merged i.e. new records will be set.";
    public String mergePath =
        "The data in this path would be merged i.e. new records will be inserted and existing records	would be updated. Note : the schema should be	exactly same as defined in the current data source.";
    public String mergeKeys = "Specify the keys,  based on which the records will be merged.";
    public String deletePath = "The records from this path will be deleted.";
    public String deleteHTemplate =
        "Specify the header template and click on icon. Then map the keys for deletion.";
    public String fieldMapping =
        "This can be modified by selecting data-type. Validations and transformation of fields can be defined,  new fields can be added.";
    public String dataTab =
        "In this section few sample records are displayed. Based on these  sample data a schema is suggessted by the system.";
    public String updateDataSource =
        "&#8208; In this section, different type of updates can be configured. Please mark the check box to enable options.";
    public String appendAll =
        "&#8208; This option helps to always append data.<br/>&#8208; Note : The schema should be exactly same as defined in the current data source.";
    public String mergeAll =
        "&#8208; This option helps to update existing records and insert new records. <br/>&#8208; Note : The schema should be exactly same as defined in the current data source.";
    public String deleteAll = "&#8208; This option helps to delete records based on key mapping.";
    public String appSetting =
        "In this section datasource setting can be configured.These configuration will be used if datasource load initiated by apllication.";
    public String useAsIt = "Do not refresh the data source , use as it is data.";
    public String refresh = "Load the data source as soon as flow runs.";
    public String dropDownForConnection = "Please select a connection";
    public String dropDownForFileType = "Please select the file type";
    public String dropDownForCompression = "Please select the compression type";
    public String headingForFileFormat =
        "Following options helps to set the file type and compression type. Data will be converted to selected file type and will be compressed as per the selected compression type";
    public String dropDownFileType = "Data-at-rest file type. Defaulted to parquet now.";
    public String dropDownCompressionType = "Please select data-at-rest compression type";
    public String optionallyEnclosedInDoubleQuotes = "Select if data is enclosed in double quotes";
  }

  public static class WorkFlowMassages {
    public String workFlowHeader =
        "&#8208; Analysis apps help to process, transform data into desired format.";
    public String statusView =
        "To view the executing app please select. Note: On completion of execution of a node, Sample data can be viewed.";
    public String designView = "To view the current design view please select.";
    public String saveApp = "This will save the latest app design.";
    public String validateApp =
        "This will validate the app design. Note : incase of error the Node is highlighted.";
    public String editApp = "This enables editing an app's details.";
    public String removeSelected =
        "This will remove/delete specific steps from the app design. Note: please save the app after deletion.";
    public String runApp = " This is to execute the latest app design.";
    public String selectedExist =
        "Please click this to select data sources on which operations have to be performed.";
    public String createDS =
        "Please click this to add new data sources. Note : this data source would be available only within the current app.";
    public String createVar = "This helps to add new calculated fields.";
    public String lookUp =
        "This helps to find a matching record in other file and mark as Y/N against records.";
    public String groupBy = "This option can be used to group records based on a key.";
    public String filter =
        "This option can be used to filter based on a condition and carry forward the selected records for further processing.";
    public String join = "This option can be used to join records from different files.";
    public String sort = "This helps to sort records (ascending/descending).";
    public String trans = "This helps to transpose records(row to column).";
    public String existHeader =
        "&#8208; The data sources configured and created are listed here. Please mark the check box to select data sources.";
    public String appLevelDSHeader =
        "&#8208; The data sources configured and created within the current app are listed here. Please mark the check box to select data sources.";
    public String nodeHeader =
        "&#8208; The data saved from other apps are listed here. Please mark the check box to select data sources.";
    public String inputDs = "The Input files defined for this app are listed here.";
    public String importApp =
        "&#8208; To Add existing application as a node in current application.";
    public String importAppTooltip =
        "To Add existing application as a node in current application.";
    public String customQueryTooltip = "To add hive query node.";
    public String customProcessTooltip = "To add spark custom component.";
    public String customSelectTooltip = "To add link node.";
    public String customJavaTooltip = "To add java node.";
    public String customMrTooltip = "To add Map reduce custom component.";
    public String customShellScriptTooltip = "To add shell script custom component.";
    public String editAppTooltip = "&#8208; Please provide paramter to run the application.";
    public String customCommon = "";
    public String rdbmsNode = "Add RDBMS node to export data to any relational database.";
    public String runningLogs = "Show logs";
    public String pushToDsNode = "Push the data of the node to a data source";
    public String saveForVisualization = "Create elasticsearch index on the data";
  }

  public static class AppProcessMassages {
    public String customMr = "&#8208; Add MapReduce custom component added as custom component.";
    public String customJava = "&#8208; Add Java custom component added as custom component.";
    public String customShellScript =
        "&#8208; Add shell script node to connect Map reduce.<br/>&#8208; Application output node to any process node.";
    public String customSelect =
        "&#8208; Add select node to connect Map reduce.<br/>&#8208; Application output node to any process node.";
    public String rdbmsNode = "&#8208; Add RDBMS node to export data to any relational database.";
    public String inputHeader = "List of fields available for processing.";
    public String outputHeader =
        "List of processed fields. These fields can be removed by deselecting. The order of the fields can also be modified by clicking on the arrow key.";
    public String joinHeader =
        "&#8208; Two different files can be joined based on key fields.<br/>&#8208; Inner Joins, Left Outer join, Right Outer join, Full Outer Join can be created here";
    public String joinFields = "Please select the key fields for the join.";
    public String joinDs = "Please select the data source to join.";
    public String joinFilter =
        "Conditional filter of records after join can be set here. After	setting click on add to condition.";
    public String joinDist = "Please check this if only distinct records are required after join.";
    public String groupHeader =
        "&#8208; The input data can be grouped and an aggregate functions can be performed here.<br/>&#8208; Aggregate functions like sum,average,count etc can be performed ";
    public String groupKey = "Please select the key for grouping.";
    public String groupFilter = "The grouped records can be filtered based on conditions.";
    public String filterHeader = "&#8208; Records can be filtered based on conditions.";
    public String addVarHeader =
        "&#8208; Additional fields that are needed across various process are created here.<br/>&#8208; These fields can be a fixed value, calculated field or Binned.";
    public String fixedHeader =
        "&#8208; New fields with fixed values can be added by selecting field name and data type.";
    public String mathHeader =
        "&#8208; New calculated fields can be created using various functions listed below.";
    public String binningHeader =
        "&#8208; A new field categorizing the record, can be added.<br/>&#8208; This can be based on a value or range of a field.";
    public String customQueryHeader =
        "&#8208; In case of complex queries custom query can be used. <br/>&#8208; Note : Queries supported by Hive only can be used here.";
    public String validateQuery = "The query can be validated, before saving.";
    public String transHeader =
        "&#8208; Here the data can be transposed based on a condition. ie Row to column conversion";
    public String lookUpHeader =
        "&#8208; This helps to find a matching record in other look up file. A new column with Y/N is added. <br/> &#8208; Note: 'Y' indicates match found.";
    public String commonHeader =
        "&#8208; User can select the fields to be carried over, output fields can be re-named and sequence of the output fields can be edited.";
    public String headerForCommon =
        "&#8208; User can select the fields to be carried over, output fields can be re-named and sequence of the output fields can be edited.<br/> &#8208; Output will be compressed using the selected compression type.";
    public String dropDownCompressType = "Please select the compression type";
    public String noSqlTooltip =
        "&#8208 This node is to be used to push flow results into a NoSQL database. <br/> &#8208 Core logic of creating mongo document structure is to be handled inside a custom component";
    public String mlCorrelation =
        "&#8208; User can select target columns to get correlation between them. <br/> &#8208; Output will be displayed in matrix format.";
    public String mlDimensionReduction =
        "&#8208; User can set Principle Component, Target Columns and Feature Columns. <br/> &#8208; Output will be saved and can be used in subsequent nodes.";
    public String mlOutlierTreatment =
        "&#8208; User can set lower quartile, upper quartile and factor, every thing that falls beyond these parameters will be removed. <br/> &#8208; Output will be saved and can be used in subsequent nodes.";
    public String mlClassification =
        "&#8208; User can select required classification algorithm. <br/> &#8208; Output will be saved and can be used in subsequent nodes.";
    public String mlClustering =
        "&#8208; User can select required Clustering algorithm. <br/> &#8208; Output will be saved and can be used in subsequent nodes.";
    public String mlRegression =
        "&#8208; User can select required Regreession algorithm. <br/> &#8208; Output will be saved and can be used in subsequent nodes.";
    public String mlUseModel =
        "&#8208; User can select existing models and map features from the input columns. <br/> &#8208; Output will be saved and can be used in subsequent nodes.";
    public String mlnormalization =
        "&#8208; Users can use this as a preprocessing step to standardize the range of independent variables or features of data. <br/> &#8208; Getting all the features on similar ranges help the Machine learning algorithms to converge faster and give better results.";
    public String mldatacleansing =
        "&#8208; Users can use this node to preprocess and cleanse the text information which helps improve the overall text mining pipeline. <br/> &#8208; This node contains the standard preprocessing techniques like Stemming, Language translation prebuilt to increase the efficiency of data scientists.";
    public String mlfeaextraction =
        "&#8208; Users can use this as a feature vectorization method for textual data, which is a core part of text mining to reflect the importance of a term. <br/> &#8208; This node contains the standard tokenization techniques like Simple, Regex, n-gram and also feature engineering techniques – Tf-IDF, Word2Vec, Count Vectorizer prebuilt.";
  }

  public static class Views {
    public String DsHeader =
        "&#8208;To refresh data, click on 'Load data source' icon.  <br/> &#8208; Data sources can be logically grouped as a 'Data source group'. They can be loaded together by clicking the group link.";
    public String DsGroupHeader =
        "&#8208; Listed below are the data source groups available. <br/> &#8208; These are logical groups of data sources as per definition.";
    public String AnalysisAppHeader =
        "&#8208; Listed below are the apps created in the system. <br/> &#8208; Analysis apps are workflows defined to augment data as per requirement.<br/> &#8208; Note: Every time an analysis app is created and run a new instance of output data is created. This data can be verified in 'Explore'";
  }

  public static class Explore {
    public String QueryEditorHeader =
        "&#8208; Please write a query to view or download output data.<br/> &#8208; Note: 'Limit' clause is a mandatory parameter to be included in the query, due to limited browser capacity.";
    public String SaveOutput = "The entire query output will be saved on the server";
    public String Download = "The Query output will be downloaded on the local machine";
    public String QueryName = "Queries can be saved and re-executed";
  }

  public static class Statistics {
    public String profilingHeader =
        "&#8208; The profiling jobs can be monitored here.<br/>&#8208; To view the output please select action.";
    public String correlationHeader =
        "&#8208; Correlation between two fields can be calculated here. <br/>&#8208; Please select the data source from data tree.";
  }

  public static class Index {
    public String indexHeader =
        "&#8208; Create/modify/rebuild a search index on your data, which can be queried later.";
  }

  public static class Query {
    public String queryHeader =
        "&#8208; Design your search query on the index, run it to see results.";
  }

  public static class CustomComponent {
    public String customHeader =
        "&#8208; Configure your custom component, so that you can use it in various places such as flows, feature extraction etc...";
  }

  public static class Connection {
    public String connectionHeader = "&#8208; Define connections to various databases here.";
  }

  /*  Start of Mercer code change */
  public static class Expressions {
    public String expressionHeader = "&#8208; Define expressions here.";
  }
  /*  End of Mercer code change */
}

package com.augmentiq.maxiq.util.external.source.rdbms.connection.lookup.directory;

import org.apache.commons.lang.StringUtils;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.constant.configuration.enums.ConnectorType;

public class ConnectionLookUpDirectory {
	
	private static String QUERY = "select * from {{tableName}}";

	public static String lookupRdbmsDriverClass(String subConnectionType) throws SystemException {

	    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> lookupRdbmsDriverClass()" + ParamUtils.getString(connectorType));
	     */
	    String driverClass = null;

	    
	    switch (subConnectionType) {
	      case "ORACLE":
	        driverClass = "oracle.jdbc.driver.OracleDriver";
	        break;
	      case "POSTGRES":
	        driverClass = "org.postgresql.Driver";
	        break;
	      case "MYSQL":
	        driverClass = "com.mysql.jdbc.Driver";
	        break;
	      case "DB2":
	        driverClass = "com.ibm.db2.jcc.DB2Driver";
	        break;
	      case "NETEZZA":
	        driverClass = "org.netezza.Driver";
	        break;
	      case "SQL_SERVER":
	        driverClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	        break;
	      case "HIVE":
	        driverClass = "org.apache.hive.jdbc.HiveDriver";
	        break;

	      default:
	        ExceptionsMessanger.throwException(
	            new IllegalArgumentException(), "ERR_033", subConnectionType);
	    }

	    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB+" << lookupRdbmsDriverClass()" + ParamUtils.getString(driverClass));
	     */ return driverClass;
	  }
	
	public static String generateRdbmsConnectUrl(String subSourceType,String hostUrl,Integer port,String sid) {
		    		//logger.debug(LoggerConstants.LOG_MAXIQWEB+" >> generateRdbmsConnectString()");
		     
		    String connectString = "";

		    if (StringUtils.isNotBlank(subSourceType)
		        && subSourceType.equalsIgnoreCase(ConnectorType.ORACLE.name())) {
		      connectString = "jdbc:oracle:thin:@" + hostUrl + ":" + port + ":" + sid;

		    } else if (StringUtils.isNotBlank(subSourceType)
		        && subSourceType.equalsIgnoreCase(ConnectorType.DB2.name())) {
		      connectString = "jdbc:db2://" + hostUrl + ":" + port ;

		    } else if (StringUtils.isNotBlank(subSourceType)
		        && subSourceType.equalsIgnoreCase(ConnectorType.POSTGRES.name())) {
		      connectString = "jdbc:postgresql://" + hostUrl + ":" + port+"/";

		    } else if (StringUtils.isNotBlank(subSourceType)
		        && subSourceType.equalsIgnoreCase(ConnectorType.MYSQL.name())) {
		      connectString = "jdbc:mysql://" + hostUrl + ":" + port;

		    } else if (StringUtils.isNotBlank(subSourceType)
		        && subSourceType.equalsIgnoreCase(ConnectorType.NETEZZA.name())) {
		      connectString = "jdbc:netezza://" + hostUrl + ":" + port;

		    } else if (StringUtils.isNotBlank(subSourceType)
		        && subSourceType.equalsIgnoreCase(ConnectorType.SQL_SERVER.name())) {
		      connectString =
		          "jdbc:sqlserver://" + hostUrl + ":" + port + ";databaseName=";
		    } else if (StringUtils.isNotBlank(subSourceType)
		        && subSourceType.equalsIgnoreCase(ConnectorType.HIVE.name())) {
		      connectString =
		    		  "jdbc:hive2://" + hostUrl + ":" + port;
		    }

		    	//	logger.debug(LoggerConstants.LOG_MAXIQWEB+" << generateRdbmsConnectString()" + ParamUtils.getString(connectString));
		      return connectString;
		  }
	
		public static String formulateQuery(String tableName) {
			
			return QUERY.replace("{{tableName}}", tableName);
		}

		 /* public String generateNoSqlUri() throws Exception {
		    String uri = "";
		    SecretService secretService = null;
		    if (ConnectorType.MONGODB.name().equalsIgnoreCase(this.getConnectorType())) {

		      if (!StringUtils.isBlank(this.getDbPassword())) {
		        secretService = new SecretService();
		      }
		      uri =
		          "mongodb://"
		              + this.getDbUserName()
		              + ":"
		              + secretService.decrypt(this.getDbPassword()).replaceAll("@", "%40")
		              + "@"
		              + this.getIp()
		              + ":"
		              + this.getPort()
		              + "/"
		              + this.getDbName()
		              + "."
		              + this.getNoSqlSchema();
		    }
		    return uri;
		  }*/


}

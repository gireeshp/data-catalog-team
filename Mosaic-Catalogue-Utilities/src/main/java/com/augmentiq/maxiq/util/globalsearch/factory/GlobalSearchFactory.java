package com.augmentiq.maxiq.util.globalsearch.factory;

import com.augmentiq.maxiq.dao.globalsearch.GlobalSearchIndexDAOImpl;
import com.augmentiq.maxiq.entity.model.configuration.bean.GlobalSearchResult;
import com.augmentiq.maxiq.model.globalsearch.GlobalSearchInput;

public class GlobalSearchFactory {

  /**
   * @param globalSearchInput is Object where we pass all required parameters
   * @return
   */
  public static GlobalSearchResult searchForGivenInput(GlobalSearchInput globalSearchInput) {
    return new GlobalSearchIndexDAOImpl().searchInDocuments(globalSearchInput);
  }

  /**
   * @param globalSearchInput is Object where we pass all required parameters
   * @return
   */
  public static GlobalSearchResult searchForGivenInputForDiscovery(
      GlobalSearchInput globalSearchInput) {
    return new GlobalSearchIndexDAOImpl().searchInDocumentsDiscovery(globalSearchInput);
  }
}

package com.augmentiq.maxiq.util.session.registry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class SessionRegistry {

  public static Map<String, String> sessions = new HashMap<String, String>();
  public static Map<String, HttpSession> sessionByEmail = new HashMap<String, HttpSession>();
  public static Map<String, List<Long>> roles = new HashMap<String, List<Long>>();

  public static void addSession(String key, String value) {
    sessions.put(key, value);
    sessions.put(value, key);
  }

  public static void addSessionByEmail(String email, HttpSession value) {
    sessionByEmail.put(email, value);
  }

  public static HttpSession getSessionByEmail(String email) {
    return sessionByEmail.get(email);
  }

  public static void addRoles(String key, List<Long> value) {
    roles.put(key, value);
  }

  public static Boolean isSessionAvailable(String key) {
    if (sessions.get(key) != null) {
      return true;
    } else {
      return false;
    }
  }

  public static String getSession(String key) {
    return sessions.get(key);
  }

  public static void removeSession(String email, String sessionId) {
    if (email == null) {
      email = sessions.get(sessionId);
    }

    if (null == sessionId) {
      sessions.remove(email);
      roles.remove(email);
    }

    if (null != sessionId) {
      sessions.remove(sessions.get(sessionId));
      sessions.remove(sessionId);
    }
  }
}

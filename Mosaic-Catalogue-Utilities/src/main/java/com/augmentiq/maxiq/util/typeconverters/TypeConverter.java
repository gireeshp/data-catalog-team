package com.augmentiq.maxiq.util.typeconverters;

import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;

public class TypeConverter {

  public static FieldDataTypes convertFromHiveDataTypeToFieldDataType(String type) {

    FieldDataTypes fieldDataTypes;

    switch (type) {
      case "INT":
        fieldDataTypes = FieldDataTypes.INTEGER;
        break;
      case "DOUBLE":
        fieldDataTypes = FieldDataTypes.DOUBLE;
        break;
      case "BIGINT":
        fieldDataTypes = FieldDataTypes.LONG;
        break;
      case "TINYINT":
        fieldDataTypes = FieldDataTypes.INTEGER;
        break;
      case "SMALLINT":
        fieldDataTypes = FieldDataTypes.INTEGER;
        break;
      case "VARCHAR":
        fieldDataTypes = FieldDataTypes.STRING;
        break;
      case "CHAR":
        fieldDataTypes = FieldDataTypes.STRING;
        break;
      case "DECIMAL":
        fieldDataTypes = FieldDataTypes.DECIMAL;
        break;
      case "BIGDECIMAL":
        fieldDataTypes = FieldDataTypes.DECIMAL;
        break;
      case "FLOAT":
        fieldDataTypes = FieldDataTypes.DOUBLE;
        break;
      case "BOOLEAN":
        fieldDataTypes = FieldDataTypes.BOOLEAN;
        break;
      case "DATE":
        fieldDataTypes = FieldDataTypes.DATE;
        break;
      case "LONG":
        fieldDataTypes = FieldDataTypes.LONG;
        break;
      case "INTEGER":
        fieldDataTypes = FieldDataTypes.INTEGER;
        break;
      case "TIMESTAMP":
        fieldDataTypes = FieldDataTypes.TIMESTAMP;
        break;
      default:
        if (type.startsWith("STRUCT")) {
          fieldDataTypes = FieldDataTypes.MAP;
        } else if (type.startsWith("ARRAY")) {
          fieldDataTypes = FieldDataTypes.LIST;
        } else {
          fieldDataTypes = FieldDataTypes.STRING;
        }
    }
    return fieldDataTypes;
  }

  public static String convertFromFieldDataTypeToHiveType(String type) {

    String fieldDataTypes;
    switch (type) {
      case "INTEGER":
        fieldDataTypes = "INT";
        break;
      case "DOUBLE":
        fieldDataTypes = "DOUBLE";
        break;
      case "LONG":
        fieldDataTypes = "BIGINT";
        break;
      case "STRING":
        fieldDataTypes = "STRING";
        break;
      case "BOOLEAN":
        fieldDataTypes = "BOOLEAN";
        break;
      case "DATE":
        fieldDataTypes = "DATE";
        break;
      case "TIMESTAMP":
        fieldDataTypes = "TIMESTAMP";
        break;
      default:
        fieldDataTypes = "STRING";
    }

    return fieldDataTypes;
  }
}

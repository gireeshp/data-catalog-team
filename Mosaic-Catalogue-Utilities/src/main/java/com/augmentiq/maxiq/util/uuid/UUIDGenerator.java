package com.augmentiq.maxiq.util.uuid;

import java.util.UUID;

public class UUIDGenerator {

  public static String getUuid() {
    return UUID.randomUUID().toString();
  }
}

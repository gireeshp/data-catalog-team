===================================================================================================

alter table customcomponentinstance add configurationFilePath varchar(100);
alter table customcomponentinstance add resourceFilePath varchar(100);

===================================================================================================

alter table maxiq_apps add column runningStatus varchar(20) default 'STOPPED';
ALTER TABLE app_graph_nodes ADD nodeDescription blob;
=======================================================================================================================================================================

alter table datasourceinstance drop column id;
alter table datarepoinstance drop column id;
alter table hiveQuery modify column hbase varchar(25);

delete from app_output_files where id in (select id from (SELECT id, COUNT(*) c FROM app_output_files GROUP BY id HAVING c > 1) as tbl)
alter table app_global_param modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY; 
alter table app_output_files modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from search_query_def where id in (select id from (SELECT id, COUNT(*) c FROM search_query_def GROUP BY id HAVING c > 1) as tbl)
alter table search_query_def modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from index_defination where id in (select id from (SELECT id, COUNT(*) c FROM index_defination GROUP BY id HAVING c > 1) as tbl)
alter table index_defination modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from dataParamInstance where id in (select id from (SELECT id, COUNT(*) c FROM dataParamInstance GROUP BY id HAVING c > 1) as tbl)
alter table dataParamInstance modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from timeSeriesData where id in (select id from (SELECT id, COUNT(*) c FROM timeSeriesData GROUP BY id HAVING c > 1) as tbl)
alter table timeSeriesData modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from customcomponentinstance where componentId in (select componentId from (SELECT componentId, COUNT(*) c FROM customcomponentinstance GROUP BY componentId HAVING c > 1) as tbl)
alter table customcomponentinstance modify column componentId INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from socialappinstance where id in (select id from (SELECT id, COUNT(*) c FROM socialappinstance GROUP BY id HAVING c > 1) as tbl)
alter table socialappinstance modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;  

delete from timeSeriesComponents where id in (select id from (SELECT id, COUNT(*) c FROM timeSeriesComponents GROUP BY id HAVING c > 1) as tbl)
alter table timeSeriesComponents modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from configuration where id in (select id from (SELECT id, COUNT(*) c FROM configuration GROUP BY id HAVING c > 1) as tbl)
alter table configuration modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from hiveQuery where id in (select id from (SELECT id, COUNT(*) c FROM hiveQuery GROUP BY id HAVING c > 1) as tbl)
alter table hiveQuery modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from externaldatadetails where id in (select id from (SELECT id, COUNT(*) c FROM externaldatadetails GROUP BY id HAVING c > 1) as tbl)
alter table externaldatadetails modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from my_collection where id in (select id from (SELECT id, COUNT(*) c FROM my_collection GROUP BY id HAVING c > 1) as tbl)
alter table my_collection modify column id INT NOT NULL AUTO_INCREMENT;

delete from feedback where id in (select id from (SELECT id, COUNT(*) c FROM feedback GROUP BY id HAVING c > 1) as tbl)
alter table feedback modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from feedback_voting where id in (select id from (SELECT id, COUNT(*) c FROM feedback_voting GROUP BY id HAVING c > 1) as tbl)
alter table feedback_voting modify column id INT NOT NULL AUTO_INCREMENT;

delete from user_feedback where id in (select id from (SELECT id, COUNT(*) c FROM user_feedback GROUP BY id HAVING c > 1) as tbl)
alter table user_feedback modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from maxiq_apps where id in (select id from (SELECT id, COUNT(*) c FROM maxiq_apps GROUP BY id HAVING c > 1) as tbl)
alter table maxiq_apps modify column appId INT(100) NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from workflow_process_info where id in (select id from (SELECT id, COUNT(*) c FROM workflow_process_info GROUP BY id HAVING c > 1) as tbl)
alter table workflow_process_info modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from job_instances where jobInstId in (select jobInstId from (SELECT jobInstId, COUNT(*) c FROM job_instances GROUP BY jobInstId HAVING c > 1) as tbl)
alter table job_instances modify column jobInstId INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from scheduled_application_details where id in (select id from (SELECT id, COUNT(*) c FROM scheduled_application_details GROUP BY id HAVING c > 1) as tbl)
alter table scheduled_application_details modify column id INT NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from appviewinstance where id in (select id from (SELECT id, COUNT(*) c FROM appviewinstance GROUP BY id HAVING c > 1) as tbl)
alter table appviewinstance modify column Id Int NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from maxiq_metadata where id in (select id from (SELECT id, COUNT(*) c FROM maxiq_metadata GROUP BY id HAVING c > 1) as tbl)
alter table maxiq_metadata modify column id int NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from datasourceinstance where dataSourceId in (select dataSourceId from (SELECT dataSourceId, COUNT(*) c FROM datasourceinstance GROUP BY dataSourceId HAVING c > 1) as tbl)
alter table datasourceinstance column dataSourceId int NOT NULL AUTO_INCREMENT PRIMARY KEY;

delete from datarepoinstance where id in (select id from (SELECT id, COUNT(*) c FROM datarepoinstance GROUP BY id HAVING c > 1) as tbl)
alter table datarepoinstance column repoId int NOT NULL AUTO_INCREMENT PRIMARY KEY;

alter table queue_master ADD COLUMN rangerPolicyId varchar(50) NULL;

alter table job_instances modify errorLog longblob;

===============================================================================================================

 CREATE TABLE `category_subcategory_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catOrSubCatName` varchar(100) DEFAULT NULL,
  `parentId` varchar(45) DEFAULT NULL,
  `createdBy` int(10) DEFAULT NULL,
  `createdDate` varchar(100) DEFAULT NULL,
  `modifiedDate` varchar(100) DEFAULT NULL,
  `modifiedBy` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1 as default with Active and 0 as Inactive',
  `shortDescription` varchar(256) DEFAULT NULL,
  `longDescription` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1



CREATE TABLE `datasource_approval_mapper` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `objectId` int(10) NOT NULL,
  `level` int(10) NOT NULL,
  `parent_level` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8


CREATE TABLE `datasource_service_apimanager` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dataSourceId` int(10) DEFAULT NULL,
  `dataSourceName` varchar(40) DEFAULT NULL,
  `userId` int(10) DEFAULT NULL,
  `apiKey` varchar(50) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `deleted` int(10) DEFAULT NULL,
  `createdDate` varchar(40) DEFAULT NULL,
  `modifiedDate` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8

CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `objectType` int(11) NOT NULL,
  `actionId` int(11) NOT NULL,
  `notificationType` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

update my_collection set objectType='DATA_SOURCE';

=================================================================================================================

alter table datasourceinstance add fileStoreObject varchar(100);
alter table activity_log MODIFY COLUMN objectName longblob;
alter table glossary_master modify shortDescription longblob;
alter table glossary_master modify longDescription  longblob;

=================================================================================================================

ALTER VIEW `datasource_view` AS select `dto`.`groupId` AS `groupId`,concat(`app`.`firstName`,' ',`app`.`lastName`) AS `createdBy`,`dto`.`dataSourceName` AS `dataSourceName`,`dto`.`dataRepoName` AS `dataRepoName`,`dto`.`dataSourceType` AS `dataSourceType`,`dto`.`updatedDate` AS `updatedDate`,`dto`.`category` AS `category`,`dto`.`subCategory` AS `subCategory`,`dto`.`totalRecords` AS `totalRecords`,`dto`.`ownerProjectId` AS `ownerProjectId`,`dto`.`dataSourceId` AS `datasourceid`,`fbk`.`avgRating` AS `avgRating`,'LOAD_EDIT' AS `accessType`,`dto`.`fileType` AS `fileType` from ((`datasourceinstance` `dto` left join `application_user` `app` on((`app`.`unqUserId` = `dto`.`createdBy`))) left join `feedback_view` `fbk` on((`dto`.`dataSourceId` = `fbk`.`dataSourceId`)));
ALTER VIEW `datasource_view` AS select `dto`.`groupId` AS `groupId`,concat(`app`.`firstName`,' ',`app`.`lastName`) AS `createdBy`,`dto`.`dataSourceName` AS `dataSourceName`,`dto`.`dataRepoName` AS `dataRepoName`,`dto`.`dataSourceType` AS `dataSourceType`,`dto`.`updatedDate` AS `updatedDate`,`dto`.`category` AS `category`,`dto`.`subCategory` AS `subCategory`,`dto`.`totalRecords` AS `totalRecords`,`dto`.`ownerProjectId` AS `ownerProjectId`,`dto`.`dataSourceId` AS `datasourceid`,`fbk`.`avgRating` AS `avgRating`,'LOAD_EDIT' AS `accessType`,`dto`.`fileType` AS `fileType`,`dto`.`fileStoreObject` AS `fileStoreObject` from ((`datasourceinstance` `dto` left join `application_user` `app` on((`app`.`unqUserId` = `dto`.`createdBy`))) left join `feedback_view` `fbk` on((`dto`.`dataSourceId` = `fbk`.`dataSourceId`)));
ALTER VIEW `datasource_view` AS select `dto`.`groupId` AS `groupId`,concat(`app`.`firstName`,' ',`app`.`lastName`) AS `createdBy`,`dto`.`dataSourceName` AS `dataSourceName`,`dto`.`dataRepoName` AS `dataRepoName`,`dto`.`dataSourceType` AS `dataSourceType`,`dto`.`updatedDate` AS `updatedDate`,`dto`.`category` AS `category`,`dto`.`subCategory` AS `subCategory`,`dto`.`totalRecords` AS `totalRecords`,`dto`.`ownerProjectId` AS `ownerProjectId`,`dto`.`dataSourceId` AS `datasourceid`,`fbk`.`avgRating` AS `avgRating`,'LOAD_EDIT' AS `accessType`,`dto`.`fileType` AS `fileType` from ((`datasourceinstance` `dto` left join `application_user` `app` on((`app`.`unqUserId` = `dto`.`createdBy`))) left join `feedback_view` `fbk` on((`dto`.`dataSourceId` = `fbk`.`dataSourceId`)));
ALTER VIEW `datasource_view` AS select `dto`.`groupId` AS `groupId`,concat(`app`.`firstName`,' ',`app`.`lastName`) AS `createdBy`,`dto`.`dataSourceName` AS `dataSourceName`,`dto`.`dataRepoName` AS `dataRepoName`,`dto`.`dataSourceType` AS `dataSourceType`,`dto`.`updatedDate` AS `updatedDate`,`dto`.`category` AS `category`,`dto`.`subCategory` AS `subCategory`,`dto`.`totalRecords` AS `totalRecords`,`dto`.`ownerProjectId` AS `ownerProjectId`,`dto`.`dataSourceId` AS `datasourceid`,`fbk`.`avgRating` AS `avgRating`,'LOAD_EDIT' AS `accessType`,`dto`.`fileType` AS `fileType`, `dto`.`appId` AS 'appId', `dto`.`fileStoreObject` AS `fileStoreObject` from ((`datasourceinstance` `dto` left join `application_user` `app` on((`app`.`unqUserId` = `dto`.`createdBy`))) left join `feedback_view` `fbk` on((`dto`.`dataSourceId` = `fbk`.`dataSourceId`)));
alter view `datasource_view` AS select `dto`.`groupId` AS `groupId`,concat(`app`.`firstName`,' ',`app`.`lastName`) AS `createdBy`,`dto`.`dataSourceName` AS `dataSourceName`,`dto`.`dataRepoName` AS `dataRepoName`,`dto`.`dataSourceType` AS `dataSourceType`,`dto`.`updatedDate` AS `updatedDate`,`dto`.`category` AS `category`,`dto`.`subCategory` AS `subCategory`,`dto`.`totalRecords` AS `totalRecords`,`dto`.`ownerProjectId` AS `ownerProjectId`,`dto`.`dataSourceId` AS `datasourceid`,`fbk`.`avgRating` AS `avgRating`,'LOAD_EDIT' AS `accessType`,`dto`.`fileType` AS `fileType`,`dto`.`fileStoreObject` AS `fileStoreObject` from ((`datasourceinstance` `dto` left join `application_user` `app` on((`app`.`unqUserId` = `dto`.`createdBy`))) left join `feedback_view` `fbk` on((`dto`.`dataSourceId` = `fbk`.`dataSourceId`))) where `dto`.`flowType` <> 'STREAM';

===============================================11-Dec-2017===================================================================


=============================================================================================================================

=============================category and subcategory audit==================================================================
// Enterprise Strategic Planning & Effectiveness START
//Cat
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('100','Enterprise Strategic Planning & Effectiveness','0','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Enterprise Strategic Planning & Effectiveness','');
//SubCat Start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('201','Business Communication','100','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Business Communication','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('202','Policy','100','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Policy','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('203','Enterprise Performance','100','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Enterprise Performance','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('204','Process','100','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Process','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('205','Program & Project','100','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Program & Project','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('206','Business Strategy','100','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Business Strategy','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('207','Enterprise Architecture','100','91',(SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Enterprise Architecture','');
//SubCat end
// Enterprise Strategic Planning & Effectiveness END


//People & Organization START
//Cat
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('102','People & Organization','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'People & Organization','');
//Subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('211','Individual','102','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Individual','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('212','Contact','102','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Contact','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('213','Organization','102','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Organization','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('214','Party','102','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Party','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('215','Persona','102','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Persona','');
//Subcat end
//People & Organization END


//Information Technology Start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('103','Information Technology','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Information Technology','');
//Subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('221','Application','103','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Application','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('222','IT Environment','103','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Information Technology','');
//Subcat end
//Information Technology end


//Human Resources start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('104','Human Resources','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Human Resources','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('231','Workforce','104','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Workforce','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('232','Candidate','104','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Candidate','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('233','Compensation','104','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Compensation','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('234','People Survey','104','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'People Survey','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('235','Performance','104','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Performance','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('236','Training','104','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Training','');
//subcat end
//Human Resources end


//Enterprise Risk start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('105','Enterprise Risk','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Enterprise Risk','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('241','Business Continuity','105','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Business Continuity','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('242','Security','105','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Security','');
//subcat end
//Enterprise Risk end


//Finance start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('106','Finance','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Finance','');
//subcat start 
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('251','Accounts Payable','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('252','Accounts Receivable','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('253','Asset','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('254','Capital','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('255','Financial Planning','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('256','General Ledger','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('257','Tax','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('258','Treasury','106','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Accounts Payable','');
//subcat end
//Finance end


// Legal start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('107','Legal','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Legal','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('261','Governance and Compliance','107','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Governance and Compliance','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('262','Intellectual Property','107','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Intellectual Property','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('263','Legal Agreement','107','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Legal Agreement','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('264','Legal Matter','107','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Legal Matter','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('265','Sales Agreement','107','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Sales Agreement','');
//subcat end
//Legal end


//Marketing start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('108','Marketing','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Marketing','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('271','Campaign','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Campaign','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('272','Interactive Marketing','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Interactive Marketing','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('273','Market Offering','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Market Offering','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('274','Marketing Communication','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Marketing Communication','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('275','Marketing Content','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Marketing Content','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('276','Marketing Event','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Marketing Event','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('277','Marketing Lead','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Marketing Lead','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('278','Marketing Program','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Marketing Program','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('279','Marketing Target','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Marketing Target','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('280','Partner Channel','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Partner Channel','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('281','Product Branding','108','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Product Branding','');
//subcat end
//Marketing END


//Product Start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('109','Product','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Product','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('291','Bill of Materials','109','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Bill of Materials','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('292','Component','109','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Component','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('293','Elemental Product','109','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Elemental Product','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('294','Fulfillment Item','109','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Fulfillment Item','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('295','Product Group Portfolio','109','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Product Group Portfolio','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('296','Product Specification','109','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Product Specification','');
//subcat end
//Product end


//Sales start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('110','Sales','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Sales','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('301','Customer Account','110','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Customer Account','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('302','Fee and Rebate','110','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Fee and Rebate','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('303','Sales Activity','110','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Sales Activity','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('304','Sales Opportunity','110','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Sales Opportunity','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('305','Sales Order','110','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Sales Order','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('306','Partner Account','110','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Partner Account','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('307','Sales Quote','110','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Sales Quote','');
//subcat end
//Sales end


//Supply Chain start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('111','Supply Chain','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Supply Chain','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('311','Fulfillment Delivery','111','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Fulfillment Delivery','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('312','Inventory','111','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Inventory','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('313','Manufacturing','111','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Manufacturing','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('314','Work Effort','111','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Work Effort','');
//subcat end
//Supply Chain end


//Agriculture & Food start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('112','Agriculture & Food','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Agriculture & Food','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('321','Farming','112','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Farming','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('322','Fishery','112','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Fishery','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('323','Poultry','112','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Poultry','');
//subcat end
//Agriculture & Food end


//Infrastructure start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('113','Infrastructure','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Infrastructure','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('331','Telecommunications','113','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Telecommunications','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('332','Roads','113','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Roads','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('333','Transportation','113','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Transportation','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('334','Electricity','113','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Electricity','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('335','Water Supply','113','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Water Supply','');
//subcat end
//Infrastructure end


//Education start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('114','Education','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Education','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('341','Primary','114','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Primary','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('342','Higher','114','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Higher','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('343','Teaching','114','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Teaching','');
//subcat end
//Education end


//Employment start 
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('115','Employment','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Employment','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('351','Opportunities','115','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Opportunities','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('352','Jobs','115','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Jobs','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('353','Skilled','115','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Skilled','');
//subcat end
//Employment end


//Energy start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('116','Energy','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Energy','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('361','Atomic','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Atomic','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('362','Thermal','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Thermal','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('363','Solar','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Solar','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('364','Hydro','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Hydro','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('365','Unconventional','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Unconventional','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('366','Conventional','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Conventional','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('367','Fossil Fuels','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Fossil Fuels','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('368','Electric','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Electric','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('369','Wind','116','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Wind','');
//subcat end
//Energy end


//Environment start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('117','Environment','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Environment','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('371','Forests','117','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Forests','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('372','Pollution','117','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Pollution','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('373','Water','117','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Water','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('374','Air','117','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Air','');
//subcat end
//Environment end


//Economics start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('118','Economics','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Economics','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('381','Finance','118','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Finance','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('382','Investment','118','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Investment','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('383','Taxation','118','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Taxation','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('384','GDP','118','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'GDP','');
//subcat end
//Economics end


//Trade  start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('119','Trade','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Trade','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('391','Export','119','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Export','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('392','Import','119','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Import','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('393','Sale','119','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Sale','');
//subcat end
//Trade  end


//Governance start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('120','Governance','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Governance','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('401','Elections','120','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Elections','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('402','Law','120','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Law','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('403','Administration','120','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Administration','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('404','Policy','120','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Policy','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('405','Politics','120','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Politics','');
//subcat end
//Governance end


//Industry & Services start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('121','Industry & Services','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Industry & Services','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('411','Manufacturing','121','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Manufacturing','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('412','Hospitality','121','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Hospitality','');
//subcat end
//Industry & Services end


//Science & Technology start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('122','Science & Technology','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Science & Technology','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('421','Research','122','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Research','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('422','Analysis','122','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Analysis','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('423','Biology','122','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Biology','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('424','IT','122','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'IT','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('425','Engineering','122','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Engineering','');
//subcat ebd
//Science & Technology end


//Social start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('123','Social','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Social','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('431','Migration','123','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Migration','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('432','Health','123','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Health','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('433','Census','123','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Census','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('434','Crime','123','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Crime','');
//subcat end
//Social start


// Transport start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('124','Transport','0','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Transport','');
//subcat start
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('441','Surface','124','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Surface','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('442','Water','124','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Water','');
INSERT INTO category_subcategory_master(`id`,`catOrSubCatName`,`parentId`,`createdBy`,`createdDate`,`modifiedDate`,`modifiedBy`,`status`,`shortDescription`,`longDescription`) VALUES('443','Air','124','91', (SELECT ROUND(UNIX_TIMESTAMP(CURTIME(4)) * 1000)),'','0','1', 'Air','');
//subcat end
// Transport end


alter table app_output_files add baapJobInstId varchar(40);

alter table datasourceinstance add accessType varchar(100);
alter table datasource_service_apimanager add query varchar(500);

#Create table with current date folder.
mkdir -p "$MAXIQ_HOME"/MysqlDump/$(date '+%d-%b-%Y')

#Start dumping the backup from database
mysqldump -u root maxiq > "$MAXIQ_HOME"/MysqlDump/$(date '+%d-%b-%Y')/maxiq.sql

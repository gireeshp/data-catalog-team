#!/bin/bash


k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"


su hdfs << HERE



export HADOOP_CONF_DIR=/etc/hadoop/conf/


/softwares/spark/spark-1.5.1-bin-hadoop2.4/bin/spark-submit  --verbose --master yarn-cluster --class  com.augmentiq.maxiq.workFlow.run.KickStartSparkDAG --jars /home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-api-jdo-3.2.6.jar,/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-rdbms-3.2.9.jar,/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-core-3.2.10.jar,$x --files /home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/conf/hive-site.xml  --deploy-mode cluster --conf spark.sql.hive.metastore.jars=/usr/lib/hive/lib/*:/usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-hdfs/* --conf spark.sql.hive.metastore.version=0.13.0 --driver-memory 4g --executor-memory 5g  --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar '$1' $2 $3 $4

HERE



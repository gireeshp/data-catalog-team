#!/bin/bash
## Created By
## 10642747

function usage {
    echo "USAGE"
    echo "  -s/--showHeader      (true/falsee)headers should come in the output or not"
    echo "  -u/--username        the username to connect as"
    echo "  -p/--password        the password to connect as"
    echo "  -c/--connectionUrl   the JDBC URL to connect to"
    echo "  -d/--delimeter       delimeter to be used in the output"
    echo "  -q/--query           query to execute"
    echo "  -f/--fileForOutput   file to store output of the query"
    exit 1
}

if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    usage;
    exit;
fi

while [[ $# -gt 1 ]]
do
        key="$1"

        case $key in
            -s|--showHeader)
            SHOWHEADER="$2"
            shift # past argument
            ;;
            -u|--username)
            USERNAME="$2"
            shift # past argument
            ;;
            -p|--password)
            PASSWORD="$2"
            shift # past argument
            ;;
            -c|--connectionUrl)
            CONNECTIONURL="$2"
            shift # past argument
            ;;
            -q|--query)
            QUERY="$2"
            shift # past argument
            ;;
            -f|--fileForOutput)
            FILEFOROUTPUT="$2"
            shift # past argument
            ;;
            -d|--delimeter)
            DELIMETER="$2"
            shift # past argument
            ;;
            --default)
            DEFAULT=YES
            ;;
            *)
                    # unknown option
            ;;
        esac
        shift # past argument or value
done

echo SHOWHEADER  = "${SHOWHEADER}"
echo USERNAME    = "${USERNAME}"
echo PASSWORD  = "${PASSWORD}"
echo CONNECTIONURL     = "${CONNECTIONURL}"
echo QUERY    = "${QUERY}"
echo FILEFOROUTPUT    = "${FILEFOROUTPUT}"
echo DELIMETER    = "${DELIMETER}"

if [[ -z "$SHOWHEADER" || -z "$USERNAME" || -z "$PASSWORD" || -z "$CONNECTIONURL" || -z "$QUERY" || -z "$FILEFOROUTPUT" || -z "$DELIMETER" ]]
  then
    echo "Please provide all the necessary arguments"
    usage
fi

## Create parent directory if not exist
DIR=$(dirname $FILEFOROUTPUT)
if [ ! -d $DIR  ]
  then
    echo "directory $DIR does not exist. Creating it.."
    mkdir -p $DIR
fi


echo "Executing command: beeline --outputformat=dsv --silent=true --showHeader=$SHOWHEADER --delimiterForDSV=$DELIMETER -n $USERNAME -p $PASSWORD -u $CONNECTIONURL --nullemptystring=true -e \"$QUERY\" > $FILEFOROUTPUT"

beeline --outputformat=dsv \
        --silent=true \
        --showHeader=$SHOWHEADER \
        --delimiterForDSV=$DELIMETER \
        -n $USERNAME \
        -p $PASSWORD \
        -u $CONNECTIONURL \
        --nullemptystring=true \
        -e "$QUERY" > $FILEFOROUTPUT
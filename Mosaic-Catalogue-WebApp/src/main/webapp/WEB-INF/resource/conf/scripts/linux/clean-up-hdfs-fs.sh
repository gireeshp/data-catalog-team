#!/bin/bash

echo "Switching user"

list=`echo "$1" | tr '-' ' '`
stage1AllFiles=`hadoop fs -ls $2/stage1/* | awk '{print $8}'`
stage2AllFiles=`hadoop fs -ls $2/stage2/* | awk '{print $8}'`


checkExitFunction(){
	
	if [[ "${list[@]}" =~ "${1}" ]]; then
   		echo "Value contains" + $1
	else
		
                /home/hdfs/maxiq/CreateScript/deleteHdfcFiles $1 		 	
   		echo "delete file Name" + $1
			
	fi
}

for hdfcFile in $stage1AllFiles
do 
	tmp="$hdfcFile/"				
	echo "display stage1 :" $tmp
	checkExitFunction $tmp  
done 


for stage2 in $stage2AllFiles
do
        tmp="$stage2/"
        echo "display stage2 : " $tmp
        checkExitFunction $tmp
done


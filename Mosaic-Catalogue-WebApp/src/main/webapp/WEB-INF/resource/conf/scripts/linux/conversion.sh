#!/bin/bash

echo "Switching user"

k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"

echo $x

su hdfs << HERE

# $1 is job instance id
# $2 is extraction job instance id
# $3 is data source id
# $4 is maxiq data path
# $5 is inputFile type
# $6 is inputCompression type
# $7 is OutputFileType
# $8 is OutputCompressionType

echo "Started Creating Data Source."

#export HADOOP_CLASSPATH=$(hbase classpath):/usr/hdp/2.3.0.0-2557/hbase/conf;
export HADOOP_CLASSPATH=/usr/lib/ams-hbase/lib/*
export HADOOP_CONF_DIR=/etc/hadoop/conf/

hadoop jar "$MAXIQ_HOME"/libs/MaxiqDataProcessor-"$MAXIQ_VERSION".jar com.augmentiq.maxiq.hadoop.DataSource.Conversion.ConvertDriver -Dfs.permissions.umask-mode=000  -Dyarn.app.mapreduce.am.staging-dir=/maxiq  $1 $2 $3 $4 $5 $6 $7 $8


#"$4/stage1/$3/$2"
#"$4/stage2/$3/$1"
#hadoop fs -rm -r -skipTrash "$4/stage1/$3/"

echo "Conversion is over."
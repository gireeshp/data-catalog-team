#!/bin/bash

name=$(hadoop fs -cat $1* | wc -l)

echo "value of count " ${name}

java -cp "$MAXIQ_HOME"/libs/MaxiqAgent-"$MAXIQ_VERSION".jar com.augmentiq.maxiq.kafka.UpdateRecordCountForQuickDs $2 $name


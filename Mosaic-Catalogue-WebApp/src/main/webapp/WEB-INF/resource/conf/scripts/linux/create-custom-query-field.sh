#!/bin/bash

k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"


class=com.augmentiq.maxiq.spark.customQuery.GenerateOpFieldFromQuery;

if [ "$3" == "true" ]
then
        class=com.augmentiq.maxiq.inputparameters.CalculateInputParametersAndSave
fi


su hdfs <<HERE

export HADOOP_CONF_DIR=/etc/hadoop/conf/

CLASSPATH=$(echo /usr/hdp/current/spark-client/lib/*.jar | tr ' ' ',')

echo $x;

echo "class is " $class

spark-submit  --class $class \
              --verbose \
              --jars $x \
              --master local[8] \
              --files /etc/hive/conf/hive-site.xml  \
              --driver-java-options "-XX:MaxPermSize=1G -Dlog4j.configuration=file:"$MAXIQ_HOME"/conf/error_conf_local_agent.xml" \
              "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4 $5

HERE

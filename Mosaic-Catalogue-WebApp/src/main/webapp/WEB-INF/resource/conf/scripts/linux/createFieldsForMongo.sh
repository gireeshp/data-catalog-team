#!/bin/bash

k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"





echo $x

spark-submit  --class com.augmentiq.maxiq.spark.mongo.GenerateFieldMappingForMongo \
              --verbose \
              --master local[8]  \
              --files $SPARK_HOME/conf/core-site.xml  \
              --conf hive.execution.engine=mr \
	      --conf spark.shuffle.compress=true \
              --driver-memory 1g \
              --executor-memory 2g \
              --executor-cores 2 \
              --name field_Generate \
              --num-executors 2 \
              --driver-java-options "-XX:MaxPermSize=1G -Dlog4j.configuration=file:"$MAXIQ_HOME"/conf/error_conf_local_agent.xml" \
		"$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4

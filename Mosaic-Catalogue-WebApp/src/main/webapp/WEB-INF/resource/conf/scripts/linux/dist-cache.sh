#!/bin/bash

su hdfs << HERE

# $1 Path to copy
# $2 ComponentId

echo '/maxiq/distCache/$2'

hadoop fs -rm -r -skipTrash /maxiq/distCache/$2
hadoop fs -put $1 /maxiq/distCache/

echo 'FINISHED'

HERE




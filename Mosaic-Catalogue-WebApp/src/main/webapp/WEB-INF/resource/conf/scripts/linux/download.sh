#!/bin/bash -f

echo "@1 param output path" $1
echo "@2 param delimiter " $2
echo "@3 paramter query " $3
echo "@4 param queueName" $4
echo "@5 param header" $5
echo "@6 param fileName" $6

x=${3//\'/\\\'}

if [ $2 == 'csv' ]
then 
     del=,
elif [ $2 == "tsv" ]
then 
      del=\\t
else 
      del=$2
fi

if [ $5 == "true" ]
then
  STR="| sed 's/[\t]/$del/g' >> $1/appendHeader.txt"
fi


fileContent=$(cat $1/appendheader.txt);
echo $fileContent "file content"

echo $STR
echo "Populated delimiter is :- " $del  	

mkdir -p $1

#echo "Query submitted to hh" hive -v  -e $'\"set mapred.job.queue.name=$4;set hive.cli.print.header=true;set hive.typecheck.on.insert=true;insert overwrite local directory \'$1\' row format delimited fields terminated by \'$del\' $x ;"' 

#echo hive -e $"\"set mapred.job.queue.name=$4;set hive.cli.print.header=true;insert overwrite  local directory '$1' row format delimited fields terminated by '$del' NULL DEFINED AS '$x';\""


hive -e $"\"set mapred.job.queue.name=$4;set hive.cli.print.header=true;insert overwrite  local directory '$1' row format delimited fields terminated by '$del' NULL DEFINED AS '' $x;\"" $STR

echo "$fileContent" >> $1/appendHeader.txt


#HERE

"$MAXIQ_HOME"/bin/merge-downloads-file.sh $1 $6
#!/bin/bash

#$1 table name 
#$2 export path 
#$3  delimiter

hive -e "INSERT OVERWRITE LOCAL DIRECTORY '$2'  ROW FORMAT  delimited fields terminated by '$3'  select * from $1 "
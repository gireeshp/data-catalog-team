#!/bin/bash

echo "name of file" $3

su hdfs << HERE

# $1 is job instance id
# $2 is data source id
# $3 is local file path
# $4 is previous file path
# $5 is maxiq data path

echo $1
echo $2
echo $3
echo $5

hadoop fs -mkdir $5/ds/stage2/$2
echo "created new ds directory"

hadoop fs -mkdir $5/ds/stage2/$2/$1
echo "created new job directory"

hadoop fs -mkdir $5/ds/stage2/$2/$1/OUT/

echo "Started Moving files into Hadoop File System"

hadoop fs -copyFromLocal $3 $5/ds/stage2/$2/$1/OUT/

if [ ! -z "$4" -a "$4" != " " ]
then
        echo "Copying old files"
        hadoop fs -cp $4/* $5/ds/stage2/$2/$1/OUT/
fi

HERE

"$MAXIQ_HOME"/bin/count-for-stp-file.sh $5/ds/stage2/$2/$1/OUT/  $2

#!/bin/bash

echo "Switching user"
su hdfs << HERE

# $1 is job instance id
# $2 is data source id
# $3 is local file path
# $4 is type of extraction
# $5 is maxiq data path

echo $1
echo $2
echo $3
echo $4
echo $5


# The following if condition will create a file in the location for replace all and Append kind of incremental type.
if [ "$4" == "APPEND" ]
then
    echo "created new job directory"
    hadoop fs -mkdir -p $5/ds/stage1/$2/$1
    echo "Started Moving files into Hadoop File System"
    echo "hadoop fs -copyFromLocal $3 $5/ds/stage1/$2/$1"
    hadoop fs -copyFromLocal $3 $5/ds/stage1/$2/$1
    echo "Transfer of files in over"
else
# This will create the folder for the merge/delete file path
    echo "created new job directory"
    hadoop fs -mkdir -p $5/ds/stage1/$2/$4/$1
    echo "Started Moving files into Hadoop File System"
    echo "hadoop fs -copyFromLocal $3 $5/ds/stage1/$2/$4/$1"
    hadoop fs -copyFromLocal $3 $5/ds/stage1/$2/$4/$1
    echo "Transfer of files in over"
fi

HERE
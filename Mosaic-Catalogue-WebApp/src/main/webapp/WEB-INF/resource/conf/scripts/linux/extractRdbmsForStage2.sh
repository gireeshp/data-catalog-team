#!/bin/bash

GLOBIGNORE=*

echo $1
echo $4
hdfsFolder=$4/stage2/$1/$2/OUT/

su hdfs << HERE
echo "Switching user"

GLOBIGNORE=*

echo "Starting exctraction for "$1

echo "outfile file create folder " + $hdfsFolder

hadoop jar "$MAXIQ_HOME"/libs/MaxiqDataProcessor-"$MAXIQ_VERSION".jar com.augmentiq.maxiq.hadoop.sqoop.SqoopClients $1 $2 $hdfsFolder $4

if [ ! -z "$3" -a "$3" != " " ]
then
        echo "Copying old files"
        hadoop fs -cp $3/* $hdfsFolder
else
        echo "No files"
fi

echo "Done exctraction for "$1

HERE

hadoopFileSize=`hadoop fs -ls $4/stage2/$1/$2/OUT/ | awk '{ print $5}'`

tmp=0
for i in $hadoopFileSize
do
        tmp=$((tmp + i))
done

totalNumberOfRecord=`hadoop fs -cat $4/stage2/$1/$2/OUT/* | wc -l`

java -cp "$MAXIQ_HOME"/libs/MaxiqAgent-"$MAXIQ_VERSION".jar com.augmentiq.maxiq.kafka.SendMessageUsingProd RDBMS_DATA_SOURCE $2 $1  $tmp $totalNumberOfRecord


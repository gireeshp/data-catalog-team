#!/bin/bash


list=`echo "$1" | tr '-' ' ' `
TOTAL_USED_FILE_SIZE=`hadoop fs -du -s $list | awk '{print $1}'`
OVERALL_SIZE=`hadoop fs -du -s /maxiq/ds/ | awk '{print $1}'`
size=0
for i in $TOTAL_USED_FILE_SIZE
do
   size=$((size + i))
done
echo "total used size : " $size
echo "overall Size :" $OVERALL_SIZE

hostname=`hostname -f`

java -cp "$MAXIQ_HOME"/libs/MaxiqAgent-"$MAXIQ_VERSION".jar com.augmentiq.maxiq.kafka.SendMessageUsingProd HdfcStats $hostname $size $OVERALL_SIZE 




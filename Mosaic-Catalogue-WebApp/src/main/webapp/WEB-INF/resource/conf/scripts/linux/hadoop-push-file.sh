#$1 input path
#$2 Output Path

su hdfs << HERE

echo "$1"

hadoop fs -rm -r -skipTrash $2

hadoop fs -mkdir $2

hadoop fs -put $1 $2


HERE

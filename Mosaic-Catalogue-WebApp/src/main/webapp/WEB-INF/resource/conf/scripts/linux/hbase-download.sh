#$1 is filePath
#$2 is fileType
#$3 is query

mkdir $1

/usr/hdp/current/phoenix-server/bin/sqlline.py localhost:2181 <<END

!outputformat $2
!record $1/tmp.$2
$3;
!record
!quit

END

sed '1d' $1/tmp.$2 > $1/tmp2.$2
sed '1d' $1/tmp2.$2 > $1/tmp3.$2
sed '/rows selected (/d' $1/tmp3.$2 > $1/sample.$2

#cat $1/tmp5.$2 > $1/sample.$2

rm -rf $1/tmp*
#!/bin/bash

k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"

CLASSPATH=$(echo /usr/hdp/current/spark2-client/jars/data*.jar | tr ' ' ',')

export HADOOP_CLASSPATH=/usr/lib/ams-hbase/lib/hbase-protocol-1.1.2.2.3.2.0-2849.jar
export HADOOP_CONF_DIR=/etc/hadoop/conf/

echo $x

spark-submit  --class com.augmentiq.maxiq.spark.index.StartSparkIndexing \
              --verbose \
              --master yarn-cluster   \
              --jars $CLASSPATH \
              --files /etc/hive/conf/hive-site.xml  \
              --deploy-mode cluster \
              --conf yarn.log-aggregation-enable=true  \
	      --conf hive.execution.engine=mr \
              --conf spark.shuffle.compress=true \
              --driver-memory $6 \
              --executor-memory $7 \
              --executor-cores $9 \
	      --name $3 \
              --queue $4 \
              --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_AMITESH"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $5
              --num-executors $8 \


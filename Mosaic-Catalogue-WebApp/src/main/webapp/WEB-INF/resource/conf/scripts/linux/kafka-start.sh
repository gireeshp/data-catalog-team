#!/bin/bash

KAFKA_HOME="/softwares/kafka_2.9.1-0.8.2.1"

nohup "${KAFKA_HOME}"/bin/kafka-server-start.sh   "${KAFKA_HOME}"/config/server.properties >  /dev/null 2>&1 &

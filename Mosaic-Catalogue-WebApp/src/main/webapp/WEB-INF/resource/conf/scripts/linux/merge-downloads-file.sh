k=""

# copying header to final output file

tail -n 1 $1/appendHeader.txt >> "$1/$2".txt

# check a flag for application killed
isCheckExist=0

for i in $1/*
do
   if [ $i != $1/$2.txt ]
   then
        isCheckExist=1
        k+="$i "
   fi
done;

x="${k%?}"

rm -fr $1/appendHeader.txt

# verify if output is generate or not
if [ $isCheckExist == "1" ]
then
    cat $x >> "$1/$2".txt
fi

"$MAXIQ_HOME"/bin/delete-unused-files.sh $1/

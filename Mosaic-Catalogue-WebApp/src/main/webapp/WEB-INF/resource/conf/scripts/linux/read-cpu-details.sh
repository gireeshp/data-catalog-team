RamDetail=`free -m | grep Mem`

CPUUtilization=`mpstat | awk '$3 ~ /CPU/ { for(i=1;i<=NF;i++) { if ($i ~ /%idle/) field=i } } $3 ~ /all/ { print 100 - $field }' `
FreeRamMemory=`echo $RamDetail | awk '{ print $3 }'`  
TotalRamMemory=`echo $RamDetail | awk '{ print $2 }'`  
HostName=$(hostname) 
TOTAL_HDD_VALUE=`df -Pk | grep  "/" | awk '{ print $2} '  | awk '{s+=$1} END {print s/(1000*1000)}'`

TOTAL_HDD_USED=`df -Pk | grep  "/" | awk '{ print $3} '  | awk '{s+=$1} END {print s/(1000*1000)}'`

echo $TOTAL_HDD_VALUE
echo $TOTAL_HDD_USED


java -cp  "$MAXIQ_HOME"/libs/MaxiqAgent-"$MAXIQ_VERSION".jar  com.augmentiq.maxiq.kafka.SendMessageUsingProd  $FreeRamMemory $TotalRamMemory  $CPUUtilization  $HostName  $TOTAL_HDD_VALUE $TOTAL_HDD_USED echo date

echo "FREE Memory : " $FreeRamMemory
echo "Total Ram Memmory :" $TotalRamMemory
echo $HostName
echo $CPUUtilization 

KAFKA_HOME="/softwares/kafka_2.9.1-0.8.2.1/"

#kafka server keeping references of topics even deleted so need to stop kafka server before deleting topics
"${KAFKA_HOME}"bin/kafka-server-stop.sh

for var in "$@"
do
   echo " create or replace $var topic"
   "${KAFKA_HOME}"bin/kafka-run-class.sh kafka.admin.DeleteTopicCommand --zookeeper localhost:2181 --topic "$var"

done

"${KAFKA_HOME}"bin/kafka-server-start.sh  -daemon "${KAFKA_HOME}"config/server.properties

for var in "$@"
do
   echo " create or replace $var topic"
   "${KAFKA_HOME}"bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic "$var"

done

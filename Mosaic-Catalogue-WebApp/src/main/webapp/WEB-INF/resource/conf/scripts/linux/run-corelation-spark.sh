#!/bin/bash

#su hdfs << HERE

#export HADOOP_CONF_DIR=/etc/hadoop/conf

#/softwares/spark/spark-1.4.1-bin-hadoop2.6/bin/spark-submit  --class com.augmentiq.maxiq.workFlow.run.kickCorelationStart  --master yarn-master --conf HADOOP_CONF_DIR=/etc/hadoop/conf/ --deploy-mode cluster --jars /softwares/spark/spark-1.4.1-bin-hadoop2.6/lib/datanucleus-api-jdo-3.2.6.jar,/softwares/spark/spark-1.4.1-bin-hadoop2.6/lib/datanucleus-rdbms-3.2.9.jar,/softwares/spark/spark-1.4.1-bin-hadoop2.6/lib/datanucleus-core-3.2.10.jar,/softwares/mysql-connector-java-5.1.17.jar --files /softwares/spark/spark-1.4.0-bin-hadoop2.6/conf/hive-site.xml  --driver-java-options "-XX:MaxPermSize=1G"  --driver-memory 12g --executor-memory 18g  "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4 $5 $6


#export HADOOP_CONF_DIR=/etc/hadoop/conf

#/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/bin/spark-submit  --class com.augmentiq.maxiq.workFlow.run.kickCorelationStart --verbose --master yarn-cluster    --jars /home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-api-jdo-3.2.6.jar,/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-rdbms-3.2.9.jar,/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-core-3.2.10.jar --files /home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/conf/hive-site.xml  --deploy-mode cluster --conf yarn.log-aggregation-enable=true  --driver-memory 4g --executor-memory 6g  --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4 $5


k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"

CLASSPATH=$(echo /usr/hdp/current/spark-client/lib/data*.jar | tr ' ' ',')

su hdfs << HERE

export HADOOP_CONF_DIR=/etc/hadoop/conf/

spark-submit  --class com.augmentiq.maxiq.workFlow.run.kickCorelationStart \
              --verbose \
              --master yarn-cluster   \
              --jars $CLASSPATH \
              --files /etc/hive/conf/hive-site.xml  \
              --deploy-mode cluster \
              --conf yarn.log-aggregation-enable=true  \
              --driver-memory 2g \
              --executor-memory 10g \
              --num-executors 1 \
              --name $6 \
              --queue $7 --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4 $5 $6

HERE

#!/bin/bash

su hdfs << HERE

export HADOOP_CLASSPATH=/usr/lib/ams-hbase/lib/hbase-protocol-1.1.2.2.3.2.0-2849.jar
export HADOOP_CONF_DIR=/etc/hadoop/conf/


/softwares/maxiq/maxiq/SparkSetup/spark-2.0.2-bin-hadoop2.7/bin/spark-submit  --verbose \
              --master yarn  \
              --files /etc/hive/conf/hive-site.xml  \
              --driver-memory 2g \
              --executor-memory 4g \
              --name $2 \
              --num-executors 2 \
              --queue $3 $1

HERE
#!/bin/bash

#su hdfs << HERE

#export HADOOP_CONF_DIR=/etc/hadoop/conf/


##/softwares/spark/spark-1.4.1-bin-hadoop2.4/bin/spark-submit  --verbose --master yarn-cluster --class com.augmentiq.maxiq.spark.profile.Profiler --jars /softwares/spark/spark-1.4.1-bin-hadoop2.4/lib/datanucleus-api-jdo-3.2.6.jar,/softwares/spark/spark-1.4.1-bin-hadoop2.4/lib/datanucleus-rdbms-3.2.9.jar,/softwares/spark/spark-1.4.1-bin-hadoop2.4/lib/datanucleus-core-3.2.10.jar,$x --files /softwares/spark/spark-1.4.1-bin-hadoop2.4/conf/hive-site.xml  --deploy-mode cluster --conf spark.sql.hive.metastore.jars=/usr/lib/hive/lib/*:/usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-hdfs/* --conf spark.sql.hive.metastore.version=0.13.0 --conf yarn.log-aggregation-enable=true --driver-memory 4g --executor-memory 6g  --driver-java-options "-XX:MaxPermSize=1G" "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4 $5



#/softwares/spark/spark-1.4.1-bin-hadoop2.6/bin/spark-submit  --class com.augmentiq.maxiq.spark.profile.Profiler --verbose --master yarn-cluster  --jars /softwares/spark/spark-1.4.1-bin-hadoop2.6/lib/datanucleus-api-jdo-3.2.6.jar,/softwares/spark/spark-1.4.1-bin-hadoop2.6/lib/datanucleus-rdbms-3.2.9.jar,/softwares/spark/spark-1.4.1-bin-hadoop2.6/lib/datanucleus-core-3.2.10.jar,/softwares/mysql-connector-java-5.1.17.jar --files /softwares/spark/spark-1.4.1-bin-hadoop2.6/conf/hive-site.xml  --deploy-mode cluster --conf yarn.log-aggregation-enable=true  --driver-memory 10g --executor-memory 14g  --driver-java-options "-XX:MaxPermSize=1G" "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4 $5


##/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/bin/spark-submit  --class com.augmentiq.maxiq.spark.profile.Profiler --verbose --master yarn-cluster    --jars /home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-api-jdo-3.2.6.jar,/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-rdbms-3.2.9.jar,/home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/lib/datanucleus-core-3.2.10.jar --files /home/hdfs/maxiq/shellScripts/spark/spark-1.4.0-bin-hadoop2.4/conf/hive-site.xml  --deploy-mode cluster --conf yarn.log-aggregation-enable=true  --driver-memory 4g --executor-memory 6g  --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4

k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"

CLASSPATH=$(echo /usr/hdp/current/spark-client/lib/data*.jar | tr ' ' ',')

su hdfs << HERE

export HADOOP_CONF_DIR=/etc/hadoop/conf/

spark-submit  --class com.augmentiq.maxiq.spark.profile.Profiler \
	      --verbose \
              --master yarn-cluster   \
              --jars $CLASSPATH \
              --files /etc/hive/conf/hive-site.xml  \
              --deploy-mode cluster \
              --conf yarn.log-aggregation-enable=true  \
              --driver-memory 2g \
              --executor-memory 10g \
              --name $7 \
              --num-executors 1 \
	      	  --queue $8 \
	      	  --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $4 $5

HERE

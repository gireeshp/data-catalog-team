#!/bin/sh


#$1 is indicator
#$2 is jarName
#$3 is className
#$4 is inputPath
#$5 is OutputPath
#$6 is Map of JSON
#$7 is Input File type
#$8 is Input Compress type
#$9 is Output File type
#$10 is Output Compress type
#$11 is queueName
su hdfs<<HERE

echo 'hadoop jar $2 $3 -Dmapreduce.job.queuename=$11  '$4' $5 '$6' $7 $8 $9 $10'

if [ "$1" == java ]; then 
    java -cp $2 $3 $4
else hadoop jar $2 $3 -Dmapreduce.job.queuename=$11  '$4' '$5' '$6' '$7' '$8' '$9' '$10'
fi

HERE 
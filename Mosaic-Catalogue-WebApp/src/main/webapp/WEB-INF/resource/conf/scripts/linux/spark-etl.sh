#!/bin/bash


k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"

CLASSPATH=$(echo /usr/hdp/current/spark-client/lib/data*.jar | tr ' ' ',')

echo $7 $8 $9 
su hdfs << HERE

export HADOOP_CLASSPATH=/usr/lib/ams-hbase/lib/hbase-protocol-1.1.2.2.3.2.0-2849.jar
export HADOOP_CONF_DIR=/etc/hadoop/conf/

              #--conf spark.driver.extraClassPath=/softwares/maxiq/libs/custom/DatamartCore.jar \
echo $x

spark-submit  --class com.augmentiq.maxiq.spark.handler.SparkHandler \
              --verbose \
              --master yarn-cluster   \
	      --jars $CLASSPATH\
              --files /etc/hive/conf/hive-site.xml  \
              --deploy-mode cluster \
              --conf yarn.log-aggregation-enable=true  \
              --conf hive.execution.engine=mr \
              --conf spark.shuffle.compress=true \
	      --driver-memory $7 \
              --executor-memory $8 \
              --executor-cores ${10} \
	      --num-executors $9 \
	      --name $4 \
              --queue $5 \
              --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_AMITESH"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3 $6

HERE


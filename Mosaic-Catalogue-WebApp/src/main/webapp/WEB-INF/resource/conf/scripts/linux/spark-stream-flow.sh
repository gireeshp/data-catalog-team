#!/bin/bash

export SPARK_DIST_CLASSPATH=$(hadoop classpath)
k=""
for i in `ls "$MAXIQ_HOME"/libs/custom/*.jar`
do
 k+="$i,"
done;

x="${k%?}"

CLASSPATH=$(echo /usr/hdp/current/spark-client/lib/data*.jar | tr ' ' ',')


su hdfs << HERE

export HADOOP_CLASSPATH=/usr/lib/ams-hbase/lib/*.jar
export HADOOP_CONF_DIR=/etc/hadoop/conf/

echo $x

/softwares/maxiq/maxiq/SparkSetup/spark-2.0.2-bin-hadoop2.7/bin/spark-submit --class com.augmentiq.maxiq.workFlow.stream.run.KickStartSparkStreamDAG \
              --verbose \
              --master yarn-cluster   \
              --jars $CLASSPATH \
              --files /etc/hive/conf/hive-site.xml  \
              --deploy-mode cluster \
              --conf yarn.log-aggregation-enable=true  \
              --conf hive.execution.engine=mr \
              --conf "spark.driver.extraClassPath=/usr/hdp/2.3.4.0-3485/hbase/lib/hbase-protocol-1.1.2.2.3.4.0-3485.jar" \
              --conf "spark.executor.extraClassPath=/usr/hdp/2.3.4.0-3485/hbase/lib/hbase-protocol-1.1.2.2.3.4.0-3485.jar" \
              --conf spark.shuffle.compress=true \
              --driver-memory $4 \
              --executor-memory $5 \
              --executor-cores $7 \
              --name $2 \
              --num-executors $6 \
              --queue $3 \
              --driver-java-options "-XX:MaxPermSize=1G"  "$MAXIQ_HOME"/libs/MaxiqAppProcessor-"$MAXIQ_VERSION".jar $1 $2 $3

HERE

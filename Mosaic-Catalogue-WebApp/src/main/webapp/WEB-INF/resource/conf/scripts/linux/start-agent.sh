#!/bin/bash

nohup java -Dlog4j.configuration=file:"$MAXIQ_HOME"/conf/error_conf_agent.xml -Dmosaic.instance.type=agent -cp "$MAXIQ_HOME"/new:"$MAXIQ_HOME"/external_libs/phoenix-4.7.0.2.5.6.0-40-client.jar:"$MAXIQ_HOME"/libs/MaxiqAgent-"$MAXIQ_VERSION".jar:"$MAXIQ_HOME"/external_libs/*:. com.augmentiq.maxiq.agent.rabbitmq.MessageConsumer > /dev/null 2>&1 &


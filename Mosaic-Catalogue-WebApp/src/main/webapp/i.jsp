<!doctype html>
<!--[if gt IE 8]><!-->
<html class="no-js" data-ng-app="app">

<!--<![endif]-->
<head>
<meta http-equiv="content-type" content="text/html" charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

<title>MOSAIC</title>

<link rel="shortcut icon" href="styles/images/landingImg/logo_blank.png" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="styles/fontCss.css">
<link rel="stylesheet" type="text/css" href="styles/homePageCss/font-awesome.css">
<link rel="stylesheet" type="text/css" href="styles/font-awesome-animation.css">

<link rel="stylesheet" type="text/css" href="styles/custom.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/workflow.css">
<link rel="stylesheet" type="text/css" href="styles/tree.css">
<link rel="stylesheet" type="text/css" href="styles/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="styles/ui-grid.css" />
<link rel="stylesheet" type="text/css" href="styles/introjs.css" />
<link rel="stylesheet" type="text/css" href="styles/angularjs-fullscreen.css" />
<link rel="stylesheet" type="text/css" href="styles/angular-loading.css" />
<link rel="stylesheet" type="text/css" href="styles/select.css" />
<link rel="stylesheet" type="text/css" href="scripts/libs/bootstrap-toggle.min.css">
<link rel="stylesheet" type="text/css" href="styles/bootstrap-datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="styles/customCss/globalSearch.css" />
<link rel="stylesheet" type="text/css" href="styles/customCss/hover-min.css">
<link rel="stylesheet" type="text/css" href="styles/homePageCss/ladda.min.css">
<link rel="stylesheet" type="text/css" href="styles/angular-pickadate.css">
<link rel="stylesheet" type="text/css" href="styles/nv.d3.min.css">
<link rel="stylesheet" type="text/css" href="styles/newStyle.css">
<link rel="stylesheet" type="text/css" href="styles/multi-select-tree/ivh-treeview.css">
<link rel="stylesheet" type="text/css" href="styles/multi-select-tree/ivh-treeview-theme-basic.css">
<link rel="stylesheet" type="text/css" href="styles/sweetalert.css">

</head>
<body data-spy="scroll" id="app" data-custom-background=""
	data-off-canvas-nav="">
	<div data-ng-controller="AppCtrl" ng-init="dataHideAndShow = false" ng-click="closePopUpIfOpen($event)">
		<div ng-model="versionNumber" data-ng-init="versionNumber = 1.0.1"></div>
		<div ng-if='!isSpecificPage()' >
			<section data-ng-include=" 'views/header.jsp' " id="header"
				class="header-container top-header bxShodow aside"></section>
			<aside data-ng-include="'views/nav.jsp' " id="nav-container"
				class="aside nav-container nav-vertical ng-scope nav-fixed bg-light"></aside>
			<%@ include file="/views/breadcrumb.jsp"%>
		</div>
		<div class="view-container " data-role-check>
			<section data-ng-view="" style="overflow-x : {{overFlowForDesign}}; left: {{getLeftPixel()}}; top : {{hideBreadCrumb() ? '' : '50px'}};margin-top:{{marginTop}};"
				class='animate-fade-up page-about {{(isLastParam == undefined || isLastParam == "" || isLastParam == null)? "" : "topSet"}}'
				id='{{id}}'></section>
			<div id="loadHtml" style="display: none;"></div>
			<%@ include file="/views/template/NotificationPanel.html"%>
		</div>
		<footer ng-if='!isSpecificPage()' id="footer-bar"
			style="opacity: 1;">
			<p class="footer-copyright col-md-12 text-muted text-center"
				style="font-size: 13px">
				&copy;<a href="https://www.lntinfotech.com"
					target="_blank" class="cursurPoint default-color"> 2017 Larsen
					&amp; Toubro Infotech Ltd.</a> All rights reserved.
			</p>
		</footer>
	</div>
	<script>
	(function () {
        var a = window.location.href,        
        b = a.lastIndexOf("/"),
        d = a.replace("i.jsp", "");
		history.pushState('data to be passed', 'SIGNIN', a);
		history.replaceState('data to be passed', 'SIGNIN', d);
        return a.substr(b + 1);
    }());
	</script>
	<script src="scripts/libs/vendor.js"></script>
	<script src="scripts/libs/ui.js"></script>
	<script src="scripts/libs/jquery-ui.js"></script>
	
	<script src="scripts/libs/angular-cookies.js"></script>
	<script src="scripts/libs/angular-route.js"></script>
	<script src="scripts/app/app.js"></script><!-- Not minified -->
	<script src="scripts/app/js/jquery-animate-css-rotate-scale.js"></script><!-- Not minified -->
	
	<script src="scripts/libs/jsnlog.min.js"></script>
	<script src="scripts/libs/loggerModule.js"></script>
	<script src="scripts/libs/intro.js"></script>
	<script src="scripts/libs/angular-fullscreen.js"></script>
	<script src="scripts/libs/spin.js"></script>
	<script src="scripts/libs/angular-loading.min.js"></script>
	<script src="scripts/libs/svg-class.js"></script>
	<script src="scripts/libs/nicescroll.min.js"></script>
	<script src="scripts/libs/mouse-capture-service.js"></script>
	<script src="scripts/libs/dragging-service.js"></script>
	<script src="scripts/libs/data-tree.js"></script>
	<script src="scripts/libs/ng-csv.js"></script>
	<script src="scripts/libs/angular-file-upload.js"></script>
	<script src="scripts/libs/d3.js" charset="utf-8"></script>
	<script src="scripts/libs/ui-grid.js"></script>
	
	
	<script src="scripts/libs/angular-touch.js"></script>
	<script src="scripts/libs/bootstrap.js"></script>
	<script src="scripts/libs/angular-cookies.js"></script>
	<script src="scripts/libs/angular-sanitize.js"></script>
	<script src="scripts/libs/select.js"></script>
	<script src="scripts/libs/alasql.min.js"></script>
	<script src="scripts/libs/bootstrap-toggle.min.js"></script>
	<script src="scripts/libs/classie.js"></script>
	<script src="scripts/libs/modernizr.custom.js"></script>
	<script src="scripts/libs/notificationFx.js"></script>
	<script src="scripts/libs/moment.js"></script>
	<script src="scripts/libs/bootstrap-datetimepicker.js"></script>

	<script src="scripts/libs/D3factory.js"></script>  
	
	
	<script src="scripts/libs/xeditable.min.js"></script>
	<script src="scripts/libs/nv.d3.min.js"></script>
	<script src="scripts/libs/multiselect-search-tree/ivh-treeview.min.js"></script>
	
	<script src="scripts/app/dataSourceView.js"></script>
	<script src="scripts/app/dataSource.js"></script>
	<script src="scripts/app/hiveExplore.js"></script>
	<script src="scripts/app/appView.js"></script>
	<script src="scripts/app/appCharts.js"></script>
	<script src="scripts/app/signin.js"></script>
	<script src="scripts/app/useCookie.js"></script>
	<script src="scripts/app/signup.js"></script>
	<script src="scripts/app/checklist-model.js"></script>
	<script src="scripts/app/rdbmsTestConnection.js"></script>
	<script src="scripts/app/userProfile.js"></script>

	<!-- Start of Mercer code change --> 
	<!-- End of Mercer code change -->	


	<script src="scripts/app/groupConfig.js"></script>
	<script src="scripts/app/accessConfig.js"></script>
	<script src="scripts/app/userManagement.js"></script>
	<script src="scripts/app/visualize.js"></script>
	
	<script src="scripts/libs/scrolling-tabs.js"></script>  
	<script src="scripts/app/js/projects.js"></script><!-- Not minified -->
	<script src="scripts/app/maxiqInit.js"></script>
	<script src="scripts/app/js/landingscripts.js"></script>
	<script src="scripts/app/js/sweetalert.js"></script><!-- Not minified -->
	<script src="scripts/app/dataSourceInfo.js"></script><!-- Not minified -->
	<script src="scripts/app/relationsPopup.js"></script> 
	<script src="scripts/app/analyticsModels.js"></script>
	<script src="scripts/app/dataSourceHistory.js"></script>
	<script src="scripts/app/glossaryView.js"></script>
	<script src="scripts/app/profileData.js"></script>
	<script src="scripts/app/js/modelSummary.js"></script>
	<script src="scripts/app/controller/dataSourceRequestApproverCtlr.js"></script>
	<script src="scripts/app/controller/settingsTabCtlr.js"></script>
	<script src="scripts/app/dataSourceApi.js"></script>
	<script src="scripts/app/categoryView.js"></script>

	<script src="scripts/app/services/connector/connectorService.js"></script>
	<script src="scripts/app/services/connection/connectionService.js"></script>
	<script src="scripts/app/services/user/usersService.js"></script>
	<script src="scripts/app/services/glossary/glossaryService.js"></script>
	<script src="scripts/app/services/explorer/explorer.js"></script>
	<script src="scripts/app/services/discover/discover.js"></script>

	<script src="scripts/app/directives/treeChartDirective.js"></script>
	<script src="scripts/app/directives/configureNewConnectionDirective.js"></script>
	
	<script src="scripts/app/controller/connector/connectorCtlr.js"></script>
	<script src="scripts/app/controller/connector/subConnectorCtrl.js"></script>
	<script src="scripts/app/controller/connector/connectionsViewCtlr.js"></script>
	<script src="scripts/app/controller/connector/createConnectionCtlr.js"></script>
	<script src="scripts/app/controller/connector/configNewConnectionCtlr.js"></script>
	<script src="scripts/app/controller/hiveExplorer/exploreHiveCtlr.js"></script>
	<script src="scripts/app/controller/discover/discoverController.js"></script>
	<script src="scripts/app/controller/discover/dataSourceRequestAccessController.js"></script>
	
</body>
</html>
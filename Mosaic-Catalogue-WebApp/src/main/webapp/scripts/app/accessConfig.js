(function(){
    "use strict";
    var app = angular.module('app.accessConfig',[]);

    app.service("UserAccessService",["$q","$http","logger",function($q,$http,logger){

        return{

            getAccessConfigDummyObject:function(){
                return{
                unqRoleMasterId:"",
                roleName:"",
                roleDescription:"",
                createdBy:"",
                createdTs:"",
                updatedBy:"",
                updatedTs:""
                }
            },

            buildCreateRoleObject:function(roleObject, actionsList, availableActionObject){
                var overallObj_;
                var filteredActionsObject = _.filter(availableActionObject, function(item) {
                   for (var i = 0; i < actionsList.length; i++) {
                     if(item.action == actionsList[i]){
                         return item;
                     }
                   }
                });
                roleObject.appName = undefined;
                overallObj_={
                        "roleObject":roleObject,
                        "actionsObject":filteredActionsObject
                }
                return overallObj_;
            },

            removeWhiteSpacesFromList:function(inputList){
                var trimedObject=[];
                angular.forEach(inputList,function(obj){
                    trimedObject.push(obj.trim());
                });
                return trimedObject;
            },

            findActionByApp : function(list, appId){
                var outPutList = _.filter(list, function(val){return val.appId === appId })
                return outPutList;
            },

            getListFromObject:function(list, parameterName){
                var outPutList=[];
                angular.forEach(list, function(obj){
                    outPutList.push(obj[parameterName]);
                })
                return outPutList;
            },

            getWithoutObject:function(url){
                var deferred=$q.defer();
                $http.post(url).success(function(data){
                    deferred.resolve(data);
                }).error(function(data){
                    logger.logError(data);
                    deferred.reject(data);
                });
                return deferred.promise;
            },

            postWithObject:function(url,object){
                var deferred=$q.defer();
                $http.post(url,object).success(function(data){
                    deferred.resolve(data);
                }).error(function(data){
                    logger.logError(data);
                    deferred.reject(data);
                });
                return deferred.promise;
            },

            postWithObjectAndAppName:function(url,object,appName){
                var deferred=$q.defer();
                $http.post(url,object,appName).success(function(data){
                    deferred.resolve(data);
                }).error(function(data){
                    logger.logError(data);
                    deferred.reject(data);
                });
                return deferred.promise;
            },

            getAppId :function(appName, allApps) {
                for(var i = parseInt(0); i < allApps.length ; i++) {
                    if(allApps[i].appName === appName){
                        return allApps[i].appId;
                    }
                }
            }
        }
    }]);

    app.controller("accessConfigController" , ["$filter","$route","$scope", "$modal" , "$log", "logger","UserAccessService", function($filter,$route,$scope,$modal,$log,logger,UserAccessService){

    	UserAccessService.getWithoutObject('/accessConfig/getAccessDetils').then(function(data){

            $scope.availableAccess=data;
            $scope.stores = $scope.availableAccess
            , $scope.searchKeywords = "",
            $scope.filteredStores = [],
            $scope.row = "",
            $scope.select = function(page) {
                $scope.pageNum = page;
                var end, start;
                return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
            },
            $scope.onFilterChange = function() {
                return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
            }, $scope.onNumPerPageChange = function(val) {
                $scope.numPerPage = val;
                return $scope.select(1), $scope.currentPage = 1;
            }, $scope.onOrderChange = function() {
                return $scope.select(1), $scope.currentPage = 1;
            }, $scope.search = function() {
                return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
            }, $scope.order = function(rowName) {
                return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
            }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
            $scope.numPerPage = $scope.numPerPageOpt[4],
            $scope.currentPage = 1,
            $scope.currentPageStores = [], ($scope.init = function() {
                return $scope.search(), $scope.select($scope.currentPage);
            })();
        })

        $scope.openConfigureRolePopUp=function(){
            var modalInstance;
            modalInstance = $modal.open({
                templateUrl: "views/template/configureNewAccessModal.html",
                controller: "configureAccessPopUpController",
                size:'lg',
                resolve:{
                   "access":function(){
                         return "";
                    }
                }
	         }),modalInstance.result.then(function(obj) {
	             $route.reload();
	             $log.info("Modal closed at: " + new Date);
	         }, function(obj) {
	             $log.info("Modal dismissed at: " + new Date);
	         });
        };


        $scope.deleteAccess=function(access){
        	var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:access.roleName, type: "role"};
					}
				}
			}),modalInstance.result.then(function() {
				UserAccessService.postWithObject('/accessConfig/deleteAccess',access.unqRoleMasterId).then(function(data){
	                $route.reload();
	                logger.logSuccess("Role has been deleted successfully");
	            });
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
         }

         $scope.editAccess=function(access){
                var modalInstance;
                modalInstance = $modal.open({
                    templateUrl: "views/template/configureNewAccessModal.html",
                    controller: "configureAccessPopUpController",
                    size:'lg',
                    resolve:{
                        "access":function(){
                            return access;
                        }
                    }
             }),modalInstance.result.then(function(obj) {
                 $route.reload();
                 $log.info("Modal closed at: " + new Date);
             }, function(obj) {
                 $log.info("Modal dismissed at: " + new Date);
             });
         }

    }]).controller("configureAccessPopUpController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","UserAccessService","access",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger, UserAccessService, access){

        $scope.select={
                selectedActions:[]
        };
        $scope.isAccess=0;
		$scope.accessConfigObject=UserAccessService.getAccessConfigDummyObject();
		UserAccessService.getWithoutObject('/accessConfig/fetchAvailableActions').then(function(data){
            $scope.availableActions=data;
            $scope.allActionList=UserAccessService.getListFromObject($scope.availableActions, "action");
            $scope.storeRequiredObjects(access);
        });


        $scope.changeAccessControl = function(appObject) {
            $scope.select.selectedActions = [];
            $scope.selectedApp = _.findWhere($scope.allApps,{"appName" : appObject.appName});
            $scope.allActionList = UserAccessService.getListFromObject(UserAccessService.findActionByApp($scope.availableActions,$scope.selectedApp.appId),"action");
        };

        $scope.onEditOfAccessControl = function(appObject) {
//            $scope.select.selectedActions = [];
            $scope.selectedApp = _.findWhere($scope.allApps,{"appName" : appObject.appName});
            $scope.allActionList = UserAccessService.getListFromObject(UserAccessService.findActionByApp($scope.availableActions,$scope.selectedApp.appId),"action");
        };

        $http({
        method : 'POST',
        url : "/accessConfig/fetchAllApps"
        }).success(function(data, status, headers, config) {
            $scope.allApps=data;
        }).error(function(data, status, headers, config) {
        logger.logError(status);
        });

        $scope.storeRequiredObjects = function (access) {

            if(access){
                //EDIT
                $scope.isAccess=1;
                $scope.accessConfigObject=angular.copy(access);
                $http({
                    method : 'POST',
                    url : "/accessConfig/fetchAllApps"
                    }).success(function(data, status, headers, config) {
                        $scope.allApps=data;
                        $scope.onEditOfAccessControl(angular.copy(access));
                    }).error(function(data, status, headers, config) {
                    logger.logError(status);

                });
                var dummyActions = [];
                if(null != access.actionsList){
                    dummyActions = access.actionsList.split(",");
                }

                $scope.select.selectedActions=UserAccessService.removeWhiteSpacesFromList(dummyActions);
                $scope.updateRoleAction=function(){
                    $scope.currentAppsAllActions = _.filter($scope.availableActions, function (val) {return val.appId == $scope.selectedApp.appId;});
                    if($scope.createRole.$invalid || $scope.select.selectedActions.length==0){
                        logger.logError("all fields are mandatory");
                    }else{
                        $scope.accessConfigObject.status='ACTIVE';
                        delete $scope.accessConfigObject.actionsList;
                        var dataObj_=UserAccessService.buildCreateRoleObject(angular.copy($scope.accessConfigObject), angular.copy($scope.select.selectedActions), angular.copy($scope.currentAppsAllActions));
                        //var dataObj_=UserAccessService.buildCreateRoleObject($scope.accessConfigObject, $scope.select.selectedActions, $scope.availableActions);
                        var appId = (UserAccessService.getAppId($scope.accessConfigObject.appName, $scope.allApps));
                        if(dataObj_){

                        	var reqDataObject = {'appId' : appId, 'roleActions' : dataObj_};
                            $http({
                                method : "post",
                                url : "/accessConfig/updateRoleAction",
                                data : reqDataObject
                            }).success(function(data){
                                logger.logSuccess("Role has been updated successfully");
                                $modalInstance.close("cancel");
                            }).error(function(status){
                            	if(status !== undefined && status !== null && status !== "" ){
                            		logger.logError(status);
                            	}
                            		
                            });
                        }
                    }
                }

            }else{
              //CREATE
                $scope.createRoleAction=function(){
                    $scope.currentAppsAllActions = _.filter($scope.availableActions, function (val) {return val.appId == $scope.selectedApp.appId;});
                    if($scope.createRole.$invalid || $scope.select.selectedActions.length==0){
                        logger.logError("all fields are mandatory");
                    }else{
                        var dataObj_=UserAccessService.buildCreateRoleObject(angular.copy($scope.accessConfigObject), angular.copy($scope.select.selectedActions), angular.copy($scope.currentAppsAllActions));
                        var appId = (UserAccessService.getAppId($scope.accessConfigObject.appName, $scope.allApps));
                        if(dataObj_){
                        	var reqDataObject = {'appId' : appId, 'roleActions': dataObj_};
                            $http({
                                method : "post",
                                url : "/accessConfig/createRoleAction",
                                data : reqDataObject
                            }).success(function(data){
                                logger.logSuccess("Role has been created successfully");
                                $modalInstance.close("cancel");
                            }).error(function(status){
                                logger.logError(status);
                            });
                        }
                    }
                }
            }
        };

        $scope.cancel = function() {
            $modalInstance.dismiss("cancel");
        }
   }])

}).call(this); 

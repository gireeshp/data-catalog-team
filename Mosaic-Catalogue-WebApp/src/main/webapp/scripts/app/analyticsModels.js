(function(){
	"use strict";

	var customComponenetModule = angular.module("app.customComponentRCode",['angularFileUpload']);

	customComponenetModule.controller("customComponentRCodeCtrl",["$scope" ,"$filter" , "$http", "$location" , "logger", "$modal", "$log", "$route","$interval","$rootScope", "$cookieStore", function($scope  ,$filter , $http, $location , logger, $modal, $log, $route, $interval, $rootScope, $cookieStore){
		$scope.projectAccessType = $cookieStore.get("projectAccessType");
		
		$scope.getAllTypeOfModels=function(){
			$http.post("/getAllTypeOfModelListFromGroupId").success(function(data, status, headers, config){
				return $scope.stores = data	
				, $scope.searchKeywords = "", 
				$scope.filteredStores = [],
				$scope.row = "", 
				$scope.select = function(page) {
					$scope.pageNum = page;
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
				}, 
				$scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.search = function() {
					return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0;
				}, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				$scope.numPerPage = $scope.numPerPageOpt[4], 
				$scope.currentPage = 1, 
				$scope.currentPageStores = [],
				($scope.init = function() {
					return $scope.search(), $scope.select($scope.currentPage),$scope.order("-modelId");
				})();
			});	
		}
		
		
		$interval(function() {
			if($rootScope.isModelCancelled){
				$scope.getAllTypeOfModels();
				$rootScope.isModelCancelled = false;
			}
		}, 100);

		$scope.viewModel = function(component) {
			var modalInstance;
			modalInstance = $modal.open({
				size: "lg",
				templateUrl: "/views/template/create-new-custom-componentRcode-modal.html",
				controller: "customComponentRModalCtrl",
				resolve : {
					showComponent : function(){
						return component; 
					},
					disable : function(){
						return true;
					},
					editable : function(){
						return true;
					}
				}

			}),modalInstance.result.then(function(reponame) {

			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		}

		$scope.redirect = function(obj){
			var url="/modelSummary/"+obj.appId+"/"+obj.nodeId+"/"+obj.jobInstanceId;
		    $location.path(url);
		}
		
		$scope.viewModalStatistics = function(component) {
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/view-modal-statistics.html",
				controller: "customModalStatisticsCtrl",
				resolve : {
					showComponent : function(){
						return component; 
					},
					disable : function(){
						return true;
					},
					editable : function(){
						return true;
					}
				}

			}),modalInstance.result.then(function(reponame) {

			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		}

		$scope.open = function(component) {
			var modalInstance;
			modalInstance = $modal.open({
				size: "lg",
				templateUrl: "/views/template/create-new-custom-componentRcode-modal.html",
				controller: "customComponentRModalCtrl",
				resolve : {
					showComponent : function(){
						return component; 
					},
					disable : function(){
						return false;
					},
					editable : function(){
						return false;
					}
				}

			}),modalInstance.result.then(function(reponame) {

			}, function() {

				$log.info("Modal dismissed at: " + new Date);
			});
		};

		$scope.deleteRModel = function(store){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve : {
					showComponent : function(){
						return store; 
					},
					deleteObject: function(){
						return {name:store.modelName, type: "custom component"};
					}
				}
			}),modalInstance.result.then(function() {
				$http({
					url: "/deleteRModel",
					method: "POST",
					data : store.modelId
				}).success(function(data){
					logger.logSuccess("R Model deleted successfully");
					$route.reload();
				}).error(function(data){
					logger.logError(data);
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};
		
		$scope.toggleButtonSwitch = function(model){
			if(model.apiAccessStatus==0){
				return false;
			}
			if(model.apiAccessStatus==1){
				return true;
			}
		}
		
		$scope.createApiKey=function(data, $index){
			$scope.compObj = $scope.stores[$index];
			var modifiedByName = $scope.compObj.modifiedByName;
			var groupName = $scope.compObj.groupName;
			var createdByName = $scope.compObj.createdByName;
			delete $scope.compObj.modifiedByName;
			delete $scope.compObj.groupName;
			delete $scope.compObj.createdByName;
			
			if($scope.stores[$index].apiAccessStatus==0){
				$scope.stores[$index].apiAccessStatus=1;
			}else{
				$scope.stores[$index].apiAccessStatus=0;
			}
			
			$http({
				url: "/creaetApiKeyForModel",
				method: "Post",
				data: $scope.compObj
			}).success(function(data){
				$scope.toggleButtonSwitch($scope.stores[$index]);
				$scope.compObj.modifiedByName= modifiedByName;
				$scope.compObj.groupName = groupName;
				$scope.compObj.createdByName = createdByName;
			}).error(function(data){
				logger.logError(data);
				if($scope.stores[$index].apiAccessStatus==1){
					$scope.stores[$index].apiAccessStatus=1;
				}else{
					$scope.stores[$index].apiAccessStatus=0;
				}
				
			});
		}

	}]).controller("customComponentRModalCtrl", ["$scope", "$modalInstance", "$http" , "$location", "logger" , "showComponent", "$upload", "$route","disable", "editable","$modal","$rootScope", function($scope, $modalInstance, $http , $location, logger, showComponent, $upload, $route,disable, editable, $modal, $rootScope){
		$scope.disabled = disable;
		$scope.disable = false;
		$scope.editable = editable;

		$scope.serverUrl = "";
		$http({
			url: "/getMetaDataOfModel",
			method: 'POST'
		}).success(function(data){
			$scope.serverUrl=data.apiUrl;
		}).error(function(data){
			if(data){
				logger.logError(data);
			}else{
				logger.logError("Problem while getting response");
			}
		});

		if(undefined != showComponent){
			$scope.compObj = showComponent;
			$scope.flagNew = "NO";
			$scope.disable = true;
			if($scope.compObj.apiKey!=undefined && $scope.compObj.apiKey!="" && $scope.compObj.apiKey!=null &&
					$scope.compObj.apiAccessStatus!=undefined && $scope.compObj.apiAccessStatus!=null 
					&&$scope.compObj.apiAccessStatus==1){
				$scope.tglBtn = true;	
			} 
		}else{
			$scope.flagNew = "YES";
			$scope.compObj = {};
			$scope.tglBtn = false;
			$scope.compObj.apiAccessStatus=0;
		}
		
		$scope.getApiUrl=function(model){
			return $scope.serverUrl+"/predictModel?id="+model.modelId+"&apiKey="+model.apiKey;
		}
		
		
		
		$scope.testAPI = function(object){
			var modalInstance;
			modalInstance = $modal.open({
				size: "lg",
				templateUrl: "/views/template/testApiSparkMLLib.html",
				controller: "TestAPISparkMLLibCtrl",
				resolve : {
					modelToTest : function(){
						return object; 
					}
				}
			}),modalInstance.result.then(function(reponame) {
				
			}, function() {

				$log.info("Modal dismissed at: " + new Date);
			});
		};

		$scope.visibleAddField = "N"; // Add fields manually by default hide

		$scope.getDataFieldsFromLocation = function(location) {
			if(location==undefined || location==null){
				logger.logError("Required path");
				return;
			} 

			$http({
				url: "/getDataDescriptionFromLocationPath",
				method: "POST",
				data : location
			}).success(function(data){
				$scope.compObj.data = data;
				if($scope.compObj.data!=undefined && $scope.compObj.data!=null && $scope.compObj.data.numberOfFeatures !=undefined && $scope.compObj.data.numberOfFeatures !=null ){
					$scope.compObj.data.numberOfFeatures = parseInt($scope.compObj.data.numberOfFeatures);
					$scope.compObj.data.fieldMappings = {};
					$scope.compObj.data.fieldMappings.inputFields = [];
					for(var i=0;i<$scope.compObj.data.numberOfFeatures;i++){
						var featureName = "Feature"+i;
						var fieldData = {"name":featureName, "dataType":"DOUBLE", "format":""};
						$scope.compObj.data.fieldMappings.inputFields.push(fieldData);
					}

					$scope.visibleAddField = "Y";
				}


			}).error(function(data){
				logger.logError(data);
			});
		},
		$scope.saveFieldMapping = function(){
			if($scope.compObj!=undefined && $scope.compObj!=null){
				if($scope.compObj.modelId==undefined|| $scope.compObj.modelId==null || $scope.compObj.modelId=="0"){
					$scope.compObj.modelId = 0.0;
				}
				
				if(_.isUndefined($scope.compObj.modelName)
						||_.isEmpty($scope.compObj.modelName)){
					logger.logError("Required Model name");
					return ;
				}
				
				if(_.isUndefined($scope.compObj) 
						|| _.isUndefined($scope.compObj.modelType) 
						|| _.isEmpty($scope.compObj.modelType)){
					logger.logError("Required Model Type");
					return ;
				}
				
				if(_.isUndefined($scope.compObj) 
						|| _.isUndefined($scope.compObj.modelLocation) 
						|| _.isEmpty($scope.compObj.modelLocation)){
					logger.logError("Required Model Path");
					return ;
				}
				
				
				
				
				
				if($scope.compObj.modelType == "SparkR"){
					$http({
						url: "/saveRModel",
						method: "Post",
						data: $scope.compObj
					}).success(function(data){
						logger.logSuccess("RModel saved successfully");
						$route.reload();
						$scope.cancel();
					}).error(function(data){
						logger.logError(data);
					});		
				}else if($scope.compObj.modelType == "SparkMLlib"){
					if($scope.compObj.data!=undefined && $scope.compObj.data!=null &&
							$scope.compObj.data.fieldMappings!=undefined && $scope.compObj.data.fieldMappings!=null){
						
						for(var i=$scope.compObj.data.fieldMappings.inputFields.length-1; i>=0; i--) {
							if($scope.compObj.data.fieldMappings.inputFields[i].dataType == ""){
								logger.logError("Datatype not selected for Fieldname "+$scope.compObj.data.fieldMappings.inputFields[i].name);
								return;
							}
						}
						
						for(var i=$scope.compObj.data.fieldMappings.inputFields.length-1; i>=0; i--) {
							delete $scope.compObj.data.fieldMappings.inputFields[i].manual;
						}

						delete $scope.compObj.modifiedByName;
						delete $scope.compObj.groupName;
						delete $scope.compObj.createdByName;

						$scope.compObj.data.fieldMappings.outputFields = [];
						for(var i=0;i<$scope.compObj.data.numberOfFeatures+1;i++){
							if($scope.compObj.data.numberOfFeatures==i){
								var featureName = "Predicted Value";
								var fieldData = {"name":featureName, "dataType":"DOUBLE", "format":""};
								$scope.compObj.data.fieldMappings.outputFields.push(fieldData);
							}else{
								var featureName = "Feature"+i;
								var fieldData = {"name":featureName, "dataType":"DOUBLE", "format":""};
								$scope.compObj.data.fieldMappings.outputFields.push(fieldData);
							}
						}	
						
						$http({
							url: "/saveFieldMapping",
							method: "Post",
							data: $scope.compObj
						}).success(function(data){
							logger.logSuccess("Model saved successfully");
							$scope.compObj = data;
							if($scope.compObj.modelType == "SparkMLlib"){
								$scope.getApiKeyByTogleBtn($scope.compObj);
							}
							$route.reload();
						}).error(function(data){
							logger.logError(data);
						});
					}
				}
				
			}else{
				logger.logError("Required Fields");
				return ;
			}
		},
		$scope.getApiKeyByTogleBtn =function(compObj){
			if(compObj.apiAccessStatus=='1'){
				$scope.tglBtn = true;
			}else{
				$scope.tglBtn = false;
			}
		},
		$scope.createApiKey=function(){
			if($scope.tglBtn){
				$scope.tglBtn = false;	
				$scope.compObj.apiAccessStatus=0;
			}else{
				$scope.tglBtn = true;
				$scope.compObj.apiAccessStatus=1;
			}

			delete $scope.compObj.modifiedByName;
			delete $scope.compObj.groupName;
			delete $scope.compObj.createdByName;

			for(var i=$scope.compObj.data.fieldMappings.inputFields.length-1; i>=0; i--) {
				delete $scope.compObj.data.fieldMappings.inputFields[i].manual;
			}
			if($scope.compObj.modelId==undefined || $scope.compObj.modelId == null || $scope.compObj.modelId == 0){
				$scope.compObj.data.fieldMappings.outputFields = [];
				for(var i=0;i<$scope.compObj.data.numberOfFeatures+1;i++){
					if($scope.compObj.data.numberOfFeatures==i){
						var featureName = "Predicted Value";
						var fieldData = {"name":featureName, "dataType":"DOUBLE", "format":""};
						$scope.compObj.data.fieldMappings.outputFields.push(fieldData);
					}else{
						var featureName = "Feature"+i;
						var fieldData = {"name":featureName, "dataType":"DOUBLE", "format":""};
						$scope.compObj.data.fieldMappings.outputFields.push(fieldData);
					}
				}
			}

			$http({
				url: "/creaetApiKeyForModel",
				method: "Post",
				data: $scope.compObj
			}).success(function(data){
				$scope.compObj=data;
				$scope.getApiKeyByTogleBtn($scope.compObj);
			}).error(function(data){
				logger.logError(data);
			});
		},
		$scope.addFieldMapping = function(){
			var len = $scope.compObj.data.fieldMappings.inputFields.length;
			var featureName = "Feature"+len;
			var fieldData = {"name":featureName, "dataType":"", "format":"", "manual":"true"};
			$scope.compObj.data.fieldMappings.inputFields.push(fieldData);	
		},
		$scope.removeFieldMapping = function(item){
			if($scope.compObj.data.fieldMappings){// this condition will check for null, undefined and empty 
				for(var i=$scope.compObj.data.fieldMappings.inputFields.length-1; i>=0;i--){
					var fieldName = $scope.compObj.data.fieldMappings.inputFields[i].name;
					if(fieldName === item.name){
						$scope.compObj.data.fieldMappings.inputFields.splice(i, 1);
						return ;
					}
				}
				
			}
		},
		$scope.cancel = function() {
			/*$route.reload();*/
			$modalInstance.dismiss("cancel");
			$rootScope.isModelCancelled = true;
		},
		$scope.createCustomComponent = function() {
			
			if(_.isUndefined($scope.compObj.modelName)
					||_.isEmpty($scope.compObj.modelName)){
				logger.logError("Required Model name");
				return ;
			}
			if(_.isUndefined($scope.compObj) 
					|| _.isUndefined($scope.compObj.modelType) 
					|| _.isEmpty($scope.compObj.modelType)){
				logger.logError("Required Model Type");
				return ;
			}
			if(_.isUndefined($scope.compObj) 
					|| _.isUndefined($scope.compObj.modelLocation) 
					|| _.isEmpty($scope.compObj.modelLocation)){
				logger.logError("Required Model Path");
				return ;
			}
			
				
			if($scope.compObj.modelType == "SparkMLlib"){
				if($scope.compObj.data!=undefined && $scope.compObj.data!=null &&
						$scope.compObj.data.fieldMappings!=undefined && $scope.compObj.data.fieldMappings!=null){
						if($scope.compObj.data!=undefined && $scope.compObj.data!=null &&
								$scope.compObj.data.fieldMappings!=undefined && $scope.compObj.data.fieldMappings!=null){
							

							for(var i=$scope.compObj.data.fieldMappings.inputFields.length-1; i>=0; i--) {
								if($scope.compObj.data.fieldMappings.inputFields[i].dataType == ""){
									logger.logError("Datatype not selected for Fieldname "+$scope.compObj.data.fieldMappings.inputFields[i].name);
									return;
								}
							}
							
							for(var i=$scope.compObj.data.fieldMappings.inputFields.length-1; i>=0; i--) {
								delete $scope.compObj.data.fieldMappings.inputFields[i].manual;
							}

							$scope.compObj.data.fieldMappings.outputFields = [];
							for(var i=0;i<$scope.compObj.data.numberOfFeatures+1;i++){
								if($scope.compObj.data.numberOfFeatures==i){
									var featureName = "Predicted Value";
									var fieldData = {"name":featureName, "dataType":"DOUBLE", "format":""};
									$scope.compObj.data.fieldMappings.outputFields.push(fieldData);
								}else{
									var featureName = "Feature"+i;
									var fieldData = {"name":featureName, "dataType":"DOUBLE", "format":""};
									$scope.compObj.data.fieldMappings.outputFields.push(fieldData);
								}
							}
						}
				}
			}
			
			delete $scope.compObj.modifiedByName;
			delete $scope.compObj.groupName;
			delete $scope.compObj.createdByName;
			
			
			$http({
				url: "/saveRModel",
				method: "Post",
				data: $scope.compObj
			}).success(function(data){
				logger.logSuccess("RModel saved successfully");
				$route.reload();
				$scope.cancel();
			}).error(function(data){
				logger.logError(data);
			});
		};
	}]).controller("TestAPISparkMLLibCtrl", ["$scope", "$modalInstance", "$http" , "$location", "logger" , "$upload", "$route", "modelToTest", function($scope, $modalInstance, $http , $location, logger, $upload, $route, modelToTest){

		if(modelToTest){
			$scope.model = modelToTest;
		}
		
		$http({
			url: "/getMetaDataOfModel",
			method: 'POST'
		}).success(function(data){
			console.log(data);
			$scope.url = data.apiUrl+methodName;
		}).error(function(data){
			if(data){
				logger.logError(data);
			}else{
				logger.logError("Problem while getting response");
			}
		});
		
		
		var methodName = "/predictModel?id="+$scope.model.modelId+"&apiKey="+$scope.model.apiKey;
		
		$scope.responseRequest = "";
		var no = parseInt($scope.model.data.numberOfFeatures);
		$scope.inputRequest = "";
		var feature = "";
		if(no>0){
			for(var i=0;i<no;i++){
				if(i==0){
					feature += '{ "featureData" :{';
				}
				
				feature = feature + '"Feature '+ i +'":"' + (i+10) +'"';
				
				if(i==(no-1)){
					feature += '}}';
					$scope.inputRequest = feature;
				}else{
					feature += ',';
				}
			}
		}
		
		
		$scope.sendRequest = function(){
			$http({
				url: methodName,
				method: 'POST',
				data: $scope.inputRequest
			}).success(function(data){
				$scope.responseRequest = JSON.stringify(data,null,"    ");
			}).error(function(data){
				if(data){
					logger.logError(data);
				}else{
					logger.logError("Problem while getting response");
				}
			});
		};
		
		$scope.cancel = function() {
			/*$route.reload();*/
			$modalInstance.dismiss("cancel");
		};
	}]);
}).call(this);

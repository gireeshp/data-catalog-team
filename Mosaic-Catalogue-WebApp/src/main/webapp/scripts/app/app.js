(function(){
    "use strict";
    
    angular.module("app.chart.ctrls", []).controller("sparklineCtrl", ["$scope", function($scope) {
        return $scope.demoData1 = {
            data: [3, 1, 2, 2, 4, 6, 4, 5, 2, 4, 5, 3, 4, 6, 4, 7],
            options: {
                type: "line",
                lineColor: "#fff",
                highlightLineColor: "#fff",
                fillColor: "#23AE89",
                spotColor: !1,
                minSpotColor: !1,
                maxSpotColor: !1,
                width: "100%",
                height: "150px"
            }
        }, $scope.simpleChart1 = {
            data: [3, 1, 2, 3, 5, 3, 4, 2],
            options: {
                type: "line",
                lineColor: "#1FB5AD",
                fillColor: "#bce0df",
                spotColor: !1,
                minSpotColor: !1,
                maxSpotColor: !1
            }
        }, $scope.simpleChart2 = {
            data: [3, 1, 2, 3, 5, 3, 4, 2],
            options: {
                type: "bar",
                barColor: "#1FB5AD"
            }
        }, $scope.simpleChart3 = {
            data: [3, 1, 2, 3, 5, 3, 4, 2],
            options: {
                type: "pie",
                sliceColors: ["#1fb5ad", "#95b75d", "#57c8f1", "#8175c7", "#f3c022", "#fa8564"]
            }
        }, $scope.tristateChart1 = {
            data: [1, 2, -3, -5, 3, 1, -4, 2],
            options: {
                type: "tristate",
                posBarColor: "#95b75d",
                negBarColor: "#fa8564"
            }
        }, $scope.largeChart1 = {
            data: [3, 1, 2, 3, 5, 3, 4, 2],
            options: {
                type: "line",
                lineColor: "#674E9E",
                highlightLineColor: "#7ACBEE",
                fillColor: "#927ED1",
                spotColor: !1,
                minSpotColor: !1,
                maxSpotColor: !1,
                width: "100%",
                height: "150px"
            }
        }, $scope.largeChart2 = {
            data: [3, 1, 2, 3, 5, 3, 4, 2],
            options: {
                type: "bar",
                barColor: "#A3C86D",
                barWidth: 10,
                width: "100%",
                height: "150px"
            }
        }, $scope.largeChart3 = {
            data: [3, 1, 2, 3, 5],
            options: {
                type: "pie",
                sliceColors: ["#A3C86D", "#7ACBEE", "#927ED1", "#FDD761", "#FF7857", "#674E9E"],
                width: "150px",
                height: "150px"
            }
        }
    }])
}).call(this),
    function() {
        "use strict";
        angular.module("app.ui.form.ctrls", []).controller("DatepickerDemoCtrl", ["$scope", function($scope) {
            return $scope.today = function() {
                return $scope.dt = new Date
            }, $scope.today(), $scope.showWeeks = !0, $scope.toggleWeeks = function() {
                return $scope.showWeeks = !$scope.showWeeks
            }, $scope.clear = function() {
                return $scope.dt = null
            }, $scope.disabled = function(date, mode) {
                return "day" === mode && (0 === date.getDay() || 6 === date.getDay())
            }, $scope.toggleMin = function() {
                var _ref;
                return $scope.minDate = null != (_ref = $scope.minDate) ? _ref : {
                    "null": new Date
                }
            }, $scope.toggleMin(), $scope.open = function($event) {
                return $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0
            }, $scope.dateOptions = {
                "year-format": "'yy'",
                "starting-day": 1
            }, $scope.formats = ["dd-MMMM-yyyy", "yyyy/MM/dd", "shortDate"], $scope.format = $scope.formats[0]
        }]).controller("TimepickerDemoCtrl", ["$scope", function($scope) {
            return $scope.mytime = new Date, $scope.hstep = 1, $scope.mstep = 15, $scope.options = {
                hstep: [1, 2, 3],
                mstep: [1, 5, 10, 15, 25, 30]
            }, $scope.ismeridian = !0, $scope.toggleMode = function() {
                return $scope.ismeridian = !$scope.ismeridian
            }, $scope.update = function() {
                var d;
                return d = new Date, d.setHours(14), d.setMinutes(0), $scope.mytime = d
            }, $scope.changed = function() {
                return console.log("Time changed to: " + $scope.mytime)
            }, $scope.clear = function() {
                return $scope.mytime = null
            }
        }]).controller("TypeaheadCtrl", ["$scope", function($scope) {
            return $scope.selected = void 0, $scope.states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        }]).controller("RatingDemoCtrl", ["$scope", function($scope) {
            return $scope.rate = 7, $scope.max = 10, $scope.isReadonly = !1, $scope.hoveringOver = function(value) {
                return $scope.overStar = value, $scope.percent = 100 * (value / $scope.max)
            }, $scope.ratingStates = [{
                stateOn: "glyphicon-ok-sign",
                stateOff: "glyphicon-ok-circle"
            }, {
                stateOn: "glyphicon-star",
                stateOff: "glyphicon-star-empty"
            }, {
                stateOn: "glyphicon-heart",
                stateOff: "glyphicon-ban-circle"
            }, {
                stateOn: "glyphicon-heart"
            }, {
                stateOff: "glyphicon-off"
            }]
        }])
    }.call(this),
    function() {
        angular.module("app.ui.form.directives", []).directive("uiRangeSlider", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return ele.slider()
                }
            }
        }]).directive("uiFileUpload", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return ele.bootstrapFileInput()
                }
            }
        }]).directive("uiSpinner", [function() {
            return {
                restrict: "A",
                compile: function(ele) {
                    return ele.addClass("ui-spinner"), {
                        post: function() {
                            return ele.spinner()
                        }
                    }
                }
            }
        }]).directive("uiWizardForm", [function() {
            return {
                link: function(scope, ele) {
                    return ele.steps()
                }
            }
        }])
    }.call(this),
    function() {
        "use strict";
        angular.module("app.form.validation", []).controller("wizardFormCtrl", ["$scope", function($scope) {
            return $scope.wizard = {
                firstName: "some name",
                lastName: "",
                email: "",
                password: "",
                age: "",
                address: ""
            }, $scope.isValidateStep1 = function() {
                return console.log($scope.wizard_step1), console.log("" !== $scope.wizard.firstName), console.log("" === $scope.wizard.lastName), console.log("" !== $scope.wizard.firstName && "" !== $scope.wizard.lastName)
            }, $scope.finishedWizard = function() {
                return console.log("yoo")
            }
        }]).controller("formConstraintsCtrl", ["$scope", function($scope) {
            var original;
            return $scope.form = {
                required: "",
                minlength: "",
                maxlength: "",
                length_rage: "",
                type_something: "",
                confirm_type: "",
                foo: "",
                email: "",
                url: "",
                num: "",
                minVal: "",
                maxVal: "",
                valRange: "",
                pattern: ""
            }, original = angular.copy($scope.form), $scope.revert = function() {
                return $scope.form = angular.copy(original), $scope.form_constraints.$setPristine()
            }, $scope.canRevert = function() {
                return !angular.equals($scope.form, original) || !$scope.form_constraints.$pristine
            }, $scope.canSubmit = function() {
                return $scope.form_constraints.$valid && !angular.equals($scope.form, original)
            }
        }]).controller("signinCtrl", ["$scope" , "$http", "$location",function($scope, $http,$location) {
            var original;
           
            return $scope.user = {
                email: "",
                password: ""
            }, $scope.showInfoOnSubmit = !1, original = angular.copy($scope.user)
            , $scope.revert = function() {
                return $scope.user = angular.copy(original), $scope.form_signin.$setPristine()
            }, $scope.canRevert = function() {
                return !angular.equals($scope.user, original) || !$scope.form_signin.$pristine
            }, $scope.canSubmit = function() {
                return $scope.form_signin.$valid && !angular.equals($scope.user, original)
            }, $scope.submitForm = function() {
            	$http({
                		url:"/home",
                		method:"POST",
                		data  : {
                			userName : $scope.user.email,
                    		password : $scope.user.password
                		},
                		header:{
                			'Content-Type': 'application/json'
                		}
                	}).success(function(data, status, headers, config){
                		$location.path("/dataRepoView");
                	});
            	return $scope.showInfoOnSubmit = !0, $scope.revert()
            }
            
            
            
        }]).controller("signupCtrl", ["$scope", function($scope) {
            var original;
            return $scope.user = {
                name: "",
                email: "",
                password: "",
                confirmPassword: "",
                age: ""
            }, $scope.showInfoOnSubmit = !1, original = angular.copy($scope.user), $scope.revert = function() {
                return $scope.user = angular.copy(original), $scope.form_signup.$setPristine(), $scope.form_signup.confirmPassword.$setPristine()
            }, $scope.canRevert = function() {
                return !angular.equals($scope.user, original) || !$scope.form_signup.$pristine
            }, $scope.canSubmit = function() {
                return $scope.form_signup.$valid && !angular.equals($scope.user, original)
            }, $scope.submitForm = function() {
                return $scope.showInfoOnSubmit = !0, $scope.revert()
            }
        }]).directive("validateEquals", [function() {
            return {
                require: "ngModel",
                link: function(scope, ele, attrs, ngModelCtrl) {
                    var validateEqual;
                    return validateEqual = function(value) {
                        var valid;
                        return valid = value === scope.$eval(attrs.validateEquals), ngModelCtrl.$setValidity("equal", valid), "function" == typeof valid ? valid({
                            value: void 0
                        }) : void 0
                    }, ngModelCtrl.$parsers.push(validateEqual), ngModelCtrl.$formatters.push(validateEqual), scope.$watch(attrs.validateEquals, function(newValue, oldValue) {
                        return newValue !== oldValue ? ngModelCtrl.$setViewValue(ngModelCtrl.$ViewValue) : void 0
                    })
                }
            }
        }])
    }.call(this),
    function() {
        "use strict";
        angular.module("app.tables", []).controller("tableCtrl", ["$scope", "$filter", "$http",function($scope, $filter , $http) {
        	
        }])
    }.call(this),
    function() {
        "use strict";
        angular.module("app.task", []).factory("taskStorage", function() {
            var DEMO_TASKS, STORAGE_ID;
            return STORAGE_ID = "tasks", DEMO_TASKS = '[ {"title": "Finish homework", "completed": true}, {"title": "Make a call", "completed": true}, {"title": "Play games with friends", "completed": false}, {"title": "Shopping", "completed": false}, {"title": "One more dance", "completed": false}, {"title": "Try Google glass", "completed": false} ]', {
                get: function() {
                    return JSON.parse(localStorage.getItem(STORAGE_ID) || DEMO_TASKS)
                },
                put: function(tasks) {
                    return localStorage.setItem(STORAGE_ID, JSON.stringify(tasks))
                }
            }
        }).directive("taskFocus", ["$timeout", function($timeout) {
            return {
                link: function(scope, ele, attrs) {
                    return scope.$watch(attrs.taskFocus, function(newVal) {
                        return newVal ? $timeout(function() {
                            return ele[0].focus()
                        }, 0, !1) : void 0
                    })
                }
            }
        }]).controller("taskCtrl", ["$scope", "taskStorage", "filterFilter", "$rootScope", "logger", function($scope, taskStorage, filterFilter, $rootScope, logger) {
            var tasks;
            return tasks = $scope.tasks = taskStorage.get(), $scope.newTask = "", $scope.remainingCount = filterFilter(tasks, {
                completed: !1
            }).length, $scope.editedTask = null, $scope.statusFilter = {
                completed: !1
            }, $scope.filter = function(filter) {
                switch (filter) {
                    case "all":
                        return $scope.statusFilter = "";
                    case "active":
                        return $scope.statusFilter = {
                            completed: !1
                        };
                    case "completed":
                        return $scope.statusFilter = {
                            completed: !0
                        }
                }
            }, $scope.add = function() {
                var newTask;
                return newTask = $scope.newTask.trim(), 0 !== newTask.length ? (tasks.push({
                    title: newTask,
                    completed: !1
                }), logger.logSuccess('New task: "' + newTask + '" added'), taskStorage.put(tasks), $scope.newTask = "", $scope.remainingCount++) : void 0
            }, $scope.edit = function(task) {
                return $scope.editedTask = task
            }, $scope.doneEditing = function(task) {
                return $scope.editedTask = null, task.title = task.title.trim(), task.title ? logger.log("Task updated") : $scope.remove(task), taskStorage.put(tasks)
            }, $scope.remove = function(task) {
                var index;
                return $scope.remainingCount -= task.completed ? 0 : 1, index = $scope.tasks.indexOf(task), $scope.tasks.splice(index, 1), taskStorage.put(tasks), logger.logError("Task removed")
            }, $scope.completed = function(task) {
                return $scope.remainingCount += task.completed ? -1 : 1, taskStorage.put(tasks), task.completed ? $scope.remainingCount > 0 ? logger.log(1 === $scope.remainingCount ? "Almost there! Only " + $scope.remainingCount + " task left" : "Good job! Only " + $scope.remainingCount + " tasks left") : logger.logSuccess("Congrats! All done :)") : void 0
            }, $scope.clearCompleted = function() {
                return $scope.tasks = tasks = tasks.filter(function(val) {
                    return !val.completed
                }), taskStorage.put(tasks)
            }, $scope.markAll = function(completed) {
                return tasks.forEach(function(task) {
                    return task.completed = completed
                }), $scope.remainingCount = completed ? 0 : tasks.length, taskStorage.put(tasks), completed ? logger.logSuccess("Congrats! All done :)") : void 0
            }, $scope.$watch("remainingCount == 0", function(val) {
                return $scope.allChecked = val
            }), $scope.$watch("remainingCount", function(newVal) {
                return $rootScope.$broadcast("taskRemaining:changed", newVal)
            })
        }])
    }.call(this),
    function() {
        "use strict";
        angular.module("app.ui.ctrls", []).controller("LoaderCtrl", ["$scope", "cfpLoadingBar", function($scope, cfpLoadingBar) {
            return $scope.start = function() {
                return cfpLoadingBar.start()
            }, $scope.inc = function() {
                return cfpLoadingBar.inc()
            }, $scope.set = function() {
                return cfpLoadingBar.set(.3)
            }, $scope.complete = function() {
                return cfpLoadingBar.complete()
            }
        }]).controller("NotifyCtrl", ["$scope", "logger", function($scope, logger) {
            return $scope.notify = function(type) {
                switch (type) {
                    case "info":
                        return logger.log("Heads up! This alert needs your attention, but it's not super important.");
                    case "success":
                        return logger.logSuccess("Well done! You successfully read this important alert message.");
                    case "warning":
                        return logger.logWarning("Warning! Best check yo self, you're not looking too good.");
                    case "error":
                        return logger.logError("Oh snap! Change a few things up and try submitting again.")
                }
            }
        }]).controller("AlertDemoCtrl", ["$scope", function($scope) {
            return $scope.alerts = [{
                type: "success",
                msg: "Well done! You successfully read this important alert message."
            }, {
                type: "info",
                msg: "Heads up! This alert needs your attention, but it is not super important."
            }, {
                type: "warning",
                msg: "Warning! Best check yo self, you're not looking too good."
            }, {
                type: "danger",
                msg: "Oh snap! Change a few things up and try submitting again."
            }], $scope.addAlert = function() {
                var num, type;
                switch (num = Math.ceil(4 * Math.random()), type = void 0, num) {
                    case 0:
                        type = "info";
                        break;
                    case 1:
                        type = "success";
                        break;
                    case 2:
                        type = "info";
                        break;
                    case 3:
                        type = "warning";
                        break;
                    case 4:
                        type = "danger"
                }
                return $scope.alerts.push({
                    type: type,
                    msg: "Another alert!"
                })
            }, $scope.closeAlert = function(index) {
                return $scope.alerts.splice(index, 1)
            }
        }]).controller("ProgressDemoCtrl", ["$scope", function($scope) {
            return $scope.max = 200, $scope.random = function() {
                var type, value;
                value = Math.floor(100 * Math.random() + 10), type = void 0, type = 25 > value ? "success" : 50 > value ? "info" : 75 > value ? "warning" : "danger", $scope.showWarning = "danger" === type || "warning" === type, $scope.dynamic = value, $scope.type = type
            }, $scope.random()
        }]).controller("AccordionDemoCtrl", ["$scope", function($scope) {
            
        }]).controller("CollapseDemoCtrl", ["$scope", function($scope) {
            return $scope.isCollapsed = !1
        }]).controller("ModalDemoCtrl", ["$scope", "$modal", "$log", function($scope, $modal, $log) {
        	$scope.items = ["item1", "item2", "item3"], $scope.open = function() {
                var modalInstance;
                modalInstance = $modal.open({
                    templateUrl: "myModalContent.html",
                    controller: "ModalInstanceCtrl",
                    resolve: {
                        items: function() {
                            return $scope.items
                        }
                    }
                }), modalInstance.result.then(function(selectedItem) {
                    $scope.selected = selectedItem
                }, function() {
                    $log.info("Modal dismissed at: " + new Date)
                })
            }
        }]).controller("ModalInstanceCtrl", ["$scope", "$modalInstance", "items", function($scope, $modalInstance, items) {
        	
        	$scope.items = items,
        	$scope.selected = {
                    item: $scope.items[0]
             }, $scope.ok = function() {
                    $modalInstance.close($scope.selected.item)
             }, $scope.cancel = function() {
                    $modalInstance.dismiss("cancel")
             }
        }]).controller("PaginationDemoCtrl", ["$scope", function($scope) {
            return $scope.totalItems = 64, $scope.currentPage = 4, $scope.maxSize = 5, $scope.setPage = function(pageNo) {
                return $scope.currentPage = pageNo
            }, $scope.bigTotalItems = 175, $scope.bigCurrentPage = 1
        }]).controller("TabsDemoCtrl", ["$scope", function($scope) {
            return $scope.tabs = [{
                title: "Dynamic Title 1",
                content: "Dynamic content 1.  Consectetur adipisicing elit. Nihil, quidem, officiis, et ex laudantium sed cupiditate voluptatum libero nobis sit illum voluptates beatae ab. Ad, repellendus non sequi et at."
            }, {
                title: "Disabled",
                content: "Dynamic content 2.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil, quidem, officiis, et ex laudantium sed cupiditate voluptatum libero nobis sit illum voluptates beatae ab. Ad, repellendus non sequi et at.",
                disabled: !0
            }], $scope.navType = "pills"
        }])
    }.call(this),
    function() {
        "use strict";
        angular.module("app.ui.directives", []).directive("uiTime", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    var checkTime, startTime;
                    return startTime = function() {
                        var h, m, s, t, time, today;
                        return today = new Date, h = today.getHours(), m = today.getMinutes(), s = today.getSeconds(), m = checkTime(m), s = checkTime(s), time = h + ":" + m + ":" + s, ele.html(time), t = setTimeout(startTime, 500)
                    }, checkTime = function(i) {
                        return 10 > i && (i = "0" + i), i
                    }, startTime()
                }
            }
        }]).directive("uiWeather", [function() {
            return {
                restrict: "A",
                link: function(scope, ele, attrs) {
                    var color, icon, skycons;
                    return color = attrs.color, icon = Skycons[attrs.icon], skycons = new Skycons({
                        color: color,
                        resizeClear: !0
                    }), skycons.add(ele[0], icon), skycons.play()
                }
            }
        }])
    }.call(this),
    
    function() {
        "use strict";
        angular.module("app.ui.services", []).factory("logger", [function() {
            var logIt;
            return toastr.options = {
                closeButton: !0,
                positionClass: "toast-bottom-right",
                timeOut: "1500"
            }, logIt = function(message, type) {
                return toastr[type](message)
            }, {
                log: function(message) {
                    logIt(message, "info")
                },
                logWarning: function(message) {
                    logIt(message, "warning")
                },
                logSuccess: function(message) {
                    logIt(message, "success")
                },
                logError: function(message) {
                    logIt(message, "error")
                }
            }
        }])
    }.call(this),
    function() {
        angular.module("app.directives", []).directive("imgHolder", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return Holder.run({
                        images: ele[0]
                    })
                }
            }
        }]).directive("customBackground", function() {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$location", function($scope, $element, $location) {
                    var addBg, path;
                    return path = function() {
                        return $location.path()
                    }, addBg = function(path) {
                        switch ($element.removeClass("body-home body-special body-tasks body-lock"), path) {
                            case "/":
                                return $element.addClass("body-home");
                            case "/404":
                            case "/pages/500":
                            case "/pages/signin":
                            case "/pages/signup":
                            case "/signup":
                            	return $element.addClass("body-special");
                            case "/pages/lock-screen":
                                return $element.addClass("body-special body-lock");
                            case "/signin":
                                return $element.addClass("body-special");
                            case "/tasks":
                                return $element.addClass("body-tasks")
                        }
                    }, addBg($location.path()), $scope.$watch(path, function(newVal, oldVal) {
                        return newVal !== oldVal ? addBg($location.path()) : void 0
                    })
                }]
            }
        }).directive("uiColorSwitch", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return ele.find(".color-option").on("click", function(event) {
                        var $this, hrefUrl, style;
                        if ($this = $(this), hrefUrl = void 0, style = $this.data("style"), "loulou" === style) hrefUrl = "styles/main.css", $('link[href^="styles/main"]').attr("href", hrefUrl);
                        else {
                            if (!style) return !1;
                            style = "-" + style, hrefUrl = "styles/main" + style + ".css", $('link[href^="styles/main"]').attr("href", hrefUrl)
                        }
                        return event.preventDefault()
                    })
                }
            }
        }]).directive("toggleMinNav", ["$rootScope", function($rootScope) {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    var $window, Timer, app, updateClass;
                    return app = $("#app"), $window = $(window), ele.on("click", function(e) {
                        return app.hasClass("nav-min") ? app.removeClass("nav-min") : (app.addClass("nav-min"), $rootScope.$broadcast("minNav:enabled")), e.preventDefault()
                    }), Timer = void 0, updateClass = function() {
                        var width;
                        return width = $window.width(), 768 > width ? app.removeClass("nav-min") : void 0
                    }, $window.resize(function() {
                        var t;
                        return clearTimeout(t), t = setTimeout(updateClass, 300)
                    })
                }
            }
        }]).directive("collapseNav", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    var $a, $aRest, $lists, $listsRest, app;
                    return $lists = ele.find("ul").parent("li"), $lists.append('<i class="fa fa-caret-right icon-has-ul"></i>'), $a = $lists.children("a"), $listsRest = ele.children("li").not($lists), $aRest = $listsRest.children("a"), app = $("#app"), $a.on("click", function(event) {
                        var $parent, $this;
                        return app.hasClass("nav-min") ? !1 : ($this = $(this), $parent = $this.parent("li"), $lists.not($parent).removeClass("open").find("ul").slideUp(), $parent.toggleClass("open").find("ul").slideToggle(), event.preventDefault())
                    }), $aRest.on("click", function() {
                        return $lists.removeClass("open").find("ul").slideUp()
                    }), scope.$on("minNav:enabled", function() {
                        return $lists.removeClass("open").find("ul").slideUp()
                    })
                }
            }
        }]).directive("highlightActive", [function() {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$attrs", "$location", function($scope, $element, $attrs, $location) {
                    var highlightActive, links, path;
                    return links = $element.find("a"), path = function() {
                        return $location.path()
                    }, highlightActive = function(links, path) {
                        return path = "#" + path, angular.forEach(links, function(link) {
                            var $li, $link, href;
                            return $link = angular.element(link), $li = $link.parent("li"), href = $link.attr("href"), $li.hasClass("active") && $li.removeClass("active"), 0 === path.indexOf(href) ? $li.addClass("active") : void 0
                        })
                    }, highlightActive(links, $location.path()), $scope.$watch(path, function(newVal, oldVal) {
                        return newVal !== oldVal ? highlightActive(links, $location.path()) : void 0
                    })
                }]
            }
        }]).directive("toggleOffCanvas", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return ele.on("click", function() {
                        return $("#app").toggleClass("on-canvas")
                    })
                }
            }
        }]).directive("slimScroll", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return ele.slimScroll({
                        height: "100%"
                    })
                }
            }
        }]).directive("goBack", [function() {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$window", function($scope, $element, $window) {
                    return $element.on("click", function() {
                        return $window.history.back()
                    })
                }]
            }
        }])
    }.call(this),
    function() {
        "use strict";
        angular.module("app.localization", []).factory("localize", ["$http", "$rootScope", "$window", function($http, $rootScope, $window) {
            var localize;
            return localize = {
                language: "",
                url: void 0,
                resourceFileLoaded: !1,
                successCallback: function(data) {
                    return localize.dictionary = data, localize.resourceFileLoaded = !0, $rootScope.$broadcast("localizeResourcesUpdated")
                },
                setLanguage: function(value) {
                    return localize.language = value.toLowerCase().split("-")[0], localize.initLocalizedResources()
                },
                setUrl: function(value) {
                    return localize.url = value, localize.initLocalizedResources()
                },
                buildUrl: function() {
                    return localize.language || (localize.language = ($window.navigator.userLanguage || $window.navigator.language).toLowerCase(), localize.language = localize.language.split("-")[0]), "i18n/resources-locale_" + localize.language + ".js"
                },
                initLocalizedResources: function() {
                    var url;
                    return url = localize.url || localize.buildUrl(), $http({
                        method: "GET",
                        url: url,
                        cache: !1
                    }).success(localize.successCallback).error(function() {
                        return $rootScope.$broadcast("localizeResourcesUpdated")
                    })
                },
                getLocalizedString: function(value) {
                    var result, valueLowerCase;
                    return result = void 0, localize.dictionary && value ? (valueLowerCase = value.toLowerCase(), result = "" === localize.dictionary[valueLowerCase] ? value : localize.dictionary[valueLowerCase]) : result = value, result
                }
            }
        }]).directive("i18n", ["localize", function(localize) {
            var i18nDirective;
            return i18nDirective = {
                restrict: "EA",
                updateText: function(ele, input, placeholder) {
                    var result;
                    return result = void 0, "i18n-placeholder" === input ? (result = localize.getLocalizedString(placeholder), ele.attr("placeholder", result)) : input.length >= 1 ? (result = localize.getLocalizedString(input), ele.text(result)) : void 0
                },
                link: function(scope, ele, attrs) {
                    return scope.$on("localizeResourcesUpdated", function() {
                        return i18nDirective.updateText(ele, attrs.i18n, attrs.placeholder)
                    }), attrs.$observe("i18n", function(value) {
                        return i18nDirective.updateText(ele, value, attrs.placeholder)
                    })
                }
            }
        }]).controller("notificationController",["$scope","$rootScope","$interval","$http","logger","$location", function($scope,$rootScope, $interval, $http, logger, $location) {
        	$scope.getMyApprovals = function() {
        		$location.path("/dataSourceToAccept");
        	};
        	
        	var notificationPromise;
        	
        	if($location.path() != "/home" && $location.path() != "/signin" && $location.path() != "/signup" 
				&& $location.path() != "/resetPassword"){
        		notifyStart();
        	}
        	
        	$scope.$on('notificationUpdateEvent', getAlertCount);
        	
        	$scope.$on('notificationStartEvent', notifyStart);
        	
        	function notifyStart() {
        		getAlertCount();
        		notificationPromise = $interval(getAlertCount, 10000); //10 seconds
        	}
        	
        	$scope.$on('notificationStopEvent', function() {
        		if(notificationPromise) {
        			$interval.cancel(notificationPromise);
        			notificationPromise = null;
        		}
        	});
        	
        	function getAlertCount() {
        		$http({
					method:"POST",

					url:"/getNotifications"
				}).success(function(data){
					if(data) {
						$scope.notificationCount = ((data.MyRequests+data.MyApprovals) > 9) ? "9+" : (data.MyRequests+data.MyApprovals);
						$rootScope.myRequestNotificationCount = data.MyRequests;
						$rootScope.myApprovalNotificationCount = data.MyApprovals;
					}
				}).error(function(data){
					//logger.logError(data);
				});
        	}
        }]).controller("LangCtrl", ["$scope", "localize", "$location","$anchorScroll", "$interval", "$route","$modal","$log","$cookieStore","UserService", function($scope, localize, $location, $anchorScroll, $interval, $route, $modal, $log, $cookieStore, UserService) {
           
        	var el = document.querySelectorAll('.hamburger');
    		for(var i=0; i<=el.length; i++) {
    		  if(typeof el[i] != "undefined"){
    			  el[i].addEventListener('mouseover', function() {
	         	    this.classList.toggle('active');
	         	  }, false);
	         	 el[i].addEventListener('mouseout', function() {
	          	    this.classList.toggle('active');
	          	  }, false); 
    		  }
    		  
         	}
        	
        		$scope.saveLastUpdatedGroup = function(groupId){
        			$http({
        				method: "POST",
        				url: "/updateGroupNameInApplicationUser",
        				data : groupId
        			}).success(function(data){
        				console.log($scope.value);
        			}).error(function(data){
        				logger.logError(data);
        			});
        		};
        		
	        	$scope.loginAs=function(groupId){
	        		$cookieStore.put("groupId",parseInt(groupId));
	        		$scope.user.GROUP_DETAILS=UserService.getUpdatedGroupMap(groupId, $scope.user.GROUP_DETAILS);
	        		$cookieStore.put("groupMap",$scope.user.GROUP_DETAILS);
	        		$scope.saveLastUpdatedGroup(groupId);
	        		var location= $scope.getHomeForLand();
	        		$location.path(location.substring(1, location.length + 1));
	        	}        	
	        	
	        	$scope.urlhash;
	        	
        	
        	function waitForElementToDisplay(selector, time) {
				
				if(document.querySelector(selector)!=null) {
					
					return true;
		        }
		        else{
		            setTimeout(function() {
		                waitForElementToDisplay(selector, time);
		            }, time);
		        }
		    }
    		
    		function isElementFound(selector) {
    			if(document.querySelector(selector)!=null) {
					
					return true;
		        }
		        else{
		        	
		        	return false;
		        	
		        }
			}
    		
    		$interval(function() {
    			$scope.urlhash = window.location.hash;
    			if($scope.urlhash.indexOf("#/dataSourceView")>=0 || $scope.urlhash.indexOf("#/appDetail")>=0 || $scope.urlhash.indexOf("#/workflow/")>=0){
    				jQuery("#notifyTour").addClass("faa-burst animated");
    				
    			}else{
    				
    				jQuery("#notifyTour").removeClass("faa-burst animated");
    			}
    			
			}, 1000);
    		
    		$scope.checkEle = function(){
    			$scope.urlhash = window.location.hash;
    			$scope.hideTour=false;
    			
    			/* prevention tour is started and element is not found  n hide tour  */
	    		if($scope.urlhash.indexOf("#/dataSourceView")>=0){
	    			
	    			$scope.stopInterval = $interval(function() {
	    				
	    				$scope.enabletour = document.querySelectorAll('#selectdsclick');
	        			
	        			if($scope.enabletour.length>0){
	        				$interval.cancel($scope.stopInterval);
	        				jQuery('#enabletour').removeClass('disable-element');
	        			}else{
	        				
	        				jQuery('#enabletour').addClass('disable-element');
	        				  
	        			}
	        			
					}, 200);
	    			
	    		}else if($scope.urlhash.indexOf("#/appDetail")>=0){
	    			
	    			$scope.stopInterval = $interval(function() {
	    				
	    				$scope.enabletour = document.querySelectorAll('#selectappclick');
	        			
	        			if($scope.enabletour.length>0){
	        				$interval.cancel($scope.stopInterval);
	        				jQuery('#enabletour').removeClass('disable-element');
	        			}else{
	        				
	        				jQuery('#enabletour').addClass('disable-element');
	        				  
	        			}
	        			
					}, 200);
	    			
	    		}else if($scope.urlhash.indexOf("#/workflow/")>=0){
	    			
	    		}else{
	    			
	    			$scope.hideTour=true;
	    		
	    		}
    		}
    		
        	$scope.startTour = function() {
        		var intro;
        		$scope.urlhash = window.location.hash;
        		
        		if($scope.urlhash.indexOf("#/dataSourceView")>=0){
        			var id = $location.hash();
        		    $location.hash("top");
        		    $anchorScroll();
        		    $location.hash(id);
        			
        			intro = introJs();
        			
        			
        			 $scope.IntroOptions = {
        					 showProgress:true,
        					 scrollToElement:true,
        					 showStepNumbers:false,
        					 //showBullets:true,
        					 disableInteraction:true,
        					 steps:[
        			            {
        			                element: "#createdsclick",
        			                intro: "<div class='introjs-tooltiptitle'>Want to connect to data sources?</div>" +
        			                		"<div class='introjs-tooltipcontent'>To connect, validate and extract data sources configure datasource.</div>",
        			                tooltiptext:"tooltiptext",
        			                position:"left"	
        			            },
        			            {
        			                element: "#selectds",
        			                intro: "<div class='introjs-tooltiptitle'>Want to connect to data sources? </div>" +
			                				"<div class='introjs-tooltipcontent'>The datasources configured are listed in the table. </div>",
			                		position: 'top'
        			            },
        			            {
        			                element: "#dsgroupname",
        			                intro: "<div class='introjs-tooltiptitle'>Want to logically group data sources? </div>" +
			                				"<div class='introjs-tooltipcontent'>Datasources can be logically grouped together within a DS-Group. By default, data sources are listed under global datasource group. This can be re-assigned to other DS-Groups.</div>",
			                		position: 'top'
        			            },
        			            {
        			                element: "#dsaction",
        			                intro: "<div class='introjs-tooltiptitle'>Data sources are configured. What next?</div>" +
			                			"<div class='introjs-tooltipcontent'>Various actions like loading data onto Hadoop system, Re-assign to DS-Group," +
			                			" Renaming the data source, deleting a data source, can be performed. </div>",
        			                position: 'auto'
        			                
        			            },
        			            {
        			                element: "#monitor",
        			                intro: "<div class='introjs-tooltiptitle'>Monitoring </div>" +
		                			"<div class='introjs-tooltipcontent'>While loading data into data sources, the status of loading jobs can be monitored.</div>",
        			                position: 'right'
        			                
        			            },
        			            {
        			                element: "#explore",
        			                intro: "<div class='introjs-tooltiptitle'>Want to explore data sources? </div>" +
		                			"<div class='introjs-tooltipcontent'>The Data Sources can be explored using the SQL editor. Various statistical analysis like correlation can be performed.</div>",
        			                position: 'right'
        			                
        			            }]
        			 }
        			 
        			 intro.setOptions($scope.IntroOptions);
        			 
        			 intro.start();
        			        			
        		}else if("#/appDetail" == $scope.urlhash){
        			
	        		var id = $location.hash();
        		    $location.hash("top");
        		    $anchorScroll();
        		    $location.hash(id);
	        		
        			intro = introJs();
        			
        			
        			 $scope.IntroOptions = {
        					 showProgress:true,
        					 scrollToElement:true,
        					 showStepNumbers:false,
        					 //showBullets:true,
        					 disableInteraction:true,
        					 steps:[
        			            {
        			                element: "#createapp",
        			                intro: "<div class='introjs-tooltiptitle'>Want to create data pipeline flow? </div>" +
		                			"<div class='introjs-tooltipcontent'>Configure a data pipeline flow by configuring an Analysis app. In this app new fields can be created, data can be filtered, data can be matched by look up and many more functions can be performed.</div>",
        			                position:"left"	
        			            },
        			            {
        			                element: "#selectapp",
        			                intro: "<div class='introjs-tooltipcontent'>The Analysis apps configured are listed here.</div>",
        			                position: 'top'
        			            },
        			            {
        			                element: "#appaction",
        			                intro: "<div class='introjs-tooltiptitle'>What happens after configuring analysis apps? </div>" +
		                			"<div class='introjs-tooltipcontent'>Various actions like running the data pipeline flow, exploring the results, editing the data pipeline flow, deleting can be performed. </div>",
        			                position: 'auto'
        			            },
        			            {
        			                element: "#monitor",
        			                intro: "<div class='introjs-tooltiptitle'>Monitoring </div>" +
		                			"<div class='introjs-tooltipcontent'>The running status of analysis apps can be can be monitored.</div>",
        			                position: 'right'
        			                
        			            }
        			           ]
        			 }
        			 
        			 intro.setOptions($scope.IntroOptions);
        			 
        			 intro.start();
        			 
        			
        		}
        		
			}
        	
        	
            return $scope.lang = "English", $scope.setLang = function(lang) {
                switch (lang) {
                    case "English":
                        localize.setLanguage("EN-US");
                        break;
                    case "EspaÃƒÆ’Ã‚Â±ol":
                        localize.setLanguage("ES-ES");
                        break;
                    case "ÃƒÂ¦Ã¢â‚¬â€�Ã‚Â¥ÃƒÂ¦Ã…â€œÃ‚Â¬ÃƒÂ¨Ã‚ÂªÃ…Â¾":
                        localize.setLanguage("JA-JP");
                        break;
                    case "ÃƒÂ¤Ã‚Â¸Ã‚Â­ÃƒÂ¦Ã¢â‚¬â€œÃ¢â‚¬Â¡":
                        localize.setLanguage("ZH-TW");
                        break;
                    case "Deutsch":
                        localize.setLanguage("DE-DE");
                        break;
                    case "franÃƒÆ’Ã‚Â§ais":
                        localize.setLanguage("FR-FR");
                        break;
                    case "Italiano":
                        localize.setLanguage("IT-IT");
                        break;
                    case "Portugal":
                        localize.setLanguage("PT-BR");
                        break;
                    case "Ãƒï¿½Ã‚Â Ãƒâ€˜Ã†â€™Ãƒâ€˜Ã¯Â¿Â½Ãƒâ€˜Ã¯Â¿Â½Ãƒï¿½Ã‚ÂºÃƒï¿½Ã‚Â¸Ãƒï¿½Ã‚Â¹ Ãƒâ€˜Ã¯Â¿Â½Ãƒï¿½Ã‚Â·Ãƒâ€˜Ã¢â‚¬Â¹Ãƒï¿½Ã‚Âº":
                        localize.setLanguage("RU-RU");
                        break;
                    case "ÃƒÂ­Ã¢â‚¬Â¢Ã…â€œÃƒÂªÃ‚ÂµÃ‚Â­ÃƒÂ¬Ã¢â‚¬â€œÃ‚Â´":
                        localize.setLanguage("KO-KR")
                }
                return $scope.lang = lang
            }
        }]);
    }.call(this),
    function() {
    
      //"use strict";	
       	
       var app=angular.module("app.controllers", ['ngCookies']);
       
       app.directive('dragable', function(){   
        	  return {
        		    restrict: 'EA',
        		    link : function(scope,ele,attr){
        		      $(ele).draggable(
        		    		  { handle: ".modal-header" }	  
        		      );
        		    }
        		  }  
       });
       
       
       app.constant('USER_ACL',{
    	 
    	 CATLOG_APP : 'CATLOG_APP', 
    	 CONNECTOR : 'CONNECTOR', 
    	 CONFIG_NEW_CONNETOR : 'CONFIG_NEW_CONNETOR',
    	 
    	 
		 DATA_SOURCES : 'DATA_SRCS',
		 CONFIG_NEW_DS :'CONFIG_NEW',
		 LOAD_DS_DATA :'LD_DS_DATA',
		 ASSGN_DS_GROUP : 'ASN_DS_GRP',
		 RENAME_DS : 'RN_DS',
		 DELETE_DS : 'DL_DS',
		 DETAILS_DS : 'DS_DTLS',
		 EDIT_DS :'EDT_DS',
		 
		 DATA_SOURCE_GROUPS : 'DATA_SRC_G',
	     CONFIG_NEW_DSG : 'CNFG_NW_DS',
	     VIEW_DS_GROUP :'VW_DSGRP',
	     LOAD_DS_GROUP : 'LD_DSGRP',
		 
		 
		 EXPLORE :'EXPLR',
		 
		 DATA_EXPLORE :'DT_EXPLR',
		 DATA_EXPLORE_SAVE_QUERY : 'DTEXPLR_SV',
		 DATA_EXPLORE_RUN_QUERY : 'DTEXPLR_RN',
		 DATA_EXPLORE_SAVE_OP : 'DTEXPLR_SV_OP',
		 DATA_EXPLORE_VIEW_GRAPH : 'DTEXPLR_VW_GRPH',
		 DATA_EXPLORE_DOWNLOAD : 'DTEXPLR_DL',
		 
		 
		 STATISTICS : 'STATISTICS',
	
		 STATISTICS_DATA_PROFILING : 'DT_PRFLG',
		 STATISTICS_CORRELATION : 'CRRLTN',
		 PROFILING_PREPROF_VIEW : 'CRRLTN_PRE',
		 PROFILING_POSTPROF_VIEW :'CRRLTN_PST',
		 
		 VISUALIZE : 'VISUALIZE',
		 VIEW_SETTINGS: 'VIEW_SETTINGS',
		 
		 MY_COLLECTIONS: 'MY_COLLECTIONS',
		 DISCOVERS: 'DISCOVERS',
		 ADD_COLLECTIONS: 'ADD_COLLECTIONS',
		 REQUEST_ACCESS: 'REQUEST_ACCESS',
		 MY_REQUEST: 'MY_REQUEST',
		 MY_APPROVALS: 'MY_APPROVALS',
		 RELATIONS : 'RELATIONS',
		 ADVANCE_SEARCH : 'ADVANCE_SEARCH',
		 CLEAR_SEARCH: 'CLEAR_SEARCH',
		 EXPRESSIONS: 'EXPRESSIONS',
		 SEARCH_APPS :'SRCH_APPS',
		 
		 
		 QUERY_CREATION : 'QRY_CRTN',
		 CREATE_NEW_QUERY : 'CRT_NW_QRY',
		 VIEW_QUERY : 'VW_QRY',
		 TEST_QUERY : 'TST_QRY',
		 
		 
		 
		 SETUP :'SETUP',
		 
		 USER_MANAGEMENT : 'USR_MANGMN',
		 CONFIG_NEW_USER:'CONFG_NW_USR',
		 EDIT_USER : 'EDT_USR',
		 DELETE_USER :'DLT_USR',
		 
		 
		 GROUP_CONFIG : 'GRP_CNFG',
		 CONFG_NEW_GROUP:'CNFG_NW_GR',
		 EDIT_GROUP : 'EDT_GRP',
		 DELETE_GROUP: 'DLT_GRP',
		 CONFG_NEW_SUBGROUP:'CNFG_NW_SBGRP',
		 EDIT_SUBGROUP : 'EDT_SBGRP',
		 DELETE_SUBGROUP : 'DLT_SBGRP', 
		 
		 ROLE_CONFIG :'ROLE_CNFG',
		 CONFIG_NEW_ROLE : 'CNFG_NW_RL',
		 EDIT_ROLE : 'EDT_RL',
		 DELETE_ROLE : 'DLT_RL',
			 
		 GLOSSARY : 'GLOSSARY',	 
		 TAGS_TAB : 'TAGS_TAB',
		 ADD_NEW_TAG : 'ADD_NEW_TAG',
		 EDIT_TAG : 'EDIT_TAG',
		 DLT_TAG : 'DLT_TAG',
	
		 ANLS_APPS : 'ANLS_APPS',
		 RUN_ANLS_A:'RUN_ANLS_A',
		 EDIT_ANLS_:'EDIT_ANLS_',
		 DLT_ANLS_A:'DLT_ANLS_A',
		 DLT_ANLS_A:'DLT_ANLS_A',
		 CNFG_NW_AN:'CNFG_NW_AN',
		 // category roles
		 CONFIG_NEW_CONN : 'CONFIG_NEW_CONN',
		 DLT_CNNCTN : 'DLT_CNNCTN',
		 EDIT_CONNECTION : 'EDIT_CONNECTION'
			 
       });
      

       var flavourLicense = [];
       //This will get called at all times except at the time of login.
       $http = angular.injector(["ng"]).get("$http");
       
       if(window.location.hash !="" && window.location.hash !="#/signin" && window.location.hash != "#/home" && window.location.hash != "#/contact" 
					&& window.location.hash != "#/requestDemo" && window.location.hash != "#/productoverview"){
			$http({
				url: "/getActions",
				method: "POST"
			}).success(function(data){
				flavourLicense = data;
			});
       }

       
       app.directive('roleCheck', function() {
           return {
               restrict : "A",
               controller : [ "$scope", "$element", "$location","$filter","UserService","FlavourService", "USER_ACL","$cookieStore","$routeParams", function($scope, $element, $location, $filter, UserService, FlavourService, USER_ACL,$cookieStore,$routeParams) {
            	   
                   		  if($cookieStore.get("groupId")){
                              UserService.getWithoutObjectRequest('/accessConfig/loadAccessControls').then(function(data){
                                UserService.setUser(data);
                                addBg($location.path());
	                              FlavourService.getFlavourPersonaObjectRequest().then(function(data){
	                            	  FlavourService.setPersonaObject(data);
	      	                  	      UserService.setMaxPersonaName(data);
	                                });
                              });
                           }
                           
                           $scope.user  = UserService.getUser();
                           $scope.USER_ROLE_CONST = USER_ACL;
                   
                           var addBg, path;
                           
                           return path = function() {
                               return $location.path();
                           }, addBg = function(path) {
                        	
                               if(path == "/signin"){
                                   if(checkCookies()){
                                       $("#app").removeClass("body-special");
                                       $location.path("/landing");
                                   }

                               }else if(path == "/signup" ||  path.includes("/modelSummary")){
                                   return;
                               }else if(path == "/landing" || path == "/userProfile" || path == "/socialApp"){
                                   if(checkCookies()){
                                       return;
                                   }else{
                                    $location.path("/signin");
                                   }
                               }else if(path == "/resetPassword"){
                            	   $location.path("/resetPassword");
                               }else if (path == "/home") {
                            	   if(checkCookies()){
                            		   $location.path("/landing");
                            	   }else {
                            		   $scope.logoutCookie();
                            	   }
                               }
                               
                               // new code for publish api
                               else if(path == "/connectors" || path == "/subconnectors" || path == "/connections" || path == "/publish"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.CONNECTOR)){
                                	   $location.path("/landing");
                                   }   
                               }
                               else if(path == "/dataSourceApiList"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.ANLS_APPS)){
                                	   $location.path("/landing");
                                   }   
                               }
       						   else if(path == "/glossaryView"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.GLOSSARY)){
                                	   $location.path("/landing");
                                   }   
                               }else if(path == "/categoryView"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.GLOSSARY)){
                                	   $location.path("/landing");
                                   }   
                               }else if(path == "/hiveExplore"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.DATA_EXPLORE)){
                                	   $location.path("/landing");
                                   }
                               }else if(path == "/statistical"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.STATISTICS)){
                                	   $location.path("/landing");
                                   }
                               }else if(path == "/visualize"){
      								if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.VISUALIZE)){
      									$location.path("/landing");
       								}
       						   }else if(path == "/modelsView"){
                            	   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.CUSTOM_COMPONENT)){
                            		   $location.path("/landing");
                            	   }
                               } else if(path == "/configNewConnection"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.CONNECTIONS)){
                                	   $location.path("/landing");
                                   }
                               }   
                               else if(path == "/userManagement"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.USER_MANAGEMENT)){
                                	   $location.path("/landing");
                                   }
                               }else if(path == "/groupConfig"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.GROUP_CONFIG)){
                                	   $location.path("/landing");
                                   }
                               }else if(path == "/accessConfig"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.ROLE_CONFIG)){
                                	   $location.path("/landing");
                                   }
                               }else if(path.includes("searchQueryExplorer")){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.VIEW_QUERY)){
                                	   $location.path("/landing");
                                   }
                               }else if(path.includes("/discover")){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.DISCOVERS)){
                                	   $location.path("/landing");
                                   }
                               }else if(path.includes("/myCollection")){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.MY_COLLECTIONS)){
                                	   $location.path("/landing");
                                   }
                               }else if(path.includes("/dataSourceRequests")){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.MY_REQUEST)){
                                	   $location.path("/landing");
                                   }
                               }else if(path == "/createDsRealations"){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.RELATIONS)){
                                	   $location.path("/landing");
                                   }
                               }else if(path.includes("/dataSourceToAccept")){
                                   if($filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES,$scope.USER_ROLE_CONST.MY_APPROVALS)){
                                	   $location.path("/landing");
                                   }
                               }else{
                            	   if(checkCookies()){
//                                       $("#app").removeClass("body-special");
                            		   $location.path("/landing");
                                   }else{
                                	   $scope.logoutCookie();
                                   }
                                  
                               }
                        	  
                           }, checkCookies=function(){
                               var allcookies = document.cookie;
                            if (allcookies) {
                                var cookiearray = allcookies.split(';');
                                var storeNameCookie = _.filter(cookiearray, function(obj){return obj.split('=')[0].indexOf("emailCookie") > -1;});
                                if(storeNameCookie[0]){
                                    return true;
                                }else{
                                    return false;
                                }
                            }
                            return false;
                           },$scope.$watch(path,function(newVal, oldVal) {
      							if(oldVal == "/landing" && newVal == "/home"){
       								window.location.reload();
       							}else if(oldVal == "/landing" && newVal == "/productoverview"){
       								window.location.reload();
       							}else {
       	   							return newVal !== oldVal ? addBg($location.path()) : void 0;
       							}

       						});
                       } ]
           };
         });       
        app.run(['$rootScope', '$sce', function($rootScope, $sce) {
    	    $rootScope.page = {
    	        setTitle: function(title) {
    	            this.title = ' Mosaic <sup> TM </sup>.Decisions - ' + title ;
    	        }
    	    }
    	    
    	    $rootScope.getTitleForCurrent = function(){
    	    	return $sce.trustAsHtml($rootScope.page.title);
    	    }
    	    
    	    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
    	        $rootScope.page.setTitle(current.$$route.title || 'Enterprise Intelligence Platform');
    	    });
    	}]);

        app.config(["$httpProvider" , function($httpProvider){
       		$httpProvider.defaults.timeout = 5000;
       		$httpProvider.interceptors.push('ExceptionHandlerInterceptor');
       	}]);
        app.service("GetAccessData",function(){
        	var indicator = false;
        	var sessionExpire = false;
        	var resetIndicator = false;
        	var acionCode;

        	return {
        		getIndicator : getIndicator,
        		setActionCode : setActionCode,
        		getActionCode : getActionCode,
        		setIndicator:setIndicator,
        		setSessionExpire : setSessionExpire,
        		getSessionExpire : getSessionExpire,
        		setSessionErrorMsg : setSessionErrorMsg,
        		getSessionErrorMsg : getSessionErrorMsg,
        		getResetPassWord : getResetPassWord,
        		setResetPassWord : setResetPassWord
    		 };
    		 
    		function setResetPassWord (data) {
         		resetIndicator = data;
         	}
         	
         	function getResetPassWord() {
         		return resetIndicator;
         	}
        	
        	function getIndicator () {
        		return indicator;
        	}
        	
        	function setActionCode (data) {
        		acionCode = data;
        	}
        	
        	function getActionCode () {
        		return acionCode;
        	}
        	
        	function setIndicator(data){
        		indicator = data;
        	}
        	
        	function setSessionExpire (data) {
        		sessionExpire = data;
        	}
        	
        	function getSessionExpire () {
        		return sessionExpire;
        	}
        	
        	function setSessionErrorMsg (data) {
        		sessionExpire = data;
        	}
        	
        	function getSessionErrorMsg () {
        		return sessionExpire;
        	}
        });
        app.service("UserService",function($http,$q,logger){
     	   
     		 var user = {
     				 ACTIONS_CODES : [],
     				 RESULT: '',
     				 GROUP_DETAILS : [],
     				 MASTERS_CODES : [],
     				 PERSONA_NAME : []
     		 };
     		 
     		 
     		 return {
     			 getUser : fetchUser,
     			 setUser : saveUser,
     			 getUserRoles : fetchRoles,
     			 getWithoutObjectRequest:getWithoutObject,
     			 getUpdatedGroupMap:getUpdatedGroupMap,
     			 saveActionCodes:saveActionCodes,
     			 getMasterCodes:getMasterCodes,
     			 setMasterCodes:setMasterCodes,
     			 getMaxPersonaName:getMaxPersonaName,
     			 setMaxPersonaName:setMaxPersonaName
     		 };
     		 
     		
     		function getUpdatedGroupMap(groupId, groupObject){
     			var groupList=angular.forEach(groupObject, function(obj){
     				if(obj.groupId==groupId){
     					obj.isSelected=true;
     				}else{
     					obj.isSelected=false;
     				}
     			})
     			return groupList;
    		}
     		 
     		 function fetchUser(){
     			 return user;
     		 }
     		 
     		function getWithoutObject(url){
	   			var deferred=$q.defer();
	   			$http.post(url).success(function(data){
	   				deferred.resolve(data);
	   			}).error(function(data){
	   				logger.logError(data);
	   				deferred.reject(data);
	   			});
	   			return deferred.promise;
     		}
     		 
     		 
     		 function saveUser(userobject){
     			 user.ACTIONS_CODES = userobject.ACTIONS_CODES || [];
     			 user.RESULT = userobject.RESULT || '';
     			 user.GROUP_DETAILS = userobject.GROUP_DETAILS || [];
     			 user.MASTERS_CODES = userobject.MASTERS_CODES || [];
     			 user.PERSONA_NAME = userobject.PERSONA_NAME || '';
     		 }
     		 
     		 function saveActionCodes(data){
     			 user.ACTIONS_CODES = data.ACTIONS_CODES || [];
             }
     		 
     		 function fetchRoles(){
     			 return user.ACTIONS_CODES;
     		 } 
     		 
     		 function getMasterCodes(){
     			 return user.MASTERS_CODES;
     		 }
     		 
     		 function setMasterCodes(data){
     			 user.MASTERS_CODES = data.MASTERS_CODES || [];
     		 }
     		 
     		 function setMaxPersonaName(data){
     			user.PERSONA_NAME = data.PERSONA_NAME;
     		 }
     		 
     		 function getMaxPersonaName(){
     			 return user.PERSONA_NAME;
     		 }
     	 });

        app.service("FlavourService",function($http,$q,logger){
      	   
   		 var flavourOperationUserDisplay = false;//Manager
		 var flavourBusinessUserDisplay = false;//Business
		 var flavourDesignUserDisplay = false;//Design

    		 
      		function getFlavourPersonaObject(url) {
	   			var deferred=$q.defer();
	   			$http.post("/getPersonas").success(function(data){
	   				deferred.resolve(data);
	   			}).error(function(data){
	   				logger.logError(data);
	   				deferred.reject(data);
	   			});
	   			return deferred.promise;
     		}    		 
    		 return {
    			 getFlavourPersonaObjectRequest:getFlavourPersonaObject,
    			 setPersonaObject:setPersonaObject,
    			 getFlavourOperationUserDisplay:getFlavourOperationUserDisplay,
    			 getFlavourBusinessUserDisplay:getFlavourBusinessUserDisplay,
    			 getFlavourDesignUserDisplay:getFlavourDesignUserDisplay
    		 };

    		 function setPersonaObject(data){
    			 if (undefined != data && null != data) {
	    			for (var eachPersona=0; eachPersona < data.length; eachPersona++) {
	    				if (data[eachPersona] == "1") flavourBusinessUserDisplay = true;
	    				if (data[eachPersona] == "2") flavourDesignUserDisplay = true;
	    				if (data[eachPersona] == "3") flavourOperationUserDisplay = true;
	    			}
    			 }
    			 var selectedPersonas = [];
    			 if (flavourDesignUserDisplay)
    				 selectedPersonas.push('Design');
    			 if (flavourBusinessUserDisplay)
    				 selectedPersonas.push('Business');
    			 if (flavourOperationUserDisplay)
    				 selectedPersonas.push('Manager');
    			 
    			 if (!(selectedPersonas.indexOf(data.PERSONA_NAME) > -1)) {
    				 data.PERSONA_NAME = selectedPersonas[0];
    			 }
    		 }
    		 
    		 function getFlavourOperationUserDisplay(){
    			 return flavourOperationUserDisplay;
    		 }
    		 function getFlavourBusinessUserDisplay(){
    			 return flavourBusinessUserDisplay;
    		 }
    		 function getFlavourDesignUserDisplay(){
    			 return flavourDesignUserDisplay;
    		 }
    	 });        
        
        app.factory("ExceptionHandlerInterceptor",function(GetAccessData, $q,$location,logger,$cookieStore,$window) {
   			return {
   				request : function(config) {
   					config.requestTimestamp = new Date().getTime();
   					return config;
   				},
   				response : function(response) {
   					response.config.responseTimestamp = new Date().getTime();

   					return response || $q.when(response);
   				},
   				responseError : function(response) {
   					if (response.status == "10000" && 
   							($cookieStore.get("emailCookie") !== null && $cookieStore.get("emailCookie") !== undefined)) {
   						// your session is expired
   		         	    localStorage.removeItem("userName");
   		         	    GetAccessData.setSessionExpire(true);
   		         	    GetAccessData.setSessionErrorMsg("Session Expired. Please login to continue.")
   					} else if (response.status == "401" && 
   							($cookieStore.get("emailCookie") !== null && $cookieStore.get("emailCookie") !== undefined)) {
   		         	    localStorage.removeItem("userName");
   		         	    GetAccessData.setSessionExpire(true);
   		         	    GetAccessData.setSessionErrorMsg("User information updated by the administrator. Please re-login to continue.")
   					} else if (response.status == "402" && 
   							($cookieStore.get("emailCookie") !== null && $cookieStore.get("emailCookie") !== undefined)) {
   						$.ajax({url: "/accessConfig/fetchNewRoleList", success: function(result){
   							GetAccessData.setIndicator(true);
   							GetAccessData.setActionCode(result);
   				        }});
   					} else  if(response.data && response.data.errorCode  == "E002"){
   						logger.logError(response.data.message);
   						$location.path("/signin");
   						
   					}else  if(response.data && response.data.errorCode  == "E001"){
   						
   						logger.logError(response.data.message);
   						$location.path("/signin");
   						
   					} else  if(response.data && response.data.errorCode  == "E003"){
   						
   						logger.logError(response.data.message);
   						//$location.path("/");
   						
   					} else if ( response.status == '500') {
   						//logger.logError(response.statusText);
   					}else if ( response.status == "403") {
   						logger.logError("Sorry!! You do not have permission to view this page");
   					}
   					return $q.reject(response);
   				}

   			};
   		});
        
         	
        app.controller("AppCtrl", ["$http","logger","$rootScope", "$scope", "$route" , "$location", "$cookieStore", "$templateCache", "UserService","FlavourService","USER_ACL","$interval", "$routeParams", "$filter", "GetAccessData","$window","$modal","$log", "$timeout","$anchorScroll",function($http,logger,$rootScope,$scope, $route, $location, $cookieStore , $templateCache, UserService, FlavourService, USER_ACL, $interval, $routeParams, $filter, GetAccessData,$window,$modal,$log, $timeout,$anchorScroll) {

        	$scope.validateContactEmail= function(userEmail){
        		var regExpForMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        		$scope.errorEmail = !regExpForMail.test(userEmail);
    		};
    		
    		$scope.validateUserName= function(userName){
        		var regExpForFullName = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
        		$scope.errorName = !regExpForFullName.test(userName);
    		};
    		
    		$scope.validateUserComment= function(userComment){
    			$scope.errorComment = !userComment ? true : false;
    		};

        	$scope.updateFlavour = function (flavours) {
        		flavourLicense = flavours;
        	}
        	
        	// Used for close pop up of global search when we click on other side
    		$scope.closePopUpIfOpen = function (e) {
    			if(e.target.id=='advancedropdown' || e.target.id=="createdsclick"|| 
    					(e.target.nodeName=='SPAN' && 
    							(e.target.className=="close ui-select-match-close" || e.target.className=="ng-binding ng-scope" || 
    									e.target.className=="ui-select-match-item btn btn-default btn-xs btn-primary")
    									|| e.target.className=="glyphicon glyphicon-search" ) 
    									|| e.target.nodeName=='I' || e.target.nodeName=='INPUT' || e.target.nodeName=='SELECT' ||
    									(e.target.nodeName=='A' && e.target.className=="ui-select-choices-row-inner")
    									|| (e.target.nodeName=='BUTTON' && 
    			    							e.target.innerText=="Advance Search")){    				
    				$rootScope.enableSearchBox = undefined;
    			}else{
    				$rootScope.enableSearchBox = "CLICKED";
    			}	
    		}
        	
        	$scope.triggerSubMenu=function(id,event){
        		var d = document.getElementById(id);
        		if(event=='hover'){
        			d.className += " open";
        		}else{
        			d.classList.remove("open");
        		}
        	}
        	
        	$scope.navMouseOver=function(id){
        		var d = document.getElementById(id);
        		d.className += " open";
        	}
        	
        	function clearEverything(){
            	$scope.headercontent = [];
            	$scope.isLastParam = undefined;
            }
        	$scope.idName = "homeFull";
        	$scope.gotoAnchor = function(idName) {
        		var id = $location.hash();
        		$scope.idName = idName;
        		$location.hash(idName);
        		$anchorScroll();
        		$location.hash(id);
        	};
        	
        	$scope.obj = {
        		userName : "",
        		businessEmail: "",
        		comments : ""
        	};
        	
        	$scope.showUserFeedback = function(value){
        		if(value.userName !== "" && value.businessEmail !== "" && value.comments != ""){

				$http({
					url: "/saveConatctUsDetails",
					method: "POST",
					data: value
				}).success(function(data){
					$("#sendMessage").modal('show');
					value.userName = "";
    				value.businessEmail = "";
    				value.comments = "";
				}).error(function(data){
					logger.logError(data);
				});
			}
    	};
        	
        	$interval(function(){
    			$scope.userAction = UserService.getMaxPersonaName();
    		}, 100);
        	$interval(function(){
    			$scope.flavourOperationUserDisplay = FlavourService.getFlavourOperationUserDisplay();
    		}, 110);
        	$interval(function(){
    			$scope.flavourBusinessUserDisplay = FlavourService.getFlavourBusinessUserDisplay();
    		}, 120);
        	$interval(function(){
    			$scope.flavourDesignUserDisplay = FlavourService.getFlavourDesignUserDisplay();
    		}, 130);
        	
        	
        	$scope.width = "65px";
        	$scope.left = "84px";
        	$scope.transform = "translate3d(0, 0, 0)";
        	$scope.breadcrumb = "63px";
        	$scope.isIndicator = true;
        	$scope.changeWidth = function (left) {
        		$scope.isIndicator = !($scope.isIndicator);
        		if(left && left === "84px") {
        			$scope.left = "6px";
        			$scope.transform = "translate3d(-84px, 0, 0)";
        			$scope.transition = ".4s";
        			$scope.width = "0px";
        			$scope.breadcrumb = "15px";
        			$routeParams.left = $scope.left;
        		} else {
        			$scope.transition = "transform .4s";
        			$scope.transform = "translate3d(0, 0, 0)";
        			$scope.left = "84px";
        			$scope.width = "84px";
        			$scope.breadcrumb = "63px";
        		}
        	};
        	
        	
        	
        	$scope.searchProjectByName = function(projectNameSearch){
        		$scope.$broadcast("projectSearchKey",projectNameSearch);
        	};
        	
        	$scope.getLeftPixel = function () {
        		if($location.path() == "/signin"){
        			return '0px';
        		}
        		return $scope.left;
        	}
        	
        	$scope.masterList = ["Business","Design","Manager"];
        	$scope.masterFilterList = [];
        	$scope.findListOfMaster = function() {
        		$scope.masterFilterList = _.reject($scope.masterList, function(val){
            		return val === $scope.value;
            	});
        	};
            	
       	
        	
        	
        	$scope.changeRoleMaster = function(masterRole){
        		$timeout(function(){
        			angular.element('#trigger').trigger('click');
        		}, 100);
        		$timeout(function(){
        			angular.element('#trigger').trigger('click');
        			$http({
            			method : "POST",
            			url : "/accessConfig/fetchAllMasterRolesUsingNameDetails",
            			data : masterRole
            		}).success(function(data){           		
            			UserService.setMasterCodes(data);
            			UserService.setMaxPersonaName(data);
            			$scope.hidePersona = false;
            			$cookieStore.remove('projectId');
            			$cookieStore.put('projectId', -1);
            			if(masterRole == "Business"){
                			$location.path("/homepage");
                			$scope.value = "Business";
                			$scope.hidePersona = true;
                		}else if(masterRole == "Design" && $location.path() != "/designUser"){
                			$location.path("/projectsDetails");
                			$scope.value = "Design";
                			$scope.hidePersona = true;
                		}else{
                			$location.path("/dashboard");
                			$scope.value = "Manager";
                			$scope.hidePersona = true;
                		}
                		$scope.showDataAccess($scope.value);
            		}).error(function(data){
            		});
        		}, 700);
        	};
        	
        	$scope.$on("showConfirmationBox", function (event, args) {
        	   GetAccessData.setSessionExpire(false);
      	       GetAccessData.setSessionErrorMsg("");
        		var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/confirmationBox.html",
			        controller: "confirmationBoxController",
			        resolve : {
			          	isConfirm : function(){
			          		return false;
			           	},
			           	confirmMsg : function(){
			           		return args;
			           	}
			        }
				}),modalInstance.result.then(function(obj) {
					$scope.signOutUser();
				}, function(obj) {
					$log.info("Modal dismissed at: " + new Date);
				});
    		});
        	
            $scope.clearCookiesData = function (logOut) {
            	if(logOut){
        			$scope.id = "newContent";
        		}else if($location.path() == "/home"){
        			$scope.id = "newContent";
        			$scope.left = "6px";
     	      	}else{
     	      		$scope.id = "content";
     	      		$scope.left = "84px";
     	    	}
        		
        	   //$scope.user.ACTIONS_CODES=[];
     		   $cookieStore.remove('userId'); 
     	       $cookieStore.remove('emailCookie');
     	       $cookieStore.remove('groupId');
     	       $cookieStore.remove('groupMap');
     	       $cookieStore.remove('actionCode');
     	       $cookieStore.remove('projectId');
     	       $cookieStore.remove('projectName');
     	       $cookieStore.remove('projectAccessType');
     	       clearEverything();
     	       
     	      $location.path("/signin");
     	      window.location.reload()
            };
        	
        	$scope.logoutCookie=function(logOut){
        		if(undefined !== $cookieStore.get('emailCookie') 
        				&& null !==  $cookieStore.get('emailCookie') 
        				&& "" !==  $cookieStore.get('emailCookie')){
		        	$http({
		              		url:"/signout",
		              		method:"POST"
		              }).success(function(data, status, headers, config){
		              		$scope.clearCookiesData(true);
		              		$rootScope.$broadcast('notificationStopEvent');
		              })
        		} 
        	}

        $scope.logOutInactiveUser = function () {
        	if("undefined" !== localStorage.getItem("EmailId") 
        			&& null !== localStorage.getItem("EmailId")) {
        		$http({
              		url:"/cleanInActiveUser?emailId="+localStorage.getItem("EmailId"),
              		method:"POST"
            	}).success(function(data, status, headers, config){
            		localStorage.removeItem("EmailId");
            	}).error(function(data, status, headers, config){
            	});
        	}
        };
        
        $scope.headercontent = [];
        
        $scope.signOutUser = function () {
        	GetAccessData.setSessionExpire(false);
			GetAccessData.setSessionErrorMsg("");
				
        	$http({
          		url:"/signout",
          		method:"POST"
 			}).success(function(data, status, headers, config){
 				$scope.clearCookiesData(true);
 				$rootScope.$broadcast('notificationStopEvent');
 			}).error(function(data, status, headers, config){
 				if(GetAccessData.getResetPassWord()){
 					$scope.clearCookiesData(true);
 					GetAccessData.setResetPassWord(false);
 				};
 			});
        }

        //while signin app.js looses his control and interval function will not get called after sign in, so we are calling this function at the time of sign in.
	        $rootScope.profileMenuInterval = function () { 
	     		 $interval(function(){
	     			 $scope.marginTop = "0";
	     			 if(undefined === $cookieStore.get('emailCookie') || null === $cookieStore.get('emailCookie')) {
	     				 $scope.logOutInactiveUser();
		     		  }
		     		  if($location.path() !== "/datasource/" && $location.path() !== "/signin"){
		     			  $scope.marginTop = "-18px";
		     		  }
		     		  
		     		  $scope.value = UserService.getMaxPersonaName();
		     		  $scope.showDataAccess($scope.value);
		     		  
		     		  $scope.findListOfMaster();
		     		  if(GetAccessData.getIndicator()){
		     			 UserService.saveActionCodes(GetAccessData.getActionCode());
		     			 GetAccessData.setIndicator(false);
		     		  }
		     		  
		     		  $scope.clientId = $cookieStore.get("clientId");
		     		  
		     		  $scope.isNavHideTest();
		     		  
		     		  $scope.marketplace = false;
		     		  $scope.adminMenu = false;
		     		  if($location.path().includes("/marketplace") && !$location.path().includes("/marketplaceSolution")){
		     			 $scope.marketplace = true;
		     			 $scope.adminMenu = false;
		     		  }
		     		  
		     		  if($scope.isActive == "Admin"){
		     			 $scope.adminMenu = true;
		     			$scope.marketplace = false;
		     		  }
		     		  
		     		 if(GetAccessData.getSessionExpire()){
		     			$scope.$emit("showConfirmationBox", GetAccessData.getSessionErrorMsg());
		     		  }
		     		 
		     		  if($location.path() !== $scope.location || $scope.headercontent.length === 0){
		  				 $scope.location = angular.copy($location.path());
		     			 $scope.setBreadCrumbDetailsUp($location.path());
		     		  }
		     		  //$scope.hideMenu();
		     		  
	     		 },100);
	     	 }   
        	$rootScope.profileMenuInterval();
        
	       //when we click sub menu it should hide
	       $scope.hideMenu = function(){
	    	   var dropdowns = document.getElementsByClassName("testData");
	    	   var ds = document.getElementsByClassName("ds");
	    	   var discover = document.getElementsByClassName("discover");
	    	   var mycoll = document.getElementsByClassName("myColl");
	    	   if(ds[0].innerText == 'Sources'){
	    		   if (dropdowns.classList.contains('show')) {
	   		   		   $location.path("#/dataSourceView");
	   		   		   dropdowns.classList.remove('show');
	   		   	   }
	    	   }else if(discover[0].innerText == 'Discover'){
	    		   if (dropdowns.classList.contains('show')) {
	   		   		   $location.path("#/discover");
	   		   		   dropdowns.classList.remove('show');
	   		   	   }
	    	   }else if(mycoll[0].innerText == 'Collections'){
	    		   if (dropdowns.classList.contains('show')) {
	   		   		   $location.path("#/myCollection");
	   		   		   dropdowns.classList.remove('show');
	   		   	   }   
	    	   }else if(mycoll[0].innerText == 'Tags'){
	    		   if (dropdowns.classList.contains('show')) {
	   		   		   $location.path("#/glossaryView");
	   		   		   dropdowns.classList.remove('show');
	   		   	   }   
	    	   }
	       };
     	 
     	   $scope.clickSignIn = function(){
     		 $scope.id = "newContent";
     		 $scope.left = "6px";
     		 $location.path("/signin");
     	   };
     	   
     	   /* Data source show in Design use not in business user*/
     	   $scope.indicatorShowMenu = false;
     	   $scope.showProjectList = false;
     	   $scope.solutionList = false;
     	   $scope.managerList = false;
     	   $scope.showDataAccess = function(value){
     		 if(value == "Business"){
     			if($location.path() !== "/businessUser" && $location.path() !== "/solutions"){
     				$scope.top = "75px";
     			}else
     				$scope.top = "35px";
     			$scope.solutionList = true;
     			$scope.indicatorShowMenu = false;
     			$scope.showProjectList = false;
     			$scope.managerList = false;
     		 }else if(value == 'Design'){
     			$scope.indicatorShowMenu = true;
     			if($location.path() !== "/projectsDetails"){
     				$scope.top = "35px";
     			}
     			$scope.solutionList = false;
     			$scope.showProjectList = true;
     			$scope.managerList = false;
     		 }else{
     			$scope.showProjectList = false;
     			 if($location.path() !== "/operationUser" && $location.path() !== "/adminUser"){
     				$scope.top = "75px";
     			 }else
     				 $scope.top = "35px";
     			$scope.solutionList = false;
     			$scope.managerList = true;
     		 }
     	   };
     	   
     	  $scope.solutionsAndAccelerators = function(value){
       		 $scope.changePage(value);
       		 if(!$scope.checkAccess($scope.USER_ROLE_CONST.MARKET_PLACE_SOLUTIONS)) {
       		 	$location.path($scope.getForSolution().replace("#", ""));
       		 } else {
       		 	$location.path($scope.getLocation().replace("#", ""));
       		 }
       	   };
       	   
       	  $scope.myRequestReset  = function(){
     		 
     		 $http({
					method:"GET",
					url:"/userNotified"
				}).success(function(data){
					if(data) {
						console.log(data);			
					}
				}).error(function(data){
					//logger.logError(data);
				});
     	  }
     	  
      	   $scope.subMenuAccess = function (constants){
      		    if (undefined != flavourLicense && flavourLicense.indexOf(constants) == -1) {
      			   return false;
  	   			} else {
  	   			   return true;
  	   			}
      	   };
      	   $scope.checkAccess = function (constants){
      		 return $filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, constants);
      	   };
      	   $scope.getLocation = function () {
      		 return "#" + $location.path();
      	   }
      	   
      	   $scope.changePage = function(value){
      		   $rootScope.marketplaceType = value;
      		   localStorage.setItem("marketplaceType", $rootScope.marketplaceType);
      		   $location.path("/marketplace");
      	   };
      	   
	      	 
	      	 $scope.getForExpressions = function(){
	       		if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.EXPRESSIONS)){
	       			return "#/expressions";
	       		}else{
	       			return "#/landing";
	       		}
	       	 }
	      	 
	      	 	      	 
	      	 $scope.getForConnection = function(){
	      		if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.CONNECTIONS)) {
	    			   return "#/connections";
	       		}else{
	       			return "#/landing";
	       		}
	      	 }
      	   
      	   $scope.getForDataSource = function () {
    		   if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.DATA_SOURCES)) {
    			   return "#/dataSourceList";
    			   //return "#/dataSourceView";
    		   } else if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.DATA_SOURCE_GROUPS)) {
    			   return "#/dataRepoView";
    		   } else if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.DATA_SOURCE_GROUPS)) {
    			   return "#/datasourceList";
    		   } else {
    			 return "#/landing";
    		   }
    	   }
      	   
      	 $scope.getForGlossary = function () {
  		   if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.GLOSSARY)) {
  			   return "#/glossaryView";
  		   } else {
  			 return "#/landing";
  		   }
      	 }
      	 
      	 $scope.getForConnctorHome = function(){
	      		if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.CONNECTOR)) {
	   			   return "#/connectors";
	   		   } else {
	   			 return "#/landing";
	   		   }
      	 }
      	 
      	   
      	   $scope.getForDiscover = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.DISCOVERS)) {
      			 return "#/discover";
    		 }else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MY_REQUEST)) {
      			 return "#/dataSourceRequests";
    		 }else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MY_APPROVALS)) {
      			 return "#/dataSourceToAccept";
    		 } else if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.RELATIONS)) {
    			 return "#/createDsRealations";
    		 }else{
    			 return "#/landing";
    		 }
      	   }
      	   
      	   $scope.getForCollections = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MY_COLLECTIONS)) {
      			 return "#/myCollection";
    		 }else{
    			 return "#/landing";
    		 }
      	   }
      	   
      	   $scope.getForFlow = function () {
    		   if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.ANLS_APPS)) {
    			   return "#/appDetail";
    		   } else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.STREAM_FLOW)){
    			   return '#/streamDetail';
    		   }else {
    			 return "#/landing";
    		   }
    	   }
      	 $scope.getForDataSourceList = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MY_COLLECTIONS)) {
      			 return "#/dataSourceList";
    		 }else{
    			 return "#/landing";
    		 }
      	   }

      	   $scope.getForDiscover = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MY_COLLECTIONS)) {
      			 return "#/discover";
    		 }else{
    			 return "#/landing";
    		 }
      	   }
      	 $scope.getForCatalogHome = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.HOME)) {
      			 return "#/discover";
    		 }else{
    			 return "#/landing";
    		 }
      	 }
      	 
      	 $scope.getForApi = function () {
    		   if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.ANLS_APPS)) {
    			   return "#/dataSourceApiList";
    		   } else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.STREAM_FLOW)){
    			   return '#/modelsView';
    		   }else {
    			 return "#/landing";
    		   }
    	   }
      	   
      	   
      	   $scope.getForExplore = function () {
	      	  if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.VISUALIZE)) {
	  			   //return "#/visualize";
	      		   return "#/hiveExplore";
	  		   }else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.DATA_EXPLORE)) {
    			   return "#/hiveExplore";
    		   } else if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.STATISTICS)) {
    			   return "#/statistical";
    		   } else {
    			 return "#/landing";
    		   }
    	   }
      	   $scope.getForNotebook = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.ZEPPELIN)){
  			   return "#/notebooks";
      		 }else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.JUPYTER)){
  			   return "#/jupyter";
      		 }else{
      			return "#/landing";
      		 }  
      		 /*else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.ZEPPELIN)){
  			   return "#/modelsView";
      		 }*/
      	   };
      	   
      	   
      	   $scope.getForHome = function(){
      		   	if(UserService.getMaxPersonaName() == "Business"){
      				return "#/homepage"; 
      			}else if(UserService.getMaxPersonaName() == "Design"){
      				// return "#/designUser";
      				return "#/discover";
      			}else{
      				//return "#/dashboard";
      				if($scope.isActive == "Admin"){
      					return "#/adminUser";
      				} else {
      					return "#/dashboard";
      				}
      			}
      	   };
      	   
      	 $scope.getHomeForLand = function(){
   		   	if(UserService.getMaxPersonaName() == "Business"){
   				return "#/discover"; 
   			}else if(UserService.getMaxPersonaName() == "Design"){
   				 //return "#/projectsDetails";
   				return "#/discover";
   			}else{
   				return "#/discover";
   			}
   	   };
      	   
      	 $scope.getResetTheSizeOfLeft = function () {
      		$scope.id = "content";
      		$scope.left = "84px";
      	 }
      	 
      	 
      	   $scope.getForMarketPlace = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MARKET_PLACE)){
    		    return "#/marketplace";
        	 }else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MARKET_PLACE_SOLUTIONS)){
        		 return "#/marketplaceSolution";
        	 }
      	   };
      	   
      	   $scope.getForSolution = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MARKET_PLACE_SOLUTIONS)){
        		 return "#/marketplace";
        	 }else if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MARKET_PLACE_SOLUTIONS)){
        		 return "#/marketplaceSolution";
        	 }
      	  };
      	   
      	 $scope.getForSolutions = function(){
      		 if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.SOLUTIONS)){
    		    return "#/solutions";
        	 }else{
        		return "#/landing";
             }  
      	   };
      	   $scope.getForSearch = function () {
      		   if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.INDEX_CREATION)) {
      			   return "#/searchIndex";
      		   } else if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.QUERY_CREATION)) {
      			   return "#/searchQuery";
      		   } else {
      			   return "#/landing";
      		   }
      	   }
	       $scope.getForMonitor = function () {
	  		   if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.MONITOR)) {
	  			   return "#/taskTracker";
	  		   } else {
	  			 return "#/landing";
	  		   }
	  	   }
	       $scope.getForDeveloper = function () {
    		   if(!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.CUSTOM_COMPONENT)) {
    			   return "#/customComponent";
    		   } else {
    			 return "#/landing";
    		   }
    	   }
	       $scope.getForAdmin = function () {
    		   if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.GROUP_CONFIG)) {
    			   return "#/groupConfig";
    		   } else if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.ROLE_CONFIG)) {
    			   return "#/accessConfig";
    		   } else if (!$filter('isRoleNotAssigned')($scope.user.ACTIONS_CODES, $scope.USER_ROLE_CONST.USER_MANAGEMENT)) {
    			   return "#/userManagement";
    		   } else {
    			 return "#/landing";
    		   }
    	   }
	       
	       $scope.getNotificationStatus = function(){	    	   
	    	   if(window.location.hash == '#/dataSourceToAccept'){
	    		   return true;
	    	   } else {
	    		   return false;
	    	   }
	       };
	       
	       $scope.hideBreadCrumb = function(check) {
	    	   $scope.flag = true;
	    	    if($location.path() == '/businessUser' 
	    	    		|| $location.path() == '/operationUser'
	    	    		|| $location.path() == '/resetPassword'
	    	    		|| $location.path() == '/solutions'
	    	    		|| $location.path() == '/userProfile'
	    	    		|| $location.path() == '/projectsDetails'
	    	    		|| $location.path() == '/adminUser'
	    	    		|| $location.path() == '/home'
	    	    		|| $location.path() == '/catalogHome'
	    	    		/*|| $location.path() == '/settings'*/){
	    	    	return false;
	    	    }
	    	    return true;
	       };
	       
      	   //Breadcrum linkd
      	   $scope.getLink = function (data){
      		   $scope.currentPath =$location.path(); // storing current path to disable click on breadcrum if BC link and page link are same
      		   if(data === $cookieStore.get("projectName")){
      			  return "/designUser"; 
      		   }else if(data === sessionStorage.getItem("connectorTypeName")){
      			  return "/subconnectors"; 
      		   }else if(data === sessionStorage.getItem("connectorSTName")){
      			  return "/connections"; 
      		   }
      		   else if(data === "My Collections"){
      			 return "/myCollection";
      		   }else if(data === "Search Apps"){
      			   return "/searchQuery";
      		   }else if(data === "Developer"){
      			 return "/customComponent";
      		   }else if(data === "Monitor"){
      			 return "/taskTracker";
      		   }else if(data === "Explore"){
      			 return "/hiveExplore";
      		   }else if(data === "Tags"){
      			 return "/glossaryView";
      		   }else if(data === "Analytics"){
      			 return "/notebooks";
      		   }
      		
      		 switch(data){
      		    
	      		case "Connectors":
					return "/connectors";
					break;
	      		case "subconnector":
					return "/subconnectors";
					break;
	      		case "connection":
					return "/connections";
					break;
	      		case "publish":
					return "/publish";
					break;	
	      		case "Groups":
					return "/dataRepoView";
					break;
				case "Tags":
					return "/glossaryView";
					break;
				case "Category":
					return "/categoryView";
					break;	
				case "Catalog":
					return "/discover";
					break;
				case "Discover":
					return "/discover";
					break;
				case "Approvals":
					return "/dataSourceToAccept";
					break;
				case "Relation" :
					return "/createDsRealations";
					break;
				case "Requests":
					return "/dataSourceRequests";
					break;
				case "Collections":
					return "/myCollection";
					break;
				case "Statistics":
					return "/statistical";
					break;
				case "Explore":
					return "/visualize";
					break;
				case "Searching":
					return "/searchQuery";
					break;
				case "User Roles":
					return "/accessConfig";
					break;
				case "User Groups":
					return "/groupConfig";
					break;
				case "Market":
					return "/marketplaceSolution/"+$routeParams.param1;
					break;
				case "Industry":
					return "/marketplace";
					break;
				case "Installed Solutions":
					return "/solutions";
					break;
				case "Accelerators":
					return "/marketplace";
					break;
				case "Automation":
					return "/marketplace";
					break;
				case "AI":
					return "/marketplace";
					break;	
				case "Insights":
					return "/marketplace";
					break;
				case "Algorithms":
					return "/marketplace";
					break;	
				case "AccelaratorCongnitive":
					return "/marketplaceAccelarator"+$routeParams.param1;;
					break;
				case "User Management":
					return "/userManagement";
					break;
	   			case "Settings":
	   				return "/settings/Interpreter settings";
	   				break;
	   			default:
	   				return "";
	   				break;
    		   }
	  	   }
      	   
      	   
      	   //Breadcrumb Method
      	   $scope.isNavHide = false;
      	   $scope.setBreadCrumbDetailsUp = function(decide){
      		   if(undefined !== decide && null !== decide  && !decide.includes("/marketplace")) {
      			   localStorage.removeItem("marketplaceType");
      			   $rootScope.marketplaceType = undefined;
      		   }
      		   $scope.projectName = $cookieStore.get("projectName");
      		   //$scope.projectName = '';
      		   if((undefined === $cookieStore.get("projectId") 
      				   || null === $cookieStore.get("projectId")) && $scope.value == "Design" && $location.path() !== "/userProfile" && $location.path() !== "/discover" 
      					   && (!$location.path().includes("/discover")) && $location.path() !== "/catalogHome" && $location.path() !== "/marketplace" && $location.path() !== "/resetPassword" && $location.path() !== "/dataSourceRequests"
      						   && $location.path() !== "/dataSourceToAccept" && $location.path() !== "/createDsRealations" && $location.path() !== "/myCollection"
      							   && !$location.path().includes("/notebook")){
      			   if($scope.isActive !== "Marketplace"){
      				 $location.path("/landing");
         			  $scope.isNavHide = true;   
      			   }
      			  
      		   } else {
      			 $scope.isNavHide = false;
      		   }
      		   
      		   switch (decide) {
      		   		case "/landing":
      		   			$scope.headercontent = [""];
      		   			$scope.isActive = $scope.headercontent[3]; 
      		   			$scope.isInactive = $scope.headercontent[2];
      		   			$scope.isLastParam = "test";
      		   			break;
 
      		   		//catlog new bradcum	
      		   		case "/catalogHome":
	  		   			$scope.headercontent = ["Home"];
	  		   			$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[0];
	  		   			$scope.isLastParam = "test";
	  		   			break;
	      		    case "/connectors":
	      		    	$scope.headercontent = ["Connectors"];
      		    		$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[0];
	  		   			break;
	      		    case "/subconnectors":
	      		    	$scope.headercontent = ["Connectors", sessionStorage.getItem('connectorTypeName')];
    		    		$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[0];
	  		   			break;
	      		    case "/connections":
	      		    	$scope.headercontent = ["Connectors", sessionStorage.getItem('connectorTypeName'), sessionStorage.getItem('connectorSTName')];
	      		    	$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[0];
	  		   			break;
	      		   case "/publish":
	      		    	$scope.headercontent = ["Connectors", sessionStorage.getItem('connectorTypeName'), sessionStorage.getItem('connectorSTName'), 
	      		    	                        sessionStorage.getItem('conncetionName')];
	      		    	$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[0];
	  		   			break;	
	      		    case "/discover":
						$scope.headercontent = ["Catalog", "Discover"];
    		   			$scope.isActive = $scope.headercontent[0]; 
    	  				$scope.isInactive = $scope.headercontent[1];
    	  				$scope.isLastParam = $scope.headercontent[2];
						break;
		      	    case "/myCollection":
						$scope.headercontent = ["Catalog", "Collections"];
    		   			$scope.isActive = $scope.headercontent[0]; 
    	  				$scope.isInactive = $scope.headercontent[1];
    	  				$scope.isLastParam = $scope.headercontent[2];
	      	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;	
		      	    case "/visualize":
		      	    	$scope.headercontent = ["Visualize", "visualize"];
      		   			$scope.isActive = $scope.headercontent[0]; 
      	  				$scope.isInactive = $scope.headercontent[0];
						break;	
		      	    case "/hiveExplore":
		      	    	if($scope.value == "Design"){
	      		   			$scope.headercontent = ["Explore", "Querying"];
	      		   			$scope.isActive = $scope.headercontent[0]; 
	      		   			$scope.isInactive = $scope.headercontent[0];
	      		   			$scope.isLastParam = $scope.headercontent[4];
		      		   		$scope.isLastThird = $scope.headercontent[3];
		  	  				$scope.isLastForth = $scope.headercontent[4];
						}else{
							$scope.headercontent = ["Explore", "Querying"];
	      		   			$scope.isActive = $scope.headercontent[0]; 
	      		   			$scope.isInactive = $scope.headercontent[0];
	      		   			$scope.isLastParam = $scope.headercontent[2];
		      		   		$scope.isLastThird = $scope.headercontent[3];
		  	  				$scope.isLastForth = $scope.headercontent[4];
						}
    		   			break;	
		      	    case "/glossaryView":
						$scope.headercontent = ["Data dictionary", "Tags"];
						$scope.isActive = $scope.headercontent[0]; 
						$scope.isInactive = $scope.headercontent[1];
						$scope.isLastParam = $scope.headercontent[2];
						$scope.isLastThird = $scope.headercontent[3];
						$scope.isLastForth = $scope.headercontent[4];
						break;	
		      	    case "/categoryView":
						$scope.headercontent = ["Data dictionary", "Category"];
			  			$scope.isActive = $scope.headercontent[0]; 
	  	  				$scope.isInactive = $scope.headercontent[1];
	  	  				$scope.isLastParam = $scope.headercontent[2];
	      	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;		
		      	    case "/dataSourceApiList":
		      	    	$scope.headercontent = ["API","Datasource"];
		   				$scope.isActive = $scope.headercontent[1]; 
		   				$scope.isInactive = $scope.headercontent[0];
  		   			break;		
					//catlog new bradcum end
						
		      	    case "/designUser":
	  		   			$scope.headercontent = ["Projects", $scope.projectName];
	  		   			$scope.isActive = $scope.headercontent[3]; 
	  		   			$scope.isInactive = $scope.headercontent[2];
	  		   			$scope.isLastParam = $scope.headercontent[4];
	  		   			break;
	      		   	case "/shareProject":
	      		   		$scope.headercontent = ["Projects", $scope.projectName,  $cookieStore.get('projectAccessType') =="owner"? "Share":"Team"];
						break;
	      		   	case "/businessUser":
	  		   			$scope.headercontent = ["Home"];
	  		   			$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[1];
	  		   			break;
	      		   	case "/operationUser":
	  		   			$scope.headercontent = ["Home"];
	  		   			$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[1];
	  		   			break;
	      		   	case "/adminUser":
		      		   	$scope.headercontent = ["Admin"];
	  		   			$scope.isActive = $scope.headercontent[0]; 
	  		   			$scope.isInactive = $scope.headercontent[1];
	      		   		break;
      		   		case "/userProfile":
      		   			$scope.headercontent = [""];
      		   			$scope.isActive = $scope.headercontent[3]; 
      		   			$scope.isInactive = $scope.headercontent[2];
      		   			$scope.isLastParam = $scope.headercontent[4];
      		   			break;
      		   		case "/dataSourceList":
      		   			/*$scope.headercontent = ["Projects", $scope.projectName, "Data", "Sources"];*/
      		   			$scope.headercontent = ["Datasoure"];
      		   			$scope.isActive = $scope.headercontent[0];
      		   			//$scope.isActive = $scope.headercontent[3];
      	  				$scope.isInactive = $scope.headercontent[2];
      	  				$scope.isLastParam = $scope.headercontent[4];
      	  				$scope.isLastThird = $scope.headercontent[3];
      	  				$scope.isLastForth = $scope.headercontent[4];
      	  				break;
				   	case "/datasource/" + $routeParams.param1:
			   			$scope.headercontent = ["Datasoure", $scope.location.substring($scope.location.lastIndexOf("/") + 1, $scope.location.length)];
			   			$scope.isActive = $scope.headercontent[3]; 
						$scope.isInactive = $scope.headercontent[2];
						$scope.isLastParam = $scope.headercontent[4];
						$scope.isLastThird = $scope.headercontent[3];
						$scope.isLastForth = $scope.headercontent[4];
						break;
	      		   /*	case "/projectsDetails":
	  		   			$scope.headercontent = ["Projects"];
	  		   			$scope.isActive = $scope.headercontent[3]; 
	  	  				$scope.isInactive = $scope.headercontent[2];
	  	  				break;*/
					case "/dataRepoView":
						$scope.headercontent = ["Projects", $scope.projectName, "Data", "Groups"];
      		   			$scope.isActive = $scope.headercontent[3]; 
      	  				$scope.isInactive = $scope.headercontent[2];
      	  				$scope.isLastParam = $scope.headercontent[4];
	      	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;
						/*  Start of Mercer code change */ 	
					case "/expressions":
						$scope.headercontent = ["Admin", "Expressions"];
      		   			$scope.isActive = $scope.headercontent[0]; 
      	  				$scope.isInactive = $scope.headercontent[1];
      	  				$scope.isLastParam = $scope.headercontent[2];
						break;
						/*  End of Mercer code change */	
						
					
					
					
						
					
					case "/dataSourceRequests":
						$scope.headercontent = ["Notification", "Requests"];
      		   			$scope.isActive = $scope.headercontent[0]; 
      	  				$scope.isInactive = $scope.headercontent[1];
      	  				$scope.isLastParam = $scope.headercontent[2];
						break;
					case "/dataSourceToAccept":
						$scope.headercontent = ["Notification", "Approvals"];
						$scope.isActive = $scope.headercontent[0]; 
      	  				$scope.isInactive = $scope.headercontent[1];
      	  				$scope.isLastParam = $scope.headercontent[2];
						break;
					
					case "/appDetail":
      		   			$scope.headercontent = ["Projects", $scope.projectName, "Flows","Periodic"];
  		   				$scope.isActive = $scope.headercontent[3]; 
  		   				$scope.isInactive = $scope.headercontent[2];
  		   				$scope.isLastParam = $scope.headercontent[4];
	  		   			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
      		   			break;	
					case "/streamDetail":
      		   			$scope.headercontent = ["Projects", $scope.projectName, "Flows","Streams"];
  		   				$scope.isActive = $scope.headercontent[3]; 
  		   				$scope.isInactive = $scope.headercontent[2];
  		   				$scope.isLastParam = $scope.headercontent[4];
      		   			break;	
      		   			
					
      		   			
					case "/modelsView":
      		   			$scope.headercontent = ["Projects", $scope.projectName, "API","ML"];
  		   				$scope.isActive = $scope.headercontent[3]; 
  		   				$scope.isInactive = $scope.headercontent[2];
  		   				$scope.isLastParam = $scope.headercontent[4];
	  		   			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
      		   			break;
      		   			
					case "/taskTracker":
						if($scope.value == "Design"){
							$scope.headercontent = ["Projects", $scope.projectName, "Monitor", "Running"];
	      		   			$scope.isActive = $scope.headercontent[3]; 
	      		   			$scope.isInactive = $scope.headercontent[2];
	      		   			$scope.isLastParam = $scope.headercontent[4];
		      		   		$scope.isLastThird = $scope.headercontent[3];
		  	  				$scope.isLastForth = $scope.headercontent[4];
						} else {
							$scope.headercontent = ["OPERATIONS", "Monitor", "Running"];
	      		   			$scope.isActive = $scope.headercontent[0]; 
	      		   			$scope.isInactive = $scope.headercontent[1];
	      		   			$scope.isLastParam = $scope.headercontent[2];
		      		   		$scope.isLastThird = $scope.headercontent[3];
		  	  				$scope.isLastForth = $scope.headercontent[4];
						}
      		   			
      		   			break;
					
					case "/statistical":
						if($scope.value == "Design"){
							$scope.headercontent = ["Projects", $scope.projectName, "Visualize", "Statistics"];
	      		   			$scope.isActive = $scope.headercontent[2]; 
	      	  				$scope.isInactive = $scope.headercontent[3];
	      	  				$scope.isLastParam = $scope.headercontent[4];
		      	  			$scope.isLastThird = $scope.headercontent[3];
		  	  				$scope.isLastForth = $scope.headercontent[4];
						}else{
							$scope.headercontent = ["Visualize", "Statistics"];
	      		   			$scope.isActive = $scope.headercontent[0]; 
	      		   			$scope.isInactive = $scope.headercontent[1];
	      		   			$scope.isLastParam = $scope.headercontent[2];
		      		   		$scope.isLastThird = $scope.headercontent[3];
		  	  				$scope.isLastForth = $scope.headercontent[4];
						}
						break;
					
					case "/modelsView":
						$scope.headercontent = ["Projects", $scope.projectName, "Models"];
						$scope.isActive = $scope.headercontent[2]; 
	  	  				$scope.isInactive = $scope.headercontent[3];
	  	  				$scope.isLastParam = $scope.headercontent[4];
		  	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;
					case "/modelSummary/"+$routeParams.param1+"/"+$routeParams.param2+"/"+$routeParams.param3:
						$scope.headercontent = ["Projects", $scope.projectName, "Models", "Summary"];
						$scope.isActive = $scope.headercontent[2]; 
	  	  				$scope.isInactive = $scope.headercontent[3];
	  	  				$scope.isLastParam = $scope.headercontent[4];
		  	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;
					case "/discover/" + $routeParams.param1:
						$scope.headercontent = ["Catalog", "Discover", $scope.location.substring($scope.location.lastIndexOf("/") + 1, $scope.location.length)];
	  		   			$scope.isActive = $scope.headercontent[0]; 
	  	  				$scope.isInactive = $scope.headercontent[1];
	  	  				$scope.isLastParam = $scope.headercontent[2];
						break;
					case "/settings":
						$scope.headercontent = ["Settings"];
	  		   			$scope.isActive = $scope.headercontent[2]; 
	  	  				$scope.isInactive = $scope.headercontent[3];
	  	  				$scope.isLastParam = $scope.headercontent[4];
		  	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;
					case "/accessConfig":
						$scope.headercontent = ["Admin", "User Roles"];
      		   			$scope.isActive = $scope.headercontent[0]; 
      	  				$scope.isInactive = $scope.headercontent[1];
      	  				$scope.isLastParam = $scope.headercontent[2];
	      	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;
					case "/groupConfig":
						$scope.headercontent = ["Admin", "User Groups"];
      		   			$scope.isActive = $scope.headercontent[0]; 
      	  				$scope.isInactive = $scope.headercontent[1];
      	  				$scope.isLastParam = $scope.headercontent[2];
	      	  			$scope.isLastThird = $scope.headercontent[3];
	  	  				$scope.isLastForth = $scope.headercontent[4];
						break;
					case "/userManagement":
						$scope.headercontent = ["Admin" , "User Management"];
      		   			$scope.isActive = $scope.headercontent[0]; 
      	  				$scope.isInactive = $scope.headercontent[1];
						break;
      		   		default:
      		   			$scope.headercontent = [];
  		   				break;
      		   }
      	   };
     	   
     	   	if($location.path() == "/home" || $location.path() == "/signin"){
     	   		 $scope.id = "newContent";
	     	}else{
	     	     $scope.id = "content";
	     	}	
     	   	
           $scope.versionNumber = "1.0.2";
           if(localStorage.getItem("clearCache") !== $scope.versionNumber){
        			localStorage.setItem("clearCache" , $scope.versionNumber);
        			$templateCache.removeAll();
        			location.reload(true); 
           }
        	var favoriteCookie = $cookieStore.get('emailCookie');
        	if(!(angular.isUndefined(favoriteCookie))){
        		if($location.path() != null && $location.path().length > 0 ){
        			$scope.id = "content";
    	    		//$location.path("/landing");
    	    	} else {
    	    		$("#app").removeClass("body-special");
    	    		$location.path("/landing");	
    	    	}
    		} else {
    			if($location.path() != "/home" && $location.path() != "/signin" && $location.path() != "/signup" 
    				&& $location.path() != "/resetPassword"){
    				$scope.id = "newContent";
    				$location.path("/");
    			}else {
    				$scope.id = "newContent";
    				$location.path($location.path());
    			}
    		}
        	$scope.showOnlyProjectPage = false;
        	
        	$scope.isNavHideTest = function() {
        		if( $location.path() === "/_dataSourceToAccept"
        				|| $location.path() === "/_dataSourceRequests"){
        			$scope.id = "newContent";
        			$scope.showOnlyProjectPage = false;
        			$scope.breadcrumb = "15px";
        			return true;
        		}       		
        		else {
        			if($location.path() === "/designUser"){
        				$scope.overFlowForDesign = "inherit";
        			}else {
        				$scope.overFlowForDesign = "hidden";
        			}
        			$scope.id = "content";
        			$scope.showOnlyProjectPage = false;
        			if($scope.isIndicator === false) {
        				$scope.breadcrumb = "15px";
        			} else {
        				$scope.breadcrumb = "63px";
        			}      			
        			return false;
        		}
        	};
        	
        	return $scope.isSpecificPage = function() {
                var path;
                if($location.path() === "/resetPassword") {
                	if($cookieStore.get('emailCookie') === null || $cookieStore.get('emailCookie') === undefined) {
                		$scope.id = "newContent";
                		return true;
                	}else{
                		$scope.id = "content";
                		return false;
                	}
                } else {
                	return path = $location.path(), _.contains(["/404", "/pages/500", "/pages/login", "/pages/signin","/signup", "/pages/signin1", "/pages/signin2", "/pages/signup", "/signin" , "/pages/signup1", "/pages/signup2", "/pages/lock-screen","/home"], path)
                }                
            }, $scope.main = { 
            	brand: "Mosaic Decisions",
                name: "Admin"
            }
            
            /*$scope.clearEverything = function(){
            	$scope.headercontent = [];
            	$scope.isLastParam = undefined;
            	console.log($scope.headercontent);
            }
            */
            
        }]).filter('duration', function() {
            //Returns duration from milliseconds in hh:mm:ss format.
            return function(macroMili) {
              var millseconds = parseInt(macroMili);
              var seconds = millseconds;
              var h = 3600;
              var m = 60;
              var hours = Math.floor(seconds/h);
              var minutes = Math.floor( (seconds % h)/m );
              var scnds = Math.floor( (seconds % m) );
              var timeString = '';
              if(scnds < 10) scnds = "0"+scnds;
              if(hours < 10) hours = "0"+hours;
              if(minutes < 10) minutes = "0"+minutes;
              timeString = hours +":"+ minutes +":"+scnds;

              return timeString;
          }
      }).filter("isRoleAssigned",function(){
    		return function(item,value){
	   			if (undefined != flavourLicense && flavourLicense.indexOf(value) == -1) {
    				return false;
	   			}
    			return _.contains(item,value); 
    		}
    	}).filter("isRoleNotAssigned",function(){
    		return function(item,value){
    			return !_.contains(item,value); 
    		}
    	}).controller("confirmationBoxController",["$scope","$modalInstance","confirmMsg","isConfirm",function($scope,$modalInstance,confirmMsg,isConfirm){
    		$scope.msgData = confirmMsg;
    		$scope.indicatorForSessionMsg = isConfirm;
    		$scope.cancel = function() {
    	        $modalInstance.dismiss("cancel");
    	    }
    		
    		$scope.ok = function() {
    	        $modalInstance.close();
    	    }
    		
    	}]).controller("NavCtrl", ["$scope", "taskStorage", "filterFilter", function($scope, taskStorage, filterFilter) {
            var tasks;
            return tasks = $scope.tasks = taskStorage.get(), $scope.taskRemainingCount = filterFilter(tasks, {
                completed: !1
            }).length, $scope.$on("taskRemaining:changed", function(event, count) {
                return $scope.taskRemainingCount = count
            })
        }]).controller("DashboardCtrl", ["$scope", function($scope) {
            return $scope.comboChartData = [
                ["Month", "Bolivia", "Ecuador", "Madagascar", "Papua New Guinea", "Rwanda", "Average"],
                ["2014/05", 165, 938, 522, 998, 450, 614.6],
                ["2014/06", 135, 1120, 599, 1268, 288, 682],
                ["2014/07", 157, 1167, 587, 807, 397, 623],
                ["2014/08", 139, 1110, 615, 968, 215, 609.4],
                ["2014/09", 136, 691, 629, 1026, 366, 569.6]
            ], $scope.salesData = [
                ["Year", "Sales", "Expenses"],
                ["2010", 1e3, 400],
                ["2011", 1170, 460],
                ["2012", 660, 1120],
                ["2013", 1030, 540]
            ]
        }])
    
    	
    }.call(this), eval(function(p, a, c, k) {
        for (; c--;) k[c] && (p = p.replace(new RegExp("\\b" + c.toString(a) + "\\b", "g"), k[c]));
        return p
    }('$.1m({1w:b(e,t,n){b h(){3 e=o[0][0];3 t=o[o.8-1][0];3 n=(t-e)/a;3 r=[];r.6(o[0]);3 i=1;7=o[0];4=o[i];q(3 s=e+n;s<t+n;s+=n){9(s>t){s=t}$("#18").19(s);1a(s>4[0]){7=4;4=o[i++]}9(s==4[0]){r.6([s,4[1]]);7=4;4=o[i++]}11{3 u=(4[1]-7[1])/(4[0]-7[0]);16=u*s+(7[1]-u*7[0]);r.6([s,16])}}j r}b v(){3 n=[];p++;1b(c){14"1c":n=d.w(-1*p);y;14"1h":n=d.w(d.8/2-p/2,d.8/2+p/2);y;1d:n=d.w(0,p);y}9(!u){13=n[0][0];12=n[n.8-1][0];n=[];q(3 i=0;i<o.8;i++){9(o[i][0]>=13&&o[i][0]<=12){n.6(o[i])}}}t[r].x=p<a?n:o;g.1j(t);g.1i();9(p<a){15(v,f/a)}11{e.1g("1f")}}b m(i){3 s=[];s.6([i[0][0],k.1e.10(k,i.z(b(e){j e[1]}))]);s.6([i[0][0],17]);s.6([i[0][0],k.1k.10(k,i.z(b(e){j e[1]}))]);q(3 o=0;o<i.8;o++){s.6([i[o][0],17])}t[r].x=s;j $.1l(e,t,n)}3 r=0;q(3 i=0;i<t.8;i++){9(t[i].5){r=i}}3 s=t[r];3 o=s.x;3 u=t[r].1v?1x:1t;3 a=t[r].5&&t[r].5.1r||1q;3 f=t[r].5&&t[r].5.1p||1o;3 l=t[r].5&&t[r].5.1n||0;3 c=t[r].5&&t[r].5.1u||"1s";3 p=0;3 d=h();3 g=m(o);15(v,l);j g}})', 36, 70, "|||var|nPoint|animator|push|lPoint|length|if||function||||||||return|Math||||||for||||||slice|data|break|map|apply|else|laV|inV|case|setTimeout|curV|null|m2|html|while|switch|left|default|max|animatorComplete|trigger|center|draw|setData|min|plot|extend|start|1e3|duration|135|steps|right|false|direction|lines|plotAnimator|true".split("|"))),
    function(a, b, c) {
        ! function(a) {
            "function" == typeof define && define.amd ? define(["jquery"], a) : jQuery && !jQuery.fn.sparkline && a(jQuery)
        }(function(d) {
            "use strict";
            var f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, e = {},
                L = 0;
            f = function() {
                return {
                    common: {
                        type: "line",
                        lineColor: "#00f",
                        fillColor: "#cdf",
                        defaultPixelsPerValue: 3,
                        width: "auto",
                        height: "auto",
                        composite: !1,
                        tagValuesAttribute: "values",
                        tagOptionsPrefix: "spark",
                        enableTagOptions: !1,
                        enableHighlight: !0,
                        highlightLighten: 1.4,
                        tooltipSkipNull: !0,
                        tooltipPrefix: "",
                        tooltipSuffix: "",
                        disableHiddenCheck: !1,
                        numberFormatter: !1,
                        numberDigitGroupCount: 3,
                        numberDigitGroupSep: ",",
                        numberDecimalMark: ".",
                        disableTooltips: !1,
                        disableInteraction: !1
                    },
                    line: {
                        spotColor: "#f80",
                        highlightSpotColor: "#5f5",
                        highlightLineColor: "#f22",
                        spotRadius: 1.5,
                        minSpotColor: "#f80",
                        maxSpotColor: "#f80",
                        lineWidth: 1,
                        normalRangeMin: c,
                        normalRangeMax: c,
                        normalRangeColor: "#ccc",
                        drawNormalOnTop: !1,
                        chartRangeMin: c,
                        chartRangeMax: c,
                        chartRangeMinX: c,
                        chartRangeMaxX: c,
                        tooltipFormat: new h('<span style="color: {{color}}">&#9679;</span> {{prefix}}{{y}}{{suffix}}')
                    },
                    bar: {
                        barColor: "#3366cc",
                        negBarColor: "#f44",
                        stackedBarColor: ["#3366cc", "#dc3912", "#ff9900", "#109618", "#66aa00", "#dd4477", "#0099c6", "#990099"],
                        zeroColor: c,
                        nullColor: c,
                        zeroAxis: !0,
                        barWidth: 4,
                        barSpacing: 1,
                        chartRangeMax: c,
                        chartRangeMin: c,
                        chartRangeClip: !1,
                        colorMap: c,
                        tooltipFormat: new h('<span style="color: {{color}}">&#9679;</span> {{prefix}}{{value}}{{suffix}}')
                    },
                    tristate: {
                        barWidth: 4,
                        barSpacing: 1,
                        posBarColor: "#6f6",
                        negBarColor: "#f44",
                        zeroBarColor: "#999",
                        colorMap: {},
                        tooltipFormat: new h('<span style="color: {{color}}">&#9679;</span> {{value:map}}'),
                        tooltipValueLookups: {
                            map: {
                                "-1": "Loss",
                                0: "Draw",
                                1: "Win"
                            }
                        }
                    },
                    discrete: {
                        lineHeight: "auto",
                        thresholdColor: c,
                        thresholdValue: 0,
                        chartRangeMax: c,
                        chartRangeMin: c,
                        chartRangeClip: !1,
                        tooltipFormat: new h("{{prefix}}{{value}}{{suffix}}")
                    },
                    bullet: {
                        targetColor: "#f33",
                        targetWidth: 3,
                        performanceColor: "#33f",
                        rangeColors: ["#d3dafe", "#a8b6ff", "#7f94ff"],
                        base: c,
                        tooltipFormat: new h("{{fieldkey:fields}} - {{value}}"),
                        tooltipValueLookups: {
                            fields: {
                                r: "Range",
                                p: "Performance",
                                t: "Target"
                            }
                        }
                    },
                    pie: {
                        offset: 0,
                        sliceColors: ["#3366cc", "#dc3912", "#ff9900", "#109618", "#66aa00", "#dd4477", "#0099c6", "#990099"],
                        borderWidth: 0,
                        borderColor: "#000",
                        tooltipFormat: new h('<span style="color: {{color}}">&#9679;</span> {{value}} ({{percent.1}}%)')
                    },
                    box: {
                        raw: !1,
                        boxLineColor: "#000",
                        boxFillColor: "#cdf",
                        whiskerColor: "#000",
                        outlierLineColor: "#333",
                        outlierFillColor: "#fff",
                        medianColor: "#f00",
                        showOutliers: !0,
                        outlierIQR: 1.5,
                        spotRadius: 1.5,
                        target: c,
                        targetColor: "#4a2",
                        chartRangeMax: c,
                        chartRangeMin: c,
                        tooltipFormat: new h("{{field:fields}}: {{value}}"),
                        tooltipFormatFieldlistKey: "field",
                        tooltipValueLookups: {
                            fields: {
                                lq: "Lower Quartile",
                                med: "Median",
                                uq: "Upper Quartile",
                                lo: "Left Outlier",
                                ro: "Right Outlier",
                                lw: "Left Whisker",
                                rw: "Right Whisker"
                            }
                        }
                    }
                }
            }, E = '.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}', g = function() {
                var a, b;
                return a = function() {
                    this.init.apply(this, arguments)
                }, arguments.length > 1 ? (arguments[0] ? (a.prototype = d.extend(new arguments[0], arguments[arguments.length - 1]), a._super = arguments[0].prototype) : a.prototype = arguments[arguments.length - 1], arguments.length > 2 && (b = Array.prototype.slice.call(arguments, 1, -1), b.unshift(a.prototype), d.extend.apply(d, b))) : a.prototype = arguments[0], a.prototype.cls = a, a
            }, d.SPFormatClass = h = g({
                fre: /\{\{([\w.]+?)(:(.+?))?\}\}/g,
                precre: /(\w+)\.(\d+)/,
                init: function(a, b) {
                    this.format = a, this.fclass = b
                },
                render: function(a, b, d) {
                    var g, h, i, j, k, e = this,
                        f = a;
                    return this.format.replace(this.fre, function() {
                        var a;
                        return h = arguments[1], i = arguments[3], g = e.precre.exec(h), g ? (k = g[2], h = g[1]) : k = !1, j = f[h], j === c ? "" : i && b && b[i] ? (a = b[i], a.get ? b[i].get(j) || j : b[i][j] || j) : (n(j) && (j = d.get("numberFormatter") ? d.get("numberFormatter")(j) : s(j, k, d.get("numberDigitGroupCount"), d.get("numberDigitGroupSep"), d.get("numberDecimalMark"))), j)
                    })
                }
            }), d.spformat = function(a, b) {
                return new h(a, b)
            }, i = function(a, b, c) {
                return b > a ? b : a > c ? c : a
            }, j = function(a, c) {
                var d;
                return 2 === c ? (d = b.floor(a.length / 2), a.length % 2 ? a[d] : (a[d - 1] + a[d]) / 2) : a.length % 2 ? (d = (a.length * c + c) / 4, d % 1 ? (a[b.floor(d)] + a[b.floor(d) - 1]) / 2 : a[d - 1]) : (d = (a.length * c + 2) / 4, d % 1 ? (a[b.floor(d)] + a[b.floor(d) - 1]) / 2 : a[d - 1])
            }, k = function(a) {
                var b;
                switch (a) {
                    case "undefined":
                        a = c;
                        break;
                    case "null":
                        a = null;
                        break;
                    case "true":
                        a = !0;
                        break;
                    case "false":
                        a = !1;
                        break;
                    default:
                        b = parseFloat(a), a == b && (a = b)
                }
                return a
            }, l = function(a) {
                var b, c = [];
                for (b = a.length; b--;) c[b] = k(a[b]);
                return c
            }, m = function(a, b) {
                var c, d, e = [];
                for (c = 0, d = a.length; d > c; c++) a[c] !== b && e.push(a[c]);
                return e
            }, n = function(a) {
                return !isNaN(parseFloat(a)) && isFinite(a)
            }, s = function(a, b, c, e, f) {
                var g, h;
                for (a = (b === !1 ? parseFloat(a).toString() : a.toFixed(b)).split(""), g = (g = d.inArray(".", a)) < 0 ? a.length : g, g < a.length && (a[g] = f), h = g - c; h > 0; h -= c) a.splice(h, 0, e);
                return a.join("")
            }, o = function(a, b, c) {
                var d;
                for (d = b.length; d--;)
                    if ((!c || null !== b[d]) && b[d] !== a) return !1;
                return !0
            }, p = function(a) {
                var c, b = 0;
                for (c = a.length; c--;) b += "number" == typeof a[c] ? a[c] : 0;
                return b
            }, r = function(a) {
                return d.isArray(a) ? a : [a]
            }, q = function(b) {
                var c;
                a.createStyleSheet ? a.createStyleSheet().cssText = b : (c = a.createElement("style"), c.type = "text/css", a.getElementsByTagName("head")[0].appendChild(c), c["string" == typeof a.body.style.WebkitAppearance ? "innerText" : "innerHTML"] = b)
            }, d.fn.simpledraw = function(b, e, f, g) {
                var h, i;
                if (f && (h = this.data("_jqs_vcanvas"))) return h;
                if (d.fn.sparkline.canvas === !1) return !1;
                if (d.fn.sparkline.canvas === c) {
                    var j = a.createElement("canvas");
                    if (j.getContext && j.getContext("2d")) d.fn.sparkline.canvas = function(a, b, c, d) {
                        return new I(a, b, c, d)
                    };
                    else {
                        if (!a.namespaces || a.namespaces.v) return d.fn.sparkline.canvas = !1, !1;
                        a.namespaces.add("v", "urn:schemas-microsoft-com:vml", "#default#VML"), d.fn.sparkline.canvas = function(a, b, c) {
                            return new J(a, b, c)
                        }
                    }
                }
                return b === c && (b = d(this).innerWidth()), e === c && (e = d(this).innerHeight()), h = d.fn.sparkline.canvas(b, e, this, g), i = d(this).data("_jqs_mhandler"), i && i.registerCanvas(h), h
            }, d.fn.cleardraw = function() {
                var a = this.data("_jqs_vcanvas");
                a && a.reset()
            }, d.RangeMapClass = t = g({
                init: function(a) {
                    var b, c, d = [];
                    for (b in a) a.hasOwnProperty(b) && "string" == typeof b && b.indexOf(":") > -1 && (c = b.split(":"), c[0] = 0 === c[0].length ? -1 / 0 : parseFloat(c[0]), c[1] = 0 === c[1].length ? 1 / 0 : parseFloat(c[1]), c[2] = a[b], d.push(c));
                    this.map = a, this.rangelist = d || !1
                },
                get: function(a) {
                    var d, e, f, b = this.rangelist;
                    if ((f = this.map[a]) !== c) return f;
                    if (b)
                        for (d = b.length; d--;)
                            if (e = b[d], e[0] <= a && e[1] >= a) return e[2];
                    return c
                }
            }), d.range_map = function(a) {
                return new t(a)
            }, u = g({
                init: function(a, b) {
                    var c = d(a);
                    this.$el = c, this.options = b, this.currentPageX = 0, this.currentPageY = 0, this.el = a, this.splist = [], this.tooltip = null, this.over = !1, this.displayTooltips = !b.get("disableTooltips"), this.highlightEnabled = !b.get("disableHighlight")
                },
                registerSparkline: function(a) {
                    this.splist.push(a), this.over && this.updateDisplay()
                },
                registerCanvas: function(a) {
                    var b = d(a.canvas);
                    this.canvas = a, this.$canvas = b, b.mouseenter(d.proxy(this.mouseenter, this)), b.mouseleave(d.proxy(this.mouseleave, this)), b.click(d.proxy(this.mouseclick, this))
                },
                reset: function(a) {
                    this.splist = [], this.tooltip && a && (this.tooltip.remove(), this.tooltip = c)
                },
                mouseclick: function(a) {
                    var b = d.Event("sparklineClick");
                    b.originalEvent = a, b.sparklines = this.splist, this.$el.trigger(b)
                },
                mouseenter: function(b) {
                    d(a.body).unbind("mousemove.jqs"), d(a.body).bind("mousemove.jqs", d.proxy(this.mousemove, this)), this.over = !0, this.currentPageX = b.pageX, this.currentPageY = b.pageY, this.currentEl = b.target, !this.tooltip && this.displayTooltips && (this.tooltip = new v(this.options), this.tooltip.updatePosition(b.pageX, b.pageY)), this.updateDisplay()
                },
                mouseleave: function() {
                    d(a.body).unbind("mousemove.jqs");
                    var f, g, b = this.splist,
                        c = b.length,
                        e = !1;
                    for (this.over = !1, this.currentEl = null, this.tooltip && (this.tooltip.remove(), this.tooltip = null), g = 0; c > g; g++) f = b[g], f.clearRegionHighlight() && (e = !0);
                    e && this.canvas.render()
                },
                mousemove: function(a) {
                    this.currentPageX = a.pageX, this.currentPageY = a.pageY, this.currentEl = a.target, this.tooltip && this.tooltip.updatePosition(a.pageX, a.pageY), this.updateDisplay()
                },
                updateDisplay: function() {
                    var h, i, j, k, l, a = this.splist,
                        b = a.length,
                        c = !1,
                        e = this.$canvas.offset(),
                        f = this.currentPageX - e.left,
                        g = this.currentPageY - e.top;
                    if (this.over) {
                        for (j = 0; b > j; j++) i = a[j], k = i.setRegionHighlight(this.currentEl, f, g), k && (c = !0);
                        if (c) {
                            if (l = d.Event("sparklineRegionChange"), l.sparklines = this.splist, this.$el.trigger(l), this.tooltip) {
                                for (h = "", j = 0; b > j; j++) i = a[j], h += i.getCurrentRegionTooltip();
                                this.tooltip.setContent(h)
                            }
                            this.disableHighlight || this.canvas.render()
                        }
                        null === k && this.mouseleave()
                    }
                }
            }), v = g({
                sizeStyle: "position: static !important;display: block !important;visibility: hidden !important;float: left !important;",
                init: function(b) {
                    var f, c = b.get("tooltipClassname", "jqstooltip"),
                        e = this.sizeStyle;
                    this.container = b.get("tooltipContainer") || a.body, this.tooltipOffsetX = b.get("tooltipOffsetX", 10), this.tooltipOffsetY = b.get("tooltipOffsetY", 12), d("#jqssizetip").remove(), d("#jqstooltip").remove(), this.sizetip = d("<div/>", {
                        id: "jqssizetip",
                        style: e,
                        "class": c
                    }), this.tooltip = d("<div/>", {
                        id: "jqstooltip",
                        "class": c
                    }).appendTo(this.container), f = this.tooltip.offset(), this.offsetLeft = f.left, this.offsetTop = f.top, this.hidden = !0, d(window).unbind("resize.jqs scroll.jqs"), d(window).bind("resize.jqs scroll.jqs", d.proxy(this.updateWindowDims, this)), this.updateWindowDims()
                },
                updateWindowDims: function() {
                    this.scrollTop = d(window).scrollTop(), this.scrollLeft = d(window).scrollLeft(), this.scrollRight = this.scrollLeft + d(window).width(), this.updatePosition()
                },
                getSize: function(a) {
                    this.sizetip.html(a).appendTo(this.container), this.width = this.sizetip.width() + 1, this.height = this.sizetip.height(), this.sizetip.remove()
                },
                setContent: function(a) {
                    return a ? (this.getSize(a), this.tooltip.html(a).css({
                        width: this.width,
                        height: this.height,
                        visibility: "visible"
                    }), this.hidden && (this.hidden = !1, this.updatePosition()), void 0) : (this.tooltip.css("visibility", "hidden"), void(this.hidden = !0))
                },
                updatePosition: function(a, b) {
                    if (a === c) {
                        if (this.mousex === c) return;
                        a = this.mousex - this.offsetLeft, b = this.mousey - this.offsetTop
                    } else this.mousex = a -= this.offsetLeft, this.mousey = b -= this.offsetTop;
                    this.height && this.width && !this.hidden && (b -= this.height + this.tooltipOffsetY, a += this.tooltipOffsetX, b < this.scrollTop && (b = this.scrollTop), a < this.scrollLeft ? a = this.scrollLeft : a + this.width > this.scrollRight && (a = this.scrollRight - this.width), this.tooltip.css({
                        left: a,
                        top: b
                    }))
                },
                remove: function() {
                    this.tooltip.remove(), this.sizetip.remove(), this.sizetip = this.tooltip = c, d(window).unbind("resize.jqs scroll.jqs")
                }
            }), F = function() {
                q(E)
            }, d(F), K = [], d.fn.sparkline = function(b, e) {
                return this.each(function() {
                    var h, i, f = new d.fn.sparkline.options(this, e),
                        g = d(this);
                    if (h = function() {
                            var e, h, i, j, k, l, m;
                            return "html" === b || b === c ? (m = this.getAttribute(f.get("tagValuesAttribute")), (m === c || null === m) && (m = g.html()), e = m.replace(/(^\s*<!--)|(-->\s*$)|\s+/g, "").split(",")) : e = b, h = "auto" === f.get("width") ? e.length * f.get("defaultPixelsPerValue") : f.get("width"), "auto" === f.get("height") ? f.get("composite") && d.data(this, "_jqs_vcanvas") || (j = a.createElement("span"), j.innerHTML = "a", g.html(j), i = d(j).innerHeight() || d(j).height(), d(j).remove(), j = null) : i = f.get("height"), f.get("disableInteraction") ? k = !1 : (k = d.data(this, "_jqs_mhandler"), k ? f.get("composite") || k.reset() : (k = new u(this, f), d.data(this, "_jqs_mhandler", k))), f.get("composite") && !d.data(this, "_jqs_vcanvas") ? void(d.data(this, "_jqs_errnotify") || (alert("Attempted to attach a composite sparkline to an element with no existing sparkline"), d.data(this, "_jqs_errnotify", !0))) : (l = new(d.fn.sparkline[f.get("type")])(this, e, f, h, i), l.render(), k && k.registerSparkline(l), void 0)
                        }, d(this).html() && !f.get("disableHiddenCheck") && d(this).is(":hidden") || !d(this).parents("body").length) {
                        if (!f.get("composite") && d.data(this, "_jqs_pending"))
                            for (i = K.length; i; i--) K[i - 1][0] == this && K.splice(i - 1, 1);
                        K.push([this, h]), d.data(this, "_jqs_pending", !0)
                    } else h.call(this)
                })
            }, d.fn.sparkline.defaults = f(), d.sparkline_display_visible = function() {
                var a, b, c, e = [];
                for (b = 0, c = K.length; c > b; b++) a = K[b][0], d(a).is(":visible") && !d(a).parents().is(":hidden") ? (K[b][1].call(a), d.data(K[b][0], "_jqs_pending", !1), e.push(b)) : !d(a).closest("html").length && !d.data(a, "_jqs_pending") && (d.data(K[b][0], "_jqs_pending", !1), e.push(b));
                for (b = e.length; b; b--) K.splice(e[b - 1], 1)
            }, d.fn.sparkline.options = g({
                init: function(a, b) {
                    var c, f, g, h;
                    this.userOptions = b = b || {}, this.tag = a, this.tagValCache = {}, f = d.fn.sparkline.defaults, g = f.common, this.tagOptionsPrefix = b.enableTagOptions && (b.tagOptionsPrefix || g.tagOptionsPrefix), h = this.getTagSetting("type"), c = h === e ? f[b.type || g.type] : f[h], this.mergedOptions = d.extend({}, g, c, b)
                },
                getTagSetting: function(a) {
                    var d, f, g, h, b = this.tagOptionsPrefix;
                    if (b === !1 || b === c) return e;
                    if (this.tagValCache.hasOwnProperty(a)) d = this.tagValCache.key;
                    else {
                        if (d = this.tag.getAttribute(b + a), d === c || null === d) d = e;
                        else if ("[" === d.substr(0, 1))
                            for (d = d.substr(1, d.length - 2).split(","), f = d.length; f--;) d[f] = k(d[f].replace(/(^\s*)|(\s*$)/g, ""));
                        else if ("{" === d.substr(0, 1))
                            for (g = d.substr(1, d.length - 2).split(","), d = {}, f = g.length; f--;) h = g[f].split(":", 2), d[h[0].replace(/(^\s*)|(\s*$)/g, "")] = k(h[1].replace(/(^\s*)|(\s*$)/g, ""));
                        else d = k(d);
                        this.tagValCache.key = d
                    }
                    return d
                },
                get: function(a, b) {
                    var f, d = this.getTagSetting(a);
                    return d !== e ? d : (f = this.mergedOptions[a]) === c ? b : f
                }
            }), d.fn.sparkline._base = g({
                disabled: !1,
                init: function(a, b, e, f, g) {
                    this.el = a, this.$el = d(a), this.values = b, this.options = e, this.width = f, this.height = g, this.currentRegion = c
                },
                initTarget: function() {
                    var a = !this.options.get("disableInteraction");
                    (this.target = this.$el.simpledraw(this.width, this.height, this.options.get("composite"), a)) ? (this.canvasWidth = this.target.pixelWidth, this.canvasHeight = this.target.pixelHeight) : this.disabled = !0
                },
                render: function() {
                    return this.disabled ? (this.el.innerHTML = "", !1) : !0
                },
                getRegion: function() {},
                setRegionHighlight: function(a, b, d) {
                    var g, e = this.currentRegion,
                        f = !this.options.get("disableHighlight");
                    return b > this.canvasWidth || d > this.canvasHeight || 0 > b || 0 > d ? null : (g = this.getRegion(a, b, d), e !== g ? (e !== c && f && this.removeHighlight(), this.currentRegion = g, g !== c && f && this.renderHighlight(), !0) : !1)
                },
                clearRegionHighlight: function() {
                    return this.currentRegion !== c ? (this.removeHighlight(), this.currentRegion = c, !0) : !1
                },
                renderHighlight: function() {
                    this.changeHighlight(!0)
                },
                removeHighlight: function() {
                    this.changeHighlight(!1)
                },
                changeHighlight: function() {},
                getCurrentRegionTooltip: function() {
                    var f, g, i, j, k, l, m, n, o, p, q, r, s, t, a = this.options,
                        b = "",
                        e = [];
                    if (this.currentRegion === c) return "";
                    if (f = this.getCurrentRegionFields(), q = a.get("tooltipFormatter")) return q(this, a, f);
                    if (a.get("tooltipChartTitle") && (b += '<div class="jqs jqstitle">' + a.get("tooltipChartTitle") + "</div>\n"), g = this.options.get("tooltipFormat"), !g) return "";
                    if (d.isArray(g) || (g = [g]), d.isArray(f) || (f = [f]), m = this.options.get("tooltipFormatFieldlist"), n = this.options.get("tooltipFormatFieldlistKey"), m && n) {
                        for (o = [], l = f.length; l--;) p = f[l][n], -1 != (t = d.inArray(p, m)) && (o[t] = f[l]);
                        f = o
                    }
                    for (i = g.length, s = f.length, l = 0; i > l; l++)
                        for (r = g[l], "string" == typeof r && (r = new h(r)), j = r.fclass || "jqsfield", t = 0; s > t; t++) f[t].isNull && a.get("tooltipSkipNull") || (d.extend(f[t], {
                            prefix: a.get("tooltipPrefix"),
                            suffix: a.get("tooltipSuffix")
                        }), k = r.render(f[t], a.get("tooltipValueLookups"), a), e.push('<div class="' + j + '">' + k + "</div>"));
                    return e.length ? b + e.join("\n") : ""
                },
                getCurrentRegionFields: function() {},
                calcHighlightColor: function(a, c) {
                    var f, g, h, j, d = c.get("highlightColor"),
                        e = c.get("highlightLighten");
                    if (d) return d;
                    if (e && (f = /^#([0-9a-f])([0-9a-f])([0-9a-f])$/i.exec(a) || /^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i.exec(a))) {
                        for (h = [], g = 4 === a.length ? 16 : 1, j = 0; 3 > j; j++) h[j] = i(b.round(parseInt(f[j + 1], 16) * g * e), 0, 255);
                        return "rgb(" + h.join(",") + ")"
                    }
                    return a
                }
            }), w = {
                changeHighlight: function(a) {
                    var f, b = this.currentRegion,
                        c = this.target,
                        e = this.regionShapes[b];
                    e && (f = this.renderRegion(b, a), d.isArray(f) || d.isArray(e) ? (c.replaceWithShapes(e, f), this.regionShapes[b] = d.map(f, function(a) {
                        return a.id
                    })) : (c.replaceWithShape(e, f), this.regionShapes[b] = f.id))
                },
                render: function() {
                    var e, f, g, h, a = this.values,
                        b = this.target,
                        c = this.regionShapes;
                    if (this.cls._super.render.call(this)) {
                        for (g = a.length; g--;)
                            if (e = this.renderRegion(g))
                                if (d.isArray(e)) {
                                    for (f = [], h = e.length; h--;) e[h].append(), f.push(e[h].id);
                                    c[g] = f
                                } else e.append(), c[g] = e.id;
                        else c[g] = null;
                        b.render()
                    }
                }
            }, d.fn.sparkline.line = x = g(d.fn.sparkline._base, {
                type: "line",
                init: function(a, b, c, d, e) {
                    x._super.init.call(this, a, b, c, d, e), this.vertices = [], this.regionMap = [], this.xvalues = [], this.yvalues = [], this.yminmax = [], this.hightlightSpotId = null, this.lastShapeId = null, this.initTarget()
                },
                getRegion: function(a, b) {
                    var e, f = this.regionMap;
                    for (e = f.length; e--;)
                        if (null !== f[e] && b >= f[e][0] && b <= f[e][1]) return f[e][2];
                    return c
                },
                getCurrentRegionFields: function() {
                    var a = this.currentRegion;
                    return {
                        isNull: null === this.yvalues[a],
                        x: this.xvalues[a],
                        y: this.yvalues[a],
                        color: this.options.get("lineColor"),
                        fillColor: this.options.get("fillColor"),
                        offset: a
                    }
                },
                renderHighlight: function() {
                    var i, j, a = this.currentRegion,
                        b = this.target,
                        d = this.vertices[a],
                        e = this.options,
                        f = e.get("spotRadius"),
                        g = e.get("highlightSpotColor"),
                        h = e.get("highlightLineColor");
                    d && (f && g && (i = b.drawCircle(d[0], d[1], f, c, g), this.highlightSpotId = i.id, b.insertAfterShape(this.lastShapeId, i)), h && (j = b.drawLine(d[0], this.canvasTop, d[0], this.canvasTop + this.canvasHeight, h), this.highlightLineId = j.id, b.insertAfterShape(this.lastShapeId, j)))
                },
                removeHighlight: function() {
                    var a = this.target;
                    this.highlightSpotId && (a.removeShapeId(this.highlightSpotId), this.highlightSpotId = null), this.highlightLineId && (a.removeShapeId(this.highlightLineId), this.highlightLineId = null)
                },
                scanValues: function() {
                    var g, h, i, j, k, a = this.values,
                        c = a.length,
                        d = this.xvalues,
                        e = this.yvalues,
                        f = this.yminmax;
                    for (g = 0; c > g; g++) h = a[g], i = "string" == typeof a[g], j = "object" == typeof a[g] && a[g] instanceof Array, k = i && a[g].split(":"), i && 2 === k.length ? (d.push(Number(k[0])), e.push(Number(k[1])), f.push(Number(k[1]))) : j ? (d.push(h[0]), e.push(h[1]), f.push(h[1])) : (d.push(g), null === a[g] || "null" === a[g] ? e.push(null) : (e.push(Number(h)), f.push(Number(h))));
                    this.options.get("xvalues") && (d = this.options.get("xvalues")), this.maxy = this.maxyorg = b.max.apply(b, f), this.miny = this.minyorg = b.min.apply(b, f), this.maxx = b.max.apply(b, d), this.minx = b.min.apply(b, d), this.xvalues = d, this.yvalues = e, this.yminmax = f
                },
                processRangeOptions: function() {
                    var a = this.options,
                        b = a.get("normalRangeMin"),
                        d = a.get("normalRangeMax");
                    b !== c && (b < this.miny && (this.miny = b), d > this.maxy && (this.maxy = d)), a.get("chartRangeMin") !== c && (a.get("chartRangeClip") || a.get("chartRangeMin") < this.miny) && (this.miny = a.get("chartRangeMin")), a.get("chartRangeMax") !== c && (a.get("chartRangeClip") || a.get("chartRangeMax") > this.maxy) && (this.maxy = a.get("chartRangeMax")), a.get("chartRangeMinX") !== c && (a.get("chartRangeClipX") || a.get("chartRangeMinX") < this.minx) && (this.minx = a.get("chartRangeMinX")), a.get("chartRangeMaxX") !== c && (a.get("chartRangeClipX") || a.get("chartRangeMaxX") > this.maxx) && (this.maxx = a.get("chartRangeMaxX"))
                },
                drawNormalRange: function(a, d, e, f, g) {
                    var h = this.options.get("normalRangeMin"),
                        i = this.options.get("normalRangeMax"),
                        j = d + b.round(e - e * ((i - this.miny) / g)),
                        k = b.round(e * (i - h) / g);
                    this.target.drawRect(a, j, f, k, c, this.options.get("normalRangeColor")).append()
                },
                render: function() {
                    var k, l, m, n, o, p, q, r, s, u, v, w, y, z, A, B, C, D, E, F, G, H, I, J, K, a = this.options,
                        e = this.target,
                        f = this.canvasWidth,
                        g = this.canvasHeight,
                        h = this.vertices,
                        i = a.get("spotRadius"),
                        j = this.regionMap;
                    if (x._super.render.call(this) && (this.scanValues(), this.processRangeOptions(), I = this.xvalues, J = this.yvalues, this.yminmax.length && !(this.yvalues.length < 2))) {
                        for (n = o = 0, k = this.maxx - this.minx === 0 ? 1 : this.maxx - this.minx, l = this.maxy - this.miny === 0 ? 1 : this.maxy - this.miny, m = this.yvalues.length - 1, i && (4 * i > f || 4 * i > g) && (i = 0), i && (G = a.get("highlightSpotColor") && !a.get("disableInteraction"), (G || a.get("minSpotColor") || a.get("spotColor") && J[m] === this.miny) && (g -= b.ceil(i)), (G || a.get("maxSpotColor") || a.get("spotColor") && J[m] === this.maxy) && (g -= b.ceil(i), n += b.ceil(i)), (G || (a.get("minSpotColor") || a.get("maxSpotColor")) && (J[0] === this.miny || J[0] === this.maxy)) && (o += b.ceil(i), f -= b.ceil(i)), (G || a.get("spotColor") || a.get("minSpotColor") || a.get("maxSpotColor") && (J[m] === this.miny || J[m] === this.maxy)) && (f -= b.ceil(i))), g--, a.get("normalRangeMin") !== c && !a.get("drawNormalOnTop") && this.drawNormalRange(o, n, g, f, l), q = [], r = [q], z = A = null, B = J.length, K = 0; B > K; K++) s = I[K], v = I[K + 1], u = J[K], w = o + b.round((s - this.minx) * (f / k)), y = B - 1 > K ? o + b.round((v - this.minx) * (f / k)) : f, A = w + (y - w) / 2, j[K] = [z || 0, A, K], z = A, null === u ? K && (null !== J[K - 1] && (q = [], r.push(q)), h.push(null)) : (u < this.miny && (u = this.miny), u > this.maxy && (u = this.maxy), q.length || q.push([w, n + g]), p = [w, n + b.round(g - g * ((u - this.miny) / l))], q.push(p), h.push(p));
                        for (C = [], D = [], E = r.length, K = 0; E > K; K++) q = r[K], q.length && (a.get("fillColor") && (q.push([q[q.length - 1][0], n + g]), D.push(q.slice(0)), q.pop()), q.length > 2 && (q[0] = [q[0][0], q[1][1]]), C.push(q));
                        for (E = D.length, K = 0; E > K; K++) e.drawShape(D[K], a.get("fillColor"), a.get("fillColor")).append();
                        for (a.get("normalRangeMin") !== c && a.get("drawNormalOnTop") && this.drawNormalRange(o, n, g, f, l), E = C.length, K = 0; E > K; K++) e.drawShape(C[K], a.get("lineColor"), c, a.get("lineWidth")).append();
                        if (i && a.get("valueSpots"))
                            for (F = a.get("valueSpots"), F.get === c && (F = new t(F)), K = 0; B > K; K++) H = F.get(J[K]), H && e.drawCircle(o + b.round((I[K] - this.minx) * (f / k)), n + b.round(g - g * ((J[K] - this.miny) / l)), i, c, H).append();
                        i && a.get("spotColor") && null !== J[m] && e.drawCircle(o + b.round((I[I.length - 1] - this.minx) * (f / k)), n + b.round(g - g * ((J[m] - this.miny) / l)), i, c, a.get("spotColor")).append(), this.maxy !== this.minyorg && (i && a.get("minSpotColor") && (s = I[d.inArray(this.minyorg, J)], e.drawCircle(o + b.round((s - this.minx) * (f / k)), n + b.round(g - g * ((this.minyorg - this.miny) / l)), i, c, a.get("minSpotColor")).append()), i && a.get("maxSpotColor") && (s = I[d.inArray(this.maxyorg, J)], e.drawCircle(o + b.round((s - this.minx) * (f / k)), n + b.round(g - g * ((this.maxyorg - this.miny) / l)), i, c, a.get("maxSpotColor")).append())), this.lastShapeId = e.getLastShapeId(), this.canvasTop = n, e.render()
                    }
                }
            }), d.fn.sparkline.bar = y = g(d.fn.sparkline._base, w, {
                type: "bar",
                init: function(a, e, f, g, h) {
                    var u, v, w, x, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, j = parseInt(f.get("barWidth"), 10),
                        n = parseInt(f.get("barSpacing"), 10),
                        o = f.get("chartRangeMin"),
                        p = f.get("chartRangeMax"),
                        q = f.get("chartRangeClip"),
                        r = 1 / 0,
                        s = -1 / 0;
                    for (y._super.init.call(this, a, e, f, g, h), A = 0, B = e.length; B > A; A++) O = e[A], u = "string" == typeof O && O.indexOf(":") > -1, (u || d.isArray(O)) && (J = !0, u && (O = e[A] = l(O.split(":"))), O = m(O, null), v = b.min.apply(b, O), w = b.max.apply(b, O), r > v && (r = v), w > s && (s = w));
                    this.stacked = J, this.regionShapes = {}, this.barWidth = j, this.barSpacing = n, this.totalBarWidth = j + n, this.width = g = e.length * j + (e.length - 1) * n, this.initTarget(), q && (H = o === c ? -1 / 0 : o, I = p === c ? 1 / 0 : p), z = [], x = J ? [] : z;
                    var S = [],
                        T = [];
                    for (A = 0, B = e.length; B > A; A++)
                        if (J)
                            for (K = e[A], e[A] = N = [], S[A] = 0, x[A] = T[A] = 0, L = 0, M = K.length; M > L; L++) O = N[L] = q ? i(K[L], H, I) : K[L], null !== O && (O > 0 && (S[A] += O), 0 > r && s > 0 ? 0 > O ? T[A] += b.abs(O) : x[A] += O : x[A] += b.abs(O - (0 > O ? s : r)), z.push(O));
                        else O = q ? i(e[A], H, I) : e[A], O = e[A] = k(O), null !== O && z.push(O);
                    this.max = G = b.max.apply(b, z), this.min = F = b.min.apply(b, z), this.stackMax = s = J ? b.max.apply(b, S) : G, this.stackMin = r = J ? b.min.apply(b, z) : F, f.get("chartRangeMin") !== c && (f.get("chartRangeClip") || f.get("chartRangeMin") < F) && (F = f.get("chartRangeMin")), f.get("chartRangeMax") !== c && (f.get("chartRangeClip") || f.get("chartRangeMax") > G) && (G = f.get("chartRangeMax")), this.zeroAxis = D = f.get("zeroAxis", !0), E = 0 >= F && G >= 0 && D ? 0 : 0 == D ? F : F > 0 ? F : G, this.xaxisOffset = E, C = J ? b.max.apply(b, x) + b.max.apply(b, T) : G - F, this.canvasHeightEf = D && 0 > F ? this.canvasHeight - 2 : this.canvasHeight - 1, E > F ? (Q = J && G >= 0 ? s : G, P = (Q - E) / C * this.canvasHeight, P !== b.ceil(P) && (this.canvasHeightEf -= 2, P = b.ceil(P))) : P = this.canvasHeight, this.yoffset = P, d.isArray(f.get("colorMap")) ? (this.colorMapByIndex = f.get("colorMap"), this.colorMapByValue = null) : (this.colorMapByIndex = null, this.colorMapByValue = f.get("colorMap"), this.colorMapByValue && this.colorMapByValue.get === c && (this.colorMapByValue = new t(this.colorMapByValue))), this.range = C
                },
                getRegion: function(a, d) {
                    var f = b.floor(d / this.totalBarWidth);
                    return 0 > f || f >= this.values.length ? c : f
                },
                getCurrentRegionFields: function() {
                    var d, e, a = this.currentRegion,
                        b = r(this.values[a]),
                        c = [];
                    for (e = b.length; e--;) d = b[e], c.push({
                        isNull: null === d,
                        value: d,
                        color: this.calcColor(e, d, a),
                        offset: a
                    });
                    return c
                },
                calcColor: function(a, b, e) {
                    var i, j, f = this.colorMapByIndex,
                        g = this.colorMapByValue,
                        h = this.options;
                    return i = h.get(this.stacked ? "stackedBarColor" : 0 > b ? "negBarColor" : "barColor"), 0 === b && h.get("zeroColor") !== c && (i = h.get("zeroColor")), g && (j = g.get(b)) ? i = j : f && f.length > e && (i = f[e]), d.isArray(i) ? i[a % i.length] : i
                },
                renderRegion: function(a, e) {
                    var q, r, s, t, u, v, w, x, y, z, f = this.values[a],
                        g = this.options,
                        h = this.xaxisOffset,
                        i = [],
                        j = this.range,
                        k = this.stacked,
                        l = this.target,
                        m = a * this.totalBarWidth,
                        n = this.canvasHeightEf,
                        p = this.yoffset;
                    if (f = d.isArray(f) ? f : [f], w = f.length, x = f[0], t = o(null, f), z = o(h, f, !0), t) return g.get("nullColor") ? (s = e ? g.get("nullColor") : this.calcHighlightColor(g.get("nullColor"), g), q = p > 0 ? p - 1 : p, l.drawRect(m, q, this.barWidth - 1, 0, s, s)) : c;
                    for (u = p, v = 0; w > v; v++) {
                        if (x = f[v], k && x === h) {
                            if (!z || y) continue;
                            y = !0
                        }
                        r = j > 0 ? b.floor(n * (b.abs(x - h) / j)) + 1 : 1, h > x || x === h && 0 === p ? (q = u, u += r) : (q = p - r, p -= r), s = this.calcColor(v, x, a), e && (s = this.calcHighlightColor(s, g)), i.push(l.drawRect(m, q, this.barWidth - 1, r - 1, s, s))
                    }
                    return 1 === i.length ? i[0] : i
                }
            }), d.fn.sparkline.tristate = z = g(d.fn.sparkline._base, w, {
                type: "tristate",
                init: function(a, b, e, f, g) {
                    var h = parseInt(e.get("barWidth"), 10),
                        i = parseInt(e.get("barSpacing"), 10);
                    z._super.init.call(this, a, b, e, f, g), this.regionShapes = {}, this.barWidth = h, this.barSpacing = i, this.totalBarWidth = h + i, this.values = d.map(b, Number), this.width = f = b.length * h + (b.length - 1) * i, d.isArray(e.get("colorMap")) ? (this.colorMapByIndex = e.get("colorMap"), this.colorMapByValue = null) : (this.colorMapByIndex = null, this.colorMapByValue = e.get("colorMap"), this.colorMapByValue && this.colorMapByValue.get === c && (this.colorMapByValue = new t(this.colorMapByValue))), this.initTarget()
                },
                getRegion: function(a, c) {
                    return b.floor(c / this.totalBarWidth)
                },
                getCurrentRegionFields: function() {
                    var a = this.currentRegion;
                    return {
                        isNull: this.values[a] === c,
                        value: this.values[a],
                        color: this.calcColor(this.values[a], a),
                        offset: a
                    }
                },
                calcColor: function(a, b) {
                    var g, h, c = this.values,
                        d = this.options,
                        e = this.colorMapByIndex,
                        f = this.colorMapByValue;
                    return g = f && (h = f.get(a)) ? h : e && e.length > b ? e[b] : d.get(c[b] < 0 ? "negBarColor" : c[b] > 0 ? "posBarColor" : "zeroBarColor")
                },
                renderRegion: function(a, c) {
                    var g, h, i, j, k, l, d = this.values,
                        e = this.options,
                        f = this.target;
                    return g = f.pixelHeight, i = b.round(g / 2), j = a * this.totalBarWidth, d[a] < 0 ? (k = i, h = i - 1) : d[a] > 0 ? (k = 0, h = i - 1) : (k = i - 1, h = 2), l = this.calcColor(d[a], a), null !== l ? (c && (l = this.calcHighlightColor(l, e)), f.drawRect(j, k, this.barWidth - 1, h - 1, l, l)) : void 0
                }
            }), d.fn.sparkline.discrete = A = g(d.fn.sparkline._base, w, {
                type: "discrete",
                init: function(a, e, f, g, h) {
                    A._super.init.call(this, a, e, f, g, h), this.regionShapes = {}, this.values = e = d.map(e, Number), this.min = b.min.apply(b, e), this.max = b.max.apply(b, e), this.range = this.max - this.min, this.width = g = "auto" === f.get("width") ? 2 * e.length : this.width, this.interval = b.floor(g / e.length), this.itemWidth = g / e.length, f.get("chartRangeMin") !== c && (f.get("chartRangeClip") || f.get("chartRangeMin") < this.min) && (this.min = f.get("chartRangeMin")), f.get("chartRangeMax") !== c && (f.get("chartRangeClip") || f.get("chartRangeMax") > this.max) && (this.max = f.get("chartRangeMax")), this.initTarget(), this.target && (this.lineHeight = "auto" === f.get("lineHeight") ? b.round(.3 * this.canvasHeight) : f.get("lineHeight"))
                },
                getRegion: function(a, c) {
                    return b.floor(c / this.itemWidth)
                },
                getCurrentRegionFields: function() {
                    var a = this.currentRegion;
                    return {
                        isNull: this.values[a] === c,
                        value: this.values[a],
                        offset: a
                    }
                },
                renderRegion: function(a, c) {
                    var o, p, q, r, d = this.values,
                        e = this.options,
                        f = this.min,
                        g = this.max,
                        h = this.range,
                        j = this.interval,
                        k = this.target,
                        l = this.canvasHeight,
                        m = this.lineHeight,
                        n = l - m;
                    return p = i(d[a], f, g), r = a * j, o = b.round(n - n * ((p - f) / h)), q = e.get(e.get("thresholdColor") && p < e.get("thresholdValue") ? "thresholdColor" : "lineColor"), c && (q = this.calcHighlightColor(q, e)), k.drawLine(r, o, r, o + m, q)
                }
            }), d.fn.sparkline.bullet = B = g(d.fn.sparkline._base, {
                type: "bullet",
                init: function(a, d, e, f, g) {
                    var h, i, j;
                    B._super.init.call(this, a, d, e, f, g), this.values = d = l(d), j = d.slice(), j[0] = null === j[0] ? j[2] : j[0], j[1] = null === d[1] ? j[2] : j[1], h = b.min.apply(b, d), i = b.max.apply(b, d), h = e.get("base") === c ? 0 > h ? h : 0 : e.get("base"), this.min = h, this.max = i, this.range = i - h, this.shapes = {}, this.valueShapes = {}, this.regiondata = {}, this.width = f = "auto" === e.get("width") ? "4.0em" : f, this.target = this.$el.simpledraw(f, g, e.get("composite")), d.length || (this.disabled = !0), this.initTarget()
                },
                getRegion: function(a, b, d) {
                    var e = this.target.getShapeAt(a, b, d);
                    return e !== c && this.shapes[e] !== c ? this.shapes[e] : c
                },
                getCurrentRegionFields: function() {
                    var a = this.currentRegion;
                    return {
                        fieldkey: a.substr(0, 1),
                        value: this.values[a.substr(1)],
                        region: a
                    }
                },
                changeHighlight: function(a) {
                    var d, b = this.currentRegion,
                        c = this.valueShapes[b];
                    switch (delete this.shapes[c], b.substr(0, 1)) {
                        case "r":
                            d = this.renderRange(b.substr(1), a);
                            break;
                        case "p":
                            d = this.renderPerformance(a);
                            break;
                        case "t":
                            d = this.renderTarget(a)
                    }
                    this.valueShapes[b] = d.id, this.shapes[d.id] = b, this.target.replaceWithShape(c, d)
                },
                renderRange: function(a, c) {
                    var d = this.values[a],
                        e = b.round(this.canvasWidth * ((d - this.min) / this.range)),
                        f = this.options.get("rangeColors")[a - 2];
                    return c && (f = this.calcHighlightColor(f, this.options)), this.target.drawRect(0, 0, e - 1, this.canvasHeight - 1, f, f)
                },
                renderPerformance: function(a) {
                    var c = this.values[1],
                        d = b.round(this.canvasWidth * ((c - this.min) / this.range)),
                        e = this.options.get("performanceColor");
                    return a && (e = this.calcHighlightColor(e, this.options)), this.target.drawRect(0, b.round(.3 * this.canvasHeight), d - 1, b.round(.4 * this.canvasHeight) - 1, e, e)
                },
                renderTarget: function(a) {
                    var c = this.values[0],
                        d = b.round(this.canvasWidth * ((c - this.min) / this.range) - this.options.get("targetWidth") / 2),
                        e = b.round(.1 * this.canvasHeight),
                        f = this.canvasHeight - 2 * e,
                        g = this.options.get("targetColor");
                    return a && (g = this.calcHighlightColor(g, this.options)), this.target.drawRect(d, e, this.options.get("targetWidth") - 1, f - 1, g, g)
                },
                render: function() {
                    var c, d, a = this.values.length,
                        b = this.target;
                    if (B._super.render.call(this)) {
                        for (c = 2; a > c; c++) d = this.renderRange(c).append(), this.shapes[d.id] = "r" + c, this.valueShapes["r" + c] = d.id;
                        null !== this.values[1] && (d = this.renderPerformance().append(), this.shapes[d.id] = "p1", this.valueShapes.p1 = d.id), null !== this.values[0] && (d = this.renderTarget().append(), this.shapes[d.id] = "t0", this.valueShapes.t0 = d.id), b.render()
                    }
                }
            }), d.fn.sparkline.pie = C = g(d.fn.sparkline._base, {
                type: "pie",
                init: function(a, c, e, f, g) {
                    var i, h = 0;
                    if (C._super.init.call(this, a, c, e, f, g), this.shapes = {}, this.valueShapes = {}, this.values = c = d.map(c, Number), "auto" === e.get("width") && (this.width = this.height), c.length > 0)
                        for (i = c.length; i--;) h += c[i];
                    this.total = h, this.initTarget(), this.radius = b.floor(b.min(this.canvasWidth, this.canvasHeight) / 2)
                },
                getRegion: function(a, b, d) {
                    var e = this.target.getShapeAt(a, b, d);
                    return e !== c && this.shapes[e] !== c ? this.shapes[e] : c
                },
                getCurrentRegionFields: function() {
                    var a = this.currentRegion;
                    return {
                        isNull: this.values[a] === c,
                        value: this.values[a],
                        percent: this.values[a] / this.total * 100,
                        color: this.options.get("sliceColors")[a % this.options.get("sliceColors").length],
                        offset: a
                    }
                },
                changeHighlight: function(a) {
                    var b = this.currentRegion,
                        c = this.renderSlice(b, a),
                        d = this.valueShapes[b];
                    delete this.shapes[d], this.target.replaceWithShape(d, c), this.valueShapes[b] = c.id, this.shapes[c.id] = b
                },
                renderSlice: function(a, d) {
                    var n, o, p, q, r, e = this.target,
                        f = this.options,
                        g = this.radius,
                        h = f.get("borderWidth"),
                        i = f.get("offset"),
                        j = 2 * b.PI,
                        k = this.values,
                        l = this.total,
                        m = i ? 2 * b.PI * (i / 360) : 0;
                    for (q = k.length, p = 0; q > p; p++) {
                        if (n = m, o = m, l > 0 && (o = m + j * (k[p] / l)), a === p) return r = f.get("sliceColors")[p % f.get("sliceColors").length], d && (r = this.calcHighlightColor(r, f)), e.drawPieSlice(g, g, g - h, n, o, c, r);
                        m = o
                    }
                },
                render: function() {
                    var h, i, a = this.target,
                        d = this.values,
                        e = this.options,
                        f = this.radius,
                        g = e.get("borderWidth");
                    if (C._super.render.call(this)) {
                        for (g && a.drawCircle(f, f, b.floor(f - g / 2), e.get("borderColor"), c, g).append(), i = d.length; i--;) d[i] && (h = this.renderSlice(i).append(), this.valueShapes[i] = h.id, this.shapes[h.id] = i);
                        a.render()
                    }
                }
            }), d.fn.sparkline.box = D = g(d.fn.sparkline._base, {
                type: "box",
                init: function(a, b, c, e, f) {
                    D._super.init.call(this, a, b, c, e, f), this.values = d.map(b, Number), this.width = "auto" === c.get("width") ? "4.0em" : e, this.initTarget(), this.values.length || (this.disabled = 1)
                },
                getRegion: function() {
                    return 1
                },
                getCurrentRegionFields: function() {
                    var a = [{
                        field: "lq",
                        value: this.quartiles[0]
                    }, {
                        field: "med",
                        value: this.quartiles[1]
                    }, {
                        field: "uq",
                        value: this.quartiles[2]
                    }];
                    return this.loutlier !== c && a.push({
                        field: "lo",
                        value: this.loutlier
                    }), this.routlier !== c && a.push({
                        field: "ro",
                        value: this.routlier
                    }), this.lwhisker !== c && a.push({
                        field: "lw",
                        value: this.lwhisker
                    }), this.rwhisker !== c && a.push({
                        field: "rw",
                        value: this.rwhisker
                    }), a
                },
                render: function() {
                    var m, n, o, p, q, r, s, t, u, v, w, a = this.target,
                        d = this.values,
                        e = d.length,
                        f = this.options,
                        g = this.canvasWidth,
                        h = this.canvasHeight,
                        i = f.get("chartRangeMin") === c ? b.min.apply(b, d) : f.get("chartRangeMin"),
                        k = f.get("chartRangeMax") === c ? b.max.apply(b, d) : f.get("chartRangeMax"),
                        l = 0;
                    if (D._super.render.call(this)) {
                        if (f.get("raw")) f.get("showOutliers") && d.length > 5 ? (n = d[0], m = d[1], p = d[2], q = d[3], r = d[4], s = d[5], t = d[6]) : (m = d[0], p = d[1], q = d[2], r = d[3], s = d[4]);
                        else if (d.sort(function(a, b) {
                                return a - b
                            }), p = j(d, 1), q = j(d, 2), r = j(d, 3), o = r - p, f.get("showOutliers")) {
                            for (m = s = c, u = 0; e > u; u++) m === c && d[u] > p - o * f.get("outlierIQR") && (m = d[u]), d[u] < r + o * f.get("outlierIQR") && (s = d[u]);
                            n = d[0], t = d[e - 1]
                        } else m = d[0], s = d[e - 1];
                        this.quartiles = [p, q, r], this.lwhisker = m, this.rwhisker = s, this.loutlier = n, this.routlier = t, w = g / (k - i + 1), f.get("showOutliers") && (l = b.ceil(f.get("spotRadius")), g -= 2 * b.ceil(f.get("spotRadius")), w = g / (k - i + 1), m > n && a.drawCircle((n - i) * w + l, h / 2, f.get("spotRadius"), f.get("outlierLineColor"), f.get("outlierFillColor")).append(), t > s && a.drawCircle((t - i) * w + l, h / 2, f.get("spotRadius"), f.get("outlierLineColor"), f.get("outlierFillColor")).append()), a.drawRect(b.round((p - i) * w + l), b.round(.1 * h), b.round((r - p) * w), b.round(.8 * h), f.get("boxLineColor"), f.get("boxFillColor")).append(), a.drawLine(b.round((m - i) * w + l), b.round(h / 2), b.round((p - i) * w + l), b.round(h / 2), f.get("lineColor")).append(), a.drawLine(b.round((m - i) * w + l), b.round(h / 4), b.round((m - i) * w + l), b.round(h - h / 4), f.get("whiskerColor")).append(), a.drawLine(b.round((s - i) * w + l), b.round(h / 2), b.round((r - i) * w + l), b.round(h / 2), f.get("lineColor")).append(), a.drawLine(b.round((s - i) * w + l), b.round(h / 4), b.round((s - i) * w + l), b.round(h - h / 4), f.get("whiskerColor")).append(), a.drawLine(b.round((q - i) * w + l), b.round(.1 * h), b.round((q - i) * w + l), b.round(.9 * h), f.get("medianColor")).append(), f.get("target") && (v = b.ceil(f.get("spotRadius")), a.drawLine(b.round((f.get("target") - i) * w + l), b.round(h / 2 - v), b.round((f.get("target") - i) * w + l), b.round(h / 2 + v), f.get("targetColor")).append(), a.drawLine(b.round((f.get("target") - i) * w + l - v), b.round(h / 2), b.round((f.get("target") - i) * w + l + v), b.round(h / 2), f.get("targetColor")).append()), a.render()
                    }
                }
            }), G = g({
                init: function(a, b, c, d) {
                    this.target = a, this.id = b, this.type = c, this.args = d
                },
                append: function() {
                    return this.target.appendShape(this), this
                }
            }), H = g({
                _pxregex: /(\d+)(px)?\s*$/i,
                init: function(a, b, c) {
                    a && (this.width = a, this.height = b, this.target = c, this.lastShapeId = null, c[0] && (c = c[0]), d.data(c, "_jqs_vcanvas", this))
                },
                drawLine: function(a, b, c, d, e, f) {
                    return this.drawShape([
                        [a, b],
                        [c, d]
                    ], e, f)
                },
                drawShape: function(a, b, c, d) {
                    return this._genShape("Shape", [a, b, c, d])
                },
                drawCircle: function(a, b, c, d, e, f) {
                    return this._genShape("Circle", [a, b, c, d, e, f])
                },
                drawPieSlice: function(a, b, c, d, e, f, g) {
                    return this._genShape("PieSlice", [a, b, c, d, e, f, g])
                },
                drawRect: function(a, b, c, d, e, f) {
                    return this._genShape("Rect", [a, b, c, d, e, f])
                },
                getElement: function() {
                    return this.canvas
                },
                getLastShapeId: function() {
                    return this.lastShapeId
                },
                reset: function() {
                    alert("reset not implemented")
                },
                _insert: function(a, b) {
                    d(b).html(a)
                },
                _calculatePixelDims: function(a, b, c) {
                    var e;
                    e = this._pxregex.exec(b), this.pixelHeight = e ? e[1] : d(c).height(), e = this._pxregex.exec(a), this.pixelWidth = e ? e[1] : d(c).width()
                },
                _genShape: function(a, b) {
                    var c = L++;
                    return b.unshift(c), new G(this, c, a, b)
                },
                appendShape: function() {
                    alert("appendShape not implemented")
                },
                replaceWithShape: function() {
                    alert("replaceWithShape not implemented")
                },
                insertAfterShape: function() {
                    alert("insertAfterShape not implemented")
                },
                removeShapeId: function() {
                    alert("removeShapeId not implemented")
                },
                getShapeAt: function() {
                    alert("getShapeAt not implemented")
                },
                render: function() {
                    alert("render not implemented")
                }
            }), I = g(H, {
                init: function(b, e, f, g) {
                    I._super.init.call(this, b, e, f), this.canvas = a.createElement("canvas"), f[0] && (f = f[0]), d.data(f, "_jqs_vcanvas", this), d(this.canvas).css({
                        display: "inline-block",
                        width: b,
                        height: e,
                        verticalAlign: "top"
                    }), this._insert(this.canvas, f), this._calculatePixelDims(b, e, this.canvas), this.canvas.width = this.pixelWidth, this.canvas.height = this.pixelHeight, this.interact = g, this.shapes = {}, this.shapeseq = [], this.currentTargetShapeId = c, d(this.canvas).css({
                        width: this.pixelWidth,
                        height: this.pixelHeight
                    })
                },
                _getContext: function(a, b, d) {
                    var e = this.canvas.getContext("2d");
                    return a !== c && (e.strokeStyle = a), e.lineWidth = d === c ? 1 : d, b !== c && (e.fillStyle = b), e
                },
                reset: function() {
                    var a = this._getContext();
                    a.clearRect(0, 0, this.pixelWidth, this.pixelHeight), this.shapes = {}, this.shapeseq = [], this.currentTargetShapeId = c
                },
                _drawShape: function(a, b, d, e, f) {
                    var h, i, g = this._getContext(d, e, f);
                    for (g.beginPath(), g.moveTo(b[0][0] + .5, b[0][1] + .5), h = 1, i = b.length; i > h; h++) g.lineTo(b[h][0] + .5, b[h][1] + .5);
                    d !== c && g.stroke(), e !== c && g.fill(), this.targetX !== c && this.targetY !== c && g.isPointInPath(this.targetX, this.targetY) && (this.currentTargetShapeId = a)
                },
                _drawCircle: function(a, d, e, f, g, h, i) {
                    var j = this._getContext(g, h, i);
                    j.beginPath(), j.arc(d, e, f, 0, 2 * b.PI, !1), this.targetX !== c && this.targetY !== c && j.isPointInPath(this.targetX, this.targetY) && (this.currentTargetShapeId = a), g !== c && j.stroke(), h !== c && j.fill()
                },
                _drawPieSlice: function(a, b, d, e, f, g, h, i) {
                    var j = this._getContext(h, i);
                    j.beginPath(), j.moveTo(b, d), j.arc(b, d, e, f, g, !1), j.lineTo(b, d), j.closePath(), h !== c && j.stroke(), i && j.fill(), this.targetX !== c && this.targetY !== c && j.isPointInPath(this.targetX, this.targetY) && (this.currentTargetShapeId = a)
                },
                _drawRect: function(a, b, c, d, e, f, g) {
                    return this._drawShape(a, [
                        [b, c],
                        [b + d, c],
                        [b + d, c + e],
                        [b, c + e],
                        [b, c]
                    ], f, g)
                },
                appendShape: function(a) {
                    return this.shapes[a.id] = a, this.shapeseq.push(a.id), this.lastShapeId = a.id, a.id
                },
                replaceWithShape: function(a, b) {
                    var d, c = this.shapeseq;
                    for (this.shapes[b.id] = b, d = c.length; d--;) c[d] == a && (c[d] = b.id);
                    delete this.shapes[a]
                },
                replaceWithShapes: function(a, b) {
                    var e, f, g, c = this.shapeseq,
                        d = {};
                    for (f = a.length; f--;) d[a[f]] = !0;
                    for (f = c.length; f--;) e = c[f], d[e] && (c.splice(f, 1), delete this.shapes[e], g = f);
                    for (f = b.length; f--;) c.splice(g, 0, b[f].id), this.shapes[b[f].id] = b[f]
                },
                insertAfterShape: function(a, b) {
                    var d, c = this.shapeseq;
                    for (d = c.length; d--;)
                        if (c[d] === a) return c.splice(d + 1, 0, b.id), void(this.shapes[b.id] = b)
                },
                removeShapeId: function(a) {
                    var c, b = this.shapeseq;
                    for (c = b.length; c--;)
                        if (b[c] === a) {
                            b.splice(c, 1);
                            break
                        }
                    delete this.shapes[a]
                },
                getShapeAt: function(a, b, c) {
                    return this.targetX = b, this.targetY = c, this.render(), this.currentTargetShapeId
                },
                render: function() {
                    var e, f, g, a = this.shapeseq,
                        b = this.shapes,
                        c = a.length,
                        d = this._getContext();
                    for (d.clearRect(0, 0, this.pixelWidth, this.pixelHeight), g = 0; c > g; g++) e = a[g], f = b[e], this["_draw" + f.type].apply(this, f.args);
                    this.interact || (this.shapes = {}, this.shapeseq = [])
                }
            }), J = g(H, {
                init: function(b, c, e) {
                    var f;
                    J._super.init.call(this, b, c, e), e[0] && (e = e[0]), d.data(e, "_jqs_vcanvas", this), this.canvas = a.createElement("span"), d(this.canvas).css({
                        display: "inline-block",
                        position: "relative",
                        overflow: "hidden",
                        width: b,
                        height: c,
                        margin: "0px",
                        padding: "0px",
                        verticalAlign: "top"
                    }), this._insert(this.canvas, e), this._calculatePixelDims(b, c, this.canvas), this.canvas.width = this.pixelWidth, this.canvas.height = this.pixelHeight, f = '<v:group coordorigin="0 0" coordsize="' + this.pixelWidth + " " + this.pixelHeight + '" style="position:absolute;top:0;left:0;width:' + this.pixelWidth + "px;height=" + this.pixelHeight + 'px;"></v:group>', this.canvas.insertAdjacentHTML("beforeEnd", f), this.group = d(this.canvas).children()[0], this.rendered = !1, this.prerender = ""
                },
                _drawShape: function(a, b, d, e, f) {
                    var h, i, j, k, l, m, n, g = [];
                    for (n = 0, m = b.length; m > n; n++) g[n] = "" + b[n][0] + "," + b[n][1];
                    return h = g.splice(0, 1), f = f === c ? 1 : f, i = d === c ? ' stroked="false" ' : ' strokeWeight="' + f + 'px" strokeColor="' + d + '" ', j = e === c ? ' filled="false"' : ' fillColor="' + e + '" filled="true" ', k = g[0] === g[g.length - 1] ? "x " : "", l = '<v:shape coordorigin="0 0" coordsize="' + this.pixelWidth + " " + this.pixelHeight + '"  id="jqsshape' + a + '" ' + i + j + ' style="position:absolute;left:0px;top:0px;height:' + this.pixelHeight + "px;width:" + this.pixelWidth + 'px;padding:0px;margin:0px;"  path="m ' + h + " l " + g.join(", ") + " " + k + 'e"> </v:shape>'
                },
                _drawCircle: function(a, b, d, e, f, g, h) {
                    var i, j, k;
                    return b -= e, d -= e, i = f === c ? ' stroked="false" ' : ' strokeWeight="' + h + 'px" strokeColor="' + f + '" ', j = g === c ? ' filled="false"' : ' fillColor="' + g + '" filled="true" ', k = '<v:oval  id="jqsshape' + a + '" ' + i + j + ' style="position:absolute;top:' + d + "px; left:" + b + "px; width:" + 2 * e + "px; height:" + 2 * e + 'px"></v:oval>'
                },
                _drawPieSlice: function(a, d, e, f, g, h, i, j) {
                    var k, l, m, n, o, p, q, r;
                    if (g === h) return "";
                    if (h - g === 2 * b.PI && (g = 0, h = 2 * b.PI), l = d + b.round(b.cos(g) * f), m = e + b.round(b.sin(g) * f), n = d + b.round(b.cos(h) * f), o = e + b.round(b.sin(h) * f), l === n && m === o) {
                        if (h - g < b.PI) return "";
                        l = n = d + f, m = o = e
                    }
                    return l === n && m === o && h - g < b.PI ? "" : (k = [d - f, e - f, d + f, e + f, l, m, n, o], p = i === c ? ' stroked="false" ' : ' strokeWeight="1px" strokeColor="' + i + '" ', q = j === c ? ' filled="false"' : ' fillColor="' + j + '" filled="true" ', r = '<v:shape coordorigin="0 0" coordsize="' + this.pixelWidth + " " + this.pixelHeight + '"  id="jqsshape' + a + '" ' + p + q + ' style="position:absolute;left:0px;top:0px;height:' + this.pixelHeight + "px;width:" + this.pixelWidth + 'px;padding:0px;margin:0px;"  path="m ' + d + "," + e + " wa " + k.join(", ") + ' x e"> </v:shape>')
                },
                _drawRect: function(a, b, c, d, e, f, g) {
                    return this._drawShape(a, [
                        [b, c],
                        [b, c + e],
                        [b + d, c + e],
                        [b + d, c],
                        [b, c]
                    ], f, g)
                },
                reset: function() {
                    this.group.innerHTML = ""
                },
                appendShape: function(a) {
                    var b = this["_draw" + a.type].apply(this, a.args);
                    return this.rendered ? this.group.insertAdjacentHTML("beforeEnd", b) : this.prerender += b, this.lastShapeId = a.id, a.id
                },
                replaceWithShape: function(a, b) {
                    var c = d("#jqsshape" + a),
                        e = this["_draw" + b.type].apply(this, b.args);
                    c[0].outerHTML = e
                },
                replaceWithShapes: function(a, b) {
                    var g, c = d("#jqsshape" + a[0]),
                        e = "",
                        f = b.length;
                    for (g = 0; f > g; g++) e += this["_draw" + b[g].type].apply(this, b[g].args);
                    for (c[0].outerHTML = e, g = 1; g < a.length; g++) d("#jqsshape" + a[g]).remove()
                },
                insertAfterShape: function(a, b) {
                    var c = d("#jqsshape" + a),
                        e = this["_draw" + b.type].apply(this, b.args);
                    c[0].insertAdjacentHTML("afterEnd", e)
                },
                removeShapeId: function(a) {
                    var b = d("#jqsshape" + a);
                    this.group.removeChild(b[0])
                },
                getShapeAt: function(a) {
                    var d = a.id.substr(8);
                    return d
                },
                render: function() {
                    this.rendered || (this.group.innerHTML = this.prerender, this.rendered = !0)
                }
            })
        })
    }(document, Math),
    function(global) {
        "use strict";

        function circle(ctx, x, y, r) {
            ctx.beginPath(), ctx.arc(x, y, r, 0, TAU, !1), ctx.fill()
        }

        function line(ctx, ax, ay, bx, by) {
            ctx.beginPath(), ctx.moveTo(ax, ay), ctx.lineTo(bx, by), ctx.stroke()
        }

        function puff(ctx, t, cx, cy, rx, ry, rmin, rmax) {
            var c = Math.cos(t * TAU),
                s = Math.sin(t * TAU);
            rmax -= rmin, circle(ctx, cx - s * rx, cy + c * ry + .5 * rmax, rmin + (1 - .5 * c) * rmax)
        }

        function puffs(ctx, t, cx, cy, rx, ry, rmin, rmax) {
            var i;
            for (i = 5; i--;) puff(ctx, t + i / 5, cx, cy, rx, ry, rmin, rmax)
        }

        function cloud(ctx, t, cx, cy, cw, s, color) {
            t /= 3e4;
            var a = .21 * cw,
                b = .12 * cw,
                c = .24 * cw,
                d = .28 * cw;
            ctx.fillStyle = color, puffs(ctx, t, cx, cy, a, b, c, d), ctx.globalCompositeOperation = "destination-out", puffs(ctx, t, cx, cy, a, b, c - s, d - s), ctx.globalCompositeOperation = "source-over"
        }

        function sun(ctx, t, cx, cy, cw, s, color) {
            t /= 12e4;
            var i, p, cos, sin, a = .25 * cw - .5 * s,
                b = .32 * cw + .5 * s,
                c = .5 * cw - .5 * s;
            for (ctx.strokeStyle = color, ctx.lineWidth = s, ctx.lineCap = "round", ctx.lineJoin = "round", ctx.beginPath(), ctx.arc(cx, cy, a, 0, TAU, !1), ctx.stroke(), i = 8; i--;) p = (t + i / 8) * TAU, cos = Math.cos(p), sin = Math.sin(p), line(ctx, cx + cos * b, cy + sin * b, cx + cos * c, cy + sin * c)
        }

        function moon(ctx, t, cx, cy, cw, s, color) {
            t /= 15e3;
            var a = .29 * cw - .5 * s,
                b = .05 * cw,
                c = Math.cos(t * TAU),
                p = c * TAU / -16;
            ctx.strokeStyle = color, ctx.lineWidth = s, ctx.lineCap = "round", ctx.lineJoin = "round", cx += c * b, ctx.beginPath(), ctx.arc(cx, cy, a, p + TAU / 8, p + 7 * TAU / 8, !1), ctx.arc(cx + Math.cos(p) * a * TWO_OVER_SQRT_2, cy + Math.sin(p) * a * TWO_OVER_SQRT_2, a, p + 5 * TAU / 8, p + 3 * TAU / 8, !0), ctx.closePath(), ctx.stroke()
        }

        function rain(ctx, t, cx, cy, cw, s, color) {
            t /= 1350;
            var i, p, x, y, a = .16 * cw,
                b = 11 * TAU / 12,
                c = 7 * TAU / 12;
            for (ctx.fillStyle = color, i = 4; i--;) p = (t + i / 4) % 1, x = cx + (i - 1.5) / 1.5 * (1 === i || 2 === i ? -1 : 1) * a, y = cy + p * p * cw, ctx.beginPath(), ctx.moveTo(x, y - 1.5 * s), ctx.arc(x, y, .75 * s, b, c, !1), ctx.fill()
        }

        function sleet(ctx, t, cx, cy, cw, s, color) {
            t /= 750;
            var i, p, x, y, a = .1875 * cw;
            for (ctx.strokeStyle = color, ctx.lineWidth = .5 * s, ctx.lineCap = "round", ctx.lineJoin = "round", i = 4; i--;) p = (t + i / 4) % 1, x = Math.floor(cx + (i - 1.5) / 1.5 * (1 === i || 2 === i ? -1 : 1) * a) + .5, y = cy + p * cw, line(ctx, x, y - 1.5 * s, x, y + 1.5 * s)
        }

        function snow(ctx, t, cx, cy, cw, s, color) {
            t /= 3e3;
            var i, p, x, y, a = .16 * cw,
                b = .75 * s,
                u = t * TAU * .7,
                ux = Math.cos(u) * b,
                uy = Math.sin(u) * b,
                v = u + TAU / 3,
                vx = Math.cos(v) * b,
                vy = Math.sin(v) * b,
                w = u + 2 * TAU / 3,
                wx = Math.cos(w) * b,
                wy = Math.sin(w) * b;
            for (ctx.strokeStyle = color, ctx.lineWidth = .5 * s, ctx.lineCap = "round", ctx.lineJoin = "round", i = 4; i--;) p = (t + i / 4) % 1, x = cx + Math.sin((p + i / 4) * TAU) * a, y = cy + p * cw, line(ctx, x - ux, y - uy, x + ux, y + uy), line(ctx, x - vx, y - vy, x + vx, y + vy), line(ctx, x - wx, y - wy, x + wx, y + wy)
        }

        function fogbank(ctx, t, cx, cy, cw, s, color) {
            t /= 3e4;
            var a = .21 * cw,
                b = .06 * cw,
                c = .21 * cw,
                d = .28 * cw;
            ctx.fillStyle = color, puffs(ctx, t, cx, cy, a, b, c, d), ctx.globalCompositeOperation = "destination-out", puffs(ctx, t, cx, cy, a, b, c - s, d - s), ctx.globalCompositeOperation = "source-over"
        }

        function leaf(ctx, t, x, y, cw, s, color) {
            var a = cw / 8,
                b = a / 3,
                c = 2 * b,
                d = t % 1 * TAU,
                e = Math.cos(d),
                f = Math.sin(d);
            ctx.fillStyle = color, ctx.strokeStyle = color, ctx.lineWidth = s, ctx.lineCap = "round", ctx.lineJoin = "round", ctx.beginPath(), ctx.arc(x, y, a, d, d + Math.PI, !1), ctx.arc(x - b * e, y - b * f, c, d + Math.PI, d, !1), ctx.arc(x + c * e, y + c * f, b, d + Math.PI, d, !0), ctx.globalCompositeOperation = "destination-out", ctx.fill(), ctx.globalCompositeOperation = "source-over", ctx.stroke()
        }

        function swoosh(ctx, t, cx, cy, cw, s, index, total, color) {
            t /= 2500;
            var b, d, f, i, path = WIND_PATHS[index],
                a = (t + index - WIND_OFFSETS[index].start) % total,
                c = (t + index - WIND_OFFSETS[index].end) % total,
                e = (t + index) % total;
            if (ctx.strokeStyle = color, ctx.lineWidth = s, ctx.lineCap = "round", ctx.lineJoin = "round", 1 > a) {
                if (ctx.beginPath(), a *= path.length / 2 - 1, b = Math.floor(a), a -= b, b *= 2, b += 2, ctx.moveTo(cx + (path[b - 2] * (1 - a) + path[b] * a) * cw, cy + (path[b - 1] * (1 - a) + path[b + 1] * a) * cw), 1 > c) {
                    for (c *= path.length / 2 - 1, d = Math.floor(c), c -= d, d *= 2, d += 2, i = b; i !== d; i += 2) ctx.lineTo(cx + path[i] * cw, cy + path[i + 1] * cw);
                    ctx.lineTo(cx + (path[d - 2] * (1 - c) + path[d] * c) * cw, cy + (path[d - 1] * (1 - c) + path[d + 1] * c) * cw)
                } else
                    for (i = b; i !== path.length; i += 2) ctx.lineTo(cx + path[i] * cw, cy + path[i + 1] * cw);
                ctx.stroke()
            } else if (1 > c) {
                for (ctx.beginPath(), c *= path.length / 2 - 1, d = Math.floor(c), c -= d, d *= 2, d += 2, ctx.moveTo(cx + path[0] * cw, cy + path[1] * cw), i = 2; i !== d; i += 2) ctx.lineTo(cx + path[i] * cw, cy + path[i + 1] * cw);
                ctx.lineTo(cx + (path[d - 2] * (1 - c) + path[d] * c) * cw, cy + (path[d - 1] * (1 - c) + path[d + 1] * c) * cw), ctx.stroke()
            }
            1 > e && (e *= path.length / 2 - 1, f = Math.floor(e), e -= f, f *= 2, f += 2, leaf(ctx, t, cx + (path[f - 2] * (1 - e) + path[f] * e) * cw, cy + (path[f - 1] * (1 - e) + path[f + 1] * e) * cw, cw, s, color))
        }
        var requestInterval, cancelInterval;
        ! function() {
            var raf = global.requestAnimationFrame || global.webkitRequestAnimationFrame || global.mozRequestAnimationFrame || global.oRequestAnimationFrame || global.msRequestAnimationFrame,
                caf = global.cancelAnimationFrame || global.webkitCancelAnimationFrame || global.mozCancelAnimationFrame || global.oCancelAnimationFrame || global.msCancelAnimationFrame;
            raf && caf ? (requestInterval = function(fn) {
                function loop() {
                    handle.value = raf(loop), fn()
                }
                var handle = {
                    value: null
                };
                return loop(), handle
            }, cancelInterval = function(handle) {
                caf(handle.value)
            }) : (requestInterval = setInterval, cancelInterval = clearInterval)
        }();
        var KEYFRAME = 500,
            STROKE = .08,
            TAU = 2 * Math.PI,
            TWO_OVER_SQRT_2 = 2 / Math.sqrt(2),
            WIND_PATHS = [
                [-.75, -.18, -.7219, -.1527, -.6971, -.1225, -.6739, -.091, -.6516, -.0588, -.6298, -.0262, -.6083, .0065, -.5868, .0396, -.5643, .0731, -.5372, .1041, -.5033, .1259, -.4662, .1406, -.4275, .1493, -.3881, .153, -.3487, .1526, -.3095, .1488, -.2708, .1421, -.2319, .1342, -.1943, .1217, -.16, .1025, -.129, .0785, -.1012, .0509, -.0764, .0206, -.0547, -.012, -.0378, -.0472, -.0324, -.0857, -.0389, -.1241, -.0546, -.1599, -.0814, -.1876, -.1193, -.1964, -.1582, -.1935, -.1931, -.1769, -.2157, -.1453, -.229, -.1085, -.2327, -.0697, -.224, -.0317, -.2064, .0033, -.1853, .0362, -.1613, .0672, -.135, .0961, -.1051, .1213, -.0706, .1397, -.0332, .1512, .0053, .158, .0442, .1624, .0833, .1636, .1224, .1615, .1613, .1565, .1999, .15, .2378, .1402, .2749, .1279, .3118, .1147, .3487, .1015, .3858, .0892, .4236, .0787, .4621, .0715, .5012, .0702, .5398, .0766, .5768, .089, .6123, .1055, .6466, .1244, .6805, .144, .7147, .163, .75, .18],
                [-.75, 0, -.7033, .0195, -.6569, .0399, -.6104, .06, -.5634, .0789, -.5155, .0954, -.4667, .1089, -.4174, .1206, -.3676, .1299, -.3174, .1365, -.2669, .1398, -.2162, .1391, -.1658, .1347, -.1157, .1271, -.0661, .1169, -.017, .1046, .0316, .0903, .0791, .0728, .1259, .0534, .1723, .0331, .2188, .0129, .2656, -.0064, .3122, -.0263, .3586, -.0466, .4052, -.0665, .4525, -.0847, .5007, -.1002, .5497, -.113, .5991, -.124, .6491, -.1325, .6994, -.138, .75, -.14]
            ],
            WIND_OFFSETS = [{
                start: .36,
                end: .11
            }, {
                start: .56,
                end: .16
            }],
            Skycons = function(opts) {
                this.list = [], this.interval = null, this.color = opts && opts.color ? opts.color : "black", this.resizeClear = !(!opts || !opts.resizeClear)
            };
        Skycons.CLEAR_DAY = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            sun(ctx, t, .5 * w, .5 * h, s, s * STROKE, color)
        }, Skycons.CLEAR_NIGHT = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            moon(ctx, t, .5 * w, .5 * h, s, s * STROKE, color)
        }, Skycons.PARTLY_CLOUDY_DAY = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            sun(ctx, t, .625 * w, .375 * h, .75 * s, s * STROKE, color), cloud(ctx, t, .375 * w, .625 * h, .75 * s, s * STROKE, color)
        }, Skycons.PARTLY_CLOUDY_NIGHT = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            moon(ctx, t, .667 * w, .375 * h, .75 * s, s * STROKE, color), cloud(ctx, t, .375 * w, .625 * h, .75 * s, s * STROKE, color)
        }, Skycons.CLOUDY = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            cloud(ctx, t, .5 * w, .5 * h, s, s * STROKE, color)
        }, Skycons.RAIN = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            rain(ctx, t, .5 * w, .37 * h, .9 * s, s * STROKE, color), cloud(ctx, t, .5 * w, .37 * h, .9 * s, s * STROKE, color)
        }, Skycons.SLEET = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            sleet(ctx, t, .5 * w, .37 * h, .9 * s, s * STROKE, color), cloud(ctx, t, .5 * w, .37 * h, .9 * s, s * STROKE, color)
        }, Skycons.SNOW = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            snow(ctx, t, .5 * w, .37 * h, .9 * s, s * STROKE, color), cloud(ctx, t, .5 * w, .37 * h, .9 * s, s * STROKE, color)
        }, Skycons.WIND = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h);
            swoosh(ctx, t, .5 * w, .5 * h, s, s * STROKE, 0, 2, color), swoosh(ctx, t, .5 * w, .5 * h, s, s * STROKE, 1, 2, color)
        }, Skycons.FOG = function(ctx, t, color) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                s = Math.min(w, h),
                k = s * STROKE;
            fogbank(ctx, t, .5 * w, .32 * h, .75 * s, k, color), t /= 5e3;
            var a = Math.cos(t * TAU) * s * .02,
                b = Math.cos((t + .25) * TAU) * s * .02,
                c = Math.cos((t + .5) * TAU) * s * .02,
                d = Math.cos((t + .75) * TAU) * s * .02,
                n = .936 * h,
                e = Math.floor(n - .5 * k) + .5,
                f = Math.floor(n - 2.5 * k) + .5;
            ctx.strokeStyle = color, ctx.lineWidth = k, ctx.lineCap = "round", ctx.lineJoin = "round", line(ctx, a + .2 * w + .5 * k, e, b + .8 * w - .5 * k, e), line(ctx, c + .2 * w + .5 * k, f, d + .8 * w - .5 * k, f)
        }, Skycons.prototype = {
            add: function(el, draw) {
                var obj;
                "string" == typeof el && (el = document.getElementById(el)), null !== el && ("string" == typeof draw && (draw = draw.toUpperCase().replace(/-/g, "_"), draw = Skycons.hasOwnProperty(draw) ? Skycons[draw] : null), "function" == typeof draw && (obj = {
                    element: el,
                    context: el.getContext("2d"),
                    drawing: draw
                }, this.list.push(obj), this.draw(obj, KEYFRAME)))
            },
            set: function(el, draw) {
                var i;
                for ("string" == typeof el && (el = document.getElementById(el)), i = this.list.length; i--;)
                    if (this.list[i].element === el) return this.list[i].drawing = draw, void this.draw(this.list[i], KEYFRAME);
                this.add(el, draw)
            },
            remove: function(el) {
                var i;
                for ("string" == typeof el && (el = document.getElementById(el)), i = this.list.length; i--;)
                    if (this.list[i].element === el) return void this.list.splice(i, 1)
            },
            draw: function(obj, time) {
                var canvas = obj.context.canvas;
                this.resizeClear ? canvas.width = canvas.width : obj.context.clearRect(0, 0, canvas.width, canvas.height), obj.drawing(obj.context, time, this.color)
            },
            play: function() {
                var self = this;
                this.pause(), this.interval = requestInterval(function() {
                    var i, now = Date.now();
                    for (i = self.list.length; i--;) self.draw(self.list[i], now)
                }, 1e3 / 60)
            },
            pause: function() {
                this.interval && (cancelInterval(this.interval), this.interval = null)
            }
        }, global.Skycons = Skycons
    }(this);



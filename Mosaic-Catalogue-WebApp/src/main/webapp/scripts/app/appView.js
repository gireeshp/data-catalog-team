(function(){
	"use strict";
	var appView = angular.module("app.appView",['angularFileUpload']);


	appView.constant('CONSTANTS',{
			INPUT: 'INPUT',
			SYSTEM: 'SYSTEM',
			TIMESTAMP: 'TIMESTAMP',
			GP_LAST_RUN_DATETIME: 'gp_lastRunDateTime',
			GP_CURRENT_RUN_DATETIME: 'gp_currentRunDateTime',
			GP_LAST_SUCCESSFULLY_RUN_DATETIME: 'gp_lastSuccessfullyRunDateTime',
			GLOABL_DEFAULT_TIMESTAMP: 'dd/MM/yyyy HH:mm:ss'
	});
	
	appView.filter('unsafe', function($sce) {
		return function(val) {
			return $sce.trustAsHtml(val);
		};
	});
	
	appView.service('SaveAsService', ["browserDetectService" , function(a) {

		this.SaveAs = function(b, c, d) {
	        var e = "\ufeff";
	        if (a.detectIE()) {
	            angular.element("body").append('<iframe id="SaveAsId" style="display: none"></iframe>');
	            var f = angular.element("body > iframe#SaveAsId")[0].contentWindow;
	            b = e + b, f.document.open("text/json", "replace"), f.document.write(b), f.document.close(), f.focus();
	            var g = Date.now();
	            f.document.execCommand("SaveAs", !1, c + "." + d);
	            var h = Date.now();
	            g === h && f.document.execCommand("SaveAs", !0, c + ".txt"), angular.element("body > iframe#SaveAsId").remove()
	        } else {
	            b = "data:image/svg;charset=utf-8," + e + encodeURIComponent(b), angular.element("body").append('<a id="SaveAsId"></a>');
	            var i = angular.element("body > a#SaveAsId");
	            i.attr("href", b), i.attr("download", c + "." + d), i.attr("target", "_blank"), i[0].click(), i.remove() 
	        }
	    } 
	}]);

	//Capitalize Case
	appView.filter('titleCase', function() {
		return function(input) {
			input = input || '';
			return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		};
	});
	
	appView.controller("appDataCtrl" , ["$scope" ,"$filter" , "$http", "$location" ,"$modal", "logger","$log", "$route","$rootScope","$cookieStore","$interval","SaveAsService", "StorageStrategyService","$timeout", function($scope, $filter , $http, $location,$modal ,logger, $log, $route,$rootScope,$cookieStore, $interval,SaveAsService, StorageStrategyService,$timeout){
		$scope.projectAccessType = $cookieStore.get("projectAccessType");
		$scope.storageStrategy =[]
		$http.post("/getDataSourcesMsg").success(function(data, status, headers, config){
			$scope.explanation = data;
		});
		
//		$rootScope.refreshFlowList = true;	//commented bcoz its making unnecessary second request in beginning 
		$interval(function() {
			if($rootScope.refreshFlowList){
				$rootScope.refreshFlowList = false;
				$scope.getFlowList($scope.currentPage,'');
			}
		}, 500);	
		
		$scope.exportApp = function(application){
			$http({
				method: "POST",
				url: "/exportFlow",
				data : application.appId
			}).success(function(data){
				var jsonContent = JSON.stringify(data);
				//SaveAsService.SaveAs(jsonContent, application.appName, 'json');
				var blob = new Blob([jsonContent], { type: 'plain/text'});
	            var filename =  application.appName +".json";
	            if(window.navigator.msSaveBlob) {
	                window.navigator.msSaveBlob(blob, filename);
	            } else{
	                var elem = window.document.createElement('a');
	                elem.href = window.URL.createObjectURL(blob);
	                elem.download = filename;
	                document.body.appendChild(elem);
	                elem.click();
	                document.body.removeChild(elem);
	           }
			}).error(function(data){
				logger.logError(data);
			});	
		};
		
		$scope.getFlowList = function(presentPage, action){
			$http.post("/getAllApplications","PERIODIC").success(function(data , status, header, config){
				return $scope.stores = data
				, $scope.searchKeywords = "", 
				$scope.filteredStores = [],
				$scope.row = "", 
				$scope.select = function(page) {
					$scope.pageNum = page;
					$scope.currentPage = page;
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
				}, 
				$scope.onFilterChange = function() {
					return $scope.select($scope.currentPage), $scope.row = "";
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.search = function() {
					return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
				}, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				$scope.numPerPage = $scope.numPerPageOpt[4], 
				$scope.currentPage = 1, 
				$scope.currentPageStores = [],
				($scope.init = function() {
					if(action == 'DELETE') {
		        		if(($scope.stores.length > 0) && (((presentPage - 1) * $scope.numPerPage) == $scope.stores.length))
			        		presentPage -= 1;
		        	}
					$scope.currentPage = (presentPage) ? presentPage : $scope.currentPage;
					return $scope.search(), $scope.select($scope.currentPage),$scope.order("-appId");
				})();
			}).error(function(data , status, header, config){

			});
		};
		
		$scope.getFlowList(1,'');

		$scope.showAppData = function(appName,type){
			$rootScope.flowType = type;
		};

		$scope.scheduleJob = function(application){
			var modalInstance;
			modalInstance = $modal.open({
				size :"md",
				templateUrl: "/views/template/scheduleJob.html",
				controller: "jobScheduleController",
				resolve : {
					application :function(){
						return application;
					}
				}
			}),modalInstance.result.then(function() {
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};

		$scope.showSchedJob = function() {

			$http({
				method: 'POST',
				url : "/getScheduleJobs",
				data : $scope.showSchedJobs
			}).success(function(data, status, headers, config){

				return $scope.stores = data
				, $scope.searchKeywords = "",
				$scope.filteredStores = [],
				$scope.row = "",
				$scope.select = function(page) {
					$scope.pageNum = page;
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
				},
				$scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.search = function() {
					return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				}, $scope.order = function(rowName) { 
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
				}, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				$scope.numPerPage = $scope.numPerPageOpt[4],
				$scope.currentPage = 1,
				$scope.currentPageStores = [],
				($scope.init = function() {
					return $scope.search(), $scope.select($scope.currentPage),$scope.order("-appId");
				})();
			}).error(function(data){
				logger.logError("Unable to start the app. Error : " + data);
			});
		};

		$scope.createClone = function (application) {
			var modalInstance;
			modalInstance = $modal.open({ 
				size :"md",
				templateUrl: "/views/template/CreateClone.html",
				controller: "createCloneController",
				resolve : {	
					application :function(){
						return application;
					} 
				}
			}),modalInstance.result.then(function() {
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};

		$scope.open = function(type){
			var modalInstance;
			modalInstance = $modal.open({ 
				size :"lg",
				templateUrl: "/views/template/appViewModal.html",
				controller: "appViewModalController",
				resolve : {	
					dataTypes:function(){
						return ["INTEGER", "LONG", "STRING" , "DATE" , "DOUBLE" , "BOOLEAN", "TIMESTAMP"];
					},
					inputParamTypes : function(){
						return ["INPUT" , "FILE" , "CALCULATED","SYSTEM"];
					},
					appName : function(){
						return null;
					},
					flowType : function(){
						return type;
					}
				}
			}),modalInstance.result.then(function(reponame) {

			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};

		$scope.importFlow = function(application){
			var modalInstance;
			modalInstance = $modal.open({ 
				size :"lg",
				templateUrl: "/views/template/importFlowModal.html",
				controller: "importFlowController"
			}),modalInstance.result.then(function(reponame) {
				
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};

		$scope.deleteApp = function(application){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:application.appName, type: "flow"};
					}
				}
			}),modalInstance.result.then(function() {
				$http({
					/*method: 'POST',
					url : "/deleteApp?appId="+application.appId*/
					method: 'POST',
					url : "/deleteApp",
					data : application.appId
				}).success(function(data, status, headers, config){
					//$rootScope.refreshFlowList = true;
					$scope.getFlowList($scope.currentPage,'DELETE');
					logger.logSuccess("Flow deleted successfully");
				}).error(function(data){
					logger.logError(data);
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		},

		$scope.checkLeafNode = function(type , application){


			$http({
				method: 'POST',
				url : "/checkWarning",
				data : application.appId
			}).
			success(function(data, status, headers, config){
				if(data !== null && data.length === 0){
					if(type === "spark")
						$scope.runAppClickOnSpark(application);
					else
						$scope.runAppClick(application);
				}else{
					var modalInstance;
					modalInstance = $modal.open({ 
						templateUrl: "/views/template/saveAs.html",
						controller: "saveAsController",
						resolve : {
							list:function(){
								return data;
							},
							messg : function(){
								return "Following leaf nodes are not selected to save.";
							},
							ind : function(){
								return false;
							},
							headerTitle : function() {
								return "Delete Confirmation";
							},
							yesBtnLable : function() {
								return "Yes";
							},
							noBtnLable : function() {
								return "No";
							}
						}
					});
					modalInstance.result.then(function (param) {
						if(param === "yes"){
							if(type === "spark")
								$scope.runAppClickOnSpark(application);
							else
								$scope.runAppClick(application);
						}
					});
				}
			}).error(function(data){
				logger.logError("Unable to start the app. Error : " + data);
				return false;
			});
		},		
		$scope.runAppClick = function(application){
			$http.post("/getSingleApplication",$scope.data.appName).success(function(data, status, headers, config){
				if("active" != data.statusEnum){
					logger.logError("App is not Active, please publish the app.");
					return false;
				}
			});
			if(true)
			{
				var modalInstance;
				modalInstance = $modal.open({ 
					size :"xl",
					templateUrl: "/views/template/appViewInputModal.html",
					controller: "appViewInputModalController",
					resolve : {
						comingFrom:function(){
							return "RUNAPP";
						},
						rootId:function(){
							return undefined;
						}
					}
				}),modalInstance.result.then(function(reponame) {

				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				}),modalInstance.application = application
				,modalInstance.dataTypes = ["INTEGER", "LONG", "STRING" , "DATE" , "DOUBLE" , "BOOLEAN", "TIMESTAMP"];
			}
		};
		function runMethod(application){
			if(true)
			{
				var modalInstance;
				modalInstance = $modal.open({ 
					size :"xl",
					templateUrl: "/views/template/appViewInputModal.html",
					controller: "appViewInputModalController",
					resolve : {
						comingFrom:function(){
							return "SPARK";
						},
						rootId:function(){
							return undefined;
						}
					}
				}),modalInstance.result.then(function(reponame) {

				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				}),modalInstance.application = application
				,modalInstance.dataTypes = ["INTEGER", "LONG", "STRING" , "DATE" , "DOUBLE" , "BOOLEAN", "TIMESTAMP"];
			}
		};
		$scope.runAppClickOnSpark = function(application){
			$http.post("/getSingleApplication",application.appName).success(function(data, status, headers, config){
				$rootScope.pollIterval=data.pollInterval;
				if("active" != data.statusEnum){
					logger.logError("App is not Active, please publish the app.");
					return false;
				}else{
					runMethod(application);
				}
			});
		};

		$scope.exploreApp = function(appId){
			$http({
				method: 'POST',
				url : "/exploreApp",
				data : appId
			}).
			success(function(data, status, headers, config){
				$location.path("hiveExplore");
			}).error(function(data){
				logger.logError("Unable to start the app. Error : " + data);
			});
		};

	}]).controller("DataLineageController",["$scope", "$modalInstance", "$http" , "$location","logger" , "$window", "$routeParams","$route", "appsList" ,function($scope, $modalInstance, $http , $location,logger , $window, $routeParams,$route,appsList){
		$scope.appsList = appsList;
		$scope.cancel = function(){
			$modalInstance.dismiss('cancel');
		};
	}]).controller("appViewModalController",["$scope", "$modalInstance", "$http" , "$location","logger" , "dataTypes" , "inputParamTypes" , "appName", "$window", "flowType","$routeParams","$route","CONSTANTS" ,function($scope, $modalInstance, $http , $location,logger , dataTypes , inputParamTypes , appName, $window, flowType,$routeParams,$route,CONSTANTS){
		$scope.dataTypes = dataTypes;
		$scope.inputParamTypes = inputParamTypes;
		$scope.selectedFields = [];
		$scope.setDesabled = false; 
		$routeParams.flowType = flowType;
		$scope.disabledView = false;
		
		$scope.getTodaysDate = function (){
			return Date.now();
		}
		
		if(appName !== null){
			$http.post("/getSingleApplication",appName).success(function(data, status, headers, config){
				$scope.application = data;
				$scope.selectedFields = $scope.application.appCombiner; 
				$scope.setDesabled = true;
			});
		}
		
		$scope.fixedDefaultInputParams = [CONSTANTS.GP_LAST_RUN_DATETIME,CONSTANTS.GP_CURRENT_RUN_DATETIME,CONSTANTS.GP_LAST_SUCCESSFULLY_RUN_DATETIME];
		
		$scope.checkIfDisableView = function(object){
			return _.contains($scope.fixedDefaultInputParams, object.name) && object.type==CONSTANTS.TIMESTAMP && object.inputParamType == CONSTANTS.SYSTEM;
		};
		
		$scope.resestAll = function(){
			$scope.inputData.transpase = {};
			$scope.inputData.name ="";
			$scope.inputData.type =undefined;
		},
		$scope.deleteSelect = function(index){
			var save = $scope.selectedFields; 
			save.splice(index , 1);
			$scope.selectedFields = save;
		},

		$scope.addSelect = function(){

			if($scope.inputData == undefined || $scope.inputData.inputParamType === undefined){
				logger.logError("Please select input param type.");
				return;
			}else if($scope.inputData.name === "" || $scope.inputData.name === undefined){
				logger.logError("Please enter the name.");
				return;
			}

			if($scope.inputData.inputParamType === "FILE")
				$scope.inputData.type = "File";
			if($scope.inputData.type === undefined){
				logger.logError("Please select type.");
				return;
			}
			else if($scope.inputData.name.toLowerCase().indexOf("gp_") !== 0){
				logger.logError("Param name always start with gp_ OR GP_");
				return;
			}else if(($scope.inputData.inputParamType).toLowerCase() === "FILE"){

				if($scope.inputData.transpase === undefined || $scope.inputData.transpase.csv === undefined || $scope.inputData.transpase.csv === ""){
					logger.logError("Please enter fields names");
					return;
				}
				else if($scope.inputData.transpase.delimeter === undefined || $scope.inputData.transpase.delimeter === ""){
					logger.logError("Please enter the delimeter");
					return;
				}

				var val = ($scope.inputData.transpase.csv).split(",");
				var myBool = true;
				for(var i= 0 ;i< val.length ; i++){
					if(val[i].indexOf("[") !== -1 && val[i].indexOf("]") !== -1){
						var type = val[i].substring(val[i].indexOf("[")+1 , val[i].indexOf("]"));
						if(type.toUpperCase() !== "INTEGER" && 
								type.toUpperCase() !== "STRING" 
									&& type.toUpperCase() !== "LONG" && type.toUpperCase() !== "DOUBLE"
										&& type.toUpperCase() !== "DATE"
											&& type.toUpperCase() !== "BOOLEAN"){
							myBool = false;
							break;
						}
					}
					else{
						myBool = false;
						break;
					}
				}
				if(myBool === false){
					logger.logError("Please write csv values in proper format");
					return false;
				}
			}

			var bool = true;
			for(var i = 0 ; i < $scope.selectedFields.length ; i++){
				if(($scope.inputData.name).toUpperCase().indexOf("GP_") === 0){
					if(($scope.inputData.name).toUpperCase() === ($scope.selectedFields[i].name).toUpperCase()){
						bool = false;
						break;
					}
				}else{
					if(("gp_"+$scope.inputData.name).toUpperCase() === ($scope.selectedFields[i].name).toUpperCase()){
						bool = false;
						break;
					}
				}
			}

			if(bool === false){
				logger.logError("Name already exist");
				return false;
			}

			if("INPUT" === $scope.inputData.inputParamType && (undefined === $scope.inputData.defaultValue  || null === $scope.inputData.defaultValue)) {
				if(($scope.inputData.type.toUpperCase() === "DATE" || $scope.inputData.type.toUpperCase() === "TIMESTAMP")  
						&& $scope.inputData.transpase.sysdate === true) {
				} else {
					logger.logError("Please provide default value as per data type you selected");
					return false;
				}
			}


			$scope.selectedFields.push(angular.copy($scope.inputData));
			$scope.inputData = {};
		},


		$scope.createApp = function(application){
			$scope.application = application;
			if($scope.application == undefined || $scope.application.appName == undefined){
				logger.logError("Application name should not start with numeric value. Also it cannot be blank and special characters/white spaces are not allowed.");
				return ;
			}
			var indexValue = $scope.application.appName.indexOf(" ");
			if(indexValue === -1) {
				if($scope.application.description == undefined)
					$scope.application.description = null;
				
				var appViewStatusData = {'appCombiner':$scope.selectedFields,'appName' : $scope.application.appName,'description' : $scope.application.description , 'flowType' : $routeParams.flowType};
				$http({url:"/appViewStatus",method:'POST',data:appViewStatusData}).success(function(data, status, headers, config){

					if(data.appId == null){
						logger.logError("App name already exists");
						return false;
					} else {
						$modalInstance.dismiss('');
						$route.reload();
						
					}
				}).error(function(data, status, headers, config){
					logger.logError(data);
					return false;
				});
			} else {
				logger.logError("Please dont use white space.");
				return;
			}
		},
		$scope.updateApp = function(){
			if($scope.application.appName == undefined){
				logger.logError("Please enter the appName");
				return ;
			}
			var indexValue = $scope.application.appName.indexOf(" ");
			if(indexValue === -1) {
				$scope.application.appCombiner = $scope.selectedFields; 
				$http({
					method: 'POST',
					url : "/appUpdateController",
					data : $scope.application
				}).success(function(data, status, headers, config){
					$modalInstance.dismiss('');	
					//logger.logSuccess("Flow detail updated successfully");
				}).error(function(data, status, headers, config){
					logger.logError(data);
					return false;
				});

				
			} else {
				logger.logError("Please dont use white space.");
				return;
			}
		},
		$scope.cancel = function(){
			$modalInstance.dismiss('cancel');
		};
	}]).controller("appViewInputModalController",["$upload", "$scope", "$modalInstance", "$http" , "$location","logger" , "$modal" , "$log", "comingFrom", "rootId","$routeParams","$rootScope","$filter","CONSTANTS",function($upload, $scope, $modalInstance, $http , $location,logger , $modal , $log, comingFrom, rootId,$routeParams,$rootScope,$filter, CONSTANTS){
		//$scope.data = $modalInstance.application;
		
		$scope.getTodaysDate = function (){
			return Date.now();
		}

		$scope.stream = {"pollInterval" : 0};
		$http.post("/getSingleApplication",$modalInstance.application.appName).success(function(data, status, headers, config){
			$scope.data = data;
			$scope.stream.pollInterval = data.pollInterval;
			$scope.selectedFields = $scope.data.appCombiner;
			$scope.flowType = data.flowType;

			if($scope.selectedFields !== null && $scope.selectedFields !== undefined && $scope.selectedFields.length > 0){
				for(var cnt = 0; cnt < $scope.selectedFields.length; cnt++){
					$scope.selectedFields[cnt].type = $scope.selectedFields[cnt].type.toUpperCase();  
				};
			}

			$scope.dataTypes = $modalInstance.dataTypes;
			$scope.Transpose = [] ;

			$http.post("/getAppSourceList",$scope.data.appId).success(function(data, status, headers, config){
				$scope.appLevelDataSources = _.filter(data , function(val){return val.dataSourceName.indexOf("temp_") !== 0;});;
			});

			$http.post("/getTempDataSourceList",$scope.data.appId).success(function(ChildData, status, headers, config){
				$scope.tempDataSources = ChildData;
			}).error(function(data, status, headers, config){
				logger.logError(ChildData);
			});

			$http({
				method : "post",
				url : "/getLastRunParam",
				data : $scope.data.appId
			}).success(function(data){
				for (var j = 0; j < $scope.selectedFields.length; j++) {
					for(var i = 0 ; i < data.length ; i++){

						if($scope.selectedFields[j].name === data[i].paramName){
							$scope.Transpose[j] = data[i].val;
						}else{
							if($scope.Transpose[j] == undefined || $scope.Transpose[j] == null || $scope.Transpose[j] == "")
								$scope.Transpose[j] = "";
						}
					}
				}

			}).error(function(status){
			});
		});

		$scope.dataTest = {
				"cores" : null,
				"RAM" : null
		};

		$scope.fileUpload = function(file , index , name){
			if(file[0] !== undefined){
				$upload.upload({
					headers: { 'Content-Type': undefined },
					url : "/fileUploadApps",
					file :file[0]
				}).
				success(function(data, status, headers, config){
					$scope.Transpose[index] = data;
				}).error(function(data){
					logger.logError(data);
				});
			}
		},

		$scope.statusCheck = function(){
			$scope.values = [];
			$http.post("/getSingleApplication",$scope.data.appName).success(function(data, status, headers, config){
				if("active" != data.statusEnum){
					logger.logError("App is not Active, please publish the app.");
					return false;
				}else {
					$scope.createApp();
					if (data.flowType != "STREAM")
						$scope.cancel();
				}

			}).error(function(data, status, headers, config){
			});
		},
		$scope.createApp = function(){
			var i = 0;

			if($scope.Priority !== "Custom"){
				$scope.dataTest.cores = null;
				$scope.dataTest.RAM = null;
			}else
				if($scope.Priority == "Custom"){
					if(($scope.dataTest.cores == "" 
						|| $scope.dataTest.cores == undefined
						|| $scope.dataTest.cores == null)){
						logger.logError("Please provide the cores values");
						return false;
					}else if(isNaN($scope.dataTest.cores)){
						logger.logError("Please provide the numbers as cores values");
						return false;
					}

					if(($scope.dataTest.RAM == "" 
						|| $scope.dataTest.RAM == undefined
						|| $scope.dataTest.RAM == null)){
						logger.logError("Please provide the RAM values");
						return false;
					}else if(isNaN($scope.dataTest.RAM)){
						logger.logError("Please provide the numbers as RAM values");
						return false;
					}
				}

			_.each($scope.selectedFields , function(values){
				if(values.name == CONSTANTS.GP_CURRENT_RUN_DATETIME){
					$scope.Transpose[i] = $filter('date')(new Date(), CONSTANTS.GLOABL_DEFAULT_TIMESTAMP);
				}
				
				var object = {
						"appId" : $scope.data.appId,
						"paramName" : values.name,
						"inputParamType" : values.inputParamType,
						"val" : $scope.Transpose[i],
						"type" : values.type
				};

				if(values.inputParamType == "CALCULATED"){
					object.val = values.transpase.dateFormat;
					$scope.Transpose[i] = object.val; 
				}

				if((values.type).toUpperCase() == "FILE"){
					_.each($scope.tempDataSources , function(datas){
						if(values.name == datas.dataSourceName || ($scope.data.appName.toLowerCase() + "_" +values.name) == datas.dataSourceName){
							object.dsId = datas.id;
							if(undefined !== $scope.Transpose[i] && null !== $scope.Transpose[i]){
								datas.fileDataIngesterDetails.serverFilePath = $scope.Transpose[i].substring(0 , $scope.Transpose[i].lastIndexOf("/")+1);
							}
							datas.availableToUse=undefined;
							$http({
								url: "/saveDataSource",
								method: 'Post',
								data : datas
							}).then(function(response) {
								//logger.logSuccess("Field Mapping updated successfully");
							}, 
							function(response) {
								//logger.logError("Something went wrong");
							});
							return ;
						}
					});
				}

				$scope.values.push(object);
				i++;
			});

			var filter = _.reject($scope.selectedFields , function(val){return val.inputParamType === "CALCULATED";});
			var dataTypesFix = _.pluck(filter , 'type');

			if($scope.Transpose[0] == ""){
				$scope.Transpose = [];
			}
			
			$scope.fixedDefaultInputParams = [CONSTANTS.GP_LAST_RUN_DATETIME,	CONSTANTS.GP_CURRENT_RUN_DATETIME,CONSTANTS.GP_LAST_SUCCESSFULLY_RUN_DATETIME];
			
			$scope.checkIfDisableView = function(object){
				return _.contains($scope.fixedDefaultInputParams, object.name) && object.type==CONSTANTS.TIMESTAMP && object.inputParamType == CONSTANTS.SYSTEM;
			};
			
			var flag = true;
			if(($scope.Transpose.length !== $scope.selectedFields.length)){
				var check=false;
				if($scope.selectedFields){
					_.each($scope.selectedFields , function(checkValue){
						check = $scope.checkIfDisableView(checkValue);
					});	
				}
				if(!check){
					logger.logError("Please provide value for parameter");
					return;
				}
			}else {
				for(var i=0 ; i < $scope.Transpose.length ; i++){
					var object = $scope.selectedFields[i];
					if(($scope.Transpose[i] == undefined || $scope.Transpose[i] == null)&&!$scope.checkIfDisableView(object)){
						logger.logError("Please provide all values");
						flag = false;
						return;
					}
				}
			}
			if(flag !== false){
				for(var i=0 ; i < $scope.Transpose.length ; i++){
					if($scope.selectedFields[i].inputParamType != "CALCULATED" && 
							(($scope.selectedFields[i].type).toUpperCase() === "INTEGER" || 
									($scope.selectedFields[i].type).toUpperCase() === "LONG" || 
									($scope.selectedFields[i].type).toUpperCase() === "DOUBLE") 
									&& isNaN($scope.Transpose[i])){
						logger.logError("Please Enter the number field in it only");
						$scope.Transpose[i] = "";
						return;
					}
				};

				$scope.descideToRunApp();

				if($scope.runDs.dataSourceIds.length > 0){
					var modalInstance;
					modalInstance = $modal.open({
						size : "lg",
						templateUrl: "/views/template/runPipeLineTemplate.html",
						controller: "runPipeLineController",
						resolve : {
							store : function(){
								return $scope.runDs;
							}
						}
					}),modalInstance.result.then(function(list) {
						_.each(list , function(val){
							$scope.values.push(val);
						});

						$scope.runApp(comingFrom , $scope.values, rootId);
					}, function() {
						$log.info("Modal dismissed at: " + new Date);
					});
				}else{
					$scope.runApp(comingFrom, $scope.values, rootId);
				}
			}
		},
		$scope.descideToRunApp = function(){
			$scope.runDs = {
					"dataSourceIds" : []	
			};
			_.each($scope.appLevelDataSources , function(val){
				if(val.checkbox == true)
					$scope.runDs.dataSourceIds.push(val.dataSourceId);
			});

		},
		$scope.runApp = function(comingFrom , values, rootId){
			if(comingFrom == "RUNAPP"){
				var appId = $scope.data.appId;
				$http({
					method: 'POST',
					url : "/startAppJob",
					data : appId 
				}).
				success(function(data, status, headers, config){
					logger.logSuccess("Started the app");
					$modalInstance.dismiss('');
				}).error(function(data){
					logger.logError("Unable to start the app. Error : " + data);
					return false;
				});
			}else if(comingFrom == "SPARK"){

				var inputData = { 'values' :values, 'applicationId' : $scope.data.appId, 'selectedBoostId': $scope.selectedBoostId,'pollInterval' : $scope.stream.pollInterval};
				$http({
					method: 'POST',
					url : "/startAppSparkJob" ,
					data : inputData
				}).
				success(function(data, status, headers, config){
					logger.logSuccess("Started the app on Spark.");
					$modalInstance.close('');
//					$route.reload();
				}).error(function(data){
					logger.logError(data);
					return false;
				});
			}else if(comingFrom == "RESTART" && rootId!=undefined){
				$scope.appName  = $scope.data.appName;
				
				var reqDataObject = {'applicationId' : $scope.data.appId, 'baapInstId' : rootId,'inputParam' : values};
				$http({
					method: "POST",
					url: "/restartApp",
					data : reqDataObject 
				}).success(function(data, status, headers, config) {
					logger.logSuccess("App successfully restarted.");
				}).error(function(data){
					logger.logError("Unable to restart the app. Error : " + data);
					return false;
				});
			}
		},
		$scope.cancel = function(){
			$modalInstance.dismiss('cancel');
		};
	}]).controller("saveAsController",["messg" , "ind" , "$scope", "$modalInstance", "$http" , "$location","logger" , "list", "headerTitle", "yesBtnLable", "noBtnLable", function(messg , ind,$scope, $modalInstance, $http , $location,logger , list, headerTitle, yesBtnLable, noBtnLable){
		$scope.list = list;
		$scope.indicator = ind;
		$scope.headerTitle = headerTitle;
		$scope.yesBtnLable = yesBtnLable;
		$scope.noBtnLable = noBtnLable;
		if($scope.list == null || $scope.list == undefined)
			$scope.list = [];

		$scope.massage = messg;
		$scope.selectMethod = function(val){
			if(val === "yes"){
				$modalInstance.close(val);
			}if(val === "no"){
				$modalInstance.close(val);
			}else{
				$modalInstance.dismiss('');
			}
		};
	}]).controller('createCloneController', ["application", "$scope", "$http", "$modal", "$log" ,"logger" , "$routeParams", "$interval","$modalInstance", "$location",  function(application, $scope,$http,$modal,$log,logger, $routeParams, $interval,$modalInstance, $location) {
		$scope.application = application;

		$scope.createClone = function(newName){

			var appViewDatas = {'oldAppId' : $scope.application.appId , 'newAppName': newName};
			$http({
				method : "POST",
				url : "/createFlowsclone",
				data : appViewDatas
			
			}).success(function(data){
				logger.logSuccess("New clone created successfully");
				$modalInstance.dismiss("cancel");
				

			}).error(function(status){
				logger.logError(status)
			});
		};

		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};

	}]).controller("jobScheduleController" , ["$scope" ,"$filter" , "$http", "$location" ,"$modal", "logger","$log", "$route", "application","$modalInstance", function($scope, $filter , $http, $location,$modal ,logger, $log, $route, application, $modalInstance){
		$scope.application = application;
		$scope.userName = localStorage.getItem("userName");
		$scope.actionStatus = "";
		$http({
			method : "POST",
			url : "/getScheduleJob",
			data : $scope.application.appId
		}).success(function(data){
			$scope.scheduleJobsList=data;
		}).error(function(status){
			logger.logError(status)
		});
		$http.get("/getScheduleJobMaxLimit").success(function(data, status, headers, config){
			$scope.scheduleJobMaxLimit = data;
		});

		$scope.cron = [{"value" : "", "name" : "None"},
			{"value" : "*/1 * * * *", "name" : "1m"},
			{"value" : "*/5 * * * *", "name" : "5m"},
			{"value" : "* */1 * * *", "name" : "1h"},
			{"value" : "* */3 * * *", "name" : "3h"},
			{"value" : "* */6 * * *", "name" : "6h"},
			{"value" : "* */12 * * *", "name" : "12h"},
			{"value" : "59 23 * * *", "name" : "1d"}];

		$scope.selectScheduleString = function(time) {
			$scope.cronString = time;
		};


		$scope.addScheduleJob = function () {
			for (var i = 0; i < $scope.scheduleJobsList.length; i++) {
				var job = $scope.scheduleJobsList[i];
				if(job !== undefined){

					if(job.cronExpression==$scope.cronString){
						logger.logError("Job is already scheduled.");
						return false;
					}
				}
			}

			if(undefined == $scope.cronString){
				logger.logError("You didn't add cron expression.");
				return false;
			}

			var schedule = {
					"id" : 0,
					"appId" : $scope.application.appId,
					"cronExpression" : $scope.cronString,
					"createdBy" : undefined
			}
			
			$scope.actionStatus = ($scope.actionStatus == "deleted") ? "updated" : "added";
			
			$scope.scheduleJobsList.push(schedule);
		};

		$scope.saveCrownJob = function (schedule) {
			
			var scheduleJobData = {'scApplicationList' :$scope.scheduleJobsList , 'appId': $scope.application.appId};
			$http({
				method : "POST",
				url : "/scheduleJob",
				data: scheduleJobData
			}).success(function(data){
				if ($scope.actionStatus){
					logger.logSuccess("Operations " + $scope.actionStatus + " successfully.");
				}
				if($scope.scheduleJobsList.length==0){
					jQuery("#"+$scope.application.appName).removeClass("sched_enable");
					jQuery("#"+$scope.application.appName).addClass("actionMenu");
				}else{
					jQuery("#"+$scope.application.appName).removeClass("actionMenu");
					jQuery("#"+$scope.application.appName).addClass("sched_enable");

				}
				$modalInstance.dismiss("cancel");
			}).error(function(status){
				logger.logError(status)
			});
		}

		$scope.deleteSelect = function(index) {
			$scope.actionStatus = ($scope.actionStatus == "added") ? "updated" : "deleted";
			
			$scope.scheduleJobsList.splice(index , 1);
		}
		$scope.cancel = function() {
			$scope.actionStatus = "";
			$modalInstance.dismiss("cancel");
		};
	}]).directive('fileModel', ['$parse', function ($parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.bind('change', function(){
					$parse(attrs.fileModel).assign(scope,element[0].files)
					scope.$apply();
				});
			}
		};
	}]).controller("importFlowController" , ["$log","$modal", "$filter","$scope", "$http" , "$location","$routeParams", "$route", "logger","$modalInstance", "$rootScope",function($log, $modal , $filter,$scope, $http , $location,$routeParams,$route, logger,$modalInstance, $rootScope){
		$scope.classForImport = "y";
		$scope.next = "n";
		$scope.click = "n";  
		$scope.importedFlowDetails = undefined;
		$scope.disabled=false;
		$scope.okBtnTxt = function(next){
			return (next=="y")?"Cancel":"OK";
		} 
		$scope.cancelBtnTxt = function(){
			return "Cancel";
		}	
		$scope.nextPrevBtn = function(next){
			return (next=="y")?"Back":"Next";	
		}
		$scope.selectedFile = function(file){
			var absoluteFilePath=file.value;
			if(absoluteFilePath!= undefined || absoluteFilePath!=null){
				var fileType=absoluteFilePath.split(".");
				if(fileType != null){
					if(fileType[1]!="json"){
						logger.logError("Please select .json file only");
						$scope.disabled=true;
					}else{
						$scope.disabled=false;
					}
				}
			}	
		}
		
		$scope.importFlowStatus = function(){
			$rootScope.graphDataFromImport = undefined;
			if (undefined === $scope.files 
					|| null === $scope.files 
					|| $scope.files.length === 0) {
				logger.logError("Please choose flow file to import.");
				return;
			}
			
			$scope.next = ($scope.next=="y")?"n":"y";
			$scope.click = "y";
			if($scope.click == "y" && $scope.next == "y"){
				for (var i = 0; i < $scope.files.length; i++) {
					var file = $scope.files[i];
					var size =$scope.files[i].size;
					var fd = new FormData();
					fd.append("file", file);
					$http({
						method: 'POST',
						url: '/importFlowFileStatus',
						headers: {'Content-Type': undefined},
						data: fd,
						transformRequest: angular.identity
					})
					.success(function(data, status) {
							$scope.click = "n";
							$scope.appId  = data.appId;
							$scope.importedAppId  = data.appId;
							if(size < 5000000 ){
								$scope.importedFlowDetails  = data.application;
								$rootScope.graphDataFromImport = data;
							} else {
								$scope.sampleCanvasGraphData ={"application" :  {"canvas_graphData" : angular.copy(data.application.canvas_graphData)} , "appId" : angular.copy(data.appId)};
								_.each($scope.sampleCanvasGraphData.application.canvas_graphData.nodes, function(value){
									value.datasource = undefined;
									value.existingDataSource = undefined;
									value.appsnode = undefined;
									value.existingAppsNode = undefined;
									value.processInfo = undefined;
								});
								data = undefined;
								$rootScope.graphDataFromImport = $scope.sampleCanvasGraphData;
							}

					}).error(function(status){
						$scope.click = false;
						logger.logError("Unable to parse json file")
						$scope.next = "n";
					});
				}
			}
		};
		
		$scope.importFlow = function(){
			$scope.click = true;
			if($scope.click){
				for (var i = 0; i < $scope.files.length; i++) {
					var file = $scope.files[i];
					var fd = new FormData();
					fd.append("appId", $rootScope.graphDataFromImport.application.id); 
					fd.append("file", file);
					$http({
						method: 'POST',
						url: '/importFlowFile',
						headers: {'Content-Type': undefined},
						data: fd,
						transformRequest: angular.identity
					})
					.success(function(data, status) {
						$scope.click = "n";
						$rootScope.refreshFlowList = true;
						logger.logSuccess("Flow Imported successfully");
						$scope.dissmissPopup();
					}).error(function(status){
						$scope.click = "n";
						if(status || status!=""){
							logger.logError(status)	
						}
					});
				}
			}
		};
		$scope.dissmissPopup = function(){
			$modalInstance.dismiss('cancel');
		};
	
	}]);
}).call(this);;
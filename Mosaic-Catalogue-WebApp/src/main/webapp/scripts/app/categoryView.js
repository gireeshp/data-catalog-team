(function(){
	"use strict";
		var discover = angular.module("app.category",['ui.bootstrap']);
		
		discover.controller("categoryTableCtrl" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll", "$timeout", "$interval",  
			function($anchorScroll, $filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,$timeout, $interval){
			$scope.isFirstLoad = true;
			$interval(function() {
				if($scope.isFirstLoad  ||$rootScope.instanceSaved){
					$scope.getAllCategoryAndSubCats();
				}
			}, 1000);
			
			$scope.getAllCategoryAndSubCats = function(){
				$http({
					method : "get",
					url : "/getAllActiveCategorySubCatMaster"
				}).success(function(data){
					if(data){
						$scope.isFirstLoad = false;
						return $scope.stores = data,	
						$scope.searchKeywords = "", 
						$scope.filteredStores = [],
						$scope.row = "", 
						$scope.select = function(page) {
							$scope.pageNum = page;
							var end, start;
							return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.connections = $scope.filteredStores.slice(start, end);
						}, 
						$scope.onFilterChange = function() {
							return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
						}, $scope.onNumPerPageChange = function() {
							return $scope.select(1), $scope.currentPage = 1;
						}, $scope.onOrderChange = function() {
							return $scope.select(1), $scope.currentPage = 1;
						}, $scope.search = function() {
							return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
						}, $scope.order = function(rowName) {
							return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
						}, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
						$scope.numPerPage = $scope.numPerPageOpt[4], 
						$scope.currentPage = 1, 
						$scope.connections = [],
						($scope.init = function() {
							return $scope.search(), $scope.select($scope.currentPage);
						})();
					}
				}).error(function(data){
					logger.logError(data);
				});
			};
			
			$scope.addNewCatOrSubCat = function(connectionConfig){
				var modalInstance;
				modalInstance = $modal.open({
					size: "md",
		            templateUrl: "/views/template/addNewCatOrSubCatModal.html",
		            controller: "addNewCatOrSubCatCtrl",
		            resolve : {
		            	listOfConnections : function(){
		            		return $scope.stores;
		            	},
		            	config : function(){
		            		return angular.copy(connectionConfig);
		            	},
		            	disable : function(){
		            		return false;
		            	}
		            }
		            	
		         }),modalInstance.result.then(function() {
			    }, function() {
		             $log.info("Modal dismissed at: " + new Date);
		         });
			};
			
			$scope.viewCategoryORSubCat = function(connectionConfig){
				var modalInstance;
				modalInstance = $modal.open({
					size: "md",
		            templateUrl: "/views/template/addNewCatOrSubCatModal.html",
		            controller: "addNewCatOrSubCatCtrl",
		            resolve : {
		            	listOfConnections : function(){
		            		return $scope.stores;
		            	},
		            	config : function(){
		            		return angular.copy(connectionConfig);
		            	},
		            	disable : function(){
		            		return true;
		            	}
		            }
		            	
		         }),modalInstance.result.then(function() {
			    }, function() {
		             $log.info("Modal dismissed at: " + new Date);
		         });
			};
			$scope.deleteCategoryOrSubCat = function(configData){
				$scope.bkpOfObj = angular.copy(configData);
				delete $scope.bkpOfObj.createdByName;
				delete $scope.bkpOfObj.modifiedByName;
				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/CategoryOrSubCateDeleteModal.html",
					controller: "deleteCatOrSubCatCtrl",
					size: "sm",
					resolve : {
		            	tag : function(){
		            		return angular.copy($scope.bkpOfObj);
		            	}
		            }					
				}),modalInstance.result.then(function() {
					$http({
						url:"/deleteFromCategoryAndSubCat",
						method: "POST",
						data : $scope.bkpOfObj
					}).success(function(data){
						logger.logSuccess("Deleted successfully");
						$route.reload();
					}).error(function(data){
						logger.logError(data);
					});
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			};
			
			$scope.editTag = function(connectionConfig){
				var modalInstance;
				modalInstance = $modal.open({
					size: "md",
		            templateUrl: "/views/template/addNewTagModal.html",
		            controller: "createNewCatOrSubCat",
		            resolve : {
		            	listOfConnections : function(){
		            		return $scope.stores;
		            	},
		            	config : function(){
		            		return angular.copy(connectionConfig);
		            	},
		            	disable : function(){
		            		return false;
		            	}
		            }
		            	
		         }),modalInstance.result.then(function() {
			    }, function() {
		             $log.info("Modal dismissed at: " + new Date);
		         });
			};
		}]);
		
		discover.controller("addNewCatOrSubCatCtrl" , ["disable","config","listOfConnections","$filter","$anchorScroll","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger" , "$modalInstance", function(disable, config , listOfConnections , $filter, $anchorScroll,$route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $modalInstance){
		$scope.disabled = disable;
		$scope.disableOnUpdate = false;
		
		if(null!=listOfConnections && undefined !=listOfConnections){
			$scope.list = listOfConnections;
		}
		$scope.category = _.filter($scope.list, function(item){
			return item.parentId == 0; 
		});
		
		if(config != null && config){
			$scope.categoryObject = config;
			if(!$scope.disabled){
				$scope.disableOnUpdate = true;
			}
			$scope.editabled = "n";
			$scope.typeOfField = $scope.categoryObject.parentId == 0 ?'category':'subCategory';
			
			if($scope.categoryObject.parentId != 0){
				$scope.selectedCategoryName = _.find($scope.list, function(item){ return  item.id==$scope.categoryObject.parentId}).catOrSubCatName;
			}   
		}else{
			$scope.editabled = "y";
			$scope.typeOfField = 'category';
			$scope.categoryObject={
					id:0
			};
			$scope.selectedCategoryName = {};
		} 
		
		
		$scope.onClickRadio= function(check){
			$scope.categoryObject.parentId = check=='category'?null:0;
		}
		
		$scope.onSelected = function (selectedItem) {
			if(undefined!=selectedItem && 
					undefined!=selectedItem.catOrSubCatName){
				$scope.categoryObject.parentId = selectedItem.id;
			}	
		}
		
		var urlToCall = "/insertCategoryOrSubCategoy";
		$scope.createTag = function(){
			if($scope.typeOfField == "category"){
				$scope.categoryObject.parentId = 0;
			}
			
			if($scope.categoryObject== undefined || $scope.categoryObject == null){
				logger.logError("Please provide the required values");
				return false;
			}else if($scope.categoryObject.parentId ==0 && $scope.typeOfField == "subCategory"){
				logger.logError("Please select category");
				return false;
			}else if($scope.categoryObject.catOrSubCatName == null || $scope.categoryObject.catOrSubCatName == undefined || $scope.categoryObject.catOrSubCatName == ""){
				logger.logError($scope.categoryObject.parentId == 0  ? "Please provide a Category name":"Please provide a SubCategory name");
				return false;
			}else if($scope.categoryObject.shortDescription == null || $scope.categoryObject.shortDescription == undefined || $scope.categoryObject.shortDescription == ""){
				logger.logError("Please enter short description");
				return false;
			}
			
			delete $scope.categoryObject.createdByName;
			delete $scope.categoryObject.modifiedByName;
			
			
			$http({
				method : "post",
				url : urlToCall,
				data : $scope.categoryObject
			}).success(function(data){
				logger.logSuccess(($scope.categoryObject.parentId==0? "Category" : "SubCategory") + " saved successfully");
				$modalInstance.dismiss("cancel");
				$route.reload();
			}).error(function(data){
				logger.logError(($scope.categoryObject.parentId==0? "Category with " : "SubCategory with " ) + data);
			});
		};
		
		$scope.updateTag = function(){
			$scope.createTag();
		}; 
		
		$scope.cancel = function(){
			$modalInstance.dismiss("cancel");
		};
	}]).controller('deleteCatOrSubCatCtrl', ["$scope", "$http", "$modal", "$log" ,"logger" , "$routeParams", "$interval","$modalInstance","tag",  function($scope,$http,$modal,$log,logger, $routeParams, $interval,$modalInstance,tag) {
		$scope.indicatorDelete = true;
	
		$scope.deleteSelectedAppDs = function(){
			if($scope.deleteds.deleteNode != undefined && $scope.deleteds.deleteNode.toLowerCase() === "delete"){
				$modalInstance.close();
			}else{
				logger.logError("Please type delete.....");
				$scope.deleteds.deleteNode = "";
				return false;
			}
		};

		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};

	}]);
}).call(this);
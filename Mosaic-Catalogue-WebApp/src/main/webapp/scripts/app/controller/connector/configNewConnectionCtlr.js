(function(){
	var connections= angular.module("app.connections");
	
	//filter for key values in object
	connections.filter('propertyFilter', function($log) {
		 return function(items, props) {
		    var out = [];
		    if (angular.isArray(items)) {
		    items.forEach(function(item) {
		        var itemMatches = false;
		        var keys = Object.keys(props);
		        var optionValue = '';
		        for (var i = 0; i < keys.length; i++) {
		             optionValue = item[keys[i]] ? optionValue + item[keys[i]].toString().toLowerCase().replace(/ /g, '') : '';
		        }
		        for (var j = 0; j < keys.length; j++) {
		            var text = props[keys[j]].toLowerCase().replace(/ /g, '');
		            if (optionValue.indexOf(text) !== -1) {
		               itemMatches = true;
		               break;
		            }
		        }
		        if (itemMatches) {
		            out.push(item);
		        }
		        });
		        } else {
		            // Let the output be the input untouched
		            out = items;
		        }
	
		        return out;
		    };
		})
		
	connections.controller("configNewConnectionCtlr",["$scope","getterSetterFactory","connectionService","$location","usersService","$cookieStore","glossaryService","logger","ivhTreeviewBfs","$timeout",  function($scope,getterSetterFactory,connectionService,$location,usersService, $cookieStore, glossaryService, logger, ivhTreeviewBfs, $timeout){
	
		$scope.connectionSources = {};
		$scope.connectorTypeId = sessionStorage.getItem("connectorTypeId");
		$scope.connectorTypeName = sessionStorage.getItem("connectorTypeName");
		$scope.connectorSTId = sessionStorage.getItem("connectorSTId");
		$scope.connectorSTName = sessionStorage.getItem("connectorSTName");
		$scope.conId = getterSetterFactory.getConnectionDetails();
		$scope.experts = [];
		$scope.connectionSources.expert = $cookieStore.get("userId");
		$scope.tags = [];
		$scope.connectionSources.tags = [];
		$scope.subSteps = 0;
		$scope.viewHeight = null;
		$scope.isDsFetch = false;
		$scope.nonDBTypeConnectorArray = ['FTP SERVER','SFTP SERVER','HDFS','BLOB','AMAZONS3'];
		$scope.profilingSamplingHide = ['COGNOS']
		$scope.isPublishMethodCalled = false;
		$scope.loadTime = false;
		$scope.selectedNodes = [];
		
		if(Object.keys($scope.conId).length < 1){
			$scope.conId = sessionStorage.getItem("connectionDetails") && JSON.parse(sessionStorage.getItem("connectionDetails"));
		}
		
		$scope.$emit('updateTimelinePublishState', 0);
		if(!$scope.connectorTypeId || !$scope.connectorTypeId || !$scope.connectorTypeName || !$scope.connectorSTName || !$scope.connectionSources.expert){
			$scope.$emit('updateTimelineState', 2);
		}
		
		
		$scope.connectionSources.sampleRowCount = parseInt(10);
		var connectionDetails = {
				"sourceId":parseInt($scope.connectorTypeId),
				"subSourceId":parseInt($scope.connectorSTId),
				"connectionId":parseInt($scope.conId.connectionId)
		}
		
		
		// assign selected expert user
		$scope.setSelectedExpert = function(expertObj){
			$scope.connectionSources.expert = expertObj.unqUserId;
		}
		        
		$scope.registerConnection  = function(configConnection){
			$scope.selectedNodes = $scope.getIvhTreeSelectedNodes(); 
			if($scope.selectedNodes.length > 0){
        		var isAtleastOnLeafeNode =  $scope.getIvhTreeLeafeNodes();
        		if(isAtleastOnLeafeNode.length > 0){
        			//check for FTP connector
        			if($scope.nonDBTypeConnectorArray.indexOf($scope.connectorTypeName) > -1){
        				$scope.checkDsObjectsIsSelectAndDelimiterAddedMoveNext(configConnection);
        			}else{
        				$scope.checkDsObjectsIsSelectAndMoveNext(configConnection);
        			}
        		}else{
        			logger.logError("Select atleast one leaf node to publish.");
        		}
        	}else{
        		logger.logError("Select datasource's.");
        	}
		}
		
        $scope.connectionSources.customFilterSelected = function (node) {
            return $scope.selecteTreeNode = node.selected && !node.isPublished;
        };
        
        $scope.connectionSources.customFilterDelimiterSelected = function (node) {
            return $scope.selecteTreeNode = node.selected && !node.isPublished && node.isDsNode;
        };
        
        $scope.checkDuplicateDatasource = function(configConnection){
        	connectionService.checkDuplicateDatasources($scope.selectedNodes).then(function(result){
				if(typeof result.SUCCESS != "undefined"){
					$scope.publish(configConnection);
				}else if(result && typeof result.error != "undefined"){
					logger.logError(result.error );
				}else{
					logger.logError(result.FAIL.toString() + " datasource's are already exists.");
					$scope.deSelectIvhTreeNodes(result.FAIL);
				}
			});
        }
        
        $scope.publish = function(configConnection){
        	$scope.isPublishMethodCalled = true;
			connectionService.publishDataSources(configConnection).then(function(result){
				if(typeof result.SUCCESS != "undefined"){
					$scope.loadTime = true;
					$timeout(function() {
						$scope.loadTime = false;
						logger.logSuccess("Datasources pulished successfully.");
						$location.path("/discover");
		        	}, 10000); 
				}else if(result && typeof result.error != "undefined"){
					logger.logError(result.error );
				}else{
					$scope.isPublishMethodCalled = false;
					$scope.loadTime = false;
					logger.logError(result.FAIL.toString() + " datasource's are already exists.");
				}
			});
        }
        
        //method for setp1 select datasources for publish
        $scope.checkDsObjectsIsSelectAndMoveNext = function(configConnection){
        	$scope.checkDuplicateDatasource(configConnection);
        }
        
        //method for step2 where user add delimeters to leaf nodes
        $scope.checkDsObjectsIsSelectAndDelimiterAddedMoveNext = function(configConnection){
        	var nodes = $scope.getIvhTreeLeafeWithoutDelimiterNodes();
        	if(nodes.length === 0){
        		$scope.checkDuplicateDatasource(configConnection);
        	}else{
        		logger.logError("Delimiter's required.");
        	}
        }
                
        //get selected nodes which are not published
        $scope.getIvhTreeSelectedNodes = function(){
        	var selectedNodes = [];
        	ivhTreeviewBfs($scope.connectionSources.dataNode, function(node) {
        		if(node.selected &&  !node.isPublished) {
        		   selectedNodes.push(node)
        		}
    	    });
        	return selectedNodes;
        }
        
        //get non published selected publishable datasources
        $scope.getIvhTreeLeafeNodes = function(){
        	var selectedNodes = [];
        	ivhTreeviewBfs($scope.connectionSources.dataNode, function(node) {
        		if(node.selected && node.isDsNode && !node.isPublished) {
        		   selectedNodes.push(node)
        		}
    	    });
        	return selectedNodes;
        }
        
        //check and get list of non published ,selected, publishable datasources and 
        $scope.getIvhTreeLeafeWithoutDelimiterNodes = function(){
        	var selectedNodes = [];
        	ivhTreeviewBfs($scope.connectionSources.dataNode, function(node) {
        		if(node.selected && node.isDsNode && !node.isPublished && !node.delimiter) {
        		   selectedNodes.push(node)
        		}
    	    });
        	return selectedNodes;
        }
        
        //de select nodes
        $scope.deSelectIvhTreeNodes = function(existingNodes){
        	ivhTreeviewBfs($scope.connectionSources.dataNode, function(node) {
        		if(existingNodes.indexOf(node.label) > -1) {
        			node.selected = false;
        		}
    	    });
        }
        
       $scope.setViewHeight = function(){
        	$timeout(function(){
        		$scope.viewHeight = angular.element("#content").height();
        	}, 100);	
        }();
        
        
		//get experts (user) list
		$scope.getAllUserList = function(){
			usersService.getAllUsers().then(function(result){
				if(result && typeof result.error == "undefined"){
					$scope.experts = result;
				}
				
			});
		}();
		
		//get tag list
		$scope.getAllTags = function(){
			glossaryService.getTagList().then(function(result){
				if(result && typeof result.error == "undefined"){
					//$scope.tags = result;
					_.each(result, function(tag){
						$scope.tags.push(tag.name);
					});
				}	
			});
		}();
        
		//get connection schema for selected connection
		$scope.getAllSchemaForConnection = function(){
			$scope.isDsFetch = true;
			$timeout(function(){
				connectionService.getAllSchema(connectionDetails).then(function(result){
					if(result && typeof result.connectionSources != "undefined"){
						$scope.isDsFetch = false;
						$timeout(function(){
							$scope.connectionSources.connectionSources = result.connectionSources;
							$scope.connectionSources.dataNode = result.dataNode;
						}, 400);
					}else if(result && typeof result.error != "undefined"){
						logger.logError(result.error );
					}
					
					
				});
			}, 1000)
		}();
		
	}]);
	
	
}).call(this);
(function(){
	var connections= angular.module("app.connections");
	
	connections.controller("connectionsViewCtlr",["$scope","$location","logger","$compile","connectionService","getterSetterFactory","$filter" ,function($scope,$location,logger, $compile,connectionService,getterSetterFactory,$filter){
			$scope.connectorTypeId = sessionStorage.getItem("connectorTypeId");
			$scope.connectorTypeName = sessionStorage.getItem("connectorTypeName");
			$scope.connectorSTId = sessionStorage.getItem("connectorSTId");
			$scope.connectorSTName = sessionStorage.getItem("connectorSTName");
			sessionStorage.setItem("conncetionName", "");
			sessionStorage.setItem("connectionDetails","");
			$scope.isConnectionCreateEdit = false;
			$scope.connectionsList = [];
			$scope.viewHeight = null;
			
			$scope.datasourceImages = {
				'MYSQL':'../styles/images/publish_catalog/rdms/mysql-publish.png',
				'ORACLE':'../styles/images/publish_catalog/rdms/oracle-publish.png',
				'POSTGRES':'../styles/images/publish_catalog/rdms/pl-publish.png',
				'POSTGRESQL':'../styles/images/publish_catalog/rdms/pl-publish.png',
				'IBMDB2':'../styles/images/publish_catalog/rdms/db2.png',
				'SQL_SERVER':'../styles/images/publish_catalog/rdms/sqlserver.png',
				
				'MONGODB':'../styles/images/publish_catalog/nosql/mongodb.png',
				'HBASE':'../styles/images/publish_catalog/nosql/hbase.png',
				'CASSANDRA':'../styles/images/publish_catalog/nosql/cassandra.png',
				'RADIS':'../styles/images/publish_catalog/nosql/redis.png',
				'NEO4J':'../styles/images/publish_catalog/nosql/neo4j.png',
				
				'AMAZONS3':'../styles/images/publish_catalog/cloud/s3.png',
				'BLOB_DATA_SOURCE':'../styles/images/publish_catalog/cloud/blob-publish.png',
				'BLOB':'../styles/images/publish_catalog/cloud/blob-publish.png',
				'DROPBOX':'../styles/images/publish_catalog/cloud/dropbox.png',
				
				'FTP_DATA':'../styles/images/publish_catalog/ftp/ftp.png',
				'CSV_DATA':'../styles/images/publish_catalog/ftp/csv.png',
				'TCV_DATA':'../styles/images/publish_catalog/ftp/tsv.png',
				'FIXED_WIDTH_DATA':'../styles/images/publish_catalog/ftp/fixedWidth.png',
				'SFTP':'../styles/images/publish_catalog/ftp/sftp.png',
				
				'COGNOS_DATA_SOURCE':'../styles/images/publish_catalog/bi/cognos-publish.png',
				
				'HDFS':'../styles/images/publish_catalog/bigdata/haddop.png',
				'HIVE':'../styles/images/publish_catalog/bigdata/hive.png',
			}
			
			$scope.datasourceAltNames =  {
				'MYSQL':'MYSQL',
				'ORACLE':'ORACLE',
				'POSTGRESQL':'POSTGRESQL',
				'POSTGRES':'POSTGRES',
				'IBMDB2':'IBMDB2',
				'SQL_SERVER':'SQL_SERVER',
				
				'MONGO_DB':'MONGO_DB',
				'HBASE':'HBASE',
				'CASSANDRA':'CASSANDRA',
				'RADIS':'RADIS',
				'NEO4J':'NEO4J',
				
				'AMAZONS3':'AMAZONS3',
				'BLOB_DATA_SOURCE':'BLOB',
				'BLOB':'BLOB',
				'DROPBOX':'DROPBOX',
				
				'FTP_DATA':'FTP',
				'CSV_DATA':'CSV',
				'TCV_DATA':'TCV',
				'FIXED_WIDTH_DATA':'FIXED WIDTH',
				'SFTP':'SFTP',
				
				'COGNOS_DATA_SOURCE':'COGNOS',
				
				'HDFS':'HDFS',
				'HIVE':'HIVE',
			}
			
			// redirect to connectors if any of the session storage param is undefined, null or blank
			if(!$scope.connectorSTId || !$scope.connectorTypeName || !$scope.connectorSTId || !$scope.connectorSTName){
				$location.path("/subconnectors");
			}
			
			//function to select connection  to publish
			$scope.selectConnection = function (event, connectionDetails){
				event.stopPropagation();
				event.preventDefault();
				getterSetterFactory.setConnectionDetails(connectionDetails);
				sessionStorage.setItem("connectionDetails",JSON.stringify(connectionDetails));
				sessionStorage.setItem("conncetionName",connectionDetails.conncetionName);
				$location.path("/publish");
			}
			
			//function to delete exiting connection
			$scope.deleteConnection = function (event, connectionDetails){
				var deleteObj = {
					'connectionId':	connectionDetails.connectionId,
					'subSourceId': parseInt($scope.connectorSTId)
				}
				event.stopPropagation();
				event.preventDefault();
				swal({
					 title: "Delete connection",
					 text: "Are you sure? Do you want to delete "+connectionDetails.conncetionName+" connection!",
					 type: "warning",
				     showCancelButton: true,
				     confirmButtonText: "Yes, delete it!",
				     cancelButtonText: "No, cancel please!",
				     closeOnConfirm: false,
				     closeOnCancel: true,
				     showSpinner: true,
			         showCancelButton: true,
				}, function () {
					connectionService.deleteConnection(deleteObj).then(function(result){
						swal.close()
						if(result && typeof result.FAIL != "undefined"){
							logger.logError(result.FAIL);
						}else if(result && typeof result.error != "undefined"){
							logger.logError(result.error);
						}else{
							logger.logSuccess("Connection deleted successfully!");
						}
						$scope.$emit('connectionDeleted', deleteObj)
					});
				});
			}
			
			var	reqParam ={"sourceId":parseInt($scope.connectorTypeId),"subSourceId":parseInt($scope.connectorSTId)};
			
			// on new connection added it receive added connection config json
			$scope.$on('newConnectionAdded', function(event, object){
				$scope.connectionsList.push(object);
			});
			
			// on new connection added it receive added connection config json
			$scope.$on('connectionDeleted', function(event, deleteObj){
				$scope.tempConnectionsList = _.without($scope.connectionsList, _.findWhere($scope.connectionsList, {
					connectionId: deleteObj.connectionId
				}));
				$scope.connectionsList = $scope.tempConnectionsList;
			});
			
			
			//fetch connections 
			$scope.fetchConnections = function(){
				connectionService.getConnectionsList(reqParam).then(function(result){
					//$scope.connectionsList = result;
					if(result && typeof result.sourceId != "undefined"){
						$scope.sourceId =result.sourceId;
						$scope.connectionType = result.connectionType;
						$scope.connectionSubSources = result.connectionSubSources[0].subSourceId;
						$scope.subConnectionType = result.connectionSubSources[0].subConnectionType;
						$scope.connectionsList = result.connectionSubSources[0].connectionConfig;
					}else if(result && typeof result.error != "undefined"){
						logger.logError(result.error);
					}
					
				});
			}();
			
			
			//create edit connection side panel
			$scope.createConnection = function(){
				$scope.isConnectionCreateEdit = !$scope.isConnectionCreateEdit;
			}
			
			$scope.$on('cancelCreateConnection', function(event, flag){
				$scope.isConnectionCreateEdit = flag;
			})
			
			$scope.setViewHeight = function(){
				$scope.viewHeight = angular.element("#content").height();
				$scope.isConnectionCreateEdit = false;
			}
		}]);
}).call(this);
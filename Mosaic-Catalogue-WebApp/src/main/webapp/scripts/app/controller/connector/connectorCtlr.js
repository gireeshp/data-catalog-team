(function(){
	var connector= angular.module("app.connector");
	
		connector.controller("connectorCtlr",["$scope","connectorService","$location","$routeParams","$filter",
					function($scope,connectorService,$location,$routeParams,$filter){
		
			$scope.viewHeight = null;
			
			// connector service to list connector
			$scope.fetchConnectors = function(){
				sessionStorage.setItem("connectorTypeName","");
				sessionStorage.setItem("connectorTypeId", "");
				sessionStorage.setItem("connectorSTId", "");
				sessionStorage.setItem("connectorSTName", "");
				sessionStorage.setItem("connectionName", "");
				sessionStorage.setItem("connectionId", "");
				
				$scope.connectorTypeList = [];
				$scope.connectorTypeName = null;
				$scope.connectorSTName = null;
				$scope.connectionDetails = null;
				
				connectorService.getConnectorTypeList().then(function(result){
					if(result && typeof result == 'object' && typeof result.error == "undefined"){
						$scope.connectorTypeList = result;
					}
				});
			}();
			
			$scope.setViewHeight = function(){
				$scope.viewHeight = angular.element("#content").height()
			}		
			
			// invoke on connector select 
			$scope.selectConnector = function(connectorTypeId, connectorTypeName){
				$scope.connectorTypeName = connectorTypeName;
				sessionStorage.setItem("connectorTypeName",connectorTypeName);
				sessionStorage.setItem("connectorTypeId",connectorTypeId);
				$location.path("/subconnectors");
			}
		
	}]);
}).call(this);
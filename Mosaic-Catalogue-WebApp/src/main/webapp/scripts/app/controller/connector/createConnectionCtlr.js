(function(){
	var connections= angular.module("app.connections");
	
	connections.controller("createConnectionCtlr",["$scope","connectionService","logger","getterSetterFactory","$location", function($scope,connectionService, logger, getterSetterFactory,$location){
			$scope.createCon={};
			$scope.connectorTypeId = sessionStorage.getItem("connectorTypeId");
			$scope.connectorTypeName = sessionStorage.getItem("connectorTypeName");
			$scope.connectorSTId = sessionStorage.getItem("connectorSTId");
			$scope.connectorSTName = sessionStorage.getItem("connectorSTName");
			$scope.createCon.subSourceId= parseInt($scope.connectorSTId	);
			$scope.createCon.subSourceType= $scope.connectorSTName;
			$scope.createCon.sourceType = $scope.connectorTypeName;
			$scope.blobProtocol = ["https","http"];
			
			$scope.createConnection = function(createCon){
				if($scope.formValidator()){
					connectionService.saveConnection(createCon).then(function(result){
						if(result && typeof result == 'object' && typeof result.SUCCESS != "undefined"){
							logger.logSuccess("Connection created!")
							getterSetterFactory.setConnectionDetails(result.connectionConfig);
							sessionStorage.setItem("connectionDetails",JSON.stringify(result.connectionConfig));
							sessionStorage.setItem("conncetionName",result.connectionConfig.conncetionName);
							$location.path("/publish");
							
						}else if(result && typeof result == 'object' && typeof result.FAIL != "undefined" ){
							logger.logError(result.FAIL)
						}else if(result && typeof result == 'object' && typeof result.error != "undefined" ){
							logger.logError(result.error)
						}else{
							logger.logError(result)
						}
					})
				}
				
			}
			
			$scope.isRequiredField = function(value , field){
				if(!value){
					logger.logError(field+" is required")
					return false;
				}
				return true;
			}
			
			$scope.noWhiteSpaceField = function(value , field){
				if(!/^\S*$/.test(value)){
					logger.logError(field+" does not allow white spaces")
					return false;
				}
				return true;
			}
			
			$scope.formValidator = function(){
				var elements = angular.element(document.querySelectorAll("[fieldName]"));
				for(var loop =0; loop < elements.length ;  loop++){
					var ele = angular.element(elements[loop]);
					if(ele && ele.length > 0 && ele[0].attributes.validator){
						var validator = ele[0].attributes.validator.value.toString();
						var validatorValues = validator.split(",");
						for(var innerLoop =0; innerLoop < validatorValues.length ;  innerLoop++){
							switch(validatorValues[innerLoop].trim()){
								case "required":
									if(!$scope.isRequiredField(ele[0].value.trim(), ele[0].attributes.fieldname.value.trim())){
										return false;
									}
									break;
								case "nospace":
									if(!$scope.noWhiteSpaceField(ele[0].value.trim(), ele[0].attributes.fieldname.value.trim())){
										return false;
									}
									break;	
								default :
									return true;
							}
						}
					}
				}
				return true;
			}
			
			$scope.testConnection = function(createCon){
				if($scope.formValidator()){
					connectionService.testConnection(createCon).then(function(result){
						if(result && typeof result == 'object' && typeof result.FAIL != "undefined"){
							logger.logError("Connection fail!")
						}else if(result && typeof result.error != "undefined"){
							logger.logError(result.error)
						}else if(result === "SUCCESS"){
							logger.logSuccess("Connection successful!")
						}else{
							logger.logError("Connection fail!")
						}
					})
				}
					
			}
			
			$scope.cancelCreateConnection = function(){
				$scope.$emit('cancelCreateConnection', false);
			}
			
			$scope.$emit('updateTimelineFieldValue', {'field':'connectionDetails', value:null});
		}]);
}).call(this);
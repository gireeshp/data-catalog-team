(function(){
	var connector= angular.module("app.connector");
	
	connector.controller("subConnectorCtlr",["$scope","connectorService","$location","$routeParams","$filter",
					function($scope,connectorService,$location,$routeParams,$filter){
		
			$scope.connectorSubTypeList = [];
			$scope.viewHeight = null;
			sessionStorage.setItem("connectorSTName","");
			sessionStorage.setItem("connectorSTId", "");
			sessionStorage.setItem("connectionName", "");
			sessionStorage.setItem("connectionId", "");
			
			var connectorTypeName = sessionStorage.getItem("connectorTypeName");
			var connectorTypeId = sessionStorage.getItem("connectorTypeId");
			 
			if(!connectorTypeName || !connectorTypeId){
				$location.path("/connectors");
			}
				
			
			//fetch sub connector list
			connectorService.getConnectorSubTypeList(connectorTypeId, connectorTypeName)
				.then(function(result){
					if(result && typeof result == 'object' && typeof result.connectionSubSources != "undefined" ){
						$scope.connectorSubTypeList = result.connectionSubSources;
					}
			});	
		
			// select sub connector and set update timeline
			$scope.selectConnectorSubType =	function(connectorSTId,connectorSTName){
				 sessionStorage.setItem("connectorSTId",connectorSTId);
				 sessionStorage.setItem("connectorSTName",connectorSTName);
				 $location.path("/connections");
		    }
			
			$scope.setViewHeight = function(){
				$scope.viewHeight = angular.element("#content").height();
			}
	}]);
}).call(this);
(function(){
	var userList = angular.module("app.userProfile");
	
	userList.controller("dataSourceRequestApproverCtlr",["$scope" , "$http","$cookieStore","logger","ManageUserService","$rootScope", "DataSourceService", "$interval", function($scope,$http,$cookieStore,logger,ManageUserService,$rootScope, DataSourceService, $interval){
		ManageUserService.getWithoutObject('/userManagement/getAllUserDetails').then(function(data){
			$scope.availableUserList=data;
		});
		
	
	
	$scope.getApproveList = function(){
		$http.get("/getDataSourceApproverList?objectId="+ $scope.datasource.dataSourceId).success(function(data){
			$scope.approverList = data;
		});
	};
		
	$scope.disabled = true;	
	$scope.onChangeApvrOne = function(){
	
		$scope.disabled = $scope.approverList[1].userId? false:true;
		
		if($scope.disabled){delete  $scope.approverList[2].userId;}
	}
	
	$scope.resetApprovers= function() {
		if($scope.approverList[1]){
			delete  $scope.approverList[1].userId;
			
		}
		if($scope.approverList[2]){
			delete  $scope.approverList[2].userId;
		}
		$scope.disabled = true;
	}
	setTimeout(function(){ // delay is required to set the value as we didn't get the at the time of page load 
		if($cookieStore.get('userId') != $cookieStore.get('dsownerid')){
			$scope.disableSelect = true;
		}
	},400);
	
	$scope.saveApprovers = function(){
		var saveDataSourceApproverListArray=[];
		var keys =[];
		var appOne = $scope.approverList[1]?$scope.approverList[1].userId:'';
		var appTwo = $scope.approverList[2]?$scope.approverList[2].userId:'';
		keys.push($scope.infoData.createdBy,appOne,appTwo);	
		_.each(keys, function(approverUId,i){
			if(approverUId){
				saveDataSourceApproverListArray.push(
					{
						"userId":parseInt(approverUId),
						"objectId":$scope.datasource.dataSourceId,
						"level":i
					}
				);
			}
		});

		$http({
			method:"POST",
			url:"/saveDataSourceApproverList",
			data:saveDataSourceApproverListArray
		}).success(function(data,status){
			logger.logSuccess("Approval Send successfully.");
			$scope.getApproveList();
		}).error(function(data){
			logger.logError(data);
		});
	}
	
}]);
}).call(this);
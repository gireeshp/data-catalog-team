(function(){
	"use strict";
	
	var discover = angular.module("app.discover");

	discover.filter("GroupNameFromGroupId",function(){
		return function(content, groupMap){		
			return groupMap[content];
		};
	});
	
	discover.filter('unsafe', function($sce) {
		  return function(val) {
	      return $sce.trustAsHtml(val);
	   };
	});
	
	discover.controller("dataSourceRequestAccessController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","dataSourceName",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger,dataSourceName){
		$scope.dsName = dataSourceName;
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
		};
		
		$scope.approveDataSourceRequest = function(justification){
			if(null != $scope.justification && undefined != $scope.justification){
				$modalInstance.close(justification);
			}else{
				$scope.justification ="";				
			}
		};

	}]);
}).call(this);
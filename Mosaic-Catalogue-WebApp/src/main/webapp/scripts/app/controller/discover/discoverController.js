(function(){
	"use strict";
	
	var discover = angular.module("app.discover");
	
	var uniqueItems = function (data, key) {
	    var result = [];
	    
	    for (var i = 0; i < data.length; i++) {
	        var value = data[i][key];
	 
	        if (result.indexOf(value) == -1) {
	            result.push(value);
	        }
	    
	    }
	    return result;
	};
	
		
	discover.filter('bytes', function() {
		return function(bytes, precision) {
			if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
			if (typeof precision === 'undefined') precision = 1;
			var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
				number = Math.floor(Math.log(bytes) / Math.log(1024));
			return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
		}
	});
	
	discover.filter("GroupNameFromGroupId",function(){
		return function(content, groupMap){		
			return groupMap[content];
		};
	});
	
	discover.filter("contentType",function(){
		return function(content){		
			if (content === "")
				return "";
			else if (content === "FILE_DATA_SOURCE")
				return "Delimited File";
			else if (content === "RDBMS_DATA_SOURCE")
				return "Rdbms";
			else  if (content === "TSV_DATA_SOURCE")
				return "Tsv";
			else  if (content === "CSV_DATA_SOURCE")
				return "Csv";
			else  if (content === "EXCEL_DATA_SOURCE")
				return "Excel Sheet";
			else  if (content === "EXTERNAL_DATA")
				return "External Data";
			else  if (content === "FTP_DATA")
				return "Ftp Data";
			else  if (content === "REMOTE_DATA")
				return "Remote Data";
			else  if (content === "STREAM_DS")
                return "Stream Ds";
			else  if (content === "FIXED_LENGTH_FORMAT")
                return "Fixed";
			else  if (content === "MYSQL")
                return "MYSQL";
			else  if (content === "BLOB_DATA_SOURCE")
                return "BLOB";
			else  if (content === "NOSQL")
                return "NOSQL";
			else  if (content === "ORACLE")
                return "ORACLE";
			else  if (content === "POSTGRES")
                return "POSTGRES";
			else  if (content === "SFTP")
                return "SFTP";
			else  if (content === "HDFS")
                return "HDFS";
			else  if (content === "COGNOS_DATA_SOURCE")
                return "COGNOS";
			else  if (content === "AMAZONS3")
                return "AMAZONS3";
			else  if (content === "POSTGRESQL")
                return "POSTGRESQL";
			else  if (content === "MONGODB")
                return "MONGODB";
			if (content === "")
				return "";
			
			else
				return "";
		};
	});
	
	discover.filter('unsafe', function($sce) {
		  return function(val) {
	      return $sce.trustAsHtml(val);
	   };
	});
	
	discover.controller("discoverController" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll", "$timeout", "$interval","glossaryService","discoverRestCalls","usersService",  function($anchorScroll ,$filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,anchorSmoothScroll, $timeout, $interval,glossaryService,discoverRestCalls,usersService){
		
		$scope.viewHeight = null;
		$scope.numPerPageOpt = [5, 9, 20,50];
		$scope.numPerPage = $scope.numPerPageOpt[1];
		$scope.dataSourceCol;
		$scope.filteredStores = [];
		$scope.currentPage = 1;
		$scope.groupMap = {};
		$scope.accesses = [];
		
		//init variable
		$scope.types = [];
		$scope.createdBy = [];
		$scope.category = [];
		$scope.sub_category = [];
		$scope.created_date = [];
		$scope.tags = [];
		$scope.sourceType = [];
		$scope.avgRating = [];
		$scope.dataRepo = [];
		$scope.loadStrategy = [];
		$scope.usergroup = [];
		
		$scope.customValCheckValue = {};
		$scope.typeCheck = [];
		$scope.createdByCheck = [];
		$scope.categoryCheck  = [];
		$scope.subCategoryCheck = [];
		$scope.createdDateCheck = [];
		$scope.tagsCheck = [];
		$scope.sourceTypeCheck = [];
		$scope.avgRatingCheck = [];
		$scope.dataRepoCheck = [];
		$scope.loadStrategyCheck = [];
		$scope.dataUserGroupCheck = [];
		$scope.serarchType = ['Data Source'];
		$scope.checked = true;
		$scope.toggleRequestAccess = false;
		$scope.toggleAddtoProject =	false;
		
		$scope.checkAllSourceType=false;
		$scope.checkAllCategory = false;
	    $scope.checkAllAvgRatingCheck = false;
	    $scope.checkAllProjectsName = false;
	    $scope.checkAllDataUserGrp = false;
	    $scope.checkAllCreatedBy = false;
	    $scope.checkAllTags = false;
	    $scope.checkAllLoadStrategy = false;
	    $scope.checkAllDataAtRest = false;
	    $scope.checkAllSubCategory = false;
	    $scope.checkAllSourceType = false;
	    
	    // variable for facets show / hide
	    $scope.facetsObject = {
	    	'sourceTypeShow': true,
	    	'categoryShow': false,
	    	'subCategoryShow': false,
	    	'dataAtRestShow': false,
	    	'loadStrategyShow': false,
	    	'tagsShow': false,
	    	'createdByShow': false,
	    	'dataUserGroupShow': false,
	    	'projectNameShow': false,
	    	'avgRatingShow': false
	    };
	    $scope.isEnableRequest = false;
		$scope.disabledExpore = false;
		$scope.selectedIndex = 0;
		$scope.requestStatus = 'NA';
		$scope.myCollectionStatus = 'NA';
		$scope.infoDiv = true;
		$scope.requestDiv = false;
		$scope.feedbackDiv = false;
		$scope.approvalDiv = false;
		$scope.rightPannel = true;
		$scope.requestObj = {};
		$scope.requestObj.requestFor = "";
		$scope.requestObj.justification = "";
		$scope.requestObj.selectedRequestGroup = "";
		$scope.dataSourceCol = [];
		$scope.showDs = null;
		$scope.discoverTabs = [
           {
        	   "title":"Feedback",
        	   "active":true
           },
          
           {
        	   "title":"Request access",
        	   "active":false
           },
           {
        	   "title":"Approval",
        	   "active":false
           }
        ];

		 /*{
      	   "title":"Discussion",
      	   "active":false
         },*/
		$scope.feedback = {};
		$scope.userDetailsForAverageRating = null;
		$scope.feedbackList = [];
		$scope.star = {
			starOne: 0,
			starTwo: 0,
			starThree: 0,
			starFour: 0,
			starFive: 0
		};
		

		$scope.datasourceImages = {
			'MYSQL':'../styles/images/publish_catalog/rdms/mysql-publish.png',
			'ORACLE':'../styles/images/publish_catalog/rdms/oracle-publish.png',
			'POSTGRES':'../styles/images/publish_catalog/rdms/pl-publish.png',
			'POSTGRESQL':'../styles/images/publish_catalog/rdms/pl-publish.png',
			'IBMDB2':'../styles/images/publish_catalog/rdms/db2.png',
			'SQL_SERVER':'../styles/images/publish_catalog/rdms/sqlserver.png',
			
			'MONGODB':'../styles/images/publish_catalog/nosql/mongodb.png',
			'HBASE':'../styles/images/publish_catalog/nosql/hbase.png',
			'CASSANDRA':'../styles/images/publish_catalog/nosql/cassandra.png',
			'RADIS':'../styles/images/publish_catalog/nosql/redis.png',
			'NEO4J':'../styles/images/publish_catalog/nosql/neo4j.png',
			
			'AMAZONS3':'../styles/images/publish_catalog/cloud/s3.png',
			'BLOB_DATA_SOURCE':'../styles/images/publish_catalog/cloud/blob-publish.png',
			'BLOB':'../styles/images/publish_catalog/cloud/blob-publish.png',
			'DROPBOX':'../styles/images/publish_catalog/cloud/dropbox.png',
			
			'FTP_DATA':'../styles/images/publish_catalog/ftp/ftp.png',
			'CSV_DATA':'../styles/images/publish_catalog/ftp/csv.png',
			'TCV_DATA':'../styles/images/publish_catalog/ftp/tsv.png',
			'FIXED_WIDTH_DATA':'../styles/images/publish_catalog/ftp/fixedWidth.png',
			'SFTP':'../styles/images/publish_catalog/ftp/sftp.png',
			
			'COGNOS_DATA_SOURCE':'../styles/images/publish_catalog/bi/cognos-publish.png',
			
			'HDFS':'../styles/images/publish_catalog/bigdata/haddop.png',
			'HIVE':'../styles/images/publish_catalog/bigdata/hive.png',
		}
		
		$scope.datasourceAltNames =  {
			'MYSQL':'MYSQL',
			'ORACLE':'ORACLE',
			'POSTGRESQL':'POSTGRESQL',
			'POSTGRES':'POSTGRES',
			'IBMDB2':'IBMDB2',
			'SQL_SERVER':'SQL_SERVER',
			
			'MONGO_DB':'MONGO_DB',
			'HBASE':'HBASE',
			'CASSANDRA':'CASSANDRA',
			'RADIS':'RADIS',
			'NEO4J':'NEO4J',
			
			'AMAZONS3':'AMAZONS3',
			'BLOB_DATA_SOURCE':'BLOB',
			'BLOB':'BLOB',
			'DROPBOX':'DROPBOX',
			
			'FTP_DATA':'FTP',
			'CSV_DATA':'CSV',
			'TCV_DATA':'TCV',
			'FIXED_WIDTH_DATA':'FIXED WIDTH',
			'SFTP':'SFTP',
			
			'COGNOS_DATA_SOURCE':'COGNOS',
			
			'HDFS':'HDFS',
			'HIVE':'HIVE',
		}
		$scope.showafterLoad = false;
		$scope.activeTab = 'Feedback';
		$scope.sortCatlogDataValue = "-createdDate";
		
		$scope.discoverDataSort = function(){
			var descSymbol = "";
			var fieldToSort = $scope.sortCatlogDataValue;
			if($scope.sortCatlogDataValue.indexOf("-") > -1){
				var splitText = $scope.sortCatlogDataValue;
				splitText = splitText.split("-");
				descSymbol = "-"
				fieldToSort = splitText[1];
			}
			$scope.dataSourceCol = _.sortBy($scope.dataSourceCol, function(o) { 
				if(o && descSymbol){
					return -o[fieldToSort];
				}else if(o){
					return o[fieldToSort];
				}
			})
		}
		
		$scope.feedbackSort = function(){
			$scope.feedbackList =  _.sortBy($scope.feedbackList, function(o) { 
				if(o){
					return -o['insertTS'];
				}
			})
		}
		
		$scope.sortCatlogData = function(value){
			$scope.sortCatlogDataValue = value;
			$scope.discoverDataSort();
		}
		
		window.addEventListener("load",function(){
			$scope.setViewHeight();
		});
		
		window.addEventListener("resize",function(){
			$scope.setViewHeight();
		});
		
		//set view height
		$scope.setViewHeight = function(){
			$timeout(function(){
				$scope.viewHeight = angular.element("#content").height();
			}, 200)
		}	
		
		//toogle fasets
		$scope.showHideFacets = function(type){
	    	if($scope.facetsObject[type])		    		
	    		$scope.facetsObject[type] = false;
	    	else
	    		$scope.facetsObject[type] = true;
	    }
		
		//check or uncheck all source-type funtion
		$scope.checkAllSourceTypeCheck = function () {
	        if ($scope.checkAllSourceType) {
	            $scope.checkAllSourceType = false;
	        } else {
	            $scope.checkAllSourceType = true;
	        }
	        
	        for(var i=0;i<$scope.sourceType.length;i++){
	        	$scope.sourceTypeCheck[$scope.sourceType[i].key] = $scope.checkAllSourceType; 
	        }
	        $scope.select(1);
	    };
	    
	    //check or uncheck all category funtion
	    $scope.checkAllCategoryFnc = function () {
	        if ($scope.checkAllCategory) {
	        	$scope.checkAllCategory = false;
	        } else {
	            $scope.checkAllCategory = true;
	        }
	        for(var i=0;i<$scope.category.length;i++){
	        	$scope.categoryCheck[$scope.category[i].key] = $scope.checkAllCategory; 
	        }
	        $scope.select(1);
	    };
	    
	    //check or uncheck all sub-category funtion
	    $scope.checkAllSubCategoryFnc = function () {
	        if ($scope.checkAllSubCategory) {
	            $scope.checkAllSubCategory = false;
	        } else {
	            $scope.checkAllSubCategory = true;
	        }
	        for(var i=0;i<$scope.sub_category.length;i++){
	        	$scope.subCategoryCheck[$scope.sub_category[i].key] = $scope.checkAllSubCategory; 
	        }
	        $scope.select(1);
	    };
	    
	    //check or uncheck all load stratergy funtion
	    $scope.checkAllLoadStrategyFnc = function () {
	        if ($scope.checkAllLoadStrategy) {
	            $scope.checkAllLoadStrategy = false;
	        } else {
	            $scope.checkAllLoadStrategy = true;
	        }
	        for(var i=0;i<$scope.loadStrategy.length;i++){
	        	$scope.loadStrategyCheck[$scope.loadStrategy[i].key] = $scope.checkAllLoadStrategy; 
	        }
	        $scope.select(1);
	    };
	    
	    //check or uncheck all tags function
	    $scope.checkAllTagsFnc = function () {
	        if ($scope.checkAllTags) {
	            $scope.checkAllTags = false;
	        } else {
	            $scope.checkAllTags = true;
	        }
	        for(var i=0;i<$scope.tags.length;i++){
	        	$scope.tagsCheck[$scope.tags[i].key] = $scope.checkAllTags; 
	        }
	        $scope.select(1);
	    };
	    
	    //check or uncheck all created function
	    $scope.checkAllCreatedByFnc = function () {
	        if ($scope.checkAllCreatedBy) {
	            $scope.checkAllCreatedBy = false;
	        } else {
	            $scope.checkAllCreatedBy = true;
	        }
	        for(var i=0;i<$scope.createdBy.length;i++){
	        	$scope.createdByCheck[$scope.createdBy[i].key] = $scope.checkAllCreatedBy; 
	        }
	        $scope.select(1);
	    };
	    
	    //check or uncheck all usere group function
	    $scope.checkAllDataUserGroup = function () {
	        if ($scope.checkAllDataUserGrp) {
	            $scope.checkAllDataUserGrp = false;
	        } else {
	            $scope.checkAllDataUserGrp = true;
	        }
	        for(var i=0;i<$scope.usergroup.length;i++){
	        	$scope.dataUserGroupCheck[$scope.usergroup[i].key] = $scope.checkAllDataUserGrp; 
	        }
	        $scope.select(1);
	    };
	    
	  //check or uncheck all avg rating function 
	   $scope.checkAllAvgRatingCheckFnc = function () {
	        if ($scope.checkAllAvgRatingCheck) {
	            $scope.checkAllAvgRatingCheck = false;
	        } else {
	            $scope.checkAllAvgRatingCheck = true;
	        }
	        for(var i=0;i<$scope.avgRating.length;i++){
	        	$scope.avgRatingCheck[$scope.avgRating[i].key] = $scope.checkAllAvgRatingCheck; 
	        }
	        $scope.select(1);
	    };
		
		//fetch data for mycollection or discover
		$scope.initialization = function(requestDsOrCollection){
			$scope.setViewHeight();
			$scope.dataSourceCol = [];
			if(requestDsOrCollection!="" && requestDsOrCollection=="discover"){
				$scope.urlToCall = "/getSearchResult";
			}else if(requestDsOrCollection!="" && requestDsOrCollection=="collection"){
				$scope.urlToCall = "/getSearchCollectionResult";
			}
			
			//Loading created by list
			discoverRestCalls.getCatlogueCreatedByList().then(function(data){
				if(data && typeof data.data != "undefined"){
					$scope.createdByList=data.data;
					$scope.runOnce = false;
				}
			})
			
			//getting active tags from  glassory service.
			glossaryService.getTagList().then(function(data){
				if(data && typeof data.error == "undefined"){
					var tempTagNames = [];
					_.each(data, function(tag){
						tempTagNames.push(tag.name);
					});
					$scope.tagsNm= tempTagNames;
				}
				$scope.runOnce = false;
			});
			
			$scope.searchData = {createdBy:[],fetchRecordsFrm:0,generalSearchString:"",searchInTypes:["DATASOURCE"], sizeOfRecords:9};
			
			//fetch all catlogs data by discoverRestCalls.getCatalogueData service
			discoverRestCalls.getCatalogueData($scope.urlToCall, $scope.searchData).then(function(data){
				if(data && typeof data.data != "undefined"){
					var data1 = data.data.globalSearchResult;
					$scope.searchResults = data1;
					if(null !== $scope.searchResults && undefined !== $scope.searchResults){
						_.each($scope.searchResults.dataSourceAndFlowType, function(val){
							$scope.types = val.type;
							$scope.createdBy = val.createdBy;
							$scope.category = _.reject(val.category, function(val){
								return val == null && val == "Null";
							});
							$scope.sub_category = _.reject(val.sub_category, function(val){
								return val == null && val == "Null" && val == "null";
							});
							$scope.created_date = val.created_date;
							$scope.tags = [];
							$scope.sourceType = val.source_type;
							$scope.avgRating = val.avg_rating;
							$scope.dataRepo = val.data_repo;
							$scope.loadStrategy = _.reject(val.load_strategy, function(val){
								return val == null && val == "Null" && val == "null";
							});
							$scope.usergroup = val.user_group;
						});
						
						if($scope.pageNumBkp!=null && $scope.pageNumBkp!=undefined){
							$scope.pageNum = angular.copy($scope.pageNumBkp);
						}
						
					}
					$scope.dataSourceCol = data.data.records;
					$scope.accesses = data.data.requestIndicator;
					$scope.filteredStores.length = data.data.totalCount;
					$scope.totalRecords = data.data.totalCount;
					$scope.discoverDataSort();
					$scope.showSelectedData($scope.dataSourceCol[0], 0, null);
				}
				
			});
			
			//fetch groups
			usersService.getGroupsData().then(function(data){
				if(data && typeof data.GROUPS != "undefined"){
					$scope.groupDetails = data.GROUPS;
					_.each($scope.groupDetails, function(group){
						$scope.groupMap[group.groupId] = group.groupName;
				    })
				}
			})
			
		}
		
		$scope.checkDataSourceResultInterval = function(){
			$interval(function(){
				if(null != $rootScope.searchDataResult && undefined != $rootScope.searchDataResult){
					$scope.searchData = angular.copy($rootScope.searchDataResult);
					$scope.isEnableRequest = true;
					$scope.getDataSourceList(true);
				};
				if($rootScope.isDataSourceInfoSaved!=null && $rootScope.isDataSourceInfoSaved!=undefined && $rootScope.isDataSourceInfoSaved){
					$rootScope.isDataSourceInfoSaved = undefined;
					$scope.getDataSourceList(true);
				}
			},100);
	    }();
	    
	    //on order change
	    $scope.onOrderChange = function() {
			return $scope.select(1), $scope.currentPage = 1;
		};
		
		// on pagignation change
		$scope.select = function(page) {
			$scope.pageNum = page;
			$scope.searchData.fetchRecordsFrm = ((page-1) * $scope.numPerPage);
			$scope.searchData.sizeOfRecords = 9;
			$scope.pageNumBkp = angular.copy($scope.pageNum);
			$scope.searchDataBck = angular.copy($scope.searchData); 
			$scope.dataSourceCol = [];
			if($scope.customValCheckValue){
				$scope.searchData.searchInTypes = [];
				$scope.searchData.createdBy = [];
				$scope.searchData.sourceType = [];
				$scope.searchData.avgRating = [];
				$scope.searchData.dataAtRest = [];
				$scope.searchData.dataRepo = [];
				$scope.searchData.loadStrategy = [];
				$scope.searchData.projectsName = [];
				$scope.searchData.userGroup = [];
				$scope.searchData.category = [];
				$scope.searchData.subCategory = [];
				var keysForTypeCheck =  _.keys($scope.typeCheck)
				$scope.searchData.searchInTypes.push('DATASOURCE');
				
				var keysForCreatedByCheck =  _.keys($scope.createdByCheck)
				_.each(keysForCreatedByCheck, function (val){
					if($scope.createdByCheck[val] == true) {
						$scope.searchData.createdBy.push(val);
					}
				});
				
				var keysForCategoryCheck =  _.keys($scope.categoryCheck)
				_.each(keysForCategoryCheck, function (val){
					if($scope.categoryCheck[val] == true) {
						$scope.searchData.category.push(val);
					}
				});
				
				var keysForSubCategoryCheck =  _.keys($scope.subCategoryCheck)
				_.each(keysForSubCategoryCheck, function (val){
					if($scope.subCategoryCheck[val] == true) {
						$scope.searchData.subCategory.push(val);
					}
				});
				
				var keysForCreatedDateCheck =  _.keys($scope.createdDateCheck)
				_.each(keysForCreatedDateCheck, function (val){
					if($scope.createdDateCheck[val] == true) {
						$scope.searchData.createdDate.push(val);
					}
				});
				
				var keysForSourceTypeCheck =  _.keys($scope.sourceTypeCheck)
				_.each(keysForSourceTypeCheck, function (val){
					if($scope.sourceTypeCheck[val] == true) {
						$scope.searchData.sourceType.push(val);
					}
				});
				
				var keysForAvgRatingCheck =  _.keys($scope.avgRatingCheck)
				_.each(keysForAvgRatingCheck, function (val){
					if($scope.avgRatingCheck[val] == true) {
						$scope.searchData.avgRating.push(val);
					}
				});
				
				var keysFordataRepoCheck =  _.keys($scope.dataRepoCheck)
				_.each(keysFordataRepoCheck, function (val){
					if($scope.dataRepoCheck[val] == true) {
						$scope.searchData.dataRepo.push(val);
					}
				});
				var keysForloadStrategyCheck =  _.keys($scope.loadStrategyCheck)
				_.each(keysForloadStrategyCheck, function (val){
					if($scope.loadStrategyCheck[val] == true) {
						$scope.searchData.loadStrategy.push(val);
					}
				});
				
				var keysFordataUserGroupCheck =  _.keys($scope.dataUserGroupCheck)
				_.each(keysFordataUserGroupCheck, function (val){
					if($scope.dataUserGroupCheck[val] == true) {
						$scope.searchData.userGroup.push(val);
					}
				});
				
				discoverRestCalls.getCatalogueData($scope.urlToCall, $scope.searchData).then(function(data){
					$scope.dataSourceCol = [];
					$scope.filteredStores.length = 0;
					if(data && typeof data.data != "undefined"){
						$scope.dataSourceCol = data.data.records;
						$scope.filteredStores.length = data.data.totalCount;
						if(undefined != $scope.dataSourceCol && null != $scope.dataSourceCol && $scope.dataSourceCol.length > 0){
							$scope.showSelectedData($scope.dataSourceCol[0], 0, null)
						}else{
							$scope.showSelectedData($scope.dataSourceCol[-1], -1, null)
						}
					}else{
						$scope.showDs = null;
					}
					
				});

				var end, start;
				return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.dataSourceCol = $scope.filteredStores.slice(start, end);
			}
			
		};
		
		$scope.order = function(rowName) {
			return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
		};

		$scope.onNumPerPageChange = function(val) {
			$scope.numPerPage = val;
			return $scope.select(1), $scope.currentPage = 1;
		};
		
		//showing catlogue data
		$scope.showSelectedData = function(data, $index, event){
			if(event){
				event.stopPropagation();
				event.preventDefault();
			}
			$scope.selectedIndex = $index;
			$scope.showDs = data;
			$scope.showafterLoad = true;
			$scope.loginUserId = localStorage.getItem("userName");
			
			if(undefined == $scope.showDs){
				$scope.infoData = [];
				$scope.dsInstance = null;
				$scope.projectDetails = null;
			}else{
				$routeParams.param1 = data.dataSourceName;
				discoverRestCalls.getDataSource($scope.showDs.dataSourceName).then(function(data){
					if(data && typeof data.error == "undefined"){
						$scope.infoData = data;
						if($scope.infoData.ingection == "typical"){
							$scope.infoData.ingection = "Advanced";
						}else{
							$scope.infoData.ingection = "Basic";
						}
						$scope.getRequestStatusOfDataSource();
						$scope.getAddedMycollectionDataSource();

					}
				});
				
				discoverRestCalls.getDataSourceInstance($scope.showDs.dataSourceName).then(function(data){
					if(data && typeof data.error == "undefined"){
						$scope.dsInstance = data.data;
						$scope.datasource = $scope.dsInstance;
						$scope.getApproverList();
					}
				});
				
				
				
				$scope.getFeedback();
				
				$scope.getRating();
				$scope.requestObj.requestFor = "";
				$scope.requestObj.justification = "";
				$scope.toggleRequestAccess = false;
				$scope.toggleAddtoProject =	false;
			}
		};
		
		$scope.getApproverList = function(){
			$http.get("/getDataSourceApproverList?objectId="+ $scope.datasource.dataSourceId).success(function(data){
				$scope.approverList = data;
			});
		}
		
		$scope.getRating = function(){
			discoverRestCalls.getRating($scope.showDs.dataSourceId).then(function(data){
				if(data && typeof data.error == "undefined"){
					$scope.userDetailsForAverageRating = data;
					$scope.showRating(data);
				}
				
			});
		}
		
		$scope.getFeedback = function(){
			$scope.feedbackList = [];
			discoverRestCalls.fetchDataSourceFeedback($scope.showDs.dataSourceId).then(function(data){
				if(data && typeof data.error == "undefined"){
					$scope.feedbackList = data;
				}
				
			});
		}
		
		$scope.getFavlist = function (){
			discoverRestCalls.getAllFavouriteDataSouceOfUser().then(function(data){
				 if(data && typeof data.error == "undefined"){
					 $scope.myFavouritelist = data;
				 }
				
			});
		};
		
		$scope.enableDisableFavDS = function(update,$event,dataSourceId){
			 $event.stopPropagation();
			 var requestObj = {"dataSourceId":""+dataSourceId+"","markedAsFavourite":update}
			 discoverRestCalls.markDataSourceAsFavourite(requestObj).then(function(data){
				 if(typeof data.success != 'undefined'){
					 logger.logSuccess(data.success);
				 }else if(data && typeof data.error != "undefined"){
					 logger.logError(data.error);
				 }else{
					 logger.logError(data);
				 }
				 
				 
			});
		}
		
		$scope.getRequestStatusOfDataSource = function() {
			var reqDataObject = {objectId : $scope.infoData.id, objectType : "DATA_SOURCE"};
			discoverRestCalls.getCollectionAndRequestStatusOfDatasource(reqDataObject).then(function(data){
				if(data && typeof data.error == "undefined"){
					$scope.requestStatus = data;
				}
				
			});
		};
		$scope.getAddedMycollectionDataSource = function() {
			var reqDataObject = {objectId : $scope.infoData.id, objectType : "DATA_SOURCE"};
			discoverRestCalls.getAddedMycollectionDataSource(reqDataObject).then(function(data){
				$scope.myCollectionStatus = data;
			});
		};
		//tabs
		$scope.openTab = function(tab){
			$scope.activeTab = tab.title;
		};
		$scope.openTab($scope.discoverTabs[0]);
		
		$scope.showInformation = function (dataSourceObj) {
			 $routeParams.param1 = dataSourceObj.dataSourceName;
			 $routeParams.param3 = "popUp";
			 $routeParams.param6 = 1;
			 $routeParams.param7 = dataSourceObj.dataSourceId;
			 $routeParams.param4 = $modal.open({
				backdrop : 'static',
				keyboard : false, 
				size : "xl",
	            templateUrl: "/views/DataSource/dataSourceInfo.jsp",
	            resolve : {
	            	$routeParams : function(){
		           	 	return $routeParams;
		           	},
		           	myParam : function(){
		           	 	return $routeParams;
		           	}
		          }
			}),$routeParams.param4.result.then(function(reponame) {
				
           }, function() {
            $log.info("Modal dismissed at: " + new Date);
          });
		};
		
		//explore
		$scope.goToDiscoverDataSource = function (event) {
			event.stopPropagation();
			event.preventDefault();
			$location.path("/discover/" + $routeParams.param1);
		};
		
		//request for datasources access
		$scope.requestForAccess = function() {
			if($scope.requestObj.requestFor == 'Group'){
				var reqDataObject = {'objectId' : $scope.infoData.id, 'objectType' : "DATA_SOURCE" };
				discoverRestCalls.getListOfGroupsToForWhichObjectNeedsToBeShared(reqDataObject).then(function(data){
					if(data && typeof data.error == "undefined"){
						$scope.requestGroups = data.data;
					}
				})
			}else{
				$scope.requestObj.requestFor = "";
				$scope.requestObj.justification = "";
			}
		};
		
		$scope.selectedRequestProject = "";
		
		$scope.onGroupChange = function(selectedRequestGroup) {
			$scope.requestObj.selectedRequestGroup = selectedRequestGroup;
		};
		
		$scope.requestForAccessConfirmation = function(requestFor,justification){
			$scope.requestObj.requestFor = requestFor;
			$scope.requestObj.justification = justification;
			if(!$scope.requestObj.requestFor) {
				logger.logError("Please select request for");
				return false;
			}
			if($scope.requestObj.requestFor == 'Group' && !$scope.requestObj.selectedRequestGroup) {
				logger.logError("Please select group");
				return false;
			}
			if(!$scope.requestObj.justification) {
				logger.logError("Please enter message");
				return false;
			}
			if($scope.requestObj.requestFor == 'User' && $scope.requestStatus.toLowerCase() == 'pending') {
				logger.logError("Your request is pending for this user");
				return false;
			}
			
			var requestData={
					"objectId":$scope.infoData.id,
					"objectType":"DATA_SOURCE",
					"requestedAccess":"USE",
					"justification":$scope.requestObj.justification,
					"requestedUserId" : parseInt($scope.infoData.createdBy),
					"groupId": ($scope.requestObj.requestFor != 'User') ? parseInt($scope.requestObj.selectedRequestGroup) : null
			};
			
			var reqDataObject = { dataRequestDTO : requestData, isRequestedForGroup : (($scope.requestObj.requestFor == 'User') ? false : true) }
			discoverRestCalls.requestForAccessConfirmation(reqDataObject).then(function(data){
				if(data && typeof data.error != "undefined"){
					logger.logError(data.error);
				}else{
					logger.logSuccess("Request sent for permission");
					$scope.getRequestStatusOfDataSource();
					$scope.requestObj.requestFor = "";
					$scope.requestObj.justification = "";
					$scope.toggleRequestAccess = false;
				}
				
			})
		};
		
		$scope.addToCollection = function(event) {
			event.stopPropagation();
			event.preventDefault();
			var reqDataObject = { objectId : $scope.infoData.id , objectType : "DATA_SOURCE"};
			discoverRestCalls.addToMyCollection(reqDataObject).then(function(data){
				if(data && typeof data.error != "undefined"){
					logger.logError(data.error);
				}else{
					logger.logSuccess("Added successfully");
					$scope.getRequestStatusOfDataSource();
					$scope.getAddedMycollectionDataSource();
				}
			})
		};
		
//				deleteMyCollectionDS function add for delete datasource from my collection.
		$scope.deleteMyCollectionDS = function(event, selectedData) {
			event.stopPropagation();
			event.preventDefault();
			swal({
				 title: "Remove from collection",
				 text: "Are you sure? Do you want to remove datasource "+selectedData.dataSourceName+" from collection!",
				 type: "warning",
			     showCancelButton: true,
			     confirmButtonText: "Yes, remove it!",
			     cancelButtonText: "No, cancel please!",
			     closeOnConfirm: false,
			     closeOnCancel: true,
			     showSpinner: true,
		         showCancelButton: true,
			}, function () {
				var reqDataObject = { objectId : selectedData.dataSourceId , objectType : "DATA_SOURCE"};
				$http({
					method : "post",
					url : "/removeFromMyCollection",
					data : reqDataObject
				}).success(function(data){
					swal.close();
					logger.logSuccess("Removed successfully");
					$scope.initialization();
				}).error(function(status){
					swal.close();
					logger.logError(status);
				});
			})
			
		};	
		
		$scope.deleteDataSource = function(event, index , dataSourceId , dataSourceName){
			event.stopPropagation();
			event.preventDefault();
			
			swal({
				 title: "Delete datasource",
				 text: "Are you sure? Do you want to delete "+dataSourceName+" datasource!",
				 type: "warning",
			     showCancelButton: true,
			     confirmButtonText: "Yes, delete it!",
			     cancelButtonText: "No, cancel please!",
			     closeOnConfirm: false,
			     closeOnCancel: true,
			     showSpinner: true,
		         showCancelButton: true,
			}, function () {
				if($scope.appId === undefined){
					if(index !== null){
						dataSourceId = angular.copy($scope.dataSourceCol[index].dataSourceId);
						$scope.dataSourceCol.splice(index , 1);
						$scope.filteredStores = _.reject($scope.dataSourceCol , function(val){return val.dataSourceId === dataSourceId;});
						
						$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
						$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
						if($scope.dataSourceOfRepo !== undefined || $scope.dataSourceOfRepo !== null || $scope.dataSourceOfRepo.length > 0)
							$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
					}
					if(dataSourceId !== null && index == null){
						$scope.dataSourceCol = _.reject(angular.copy($scope.dataSourceCol) , function(val){return val.dataSourceId === dataSourceId;});
						$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
						$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
						$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
						$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
					}
				}
				$http({
					method: 'POST',
					url : "/deleteDataSource",
					data : dataSourceId
				}).success(function(data, status, headers, config){
					swal.close();
					logger.logSuccess("Data-source deleted successfully");
					$scope.$broadcast("deleteLevelAppDs",dataSourceId);
				}).error(function(data){
					swal.close()
					logger.logError(data);
				});
			});
		};
		
		$scope.advanceSearchOpen = function(){
			$scope.searchData = {"searchInTypes" : ["DATASOURCE"]};
			$scope.searchData.fetchRecordsFrm = 0;
			$scope.searchData.sizeOfRecords = 9;
			$rootScope.searchData = $scope.searchData;
		}
		
		$scope.clearSearch = function (){
			$scope.searchData = {"searchInTypes" : ["DATASOURCE"]};
			$scope.searchData.fetchRecordsFrm = 0;
			$scope.searchData.sizeOfRecords = 9;
			$scope.isEnableRequest = false;
			$scope.getDataSourceList();
		};
		
		var timeToElaspse;
		$scope.onKeyPress = function(){
			if(timeToElaspse){$timeout.cancel(timeToElaspse);}
			timeToElaspse =  $timeout(function () {
				if($scope.searchKeywords != undefined && null != $scope.searchKeywords){
					if($scope.searchData === "") {
						$scope.searchData.generalSearchString = "*";
					} else {
						$scope.searchData.generalSearchString =  "*" + $scope.searchKeywords + "*";
					}
					
					
					$scope.select(1);
				}				 
			}, 500);
		};
		 
		$scope.submitPost = function(object){
			if(!object.question){
				logger.logError("Enter feedback title");
				return false;
			}
			if(!object.answer){
				logger.logError("Enter feedback description");
				return false;
			}
			var dataObjectRequest = {"dataSourceId": $scope.infoData.id, "sourceFeedback": object};
			discoverRestCalls.saveFeedback(dataObjectRequest).then(function(data){
				if(data && typeof data.error != "undefined"){
					logger.logError(data.error);
				}else{
					logger.logSuccess("Added successfully");
					$scope.feedback = {}
					$scope.getFeedback();
				}
			})
		}
		
		$scope.checkRating = function(value){
			$("#starOne").removeClass("checked");
			$("#starTwo").removeClass("checked");
			$("#starThree").removeClass("checked");
			$("#starFour").removeClass("checked");
			$("#starFive").removeClass("checked");
			
			if(value == "starOne"){
				$("#starTwo").removeClass("checked");
				$("#starThree").removeClass("checked");
				$("#starFour").removeClass("checked");
				$("#starFive").removeClass("checked");
				$scope.star = {
						starOne: 1,
						starTwo: 0,
						starThree: 0,
						starFour: 0,
						starFive: 0
				};
			}else if(value == "starTwo"){
				$("#starThree").removeClass("checked");
				$("#starFour").removeClass("checked");
				$("#starFive").removeClass("checked");
				$scope.star = {
						starOne: 1,
						starTwo: 1,
						starThree: 0,
						starFour: 0,
						starFive: 0
				};
			}else if(value == "starThree"){
				$("#starFour").removeClass("checked");
				$("#starFive").removeClass("checked");
				$scope.star = {
						starOne: 1,
						starTwo: 1,
						starThree: 1,
						starFour: 0,
						starFive: 0
				};
			}else if(value == "starFour"){
				$scope.star = {
						starOne: 1,
						starTwo: 1,
						starThree: 1,
						starFour: 1,
						starFive: 0
				};
				$("#starFive").removeClass("checked");
			} else {
				$scope.star = {
						starOne: 1,
						starTwo: 1,
						starThree: 1,
						starFour: 1,
						starFive: 1
				};
			}
		};
		
		$scope.showRating = function(data){
			if(data && data.length > 0){
				$scope.ratingCount = data[0].rating;
			}else{
				$scope.ratingCount = 0;
			}
			$("#starOne").removeClass("checked");
			$("#starTwo").removeClass("checked");
			$("#starThree").removeClass("checked");
			$("#starFour").removeClass("checked");
			$("#starFive").removeClass("checked");
			for(var i = 0; i< data.length; i++){
				if(data[i].rating == 1){
					$("#starOne").addClass("checked");
					
					$scope.star = {
							starOne: 1,
							starTwo: 0,
							starThree: 0,
							starFour: 0,
							starFive: 0
					};
					
					$("#starTwo").removeClass("checked");
					$("#starThree").removeClass("checked");
					$("#starFour").removeClass("checked");
					$("#starFive").removeClass("checked");
				}else if(data[i].rating == 2){
					$("#starOne").addClass("checked");
					$("#starTwo").addClass("checked");
					
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 0,
							starFour: 0,
							starFive: 0
					};
					
					$("#starThree").removeClass("checked");
					$("#starFour").removeClass("checked");
					$("#starFive").removeClass("checked");
				}else if(data[i].rating == 3){
					$("#starOne").addClass("checked");
					$("#starTwo").addClass("checked");
					$("#starThree").addClass("checked");
					
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 1,
							starFour: 0,
							starFive: 0
					};
					
					$("#starFour").removeClass("checked");
					$("#starFive").removeClass("checked");
				}else if(data[i].rating == 4){
					$("#starOne").addClass("checked");
					$("#starTwo").addClass("checked");
					$("#starThree").addClass("checked");
					$("#starFour").addClass("checked");
					
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 1,
							starFour: 1,
							starFive: 0
					};
					
					$("#starFive").removeClass("checked");
				}else if(data[i].rating == 5){
					$("#starOne").addClass("checked");
					$("#starTwo").addClass("checked");
					$("#starThree").addClass("checked");
					$("#starFour").addClass("checked");
					$("#starFive").addClass("checked");
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 1,
							starFour: 1,
							starFive: 1
					};
				}
			}
		}
		$scope.submitRating = function(value){
			var sum = 0;
			if(parseInt(value.starFive) > 0) {
				sum = 5;
			} else if (parseInt(value.starFour) > 0){
				sum = 4;
			} else if(parseInt(value.starThree) > 0) {
				sum = 3;
			} else if(parseInt(value.starTwo) > 0) {
				sum = 2;
			} else if(parseInt(value.starOne) > 0) {
				sum = 1;
			}
			
			var reqDataObject = {'dataSourceId': $scope.infoData.id,'rating':sum};
			discoverRestCalls.updateDataSourceRating(reqDataObject).then(function(data){
				if(data && typeof data.error != "undefined"){
					logger.logError(data.error);
				}else{
					logger.logSuccess("Rating added successfully");
					$scope.getRating();
				}
			});
			
	   };
	   
	   $scope.updateVote = function(vote, $index, item){
			
			var vote = parseInt(vote);
			var reqDataObject = {'dataSourceId': item.dataSourceId,'vote':+vote,'feedbackId':item.id};
			$http({
				method: "POST",
				url: "/updateDataSourceVote",
				data : reqDataObject
			}).success(function(data){
				
				if(data=="alreadyVoted"){
					logger.logError("You already voted");	
				}
				
				if(data=="0"){
					$scope.feedbackList[$index].votingCount = parseInt(item.votingCount);
					$scope.feedbackList[$index].votingCount = parseInt(item.votingCount) + vote;
					$scope.feedbackList[$index].selfVote = 0;		

					if(vote==1){
						$scope.feedbackList[$index].votingNegativeCount = parseInt($scope.feedbackList[$index].votingNegativeCount) + vote;	
					}
					
					if(vote==-1){
						$scope.feedbackList[$index].votingPositiveCount = parseInt($scope.feedbackList[$index].votingPositiveCount) + vote;	
					}
				}
				
				if(data=="1" || data=="-1"){						
					$scope.feedbackList[$index].votingCount = parseInt(item.votingCount);
					$scope.feedbackList[$index].votingCount = parseInt(item.votingCount) + vote;
					$scope.feedbackList[$index].selfVote = vote;
					
					if(vote==1){
						$scope.feedbackList[$index].votingPositiveCount = parseInt($scope.feedbackList[$index].votingPositiveCount) + vote;	
					}
					
					if(vote==-1){
						$scope.feedbackList[$index].votingNegativeCount = parseInt($scope.feedbackList[$index].votingNegativeCount) + vote;	
					}
					logger.logSuccess("Vote submitted.");
				}
				
			}).error(function(data){
				logger.logError(data);					
			});
		};
	}])
}).call(this);
(function(){
	var dataExplorer = angular.module("app.hiveExplorer");
	
	var globe = null;
	
	dataExplorer.controller("exploreHiveController", [ "$scope", "$filter", "$modal", "$rootScope",
			"$http", "$location", "logger", "$route","GrabDataForExplorer","$window","$log","$cookieStore","Fullscreen", "StorageStrategyService","$timeout","ivhTreeviewBfs",
			function($scope, $filter, $modal, $rootScope, $http, $location, logger, $route, GrabDataForExplorer,$window,$log,$cookieStore,Fullscreen, StorageStrategyService,$timeout,ivhTreeviewBfs) {
			
			$scope.datasourceImages = {
				'MYSQL':'../styles/images/publish_catalog/rdms/mysql-publish.png',
				'ORACLE':'../styles/images/publish_catalog/rdms/oracle-publish.png',
				'POSTGRES':'../styles/images/publish_catalog/rdms/pl-publish.png',
				'POSTGRESQL':'../styles/images/publish_catalog/rdms/pl-publish.png',
				'IBMDB2':'../styles/images/publish_catalog/rdms/db2.png',
				'SQL_SERVER':'../styles/images/publish_catalog/rdms/sqlserver.png',
				
				'MONGODB':'../styles/images/publish_catalog/nosql/mongodb.png',
				'HBASE':'../styles/images/publish_catalog/nosql/hbase.png',
				'CASSANDRA':'../styles/images/publish_catalog/nosql/cassandra.png',
				'RADIS':'../styles/images/publish_catalog/nosql/redis.png',
				'NEO4J':'../styles/images/publish_catalog/nosql/neo4j.png',
				
				'AMAZONS3':'../styles/images/publish_catalog/cloud/s3.png',
				'BLOB_DATA_SOURCE':'../styles/images/publish_catalog/cloud/blob-publish.png',
				'BLOB':'../styles/images/publish_catalog/cloud/blob-publish.png',
				'DROPBOX':'../styles/images/publish_catalog/cloud/dropbox.png',
				
				'FTP_DATA':'../styles/images/publish_catalog/ftp/ftp.png',
				'CSV_DATA':'../styles/images/publish_catalog/ftp/csv.png',
				'TCV_DATA':'../styles/images/publish_catalog/ftp/tsv.png',
				'FIXED_WIDTH_DATA':'../styles/images/publish_catalog/ftp/fixedWidth.png',
				'SFTP':'../styles/images/publish_catalog/ftp/sftp.png',
				
				'COGNOS_DATA_SOURCE':'../styles/images/publish_catalog/bi/cognos-publish.png',
				
				'HDFS':'../styles/images/publish_catalog/bigdata/haddop.png',
				'HIVE':'../styles/images/publish_catalog/bigdata/hive.png',
			}
			
			$scope.datasourceAltNames =  {
				'MYSQL':'MYSQL',
				'ORACLE':'ORACLE',
				'POSTGRESQL':'POSTGRESQL',
				'POSTGRES':'POSTGRES',
				'IBMDB2':'IBMDB2',
				'SQL_SERVER':'SQL_SERVER',
				
				'MONGO_DB':'MONGO_DB',
				'HBASE':'HBASE',
				'CASSANDRA':'CASSANDRA',
				'RADIS':'RADIS',
				'NEO4J':'NEO4J',
				
				'AMAZONS3':'AMAZONS3',
				'BLOB_DATA_SOURCE':'BLOB',
				'BLOB':'BLOB',
				'DROPBOX':'DROPBOX',
				
				'FTP_DATA':'FTP',
				'CSV_DATA':'CSV',
				'TCV_DATA':'TCV',
				'FIXED_WIDTH_DATA':'FIXED WIDTH',
				'SFTP':'SFTP',
				
				'COGNOS_DATA_SOURCE':'COGNOS',
				
				'HDFS':'HDFS',
				'HIVE':'HIVE',
			}
			$scope.viewHeight = null;
			$scope.isDsFetch = null;
			$scope.dataForTheTree = [];
			$scope.abortSaveOutput = false;
			$scope.query={
					"queryName":""
			};
			$scope.rowSize = 8;
			$scope.queryResult = [];
			$scope.headerRow = [];
			$scope.gridOptions = {
					enableSorting: true,
					columnDefs: [],
					data: []
			};
			$scope.selectedDatasources = [];
			
			//set view height
			$scope.setViewHeight = function(){
				$timeout(function(){
					$scope.viewHeight = angular.element("#content").height();
				},1000);	
			}
			
			//fetch connection data sources
			$scope.fetchConnectionsData = function(){
				$scope.isDsFetch = true;
				$timeout(function(){
					GrabDataForExplorer.getExplorerData().then(function(data){
						$scope.isDsFetch = false;
						if(data.status === 200 && data.data){
							$timeout(function(){
								$scope.dataForTheTree = data.data;
							},300);
						}else{
							logger.logError("No datasources exists.");
						}	
					})
				},1000);
			}();
			
			//query editor full screen
			$scope.fullTextarea = function(){
				if (Fullscreen.isEnabled()){
					$scope.myCssApply = "";
					Fullscreen.cancel();
				}else{
					$scope.myCssApply = "100%";
					Fullscreen.enable(queryeditor);
				}
			};
			
			//resize query text area
			$scope.resizeText = function(input){
				input = parseInt(input);
				if($scope.rowSize == 20){
					return false;
				}else{
					$scope.rowSize = $scope.rowSize + input;
				}
			};
			
			//size change width text area
			$scope.sizeChanger = function(input){
				if($scope.rowSize == 4){
					return false;
				}else{
					$scope.rowSize = $scope.rowSize + input;
				}
			};
			
			//clear query
			$scope.clearQuery = function(){
				$scope.queryeditor = "";
				$scope.executeSelectedQueryCombo = "";
				$scope.queryName = "";
				$scope.queryId = -1;
				globe = null;
			};
			
			//run query
			$scope.runQuery = function() {
				var query = angular.copy($scope.queryeditor.replace(/\n/g, ' '));
				$scope.gridOptions.enableSorting = true;
				$scope.gridOptions.columnDefs = [];
				$scope.gridOptions.data = [];
				
				GrabDataForExplorer.runPrestoQuery(query).then(function(data){
					if(data && typeof data == "object" && data.length > 0){
						$scope.showResult(data);
					}else if(!data){
						logger.logError("No record found");
					}else{	
						logger.logError(data);
					}
				})
			};
			
			//show query results
			$scope.showResult = function (data) {
				$scope.value = "RunQuery";
				$scope.queryResult = data;
				
				var objectOfOutput = [];
				var columnCalculate = [];
				var widthCalculate = 100/($scope.queryResult.length - 1) ;
				var columnNames = [];
				_.each($scope.queryResult , function(values){
					var obj = {field : values.fieldName , width: widthCalculate+'%' , minWidth: 70 };
					obj.field = angular.lowercase(obj.field); 
					columnCalculate.push(obj);
					columnNames.push(values.fieldName)
				});
				if(columnCalculate.length > 0){
					_.each($scope.queryResult[0].sampleRecords, function(values, index){
						var obj = {};
						_.each($scope.queryResult, function(value, indexInner){
							obj[columnCalculate[indexInner].field] = $scope.queryResult[indexInner].sampleRecords[index];
						});
						objectOfOutput.push(obj);
					});
				}
					
				
				$scope.gridOptions.columnDefs = columnCalculate; 
				$scope.gridOptions.data = objectOfOutput;
				
				if($scope.queryResult.length > 0){
					$scope.headerRow = columnNames;
					$scope.queryResult.splice(0, 1);
				}
				if($scope.gridOptions.data.length < 1){
					logger.logError("No record found");
				}
				
			};
			
			$scope.onTreeSelectionChange = function(){
				$scope.selectedDatasources = [];
				ivhTreeviewBfs($scope.dataForTheTree, function(node) {
	        		if(node.selected && node.label != "Columns" && !node._genericField && node.label != "Data Sources") {
	        			$scope.selectedDatasources.push(node)
	        		}
	    	    });
			}
			
	}])
	

}).call(this);
(function(){
	var userList = angular.module("app.userProfile");
	
	userList.controller("settingsTabCtlr",["$scope" , "$http","logger","$location", function($scope,$http,logger,$location){
		$scope.activeSubTab = "";
		$scope.activeTab = "";
		$scope.subTabList = [];
		$scope.hideSettingsTab = function(){
		return	$location.path() == "/settings" ? false:true;
		}
		
		$scope.subTabs = [
			{
				title: 'User group',
				active : false
			},
			{
				title: 'User role',
				active : false
			},
			{
				title: 'User management',
				active : false
			}
		]
		
		
		$scope.changSubTab = function(tab){
			$scope.activeSubTab = tab.title;
		}
		$scope.changSubTab($scope.subTabs[0]);
	
	}]);
	
}).call(this);
(function() {
	"user strict";
	var module = angular.module("app.datasource" , ['app.dataSourceTable','angularFileUpload','checklist-model','app.dataSourceTable','FBAngular','app.relations','app.datasourceinfo']);
	
	module.constant('CONSTANTS',{
		INPUT: 'INPUT',
		TIMESTAMP: 'TIMESTAMP',
		GP_LAST_RUN_DATETIME: 'gp_lastRunDateTime',
		GP_CURRENT_RUN_DATETIME: 'gp_currentRunDateTime',
		GP_LAST_SUCCESSFULLY_RUN_DATETIME: 'gp_lastSuccessfullyRunDateTime'
	});
    
	module.filter("sizeConvert", function() {
		return function(bytes) {
			if (bytes < 1024)
				return bytes + " Bytes";
			else if (bytes < 1048576)
				return Math.round(bytes / 1024) + " KB";
			else if (bytes < 1073741824)
				return Math.round(bytes / 1048576) + " MB";
			else
				return Math.round(bytes / 1073741824) + " GB";
		};
	});
	
	module.filter("sizeConvertForHistoryTab", function() {
		return function(bytes) {
			if (bytes < 1024)
				return parseFloat(bytes.toFixed(1)) + " Bytes";
			else if (bytes < 1048576)
				return parseFloat((bytes / 1024).toFixed(1)) + " KB";
			else if (bytes < 1073741824)
				return parseFloat((bytes / 1048576).toFixed(1)) + " MB";
			else
				return parseFloat((bytes / 1073741824).toFixed(1)) + " GB";
		};
	});


	module.factory('DataSourceFactory', function() {
		return {
			data : {
				indicator : true
			},
			update : function(indicator) {
				this.data.indicator = indicator;
			}
		};
	});
	
	module.directive('ngEnter', function($document) {
		return {
			scope : {
				ngEnter : "&"
			},
			link : function($scope, element, attrs) {
				var enterWatcher = function(event) {
					if (event.which === 13) {
						$scope.ngEnter();
						$scope.$apply();
						event.preventDefault();
						$document.unbind("keydown keypress", enterWatcher);
					};
				};
				$document.bind("keydown keypress", enterWatcher);
			}
		};
	});

	module.service('uniqueArray', function() {
		return {
			unique : function unique(array) {
				var newArr = _.filter(array, function(element, index) {
					for (index += 1; index < array.length; index += 1) {
						if (_.isEqual(element, array[index])) {
							return false;
						}
					}	return true;
				});
				return newArr;
			}
		};
	}),
			module.service("DataSourceService", function($http, $q) {
				function getDataSource(url, param) {
					return $http.post(url, param).then(function(result) {
						return result.data;
					});
				}
				return {
					getDataSource : getDataSource
				};
			}),
			module.service("StorageStrategyService", function($http, $q) {
				function getStorageStrategy(url) {
					return $http.get(url).then(function(result) {
						return result.data;
					});
				}
				return {
					getStorageStrategy : getStorageStrategy
				};
			}),
				

	module.directive('onReadFile', function ($parse) {
		return {
			restrict: 'A',
			scope: false,
			link: function($scope, element, attrs) {
				element.bind('change', function(e) {

					var onFileReadFn = $parse(attrs.onReadFile);
					var reader = new FileReader();
					reader.onload = function() {
						var fileContents = reader.result;

						$scope.$apply(function() {
							onFileReadFn($scope, {
								'contents' : fileContents
							}
							);
						});
					};
					reader.readAsText(element[0].files[0]);
				});
			}
		};
	}).controller("dataSourceInformer" , ["DataSourceFactory","$anchorScroll","$route", "DataSourceService","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "$loading","$cookieStore","$filter","$interval","StorageStrategyService", function(DataSourceFactory, $anchorScroll,$route,DataSourceService,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $loading,$cookieStore,$filter,$interval, StorageStrategyService){

		$scope.projectAccessType = $cookieStore.get("projectAccessType");
		$scope.userSelectedProjectId = $cookieStore.get("projectId");
		$scope.userId = $cookieStore.get("userId");
        $scope.logedInuserId = $cookieStore.get("userId");
		$scope.categories = [];
		
		$scope.storageStrategy = [];
		$scope.disableApplicationSetting = ($scope.storageStrategy === "HDFS" ? false : true);
		$http.post("/getAllCategorySubCatMaster").success(function(data){
			$scope.categoriesDetails = data;
			$scope.categories = _.keys($scope.categoriesDetails);
		}).error(function(data){
			logger.logError(data);
		});
		$scope.tagNames = [];
		$http.post("/getAllActiveTags").success(function(data){
			var tagNames = [];
			_.each(data, function(tag){
				tagNames.push(tag.name);
			});
			$scope.tagNames=tagNames;  
		}).error(function(data){
			logger.logError(data);
		});
		
		
		$scope.disableOverview = false;
		$scope.disableRating = false;
		$scope.joinRelationChartTreeData = [];
		if($location.path().includes("/discover")) {
			$routeParams.param1 = decodeURIComponent($routeParams.param1);
			$scope.tabs = _.reject($scope.tabs, function(val){return val.title == "Configurations"})
			$scope.disableOverview = true;
			$scope.disableRating = false;
			
			// get datasource id by name and checking user has previlege to rate feedback for datasource
			$http({
				url: "/getDataSource",
				method: "POST",
				data: $routeParams.param1
			}).success(function(data){
				$scope.dataSourceObject = data;
				$scope.joinRelationChartTreeData = [];
				if($scope.dataSourceObject.dataSourceType === "COGNOS_DATA_SOURCE"){
					var joins  = $scope.dataSourceObject.fieldMappings[0].reportPublishData.joins;
					if(joins){
						var rootObject = {"name":"JOIN", "parent":null, "children":[]};
						for(var i = 0; i < joins.length;i++){
							var alisa = {"name": joins[i].leftTable+"<-->"+joins[i].rightTable, "parent": "JOIN", "children":[]}
							var leftTable = {"name": joins[i].leftTable, "parent": alisa.name, "children":[]}
							var rightTable = {"name": joins[i].rightTable, "parent": alisa.name, "children":[]}
							//left column
							for(var j = 0; j < joins[i].leftColumn.length;j++){
								leftTable.children.push({"name": joins[i].leftColumn[j], "parent":joins[i].leftTable, "children":[]})
							}
							
							//right column
							for(var k = 0; k < joins[i].rightColumn.length;k++){
								rightTable.children.push({"name": joins[i].rightColumn[k], "parent":joins[i].rightTable, "children":[]})
							}
							
							alisa.children.push(leftTable)
							alisa.children.push(rightTable)
							rootObject.children.push(alisa)
						}
						$scope.joinRelationChartTreeData.push(rootObject)
					}
				}
				var reqDataObject = {objectId : $scope.dataSourceObject.id, objectType : "DATA_SOURCE"};
				$http({
					method : "POST",
					url : "/getCollectionAndRequestStatusOfDatasource",
					data : reqDataObject
				}).success(function(data, status){
					if(status == 200)
						if(data != 'Accept'){
							$scope.disableRating = true;
						}
						if($scope.dataSourceObject.createdBy == $scope.userId){
							$scope.disableOverview = false;
							$scope.disableRating = false;
						}
				}).error(function(data){
				});
			}).error(function(data){
				log.logError(data);
			});
		} else {
			$scope.disableOverview = false;
			$scope.disableRating = false;
		};
		
		//cognos field show function 
		$scope.selectedCognosField= null;
		$scope.selectedCognosFieldValues = null;
		$scope.selectedCognosFieldType = null;
		$scope.showCognosFields = function (key, object, typeOfField){
			$scope.selectedCognosField = key;
			$scope.selectedCognosFieldValues = object;
			$scope.selectedCognosFieldType = typeOfField;
		};
		
		$scope.hiddenIndicatorForRdbms = DataSourceFactory.data;
		var myParam = angular.copy($routeParams);
		$scope.selected  = $routeParams.param1,
		$scope.modelCheck  = $routeParams.param3,
		$scope.appId = $routeParams.param10;
		$scope.selectedFields = [],
		$scope.checkDataType;
		$scope.validationChkBox = [];
		$scope.transformationChkBox = [];
		$scope.distinctProfilePreChkBox = [];
		$scope.distinctProfilePostChkBox = [];
		$scope.distinctPreValueFreqChkBox = [];
		$scope.distinctPostValueFreqChkBox = [];
		$scope.appId = $routeParams.param10;
		$scope.appName = $routeParams.param20;
		$scope.decideDesabled = 0;
		$scope.list = [];
		$scope.tags = [];
		
		$rootScope.basicDataIngection = false;
		
		$scope.hbase = {
				primary : null,
				secondary : null
		}

		$scope.disableCheck = {preMyIndicator : false,postMyIndicator:false};


		$scope.options = {
				text: 'Loading DataSource...',

				spinnerOptions: {
					zIndex: 99,
					color: '#0086b3'
				}
		}

		$loading.setDefaultOptions($scope.options)

		//$loading.start('loadingDS');


		if($scope.appName != undefined){
			$http.post("/getSingleApplication",$scope.appName).success(function(data, status, headers, config){
				$scope.globleParam = data.appCombiner;
			});	
		}

		$scope.delAppSettings = function(value , index){
			if(value == undefined){
				$scope.list[index] = undefined;
			}
		};

		$scope.saveAppSettings = function(){

			if($scope.appSetting.use == "default"){
				$scope.list = [];
				
			}


			var applicationData  = [{
				type : "DS",
				globalParamName : "",
				componantId : $scope.datasource.id,
				appId : $scope.appId,
				globalParamValue : ""
			}];

			if($scope.appSetting.globalParam != undefined && $scope.appSetting.use !== "default"){

				var BasePath = {
						type : "DS",
						globalParamName : "BasePath",
						componantId : $scope.datasource.id,
						appId : $scope.appId,
						globalParamValue : $scope.appSetting.globalParam.name
				};
				applicationData.push(BasePath);
			}


			if($scope.list.length > 0){
				var i=0;
				_.each($scope.list , function(val){
					if(val !== undefined){
						var appData = {
								type : "DS",
								globalParamName : $scope.datasource.dsLevelParams[i].paramName,
								componantId : $scope.datasource.id,
								appId : $scope.appId,
								globalParamValue : val.name
						};
						applicationData.push(appData);
					}
					i++;
				});
			}


			if(applicationData.length == 1 && $scope.appSetting.use == "Refreshed"){
				var BasePath = {
						type : "DS",
						globalParamName : "Refreshed",
						componantId : $scope.datasource.id,
						appId : $scope.appId,
						globalParamValue : "true"
				};
				applicationData.push(BasePath);
			}

			$http({
				method : "post",
				url : "/saveAppGlobal",
				data : applicationData
			}).success(function(data){
				logger.logSuccess("Saved successfully");
			}).error(function(status){
				logger.logError(status);
			});
		};

		
		

		$scope.nextPrevous = function(decideWhich){
			$scope.$broadcast("nextPrevous" , decideWhich);
			$scope.gotoAnchor("top");
		},

		$scope.gotoAnchor = function(repoName) {
			var id = $location.hash();
			$location.hash(repoName);
			$anchorScroll();
			$location.hash(id);
		};
		$scope.saveDataIngestion = function(){
			$scope.datasource.availableToUse = undefined;
			if($scope.datasource.ingection == "typical" && $scope.datasource.fileStoreObject === "SOURCE"){
				logger.logError("You cannot choose Advanced data Ingestion method with SOURCE data-at-rest type.\n Please deselect SOURCE from Advanced Setting > data-at-rest type.");
				$scope.datasource.ingection = "quick";
				return false;
			}
			if(backUp !== $scope.datasource.ingection){
				if($scope.datasource.ingection  === "quick"){
					var modalInstance;
					modalInstance = $modal.open({ 
						templateUrl: "/views/template/saveAs.html",
						controller: "saveAsController",
						resolve : {	
							list:function(){
								return [];
							},
							messg : function(){
								return "The Transformation, Data-Profiling and Validations will be lost.";
							}, 
							ind : function(){
								return false;
							},
							headerTitle : function(){
								return "";
							},
							yesBtnLable : function() {
								return "Yes";
							},
							noBtnLable : function() {
								return "No";
							}
						}
					});
					modalInstance.result.then(function (param) {
						if(param === "yes"){
							$scope.datasource.advancedValidations = [];
							$scope.datasource.featureExtraction = [];
							$scope.datasource.advancedValidationsStore = [];
							$scope.datasource.transformations = [];
							$scope.datasource.dataCleansers = [];
							$scope.datasource.fieldMappingsStore = [];
							$scope.datasource.preIngestion = false;
							_.each($scope.datasource.fieldMappings , function (val){
								val.dataValidations = null;
								val.preIngestion = false;
								val.preValueFrequency = false;
							});


							if($scope.fileDataIngesterDetails != null){
								$scope.datasource.fileDataIngesterDetails.containsHeader = false+"";
								$scope.datasource.fileDataIngesterDetails.headerStarting = "";
							}
							$scope.datasource.availableToUse=undefined;
							$http({
								url: "/saveDataSource",
								method: 'Post',
								data : $scope.datasource
							}).then(function(response) {
								DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
									$scope.datasource = data;
									
									if($scope.datasource.ingection === "quick"){
										$scope.ingections = "Basic";
										$scope.dataHide = true;
									}else{
										$scope.ingections = "Advance";
										$scope.dataHide = false;
									}
									$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
									$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion); 

									logger.logSuccess("Data Ingestion Updated successfully");
									$scope.$emit("shareDataSourceToAllController", $scope.datasource);
								});
							}, 
							function(response) {
								logger.logError("Something went wrong");
							});
						}
					});
				}else{
					$scope.datasource.availableToUse=undefined;
					$http({
						url: "/saveDataSource",
						method: 'Post',
						data : $scope.datasource
					}).then(function(response) {
						logger.logSuccess("Data Ingestion Updated successfully");

						DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
							$scope.datasource = data;
							if($scope.datasource.ingection === "quick"){
								$scope.ingections = "Basic";
								$scope.dataHide = true;
							}else{
								$scope.ingections = "Advance";
								$scope.dataHide = false;
							}
							$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
							$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion); 
							$scope.$emit("shareDataSourceToAllController", $scope.datasource);
						});
					}, 
					function(response) {
						logger.logError("Something went wrong");
					});
				}
			}else{
				logger.logError("Please change data ingestion method");
				return false;
			}
		};
		
		if($routeParams.param6 === 1){
			$scope.myTbHide = true;
			$scope.myTbHide1 = true;
			$routeParams.param6++;
		}else{
			$scope.myTbHide = true;
		};

		if($routeParams.param6 === undefined){
			$scope.myTbHide1 = true;
		}

		$scope.check = [];


		$scope.$on("hourEvents", function (event, args) {
			$scope.explanation = args;
		});

		$scope.$on("shareDataSourceToAllController", function (event, args) {
			$scope.datasource = args;
			$scope.selectRejectValueForPrimary[0] = $scope.datasource.fieldMappings;
			$scope.selectRejectValueForSecondary[0] = $scope.datasource.fieldMappings;
			$scope.fetchFieldMappingForHbase();
			backUp = $scope.datasource.ingection;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

			var count = -1;

			_.each($scope.datasource.fieldMappings, function(key){
				count++;

				if(key.transformation !== null && key.transformation.length > 0){
					$scope.transformationChkBox[count] = true;
				}else{
					$scope.transformationChkBox[count] = false;
				}
				if(key.dataValidations !== null && key.dataValidations.length > 0){
					$scope.validationChkBox[count] = true;
				}else{					
					$scope.validationChkBox[count] = false;
				}				
				if(key.preIngestion !== null && key.preIngestion !== false){
					$scope.distinctProfilePreChkBox[count] = true;
				}else{
					$scope.distinctProfilePreChkBox[count] = false;
				}
				if(key.postIngestion !== null && key.postIngestion !== false){
					$scope.distinctProfilePostChkBox[count] = true;
				}else{
					$scope.distinctProfilePostChkBox[count] = false;
				}
				if(key.preValueFrequency !== null && key.preValueFrequency !== false){
					$scope.distinctPreValueFreqChkBox[count] = true;
				}else{
					$scope.distinctPreValueFreqChkBox[count] = false;
				}
				if(key.posValueFrequency !== null && key.posValueFrequency !== false){
					$scope.distinctPostValueFreqChkBox[count] = true;
				}else{
					$scope.distinctPostValueFreqChkBox[count] = false;
				}

			});

			$scope.disableCheck.preMyIndicator = angular.copy($scope.datasource.preIngestion);
			$scope.disableCheck.postMyIndicator = angular.copy($scope.datasource.postIngestion);
			
			if($scope.datasource.fileStoreObject === "HDFS" && $scope.storageStrategy != "HDFS"){
				$scope.datasource.fileStoreObject = $scope.storageStrategy;
			}
			
			if($scope.datasource.fileStoreObject === $scope.storageStrategy){

				if($scope.datasource.dataAtRestFileType === undefined){
					logger.logError("Please Select File Type.");
					return false;
				}

				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];

			}else if($scope.datasource.fileStoreObject === "SOURCE"){
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
			}else {
				if(undefined != $scope.datasource.primaryKeysForIndex 
						&& $scope.datasource.primaryKeysForIndex.length === 0 && $scope.datasource.fileStoreObject !== "SOURCE"){
					logger.logError("Please add primary key");
					return false;
				}

				$scope.datasource.dataAtRestFileType = null;
				$scope.datasource.dataAtRestCompressionType = null;
			}
			
			$scope.$broadcast("myEventDataSource",$scope.datasource);
		});
		$scope.subMenu = [];
		
		$scope.onClickSubMenuTab = function (tab) {
			$scope.activatedSubMenu = tab.title;
			$scope.$emit("mySubMenuEvents", tab.title);
			$scope.$broadcast("mySubMenuEvents", tab.title);
			
		};
		
		$scope.$on("myEvent1", function (event, args) {
			   $scope.activeTab = args;
			   if($scope.activeTab == "Overview"){
				   $scope.decideDesabled = 0;
			   }else if($scope.activeTab == "Configurations")
				   $scope.decideDesabled = 1;
			   else if($scope.appId == undefined && $scope.appId==0 && $scope.activeTab == "Data")
				   $scope.decideDesabled = 3;
			   else if($scope.appId == undefined && $scope.appId==0 && $scope.activeTab == "Explore")
				   $scope.decideDesabled = 3;
			   else if($scope.appId == undefined && $scope.appId==0 && $scope.activeTab == "Feedback")
				   $scope.decideDesabled = 3;
			   else if($scope.appId !== undefined && $scope.appId > 0 && $scope.activeTab == "Application settings")
				   $scope.decideDesabled = 3;
			   else
				   $scope.decideDesabled = 1;
		});
		
		$scope.$on("myEvent1", function (event, args) {
			   $scope.activeTab = args;
			   if($scope.activeTab == "Configurations") {
				   if(null != $scope.datasource && $scope.datasource.dataSourceType === "STREAM_DS"){
	   					$scope.subMenu = [{
	   							title: 'Connection',
	   							active: true
	   						}, {
	   							title: 'Fields',
	   							active: false
	   						} 
	   						];
	   				   } else {
			   			$scope.subMenu = [{
			   				title: 'Connection',
							active: true
						}, {
							title: 'Fields',
							active: false
						}, {
							title: 'Adv. Settings',
							active: false
						}, {
							title: 'History',
							active: false
						}, {
							title: 'Snapshots',
							active: false
						} 
						];
			   		}
				} else if ($scope.activeTab == "Data") {
					if(null != $scope.datasource && $scope.datasource.dataSourceType === "STREAM_DS"){
						$scope.subMenu = [{
								title: 'Fields',
								active: true
							}]
						} else {
							$scope.subMenu = [{
								title: 'Fields',
								active: true
							}, {
								title: 'Sample',
								active: false
							}, {
								title: 'Profile',
								active: false
							}];
						}
				} else if($scope.activeTab == "Explore") {
					$scope.subMenu = [{
						title: 'Features',
						active: true
					}, {
						title: 'Correlation',
						active: false
					}]
				} else if($scope.activeTab == "Relationship") {
					$scope.subMenu = [];
				} else if($scope.activeTab == "Feedback") {
					$scope.subMenu = [];
				} else if($scope.activeTab == "Approval"){
					$scope.subMenu = [];
				} else if($scope.activeTab == "Dashboard"){
					$scope.subMenu = [];
				} else if($scope.activeTab == "Lineage"){
					$scope.subMenu = [];
				}else {
					$scope.subMenu = [];
				};
				
				if($scope.subMenu.length > 0){
					$scope.onClickSubMenuTab($scope.subMenu[0])
				} else {
					$scope.activatedSubMenu = "";
				}
					
		});
		
		$scope.initRating = function(){
			$http({
				method: "POST",
				url: "/getAvgRatingForDataSource?dataSourceId="+$scope.datasource.id
			}).success(function(data){
				$scope.userDetailsForAverageRating = data;
				$scope.showRating(data);
			}).error(function(data){
				logger.logError(data);
			});
		};
		
		$interval(function() {
			$scope.initRating();	
		}, 10000);
		
		
		$scope.showRating = function(data){
			for(var i = 0; i< data.length; i++){
				if(data[i].rating == 1){
					$("#starOne11").addClass("checked");
					
					$scope.star = {
							starOne1: 1,
							starTwo2: 0,
							starThree3: 0,
							starFour4: 0,
							starFive5: 0
					};
					
					$("#starTwo22").removeClass("checked");
					$("#starThree33").removeClass("checked");
					$("#starFour44").removeClass("checked");
					$("#starFive55").removeClass("checked");
				}else if(data[i].rating == 2){
					$("#starOne11").addClass("checked");
					$("#starTwo22").addClass("checked");
					
					$scope.star = {
							starOne1: 1,
							starTwo2: 1,
							starThree3: 0,
							starFour4: 0,
							starFive5: 0
					};
					
					$("#starThree33").removeClass("checked");
					$("#starFour44").removeClass("checked");
					$("#starFive55").removeClass("checked");
				}else if(data[i].rating == 3){
					$("#starOne11").addClass("checked");
					$("#starTwo22").addClass("checked");
					$("#starThree33").addClass("checked");
					
					$scope.star = {
							starOne1: 1,
							starTwo2: 1,
							starThree3: 1,
							starFour4: 0,
							starFive5: 0
					};
					
					$("#starFour44").removeClass("checked");
					$("#starFive55").removeClass("checked");
				}else if(data[i].rating == 4){
					$("#starOne11").addClass("checked");
					$("#starTwo22").addClass("checked");
					$("#starThree33").addClass("checked");
					$("#starFour44").addClass("checked");
					
					$scope.star = {
							starOne1: 1,
							starTwo2: 1,
							starThree3: 1,
							starFour4: 1,
							starFive5: 0
					};
					
					$("#starFive55").removeClass("checked");
				}else if(data[i].rating == 5){
					$("#starOne11").addClass("checked");
					$("#starTwo22").addClass("checked");
					$("#starThree33").addClass("checked");
					$("#starFour44").addClass("checked");
					$("#starFive55").addClass("checked");
					$scope.star = {
							starOne1: 1,
							starTwo2: 1,
							starThree3: 1,
							starFour4: 1,
							starFive5: 1
					};
				}
			}
		}
		
		//Dashboard changes
		$scope.projectData = false;
		$scope.userData = false;
		$scope.feedbackData = false;
		$scope.piiData = false;
		$scope.apiData = false;
		
		$scope.clickUserDetails = function(){
			$http.post("/getDataDashboardUserDetails").then(function(response){
				$scope.userData = response.data;
			});
			
			$scope.userData = true;
			$scope.feedbackData = false;
			$scope.projectData = false;
			$scope.piiData = false;
			$scope.apiData = false;
		};
		
		$scope.clickFeedbackDetails = function(){
			$http.post("/fetchDataSourceFeedback"+$scope.datasource.id).then(function(response){
				$scope.feedbackObj = response.data;
			});
			$scope.feedbackData = true;
			$scope.userData = false;
			$scope.projectData = false;
			$scope.piiData = false;
			$scope.apiData = false;
		};
		
		$scope.clickPrjectDetails = function(){
			
			$scope.feedbackData = false;
			$scope.userData = false;
			$scope.projectData = true;
			$scope.piiData = false;
			$scope.apiData = false;
		};
		
		$scope.clickPIIDetails = function(){
			$scope.fieldData = _.filter($scope.datasource.fieldMappings,function(val){
				if(false !== val.pIIId){
					return true;
				}
				return false;
			});
				
			$scope.feedbackData = false;
			$scope.userData = false;
			$scope.projectData = false;
			$scope.piiData = true;
			$scope.apiData = false;
		};
		
		$scope.clickAPIDetails = function(){
			$http.post("/getDeployedDSApiByDatasourceId?dataSourceId="+$scope.datasource.id).success(function(data){
				$scope.apiList = data;
			});
			$scope.feedbackData = false;
			$scope.userData = false;
			$scope.projectData = false;
			$scope.piiData = false;
			$scope.apiData = true;
		};
		
		$scope.$on("myEvent", function (event, args) {
			$scope.checkDataType = args;
		});

		$scope.dismissModel = function(){
			$routeParams.param3 = undefined;
			$routeParams.param1 = undefined;
			$scope.modelCheck = undefined;
			$routeParams.param6 = undefined;
			$routeParams.param10 = undefined;
			$routeParams.param4.close("");
		}; 
		$scope.AddFieldMapping = function(){
			if($scope.datasource.appId == null || $scope.datasource.appId == undefined || $scope.datasource.appId == ""){
				$scope.datasource.appId = 0;
				$scope.datasource.nodeId = 0;
			} 

			var reqDataObject = {'appId' : $scope.datasource.appId, 'nodeId' : $scope.datasource.nodeId};
			$http.post("/getDataSourceJson", reqDataObject).then(function(response){
				var modalInstance;
				modalInstance = $modal.open({
					size: "xl",
					templateUrl: "/views/template/fieldAddController.html",
					controller: "fieldAddController",
					resolve : {
						processData : function(){
							if(response !== null){
								return response.data;
							}
							else
								return null; 
						},
						datasource : function(){
							return angular.copy($scope.datasource);
						}
					}
				});
				modalInstance.result.then(function (paramFromDialog) {
					$scope.datasource = paramFromDialog;

					$scope.datasource.availableToUse = undefined;
					$http({
						url: "/saveDataSource",
						method: 'Post',
						data : $scope.datasource
					}).then(function(response) {
						DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
							$scope.datasource = data;
							$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
							$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion); 
							$scope.$emit("shareDataSourceToAllController", $scope.datasource);
						});
						logger.logSuccess("Field Mapping updated successfully");
						if($routeParams.param4 === undefined || $routeParams.param4 === null)
							$route.reload();
					}, 
					function(response) {
						logger.logError("Something went wrong");
					});	
				});
			}); 
		};

		/**
		 * service to get datasource based on data source name url :
		 * /getDataSource?datasourcename= param : $scope.selected
		 * 
		 */
		$scope.ingections = "";
		$scope.popUpValue = false; 

		$scope.$on("metaData" , function(event , args){
			
			DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
				$scope.datasource  = data;

				if($scope.datasource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
				$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
				$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion); 
				$scope.$emit("shareDataSourceToAllController",$scope.datasource);
			});
		});
		
		var backUp;

		$scope.fetchFieldMappingForHbase = function(){
			if($scope.datasource.primaryKeysForIndex != null && $scope.datasource.primaryKeysForIndex.length > 0){
				var i = 1;
				_.each($scope.datasource.primaryKeysForIndex, function(value){
					$scope.selectRejectValueForPrimary[i] = _.reject($scope.selectRejectValueForPrimary[i - 1], function(val){
						return val.fieldName === value.fieldName; 
					})
					i++;
				});
			}
			if($scope.datasource.secondaryKeys != null && $scope.datasource.secondaryKeys.length > 0){
				var i = 1;
				_.each($scope.datasource.secondaryKeys, function(value){
					$scope.selectRejectValueForSecondary[i] = _.reject($scope.selectRejectValueForSecondary[i - 1], function(val){
						return val.fieldName === value.fieldName; 
					})
					i++;
				});
			}
		};
		
		$scope.initMethod = function() {
			$scope.piiCount = 0;
			 _.filter($scope.datasource.fieldMappings,function(val){
				if(val.pIIId == true){
					$scope.piiCount = $scope.piiCount + 1
					return $scope.piiCount; 
				}
			});
			
			$http.post("/getDataDashboardCountDetails?dataSourceId="+$scope.datasource.id).success(function(data){
				$scope.dashboard = data;
				$scope.dashboard.push({'count': $scope.piiCount, 'name': 'Pii'});
				$scope.clickPrjectDetails();
				$scope.initRating();
			});
			
		};
		
		$scope.getUpdatedRelation = function(){
			//RELATIONS
			$http.post("/relationship/getDsDagData",$scope.dsObject.id).success(function(data){
				$scope.dsDagData=data;
			}).error(function(data){
				logger.logError(data);
			});
		}
		
		$scope.dsObject = null;
		
		$scope.myApplicationTab = false;
		DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
			$scope.datasource  = data;
			$scope.dsObject = data;
			$cookieStore.put('dsownerid',$scope.datasource.createdBy);
			
			
			
//			$scope.init=function(){
//				Relations.getWithoutObject("/relationship/fetchAvailableRelations").then(function(data){
//					$scope.allRelationsDagData=data;
//				});
//			}
			$scope.init = function(){
				//RELATIONS
				$http.post("/relationship/getDsDagData",$scope.datasource.id).success(function(data){
					$scope.dsDagData=data;
				}).error(function(data){
					logger.logError(data);
				});
			}();
			
			
			$scope.openConfigureRelationPopup=function(){

				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "views/template/configureRelationPopup.html",
					controller: "createRelationController",
					size:'lg'
			 }),modalInstance.result.then(function(obj) {
				 logger.logSuccess("all relations has been saved successfully")
				 $log.info("Modal closed at: " + new Date);
				 $scope.getUpdatedRelation();
			 }, function(obj) {
				 $log.info("Modal dismissed at: " + new Date);
			 });

			}
			
			$rootScope.dsHistorySnapshotId = $scope.datasource.id; 

			$scope.selectRejectValueForPrimary[0] = $scope.datasource.fieldMappings;
			$scope.selectRejectValueForSecondary[0] = $scope.datasource.fieldMappings;
			$scope.fetchFieldMappingForHbase();
			backUp = angular.copy($scope.datasource.ingection);
			
			$rootScope.advancedDisabled = ($scope.datasource.dataSourceType == 'NOSQL' ? true : false);
			
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

			


			if($scope.appId != undefined && $scope.appId > 0){
				var reqDataObject = {'appId' :$scope.appId, 'dataSourceId' : $scope.datasource.id};
				$http.post("/getDSAppParam", reqDataObject).success(function(data){
					if(data.length > 0){
						$scope.appSetting = {use : "Refreshed", globalParam : null};
						if(!(data[0].globalParamName == "Refreshed")){
							_.each(data,function(val){
								if(val.globalParamName == "BasePath"){
									$scope.appSetting.globalParam = {name: val.globalParamValue};
								}else{
									$scope.list.push({name:val.globalParamValue});
								}
							});
						}
					}
				});
			}
			
			$scope.indexDetails = {indexName : ""}
			if($scope.datasource.indexId !== null){
				$http.post("/getIndexNameByIndexId",$scope.datasource.indexId).success(function(data){
					$scope.indexDetails.indexName = data;
				});
			}

			var count = -1;

			_.each($scope.datasource.fieldMappings, function(key){
				count++;

				if(key.transformation !== null && key.transformation.length > 0){
					$scope.transformationChkBox[count] = true;
				}else{
					$scope.transformationChkBox[count] = false;
				}
				if(key.dataValidations !== null && key.dataValidations.length > 0){
					$scope.validationChkBox[count] = true;
				}else{					
					$scope.validationChkBox[count] = false;
				};				
				if(key.preIngestion !== null && key.preIngestion !== false){
					$scope.distinctProfilePreChkBox[count] = true;
				}else{
					$scope.distinctProfilePreChkBox[count] = false;
				}
				if(key.postIngestion !== null && key.postIngestion !== false){
					$scope.distinctProfilePostChkBox[count] = true;
				}else{
					$scope.distinctProfilePostChkBox[count] = false;
				}
				if(key.preValueFrequency !== null && key.preValueFrequency !== false){
					$scope.distinctPreValueFreqChkBox[count] = true;
				}else{
					$scope.distinctPreValueFreqChkBox[count] = false;
				}
				if(key.posValueFrequency !== null && key.posValueFrequency !== false){
					$scope.distinctPostValueFreqChkBox[count] = true;
				}else{
					$scope.distinctPostValueFreqChkBox[count] = false;
				}

			});


			//This is for fetching data in HDFS and HBASE
			if($scope.datasource.fileStoreObject == undefined || $scope.datasource.fileStoreObject == null){ 
				$scope.datasource.fileStoreObject = $scope.storageStrategy;
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
			}else if($scope.datasource != undefined && $scope.datasource != null && $scope.datasource.primaryKeysForIndex == null){
				$scope.datasource.primaryKeysForIndex = [];
			}
			else if($scope.datasource != undefined && $scope.datasource != null && $scope.datasource.secondaryKeys == null){
				//				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];

			}

			if($scope.datasource.fileStoreObject == $scope.storageStrategy){
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
			}else if($scope.datasource.fileStoreObject == "HBASE"){
				$scope.hbase.primary = $scope.datasource.primaryKeysForIndex;
				$scope.hbase.secondary = $scope.datasource.secondaryKeys;
			}
			
			

			$scope.disableCheck.preMyIndicator = angular.copy($scope.datasource.preIngestion);
			$scope.disableCheck.postMyIndicator = angular.copy($scope.datasource.postIngestion);
			$rootScope.dataSourceId = data.id;
			$scope.initMethod();
			$scope.$emit("shareDataSourceToAllController", $scope.datasource);
			$scope.$broadcast("setNoSqlUIElements", $scope.datasource);
		});

		/**
		 * Get Data source Instance
		 */


		$scope.activateFormat = function(index , fieldDataType){
			if(fieldDataType !== 'DATE'){
				$scope.check[index] = true;
			}else{
				$scope.check[index] = false;
			}
		};
		
		$scope.getDataSourceInstance = function(){
			DataSourceService.getDataSource("/getDataSourceInstance",$scope.selected ).then(function(data){
				$scope.currentgroupId = data.groupId;
				$scope.cookiesGroupId = $cookieStore.get('groupId');
				
				if($scope.cookiesGroupId !== $scope.currentgroupId){
					$scope.myApplicationTab = true;
				}
				
				$scope.dataSourceInstance  = data;
				if($scope.dataSourceInstance.ingection === "quick"){
					$scope.ingections = "Basic";
	    	  	}else{
					$scope.ingections = "Advance";
				}
				//To check status of datasource is requested or accepted
				$scope.GetRequestStatusOfDatasource($scope.dataSourceInstance);

				$scope.RepoName = $location.search().dataRepoName;

				if($scope.dataSourceInstance.totalRecords === 0){
					$scope.checked = false;
				}
				else{
					$scope.checked = true;
				}
				
				if($scope.appId !== undefined && $scope.appId >0){
					if($scope.appId !== $scope.dataSourceInstance.appId){
						$scope.popUpValue = true; 
					} else {
						$scope.myApplicationTab = true;
						$scope.appSetting = {use : "Refreshed", globalParam : null};
					}
				}
				$scope.isFirst = false;
			});	
		}
		
		$scope.isFirst = true;
		if($scope.isFirst){
			$scope.getDataSourceInstance();
		}
		$interval(function() {
			if($rootScope.DsRestored || $rootScope.instanceSaved){
				$scope.getDataSourceInstance();	
			}
		}, 1000);
		

		$scope.$on("maxiqCommons", function (event, args) {
			$scope.maxiqCommonsAdv = args;
			$scope.maxiqCommonsAdvUnique = args;
			$scope.maxiqCommonsAdvUnique = _.uniq(args,function(obj){
				return obj.keyOf; 
			});
			$scope.maxiqCommonsAdv = _.uniq(args,function(obj){
				return obj.value;
			});
		});

		$scope.selectRejectValueForPrimary = [];

		$scope.addPrimaryKeyValue = function(primaryKey, index){

			$scope.selectRejectValueForPrimary[index+1] = _.reject($scope.selectRejectValueForPrimary[index] , function(data){
				return data.fieldName == primaryKey.fieldName;
			});

			if(primaryKey == undefined){
				logger.logError("Please select Primary key and add it.");
				return false;
			}
			$scope.datasource.primaryKeysForIndex.push(primaryKey);
			$scope.hbase.primary = null; 
		};

		$scope.deletePrimaryValue = function(index){
			var object = _.find($scope.selectRejectValueForPrimary[0] , function(values){
				return $scope.datasource.primaryKeysForIndex[index].fieldName == values.fieldName; 
			});

			var length = $scope.selectRejectValueForPrimary.length;

			for(var i = index ; i < length-1 ; i++){
				$scope.selectRejectValueForPrimary[i] = angular.copy($scope.selectRejectValueForPrimary[i+1]);
				$scope.selectRejectValueForPrimary[i].push(object);
			}
			$scope.selectRejectValueForPrimary.pop();
			$scope.datasource.primaryKeysForIndex.splice(index, 1);
		};
		$scope.selectRejectValueForSecondary = [];

		$scope.addSecondaryKeyValue = function(secondaryKey, index){

			$scope.selectRejectValueForSecondary[index+1] = _.reject($scope.selectRejectValueForSecondary[index] , function(data){
				return data.fieldName == secondaryKey.fieldName;
			});

			if(secondaryKey == undefined){
				logger.logError("Please select Secondary key and add it.");
				return false;
			}

			$scope.datasource.secondaryKeys.push(secondaryKey);
			$scope.hbase.secondary = null; 
		};
		$scope.deleteSecondaryValue = function(index){
			var object = _.find($scope.selectRejectValueForSecondary[0] , function(values){
				return $scope.datasource.secondaryKeys[index].fieldName == values.fieldName; 
			});

			var length = $scope.selectRejectValueForSecondary.length;

			for(var i = index ; i < length-1 ; i++){
				$scope.selectRejectValueForSecondary[i] = angular.copy($scope.selectRejectValueForSecondary[i+1]);
				$scope.selectRejectValueForSecondary[i].push(object);
			}
			$scope.selectRejectValueForSecondary.pop();
			$scope.datasource.secondaryKeys.splice(index, 1);
		};

		$scope.saveRestFile = function() {
			$scope.datasource.availableToUse = undefined;
			
			if($scope.datasource.ingection == "typical" && $scope.datasource.fileStoreObject === "SOURCE"){
				logger.logError("You cannot choose SOURCE with Advanced data Ingestion method.\n Please select Basic data ingestion method to select SOURCE.");
				$scope.datasource.fileStoreObject = $scope.storageStrategy;
				return false;
			}
			
			if($scope.datasource.fileStoreObject === $scope.storageStrategy){

				if($scope.datasource.dataAtRestFileType === undefined){
					logger.logError("Please Select File Type.");
					return false;
				}

				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];

			}else if($scope.datasource.fileStoreObject === "SOURCE"){
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
			}else {
				if($scope.datasource.primaryKeysForIndex.length === 0){
					logger.logError("Please add primary key");
					return false;
				}
				if($scope.datasource.fileStoreObject === "SOURCE"){
					$scope.saveForVisualization=false;
				}
				$scope.datasource.dataAtRestFileType = null;
				$scope.datasource.dataAtRestCompressionType = null;
			}
			$scope.datasource.availableToUse=undefined;
			$http({
				url: "/saveDataSource",
				method: 'Post',
				data : $scope.datasource
			}).success(function(data, status, headers, config){
				refreshedDataSource(true);
				logger.logSuccess('Saved successfully.');
			}).error(function(data, status, headers, config){
				logger.logError(data);
			});

		}

		$scope.actInactAll = function(type , value){
			var count = 0;
			if(type.toUpperCase() == "PRE"){
				value = !$scope.datasource.preIngestion;
				if(value == false)
					_.each($scope.datasource.fieldMappings , function(values ){

						if(values.fieldDataType.toUpperCase() != 'STRING' && values.fieldDataType.toUpperCase() != 'DATE'){
							$scope.distinctProfilePreChkBox[count] = value;
							$scope.datasource.fieldMappings[count].preIngestion = value;
						}
						$scope.distinctPreValueFreqChkBox[count] = value;
						$scope.datasource.fieldMappings[count].preValueFrequency = value;
						count++;
					});
			}else{
				value = !$scope.datasource.postIngestion;
				if(value == false)
					_.each($scope.datasource.fieldMappings , function(values){

						if(values.fieldDataType.toUpperCase() != 'STRING' && values.fieldDataType.toUpperCase() != 'DATE'){
							$scope.distinctProfilePostChkBox[count] = value;
							$scope.datasource.fieldMappings[count].postIngestion = value;
						}
						$scope.distinctPostValueFreqChkBox[count] = value;
						$scope.datasource.fieldMappings[count].posValueFrequency = value;
						count++;
					});
			}
		},

		$scope.openTransformation = function(value){

			if($scope.popUpValue === true){
				return false;
			}

			if(_.isUndefined(value.fieldName)){
				logger.logError("Please check the Field Name");
				return;
			}

			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/advancedTransformationModal.html",
				controller: "createNewAdvancedTransformationModalCntr",
				resolve:{
					length : function(){
						if(value.transformation != null &&  value.transformation != undefined)
							return value.transformation.length;
						else
							return 0;
					},
					existingDataSource : function(){
						return $scope.datasource;
					},
					fieldMappings : function(){
						return  value;
					}
				}
			}),modalInstance.result.then(function() {
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
					$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion); 
					$scope.$emit("shareDataSourceToAllController", data);
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		},

		$scope.injunctionChange = function(type , index , value){
			if(type.toUpperCase() == "PRE")
				$scope.datasource.fieldMappings[index].preIngestion = value;
			else
				$scope.datasource.fieldMappings[index].postIngestion = value;
		},
		$scope.inValueFreqencyChange = function(type , index , value){
			if(type.toUpperCase() == "PRE")
				$scope.datasource.fieldMappings[index].preValueFrequency = value;
			else
				$scope.datasource.fieldMappings[index].posValueFrequency = value;
		},

		$scope.saveFieldMapping = function(fm,indicatorForMsg){
			$rootScope.indicatorForMsg = indicatorForMsg;
			var bool = true;
			var bool1 = true ;
			var bool2 = true ;
			var i = 1;
			angular.forEach(fm,function(val){
				if(val.fieldName === null || val.fieldName === ""){
					bool = false;
					return false; 
				}else if(val.fieldName === undefined){
					bool2 = false;
					return false;
				}
				if(bool !== false && bool2 !== false)
					i++;
			});

			if(bool === false){
				logger.logError("All Field Names are mandatory");
				return false;
			}else if(bool2 === false){
				logger.logError("Invalid Field Name found at "+i);
				return false;
			}
			var bool2 = true;
			angular.forEach(fm,function(val){
				if(val.fieldName.indexOf(" ") !== -1){
					bool1 = false;
					return false; 
				}else if(val.fieldName.substring(0 , 3).toLowerCase() === "gp_"){
					bool2 = false;
					return false;
				}
			});

			if(bool1 === false){
				logger.logError("Spaces are not allowed in Field Name");
				return false;
			}else if(bool2 === false){
				logger.logError("Field Name starts with gp_ OR GP_ which is invalid");
				return false;
			};
			if($scope.datasource.nodeId !== null){
				var reqDataObject = {'appId' : $scope.datasource.appId, 'nodeId' : $scope.datasource.nodeId};
				$http.post("/getDataSourceJson", reqDataObject).then(function(response){
					response.data.input.sources[0].fields = [];
					_.each($scope.datasource.fieldMappings , function(values){
						var object = {"filedName" : "" , "sourceLabel": "" , "select" : true,"fieldSourceEnum" : null , 
								"sourceRefField" : null , "pos" :"" , "source" : "" , "format":"" , "fieldType" : ""};
						object.filedName = values.fieldName;
						object.sourceLabel = $scope.datasource.dataSourceName;
						object.fieldSourceEnum = null;
						object.sourceRefField = null;
						object.pos = values.position;
						object.format = values.format;
						object.fieldType = values.fieldDataType;
						object.source = $scope.datasource.dataSourceName;
						response.data.input.sources[0].fields.push(object);
					});
					$http({
						url:"/getValidationMassage",
						method: "POST",
						data : response.data
					}).success(function(data){
						if(data !== "SUCCESS"){
							logger.logError("Not able to save : " +data);
							return false;
						}else{
							if(null == $scope.datasource.appId || undefined == $scope.datasource.appId){
								$scope.impactedApps();
							}else{
								$scope.saveAll();
							}

						};
					});
				});
			}else{
				if((null == $scope.datasource.appId || undefined == $scope.datasource.appId) && null != fm){
					$scope.impactedApps();
				}else{
					$scope.saveAll();
				}

			};
		};
		
		function refreshedDataSource (isSaveForVisualization){
			DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
				$scope.datasource  = data;
				backUp = angular.copy($scope.datasource.ingection);
				if($scope.datasource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
				$scope.datasource.availableToUse = "true";

				$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


				if($scope.datasource !== null && 
						$scope.datasource.dataSourceType === "RDBMS_DATA_SOURCE"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "RDBMS_DATA_SOURCE"
					};

				}

																
				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
					$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
				};
																

																
				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
					$scope.fileDataIngesterDetails.delimiter = "\\t";
				}

				$scope.$emit("shareDataSourceToAllController", $scope.datasource);
				
				//If File saved then reload list of datasources
				$rootScope.isDataSourceFileSaved = true;
				
				if(isSaveForVisualization){
					$scope.saveForVisualize();
				}
			});
		};

		$scope.impactedApps = function() {
			$scope.saveAll();
		}

		$scope.saveAll = function(){
			var category = '';
			var subCategory = '';
			var description = '';
			if($scope.datasource.category != null || $scope.datasource.subCategory != null || $scope.datasource.description != null || $scope.datasource.tags != null){
				category = window.encodeURIComponent($scope.datasource.category);
				subCategory = window.encodeURIComponent($scope.datasource.subCategory);
				description = window.encodeURIComponent($scope.datasource.description);
			}else{
					var category = '';
					var subCategory = '';
					var description = '';
					$scope.datasource.tags = [];
			}
			
			$http({
				url:"/updateAllFieldMappings?datasourceId="+$scope.datasource.id+"&category="+category+"&subCategory="+subCategory+"&descriptions="+description+"&tags="+$scope.datasource.tags,
				method: "POST",
				data : $scope.datasource.fieldMappings
			}).success(function(data, status, headers, config){
				$rootScope.isDataSourceFileSaved = true;
				if(data == null || data == "null" || data == ""){
					if($rootScope.indicatorForMsg == "Details"){
						logger.logSuccess('Details saved successfully');
					}else
						logger.logSuccess('Field Mapping saved successfully');
					
					DataSourceService.getDataSource("/getDataSource" ,$scope.selected).then(function(data){
						$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
						$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion); 
						$scope.$emit("shareDataSourceToAllController", $scope.datasource);
					});
				}else
					logger.logError(data);

			}).error(function(data, status, headers, config){
				logger.logError(data);
				if($routeParams.param4 === undefined || $routeParams.param4 === null)
					$route.reload();
			});
		};

		$scope.validationTypes = ["NOT_NULL","LENGTH_CHECK"];
		$scope.open = function(fieldMapping){

			if($scope.popUpValue === true){
				return false;
			}
			if(_.isUndefined(fieldMapping.fieldName)){
				logger.logError("Please check the Field Name");
				return;
			}
			var modalInstance;

			modalInstance = $modal.open({
				templateUrl: "/views/template/advancedValidationModal.html",
				controller: "createValidationModalCntr",
				resolve: {

					length : function(){
						if(fieldMapping.dataValidations != null && fieldMapping.dataValidations != undefined)
							return fieldMapping.dataValidations.length;
						else
							return 0;
					},
					validationType : function(){
						return $scope.validationTypes;
					},
					fieldMappings : function(){
						return  fieldMapping;
					},
					dataSource : function(){
						return  angular.copy($scope.datasource);
					}
				}
			}),modalInstance.result.then(function(returnValue) {
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
					$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion); 
					$scope.$emit("shareDataSourceToAllController", data);
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};

		$scope.saveForVisualize = function(){
			$scope.datasource.availableToUse = undefined;

			$scope.indexDetails= {indexName : $scope.datasource.dataSourceName };
			$http({
				url: "/saveForVisualize?saveForVisualize="+$scope.datasource.saveForVisualize+"&dataSourceId="+$scope.datasource.id+"&isFieldsChange="+false,
				method: 'Post',
				data: $scope.indexDetails 
			}).then(function(response) {
				$scope.$emit("shareDataSourceToAllController", response.data);
			}, function(response) {
				logger.logError(response.data);
			});
			//	}
		};

		$scope.saveProfilingOption = function(){
			$scope.datasource.availableToUse = undefined;
			$http({
				url: "/saveDataSource",
				method: 'Post',
				data : $scope.datasource
			}).then(function(response) {
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
					$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion);
					$scope.actInactAll("PRE",$scope.disableCheck.preMyIndicator);
					$scope.actInactAll("POST",$scope.disableCheck.postMyIndicator);
					logger.logSuccess("Profiling option saved successfully");
				});
			}, 
			function(response) {
				logger.logError("Something went wrong");
			});
		};

		//DataRequest For
		var RFA = "Request access";		
		var PENDING = "Pending";
		var REJECT = "Reject";
		var ACCEPT = "Accept";
		var REMOVE = "Remove";
		var ADDTOCOLLECTION = "Add To Collection"
			var REMOVE_FRM_COLL = "Remove From My Collection";

		$scope.requestForDatasource = function(dataSourceInstance, justification, dataSourceRequestLabel) {
			if(dataSourceRequestLabel==RFA){
				$http({
					method : "post",
					url : "/createRequestForDatasource?datasourceid="+dataSourceInstance.dataSourceId+"&justification="+justification
				}).success(function(data){
					if(data=="success"){
						logger.logSuccess("Request sent for permission.");	
						$scope.dataSourceRequestLabel = "Requested";
						$scope.dataSourceRequestDisable = true;
					}else{
						logger.logError("failed to save");
					}
				}).error(function(status){
					logger.logError(status);
				});	
			}else if(dataSourceRequestLabel == ADDTOCOLLECTION){
				//Add to My Collection
				var reqDataObject = { objectId : dataSourceInstance.dataSourceId , objectType : DATA_SOURCE};
				$http({
					method : "post",
					url : "/addToMyCollection",
					data : reqDataObject
				}).success(function(data){
					if(data=="success"){
						logger.logSuccess("Added successfully");	
						$scope.dataSourceRequestLabel = REMOVE_FRM_COLL;
						$scope.dataSourceRequestDisable = false;
					}else{
						logger.logError("failed to save");
					}
				}).error(function(status){
					logger.logError(status);
				});
			}else if(dataSourceRequestLabel == REMOVE_FRM_COLL){
				//Remove from My Collection
				var reqDataObject = { objectId : dataSourceInstance.dataSourceId , objectType : DATA_SOURCE};
				$http({
					method : "post",
					url : "/removeFromMyCollection",
					data : reqDataObject
				}).success(function(data){
					if(data=="success"){
						logger.logSuccess("Removed successfully");	
						$scope.dataSourceRequestLabel = ADDTOCOLLECTION;
						$scope.dataSourceRequestDisable = false;
						$rootScope.refreshCollectionList = true;
					}else{
						logger.logError("failed to save");
					}
				}).error(function(status){
					logger.logError(status);
				});
			}

		};

		$scope.GetRequestStatusOfDatasource = function(dataSourceInstance) {
			//Get Status of DataSource request - pending, rejected, approved
			var reqDataObject = {objectId : dataSourceInstance.dataSourceId, objectType : "DATA_SOURCE"};
			$http({
				method : "post",
				url : "/getCollectionAndRequestStatusOfDatasource",
				data : reqDataObject
			}).success(function(data){
				if(data == PENDING){
					$scope.dataSourceRequestLabel = "Requested";
					$scope.dataSourceRequestDisable = true;
				}else if(data == REJECT){
					$scope.dataSourceRequestLabel = "Rejected";
					$scope.dataSourceRequestDisable = true;
				}else if(data == ACCEPT){
					$scope.dataSourceRequestLabel = ADDTOCOLLECTION;
					$scope.dataSourceRequestDisable = false;
				}else if(data == REMOVE){
					$scope.dataSourceRequestLabel = REMOVE_FRM_COLL
				}else{
					$scope.dataSourceRequestLabel = RFA;
				}

			}).error(function(status){
				logger.logError(status);
			});
		};
		
		


	}]).controller("impactionOnAppCtrl",["$scope", "$modal" , "$log" ,"$http", "logger","$modalInstance", "impactedAppsList", "componentType",
	                                     function($scope, $modal , $log, $http, logger, $modalInstance, impactedAppsList, componentType) {
		$scope.impactedAppsList = impactedAppsList;
		$scope.componentType = componentType;
		$scope.selectAction = function(flag) {
			if("true" == flag){
				$scope.goAheadToSaveFieldmapping = true;
			}else if("false" == flag){
				$scope.goAheadToSaveFieldmapping = false;
			}
			$modalInstance.close($scope.goAheadToSaveFieldmapping);
		}

	}]).controller("connectionController" , ["DataSourceFactory","$routeParams" , "$log", "$modal","$scope" , "$upload" ,  "DataSourceService", "logger"  , "$http", "uniqueArray","Fullscreen", "$window" , "$location", "$rootScope", function(DataSourceFactory, $routeParams ,$log, $modal,$scope , $upload , DataSourceService , logger ,$http, uniqueArray,Fullscreen, $window, $location, $rootScope){
		$scope.hiddenIndicatorForRdbms = DataSourceFactory.data;
		$scope.connections = [];

		//$scope.externalConnections = ["Google Analytics", "AmazonS3", "Salesforce", "Dropbox"];

		$scope.connObj = {"connectionName" : null}; 
		
		
		Array.prototype.contains = function(obj) {
		    var i = this.length;
		    while (i--) {
		        if (this[i] === obj) {
		            return true;
		        }
		    }
		    return false;
		}

		$scope.getStreamData = function(){
			$scope.connectionsDetails = [];
			 _.each($scope.connections , function(val){
				if(val.configConnectionType === "STREAM"){
					if(!($scope.connectionsDetails.contains(val.connectorType))){
						$scope.connectionsDetails.push(val.connectorType);
					}
				}
			});

			 if(null !== $scope.datasource.config 
					 && undefined !== $scope.datasource.config 
					 && $scope.datasource.streamType !== null 
					 && $scope.datasource.streamType != undefined){
				 $scope.selectTheStreamType($scope.datasource.streamType);
			 }
		}
		
		$scope.selectTheStreamType = function (type){
			$scope.connectionsNameDetails =  _.filter($scope.connections , function(val){
				return val.connectorType === type ; 
			});
		}

		$scope.differentiateRDBMSAndFTP = function(){
			if($scope.fileDataIngesterDetails !== null &&
					undefined !== $scope.fileDataIngesterDetails &&
					$scope.fileDataIngesterDetails.dataSourceType !== undefined 
					&& $scope.fileDataIngesterDetails.dataSourceType !== null &&
					$scope.fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE'){
				$scope.connectionsDetails = _.filter($scope.connections , function(val){
					return val.configConnectionType === "RDBMS"
				}); 
			}else if($scope.fileDataIngesterDetails !== undefined && 
					$scope.fileDataIngesterDetails !== null  && 
					$scope.fileDataIngesterDetails.dataSourceType !== undefined 
					&& $scope.fileDataIngesterDetails.dataSourceType !== null){
				$scope.connectionsDetails = _.filter($scope.connections , function(val){
					if(null != val){
						return val.configConnectionType != "RDBMS"
					}
				});
			}
		};
		
		$scope.differentiateEXTERNALCONNECTOR = function(){
			
			$scope.externalConnections = _.filter($scope.connections , function(val){
				if(null != val){
					return val.configConnectionType === "EXTERNALCONNECTOR"
				}
			}); 
		}
		$scope.differentiateNosqlConnection = function(){
			$scope.NoSqlConnections = _.filter($scope.connections , function(val){
				return val.configConnectionType === "NOSQL"
			}); 
		}
		
		$scope.$on("setNoSqlUIElements",function(event,args){
			$scope.data=args;
			if($scope.data.config!=undefined){
			if($scope.data.config.configConnectionType==="NOSQL"){
				$scope.NoSqlConnectionType=$scope.data.config.connectorType;
				$scope.schema=$scope.data.config.noSqlSchema;
			}
			}
		});
		
		
		
		$scope.getNoSqlConnections=function(noSqlConnectionType){
			$scope.NoSqlConnections=_.filter($scope.connections , function(val){
				return val.connectorType === noSqlConnectionType;
			});
		}
		
		
		$scope.getExternalConnecton = function(type){
			$scope.externalConnections = _.filter($scope.connections , function(val){
			if(null != val){
			  return val.connectorType === type ;
			}
			});
		}
		var reqDataObject = {'groupName' : "FileFormat", 'keyOf' : "null" };
		

		$scope.refreshMaxiqCommons=function(data){
			$scope.$emit("maxiqCommons", data);
		},

		$scope.$on("hourEvents", function (event, args) {
			$scope.explanation = args;
		});

		$scope.values = {
				xlsSheetName : "",
				xlsFilePathFrom : "",
				xlsFilePathTo : "",
				headerChkBox : false
		};
		$scope.rowSize = 4;
		$scope.resizeText = function(input){
			input = parseInt(input);
			if($scope.rowSize == 20){
				return false;
			}else{
				$scope.rowSize = $scope.rowSize + input;
			}
		};

		$scope.configured = function (externalType,config){
			if(externalType == undefined){
				logger.logError("Please select the external Connection type.");
				return false;
			}
			
			if("GoogleAnalytics" === externalType){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl: "/views/template/GoogleAnalytics.html",
					controller: "GoogleAnalyticsController",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {

				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if("AMAZONS3" === externalType){
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/AmazonS3.html",
					controller: "AmazonS3Controller",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {

				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if("SALESFORCE" === externalType){
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/Salesforce.html",
					controller: "SalesforceController",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {

				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if("DROPBOX" === externalType){
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/Dropbox.html",
					controller: "DropboxController",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {

				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if("FACEBOOKPAGE" === externalType){
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/FacebookPage.html",
					controller: "FacebookController",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {
					$scope.externalData = obj;
					$scope.externalData.facebookPage.facebookAccessToken= $scope.datasource.config.facebookAccessToken;
					$scope.externalData.facebookPage.facebookSecretId= $scope.datasource.config.facebookSecretId;
					$scope.externalData.facebookPage.connectionName= $scope.datasource.config.conncetionName;
					$http({
						url: "/saveExternalDataFacebookPage",
						method: 'Post',
						data :  $scope.externalData
					}).success(function(data, status, headers, config) {
						$scope.dataSource = data;
						logger.logSuccess("Connection details saved successfully");
						refreshedDataSource();
						$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
						$modalInstance.dismiss("cancel");
					}).error(function(data, status, headers, config) { 
						$rootScope.$broadcast("fieldMappingWarning", data);
						$modalInstance.dismiss("cancel");
					});
				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if("HECKYL" === externalType){
				
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/Hackyl.html",
					controller: "hackylController",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {
					$scope.externalData = obj;
					
					$scope.externalData.hackyl.ipUrl= $scope.datasource.config.hackylIPAddress;
					$scope.externalData.hackyl.connectionName= $scope.datasource.config.conncetionName;
					
					$http({
						url: "/saveExternalDataHackyl",
						method: 'Post',
						data :  $scope.externalData
					}).success(function(data, status, headers, config) {
						$scope.dataSource = data;
						logger.logSuccess("Connection details saved successfully");
						refreshedDataSource();
						$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
						$modalInstance.dismiss("cancel");
					}).error(function(data, status, headers, config) { 
						$rootScope.$broadcast("fieldMappingWarning", data);
						$modalInstance.dismiss("cancel");
					});
				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if("PROBE42" === externalType){
				
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/ProbePage.html",
					controller: "ProbeController",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {

				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			} else if("SAPERP" === externalType){
				
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/SapErp.html",
					controller: "SAPERPController",
					resolve:{
						dataSource : function(){
							return $scope.datasource;
						}
					}
				}),modalInstance.result.then(function(obj) {

				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

 			}
		};
		
		$scope.sizeChanger = function(input){
			if($scope.rowSize == 4){
				return false;
			}else{
				$scope.rowSize = $scope.rowSize + input;
			}
		};

		$scope.fullTextarea = function(){
			if (Fullscreen.isEnabled()){
				$scope.myCssApply = "";
				Fullscreen.cancel();
			}
			else{
				$scope.myCssApply = "100%";
				Fullscreen.enable(rdbmsNode);
			}
		};	

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = angular.copy(args);
			$scope.datasource_bk = angular.copy(args);
			$scope.fileDataIngesterDetails = $scope.datasource.fileDataIngesterDetails;


			if($scope.fileDataIngesterDetails == null || $scope.fileDataIngesterDetails == undefined){
				$scope.fileDataIngesterDetails = {
						"availableToUse" : true,
						"containsHeader" :  false+"",
						"dataSourceType" :  "",  
						"delimiter": "",
						"headerStarting" : "1",
						"headerTemplate" : "",
						"isDelimiterRegEx": null,
						"recordLoadLimit" : "",
						"recordStartLine" : "",
						"recordStartsWith" : false+"",
						"recordsIgnore" : "",
						"serverFilePath" :   "",  
						"startingRegularExpre" : "",
						"optionallyEnclosedInDoubleQuotes" : false+""
				};
			}

			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}


			if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
				$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
			};		


			if($scope.datasource !== null && 
					$scope.datasource.dataSourceType === "RDBMS_DATA_SOURCE"){

				$scope.fileDataIngesterDetails = {
						"dataSourceType" : "RDBMS_DATA_SOURCE"
				};
				$scope.myTest("RDBMS_DATA_SOURCE");

			}
		});

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});

		$scope.ingections = "";
		$scope.dataHide = false;
		$scope.xlsObj = [{ 
			xlsFilePath : "",
			xlsFilePathFrom : "",
			xlsFilePathTo : "",
			xlsHeaderOptions : "" 
		}];

		$scope.resetFilePath = function(){
			if($scope.myFile != undefined) {
				$scope.datasource.filePathName = undefined;
			}else{
				$scope.saveConnectionDetails(null);
			}
		},

		$scope.onKeyUp = function(value){
			if(!isNaN($scope.datasource.headerStarting))
				$scope.datasource.recordStartLine = parseInt(value) + 1;
			else{
				logger.logError("Please enter numeric values only");
				$scope.datasource.headerStarting = "";
			}
		},
		$scope.myTest = function (selectDataSource){
			
			$scope.clearFileDataSource(selectDataSource);
			$scope.$emit("myEvent", selectDataSource);
			if(selectDataSource === "RDBMS_DATA_SOURCE"){
				DataSourceFactory.update(false);
			}else {
				DataSourceFactory.update(true);
			}
			if(selectDataSource === "NOSQL"){
				$rootScope.advancedDisabled = true;
			} else {
				$rootScope.advancedDisabled = false;
			}
			$scope.differentiateRDBMSAndFTP();
			if(selectDataSource === "EXTERNAL_DATA"){
				$rootScope.basicDataIngection= true;
			}else{
				$rootScope.basicDataIngection=false;
			}
		};

		$scope.changeRegularExp = function(){
        	if($scope.datasource.dataSourceType === "FIXED_LENGTH_FORMAT"){
			  $scope.fileDataIngesterDetails.recordStartLine="";
        	}
		};
		
		$scope.addXlsField = function() {

			if($scope.values.xlsFilePathFrom == "" || $scope.values.xlsFilePathFrom == undefined || 
					$scope.values.xlsFilePathTo == "" || $scope.values.xlsFilePathTo == undefined
					|| $scope.values.xlsSheetName == "" || $scope.values.xlsSheetName == undefined){
				logger.logError("Please fill mandatory fields");
				return false;
			}

			var object = {
					xlsFilePathFrom : $scope.values.xlsFilePathFrom,
					xlsFilePathTo : $scope.values.xlsFilePathTo,
					xlsHeaderOptions : $scope.values.headerChkBox,
					xlsSheetName : $scope.values.xlsSheetName
			};

			if($scope.fileDataIngesterDetails.excelRanges == null)
				$scope.fileDataIngesterDetails.excelRanges = [];

			$scope.fileDataIngesterDetails.excelRanges.push(object);

			$scope.values.xlsFilePathFrom = "";
			$scope.values.xlsFilePathTo = "";
			$scope.values.headerChkBox = "";
			$scope.values.xlsSheetName = "";

			logger.logSuccess('xls fields added successfully');
		};
		var backUp = "";
		$scope.removeXlsField = function(index){
			$scope.fileDataIngesterDetails.excelRanges.splice(index , 1);
		},
		$scope.selected = $routeParams.param1;

		$scope.$on("fieldMappingWarning", function (event, args) {
			if(args.indexOf("FIELDMAPPING_WARNING$$")!=-1){
				$scope.compareFieldMappingSchema(args.split("$$")[1], $scope.datasource);
			}else {
				logger.logError(args);
			}
		});

		$scope.compareFieldMappingSchema = function (uuId, dataSource) {

			var modalInstance;
			modalInstance = $modal.open({
				size : "xl",
				backdrop: 'static',
				templateUrl: "/views/template/compareFieldMappingSchema.html",
				controller: "compareFieldMappingSchemaCtrl",
				resolve:{
					dataSource : function(){
						return dataSource;
					},
					uuId : function(){
						return uuId;
					}
				}

			}),
			modalInstance.result.then(function (paramFromDialog) {
				$http({
					url: "/deleteCommunicationsDetailsByuuId",
					method: "POST",
					data : uuId
				})

				if("cancel" != paramFromDialog){
					refreshedDataSource();
				}else{
					$scope.datasource_bk.availableToUse=undefined;
					$http({
						url: "/saveDataSource",
						method: 'Post',
						data : $scope.datasource_bk
					}).success(function(data, status, headers, config) {
						refreshedDataSource();
					}).error(function(data, status, headers, config) {
						logger.logError(data);
						return false;
					})
				}

			}, function(result) {
				$log.info("Modal dismissed at: " + new Date);
			});
		}

		function refreshedDataSource (){
			DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
				$scope.datasource  = data;
				backUp = angular.copy($scope.datasource.ingection);
				if($scope.datasource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
				$scope.datasource.availableToUse = "true";

				$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


				if($scope.datasource !== null && 
						$scope.datasource.dataSourceType === "RDBMS_DATA_SOURCE"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "RDBMS_DATA_SOURCE"
					};

					$scope.myTest("RDBMS_DATA_SOURCE");

				} else if($scope.datasource !== null && 
						$scope.datasource.dataSourceType === "NOSQL"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "NOSQL"
					};
					
					$scope.schema = data.config.noSqlSchema;
				}

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
					$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
				};

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
					$scope.fileDataIngesterDetails.delimiter = "\\t";
				}

				$scope.$emit("shareDataSourceToAllController", $scope.datasource);
				
				//If File saved then reload list of datasources
				$rootScope.isDataSourceFileSaved = true;
			});
										}

		angular.element($window).bind("beforeunload", function (event) {
			if(($scope.datasource.fieldMappings == null || ($scope.datasource.fieldMappings.length > 0) == false) && 
					$location.path() !== "/home"){
				return 'Sure?';
			}
		});

		$scope.inputParamCall = function (){
			if($scope.fileDataIngesterDetails.dataSourceType === 'RDBMS_DATA_SOURCE' && undefined == $scope.datasource.config.inputFileType){
				logger.logError("Please Select File Type.");
				return false;
			}

			if($scope.fileDataIngesterDetails.dataSourceType === 'RDBMS_DATA_SOURCE' && undefined == $scope.datasource.config.inputCompressionType){
				logger.logError("Please Select File Compression Type.");
				return false;
			}
			
			if($scope.fileDataIngesterDetails.dataSourceType === 'EXCEL_DATA_SOURCE' && $rootScope.excelData){
				logger.logError("Please fill excel details.");
				return false;
			}

			$scope.datasource.availableToUse = undefined;
			if ($scope.datasource.dsLevelParams !== undefined && $scope.datasource.dsLevelParams !== null
					&& $scope.datasource.dsLevelParams.length > 0
					&& $scope.fileDataIngesterDetails.dataSourceType === "RDBMS_DATA_SOURCE")
			{
				$scope.repo = false;
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/captureInputParam.html",
					controller: "captureValuesForParam",
					resolve:{
						existingDataSource : function(){
							return $scope.datasource;
						},
						repo : function(){
							return $scope.repo;
						}
					}

				}),
				modalInstance.result.then(function (paramFromDialog) {
					$scope.saveConnectionDetails(paramFromDialog);
				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if($scope.fileDataIngesterDetails.dataSourceType==='NOSQL'){
				$scope.saveNoSqlConnectionDetails();
			}
			else{
				$scope.saveConnectionDetails(null);
			}
		},

		$scope.saveDataSourceFromData = function(_serverObj){
			// sending data to server in normal way
			if( (undefined == $scope.myFile && undefined != $scope.fileDataIngesterDetails.serverFilePath)){
				$http.post("/connectionDetails",_serverObj).success(function(data, status, headers, config){
					refreshedDataSource();
					logger.logSuccess("Connection details saved successfully");
				}).error(function(data, status, headers, config){
					if(data.indexOf("FIELDMAPPING_WARNING$$")!=-1){
						$scope.datasource.dataSourceType = 'FIXED_LENGTH_FORMAT';
						$scope.compareFieldMappingSchema(data.split("$$")[1], $scope.datasource);

					}else {
						logger.logError(data);
					}
				});
			}else if(undefined != $scope.myFile && 
					(undefined == $scope.fileDataIngesterDetails.serverFilePath 
							|| "" == $scope.fileDataIngesterDetails.serverFilePath)){
				// sending file data along with form data to server if file is
				// present
				promise = $upload.upload({
					url:"/Fileupload",
					data: _serverObj,
					file: $scope.myFile
				}).success(function(data, status, headers, config) {
					$rootScope.isDataSourceFileSaved = true;
					refreshedDataSource();
					logger.logSuccess('Connection details saved successfully');
				}).error(function(data, status, headers, config) {
					if(data.indexOf("FIELDMAPPING_WARNING$$")!=-1){
						$scope.datasource.dataSourceType = 'FIXED_LENGTH_FORMAT';
						$scope.compareFieldMappingSchema(data.split("$$")[1], $scope.datasource);
					}else {
						logger.logError(data);
					}
				});
			}else if(undefined != $scope.myFile && undefined != $scope.fileDataIngesterDetails.serverFilePath
					&& "" !=  $scope.fileDataIngesterDetails.serverFilePath){
				logger.logError("Please Provide Either File Or File Path Not Both.");
				return false;
			}

		}

		$scope.saveConnectionDetails = function(paramFromDialog){

			if($scope.fileDataIngesterDetails.recordStartsWith == "true" 
				&& $scope.fileDataIngesterDetails.dataSourceType !== 'RDBMS_DATA_SOURCE' &&
				$scope.fileDataIngesterDetails.dataSourceType !== 'FTP_DATA' &&
				$scope.datasource.ingection !== "quick" &&
				($scope.fileDataIngesterDetails.startingRegularExpre === "" || 
						$scope.fileDataIngesterDetails.startingRegularExpre === undefined || 
						$scope.fileDataIngesterDetails.startingRegularExpre === null)
			){
				logger.logError("Please enter regular expression");
				return false;
			}
			else if($scope.datasource.ingection !== "quick" &&
					$scope.fileDataIngesterDetails.dataSourceType !== 'RDBMS_DATA_SOURCE' &&
					$scope.fileDataIngesterDetails.dataSourceType !== 'FTP_DATA' &&
					$scope.fileDataIngesterDetails.dataSourceType !== 'EXCEL_DATA_SOURCE' && 
					$scope.fileDataIngesterDetails.dataSourceType !== 'REMOTE_DATA' && 
					$scope.fileDataIngesterDetails.recordStartsWith == "false" &&
					($scope.fileDataIngesterDetails.recordStartLine == "" ||
							$scope.fileDataIngesterDetails.recordStartLine == null ||
							$scope.fileDataIngesterDetails.recordStartLine == undefined) ){
				logger.logError("Please enter record start line expression");
				return false;
			}else if($scope.fileDataIngesterDetails.containsHeader == "true" &&
					$scope.fileDataIngesterDetails.dataSourceType !== 'RDBMS_DATA_SOURCE' &&
					$scope.fileDataIngesterDetails.dataSourceType !== 'FTP_DATA' &&
					$scope.fileDataIngesterDetails.dataSourceType !== 'EXCEL_DATA_SOURCE' &&
					$scope.fileDataIngesterDetails.dataSourceType !== 'REMOTE_DATA' &&
					($scope.fileDataIngesterDetails.headerStarting == null ||
							$scope.fileDataIngesterDetails.headerStarting == undefined 
							|| $scope.fileDataIngesterDetails.headerStarting == "") ){
				logger.logError("Please enter header start at line");
				return false;
			}else if($scope.fileDataIngesterDetails.containsHeader === "true"
				&& $scope.fileDataIngesterDetails.dataSourceType !== 'RDBMS_DATA_SOURCE' &&
				$scope.fileDataIngesterDetails.dataSourceType !== 'REMOTE_DATA' &&
				$scope.fileDataIngesterDetails.dataSourceType !== 'FTP_DATA' &&
				$scope.fileDataIngesterDetails.dataSourceType !== 'EXCEL_DATA_SOURCE'
					&& $scope.fileDataIngesterDetails.recordStartsWith == "false") 
				if(parseInt($scope.fileDataIngesterDetails.headerStarting) > parseInt($scope.fileDataIngesterDetails.recordStartLine)){
					logger.logError("Please enter record starts line number greater than header at line number");
					$scope.fileDataIngesterDetails.recordStartLine="";
					return false;
				}else if(undefined != $scope.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes && null != $scope.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes && true == $scope.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes){
					if(null != $scope.fileDataIngesterDetails.delimiter && $scope.fileDataIngesterDetails.delimiter.length > 1 && "\\t" != $scope.fileDataIngesterDetails.delimiter){
						logger.logError("Optionally Enclosed with double quotes is not supported for this delimiter");
						return false;
					}

				}

											var _serverObj = null;

											if ($scope.fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE') {

												if ($scope.datasource.containsHeader == "false") {
													$scope.datasource.headerStarting = "";
												}

												var segmentNameType = angular.copy($scope.datasource.config.queryToExecute);
				if(paramFromDialog !== null && paramFromDialog.length > 0){
					for(var i= 0 ; i < paramFromDialog.length ; i++){
						if(segmentNameType.indexOf("${"+paramFromDialog[i].paramName+"}") !== -1){
							segmentNameType = segmentNameType.replace("${"+paramFromDialog[i].paramName+"}" , paramFromDialog[i].defaultVal);
							i--;
						}
					}
				}
				
				if($scope.datasource.fileStoreObject == 'SOURCE'){
					logger.logError('Unable to update connection settings. Please deselect SOURCE from Advanced Settings > data-at-rest');
					return false;
				}
				
				$scope.datasource.config.finalQueryToExecute = segmentNameType;
				_serverObj = {
						dbName : $scope.datasource.config.dbName,
						ip : $scope.datasource.config.ip ,
						conncetionName : $scope.datasource.config.conncetionName,
						port : $scope.datasource.config.port ,
						dbUserName : $scope.datasource.config.dbUserName,
						dbPassword : $scope.datasource.config.dbPassword,
						queryToExecute:$scope.datasource.config.queryToExecute,
						configConnectionType:$scope.datasource.config.configConnectionType,
						connectorType : $scope.datasource.config.connectorType,
						dbPrimaryKey : $scope.datasource.config.dbPrimaryKey,
						finalQueryToExecute : $scope.datasource.config.finalQueryToExecute,
						inputFileType : $scope.datasource.config.inputFileType,
						inputCompressionType : $scope.datasource.config.inputCompressionType
				};

				var fileIng = {
						"dataSourceId" :    $scope.datasource.id,
						"dataSourceType" :  $scope.fileDataIngesterDetails.dataSourceType,  
						"delimiter": ","
				};
				var reqDataObject = {'connectorConfig':_serverObj,'dataSourceId':$scope.datasource.id};
				$http({
					url:"/saveRDBMSDSourceConnectionDetails",
					method: "POST",
					data : reqDataObject
				}).success(function(data, status, headers, config){

					if(data.status == 'failed') {

						if(data.message.trim().indexOf("FIELDMAPPING_WARNING$$")!=-1){
							$scope.compareFieldMappingSchema(data.message.trim().split("$$")[1], $scope.datasource);
						}else {
							logger.logError(data.message);
							return false;
						}


					}else{
						$http.post("/connectionDetails",fileIng).success(function(data, status, headers, config){
							refreshedDataSource();
						}).error(function(data, status, headers, config){
							if(data.indexOf("FIELDMAPPING_WARNING$$")!=-1){
								$scope.compareFieldMappingSchema(data.split("$$")[1], $scope.datasource);
							}else {
								logger.logError(data);
							}
						});
						logger.logSuccess("Successfully saved the connection details.");
					}

					if($scope.datasource.ingection === "quick"){
						$scope.ingections = "Basic";
						$scope.dataHide = true;
					}else{
						$scope.ingections = "Advance";
						$scope.dataHide = false;
					}


				}).error(function(data){
					logger.logError(data);
				});

			}else if($scope.fileDataIngesterDetails.dataSourceType == 'FILE_DATA_SOURCE' 
				|| $scope.fileDataIngesterDetails.dataSourceType == 'FIXED_LENGTH_FORMAT'
					|| $scope.fileDataIngesterDetails.dataSourceType == 'TSV_DATA_SOURCE' 
						|| $scope.fileDataIngesterDetails.dataSourceType == 'CSV_DATA_SOURCE'
							|| $scope.fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'){

				var file = $scope.myFile != undefined ? $scope.myFile[0] : '';

				$scope.filePathError="";
				$scope.delimiterError="";
				$scope.containtHeaderError="";
				if(undefined == $scope.fileDataIngesterDetails.containsHeader){
					$scope.containtHeaderError=" Contains Header is required";
				}
				if(undefined == $scope.fileDataIngesterDetails.delimiter || '' == $scope.fileDataIngesterDetails.delimiter){
					$scope.delimiterError = "Delimiter is required";
				}

				if($scope.fileDataIngesterDetails.containsHeader === "false"){
					$scope.fileDataIngesterDetails.headerStarting = null;
				}

				if($scope.datasource.ingection == "quick"){
					$scope.fileDataIngesterDetails.recordStartLine = 1;
				}


				_serverObj = {
						"availableToUse" : $scope.fileDataIngesterDetails.availableToUse,
						"containsHeader" :  $scope.fileDataIngesterDetails.containsHeader,
						"dataSourceId" :    $scope.datasource.id,
						"dataSourceType" :  $scope.fileDataIngesterDetails.dataSourceType,  
						"delimiter": $scope.fileDataIngesterDetails.delimiter,
						"headerStarting" : $scope.fileDataIngesterDetails.headerStarting,
						"headerTemplate" : $scope.fileDataIngesterDetails.headerTemplate,
						"isDelimiterRegEx": $scope.fileDataIngesterDetails.isDelimiterRegEx,
						"recordLoadLimit" : $scope.fileDataIngesterDetails.recordLoadLimit,
						"recordStartLine" : $scope.fileDataIngesterDetails.recordStartLine,
						"recordStartsWith" : $scope.fileDataIngesterDetails.recordStartsWith,
						"recordsIgnore" : $scope.fileDataIngesterDetails.recordsIgnore,
						"serverFilePath" :   $scope.fileDataIngesterDetails.serverFilePath,  
						"startingRegularExpre" : $scope.fileDataIngesterDetails.startingRegularExpre,
						"webServerTempPath" : $scope.fileDataIngesterDetails.webServerTempPath,
						"inputFileType" : 'TEXT',
						"inputCompressionType" : 'UNCOMPRESSED',
						"optionallyEnclosedInDoubleQuotes" : $scope.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes

				};

				if($scope.fileDataIngesterDetails.dataSourceType == 'FIXED_LENGTH_FORMAT'){
					if($scope.fileDataIngesterDetails.headerTemplate == null || $scope.fileDataIngesterDetails.headerTemplate == undefined 
							|| $scope.fileDataIngesterDetails.headerTemplate == ""){
						logger.logError("Please provide the Header Template");
						return false;
					}
					var boolTest = true;
					var arrayOfHeader = ($scope.fileDataIngesterDetails.headerTemplate).split(",");
					for(var i = 0; i < arrayOfHeader.length ; i++){
						var string = arrayOfHeader[i].substring(arrayOfHeader[i].lastIndexOf("[") + 1 , arrayOfHeader[i].lastIndexOf("]"));
						if(isNaN(string) || string == ""){
							boolTest = false;
							break;
						}
					}
					if(boolTest == false){
						logger.logError("Please provide the length of column in integer");
						return false;
					}
												}

				if($scope.fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'){
					$scope.saveDataSourceFromData(_serverObj);
					return false;
				}else if($scope.fileDataIngesterDetails.dataSourceType == 'FIXED_LENGTH_FORMAT' || 
						((undefined != $scope.fileDataIngesterDetails.containsHeader) && 
								(undefined != $scope.fileDataIngesterDetails.delimiter && '' != $scope.fileDataIngesterDetails.delimiter))) {
					$scope.saveDataSourceFromData(_serverObj);
				}else{
					logger.logError("Unable to save the connection details");
				} 

											} else if ($scope.fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE') {

				if($scope.myFile == undefined){
					if(undefined == $scope.fileDataIngesterDetails.serverFilePath || '' == $scope.fileDataIngesterDetails.serverFilePath){
						$scope.filePathError = "File Path is required";
					}
				}

				$http({
					url:"/fromToValid",
					method:"POST",
					data: $scope.fileDataIngesterDetails.excelRanges
				}).success(function(data, status, headers, config) {
					if(data.toUpperCase() != "TRUE"){
						logger.logError("Column difference found");
						return false;
					}

					$scope.saveExcel();
				}).error(function(data, status, headers, config) {
					logger.logError('Error while saving connection details');
				});
			}else if($scope.fileDataIngesterDetails.dataSourceType == 'FTP_DATA') {
				if(undefined != $scope.datasource.config.optionallyEnclosedInDoubleQuotes
						&& null != $scope.datasource.config.optionallyEnclosedInDoubleQuotes 
						&& true == $scope.datasource.config.optionallyEnclosedInDoubleQuotes){
					if(null != $scope.datasource.config.dbPrimaryKey && $scope.datasource.config.dbPrimaryKey.length > 1 && "\\t" != $scope.datasource.config.dbPrimaryKey){
						logger.logError("Optionally enclosed option is not supported for this delimiter");
						return false;
					}
				} else if ($scope.datasource.config.defaultLocationForFTP === undefined  
						|| $scope.datasource.config.defaultLocationForFTP === null 
						|| $.trim($scope.datasource.config.defaultLocationForFTP) === ''){
					logger.logError("please provide the file path for ftpDataSource.");
					return false;
				} else if ($scope.fileDataIngesterDetails.delimiter === undefined  
						|| $scope.fileDataIngesterDetails.delimiter === null 
						|| $.trim($scope.fileDataIngesterDetails.delimiter) === ''){
					logger.logError("please provide the delimeter for ftpDataSource.");
					return false;
				}
				$scope.datasource.config.dbPrimaryKey = $scope.fileDataIngesterDetails.delimiter;
				var reqDataObject = {'dataSourceId':$scope.datasource.id, 'config': $scope.datasource.config };
				$http({
					url:"/ftpDataSource",
					method:"POST",
					data: reqDataObject
				}).success(function(data, status, headers, config) {
					refreshedDataSource();
					logger.logSuccess("Connection details saved successfully");
				}).error(function(data, status, headers, config) {
					if(data.indexOf("FIELDMAPPING_WARNING$$")!=-1){
						$scope.compareFieldMappingSchema(data.split("$$")[1], $scope.datasource);
					}else {
						logger.logError(data);
					}
				});
			}else if($scope.fileDataIngesterDetails.dataSourceType == 'STREAM_DS' && $scope.dataSourceInstance.flowType == 'STREAM'){
				$scope.datasource.fileDataIngesterDetails = $scope.fileDataIngesterDetails;
				$scope.datasource.fileDataIngesterDetails.dataSourceType = "STREAM_DS";
				$scope.datasource.dataSourceType = "STREAM_DS";
				
				$http({
					method:"POST",
					url:"/saveStreamDataSource",
					data:$scope.datasource
				}).success(function(data){
					refreshedDataSource();
					logger.logSuccess("Connection details saved successfully");
				}).error(function(data){
				});
			}
		};

	$scope.saveNoSqlConnectionDetails = function() {
	
		if ($scope.NoSqlConnectionType === "EMPTY") {
			logger.logError("select nosql db type");
		} else if ($scope.datasource.config == undefined) {
			logger.logError("No mongo connection selected");
		} else if ($scope.schema == undefined || $scope.schema == "") {
				logger.logError("Please select collection name");
		} else {
			_serverObj = {
					dbName : $scope.datasource.config.dbName,
					ip : $scope.datasource.config.ip ,
					conncetionName : $scope.datasource.config.conncetionName,
					port : $scope.datasource.config.port ,
					dbUserName : $scope.datasource.config.dbUserName,
					dbPassword : $scope.datasource.config.dbPassword,
					queryToExecute:$scope.datasource.config.queryToExecute,
					configConnectionType:$scope.datasource.config.configConnectionType,
					connectorType : $scope.datasource.config.connectorType,
					dbPrimaryKey : $scope.datasource.config.dbPrimaryKey,
					noSqlSchema: $scope.schema
			};
			$http({
				url: "/saveNosqlDataSource?datasourceId="+$scope.datasource.id,
				method: 'Post',
				data : _serverObj
			}).success(function(data, status, headers, config) {
				if(data.status == 'failed') {
					if(data.message.trim().indexOf("FIELDMAPPING_WARNING$$")!=-1){
						$scope.compareFieldMappingSchema(data.message.trim().split("$$")[1], $scope.datasource);
					}else {
						logger.logError(data.message);
						return false;
					}
				}else if (data.status == 'success'){
					refreshedDataSource();
					logger.logSuccess(data.message);
				}
			}).error(function(data, status, headers, config) {
				logger.logError(data);
			})
			}
	}

		$scope.saveExcel = function(){

			var _serverObject = {
					"availableToUse" : $scope.fileDataIngesterDetails.availableToUse,
					"serverFilePath" : $scope.fileDataIngesterDetails.serverFilePath,
					"dataSourceId" : $scope.datasource.id,
					"dataSourceType" : $scope.fileDataIngesterDetails.dataSourceType,
					"excelRanges" : $scope.fileDataIngesterDetails.excelRanges,
					"headerTemplate" : $scope.fileDataIngesterDetails.headerTemplate,
					"containsHeader" :  $scope.fileDataIngesterDetails.containsHeader,
					"optionallyEnclosedInDoubleQuotes" :$scope.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes,
					"inputFileType" : 'TEXT',
					"inputCompressionType" : 'UNCOMPRESSED'
			};

			var file = $scope.myFile != undefined ? $scope.myFile[0] : '';

			if((undefined == $scope.myFile && undefined != $scope.fileDataIngesterDetails.serverFilePath) ){
				$http.post("/connectionDetails",_serverObject).success(function(data, status, headers, config){
					refreshedDataSource();
					logger.logSuccess("Connection details saved successfully");
				}).error(function(data, status, headers, config){

					if(data.indexOf("FIELDMAPPING_WARNING$$")!=-1){
						$scope.compareFieldMappingSchema(data.split("$$")[1], $scope.datasource);
					}else {
						logger.logError(data);
					}

				});
			}else{
				if((undefined == $scope.myFile && undefined != $scope.fileDataIngesterDetails.serverFilePath) || 
						(undefined != $scope.myFile && (null == $scope.fileDataIngesterDetails.serverFilePath) 
								|| ("" == $scope.fileDataIngesterDetails.serverFilePath))){
					promise = $upload.upload({
						url:"/Fileupload",
						method:"POST",
						data: _serverObject,
						file: file
					}).success(function(data, status, headers, config) {
						refreshedDataSource();
						$scope.myFile=undefined;
						logger.logSuccess('Connection details saved successfully');
					}).error(function(data, status, headers, config) {
						if(data.indexOf("FIELDMAPPING_WARNING$$")!=-1){
							$scope.compareFieldMappingSchema(data.split("$$")[1], $scope.datasource);
						}else {
							logger.logError('Error while saving connection details');
						}

					});
				}else{
					logger.logError("Unable to save the connection details");
				}
			};
		};

		$scope.clearFileDataSource = function(selectDataSource){
			if($scope.datasource.dataSourceType !== $scope.fileDataIngesterDetails.dataSourceType){
				$scope.fileDataIngesterDetails = {};
				$scope.fileDataIngesterDetails.dataSourceType = selectDataSource;
				$scope.fileDataIngesterDetails.filePathName = undefined;
				$scope.fileDataIngesterDetails.myFile = undefined;
				$scope.fileDataIngesterDetails.containsHeader = false+"";
				$scope.fileDataIngesterDetails.headerTemplate = "";
				$scope.fileDataIngesterDetails.delimiter = undefined;
				$scope.fileDataIngesterDetails.availableToUse = undefined;
				$scope.fileDataIngesterDetails.headerStarting = undefined;
				$scope.fileDataIngesterDetails.numberOfLines = undefined;
				$scope.fileDataIngesterDetails.recordStartLine = undefined;
				$scope.fileDataIngesterDetails.startingRegularExpre = undefined;
				$scope.fileDataIngesterDetails.recordLoadLimit = undefined;
				$scope.fileDataIngesterDetails.recordsIgnore = undefined;
				$scope.fileDataIngesterDetails.serverFilePath = "";
				$scope.fileDataIngesterDetails.excelRanges=[];
				$scope.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes = undefined;
				$("#uploadFile").val('');

				if($scope.datasource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.fileDataIngesterDetails.recordStartsWith = false+"";
					$scope.dataHide = false;
				}
			}else{
				$scope.fileDataIngesterDetails.dataSourceType = selectDataSource;
				if($scope.datasource.fileDataIngesterDetails != null ){
					$scope.fileDataIngesterDetails.serverFilePath = $scope.datasource.fileDataIngesterDetails.serverFilePath;
					$scope.fileDataIngesterDetails.delimiter = $scope.datasource.fileDataIngesterDetails.delimiter;
					$scope.fileDataIngesterDetails.containsHeader = $scope.datasource.fileDataIngesterDetails.containsHeader;
					$scope.fileDataIngesterDetails.headerTemplate = $scope.datasource.fileDataIngesterDetails.headerTemplate;
					$scope.fileDataIngesterDetails.availableToUse = $scope.datasource.fileDataIngesterDetails.availableToUse;
					$scope.fileDataIngesterDetails.headerStarting = $scope.datasource.fileDataIngesterDetails.headerStarting;
					$scope.fileDataIngesterDetails.numberOfLines = undefined;
					$scope.fileDataIngesterDetails.recordStartLine = $scope.datasource.fileDataIngesterDetails.recordStartLine;
					$scope.fileDataIngesterDetails.startingRegularExpre = $scope.datasource.fileDataIngesterDetails.startingRegularExpre;
					$scope.fileDataIngesterDetails.recordLoadLimit = $scope.datasource.fileDataIngesterDetails.recordLoadLimit;
					$scope.fileDataIngesterDetails.recordsIgnore = $scope.datasource.fileDataIngesterDetails.recordsIgnore;
					$scope.fileDataIngesterDetails.excelRanges=$scope.datasource.fileDataIngesterDetails.excelRanges;
					$scope.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes = $scope.datasource.fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes;
				}
				
				$("#uploadFile").val('');

				if($scope.datasource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.fileDataIngesterDetails.recordStartsWith = false+"";
					$scope.dataHide = false;
				}
			}
		};

		$scope.validateRdbmsConnection = function(){

			if ($scope.datasource.dsLevelParams !== undefined && $scope.datasource.dsLevelParams !== null
					&& $scope.datasource.dsLevelParams.length > 0
					&& $scope.fileDataIngesterDetails.dataSourceType == "RDBMS_DATA_SOURCE")
			{
				$scope.repo = false;
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/captureInputParam.html",
					controller: "captureValuesForParam",
					resolve:{
						existingDataSource : function(){
							return $scope.datasource;
						},
						repo : function(){
							return $scope.repo;
						}
					}

				}),
				modalInstance.result.then(function (paramFromDialog) {
					var segmentNameType = angular.copy($scope.datasource.config.queryToExecute);
					if(paramFromDialog !== null && paramFromDialog.length > 0){
						for(var i= 0 ; i < paramFromDialog.length ; i++){
							if(segmentNameType.indexOf("${"+paramFromDialog[i].paramName+"}") !== -1){
								segmentNameType = segmentNameType.replace("${"+paramFromDialog[i].paramName+"}" , paramFromDialog[i].defaultVal);
								i--;
							}
						}
					}
					$scope.datasource.config.finalQueryToExecute = segmentNameType;
					var _serverObj = {
							dbName : $scope.datasource.config.dbName,
							ip : $scope.datasource.config.ip ,
							conncetionName : $scope.datasource.config.conncetionName,
							port : $scope.datasource.config.port ,
							dbUserName : $scope.datasource.config.dbUserName,
							dbPassword : $scope.datasource.config.dbPassword,
							connectorType : $scope.datasource.config.connectorType,
							configConnectionType : $scope.datasource.config.configConnectionType,
							queryToExecute : $scope.datasource.config.queryToExecute,
							finalQueryToExecute : $scope.datasource.config.finalQueryToExecute,
							topicName: $scope.datasource.fileDataIngesterDetails.topicName
					};

					$scope.testMyConnection(_serverObj); 
				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});

			}else if($scope.fileDataIngesterDetails.dataSourceType == "RDBMS_DATA_SOURCE"
						|| $scope.fileDataIngesterDetails.dataSourceType == "STREAM_DS"){

				$scope.repo = false;
				$scope.datasource.config.finalQueryToExecute = $scope.datasource.config.queryToExecute;
				var _serverObj = {
						dbName : $scope.datasource.config.dbName,
						ip : $scope.datasource.config.ip ,
						conncetionName : $scope.datasource.config.conncetionName,
						port : $scope.datasource.config.port ,
						dbUserName : $scope.datasource.config.dbUserName,
						dbPassword : $scope.datasource.config.dbPassword,
						connectorType : $scope.datasource.config.connectorType,
						configConnectionType : $scope.datasource.config.configConnectionType,
						queryToExecute : $scope.datasource.config.queryToExecute,
						finalQueryToExecute : $scope.datasource.config.finalQueryToExecute,
						topicName: null != $scope.datasource.fileDataIngesterDetails ? $scope.datasource.fileDataIngesterDetails.topicName : null
				};

				$scope.testMyConnection(_serverObj); 
			}else if($scope.fileDataIngesterDetails.dataSourceType == "NOSQL"){
				var _serverObj = {
						dbName : $scope.datasource.config.dbName,
						ip : $scope.datasource.config.ip ,
						conncetionName : $scope.datasource.config.conncetionName,
						port : $scope.datasource.config.port ,
						dbUserName : $scope.datasource.config.dbUserName,
						dbPassword : $scope.datasource.config.dbPassword,
						connectorType : $scope.datasource.config.connectorType,
						configConnectionType : $scope.datasource.config.configConnectionType,
						queryToExecute : $scope.datasource.config.queryToExecute,
						finalQueryToExecute : $scope.datasource.config.finalQueryToExecute
				};
				$scope.testNoSQlConnection(_serverObj);
			}	
		};
		
		$scope.testMyConnection = function(_serverObj){
			var reqDataObject = {'config':_serverObj,'indicator':true};
			$http({
				url:"/TestRdbmsConnection",
				method: "POST",
				data : reqDataObject
			}).success(function(data, status, headers, config){

				if(data.status == 'failed') {
					logger.logError(data.message);
				}else if (data.status == 'success'){
					logger.logSuccess('Connection successfull');
				}
				if(data.fieldsDetails != null && data.fieldsDetails.length > 0){
					$scope.openRdbmsFieldMappingModel(data.fieldsDetails);
				}
			}).error(function(data, status, headers, config){
				if(status){
					logger.logError('Error while testing connection');
				}

			});
		}
		
		$scope.testNoSQlConnection=function(_serverObj){
			$http({
				url:"/TestNosqlConnection?indicator="+true+"&datasourceId="+$scope.dataSourceId,
				method: "POST",
				data : _serverObj
			}).success(function(data, status, headers, config){
				if(data.status == 'failed') {
					logger.logError(data.message);
				}else if (data.status == 'success'){
					logger.logSuccess(data.message);
				}
			}).error(function(data, status, headers, config){
				if(status){
					logger.logError('Error while creating connection...Check DB credentials and Name '+$scope.datasource.id);
				}
			});
		}
		
		$scope.openRdbmsFieldMappingModel = function(fieldMapping) {
			var modalInstance;
			modalInstance = $modal.open({
				size : "lg",
				templateUrl: "/views/template/selectRdbmsFieldSchema.html",
				controller: "rdbmsFieldMapping",
				resolve:{
					fieldMapping : function(){
						return fieldMapping;
					}
				}
			}),modalInstance.result.then(function(obj) {

			}, function(result) {
				$log.info("Modal dismissed at: " + new Date);
			});
		};

        $scope.testFacebookData = function (){
        	
        	if($scope.fileDataIngesterDetails.dataSourceType == "EXTERNAL_DATA"){
        		
        	   var _serverObj = {
						
						exteranlConnectorType : $scope.datasource.config.exteranlConnectorType,
						facebookSecretId : $scope.datasource.config.facebookSecretId,
						facebookAccessToken : $scope.datasource.config.facebookAccessToken,
						facebookDataType : $scope.datasource.config.facebookDataType,
						facebookPageName : $scope.datasource.config.facebookPageName
						
				};
        		$scope.testFacebookConnection(_serverObj); 
        	}
		};
		
		
		$scope.testFacebookConnection =  function(_serverObj){
			
			$http({
				url:"/testFacebookConnection?indicator="+true+"",
				method: "POST",
				data : _serverObj
			}).success(function(data, status, headers, config){

				if(data.status == 'failed') {
					logger.logError(data.message);
				}else if (data.status == 'success'){
					logger.logSuccess('Connection successfull');
				}
				
			}).error(function(data, status, headers, config){
				if(status){
					logger.logError('Error while testing connection');
				}

			});
			
		};
		
		
	}]).controller("advancedValidationController",  ["$route","uniqueArray","$scope", "$modal" , "$log"  , "$location" , "logger", "$http", "DataSourceService", function($route,uniqueArray,$scope, $modal, $log , $location,logger,$http, DataSourceService){

		$scope.oneAtATime = !0;
		$scope.ingections = "";

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = angular.copy(args);

			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

			$scope.datasource.fieldMappings =  [];
			$scope.fieldMappings = args.fieldMappings;
			$scope.validators = [];

			if(null != $scope.datasource.advancedValidations && $scope.datasource.advancedValidations.length > 0){
				_.each($scope.datasource.advancedValidations, function(advancedvalidation, index) {
					var fieldStores = [];
					_.each($scope.fieldMappings, function(mappingObject,index){
						_.each(advancedvalidation.fieldsStore , function(storeObject,index){
							if(mappingObject.id ===  storeObject){
								fieldStores.push(mappingObject);

							}
						});
					});
					$scope.validators.push(
							{   title:"Validation "+index, 
								selectFields : fieldStores, 
								validationsApplied : advancedvalidation.dataValidations, 
								advancedvalidationId : advancedvalidation.id,
								dropdown : [$scope.fieldMappings]
							});
					changingSelectTest($scope.validators[index].dropdown , fieldStores , index);
				});

			}
		});

		function changingSelectTest(dropdown , fieldStores ,index)
		{
			var j=1;
			_.each(fieldStores ,function(track){
				$scope.validators[index].dropdown[j] = _.reject($scope.validators[index].dropdown[j - 1],function(object){
					return object.id == track.id;
				});
				j++;
			});

			$scope.validators[index].dropdown.splice(j , 1);
		}

		$scope.changingSelect = function (value , parentIndex , index){

			for(var i=0; i < $scope.validators[parentIndex].selectFields.length ; i++){
				if($scope.validators[parentIndex].selectFields[i].id == $scope.validators[parentIndex].selectFields[index].id && i !== index){
					$scope.validators[parentIndex].selectFields[i] = undefined;
				}
			}

			$scope.validators[parentIndex].dropdown[index+1] = _.reject($scope.validators[parentIndex].dropdown[index],function(object){
				return object.id == value.id;
			}); 
		};
		$scope.deleteValidation = function($index){
			var reqDataObject = {'dataSourceId' : $scope.dataSourceId,'advancedFieldValidationId' : $scope.validators[$index].advancedvalidationId};
			$http.post("/deleteAdvancedValidation", reqDataObject).success(function(data, status, headers, config){
				$scope.validators.splice($index , 1);
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.datasource = data;
					logger.logSuccess('Validation deleted successfully.');
					$scope.$emit("shareDataSourceToAllController", $scope.datasource);
				});
			}).error(function(){
				logger.logError("Something went wrong");
				return false;
							
			});

			var length = $scope.validators.length;
			if(length > 1){
				$scope.validators.splice($index ,1);
			}
		},
		$scope.addSelect = function(value,$parentindex,$index){

			if($scope.validators[$parentindex].selectFields.length < $scope.fieldMappings.length)
			{
				$scope.validators[$parentindex].selectFields.push('');
				addFields($index,value,$parentindex);
			}
			else{
				logger.logError("Number of Fields and select box are same");
			}
		};
		function addFields($index, value ,$parentindex){

			$scope.validators[$parentindex].dropdown[$index+1] = _.reject($scope.validators[$parentindex].dropdown[$index],function(object){
				return object.id == $scope.validators[$parentindex].selectFields[$index].id;
			}); 
		}

		$scope.removeSelects = function($parentIndex , $index){

			var test=$scope.validators[$parentIndex].selectFields[$index];
			var length = $scope.validators[$parentIndex].selectFields.length;

			if(length > 1){

				$scope.validators[$parentIndex].selectFields.splice($index , 1);
				$scope.validators[$parentIndex].dropdown.splice($index , 1);

				for(var i=$index ; i<$scope.validators[$parentIndex].dropdown.length;i++){
					$scope.validators[$parentIndex].dropdown[i].push(test);
				}

			}

		},

		$scope.openModal = function($parent, $index) {
			var flag=true;
			for(var i=0;i < $scope.validators[$parent].selectFields.length ; i++){
				if($scope.validators[$parent].selectFields[i] === undefined){
					logger.logError("Please select all fields");
					return ;
				}
			}

			var test=_.pluck($scope.validators[$parent].selectFields , "id");

			_.each(test ,function (val , index){
				if(val === undefined){
					flag=false;
					return;
				}
			});
			if(flag === false){
				logger.logError("Please select all fields first");
				return;
			}


			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AdvancedValidationOnMultipleFieldModal.html",
				controller: "createNewAdvancedValidationModalCntr",
				resolve:{
					dataSource : function(){
						return $scope.datasource;
					},
					length : function(){
						if($scope.datasource.advancedValidations !== null &&
								$scope.datasource.advancedValidations !== undefined && 
								$scope.datasource.advancedValidations[$parent] !== undefined)
							return $scope.datasource.advancedValidations[$parent].dataValidations.length;
						else
							return 0;
					},
					selectedFields : function(){
						return $scope.validators[$parent].selectFields;
					},
					validationsApplied : function(){
						return $scope.validators[$parent].validationsApplied;
					}, 
					validationId : function(){
						return $scope.validators[$parent].advancedvalidationId;
					}
				}
			}),modalInstance.result.then(function(returnValue) {
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.datasource = data;
					$scope.$emit("shareDataSourceToAllController", $scope.datasource);
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};

	}]).controller("createNewAdvancedValidation" ,  ["$scope", "$http",  function($scope, $http){
		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.addValidation = function(){
			var newValidation;
			newValidation = $scope.validators.length , $scope.validators.push({title:"Validation"+newValidation,selectFields : [{select:["1","2","3"]}] , dropdown:[$scope.fieldMappings]});
		};
	}]).controller("createNewAdvancedValidationModalCntr" , ["DataSourceService","dataSource", "length", "$route","uniqueArray","$scope" , "$modalInstance" , "$http" , "$location","$routeParams" ,"logger","selectedFields","$rootScope","validationsApplied" , "validationId", function(DataSourceService , dataSource, length, $route,uniqueArray,$scope , $modalInstance , $http , $location,$routeParams , logger , selectedFields , $rootScope,validationsApplied, validationId){
		$scope.datasource = dataSource;
		$scope.counter = 0;
		$scope.dataValidations = {};
		$scope.dataValidationsFields = [];
		$scope.rejectData = "REJECT RECORD";
		$scope.obj={};
		$scope.hideValue = true;
		$scope.validationNotification = [];
		$scope.fieldMappings = $scope.datasource.fieldMappings; 

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		if(!(validationId > 0)){
			validationId = 0;
		};
		$scope.selectedFields = selectedFields,

		$scope.validationTypes = ["NOT_NULL","LENGTH_CHECK"],

		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};

		angular.forEach(validationsApplied,function(applied, index){
			$scope.validationNotification.push(applied);
		}),
		$scope.addVal= function (index){

			if($scope.obj.rejectFieldValue == undefined || $scope.obj.rejectFieldValue == null || $scope.obj.rejectFieldValue == ""){
				logger.logError("Please provide the value for field reject");
				return false;
			}else if($scope.rejectWithData == null || $scope.rejectWithData == null || $scope.rejectWithData == ""){
				logger.logError("Please select the fields  whose values going to be reject");
				return false;
			}

			var pos = index.position;
			var test = {
					value : $scope.obj.rejectFieldValue,
					object : index
			};

			if($scope.dataValidations[pos] == undefined)
				$scope.dataValidationsFields.push(test);
			else{
				_.each($scope.dataValidationsFields , function(values){
					if(values.object.position == index.position){
						values.value = $scope.obj.rejectFieldValue;
						return;
					}
				});
			}

			$scope.dataValidations[pos] = $scope.obj.rejectFieldValue;

			$scope.obj.rejectFieldValue="";
			$scope.rejectWithData = undefined;

		},
		$scope.deleteFields = function(key , index){
			$scope.dataValidationsFields.splice(index , 1);
			$scope.dataValidations = _.omit($scope.dataValidations , key);
		},
		$scope.createAdvancedValidation = function(){

			$scope.fieldStore =_.pluck(selectedFields,"id");

			var advancedvalidation = {
					dataValidations : $scope.validationNotification,
					fieldsStore : $scope.fieldStore
			};

			if(length === $scope.validationNotification.length && $scope.counter === 0){
				logger.logError("Please update the validation");
				return false;
			}
			else{
				$http.post("/addAdvancedValidation",{'dataSourceId':$rootScope.dataSourceId,'addAdvancedValidation':validationId,'advancedValidations':advancedvalidation}).success(function(data, status, headers, config){
					if($scope.validationNotification.length == 0){
						logger.logSuccess("Validations cleared successfully");
						$modalInstance.close('');
						if($routeParams.param4 === undefined || $routeParams.param4 === null)
							$route.reload();
					}else{
						logger.logSuccess('Validation saved successfully');
						$modalInstance.close('');
						if($routeParams.param4 === undefined || $routeParams.param4 === null)
							$route.reload();
					}
				}).error(function(){
					$modalInstance.close('');
					logger.logError('error while saving validation');
				});
			}
		},
		$scope.removeVal = function(notification){		

			if(length !== $scope.validationNotification.length){
				$scope.counter++;
			}

			var index = -1;		
			var comArr = eval( $scope.validationNotification );
			for( var i = 0; i < comArr.length; i++ ) {
				if( comArr[i].lengthRangeFrom === notification.lengthRangeFrom && comArr[i].lengthRangeTo === notification.lengthRangeTo ) {
					index = i;
					break;
				}
			}
			if( index === -1 ) {
				alert( "Something gone wrong" );
			}
			$scope.validationNotification.splice( index, 1 );
		};
		$scope.closeCallout= function($index){
		},
		$scope.clearDependentInput = function(){
			$scope.lengthGreaterThan = '';
			$scope.lengthLessThan = '';
		},
		$scope.addValidatioNotification = function(){

			if(length !== $scope.validationNotification.length){
				$scope.counter++;
			}
			var dublicate=_.pluck($scope.validationNotification,"validator");

			var fix1=_.find(dublicate , function(check){
				return check == "NOT_NULL";
			});
			if($scope.validationModalSelect == "NOT_NULL")
			{
				if(fix1 == "NOT_NULL"){	
					logger.logWarning("You already Entered NOT NULL Validation");
					return;
				}
			}
			var fix=_.find(dublicate , function(check){
				return check == "LENGTH_CHECK";
			});

			if($scope.validationModalSelect == "LENGTH_CHECK")
			{
				if(fix == "LENGTH_CHECK"){	
					logger.logWarning("You already Entered Length Validation");
					return;
				}
				if($scope.lengthGreaterThan == "" || $scope.lengthLessThan == ""){
					logger.logWarning("Please Enter Both Value");
					return;
				}
				else if(isNaN($scope.lengthGreaterThan) || isNaN($scope.lengthLessThan))
				{
					logger.logWarning("Please Enter numbers in both field");
					$scope.lengthGreaterThan="";
					$scope.lengthLessThan="";
					return;
				}			 
				else if(parseInt($scope.lengthGreaterThan) > parseInt($scope.lengthLessThan) ){
					logger.logWarning("Please Enter GreaterThan is less or equal to  than lessThan");
					$scope.lengthGreaterThan="";
					$scope.lengthLessThan="";
					return;
				}
			}
			if($scope.validationModalSelect !== undefined && $scope.validationModalSelect != null){
				if (undefined == $scope.rejectData || null == $scope.rejectData) {
					logger.logWarning('Please select reject data');
					return;
				}

				if($scope.rejectData == "REJECT FIELD" &&  $scope.dataValidationsFields.length == 0){
					logger.logError("Please provides the values");
					return false;
				}	

				var reject = "";
				var len = 0;
				_.each($scope.dataValidationsFields , function(values){
					if($scope.dataValidationsFields.length - 1 > len)
						reject = reject + values.value + ",";
					else
						reject = reject + values.value;

					len++;
				});

				var _object = {
						validator : $scope.validationModalSelect,
						lengthRangeFrom : $scope.lengthGreaterThan,
						lengthRangeTo : $scope.lengthLessThan,
						rejectData : $scope.rejectData,
						rejectFieldValue : reject,
						advRejectFields : $scope.dataValidations  
				};

				$scope.lengthGreaterThan="";
				$scope.lengthLessThan="";
				$scope.rejectFieldValue="null";
				$scope.dataValidations = {};
				$scope.dataValidationsFields = [];

				$scope.validationNotification.push(_object);
				$scope.validationNotification = uniqueArray.unique($scope.validationNotification);
			}else{
				logger.logWarning('Please select validation type');
			}
		};	
		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});

	}]).controller("createValidationModalCntr" , ["DataSourceService" , "length", "$route","$scope" , "$modalInstance" , "$http" , "$location","$routeParams" ,"logger" , "validationType","fieldMappings","$rootScope", "dataSource", function(DataSourceService ,length, $route, $scope , $modalInstance , $http , $location,$routeParams , logger , validationType , fieldMappings , $rootScope, dataSource){

		if(dataSource){
			$scope.dataSource = dataSource;	
		}
		 
		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.length = length;
		$scope.counter = 0;
		$scope.mulValuesIn = "";
		$scope.mulValuesNot = "";
		$scope.selectedFields=[];
		$scope.validationTypes = ["NOT_NULL","LENGTH_CHECK","IN_VALUES","NOT_IN_VALUES"];
		$scope.validationNotification = [];
		$scope.selectedFields.push(fieldMappings);
		$scope.objj={};
		$scope.validationType = validationType,
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		},

		angular.forEach(fieldMappings.dataValidations,function(validationsApplied, index){
			$scope.validationNotification.push(validationsApplied);

			if(validationsApplied.validator == "IN_VALUES"){
				for(var i = 0; i < validationsApplied.multiValues.length ; i++){
					if(i == validationsApplied.multiValues.length - 1){
						$scope.mulValuesIn = $scope.mulValuesIn + validationsApplied.multiValues[i];
					}else{
						$scope.mulValuesIn = $scope.mulValuesIn + validationsApplied.multiValues[i] + ",";
					}
				}
			}
			if(validationsApplied.validator == "NOT_IN_VALUES"){
				for(var i = 0; i < validationsApplied.multiValues.length ; i++){
					if(i == validationsApplied.multiValues.length - 1){
						$scope.mulValuesNot = $scope.mulValuesNot + validationsApplied.multiValues[i];
					}else{
						$scope.mulValuesNot = $scope.mulValuesNot + validationsApplied.multiValues[i] + ",";
					}
				}
			}

		}),
		$scope.createAdvancedValidation = function(){
			fieldMappings.dataValidations = $scope.validationNotification;
			if($scope.length === fieldMappings.dataValidations.length && $scope.counter === 0){
				logger.logError("Please update the validation");
				return false;
			}
			else{
				if($scope.dataSource.tags==undefined || $scope.dataSource.tags!=null || $scope.dataSource.tags !="null"){
					$scope.dataSource.tags = [];
				} 
				var reqDataObject = {'dataSourceId':$rootScope.dataSourceId,'tags':$scope.dataSource.tags, 'fieldMapping':fieldMappings};
				$http.post("/updateFieldMapping", reqDataObject).success(function(data, status, headers, config){
					if($scope.validationNotification.length == 0){
						logger.logSuccess("Field mapping-Validations cleared successfully");
						$modalInstance.close("");
						DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
							$scope.$emit("shareDataSourceToAllController", data);
						});
					}else{
						logger.logSuccess('Field Mapping validations saved successfully');
						DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
							$scope.$emit("shareDataSourceToAllController", data);
						});
						$modalInstance.close("");
					}
				}).error(function(data){
					logger.logError("Error while storing the Field Mapping validations.. Error -> " + data);
				});
			}	
		};

		$scope.closeCallout= function($index){
		},
		$scope.clearDependentInput = function(){
			$scope.lengthGreaterThan = '';
			$scope.lengthLessThan = '';
			$scope.multiValues = '';
		},
		$scope.removeVal = function(notification){		

			if($scope.length !== $scope.validationNotification.length){
				$scope.counter++;
			}
			var index = -1;		
			var comArr = eval( $scope.validationNotification );
			for( var i = 0; i < comArr.length; i++ ) {
				if( comArr[i].lengthRangeFrom === notification.lengthRangeFrom 
						&& comArr[i].lengthRangeTo === notification.lengthRangeTo
						&& comArr[i].validator == notification.validator) {
					index = i;
					break;
				};
			}
			if( index === -1 ) {
				alert( "Something gone wrong" );
			}
			$scope.validationNotification.splice( index, 1 );
		},
		$scope.addValidatioNotification = function(fieldmapping){
			if($scope.objj.rejectFieldValue==undefined){
				logger.logError("Please enter valid value");
				return;
			}
			if($scope.length !== $scope.validationNotification.length){
				$scope.counter++;
			}
			var dublicate=_.pluck($scope.validationNotification,"validator");

			var fix1=_.find(dublicate , function(check){
				return check == "NOT_NULL";
			});
			if($scope.validationModalSelect == "NOT_NULL")
			{
				if(fix1 == "NOT_NULL"){	
					logger.logWarning("You already Entered NOT NULL Validation");
					return;
				}
			}

			var fix=_.find(dublicate , function(check){
				return check == "LENGTH_CHECK";
			});
			if($scope.validationModalSelect == "LENGTH_CHECK")
			{
				if(fix == "LENGTH_CHECK"){	
					logger.logWarning("You already Entered Length Validation");
					return;
				}
				if($scope.lengthGreaterThan == "" || $scope.lengthLessThan == ""){
					logger.logWarning("Please Enter Both Value");
					return;
				}
				else if(isNaN($scope.lengthGreaterThan) || isNaN($scope.lengthLessThan))
				{
					logger.logWarning("Please Enter numbers in both field");
					$scope.lengthGreaterThan="";
					$scope.lengthLessThan="";
					return;
				}
				else if(parseInt($scope.lengthGreaterThan) > parseInt($scope.lengthLessThan) ){
					logger.logWarning("Please Enter GreaterThan is less or equal to  than lessThan");
					$scope.lengthGreaterThan="";
					$scope.lengthLessThan="";
					return;
				}
			}

			if(undefined != $scope.multiValues){
				$scope.values = $scope.multiValues.split("\n");
			}
			var fix2=_.find(dublicate , function(check){
				return check == "IN_VALUES";
			});

			if($scope.validationModalSelect == "IN_VALUES")
			{
				if(fix2 == "IN_VALUES"){	
					logger.logWarning("You already Entered IN_VALUES Validation");
					return;
				}
			}
			var fix3=_.find(dublicate , function(check){
				return check == "NOT_IN_VALUES";
			});

			if($scope.validationModalSelect == "NOT_IN_VALUES")
			{
				if(fix3 == "NOT_IN_VALUES"){	
					logger.logWarning("You already Entered NOT_IN_VALUES Validation");
					return;
				}
			}

			if($scope.validationModalSelect == "IN_VALUES"){
				$scope.mulValuesIn = "";
				for(var i = 0 ; i < $scope.values.length ; i++){
					if(i == $scope.values.length - 1 && $scope.values[i] !== ""){
						$scope.mulValuesIn = $scope.mulValuesIn + $scope.values[i];
					}else if($scope.values[i] !== ""){
						$scope.mulValuesIn = $scope.mulValuesIn + $scope.values[i] + ",";
					}
					if($scope.values[i] == ""){
						$scope.values.splice(i , 1);
						i--;
					}
				}
			}
			if($scope.validationModalSelect == "NOT_IN_VALUES"){
				$scope.mulValuesNot = "";
				for(var i = 0 ; i < $scope.values.length ; i++){
					if(i == $scope.values.length - 1 && $scope.values[i] !== ""){
						$scope.mulValuesNot = $scope.mulValuesNot + $scope.values[i];
					}else if($scope.values[i] !== ""){
						$scope.mulValuesNot = $scope.mulValuesNot + $scope.values[i] + ",";
					}
					if($scope.values[i] == ""){
						$scope.values.splice(i , 1);
						i--;
					}
				}
			}

			if(($scope.validationModalSelect == "IN_VALUES" 
				|| $scope.validationModalSelect == "NOT_IN_VALUES") 
				&& $scope.values.length == 0){
				logger.logError("Please provide the values");
				return false;
			}

			if($scope.validationModalSelect !== undefined && $scope.validationModalSelect != null){
				var _object = {
						validator : $scope.validationModalSelect,
						multiValues : $scope.values,
						lengthRangeFrom : $scope.lengthGreaterThan,
						lengthRangeTo : $scope.lengthLessThan,
						rejectData : $scope.rejectData,
						rejectFieldValue : $scope.objj.rejectFieldValue
				};

				var length = $scope.validationNotification.length,
				isAvailable = false;

				if(length > 0){
					for ( var i = 0; i < length; i++){
						if($scope.validationNotification[i].validator == _object.validator){
							if($scope.validationNotification[i].lengthRangeFrom == _object.lengthRangeFrom
									&& $scope.validationNotification[i].lengthRangeTo == _object.lengthRangeTo){
								isAvailable  = true;
								break;
							}
						}
					}
				}
				if(!isAvailable){

					$scope.lengthGreaterThan="";
					$scope.lengthLessThan="";

					$scope.validationNotification.push(_object);
				}
			}else{
				logger.logWarning('Please select validation type');
			}

		};

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
	}]).controller("featureExtractionController",  ["$route","uniqueArray","$scope", "$modal" , "$log"  , "$location" , "logger", "$http", "DataSourceService", "$rootScope", function($route,uniqueArray,$scope, $modal, $log , $location,logger,$http, DataSourceService, $rootScope ){
		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;

			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.ingections = "";

		$scope.deleteFeatureExtraction = function(featureExtractionId){
			var reqDataObject = {'dataSourceId': $scope.dataSourceId,'featureExtractionId':featureExtractionId};
			$http.post("/deleteFeatureExtraction", reqDataObject).success(function(data, status, headers, config){
				logger.logSuccess('Deleted the Feature Extraction.');
			}).error(function(){
				logger.logError('Unable to delete the Feature Extraction.');				
			});

		},

		$scope.openModal = function() {
			var modalInstance;
			modalInstance = $modal.open({

				templateUrl: "/views/template/createNewFeatureExtractionModal.html",
				controller: "createNewFeatureExtractionModalCntr",
				resolve:{

					existingDataSource : function(){
						return $scope.datasource;
					}

				}


			}),modalInstance.result.then(function(obj) {
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.$emit("shareDataSourceToAllController", data);
				});
			}, function(result) {

				$log.info("Modal dismissed at: " + new Date);
			});
		};





	}]).controller("createNewFeatureExtractionModalCntr" , ["DataSourceService", "uniqueArray","$scope" , "$modalInstance" , "$http" , "$location","$routeParams" ,"logger","$rootScope", "existingDataSource", "$route",  function(DataSourceService , uniqueArray,$scope , $modalInstance , $http , $location,$routeParams , logger , $rootScope, existingDataSource, $route){

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.existingDS =existingDataSource;
		$scope.selectedFields = [];

		$scope.myMethodChange = function(inputValue){
			if(inputValue.componentName === "GEO_TAGGING" || 
					inputValue.componentName === "NAMED ENTITY RECOGNITION" || 
					inputValue.componentName  === "PART OF SPEECH"){
				logger.logError("No feature extraction model is create for "+"'"+inputValue.componentName+"'");
				$scope.featureExtractionSelect = undefined;
				return false;
			}
		},
		$scope.selectField = function(inputValue, fieldValue){
			var _obj ={
					input : inputValue,
					field : fieldValue	
			};
			var flag = undefined;
			var indexObj;

			loop:
				for(var i=0; i<$scope.selectedFields.length; i++){
					var tempValues = _.values($scope.selectedFields[i]);
					for(var j=0;j<tempValues.length;j++){
						if(tempValues[j] == inputValue){
							flag = tempValues[j];
							indexObj = i;
							break loop;
						}
					}
				}

			if( !_.isUndefined(flag)){
				if(_.isUndefined(fieldValue)){
					$scope.selectedFields.splice(indexObj,1);
				}else{
					$scope.selectedFields[indexObj] = _obj;
				}	
			}else{
				$scope.selectedFields.push(_obj);
			}	

		};

		$scope.createFeatureExtraction=function(){

			if(_.isUndefined($scope.featureExtractionSelect) || _.isNull($scope.featureExtractionSelect)){
				logger.logError("Please select Feature Extraction type.");
				return;
			}

			if(_.isUndefined($scope.selectedFields) || _.isNull($scope.selectedFields) || $scope.selectedFields.length<1){
				logger.logError("Please select Input Fields.");
				return;
			}	

			if( _.isUndefined($scope.featureExtractionSelect.selectedOutputs) || _.isNull($scope.featureExtractionSelect.selectedOutputs) || $scope.featureExtractionSelect.selectedOutputs.length<1){
				logger.logError("Please select Output Fields.");
				return;
			}

			if(!($scope.featureExtractionSelect.id > 0)){
				$scope.featureExtractionSelect.id = 0;
			};

			var featureInputList = {};
			_.each($scope.selectedFields, function(e) {
				featureInputList[e.input] = parseInt(e.field); 
			});




			var featureExtraction = {
					id : $scope.featureExtractionSelect.id,
					customComponentId : $scope.featureExtractionSelect.componentId,
					description : $scope.featureExtractionSelect.componentDescription,
					featureExtractionType : $scope.featureExtractionSelect.componentName,
					featureInputList : featureInputList,
					featureOutputList : $scope.featureExtractionSelect.selectedOutputs
			};
			var reqDataObject = {'dataSourceId' : $rootScope.dataSourceId,'featureExtraction': featureExtraction};
			$http({
				method: 'POST',
				url : "/addFeatureExtraction",
				data : reqDataObject

			}).success(function(data, status, headers, config){
				logger.logSuccess('Feature Extraction saved successfully');
				$modalInstance.close('');
			}).error(function(){
				logger.logError('error while saving Feature Extraction');
			});

		},
		$scope.cancel = function() {

			$modalInstance.close();
		};

		$scope.featureExtractionType = [];
		$http.post("/getAllCustomComponent").success(function(data, status, headers, config){
			$scope.storeComponents = data;	
			var extract = ["GEO_TAGGING" , "NAMED ENTITY RECOGNITION" , "PART OF SPEECH"];
			_.each(extract , function(values){
				var object = {
						componentName : values	
				}; 
				$scope.featureExtractionType.push(object);
			});

			for( var i = 0; i < $scope.storeComponents.length; i++ ) {
				if(_.isEqual($scope.storeComponents[i].componentType, "Feature_Extraction") && ! _.isUndefined($scope.storeComponents[i])){
					$scope.featureExtractionType.push($scope.storeComponents[i]);
				};
			}
			var tempSpliceIndex = [];
			for( var i = 0; i < $scope.featureExtractionType.length; i++ ) {
				if($scope.existingDS.featureExtraction !== null)
					for( var j = 0; j < $scope.existingDS.featureExtraction.length; j++ ) {
						if(!_.isUndefined($scope.featureExtractionType[i]) && _.isEqual($scope.featureExtractionType[i].componentId, $scope.existingDS.featureExtraction[j].customComponentId)){
							tempSpliceIndex.push(i);
						};
					};
			}
			tempSpliceIndex.reverse();
			for( var i = 0; i < tempSpliceIndex.length; i++ ) {
				$scope.featureExtractionType.splice(tempSpliceIndex[i],1)
			};	

		});



	}]).controller("DataCleansingCntr" , ["DataSourceService" ,"uniqueArray","$scope" , "$http", "logger", function(DataSourceService, uniqueArray,$scope , $http, logger){

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.ingections = "";

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.dataCleansingFrom = '';
		$scope.dataCleansingTo = '';
		$scope.dataCleansingFields = [];

		$scope.removeCleaseing = function(notification){				
			var index = -1;		
			var comArr = eval( $scope.dataCleansingFields );
			for( var i = 0; i < comArr.length; i++ ) {
				if( comArr[i].fromValue === notification.fromValue && comArr[i].toValue === notification.toValue ) {
					index = i;
					break;
				}
			}
			if( index === -1 ) {
				alert( "Something gone wrong" );
			}
			$scope.dataCleansingFields.splice( index, 1 );
		};
		$scope.addDataCleansing = function(){

			if($scope.dataCleansingFrom == ''){
				logger.logWarning("Please enter from value");
				return ;
			}
			var cleansingObject = {
					fromValue : $scope.dataCleansingFrom,
					toValue : $scope.dataCleansingTo
			};
			$scope.dataCleansingFrom="";
			$scope.dataCleansingTo="";

			$scope.dataCleansingFields.push(cleansingObject);
			$scope.dataCleansingFields = uniqueArray.unique($scope.dataCleansingFields);

		};
		$scope.saveDataCleansing = function(){

			var data=$scope.dataCleansingFields;
			$http.post("saveDataCleansing?dataSourceId="+$scope.datasource.id,data).success(function(resopnse){
				if(data.length == 0){
					logger.logSuccess("Data cleansing cleared successfully");
					DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
						$scope.$emit("shareDataSourceToAllController", data);
					});
					$scope.dataCleansingFrom="";
					$scope.dataCleansingTo="";
				}else{
					logger.logSuccess("Data cleansing saved successfully");
					DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
						$scope.$emit("shareDataSourceToAllController", data);
					});
					$scope.dataCleansingFrom="";
					$scope.dataCleansingTo="";	
				}
			}).error(function(resopnse){
				logger.logError("Problem in saving Data Cleanser");
			});
		};		

	}]).controller("createNewAdvancedTransformationModalCntr" , ["$routeParams","DataSourceService","length", "uniqueArray","$scope" , "$modalInstance" , "$http", "existingDataSource","logger", "fieldMappings", "$route", function($routeParams, DataSourceService, length, uniqueArray,$scope , $modalInstance , $http,  existingDataSource,logger, fieldMappings, $route){

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.counter = 0;
		$scope.selectedFields=[];
		var dataSource  = existingDataSource;
		$scope.existingTransformation=[],
		$scope.transformationNotifier = [];
		$scope.selectedFields.push(fieldMappings);
		if(fieldMappings.transformation){
			_.each(fieldMappings.transformation, function(data){
				$scope.transformationNotifier.push(_.pick(data, 'fromValue','toValue' , 'inputFieldsPos'));
			});	
		}

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.dataSourceId = dataSource.id;

		// function to show uploaded transformations in textarea
		$scope.displayFileContents = function(contents) {

			var bool = true;
			var valid = contents.split("\n");
			for(var i = 0 ; i < valid.length - 1 ; i++){
				for(var j=i+1 ; j < valid.length ; j++){
					if(valid[i] !== "" && valid[j] !== "")
						if((valid[i].split("="))[0] === (valid[j].split("="))[0]){
							bool = false;
							break;
						}
				}
				if(bool == false){
					break;
				}
			}
			if(bool ==  false){
				$scope.results = "";
				logger.logError("From values are found more than one repeated.");
			}else{
				$scope.results = contents;
			}
		};

		if(fieldMappings.fieldDataType == "DATE"){
			$scope.set="Date";
		}
		else{
			$scope.set="text";
		}
		$scope.addTransformation = function(){

			if(length !== $scope.transformationNotifier.length){
				$scope.counter++;
			}
			if(fieldMappings.fieldDataType === "DATE"){

				if ((document.getElementById("transformationFrom").value === "" || document.getElementById("transformationTo").value === "")
						|| (document.getElementById("transformationFrom").value === undefined || document.getElementById("transformationTo").value === undefined)) {
					logger.logWarning("Please enter From Date  & To Date");
					document.getElementById("transformationFrom").value="";
					document.getElementById("transformationTo").value="";
					return;
				}
				var object = {
						inputFieldsPos : [fieldMappings.id],
						fromValue : document.getElementById("transformationFrom").value,
						toValue : document.getElementById("transformationTo").value	
				};

				document.getElementById("transformationFrom").value="";
				document.getElementById("transformationTo").value="";

				$scope.transformationNotifier.push(object) ;
				$scope.transformationNotifier = uniqueArray.unique($scope.transformationNotifier);
			}
			else{
				if ((document.getElementById("transformationFrom").value === "" || document.getElementById("transformationTo").value === "")
						|| (document.getElementById("transformationFrom").value === undefined || document.getElementById("transformationTo").value === undefined)) {
					logger.logWarning("Please enter fromValue & toValue");
					document.getElementById("transformationFrom").value="";
					document.getElementById("transformationTo").value="";
					return;
				}
				if(fieldMappings.fieldDataType == "INTEGER" || fieldMappings.fieldDataType == "DOUBLE" || fieldMappings.fieldDataType == "LONG")
				{
					if (isNaN(document.getElementById("transformationFrom").value) || isNaN(document.getElementById("transformationTo").value)){
						logger.logWarning("Please enter number in fromValue & toValue");
						document.getElementById("transformationFrom").value="";
						document.getElementById("transformationTo").value="";
						return false;
					}
				}
				if(fieldMappings.fieldDataType == "BOOLEAN")
				{
					if (/*((document.getElementById("transformationFrom").value).toLowerCase() === "true" || (document.getElementById("transformationFrom").value).toLowerCase() === "false")
					&& */((document.getElementById("transformationTo").value)
							.toLowerCase() === "true" || (document.getElementById("transformationTo").value)
							.toLowerCase() === "false")) {

					}
					else
					{
						logger.logWarning("Please Enter True or False only in fromValue & toValue");
						document.getElementById("transformationFrom").value="";
						document.getElementById("transformationTo").value="";
						return false;
					}
				}
				var bool = true;
				for(var i=0 ; i< $scope.transformationNotifier.length ; i++){
					if(document.getElementById("transformationFrom").value == $scope.transformationNotifier[i].fromValue){
						bool = false;
						break;
					}
				}

				if(bool == false){
					logger.logError("From value already exist");
					return false;
				}

				var object = {
						inputFieldsPos : [fieldMappings.id],
						fromValue : document.getElementById("transformationFrom").value,
						toValue : document.getElementById("transformationTo").value
				};
				document.getElementById("transformationFrom").value="";
				document.getElementById("transformationTo").value="";

				$scope.transformationNotifier.push(object) ;
				$scope.transformationNotifier = uniqueArray.unique($scope.transformationNotifier);
			}
		},
		$scope.removeTran = function(notification){				
			if(length !== $scope.transformationNotifier.length){
				$scope.counter++;
			}
			var index = -1;		

			var comArr = eval( $scope.transformationNotifier );

			for( var i = 0; i < comArr.length; i++ ) {
				if( comArr[i].toValue === notification.toValue && comArr[i].fromValue === notification.fromValue ) {
					index = i;
					break;
				}
			}
			if( index === -1 ) {
				alert( "Something gone wrong" );
			}
			$scope.transformationNotifier.splice( index, 1 );
		},
		$scope.saveTransformation = function(){
			fieldMappings.transformation = $scope.transformationNotifier;

			var bool = true;
			var valid = [];
			if($scope.results != undefined)
				valid = $scope.results.split("\n");
			for(var i = 0 ; i < valid.length - 1 ; i++){
				for(var j=i+1 ; j < valid.length ; j++){
					if(valid[i] !== "" && valid[j] !== "")
						if((valid[i].split("="))[0] === (valid[j].split("="))[0]){
							bool = false;
							break;
						}
				}
				if(bool == false){
					break;
				}
			}
			if(bool === false){
				logger.logError("From values are repeated.");
				return false;
			}

			if($scope.results != undefined){
				var lines = $scope.results.split("\n");
				for(var i=0 ; i<lines.length;i++){
					if(lines[i] === ""){
						lines.splice(i, 1);
						i--;
					}
				}

				for(var i=0 ; i<lines.length;i++){
					lines[i]  = (lines[i].split("\r"))[0];
					var arr = lines[i].split("=");
					var object = {
							inputFieldsPos : [fieldMappings.id],
							fromValue : arr[0], 
							toValue : arr[1]
					};
					fieldMappings.transformation.push(object);
				}
			}

			if(length === fieldMappings.transformation.length && $scope.counter === 0){
				logger.logError("Please update the transformation");
				return false;
			} 
			else{
				if(dataSource.tags==undefined || dataSource.tags!=null || dataSource.tags !="null"){
					dataSource.tags = [];
				} 
				var reqDataObject = {'datasourceId':$scope.dataSourceId,'tags':dataSource.tags, 'fieldMapping':fieldMappings};
				$http.post("/updateFieldMapping", reqDataObject).success(function(data, status, headers, config){
					if((fieldMappings.transformation).length == 0){
						logger.logSuccess("Field mapping-Transformation cleared successfully");
						$modalInstance.close();
						if($routeParams.param4 === undefined || $routeParams.param4 === null)
							$route.reload();
					}else{
						logger.logSuccess("Field mapping-Transformation saved successfully");
						$modalInstance.close();
						if($routeParams.param4 === undefined || $routeParams.param4 === null)
							$route.reload();
					}
				}).error(function(data){
					logger.logError("Error while storing the field mapping. Error -> " + data);
				});
			}
		},

		$scope.clearAllTransformation = function(){
			$scope.transformationNotifier = [];
			fieldMappings.transformation = [];
			if(dataSource.tags==undefined || dataSource.tags!=null || dataSource.tags !="null"){
				dataSource.tags = [];
			} 
			var reqDataObject = {'datasourceId':$scope.dataSourceId,'tags':dataSource.tags, 'fieldMapping':fieldMappings};
			$http.post("/updateFieldMapping", reqDataObject).success(function(data, status, headers, config){
				if((fieldMappings.transformation).length == 0){
					logger.logSuccess("Field mapping-Transformation cleared successfully");
				}
			}).error(function(data){
				logger.logError("Error while storing the field mapping. Error -> " + data);
			});
		},
		$scope.cancelTransformation = function(){
			$modalInstance.dismiss("cancel");
		};

	}]).controller("advancedTransformationController",  ["uniqueArray","$scope", "$modal" , "$log"  , "$location" , "logger", "$http", "$rootScope","DataSourceService",function(uniqueArray,$scope, $modal, $log , $location,logger,$http, $rootScope , DataSourceService){

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.oneAtATime = !0,
		$scope.ingections = "";

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});


		$scope.saveAdvancedTransformation = function(){

			var map = {};

			if(!_.isUndefined($scope.field1)){
				map['1'] =  [$scope.field1.id];
			}
			if(!_.isUndefined($scope.field2)){
				map['2'] =  [$scope.field2.id];
			}
			if(!_.isUndefined($scope.field3)){
				map['3'] =  [$scope.field3.id];
			}
			if(!_.isUndefined($scope.field4)){
				map['4'] =  [$scope.field4.id];
			}
			if(!_.isUndefined($scope.field5)){
				map['5'] =  [$scope.field5.id];
			}
			if(!_.isUndefined($scope.field6)){
				map['6'] =  [$scope.field6.id];
			}
			if(!_.isUndefined($scope.field7)){
				map['7'] =  [$scope.field7.id];
			}
			if(!_.isUndefined($scope.field8)){
				map['8'] =  [$scope.field8.id];
			}

			var apiDomainObject = {
					name : "com.augmentiq.identiq.apis.IdentIQEngine",
					outputFieldsPos : [2,3,4,5,6],
					outDel : "|",
					inFieldPosForApi : map,
					initFn : "init",
					transformFnInputTypes : ["STRING","STRING","STRING","STRING","STRING","STRING","STRING","STRING"],
					transformFn : "parseAddress"
			};

			var transformationObject = {
					inputFieldsPos : null,
					type : "OTHER",
					fromValue : null,
					toValue : null,
					replaceAll : null,
					apiDomain : apiDomainObject
			};

			var reqDataObject = {'dataSourceId': $scope.dataSourceId, 'transformations' : transformationObject};
			$http.post("/addAdvancedTransformation", reqDataObject).success(function(data, status, headers, config){
				logger.logSuccess("Advanced transformations saved successfully.");
			}).error(function(data, status, headers, config){
				logger.logError("error while saving transformation");
			});
		};	
	}]).controller("fieldAddController" , ["$modal","$route","datasource","$scope","processData","$modalInstance" , "$upload" ,  "DataSourceService", "logger"  , "$http", "uniqueArray","ModalWindowService","saveExpFactory","$rootScope","validateExpFactory", function($modal,$route,datasource, $scope, processData, $modalInstance , $upload , DataSourceService , logger ,$http, uniqueArray,ModalWindowService,saveExpFactory,$rootScope,validateExpFactory){


		$scope.OpenPopupExpressionForTestRun=function(regExp){
			$rootScope.textAreaContents = $scope.addVarMath.mathExpression;
			var textAreaValue = $rootScope.textAreaContents;
			ModalWindowService.openAddItemDialog(textAreaValue);
		}
		$scope.AddFieldMappingFromExpression = function(){
			
			var modalInstance = $modal.open({
				size: "lg",
				templateUrl: "/views/template/addExpressionContent.html",
				controller: "configureExpressionContrller",
				resolve : {
					expression : function(){
						return null;
					}
				}
			});
			modalInstance.result.then(function (listOfExpression) {
				$scope.clearScopeVariables("add_var_math");
				$scope.expList = angular.copy(listOfExpression);
			}, function() {
				$scope.clearScopeVariables("add_var_math");
				$rootScope.disableSection = true;
				$http({
					method : "GET",
					url : "/getExpressionDetails"
				}).success(function(data){
					$scope.expList = data;
				});
			});
		};
		$scope.createExpression=function()
		{
			addDataObj = { "outPutVariable": angular.copy($scope.addVarMath.outPutVariable), "outPutVariabeFieldType" : angular.copy($scope.addVarMath.outPutVariabeFieldType), 
					"mathExpression": angular.copy($scope.addVarMath.mathExpression) , "processType": "add_var_math" };
			saveExpFactory.createExpression($http,addDataObj,logger);
		}
		$scope.validateExpression=function()
		{
			addDataObj = { "outPutVariable": angular.copy($scope.addVarMath.outPutVariable), "outPutVariabeFieldType" : angular.copy($scope.addVarMath.outPutVariabeFieldType), 
					"mathExpression": angular.copy($scope.addVarMath.mathExpression) , "processType": "add_var_math" };
			validateExpFactory.validateExpression($http,addDataObj,logger);
		}
		$http.post("/getAppProcessMassages").success(function(data, status, headers, config){
			$scope.explanation = data;
		});

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.dataTypes = $modalInstance.dataType;
		$scope.datasource = datasource;
		$scope.dsLevelParam = $scope.datasource.dsLevelParams;
		$scope.binningFields = [];
		$rootScope.placeholderValue = "Variable name";
		$rootScope.disableSection = true;
		$rootScope.disable = false;
		$rootScope.disableDropDrown = false;

		$scope.FLOWCHART_CONSTANTS = null;

		function getMaximumNumberValue(str){
			var max = 0;
			for (i = 0; i < str.length; i++) 
				if(parseInt(str[i])>max){
					max = parseInt(str[i]);
				}
			return max;
										}

		$http({
			method : "post",
			url : "/getDatabaseFunctionsJson",
			data : 3
		}).success(function(data){
			$scope.databaseFunctionsJson = data;

			$http({
				method : "post",
				url : "/getDatabaseFunctionsParametersJson",
				data : 3
			}).success(function(data){
				$scope.databaseFunctionsParametersJson = data;

				var maxDatabaseFunctionParametersIndex = getMaximumNumberValue(Object.keys($scope.databaseFunctionsParametersJson)) + 1;
				$scope.databaseFunctionsJson.fxDatabaseFunctionsJson[maxDatabaseFunctionParametersIndex] = {
						"id": maxDatabaseFunctionParametersIndex,
						"name":"case when",
						"parameters":"CASE WHEN <CONDITION> THEN <VALUE> [WHEN <CONDITION> THEN <VALUE>]* [ELSE <VALUE>] END",
						"returnType":"{(Column)}",
						"databaseFunctionType":"FX",
						"databaseFunctionTypeId":"6",
						"databaseFunctionUDFId":"0",
						"databaseName":"SPARK_SQL",
						"databaseNameId":"3"
				}

				$scope.databaseFunctionsParametersJson[maxDatabaseFunctionParametersIndex]= "CASE WHEN () THEN () [WHEN () THEN ()]* [ELSE ()] END";
			}).error(function(data){
				logger.logError(data);
			});
		}).error(function(data){
			logger.logError(data);
		});
		
		$http({
			method : "POST",
			url : "/getExpressionDetails"
		}).success(function(data){
			$scope.expList = data;
		});
		
		$scope.populate = function(object){
			$scope.addVarMath = angular.copy(object);
			$scope.addVarMath.selectMathDataType = object;
		};

									
		var backUp = _.reject($scope.datasource.fieldMappings , function(values) {;
		return values.derivedFieldInd == false || values.addVarIndicator === true;
		});

		$scope.$on("eventForFieldAdd", function (event, args) {
			if(args == "Expressions"){
				$scope.addVariableDropDown = "Math";
				$scope.mathMessage = false;
				$scope.clearScopeVariables("add_var_math");
			}
			else if(args == "Binning"){
				$scope.addVariableDropDown = "binning";
				$scope.binningFields = angular.copy($scope.inputFields[0].fields);
				_.each($scope.outPutFields[0].fields , function(values){
					if(values.fieldSourceEnum == "calculated"){
						var valueTest = angular.copy(values);
						valueTest.pos = $scope.binningFields[$scope.binningFields.length - 1].pos + 1;
						$scope.binningFields.push(valueTest);
					}
				});

				$scope.binningFieldsRange = _.reject($scope.binningFields , function(fields){
					return (fields.fieldType).toUpperCase() !== "INTEGER"  && (fields.fieldType).toUpperCase() !== "LONG" && (fields.fieldType).toUpperCase() !== "DOUBLE" && (fields.fieldType).toUpperCase() !== "DATE";
				});

				if($scope.binningRanges  == undefined) {
					$scope.binningRanges = [ { "seq": 0,"from": "0", "to": "", "label": "" } ];
				}
				$scope.binningOnElse = {"rangeElse" : ""};
				$scope.binningType = "";
				$scope.clearScopeVariables("add_var_binning");
			}
			else if(args == "Fixed"){
				$scope.addVariableDropDown = "Fixed Value";
				if($scope.periodModel.dataValue == undefined){
					$scope.periodModel = {
							dataValue: ""
					};
				}
				$scope.clearScopeVariables("add_var_segment");
			}

		});

		$scope.bin = { "binningFieldValue" : null } ;

		if(processData == null || processData == undefined || processData == ""){
			processData = { "input":{"sources":[]},"process": [] ,"nodeType":"node_create_variables","nodeId":0,"appId":0,
					"dataSourceId":0
			};
			if(processData.input.sources.length == 0){
				processData.input.sources[0] = {"fields":[]};
			}
			processData.input.sources[0].refDataSourceId = $scope.datasource.id;

			_.each($scope.datasource.fieldMappings , function(values){
				var object = {"filedName" : "" , "sourceLabel": "" , "select" : true,"fieldSourceEnum" : null , 
						"sourceRefField" : null , "pos" :"" , "source" : "" , "format":"" , "fieldType" : ""};
				object.filedName = values.fieldName;
				object.sourceLabel = $scope.datasource.dataSourceName;
				object.fieldSourceEnum = null;
				object.sourceRefField = null;
				object.pos = values.position;
				object.format = values.format;
				object.fieldType = values.fieldDataType;
				object.source = $scope.datasource.dataSourceName;
				processData.input.sources[0].fields.push(object);
			});

			$scope.inputFields = processData.input.sources;

			$scope.outPutFields = [];
			$scope.outPutFields[0] = {"fields":[]};

			$http.post("/getAppIdNodeId").success(function(data, status, headers, config){

				processData.appId = data[0];
				processData.nodeId = data[1];

			}).error(function(data){
				logger.logError("Error while retrieving Ids -> " + data);
			});
		}
		else{
			$scope.outPutFields = processData.outPut.sources;
			$scope.inputFields = processData.input.sources;			
		}


		if(processData != null || processData == undefined || processData == ""){
			processData.input.sources[0] = {"fields":[]};
			processData.input.sources[0].refDataSourceId = $scope.datasource.id;
			_.each($scope.datasource.fieldMappings , function(values){
				var object = {"filedName" : "" , "sourceLabel": "" , "select" : true,"fieldSourceEnum" : null , 
						"sourceRefField" : null , "pos" :"" , "source" : "" , "format":"" , "fieldType" : ""};
				object.filedName = values.fieldName;
				object.sourceLabel = $scope.datasource.dataSourceName;
				object.fieldSourceEnum = null;
				object.sourceRefField = null;
				object.pos = values.position;
				object.format = values.format;
				object.fieldType = values.fieldDataType;
				object.source = $scope.datasource.dataSourceName;
				if(values.addVarIndicator !== true && values.derivedFieldInd !== true)
					processData.input.sources[0].fields.push(object);
			});

			$scope.inputFields = processData.input.sources;
		}
		processData.dataSourceId = $scope.datasource.id;
		$scope.processDatas = processData.process;

		var lengthOfProcess = processData.process.length;

		$scope.periodModel = { "dataValue"  : "" };

		$scope.createNewFields = function(){

			for(var i = 0;i < $scope.datasource.fieldMappings.length;i++){
				if($scope.datasource.fieldMappings[i].addVarIndicator !== null 
						&& $scope.datasource.fieldMappings[i].addVarIndicator !== undefined 
						&& $scope.datasource.fieldMappings[i].addVarIndicator === true){
					datasource.fieldMappings.splice(i , 1);
					for(var j= i ; j < $scope.datasource.fieldMappings.length ; j++){
						$scope.datasource.fieldMappings[j].position = j;
					}
					i--;
				}
			};

			datasource.fieldMappings = _.reject(datasource.fieldMappings , function (values){
				return values.derivedFieldInd === true;
			});
			var flagTest = true;
			_.each($scope.outPutFields[0].fields , function(outPutValues){
				if((outPutValues.filedName.toUpperCase()).indexOf("GP_") === 0){
					flagTest = false;
					return ;
				}
			});			

			if(flagTest === false){
				logger.logError("Output field name start with gp_");
				return false;
			}

			_.each($scope.outPutFields[0].fields,function(newValues){
				var DS = {"dataSourceId":"" , "derivedFieldInd":true , "addVarIndicator":true , "fieldDataType" : (newValues.fieldType).toUpperCase() , 
						"fieldName":newValues.filedName , "position" : datasource.fieldMappings.length, "sampleRecord1":"" , "sampleRecord2":"", 
						"sampleRecord3":"" ,"format":newValues.format};
				datasource.fieldMappings.push(DS);
			});
													_
			_.each(backUp , function(values){
				values.position = datasource.fieldMappings.length;
				datasource.fieldMappings.push(values);
			});

			var i = -1;
			$scope.binningFields = _.reject($scope.inputFields[0].fields, function(values){
				i++;
				return $scope.datasource.fieldMappings[i].derivedFieldInd === true;
			});

			_.each($scope.outPutFields[0].fields , function(values){
				if(values.fieldSourceEnum == "calculated"){
					var valueTest = angular.copy(values);
					valueTest.pos = $scope.binningFields[$scope.binningFields.length - 1].pos + 1;
					$scope.binningFields.push(values);
				}
			});
			$scope.binningFieldsRange = _.reject($scope.binningFields , function(fields){
				return (fields.fieldType).toUpperCase() !== "INTEGER" && (fields.fieldType).toUpperCase() !== "DOUBLE" && (fields.fieldType).toUpperCase() !== "LONG" && (fields.fieldType).toUpperCase() !== "DATE" && (fields.fieldType).toUpperCase() !== "BOOLEAN";
			});

			if($scope.count !== 0)
				processData.saveAs = $scope.saveOnFile;
			else
				processData.saveAs = processData.saveAs;

			processData.indicator = "DR";
			processData.dataSourceId = datasource.id;

			$http.post("/saveProcessNodeData", processData ).success(function(data, status, headers, config) {
				if(data.toUpperCase() !== "SUCCESS"){
					logger.logError(data);
					return false;
				}else{
					if(processData.outPut.sources[0].fields.length > 0){
						datasource.appId = processData.appId;
						datasource.nodeId = processData.nodeId;
					}else{
						datasource.appId = null;
						datasource.nodeId = null;
					}

					$modalInstance.close(datasource);
				}
			});

		},
		$scope.cancelFields = function(){
			$modalInstance.dismiss();
		},
		$scope.onAddFixedVariableValueForMath = function(dataType,variableName,segmentName,requestFor){
			$scope.evaluateExpression(dataType,variableName,segmentName,requestFor);
			dataType = undefined;
			variableName = "";
			segmentName = "";
		},
		$scope.onKeyUp = function(value) {
			$scope.mathExpression = value;
		},
		$scope.onselectInputParameter = function(index) {
			$scope.addIntoMathExpression("{" + $scope.dsLevelParam[index].paramName +"}");
		},
		$scope.evaluateExpression = function(dataType,variableName,segmentName,requestFor) {
			// ADD NEWLY ADDED VARIABLE TO PROCESS
			var sourceName = undefined;
			if($scope.inputFields[0] !== undefined &&  $scope.inputFields[0].fields[0] !== undefined)
			{
			var sourceName = $scope.inputFields[0].fields[0].source;
			}
			$scope.addToProcessDomain(dataType,variableName,sourceName,segmentName,requestFor);


			if(null == processData.outPut){
				processData.outPut = {"sources" : [ { "fields" : [ ] }]};
			}

			$http({
				method 	: "post",
				url 	: "/evaluateExpressionForAddVariables",
				data 	: processData
			}).success(function(data, status, headers, config){
				if(null != data.errorMessage 
						&& "null" != data.errorMessage){
					//REMOVE NEWLY ADDED VARIABLE FROM PROCESS

					if(undefined === $scope.updatedFieldName 
							|| "" === $scope.updatedFieldName 
							|| null === $scope.updatedFieldName) {
						processData.process.pop();
					}

					logger.logError(data.errorMessage);
					$scope.clearScopeVariables(requestFor);
				}else{
					//REMOVE NEWLY ADDED VARIABLE FROM PROCESS
					//processData.process.pop();
					//AND ADD IT PERMANENTLY

					$scope.onAddFixedVariableValue(dataType, variableName, segmentName, requestFor, undefined, undefined, true);
					$scope.clearScopeVariables(requestFor);
				}
				//databaseFunctionsParametersJson = data;

			}).error(function(status){
				$scope.paramList = [];
				logger.logError(status);
			});
		};
		function checkAndUpdateFieldData(dataType,variableName,segmentName) {

			for(var i =0 ; i < $scope.outPutFields.length; i++ ) {
				for(var j= 0 ; j < $scope.outPutFields[i].fields.length; j++) {
					if($scope.outPutFields[i].fields[j].filedName == $scope.updatedFieldName) {
						$scope.outPutFields[i].fields[j].filedName = variableName;
						$scope.outPutFields[i].fields[j].fieldNameAlias = variableName;
						$scope.outPutFields[i].fields[j].fieldType = dataType;
					}
				}
			}
			for(var i =0 ; i < processData.process.length; i++) {
				if(processData.process[i].outPutVariable == $scope.updatedFieldName){
					if($scope.addVariableDropDown == "Fixed Value"){
						processData.process[i].outPutVariable = variableName;
						processData.process[i].dataType = dataType;
						processData.process[i].segment = segmentName;
					} else if($scope.addVariableDropDown == "Math"){
						processData.process[i].outPutVariable = variableName;
						processData.process[i].outPutVariabeFieldType = dataType;
						processData.process[i].mathExpression = segmentName;
					}
					else if($scope.addVariableDropDown == "binning"){
						$scope.processDatas[i].outPutVariabeFieldType = dataType;
						$scope.processDatas[i].outPutVariable = variableName;
						$scope.processDatas[i].binningOnField = segmentName;
						$scope.processDatas[i].binningRanges = $scope.binningRanges;
						$scope.processDatas[i].binningType  = $scope.addVarMath.binningType;
						$scope.processDatas[i].rangeElse = $scope.binningOnElse.rangeElse;
						$scope.processDatas[i].elseCondition = $scope.binningOnElse.rangeElse;
					}
				}
			}
		};
		$scope.onAddFixedVariableValue = function(dataType,variableName,segmentName,requestFor,rangeElse,binningType, isEnable) {

			if(isEnable === undefined || isEnable === null) {
				isEnable = false;
			}

			if(variableName == undefined){
				logger.logError("No special charector allows other than underscore in variable name");
				return false;
			}

			if(($scope.updatedFieldName != undefined && $scope.updatedFieldName != "" && $scope.updatedFieldName.length > 0)) {

				if($scope.updatedFieldName != variableName && !checkVariableIsAlreadyAdd(variableName)){
					return false;
				}	else {
					checkAndUpdateFieldData(dataType,variableName,segmentName,binningType);
					$scope.clearScopeVariables(requestFor);
					$scope.setMode(false);
					logger.logSuccess("Updated Successfully.");
				}

			} else if(checkVariableIsAlreadyAdd(variableName) == false) {
				return false;
			} else {
				if(processData.process == null 
						|| 0 == processData.process.length) {
					processData.process =  [];
					$scope.processDatas =  processData.process;
				}
				var sourceName = $scope.inputFields[0].fields[0].source;
				$scope.onAddVariableToOutPutList(-1,dataType,variableName,sourceName,requestFor);
				if(!isEnable) {
					$scope.addToProcessDomain(dataType,variableName,sourceName,segmentName,requestFor);
				}

				$scope.clearScopeVariables(requestFor);
			}
		},

		$scope.onAddVariableToOutPutList = function(index,dataType,variableName,sourceName,requestFor){

			var pos;
			var variable;
			var object;
			var format;
			if(dataType.toUpperCase() == "DATE"){
				if(requestFor === "add_var_binning" || requestFor === "add_var_segment"){
					format = "dd/MM/yyyy";
				}else{
					format = "dd/MM/yyyy/HH/mm/ss";
				}
			}else{
				format = "";
			}

			if($scope.outPutFields != null && $scope.outPutFields[0].fields.length > 0){
				pos = $scope.outPutFields[0].fields.length;
				if($scope.show == "4" || $scope.show == "6"){
					pos = pos - 1;
				}
				variable = $scope.outPutFields[0].fields[0];
				var checkForInputField= { "newType" : "calculated" };
				var sourceReferenceField = getSourceReferenceFieldObject(variableName,variable,dataType,pos,checkForInputField);

				if(sourceReferenceField.format !== null && sourceReferenceField.format !== undefined && sourceReferenceField.format !== ""){
					format = sourceReferenceField.format; 
				}
				object = {
						"fieldType" : dataType,
						"fieldDataType" : dataType,
						"source" : sourceName,
						"fieldSourceEnum" : checkForInputField.newType,
						"pos" : pos,
						"select" : true,
						"fieldNameAlias" : variableName,
						"format":format,
						"filedName" : variableName,
						"sourceLabel" : variable.sourceLabel,
						"sourceRefField" : sourceReferenceField
				};   
			}else {
				pos = 0;
				var checkForInputField= { "newType" : "calculated" };
				variable = $scope.outPutFields[0].fields[0];
				var sourceReferenceField = getSourceReferenceFieldObject(variableName,variable,dataType,pos,checkForInputField);
				if(sourceReferenceField.format !== null && sourceReferenceField.format !== undefined && sourceReferenceField.format !== ""){
					format = sourceReferenceField.format; 
				}
				processData.outPut = {"sources" : [ { "fields" : [ ] }]};
				$scope.outPutFields = processData.outPut.sources;
				object = {
						"fieldType" : dataType,
						"fieldDataType" : dataType,
						"source" : sourceName,
						"fieldSourceEnum" : "calculated",
						"pos" : pos,
						"format":format,
						"filedName" : variableName,
						"sourceLabel" : $scope.datasource.dataSourceName,
						"fieldNameAlias" : variableName,
						"sourceRefField" : sourceReferenceField
				};
			}
			$scope.outPutFields[0].fields.push(object);
			logger.logSuccess("Added Successfully");
		},
		$scope.selectMathFunction = function(number){
			$scope.addIntoMathExpression(selectMathOperationPerformOnField(number));
		};

		$scope.addVarMath = {"mathOutPutVariable" : "", "selectMathDataType": undefined,
				"selectBinningDataType": undefined, "binningVariableName" : "",
				"selectDataType": undefined, "outPutVariableName": "", "binningType" :""};

		function checkVariableIsAlreadyAdd(variableName) {

			var matchFound = true;
			var matchInputFound = true;
			if(variableName.indexOf(" ") >= 0){
				logger.logError("Please dont provide any white spaces");
				return false;
			} 
			if($scope.outPutFields[0] !== undefined)
				_.find($scope.outPutFields[0].fields, function(field) {
					if(field.filedName != null && field.filedName.toUpperCase() == variableName.toUpperCase()){ matchFound = false; } 
				});
			if($scope.datasource.fieldMappings[0] !== undefined)
				_.find($scope.datasource.fieldMappings, function(field) {
					if(field.fieldName != null && field.fieldName.toUpperCase() == variableName.toUpperCase()){ matchInputFound = false; } 
				});
			if(!matchFound){
				logger.logError("Variable with same name already exist.");
				return false;
			}else if(!matchInputFound){
				logger.logError("Name already in Input Parameter");
				return false;
			}

			return matchFound;
		}

		var selectMathOperationPerformOnField = function (numberValue){
		
			return $scope.databaseFunctionsParametersJson[numberValue];
		};

		$scope.addIntoMathExpression = function(value){

			if($scope.mathExpression == undefined){
				$scope.mathExpression = "";
			}
			$scope.$root.$broadcast("add",value);
			$scope.mathExpression = $scope.$root.textAreaValue;
			$scope.addVarMath.mathExpression = $scope.mathExpression;
		},
		$scope.onShowFunction = function(value) {
			$scope.showFunction = "0";
			$scope.showFunction = value;
		},
		$scope.onChangeAddVariable = function(valueName) {
			$scope.showPopUpFor = valueName;
			if(valueName == "Fixed Value") {
				if($scope.periodModel == undefined || $scope.periodModel.dataValue == undefined){
					$scope.periodModel = {
							dataValue: ""
					};
				}
				$scope.clearScopeVariables("add_var_segment"); 

			} else if (valueName == "binning"){

				$scope.binningFields = [];

				var i = -1;
				$scope.binningFields = _.reject($scope.inputFields[0].fields, function(values){
					i++;
					return $scope.datasource.fieldMappings[i].derivedFieldInd === true;
				});

				_.each($scope.outPutFields[0].fields , function(values){
					if(values.fieldSourceEnum == "calculated"){
						var valueTest = angular.copy(values);
						valueTest.pos = $scope.binningFields[$scope.binningFields.length - 1].pos + 1;
						$scope.binningFields.push(values);
					}
				});

				$scope.binningFieldsRange = _.reject($scope.binningFields , function(fields){
					return (fields.fieldType).toUpperCase() !== "INTEGER" && (fields.fieldType).toUpperCase() !== "DOUBLE" && (fields.fieldType).toUpperCase() !== "LONG" && (fields.fieldType).toUpperCase() !== "DATE" && (fields.fieldType).toUpperCase() !== "BOOLEAN";
				});

				if($scope.binningRanges  == undefined) {
					$scope.binningRanges = [ { "seq": 0,"from": "0", "to": "", "label": "" } ];
				}
				$scope.binningOnElse = {"rangeElse" : ""};
				$scope.addVarMath.binningType = "";
				$scope.clearScopeVariables("add_var_binning"); 

			} else if(valueName == "Math"){
				$scope.mathMessage = false;
				$scope.clearScopeVariables("add_var_math"); 
			}

		},
		$scope.onchangeBinningField = function(binningFieldValue) {
			$scope.binningRangesVariableTypes = binningFieldValue.fieldType;
		},
		$scope.onChangeDataTypeValue = function(value){
			$scope.fixedVariableField = value;
		},
		$scope.clearScopeVariables = function (requestFor){
			$scope.updatedFieldName = "";
			if(requestFor == "add_var_segment"){
				if (!$scope.addVarMath)
					$scope.addVarMath = {};
				$scope.addVarMath.selectDataType = "";
				$scope.addVarMath.outPutVariableName = "";
				if(!$scope.periodModel)
					$scope.periodModel = {};
				$scope.periodModel.dataValue = "";
				$scope.fixedVariableField = "";
			} else if(requestFor == "add_var_math") {
				if (!$scope.addVarMath)
					$scope.addVarMath = {};
				$scope.addVarMath.selectMathDataType = undefined;
				$scope.addVarMath.outPutVariable="";
				$scope.addVarMath.outPutVariabeFieldType="";
				$scope.addVarMath.mathExpression="";
				$scope.addVarMath.mathOutPutVariable = "";
				$scope.mathExpression = "";
				$scope.mathMessage = false;
				$scope.$root.$broadcast("clear","");
			} else if(requestFor == "add_var_binning") {
				if (!$scope.addVarMath)
					$scope.addVarMath = {};
				$scope.addVarMath.selectBinningDataType = "";
				$scope.addVarMath.binningVariableName ="";
				if (!$scope.bin)
					$scope.bin = {};
				$scope.bin.binningFieldValue = "";
				$scope.addVarMath.binningType = "";
				if (!$scope.binningOnElse)
					$scope.binningOnElse = {};
				$scope.binningOnElse.rangeElse = "";
				$scope.binningRanges = [ { "seq": 0,"from": "0", "to": "", "label": "" }];
			} else if(requestFor == "node_transpose"){
				$scope.transposeDomain = {};
				$scope.$root.$broadcast("clear","");
			}
		},
		$scope.onselectOutputFields = function(index) {
			$scope.addIntoMathExpression("{" + $scope.outPutFields[0].fields[index].filedName +"}");
		},
		$scope.onselectInputFields = function(index) {
			$scope.addIntoMathExpression("{" + $scope.datasource.fieldMappings[index].fieldName +"}");
		};
		$scope.addToProcessDomain = function(dataType,variableName,sourceName,segmentName,requestFor){
			var addDataObj={};

			if(requestFor == "add_var_segment") {
				addDataObj = {"segment" : angular.copy(segmentName),"dataType" : angular.copy(dataType),
						"outPutVariable" : angular.copy(variableName),"processType" : "add_var_segment" };

			} else if (requestFor == "add_var_math") {			
				addDataObj = { "outPutVariable": angular.copy(variableName), "outPutVariabeFieldType" : angular.copy(dataType), 
						"mathExpression": angular.copy(segmentName) , "processType": "add_var_math" };

			} else if (requestFor == "add_var_binning") {

				addDataObj = { "binningType" : angular.copy($scope.addVarMath.binningType),"binningRanges": angular.copy($scope.binningRanges) , "processType":"add_var_binning",
						"binningOnField": angular.copy(segmentName),
						"outPutVariable" : angular.copy(variableName) ,"outPutVariabeFieldType" : angular.copy(dataType), "rangeElse" : angular.copy($scope.binningOnElse.rangeElse)};
			}

			if(undefined !== $scope.updatedFieldName
					&& "" !== $scope.updatedFieldName
					&& null !== $scope.updatedFieldName 
					&& 0 < $scope.updatedFieldName.length){

				for(var i =0; i < processData.process.length;i++){
					if($scope.updatedFieldName === processData.process[i].outPutVariable){
						processData.process[i] = angular.copy(addDataObj);
						break;
					}
				}

			}else{
				processData.process.push(angular.copy(addDataObj));
			}
			addDataObj = undefined

		
		},
		$scope.onDeleteFiled = function(indexToDelete,parentValue,fieldName,requestFor) {
			$scope.outPutFields[parentValue].fields.splice(indexToDelete,1);
			_.groupBy($scope.processDatas, function(num,index) {
				if(num != undefined && num.outPutVariable == fieldName) {
					$scope.clearScopeVariables($scope.processDatas[index].processType);
					$scope.processDatas.splice(index,1);
				}
			});

			if(($scope.outPutFields[0].fields.length  > indexToDelete)) {
				for(var i = indexToDelete ; i < $scope.outPutFields[0].fields.length ;i++) {
					if($scope.outPutFields[parentValue] !== undefined)
						$scope.outPutFields[parentValue].fields[i].pos = i;	
				}
			}
		};

		$scope.$on("eventForFieldAdd", function (event, args) {
			if(args == "Expressions"){
				$scope.addVariableDropDown = "Math";
				$scope.mathMessage = false;
				$scope.clearScopeVariables("add_var_math");
			}
			else if(args == "Binning"){
				$scope.addVariableDropDown = "binning";
				$scope.binningFields = angular.copy($scope.inputFields[0].fields);
				_.each($scope.outPutFields[0].fields , function(values){
					if(values.fieldSourceEnum == "calculated"){
						var valueTest = angular.copy(values);
						valueTest.pos = $scope.binningFields[$scope.binningFields.length - 1].pos + 1;
						$scope.binningFields.push(valueTest);
					}
				});

				$scope.binningFieldsRange = _.reject($scope.binningFields , function(fields){
					return (fields.fieldType).toUpperCase() !== "INTEGER"  && (fields.fieldType).toUpperCase() !== "LONG" && (fields.fieldType).toUpperCase() !== "DOUBLE" && (fields.fieldType).toUpperCase() !== "DATE" && (fields.fieldType).toUpperCase() !== "TIMESTAMP";
				});

				if($scope.binningRanges  == undefined) {
					$scope.binningRanges = [ { "seq": 0,"from": "0", "to": "", "label": "" } ];
				}
				$scope.binningOnElse = {"rangeElse" : ""};
				$scope.addVarMath.binningType = "";
				$scope.clearScopeVariables("add_var_binning");
			}
			else if(args == "Fixed"){
				$scope.addVariableDropDown = "Fixed Value";
				if($scope.periodModel.dataValue == undefined){
					$scope.periodModel = {
							dataValue: ""
					};
				}
				$scope.clearScopeVariables("add_var_segment");
			}
		});

		$scope.setMode = function(mode)  {
			$scope.isUpdateCaption=mode;
		}
		$scope.onUpdateFieldValue = function(indexToEdit,parentValue)  {

			var variableName = $scope.outPutFields[0].fields[indexToEdit].filedName;

			for(var i = 0; i < $scope.processDatas.length ; i++ ) {
				if(variableName == $scope.processDatas[i].outPutVariable 
						&& "add_var_segment" == $scope.processDatas[i].processType) {
					$scope.addVariableDropDown = "Fixed Value";
					$scope.showPopUpFor = "Fixed Value";
					$scope.$broadcast("editOutPutAddFields", 2);
					$scope.updatedFieldName = $scope.processDatas[i].outPutVariable;
					$scope.addVarMath.selectDataType = $scope.processDatas[i].dataType;
					$scope.addVarMath.outPutVariableName = $scope.processDatas[i].outPutVariable;
					$scope.periodModel.dataValue = $scope.processDatas[i].segment;
					$scope.onChangeDataTypeValue($scope.processDatas[i].dataType);
					break;
				} else if(variableName == $scope.processDatas[i].outPutVariable 
						&& "add_var_math" == $scope.processDatas[i].processType) {
					$scope.addVariableDropDown = "Math";
					$scope.showPopUpFor = "Math";
					$scope.$broadcast("editOutPutAddFields", 0);
					$scope.addVarMath.selectMathDataType = $scope.processDatas[i];
					$scope.addVarMath.mathOutPutVariable = angular.copy($scope.processDatas[i].outPutVariable);
					$scope.$root.$broadcast("clear","");
					$scope.$root.$broadcast("add",$scope.processDatas[i].mathExpression);
					$scope.mathExpression = $scope.processDatas[i].mathExpression;
					$scope.updatedFieldName = $scope.processDatas[i].outPutVariable;
					
					$scope.addVarMath.outPutVariable = $scope.processDatas[i].outPutVariable;
					$scope.addVarMath.outPutVariabeFieldType = $scope.processDatas[i].outPutVariabeFieldType;
					$scope.addVarMath.mathExpression = $scope.processDatas[i].mathExpression;
					break;
				} else if(variableName == $scope.processDatas[i].outPutVariable 
						&& "add_var_binning" == $scope.processDatas[i].processType) {

					var j = -1;
					$scope.binningFields = _.reject($scope.inputFields[0].fields, function(values){
						j++;
						return $scope.datasource.fieldMappings[i].derivedFieldInd === true;
					});

					_.each($scope.outPutFields[0].fields , function(values){
						if(values.fieldSourceEnum == "calculated"){
							var valueTest = angular.copy(values);
							valueTest.pos = $scope.binningFields[$scope.binningFields.length - 1].pos + 1;
							$scope.binningFields.push(values);
						}
					});

					$scope.binningFieldsRange = _.reject($scope.binningFields , function(fields){
						return (fields.fieldType).toUpperCase() !== "INTEGER" && (fields.fieldType).toUpperCase() !== "DOUBLE" && (fields.fieldType).toUpperCase() !== "LONG" && (fields.fieldType).toUpperCase() !== "DATE" && (fields.fieldType).toUpperCase() !== "BOOLEAN";
					});

					$scope.addVariableDropDown = "binning";
					$scope.showPopUpFor = "binning";
					$scope.$broadcast("editOutPutAddFields",1);
					$scope.addVarMath.selectBinningDataType = $scope.processDatas[i].outPutVariabeFieldType;
					$scope.addVarMath.binningVariableName = $scope.processDatas[i].outPutVariable;
					$scope.bin.binningFieldValue = $scope.processDatas[i].binningOnField;
					$scope.binningRanges = $scope.processDatas[i].binningRanges;
					$scope.addVarMath.binningType = $scope.processDatas[i].binningType;
					if($scope.binningOnElse == undefined){
						$scope.binningOnElse = { "rangeElse" : ""};
					}
					$scope.binningOnElse.rangeElse = $scope.processDatas[i].rangeElse;
					$scope.updatedFieldName = $scope.processDatas[i].outPutVariable;
					break;

				} 
			}
		};
		function getSourceReferenceFieldObject(variableName,variable,dataType,pos,checkForInputField){

			var sourceReferenceField  =  _.filter($scope.inputFieldsValue,function(num,j){
				if(variableName == num.filedName) {
					return num;
				}
			});

			if(sourceReferenceField != null && sourceReferenceField.length > 0){

				if(checkForInputField != undefined){
					checkForInputField.newType = "input";
				}
				return sourceReferenceField[0];
			}else {
				return { "fieldSourceEnum" : null, 
					"fieldType": dataType,
					"filedName" : variableName,
					"pos": pos,
					"select": true,
					"source": $scope.datasource.dataSourceName,
					"sourceLabel": $scope.datasource.dataSourceName,
					"sourceRefField": null };
			}
		};
		$scope.swapFieldOrder = function(commonIndex,parentIndex,requestForUp){

			var secondIndex = -1;
			var fullLengthOutPutField = $scope.outPutFields[parentIndex].fields.length;

			if(commonIndex == 1 && ($scope.show == "4" || $scope.show == "6") && requestForUp == 'up'){
				return false;
			} else if(requestForUp == 'up' && commonIndex == 0){
				return false;
			} else if(requestForUp == 'down' && fullLengthOutPutField == commonIndex + 1){
				return false;
			}
			var requestJson = $scope.outPutFields[parentIndex].fields[commonIndex];

			var newIndexPosition = -1;
			if(requestForUp == 'up') {
				secondIndex = commonIndex - 1;
				requestJson.pos = requestJson.pos - 1;
				newIndexPosition = requestJson.pos + 1;

			} else if(requestForUp == 'down') {
				secondIndex = commonIndex + 1;
				requestJson.pos = requestJson.pos + 1;
				newIndexPosition = requestJson.pos - 1;
			}
			$scope.outPutFields[parentIndex].fields[commonIndex] =$scope.outPutFields[parentIndex].fields[secondIndex]; 
			$scope.outPutFields[parentIndex].fields[secondIndex] = requestJson; 
			$scope.outPutFields[parentIndex].fields[commonIndex].pos = newIndexPosition;
		},

		$scope.onAddOperatorInMath = function(value){
			$scope.addIntoMathExpression(value);
		},

		$scope.deleteRange = function(index1,parentIndex,variableName){
			$scope.binningRanges.splice(index1, 1);
			_.groupBy($scope.binningRanges,function(num,index){
				num.seq = index;
			});
		},
		$scope.addRange = function(index1,parentIndex,previousVariable,groupByProcess){

			$scope.binningRanges.push({"from" : 0,"label": "" , "seq":  $scope.binningRanges.length , "to": "" });

		};
	}]).controller("metaDataController" , ["$modal","$route","$scope","DataSourceService", "logger"  , "$http", "uniqueArray", function($modal,$route, $scope, DataSourceService , logger ,$http, uniqueArray){

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.ingections = "";

		$scope.deleteMetaData = function (){
			$scope.metaDataP.metaDataParents = [];
			$scope.metaDataData.metaData = null;

			$http({
				url:"/updateAllFieldMappings?datasourceId="+$scope.datasource.id+"&tags="+$scope.datasource.tags,
				method: "POST",
				data : $scope.copyDS
			}).success(function(data, status, headers, config){
				$scope.$emit("metaData" , "");
			}).error(function(data, status, headers, config){
				logger.logError(data);
			});


			$http({
				method: 'POST',
				url : "/addMetaData",
				data : $scope.metaDataData
			}).success(function(data, status, headers, config){
				
				logger.logSuccess(data);
			}).error(function(data, status, headers, config){
				logger.logError(data);
			});
		},
		$scope.openModal = function() {
			var modalInstance;
			modalInstance = $modal.open({

				templateUrl: "/views/template/createNewMetaDataExtractionModal.html",
				controller: "createNewMetaDataModalCntr",
				resolve:{
					existingDataSource : function(){
						return $scope.datasource;
					},
					metaData : function(){
						return $scope.metaDataData;
					}
				}

			}),modalInstance.result.then(function(obj) {
				
				$scope.$emit("metaData" , "");
			}, function(result) {
				$log.info("Modal dismissed at: " + new Date);
			});
		};
	}]).controller("createNewMetaDataModalCntr" , ["metaData", "$upload","$route","$scope","$modalInstance" ,"existingDataSource" , "DataSourceService", "logger"  , "$http", "uniqueArray", function(metaData, $upload, $route, $scope, $modalInstance, existingDataSource, DataSourceService , logger ,$http, uniqueArray){

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.datasource = existingDataSource; 
		$scope.datasource.metaDataField = [];
		$scope.datasource.metaDataParents = [];
		$scope.datasource.metaDataStr = [];
		$scope.datasource.metaDataFields = [];

		$scope.copyDS = [];

		_.each($scope.datasource.fieldMappings , function(fields){
			var val = angular.copy(fields);
			val.alias = null;
			$scope.copyDS.push(val);
		});

		var id = null;

		if(metaData != null || metaData !== undefined)
			id = metaData.id; 
		else
			id = null;

		$scope.metaDataP = null;

		if($scope.metaDataP == undefined 
				|| $scope.datasource.metaDataP == null
				|| $scope.metaDataP == ""){
			$scope.metaDataP  = {
					"id" : id,
					"metaDataParents" : [],
					"metaDataFields" : [],
					"metaDataStructures" : []
			}; 
		}

		var arr = [];
		$scope.metaData = {
				"filePath" : null,
				"fileName" :"",
				"delimiter" : "",
				"fileContent" : ""
		};

		$scope.createMetaData = function(){

			if($scope.metaData.delimiter == null ||$scope.metaData.delimiter == undefined || $scope.metaData.delimiter == ""
				|| $scope.metaData.fileName == null ||$scope.metaData.fileName == undefined || $scope.metaData.fileName == ""
					|| $scope.myFile == null || $scope.myFile == undefined || $scope.myFile == ""){
				logger.logError("All fields are mandatory");
				return false;
			}else{
				for(var i = 1 ; i < arr.length ; i++){

					var array = arr[i].split($scope.metaData.delimiter);
					if(array[0] !== undefined && array[0] !== null && array[0] !== "")
						$scope.datasource.metaDataField.push(array[0]);
				}
				$scope.metaDataP.metaDataFields.push({"metaDataField":$scope.datasource.metaDataField});
				$scope.metaDataP.metaDataParents.push($scope.metaData.fileName);

				if($scope.myFile[0] !== undefined){
					$upload.upload({
						headers: { 'Content-Type': undefined },
						url : "/fileUploadApps",
						file :$scope.myFile[0]
					}).
					success(function(data, status, headers, config){

						$scope.metaData.filePath = data;

						$scope.metaDataP.metaDataStructures.push($scope.metaData);

						var metaData_ = {

								"metaDataParents" : $scope.metaDataP.metaDataParents,
								"metaDataFields" : $scope.metaDataP.metaDataFields,
								"metaDataStructures" : $scope.metaDataP.metaDataStructures
						};

						var metaDatatable = {
								"dataSourceId" : $scope.datasource.id,
								"id" : $scope.metaDataP.id,
								"metaData" : metaData_
						};


						$http({
							url:"/updateAllFieldMappings?datasourceId="+$scope.datasource.id+"&tags="+$scope.datasource.tags,
							method: "POST",
							data : $scope.copyDS
						}).success(function(data, status, headers, config){
						}).error(function(data, status, headers, config){
							logger.logError(data);
						});

						$http({
							method: 'POST',
							url : "/addMetaData",
							data : metaDatatable
						}).success(function(data, status, headers, config){
							logger.logSuccess(data);
							$modalInstance.close('');
						}).error(function(data, status, headers, config){
							logger.logError(data);
							$modalInstance.close('');
						});
						//$modalInstance.close();
					}).error(function(data, status, headers, config){

						logger.logError(data);

					});
				}
			}
		},
		$scope.cancel = function() {
			$modalInstance.close();
		},
		$scope.displayFileContents = function(content){
			$scope.metaData.fileContent = content;
			arr = content.split("\n");
		};
	}]).controller("GoogleAnalyticsController" , ["$upload","$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($upload, $modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
		$scope.dataSource = dataSource;
		$scope.userToggle = [true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
		$scope.map = {};

		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};

		$scope.selectedList = [];

		$scope.hideAndShow = function(index){
			$scope.userToggle[index] = !($scope.userToggle[index]);
		};

		if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
			$http({
				url: "/fetchExternalDataSource",
				method: 'Post',
				data : $scope.dataSource.externalDataId
			}).then(function(response) {
				$scope.externalData = response.data;
				$scope.externalData.amazonS3 = undefined;
				$scope.googleAnalytics = $scope.externalData.googleAnalytics; 
				$scope.connectorType = $scope.externalData.connectorType;
			});

		}else{
			$scope.externalData = {
					id : null,
					dataSourceId : dataSource.id,
					delimeter : "",
					storagePath : "",
					googleAnalytics: null,
					connectorType : "GoogleAnalytic"
			};

			$scope.googleAnalytics = {
					googleCollections :[
					                    {name : "USER", dimensions:["ga:userType","ga:sessionCount","ga:daysSinceLastSession","ga:userDefinedValue"], metrics:["ga:newUsers","ga:sessionsPerUser"], dimensionsChk:[false,false,false,false], metricsChk:[false,false]},
					                    {name : "SESSION", dimensions:["ga:sessionDurationBucket"], metrics:["ga:sessions","ga:bounces","ga:bounceRate","ga:sessionDuration","ga:avgSessionDuration","ga:hits","ga:organicSearches"], dimensionsChk:[false], metricsChk:[false,false,false,false,false,false,false]},
					                    {name : "TRAFFIC SOURCES", dimensions:["ga:referralPath","ga:fullReferrer","ga:campaign","ga:source","ga:medium","ga:sourceMedium","ga:keyword","ga:adContent","ga:socialNetwork","ga:hasSocialSourceReferral","ga:campaignCode"], metrics:["ga:organicSearches"], dimensionsChk:[false,false,false,false,false,false,false,false,false,false,false], metricsChk:[false]},
					                    {name : "ADWORDS", dimensions:["ga:adGroup","ga:adSlot","ga:adDistributionNetwork","ga:adMatchType","ga:adKeywordMatchType","ga:adMatchedQuery","ga:adPlacementDomain","ga:adPlacementUrl","ga:adFormat","ga:adTargetingType","ga:adTargetingOption","ga:adDisplayUrl","ga:adDestinationUrl","ga:adwordsCustomerID","ga:adwordsCampaignID","ga:adwordsAdGroupID","ga:adwordsCreativeID","ga:adwordsCriteriaID","ga:adQueryWordCount","ga:isTrueViewVideoAd"], metrics:["ga:impressions","ga:adClicks","ga:adCost","ga:CPM","ga:CPC","ga:CTR","ga:costPerTransaction","ga:costPerGoalConversion","ga:costPerConversion","ga:RPC","ga:ROAS"],dimensionsChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false], metricsChk:[false,false,false,false,false,false,false,false,false,false,false]},
					                    {name : "GOAL CONVERSIONS", dimensions:["ga:goalCompletionLocation","ga:goalPreviousStep1","ga:goalPreviousStep2","ga:goalPreviousStep3"], metrics:["ga:goalXXStarts","ga:goalStartsAll","ga:goalXXCompletions","ga:goalCompletionsAll","ga:goalXXValue","ga:goalValueAll","ga:goalValuePerSession","ga:goalXXConversionRate","ga:goalConversionRateAll","ga:goalXXAbandons","ga:goalAbandonsAll","ga:goalXXAbandonRate","ga:goalAbandonRateAll"], dimensionsChk:[false,false,false,false], metricsChk:[false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                    {name : "PLATFORM OR DEVICE", dimensions:["ga:browser","ga:browserVersion","ga:operatingSystem","ga:operatingSystemVersion","ga:mobileDeviceBranding","ga:mobileDeviceModel","ga:mobileInputSelector","ga:mobileDeviceInfo","ga:mobileDeviceMarketingName","ga:deviceCategory","ga:browserSize","ga:dataSource"], metrics:[], dimensionsChk:[false,false,false,false,false,false,false,false,false,false,false,false], metricsChk:[]},
					                    {name : "GEO NETWORK", dimensions:["ga:continent","ga:subContinent","ga:country","ga:region","ga:metro","ga:city","ga:latitude","ga:longitude","ga:networkDomain","ga:networkLocation","ga:cityId","ga:countryIsoCode","ga:regionId","ga:regionIsoCode","ga:subContinentCode"], metrics:[], dimensionsChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false], metricsChk:[]},
					                    {name : "SYSTEM", dimensions:["ga:flashVersion","ga:javaEnabled","ga:language","ga:screenColors","ga:sourcePropertyDisplayName","ga:sourcePropertyTrackingId","ga:screenResolution"], metrics:[], dimensionsChk:[false,false,false,false,false,false,false], metricsChk:[]},
					                    {name : "PAGE TRACKING", dimensions:["ga:hostname","ga:pagePath","ga:pagePathLevel1","ga:pagePathLevel2","ga:pagePathLevel3","ga:pagePathLevel4","ga:pageTitle","ga:landingPagePath","ga:secondPagePath","ga:exitPagePath","ga:previousPagePath","ga:pageDepth"], metrics:["ga:pageValue","ga:entrances","ga:entranceRate","ga:pageviews","ga:pageviewsPerSession","ga:uniquePageviews","ga:timeOnPage","ga:avgTimeOnPage","ga:exits","ga:exitRate"], dimensionsChk:[false,false,false,false,false,false,false,false,false,false,false,false], metricsChk:[false,false,false,false,false,false,false,false,false,false]},
					                    {name : "CONTENT GROUPING", dimensions:["ga:landingContentGroupXX","ga:previousContentGroupXX","ga:contentGroupXX"], metrics:["ga:contentGroupUniqueViewsXX"], dimensionsChk:[false,false,false], metricsChk:[false]},
					                    {name : "INTERNAL SEARCH", dimensions:["ga:searchUsed","ga:searchKeyword","ga:searchKeywordRefinement","ga:searchCategory","ga:searchStartPage","ga:searchDestinationPage","ga:searchAfterDestinationPage"], metrics:["ga:searchResultViews","ga:searchUniques","ga:avgSearchResultViews","ga:searchSessions","ga:percentSessionsWithSearch","ga:searchDepth","ga:avgSearchDepth","ga:searchRefinements","ga:percentSearchRefinements","ga:searchDuration","ga:avgSearchDuration","ga:searchExits","ga:searchExitRate","ga:searchGoalXXConversionRate","ga:searchGoalConversionRateAll","ga:goalValueAllPerSearch"], dimensionsChk:[false,false,false,false,false,false,false], metricsChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                    {name : "SITE SPEED", dimensions:[], metrics:["ga:pageLoadTime","ga:pageLoadSample","ga:avgPageLoadTime","ga:domainLookupTime","ga:avgDomainLookupTime","ga:pageDownloadTime","ga:avgPageDownloadTime","ga:redirectionTime","ga:avgRedirectionTime","ga:serverConnectionTime","ga:avgServerConnectionTime","ga:serverResponseTime","ga:avgServerResponseTime","ga:speedMetricsSample","ga:domInteractiveTime","ga:avgDomInteractiveTime","ga:domContentLoadedTime","ga:avgDomContentLoadedTime","ga:domLatencyMetricsSample"], dimensionsChk:[], metricsChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                    {name : "APP TRACKING", dimensions:["ga:appInstallerId","ga:appVersion","ga:appName","ga:appId","ga:screenName","ga:screenDepth","ga:landingScreenName","ga:exitScreenName"], metrics:["ga:screenviews","ga:uniqueScreenviews","ga:screenviewsPerSession","ga:timeOnScreen","ga:avgScreenviewDuration"], dimensionsChk:[false,false,false,false,false,false,false,false], metricsChk:[false,false,false,false,false]},
					                    {name : "EVENT TRACKING", dimensions:["ga:eventCategory","ga:eventAction","ga:eventLabel"], metrics:["ga:totalEvents","ga:uniqueEvents","ga:eventValue","ga:avgEventValue","ga:sessionsWithEvent","ga:eventsPerSessionWithEvent"], dimensionsChk:[false,false,false], metricsChk:[false,false,false,false,false,false]},
					                    {name : "ECOMMERCE", dimensions:["ga:transactionId","ga:affiliation","ga:sessionsToTransaction","ga:daysToTransaction","ga:productSku","ga:productName","ga:productCategory","ga:currencyCode","ga:checkoutOptions","ga:internalPromotionCreative","ga:internalPromotionId","ga:internalPromotionName","ga:internalPromotionPosition","ga:orderCouponCode","ga:productBrand","ga:productCategoryHierarchy","ga:productCategoryLevelXX","ga:productCouponCode","ga:productListName","ga:productListPosition","ga:productVariant","ga:shoppingStage"], metrics:["ga:transactions","ga:transactionsPerSession","ga:transactionRevenue","ga:revenuePerTransaction","ga:transactionRevenuePerSession","ga:transactionShipping","ga:transactionTax","ga:totalValue","ga:itemQuantity","ga:uniquePurchases","ga:revenuePerItem","ga:itemRevenue","ga:itemsPerPurchase","ga:localTransactionRevenue","ga:localTransactionShipping","ga:localTransactionTax","ga:localItemRevenue","ga:buyToDetailRate","ga:cartToDetailRate","ga:internalPromotionCTR","ga:internalPromotionClicks","ga:internalPromotionViews","ga:localProductRefundAmount","ga:localRefundAmount","ga:productAddsToCart","ga:productCheckouts","ga:productDetailViews","ga:productListCTR","ga:productListClicks","ga:productListViews","ga:productRefundAmount","ga:productRefunds","ga:productRemovesFromCart","ga:productRevenuePerPurchase","ga:quantityAddedToCart","ga:quantityCheckedOut","ga:quantityRefunded","ga:quantityRemovedFromCart","ga:refundAmount","ga:revenuePerUser","ga:totalRefunds","ga:transactionsPerUser"], dimensionsChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false], metricsChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                    {name : "SOCIAL INTERACTIONS", dimensions:["ga:socialInteractionNetwork","ga:socialInteractionAction","ga:socialInteractionNetworkAction","ga:socialInteractionTarget","ga:socialEngagementType"], metrics:["ga:socialInteractions","ga:uniqueSocialInteractions","ga:socialInteractionsPerSession"], dimensionsChk:[false,false,false,false,false], metricsChk:[false,false,false]},
					                    {name : "USER TIMINGS", dimensions:["ga:userTimingCategory","ga:userTimingLabel","ga:userTimingVariable"], metrics:["ga:userTimingValue","ga:userTimingSample","ga:avgUserTimingValue"], dimensionsChk:[false,false,false], metricsChk:[false,false,false]},
					                    {name : "EXCEPTIONS", dimensions:["ga:exceptionDescription"], metrics:["ga:exceptions","ga:exceptionsPerScreenview","ga:fatalExceptions","ga:fatalExceptionsPerScreenview"], dimensionsChk:[false], metricsChk:[false,false,false,false]},
					                    {name : "CONTENT EXPERIMENTS", dimensions:["ga:experimentId","ga:experimentVariant"], metrics:[], dimensionsChk:[false,false], metricsChk:[]},
					                    {name : "CUSTOM VARIABLES OR COLUMNS", dimensions:["ga:dimensionXX","ga:customVarNameXX","ga:customVarValueXX"], metrics:["ga:metricXX"], dimensionsChk:[false,false,false], metricsChk:[false]},
					                    {name : "TIME", dimensions:["ga:date"], metrics:[], dimensionsChk:[false], metricsChk:[]}
					                    ],
					                    p12FilePath : "",
					                    fromDt : "",
					                    toDt : "",
					                    accesskeyid : ""
					                   
					                    

			};
		}


		$scope.googleCollections = 
			$scope.saveFileAndGetPath = function(myFile){

			if(undefined == $scope.myFile){
				logger.logError("Please upload the p12 file");
				return false;
			}else if($scope.googleAnalytics.fromDt === undefined || $scope.googleAnalytics.fromDt === ""){
				logger.logError("Please select the from date");
				return false;
			}else if($scope.googleAnalytics.toDt === undefined || $scope.googleAnalytics.toDt === ""){
				logger.logError("Please select the to date");
				return false;
			}else if($scope.googleAnalytics.accesskeyid === undefined || $scope.googleAnalytics.accesskeyid === ""){
				logger.logError("service account email is mandatory");
				return false;
			}



			$upload.upload({
				headers: { 'Content-Type': undefined },
				url : "/fileUploadApps",
				file :myFile[0]
			}).success(function(data, status, headers, config){
				$scope.googleAnalytics.p12FilePath = data;
				$scope.saveExternalData();
			}).error(function(data){
				logger.logError(data);
			});
		};

		$scope.saveExternalData = function(){
			$scope.externalData.googleAnalytics = $scope.googleAnalytics;

			$http({
				url: "/saveExternalWithGoogle",
				method: 'Post',
				data: $scope.externalData
			}).success(function(data, status, headers, config) {
				$scope.dataSource = data;
				$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
				$modalInstance.dismiss("cancel");
			}).error(function(data, status, headers, config) { 
				$rootScope.$broadcast("fieldMappingWarning", data);
				$modalInstance.dismiss("cancel");
			});
		};


	}]).controller("AmazonS3Controller" , ["$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
		$scope.dataSource = dataSource;

		if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
			$http({
				url: "/fetchExternalDataSource",
				method: 'Post',
				data : $scope.dataSource.externalDataId
			}).then(function(response) {
				$scope.externalData = response.data;
				$scope.externalData.googleAnalytics = undefined;
				$scope.externalData.connectorType = $scope.externalData.connectorType;
				$scope.properties = {
						"accesskeyid" : $scope.externalData.amazonS3.accesskeyid,
						"secretkey" : $scope.externalData.amazonS3.secretkey
				};

				$scope.getBrokerList($scope.properties.accesskeyid, $scope.properties.secretkey);

			});

		}else{
			$scope.externalData = {
					id : null,
					dataSourceId : dataSource.id,
					amazonS3 : null,
					delimeter : "",
					storagePath : "",
					connectorType : ""
			};


			$scope.amazonS3 = {
					accesskeyid : "",
					secretkey : "",
					bucket : null,
					hierarchyInfo : [],
					selectedBucket : null,
					optionallyEnclosedInDoubleQuotes : false
			};
		}


		$scope.selectedFilesOrFolder = function(bucket) {
			$scope.amazonS3.selectedBucket = bucket;
		};

		$scope.amazonS3Assogn = function(){
			if($scope.externalData.amazonS3 != null)
				$scope.amazonS3 = $scope.externalData.amazonS3;

			if($scope.amazonS3.hierarchyInfo.length -1 >= 0){
				var obj= $scope.amazonS3.hierarchyInfo[$scope.amazonS3.hierarchyInfo.length -1];
				$scope.getBucketDetails(obj, obj.name_, true);
			}else{
				$scope.getBucketDetails($scope.amazonS3.accesskeyid, $scope.amazonS3.secretkey);
			}
		};

		$scope.saveAmazonDetails = function(){

			if($scope.amazonS3.accesskeyid === undefined || $scope.amazonS3.accesskeyid === ""){
				logger.logError("accesskeyid is mandatory");
				return false;
			}else if($scope.amazonS3.secretkey === undefined || $scope.amazonS3.secretkey === ""){
				logger.logError("secretkey is mandatory");
				return false;
			}else if($scope.amazonS3.bucket === null){
				logger.logError("Please select the bucket from bucket list");
				return false;
			}else if($scope.amazonS3.selectedBucket === null){
				logger.logError("Please select file from the table");
				return false;
			}else if(undefined != $scope.amazonS3.optionallyEnclosedInDoubleQuotes && null != $scope.amazonS3.optionallyEnclosedInDoubleQuotes && $scope.amazonS3.optionallyEnclosedInDoubleQuotes == true){
				if(null != $scope.externalData.delimeter && $scope.externalData.delimeter.length > 1 && "\\t" != $scope.externalData.delimeter){
					logger.logError("Optionally enclosed with double quotes option is not supported for this delimiter");
					return false;
				}
			}
			$scope.externalData.connectorType ="AmazonS3";
			$scope.externalData.amazonS3 = $scope.amazonS3;
			$http({
				url: "/saveExternalDataSource",
				method: 'Post',
				data : $scope.externalData 
			}).success(function(data, status, headers, config) {
				$scope.dataSource = data;
				$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
				$modalInstance.dismiss("cancel");
			}).error(function(data, status, headers, config) { 
				$rootScope.$broadcast("fieldMappingWarning", data);
				$modalInstance.dismiss("cancel");
			});

		}; 

		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};

		$scope.popRecords = function(index){
			if($scope.amazonS3.hierarchyInfo.length > (index + 1)){
				var length = $scope.amazonS3.hierarchyInfo.length - (index + 1);

				for(var i=0 ; i < length; i++)
					$scope.amazonS3.hierarchyInfo.pop();

				var obj = $scope.amazonS3.hierarchyInfo[$scope.amazonS3.hierarchyInfo.length -1];
				$scope.getBucketDetails(obj, obj.name_, true);
			}
		};

		$scope.getBrokerList = function(acceskeyid, secretkey) {

			$scope.properties = {
					"accesskeyid" : acceskeyid,
					"secretkey" : secretkey
			};

			$http({
				url: "/getBucketList",
				method: 'Post',
				data : $scope.properties
			}).then(function(response) {
				$scope.bucketList = response.data;
				if($scope.amazonS3 !== undefined)
					$scope.amazonS3.hierarchyInfo = [];
				$scope.amazonS3Assogn();
			});
		};

		$scope.getBucketDetails = function(bucketObject, location, popIndicator) {
			if(location === undefined || location === null || location === "")
			{
				location = null
			}else if(popIndicator == undefined && !popIndicator){
				$scope.amazonS3.hierarchyInfo.push(bucketObject);
			}

			if($scope.amazonS3!= null && $scope.amazonS3.bucket != null)
				$scope.properties.bucketName = $scope.amazonS3.bucket.name_;
				var reqDataObject = {location : location, properties : $scope.properties};
			$http({
				url: "/getBucket",
				method: 'Post',
				data : reqDataObject
			}).then(function(response) {
				$scope.bucketDetails = response.data;
			});
		};

	}]).controller("inputParamController" , ["$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", function($modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray){

		$scope.$on("hourEvents", function (event, args) {
			$scope.explanation = args;
		});
		
		$scope.getTodaysDate = function (){
			return Date.now();
		}
		
		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}
		});
		
		$scope.getTodaysDate = function (){
			return Date.now();
		}

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.ingections = "";

		$scope.openModal = function() {
			var modalInstance;
			modalInstance = $modal.open({
				size : "lg",
				templateUrl: "/views/template/createInputParametersForData.html",
				controller: "createInputParamForDataController",
				resolve:{
					existingDataSource : function(){
						return $scope.datasource;
					}
				}
			}),modalInstance.result.then(function(obj) {
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.datasource = data;
					$scope.$emit("shareDataSourceToAllController", $scope.datasource);
				});
			}, function(result) {
				$log.info("Modal dismissed at: " + new Date);
			});
		};
	}]).controller("createInputParamForDataController" , ["$upload","$route","$scope","$modalInstance" ,"existingDataSource" , "DataSourceService", "logger"  , "$http", "uniqueArray","CONSTANTS", function($upload, $route, $scope, $modalInstance, existingDataSource, DataSourceService , logger ,$http, uniqueArray, CONSTANTS){
		$scope.paramLength = length;
		$scope.count = 0;
		
		$scope.getTodaysDate = function (){
			return Date.now();
		}

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = args;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

		});

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		
		$scope.fixedDefaultInputParams = [CONSTANTS.GP_LAST_RUN_DATETIME, CONSTANTS.GP_CURRENT_RUN_DATETIME, CONSTANTS.GP_LAST_SUCCESSFULLY_RUN_DATETIME];

		$scope.checkIfDisableView = function(object){
			return _.contains($scope.fixedDefaultInputParams, object.paramName) && object.paramType==CONSTANTS.TIMESTAMP;
		};
		
		$scope.dataTypes = ["INTEGER" , "STRING" , "DATE" , "BOOLEAN" , "DOUBLE" , "LONG" , "TIMESTAMP"];
		$scope.datasource = existingDataSource;
		$scope.duplicateDataSource = angular.copy($scope.datasource);
		$scope.input = {
				"paramName" : "",
				"defaultVal" : "",
				"paramType" : undefined
		};
		if($scope.duplicateDataSource.dsLevelParams == null || $scope.duplicateDataSource.dsLevelParams == undefined)
		{
			$scope.duplicateDataSource.dsLevelParams = [];
		}
		$scope.addSelect = function (input){

			if($scope.paramLength !== $scope.duplicateDataSource.dsLevelParams.length)
			{
				$scope.count++;
			}		

			if(input.paramName === undefined || input.paramName === ""){
				logger.logError("Please enter the paramName");
				return false;
			}else if((input.paramName.toLowerCase().indexOf("gp_")) !== 0){
				logger.logError("Param name always start with gp_ OR GP_");
				return false;
			}else if(input.paramName == "" || input.defaultVal == "" || input.paramType == undefined){
				logger.logError("Please provide all the values");
				return false;
			}else if(input.paramName == undefined){
				logger.logError("Allow only underscore in parameter name");
				return false;
			}else if((input.paramType == "INTEGER" || input.paramType == "DOUBLE" || input.paramType == "LONG") && isNaN(input.defaultVal)){
				logger.logError("Please provide the integer value for integer type");
				return false;
			}else if(input.paramType == "BOOLEAN" && (input.defaultVal.toUpperCase() !== "TRUE" && input.defaultVal.toUpperCase() !== "FALSE")){
				logger.logError("Please provide the true and false value for boolean type");
				return false;
			}
			var flag = true;
			_.each($scope.duplicateDataSource.dsLevelParams , function (val){
				if(val.paramName == input.paramName){
					flag = false;
					return;
				}
			});

			if(flag == false){
				logger.logError("Parameters with this name already created");
				return false;
			}

			var value = angular.copy(input);
			$scope.duplicateDataSource.dsLevelParams.push(value);
			input.paramName = "";
			input.defaultVal = "";
			input.paramType = undefined;
		},
		$scope.deleteSelect = function(index){
			if($scope.paramLength !== $scope.duplicateDataSource.dsLevelParams.length)
			{
				$scope.count++;
			}
			if(index == 0){
				logger.logError("Please update all the value");
			}
			$scope.duplicateDataSource.dsLevelParams.splice(index , 1);
		},
		$scope.cancel = function() {
			$modalInstance.dismiss('cancel');
		},
		$scope.createInputParams = function(){
			if($scope.paramLength === $scope.duplicateDataSource.dsLevelParams.length && $scope.count === 0){
				logger.logError("Please fill all the value");
				return false;
			}else{
				$scope.duplicateDataSource.availableToUse=undefined;
				$http({
					url: "/saveDataSource",
					method: 'Post',
					data : $scope.duplicateDataSource
				}).then(function(response) {
					logger.logSuccess("Input parameters saved successfully");
					$modalInstance.close();
				}, 
				function(response) {
					logger.logError("Something went wrong");
				});
			};
		}
	}]).controller("captureValuesForParam" , ["$log","repo","$upload","$route","$scope","$modalInstance" ,"existingDataSource", "DataSourceService", "logger"  , "$http", "uniqueArray", function($log, repo, $upload, $route, $scope, $modalInstance, existingDataSource, DataSourceService , logger ,$http, uniqueArray){

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.dataSource = args;
			if($scope.dataSource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}
		});

		$scope.$on("myEvent1", function (event, args) {
			$scope.activeTab = args;
		});
		$scope.dataTypes = ["INTEGER" , "STRING" , "DATE" , "BOOLEAN" , "DOUBLE" , "LONG" , "BOOLEAN"];
		$scope.dataSource = angular.copy(existingDataSource);
		$scope.repo = repo;
		$scope.disabledValues = [];
		$scope.indexList = [];

		var valChange = false;

		_.each($scope.dataSource.dsLevelParams , function(values){
			$scope.disabledValues.push(true);
		});

		$scope.enableTextValue = function (index){
			$scope.disabledValues[index] = false;

			if($scope.indexList.indexOf(index) === -1)
				$scope.indexList.push(index);

			valChange = true;
		},
		$scope.changeDefaultVal = function(){
			if(valChange == true){
				var flag = null;
				_.each($scope.indexList , function(val){
					if($scope.dataSource.dsLevelParams[val].defaultVal == ""){
						flag = "Please provide the values";
						return false;
					}else if($scope.dataSource.dsLevelParams[val].paramType == "INTEGER" || 
							$scope.dataSource.dsLevelParams[val].paramType == "DOUBLE" || 
							$scope.dataSource.dsLevelParams[val].paramType == "LONG" && 
							isNaN($scope.dataSource.dsLevelParams[val].defaultVal)){
						flag = "Please provide the numeric value for numeric type";
						return false;
					}else if($scope.dataSource.dsLevelParams[val].paramType == "BOOLEAN" && 
							($scope.dataSource.dsLevelParams[val].defaultVal.toUpperCase() !== "TRUE" && 
									$scope.dataSource.dsLevelParams[val].defaultVal.toUpperCase() !== "FALSE")){
						flag = "Please provide the true and false value for boolean type";
						return false;
					}
				});
				if(flag !== null){
					logger.logError(flag);
					return false;
				}
				$modalInstance.close($scope.dataSource.dsLevelParams);
			}else{
				$modalInstance.close($scope.dataSource.dsLevelParams);
			}
		},
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};
	}]).controller("compareFieldMappingSchemaCtrl",["$scope", "$modal" , "$log" ,"$http", "logger","dataSource", "uuId", "DataSourceService", "$modalInstance", "$q",
	                                                function($scope, $modal , $log, $http, logger, dataSource, uuId, DataSourceService, $modalInstance, $q) {
		/* In case of FTP ,RDBMS we store config object in field1 of communication_deatils table. other than that we store fileDataIngesterDetails*/
		$scope.newDataSource = dataSource;
		$scope.datasource;

		$http({
			url:"/fetchCommunicationsDetailsByuuId",
			method: "POST",
			data : uuId
		}).success(function(data, status, headers, config){
			$scope.communicationsDetails = data;

			if(undefined != $scope.communicationsDetails && null != $scope.communicationsDetails  
					&& undefined != $scope.communicationsDetails.field2 && null != $scope.communicationsDetails.field2 
					&& undefined != $scope.communicationsDetails.field1 && null != $scope.communicationsDetails.field1 ){

				if(null != angular.isObject($scope.communicationsDetails.field1) && null != angular.isObject($scope.communicationsDetails.field1) && null != angular.isObject($scope.communicationsDetails.field2)){
					$scope.newFieldMappings = $scope.communicationsDetails.field2;
					$scope.newFieldMappings = _.sortBy($scope.newFieldMappings,'position');

					$scope.newConnectionDetails = $scope.communicationsDetails.field1;
				}else{
					logger.logError("Undefined communication details data");
					return false;
				}

			}else{
				logger.logError("Undefined communication details data");
				return false;
			}

			DataSourceService.getDataSource("/getDataSourceById" ,$scope.newDataSource.id).then(function(data){
				$scope.datasource = data;
				$scope.existingFieldMappings = $scope.datasource.fieldMappings;
				$scope.existingFieldMappings = _.sortBy($scope.existingFieldMappings,'position');
			});


		}).error(function(data, status, headers, config){
			logger.logError(data);
		})

		$scope.replaceSpecialCharacters = function(field) {
			if(field != null){
				field = field.replace("\t", "\\t");
				field = field.replace("\n", "\\n");
				field = field.replace("\r", "\\r");
			}
			return field;
		}

		$scope.saveFieldMappingChanges = function(decisionOnFieldmappingChanges) {

			if("IgnoreFieldMappings" == decisionOnFieldmappingChanges 
					|| "OverwriteFieldMappings" == decisionOnFieldmappingChanges){
				if($scope.newDataSource.fileDataIngesterDetails == null && ("RDBMS_DATA_SOURCE" == $scope.newDataSource.dataSourceType 
						|| "FTP_DATA" == $scope.newDataSource.dataSourceType)){

					if(null != $scope.newConnectionDetails.dataSourceType && $scope.newConnectionDetails.dataSourceType != undefined && $scope.newConnectionDetails.dataSourceType != "RDBMS_DATA_SOURCE"
						&& $scope.newConnectionDetails.dataSourceType != "FTP_DATA"){
						$scope.datasource.fileDataIngesterDetails = $scope.newConnectionDetails;
					} else {
						$scope.datasource.config = $scope.newConnectionDetails;
					}
				}else if("RDBMS_DATA_SOURCE" == $scope.newDataSource.fileDataIngesterDetails.dataSourceType 
						|| "FTP_DATA" == $scope.newDataSource.fileDataIngesterDetails.dataSourceType
						|| "NOSQL" == $scope.newDataSource.fileDataIngesterDetails.dataSourceType){
					$scope.datasource.fileDataIngesterDetails.dataSourceType = $scope.newDataSource.fileDataIngesterDetails.dataSourceType;
					$scope.datasource.fileDataIngesterDetails.delimiter = $scope.replaceSpecialCharacters($scope.newDataSource.fileDataIngesterDetails.delimiter);
					
					if($scope.newConnectionDetails.dataSourceType != undefined && $scope.newConnectionDetails.dataSourceType != "RDBMS_DATA_SOURCE"
						&& $scope.newConnectionDetails.dataSourceType != "FTP_DATA"){
						$scope.datasource.fileDataIngesterDetails = $scope.newConnectionDetails;
					} else {
						$scope.datasource.config = $scope.newConnectionDetails;
					}
				}else{
					$scope.datasource.fileDataIngesterDetails.dataSourceType = $scope.newDataSource.fileDataIngesterDetails.dataSourceType;
					if(null != $scope.newDataSource.fileDataIngesterDetails && undefined != $scope.newDataSource.fileDataIngesterDetails){
						$scope.datasource.fileDataIngesterDetails.delimiter = $scope.replaceSpecialCharacters($scope.newDataSource.fileDataIngesterDetails.delimiter);
					}

					$scope.datasource.fileDataIngesterDetails = $scope.newConnectionDetails;
				}
				$scope.isChangeFields = false;	

				if("OverwriteFieldMappings" == decisionOnFieldmappingChanges){
					$scope.datasource.fieldMappings = $scope.newFieldMappings;
					$scope.isChangeFields = true;
					var reqDataObject = {'dataSourceId' : $scope.datasource.id, 'fieldMapping': $scope.newFieldMappings};
					$http({
						method : "post",
						url : "/getDataSourceImpactedAppsList",
						data : reqDataObject
					}).success(function(data){
						if(undefined != data && data.length >0){
							var modalInstance;
							modalInstance = $modal.open({
								backdrop: 'static',
								templateUrl: "/views/template/ImpactionOnApp.html",
								controller: "impactionOnAppCtrl",
								resolve: {

									impactedAppsList : function(){
										return data;
									},
									componentType : function(){
										return "DATASOURCE";
									}
								}
							}),modalInstance.result.then(function(returnValue) {
								if(false == returnValue){
									$modalInstance.close('cancel');
									return false;
								}else if(true == returnValue){
									saveDS();
								}
							}, function() {
								$log.info("Modal dismissed at: " + new Date);
							});
						}else{
							saveDS();
						}
					}).error(function(data){
						logger.logError(data);
						return false;
					});


				}else{
					saveDS();
				}

				function saveDS() {
					$scope.datasource.availableToUse=undefined;
					$http({
						url: "/saveDataSource",
						method: 'Post',
						data : $scope.datasource
					}).success(function(data){
						$scope.saveForVisualize();

						logger.logSuccess("Data Source FieldMapping reflected successfully.");
						$modalInstance.close('');
					}).error(function(data){
						logger.logError(data);
						return false
					});
				}

			}else{
				$modalInstance.close('cancel');
			}
		};


		$scope.saveForVisualize = function(){
			var deferred = $q.defer();
			$scope.datasource.availableToUse = undefined;

			$scope.indexDetails= {indexName : $scope.datasource.dataSourceName };
			$http({
				url: "/saveForVisualize?saveForVisualize="+$scope.datasource.saveForVisualize+"&dataSourceId="+$scope.datasource.id+"&isFieldsChange="+$scope.isChangeFields,
				method: 'Post',
				data: $scope.indexDetails 
			}).success(function(data){
				DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
					$scope.$emit("shareDataSourceToAllController", data);
				});
				deferred.resolve(data);
			}).error(function(data){
				logger.logError(data);
				deferred.reject(data);
				return false;
			});
			return deferred.promise;
		};

	}]).controller("TabsCtrl", ["$route", "$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger","$cookieStore", function($route, $scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,$cookieStore){
		$scope.$emit("myEvent1", "Overview");
		if($routeParams.param10 != undefined && $routeParams.param10 > 0){
			if(undefined != $routeParams.flowType && $routeParams.flowType && $routeParams.flowType == 'STREAM'){
				$scope.tabs = [{
					title: 'Overview',
					active: false
				}, {
					title: 'Data',
				active : false
				}];
			} else {
				$scope.tabs = [{
					title: 'Overview',
					active: false
				}, {
					title: 'Data',
					active : false
				}, {
					title: 'Feedback',
					active : false
				}, {
					title: "Application settings",
					active : false
				}];
			}
		} else {
			$scope.tabs = [{
				title: 'Overview',
				active: false
			}, {
				title: 'Meta data',
				active : false
			}, {
				title: 'Sample records',
				active : false
			}, {
				title: 'Relationship',
				active : false
			}];
		}
		
		/*{
			title: 'Profiling',
			active : false
		}*/
		
				
		setTimeout(function(){
			if($cookieStore.get('userId')!=$cookieStore.get('dsownerid')){
				for(var i=0; i < $scope.tabs.length; i++) {
					   if($scope.tabs[i].title == "Dashboard")
					   {
						   $scope.tabs.splice(i,1);
						  
					   }
					}
				}
			}, 350);
		
		if($routeParams.param6 === 1)
			$scope.currentTab = '/views/DataSource/dataSource.jsp';

		$scope.onClickTab = function (tab) {
			$scope.$emit("myEvent1", tab.title);
		};

		$scope.isActiveTab = function(tabUrl) {
			return tabUrl == $scope.currentTab;
		};

		$scope.$on("nextPrevous" , function(event , args){
			var tabSelect= null;
			if(args === "next"){
				for(var i = 0 ; i < $scope.tabs.length; i++){
					if($scope.tabs[i].active == true){
						$scope.tabs[i].active = false;
						if(i+1 == $scope.tabs.length){
							$scope.tabs[0].active = true;
							tabSelect = $scope.tabs[0]; 
						}
						else{
							$scope.tabs[i+1].active = true;
							tabSelect = $scope.tabs[i+1];
						}
						break;
					}
				}
			}else{
				for(var i = 0 ; i < $scope.tabs.length; i++){
					if($scope.tabs[i].active == true){
						$scope.tabs[i].active = false;
						if(i-1 == -1){
							$scope.tabs[$scope.tabs.length - 1].active = true;
							tabSelect = $scope.tabs[$scope.tabs.length - 1];
						}
						else{
							$scope.tabs[i - 1].active = true;
							tabSelect = $scope.tabs[i - 1];
						}
						break;
					}
				}
			} 
			$scope.onClickTab(tabSelect);
		});
		
		$scope.disableOverview = false;
		if($location.path().includes("/discover")) {
			$scope.tabs = _.reject($scope.tabs, function(val){return val.title == "Configurations"})
			$scope.disableOverview = true;
		} else {
			$scope.disableOverview = false;
		};
		
		
	}]).controller("fieldAddTabsCtrl",["$scope","$http",function($scope,$http){


		$scope.fieldAddTabs = [{
			title: 'Expressions',
			active: true
		}, {
			title: 'Binning',
			active: false
		}, {
			title: 'Fixed',
			active: false
		}, {
			title: 'Text mining',
			disabled: true
		},{
			title: 'Custom',
			disabled: true
		}];
		$scope.onTabClick = function (tab) {
			$scope.$emit("eventForFieldAdd", tab.title);
		};
		$scope.$on("editOutPutAddFields" , function(event , args){
			for(var counter = 0; counter < $scope.fieldAddTabs.length; counter++){
				if($scope.fieldAddTabs[counter].active === true){
					$scope.fieldAddTabs[counter].active = false;
					break;
				}
			}
			$scope.fieldAddTabs[args].active = true;
			$scope.onTabClick($scope.fieldAddTabs[args]);
		});

		$scope.onTabClick($scope.fieldAddTabs[0]);

		$scope.FLOWCHART_CONSTANTS = null;

		function getMaximumNumberValue(str){
			var max = 0;
			for (i = 0; i < str.length; i++) 
				if(parseInt(str[i])>max){
					max = parseInt(str[i]);
				}
			return max;
		}

		$http({
			method : "post",
			url : "/getDatabaseFunctionsJson",
			data : 3
		}).success(function(data){
			$scope.databaseFunctionsJson = data;
			$http({
				method : "post",
				url : "/getDatabaseFunctionsParametersJson",
				data : 3
			}).success(function(data){
				$scope.databaseFunctionsParametersJson = data;

				var maxDatabaseFunctionParametersIndex = getMaximumNumberValue(Object.keys($scope.databaseFunctionsParametersJson)) + 1;
				$scope.databaseFunctionsJson.fxDatabaseFunctionsJson[maxDatabaseFunctionParametersIndex] = {
						"id": maxDatabaseFunctionParametersIndex,
						"name":"case when",
						"parameters":"CASE WHEN <CONDITION> THEN <VALUE> [WHEN <CONDITION> THEN <VALUE>]* [ELSE <VALUE>] END",
						"returnType":"{(Column)}",
						"databaseFunctionType":"FX",
						"databaseFunctionTypeId":"6",
						"databaseFunctionUDFId":"0",
						"databaseName":"SPARK_SQL",
						"databaseNameId":"3"
				}

				$scope.databaseFunctionsParametersJson[maxDatabaseFunctionParametersIndex]= "CASE WHEN () THEN () [WHEN () THEN ()]* [ELSE ()] END";
			}).error(function(data){
				logger.logError(data);
			});
		}).error(function(data){
			logger.logError(data);
		});

	}]).controller("SalesforceController" , ["$upload","$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($upload, $modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
		
		$scope.dataSource = dataSource;
		$scope.userToggle = [true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
		$scope.map = {};

		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};

		$scope.selectedList = [];
		
		$scope.hideAndShow = function(index){
			for(var j=0; j< $scope.userToggle.length ; j++){
				if(index == j){
					$scope.userToggle[j] = !($scope.userToggle[j]);
				}else{
					$scope.userToggle[j] =false;
				}
			}
		};

		$scope.$on("myEventDataSource", function (event, args) {
			$scope.dataSource = angular.copy(args);

			if($scope.dataSource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

			$scope.dataSource.fieldMappings =  [];
			$scope.fieldMappings = args.fieldMappings;
			$scope.validators = [];

			if(null != $scope.dataSource.advancedValidations && $scope.dataSource.advancedValidations.length > 0){
				_.each($scope.dataSource.advancedValidations, function(advancedvalidation, index) {
					var fieldStores = [];
					_.each($scope.fieldMappings, function(mappingObject,index){
						_.each(advancedvalidation.fieldsStore , function(storeObject,index){
							if(mappingObject.id ===  storeObject){
								fieldStores.push(mappingObject);

							}
						});
					});
					$scope.validators.push(
							{   title:"Validation "+index, 
								selectFields : fieldStores, 
								validationsApplied : advancedvalidation.dataValidations, 
								advancedvalidationId : advancedvalidation.id,
								dropdown : [$scope.fieldMappings]
							});
					changingSelectTest($scope.validators[index].dropdown , fieldStores , index);
				});

			}
		});
		
		if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
			$http({
				url: "/fetchExternalDataSource",
				method: 'Post',
				data : $scope.dataSource.externalDataId
			}).then(function(response) {
				$scope.externalData = response.data;
				$scope.externalData.amazonS3 = undefined;
				$scope.googleAnalytics = undefined; 
				$scope.salesforce= $scope.externalData.salesforce;
				$scope.connectorType = "SALESFORCE";
			});
			
		}else{
			$scope.externalData = {
					id : null,
					dataSourceId : dataSource.id,
					delimeter : "",
					storagePath : "",
					googleAnalytics: null,
					salesforce: null,
					connectorType:"SALESFORCE"
					
			};
			
			$scope.externalData.connectorType = "SALESFORCE";
			
			$scope.salesforce = {
					salesforceCollection :[            
					                       {name : "ACCOUNT", field:["Name","AccountNumber","Phone","Type","YearStarted","AccountSource","AnnualRevenue","BillingCountry","BillingState","BillingCity","BillingStreet","BillingPostalCode","ShippingCountry","ShippingState","ShippingCity","ShippingStreet","ShippingPostalCode","Industry","CreatedDate","Rating","Website","IsDeleted"], fieldChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                       {name : "OPPORTUNITY", field:["Name","AccountId","type","Amount","CreatedDate","CloseDate","CurrentGenerators__c","Description","ExpectedRevenue","Fiscal","FiscalQuarter","FiscalYear","ForecastCategory","ForecastCategoryName","HasOpenActivity","HasOpportunityLineItem","HasOverdueTask","LastActivityDate","LeadSource","StageName"],fieldChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                       {name : "CAMPAIGN", field:["Name","id","IsActive","ActualCost","AmountAllOpportunities","AmountWonOpportunities","BudgetedCost","CreatedDate","Description","EndDate","ExpectedRevenue","LastActivityDate","NumberOfContacts","NumberOfConvertedLeads","NumberOfLeads","NumberOfOpportunities","NumberSent","NumberOfResponses","Status","Type"], fieldChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                       {name : "CONTACT", field:["FirstName","LastName","Title","Department","AssistantName","AssistantPhone","Birthdate","CleanStatus","Description","Email","Fax","HomePhone","MobilePhone","IsDeleted","LastActivityDate","LeadSource","MailingCountry","MailingState","MailingCity","MailingStreet","MailingPostalCode","Salutation"], fieldChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                       {name : "TASK", field:["Subject","Status","Priority","ActivityDate","CallDisposition","CallDurationInSeconds","CallObject","CallType","CreatedDate","Description","IsClosed","IsDeleted","IsHighPriority","IsReminderSet","TaskSubtype","RecurrenceType","RecurrenceDayOfMonth","RecurrenceInstance","RecurrenceMonthOfYear","RecurrenceEndDateOnly"], fieldChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]},
					                       {name : "LEAD", field:["FirstName","LastName","Title","Email","Fax","Phone","MobilePhone","Company","CompanyDunsNumber","Industry","Website","AnnualRevenue","Country","State","City","Street","PostalCode","CreatedDate","Description","Status","IsDeleted"], fieldChk:[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]}
					                       
					                      ],
						                    clientId : $scope.dataSource.config.sfClientId,
						                    secretId : $scope.dataSource.config.sfSecretId,
						                    userName : $scope.dataSource.config.sfUserName,
						                    passwordSecurityToken : $scope.dataSource.config.sfPasswordSecurityToken
						                  

			};
		}

		
		function refreshedDataSource (isSaveForVisualization){
			DataSourceService.getDataSource("/getDataSource" ,$scope.dataSource.dataSourceName).then(function(data){
				$scope.dataSource  = data;
				backUp = angular.copy($scope.dataSource.ingection);
				if($scope.dataSource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
				$scope.dataSource.availableToUse = "true";

				$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


				if($scope.dataSource !== null && 
						$scope.dataSource.dataSourceType === "RDBMS_DATA_SOURCE"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "RDBMS_DATA_SOURCE"
					};
				} else if($scope.datasource !== null && 
						$scope.datasource.dataSourceType === "NOSQL"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "NOSQL"
					};
					
					$scope.schema = data.config.noSqlSchema;
				}

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
					$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
				};

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
					$scope.fileDataIngesterDetails.delimiter = "\\t";
				}

				$rootScope.$broadcast("shareDataSourceToAllController", $scope.dataSource);
				
				//If File saved then reload list of datasources
				$rootScope.isDataSourceFileSaved = true;
				
				if(isSaveForVisualization){
					$scope.saveForVisualize();
				}
			});
		};
		
		$scope.saveExternalData = function(){
			
		    $scope.externalData.salesforce = $scope.salesforce;
		    $scope.externalData.connectorType= "SALESFORCE";
		    $scope.externalData.salesforce.connectionName= $scope.dataSource.config.conncetionName;
		    $scope.selectedAll=false;
			$http({
				url: "/saveExternalWithSalesforce",
				method: 'Post',
				data: $scope.externalData
				
			}).success(function(data, status, headers, config) {
				logger.logSuccess("Connection details saved successfully");
				$scope.dataSource = data;
				refreshedDataSource();
				$modalInstance.dismiss("cancel");
			}).error(function(data, status, headers, config) { 
				$rootScope.$broadcast("fieldMappingWarning", data);
				$modalInstance.dismiss("cancel");
			});
		};
		
		
		
      	$scope.saveSalesforceData = function(){

			if($scope.salesforce.userName === undefined || $scope.salesforce.userName === ""){
				logger.logError("Please enter salesforce.userName");
				return false;
			}else if($scope.salesforce.passwordSecurityToken === undefined || $scope.salesforce.passwordSecurityToken === ""){
				logger.logError("Please enter salesforce.password");
				return false;
			}else if($scope.salesforce.clientId === undefined || $scope.salesforce.clientId === ""){
				logger.logError("Please enter salesforce.accesskeyid");
				return false;
			}else if($scope.salesforce.secretId === undefined || $scope.salesforce.secretId === ""){
				logger.logError("Please enter salesforce.secretkey");
				return false;
			}



          $scope.saveExternalData();
		};

		$scope.testSFConnection = function(){
			
			if($scope.salesforce.userName === undefined || $scope.salesforce.userName === ""){
				logger.logError("Please enter salesforce.userName");
				return false;
			}else if($scope.salesforce.passwordSecurityToken === undefined || $scope.salesforce.passwordSecurityToken === ""){
				logger.logError("Please enter salesforce.password");
				return false;
			}else if($scope.salesforce.clientId === undefined || $scope.salesforce.clientId === ""){
				logger.logError("Please enter salesforce.accesskeyid");
				return false;
			}else if($scope.salesforce.secretId === undefined || $scope.salesforce.secretId === ""){
				logger.logError("Please enter salesforce.secretkey");
				return false;
			}
			
			$scope.externalData.salesforce = $scope.salesforce;
			
			$http({
				url: "/testSalesforceConnection",
				method: 'Post',
				data: $scope.externalData
				
			}).success(function(data, status, headers, config) {
				logger.logSuccess("Connection done successfully");
				$scope.dataSource = data;
				$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
				
			}).error(function(data, status, headers, config) { 
                logger.logError(data);

				
			});
		}
		
		$scope.selectedAll=false;

		$scope.checkAll = function(value){

			$scope.temp=[];
			$scope.toggleNumber;
			var i = 0;

			for(var j=0; j< $scope.userToggle.length ; j++){
				if($scope.userToggle[j] == true){
					$scope.toggleNumber= j;
				}

			}
			if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
			if(value){

				angular.forEach($scope.externalData.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk,function(Obj){
					
					$scope.temp.push(false);
				})	
				$scope.externalData.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk =  $scope.temp;
			
			}else{
				angular.forEach($scope.externalData.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk,function(Obj){
					
					$scope.temp.push(true); 
				})
				$scope.externalData.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk =  $scope.temp;
				
			}
			}else{
				
				//for - $scope.dataSource and $scope.dataSource.externalDataId is null  
				//when we create new records
				if(value){
				angular.forEach($scope.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk,function(Obj){
					
					$scope.temp.push(false);
				})	
				$scope.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk =  $scope.temp;
				
				}else{
					angular.forEach($scope.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk,function(Obj){
						
						$scope.temp.push(true); 
					})
					$scope.salesforce.salesforceCollection[$scope.toggleNumber].fieldChk =  $scope.temp;
					
				}
			}

		};
	}]).controller("DropboxController" , ["$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
		$scope.dataSource = dataSource;
		$scope.disabled = true;
		$scope.$on("myEventDataSource", function (event, args) {
			$scope.dataSource = angular.copy(args);

			if($scope.dataSource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}

			$scope.dataSource.fieldMappings =  [];
			$scope.fieldMappings = args.fieldMappings;
			$scope.validators = [];

			if(null != $scope.dataSource.advancedValidations && $scope.dataSource.advancedValidations.length > 0){
				_.each($scope.dataSource.advancedValidations, function(advancedvalidation, index) {
					var fieldStores = [];
					_.each($scope.fieldMappings, function(mappingObject,index){
						_.each(advancedvalidation.fieldsStore , function(storeObject,index){
							if(mappingObject.id ===  storeObject){
								fieldStores.push(mappingObject);

							}
						});
					});
					$scope.validators.push(
							{   title:"Validation "+index, 
								selectFields : fieldStores, 
								validationsApplied : advancedvalidation.dataValidations, 
								advancedvalidationId : advancedvalidation.id,
								dropdown : [$scope.fieldMappings]
							});
					changingSelectTest($scope.validators[index].dropdown , fieldStores , index);
				});

			}
		});
		
		
		if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
			$http({
				url: "/fetchExternalDataSource",
				method: 'Post',
				data : $scope.dataSource.externalDataId
			}).then(function(response) {
				$scope.externalData = response.data;
				$scope.externalData.googleAnalytics = undefined;
				$scope.externalData.amazonS3 = undefined;
				$scope.externalData.connectorType = $scope.externalData.connectorType;
				$scope.properties = {
						"access_token" : $scope.externalData.dropbox.accessToken
				};

				$scope.getFolderList($scope.properties.access_token);

			});

		}else{
			$scope.externalData = {
					id : null,
					dataSourceId : dataSource.id,
					amazonS3 : null,
					delimeter : "",
					storagePath : "",
					connectorType : "",
					salesforce : null
			};

           $scope.dropbox = {
					
					appName : "",
					accessToken : $scope.dataSource.config.dropboxAccessToken,
					storagePath : "",
					filePath : "",
					hierarchyInfo : [],
					folders : null
					
			};
		}

        $scope.testDropboxCon = function(){
			
		    if($scope.dropbox.accessToken === undefined || $scope.dropbox.accessToken === ""){
				logger.logError("Please enter dropbox.accessToken");
				return false;
			}
				$scope.properties = {
						"access_token" : $scope.dropbox.accessToken
				};
				
			$scope.externalData.dropbox = $scope.dropbox;
			
			$http({
				url: "/testDropboxConnection",
				method: 'Post',
				data: $scope.properties
				
			}).success(function(data, status, headers, config) {
				logger.logSuccess("Connection done successfully");
				$scope.dataSource = data;
				$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
				
			}).error(function(data, status, headers, config) { 
				logger.logError("Error while testing connection");
			});
		  
        }
        
		$scope.getFolderList = function(accessToken) {
			
			if(accessToken === undefined || accessToken === ""){
				logger.logError("Please enter dropbox.accessToken");
				return false;
			}
			
			if(accessToken == ""){
				accessToken = $scope.dropbox.accessToken;
			}
			$scope.properties = {
					"access_token" : accessToken
			};

			$http({
				url: "/getFolderList",
				method: 'Post',
				data : $scope.properties
			}).then(function(response) {
				$scope.foldersList = response.data;
				if($scope.dropbox !== undefined)
					$scope.dropbox.hierarchyInfo = [];
				$scope.dropboxAssogn();
			});
		};
		
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};
		
		$scope.selectedFilesOrFolder = function(folders) {
			$scope.dropbox.selectedFolder = folders;
		};

		$scope.getFolderDetails = function(bucketObject, location, popIndicator) {
			
			if(location === undefined || location === null || location === "")
			{
				location = null
			}else if(popIndicator == undefined && !popIndicator){
				$scope.dropbox.hierarchyInfo.push(bucketObject);
			}
			
			$scope.properties = {
					"access_token" : $scope.dropbox.accessToken,
					"folderName" :  bucketObject.path
			};
			var reqDataObject = {location : location, properties : $scope.properties};
			$http({
				url: "/getInnerFolderList",
				method: 'Post',
				data : reqDataObject
			}).then(function(response) {
				$scope.foldersList = response.data;
			});
		};
		
		$scope.dropboxAssogn = function(){
			if($scope.externalData.dropbox != null)
				$scope.dropbox = $scope.externalData.dropbox;

			if($scope.dropbox.hierarchyInfo.length -1 >= 0){
				var obj= $scope.dropbox.hierarchyInfo[$scope.dropbox.hierarchyInfo.length -1];
				$scope.getFolderDetails(obj, obj.name_, true);
			}else{
				$scope.getFolderDetails($scope.dropbox.accessToken);
			}
		};

		$scope.popRecords = function(index){
			if($scope.dropbox.hierarchyInfo.length > (index + 1)){
				var length = $scope.dropbox.hierarchyInfo.length - (index + 1);

				for(var i=0 ; i < length; i++)
					$scope.dropbox.hierarchyInfo.pop();

				var obj = $scope.dropbox.hierarchyInfo[$scope.dropbox.hierarchyInfo.length -1];
				$scope.getFolderDetails(obj, obj.name_, true);
			}
		};
		
		
		function refreshedDataSource (isSaveForVisualization){
			DataSourceService.getDataSource("/getDataSource" ,$scope.dataSource.dataSourceName).then(function(data){
				$scope.dataSource  = data;
				backUp = angular.copy($scope.dataSource.ingection);
				if($scope.dataSource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
				$scope.dataSource.availableToUse = "true";

				$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


				if($scope.dataSource !== null && 
						$scope.dataSource.dataSourceType === "RDBMS_DATA_SOURCE"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "RDBMS_DATA_SOURCE"
					};
				}

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
					$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
				};

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
					$scope.fileDataIngesterDetails.delimiter = "\\t";
				}

				$rootScope.$broadcast("shareDataSourceToAllController", $scope.dataSource);
				
				//If File saved then reload list of datasources
				$rootScope.isDataSourceFileSaved = true;
				
				if(isSaveForVisualization){
					$scope.saveForVisualize();
				}
			});
		};
		
			$scope.saveDropboxDetails = function(){

			if($scope.dropbox.accessToken === undefined || $scope.dropbox.accessToken === ""){
					logger.logError("Please enter dropbox.accessToken");
					return false;
			}else if($scope.dropbox.selectedFolder === null){
				logger.logError("Please select the File from File list");
				return false;
			}else if($scope.externalData.delimeter === undefined  || $scope.externalData.delimeter === "" ||  $scope.externalData.delimeter === null){
				logger.logError("Please enter delimeter");
				return false;
			}else if(undefined != $scope.dropbox.optionallyEnclosedInDoubleQuotes && null != $scope.dropbox.optionallyEnclosedInDoubleQuotes && $scope.dropbox.optionallyEnclosedInDoubleQuotes == true){
				if(null != $scope.externalData.delimeter && $scope.externalData.delimeter.length > 1 && "\\t" != $scope.externalData.delimeter){
					logger.logError("Optionally enclosed with double quotes option is not supported for this delimiter");
					return false;
				}
			}
			
			$scope.dropbox.connectionName = $scope.dataSource.config.conncetionName;
			$scope.externalData.connectorType ="DROPBOX";
			$scope.externalData.dropbox = $scope.dropbox;
			$http({
				url: "/saveExternalDataDropbox",
				method: 'Post',
				data : $scope.externalData 
			}).success(function(data, status, headers, config) {
				$scope.dataSource = data;
				logger.logSuccess("Connection details saved successfully");
				refreshedDataSource();
				$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
				$modalInstance.dismiss("cancel");
			}).error(function(data, status, headers, config) { 
				$rootScope.$broadcast("fieldMappingWarning", data);
				$modalInstance.dismiss("cancel");
			});

		}; 
	}]).controller("featureExplorationCtrl",["$scope","$routeParams","$http","logger","$modal","$log","$interval","$rootScope","$q", "$sce",function($scope,$routeParams,$http,logger,$modal,$log,$interval,$rootScope, $q, $sce){
		
		$scope.dataSourceName = $routeParams.param1;
		$scope.overviewIframSrc = ""; //$sce.trustAsResourceUrl("https://www.yahoo.com");
		$scope.diveIframSrc = ""; //$sce.trustAsResourceUrl("https://www.google.com");
		$scope.loader = false;
		$scope.tabs = [{
				title: 'Overview',
				active: false
			},
			{
				title: 'Dive',
				active: false
			}
		]
		$scope.onClickTab = function (tab) {
			$scope.featureExplorActiveTab = tab.title;
		};
		
		// promise object
		var deferred = $q.defer();
		 
		$scope.initFeatureExploration = function(){
			 
		}
		
	
}]).controller("FacebookController" , ["$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
	$scope.dataSource = dataSource;

	$scope.$on("myEventDataSource", function (event, args) {
		$scope.dataSource = angular.copy(args);

		if($scope.dataSource.ingection === "quick"){
			$scope.ingections = "Basic";
			$scope.dataHide = true;
		}else{
			$scope.ingections = "Advance";
			$scope.dataHide = false;
		}

		$scope.dataSource.fieldMappings =  [];
		$scope.fieldMappings = args.fieldMappings;
		$scope.validators = [];

		if(null != $scope.dataSource.advancedValidations && $scope.dataSource.advancedValidations.length > 0){
			_.each($scope.dataSource.advancedValidations, function(advancedvalidation, index) {
				var fieldStores = [];
				_.each($scope.fieldMappings, function(mappingObject,index){
					_.each(advancedvalidation.fieldsStore , function(storeObject,index){
						if(mappingObject.id ===  storeObject){
							fieldStores.push(mappingObject);

						}
					});
				});
				$scope.validators.push(
						{   title:"Validation "+index, 
							selectFields : fieldStores, 
							validationsApplied : advancedvalidation.dataValidations, 
							advancedvalidationId : advancedvalidation.id,
							dropdown : [$scope.fieldMappings]
						});
				changingSelectTest($scope.validators[index].dropdown , fieldStores , index);
			});

		}
	});
	
	
	if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
		$http({
			url: "/fetchExternalDataSource",
			method: 'Post',
			data : $scope.dataSource.externalDataId
		}).then(function(response) {
			$scope.externalData = response.data;
			$scope.externalData.googleAnalytics = undefined;
			$scope.externalData.amazonS3 = undefined;
			$scope.externalData.connectorType = $scope.externalData.connectorType;
			$scope.facebookPage= $scope.externalData.facebookPage;
			//$scope.connectorType = "facebookPage";
			
		});

	}else{
		$scope.externalData = {
				id : null,
				dataSourceId : dataSource.id,
				amazonS3 : null,
				delimeter : "",
				storagePath : "",
				connectorType : "",
				salesforce : null,
				dropbox : null
		};

       $scope.facebookPage = {
				
    		  	
    		  //  "facebookAccessToken" : $scope.datasource.config.facebookAccessToken,
				//"facebookSecretId" : $scope.datasource.config.facebookSecretId,
				"facebookDataType" : "",
				"facebookPageName" :""
    	
		};
	}; 
	
	function refreshedDataSource (isSaveForVisualization){
		DataSourceService.getDataSource("/getDataSource" ,$scope.dataSource.dataSourceName).then(function(data){
			$scope.dataSource  = data;
			backUp = angular.copy($scope.dataSource.ingection);
			if($scope.dataSource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}
			$scope.dataSource.availableToUse = "true";

			$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


			if($scope.dataSource !== null && 
					$scope.dataSource.dataSourceType === "RDBMS_DATA_SOURCE"){

				$scope.fileDataIngesterDetails = {
						"dataSourceType" : "RDBMS_DATA_SOURCE"
				};
			}

			if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
				$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
			};

			if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
				$scope.fileDataIngesterDetails.delimiter = "\\t";
			}

			$rootScope.$broadcast("shareDataSourceToAllController", $scope.dataSource);
			
			//If File saved then reload list of datasources
			$rootScope.isDataSourceFileSaved = true;
			
			if(isSaveForVisualization){
				$scope.saveForVisualize();
			}
		});
	};
	
	$scope.cancel = function() {
		$modalInstance.dismiss("cancel");
	};
	
	 $scope.saveFacebookData = function (){
        	
		     if($scope.facebookPage.facebookDataType === undefined  || $scope.facebookPage.facebookDataType === "" ||  $scope.facebookPage.facebookDataType === null){
					logger.logError("Please select Facebook Data Type");
					return false;
			 }else if($scope.facebookPage.facebookPageName === undefined  || $scope.facebookPage.facebookPageName === "" ||  $scope.facebookPage.facebookPageName === null){
					logger.logError("Please enter Facebook Page Name");
					return false;
			 }else if($scope.externalData.delimeter === undefined  || $scope.externalData.delimeter === "" ||  $scope.externalData.delimeter === null){
					logger.logError("Please enter delimeter");
					return false;
				}
			$scope.externalData.connectorType= "FACEBOOKPAGE";
			$scope.externalData.facebookPage = $scope.facebookPage;
			
			$modalInstance.close($scope.externalData);
		};
}]).controller("hackylController" , ["$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
	$scope.dataSource = dataSource;		
		
		if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
			$http({
				url: "/fetchExternalDataSource",
				method: 'Post',
				data : $scope.dataSource.externalDataId
			}).then(function(response) {
				$scope.externalData = response.data;
				$scope.externalData.googleAnalytics = undefined;
				$scope.externalData.amazonS3 = undefined;
				$scope.externalData.connectorType = $scope.externalData.connectorType;
				$scope.hackyl= $scope.externalData.hackyl;
			});

		}else{
			$scope.externalData = {
					id : null,
					dataSourceId : dataSource.id,
					amazonS3 : null,
					delimeter : "",
					storagePath : "",
					connectorType : "",
					salesforce : null,
					dropbox : null,
					facebookPage: null
			};

           $scope.hackyl = {
					
        		 	"companyDetails" : "",
					"asset" :"",
					"entityType" : "",
					"entityCode" : "",
					"ipUrl": ""
        	
			};
		}; 
		
		function refreshedDataSource (isSaveForVisualization){
			DataSourceService.getDataSource("/getDataSource" ,$scope.dataSource.dataSourceName).then(function(data){
				$scope.dataSource  = data;
				backUp = angular.copy($scope.dataSource.ingection);
				if($scope.dataSource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
				$scope.dataSource.availableToUse = "true";

				$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


				if($scope.dataSource !== null && 
						$scope.dataSource.dataSourceType === "RDBMS_DATA_SOURCE"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "RDBMS_DATA_SOURCE"
					};
				}

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
					$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
				};

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
					$scope.fileDataIngesterDetails.delimiter = "\\t";
				}

				$rootScope.$broadcast("shareDataSourceToAllController", $scope.dataSource);
				
				//If File saved then reload list of datasources
				$rootScope.isDataSourceFileSaved = true;
				
				if(isSaveForVisualization){
					$scope.saveForVisualize();
				}
			});
		};
		
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};
		
		$scope.saveHackylData = function(){
			
			if($scope.hackyl.companyDetails === undefined  || $scope.hackyl.companyDetails === "" ||  $scope.hackyl.companyDetails === null){
				logger.logError("Please select company details");
				return false;
		    }else if($scope.hackyl.asset === undefined  || $scope.hackyl.asset === "" ||  $scope.hackyl.asset === null){
				logger.logError("Please select asset");
				return false;
		    }else if($scope.hackyl.entityType === undefined  || $scope.hackyl.entityType === "" ||  $scope.hackyl.entityType === null){
				logger.logError("Please select entity type");
				return false;
		    }else if($scope.hackyl.entityCode === undefined  || $scope.hackyl.entityCode === "" ||  $scope.hackyl.entityCode === null){
				logger.logError("Please enter entity code");
				return false;
		    }else if($scope.externalData.delimeter === undefined  || $scope.externalData.delimeter === "" ||  $scope.externalData.delimeter === null){
				logger.logError("Please enter delimeter");
				return false;
			}
			
			$scope.externalData.connectorType= "HECKYL";
			$scope.externalData.hackyl = $scope.hackyl;
			
			$modalInstance.close($scope.externalData);
		};
			
   }]).controller("ProbeController" , ["$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
	$scope.dataSource = dataSource;
	
		
		if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
			$http({
				url: "/fetchExternalDataSource",
				method: 'Post',
				data : $scope.dataSource.externalDataId
			}).then(function(response) {
				$scope.externalData = response.data;
				$scope.externalData.googleAnalytics = undefined;
				$scope.externalData.amazonS3 = undefined;
				$scope.externalData.connectorType = $scope.externalData.connectorType;
				$scope.hackyl= undefined;
				$scope.probe42= $scope.externalData.probe42;
				$scope.type = $scope.probe42.companyDetails;
				$scope.companyDetails($scope.type);
			});

		}else{
			$scope.externalData = {
					id : null,
					dataSourceId : dataSource.id,
					amazonS3 : null,
					delimeter : "",
					storagePath : "",
					connectorType : "",
					salesforce : null,
					dropbox : null,
					facebookPage: null,
					hackyl : null
			};

           $scope.probe42 = {
					
        		 	"companyDetails" : "",
					"companyBaseData" :"",
					"companyFinancials" : "",
					"companyCompliance" : "",
					"companyIndustrySegment" : "",
					"companyHoldingSubsidiary" : ""
					
        	
			};
		}; 
		
		function refreshedDataSource (isSaveForVisualization){
			DataSourceService.getDataSource("/getDataSource" ,$scope.dataSource.dataSourceName).then(function(data){
				$scope.dataSource  = data;
				backUp = angular.copy($scope.dataSource.ingection);
				if($scope.dataSource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
				$scope.dataSource.availableToUse = "true";

				$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


				if($scope.dataSource !== null && 
						$scope.dataSource.dataSourceType === "RDBMS_DATA_SOURCE"){

					$scope.fileDataIngesterDetails = {
							"dataSourceType" : "RDBMS_DATA_SOURCE"
					};
				}

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
					$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
				};

				if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
					$scope.fileDataIngesterDetails.delimiter = "\\t";
				}

				$rootScope.$broadcast("shareDataSourceToAllController", $scope.dataSource);
				
				//If File saved then reload list of datasources
				$rootScope.isDataSourceFileSaved = true;
				
				if(isSaveForVisualization){
					$scope.saveForVisualize();
				}
			});
		};
		
		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};
		
		$scope.companyDetails = function(type){
			$scope.detailsType = type ;
		}
		
		$scope.saveProbe42Data = function(){
			
			if($scope.probe42.companyDetails === undefined  || $scope.probe42.companyDetails === "" ||  $scope.probe42.companyDetails === null){
				logger.logError("Please select company details");
				return false;
		    }else if($scope.probe42.companyCINNumber === undefined  || $scope.probe42.companyCINNumber === "" ||  $scope.probe42.companyCINNumber === null){
				logger.logError("Please enter company CIN Number");
				return false;
		    }else if($scope.externalData.delimeter === undefined  || $scope.externalData.delimeter === "" ||  $scope.externalData.delimeter === null){
				logger.logError("Please enter delimeter");
				return false;
		    }
			
			$scope.externalData.connectorType= "PROBE42";
			$scope.externalData.probe42 = $scope.probe42;
			
			$scope.externalData.probe42.probe42ApiKey= $scope.dataSource.config.probe42ApiKey;
			$scope.externalData.probe42.connectionName= $scope.dataSource.config.conncetionName;
			
			$http({
				url: "/saveExternalDataProbe42",
				method: 'Post',
				data :  $scope.externalData
			}).success(function(data, status, headers, config) {
				$scope.dataSource = data;
				logger.logSuccess("Connection details saved successfully");
				refreshedDataSource();
				$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
				$modalInstance.dismiss("cancel");
			}).error(function(data, status, headers, config) { 
				$rootScope.$broadcast("fieldMappingWarning", data);
				$modalInstance.dismiss("cancel");
			});
		};
   }]).controller("SAPERPController" , ["$modalInstance","dataSource","$modal","$route","$scope","DataSourceService", "logger"  , "$http","$log", "uniqueArray", "$rootScope", function($modalInstance, dataSource, $modal,$route, $scope, DataSourceService , logger ,$http, $log, uniqueArray, $rootScope){
		$scope.dataSource = dataSource;
		
			if($scope.dataSource != null && $scope.dataSource.externalDataId != null){
				$http({
					url: "/fetchExternalDataSource?eDetailsId="+$scope.dataSource.externalDataId,
					method: 'Post'
				}).then(function(response) {
					$scope.externalData = response.data;
					$scope.externalData.googleAnalytics = undefined;
					$scope.externalData.amazonS3 = undefined;
					$scope.externalData.connectorType = $scope.externalData.connectorType;
					$scope.hackyl= undefined;
					$scope.sapErp= $scope.externalData.sapErp;
					//$scope.getCoustomColumn();
					$scope.columnList = $scope.externalData.sapErp.columnName;
				});

			}else{
				$scope.externalData = {
						id : null,
						dataSourceId : dataSource.id,
						amazonS3 : null,
						delimeter : "",
						storagePath : "",
						connectorType : "",
						salesforce : null,
						dropbox : null,
						facebookPage: null,
						hackyl : null,
						probe42 : null
				};

	           $scope.sapErp = {
						
	        		 	"hostName" : "",
						"instanceNo" :"",
						"client" : "",
						"user" : "",
						"password" : "",
						"language" : "",
						"selectColumn" : false
						
	        	
				};
			}; 
			
			function refreshedDataSource (isSaveForVisualization){
				DataSourceService.getDataSource("/getDataSource?datasourcename=" ,$scope.dataSource.dataSourceName).then(function(data){
					$scope.dataSource  = data;
					backUp = angular.copy($scope.dataSource.ingection);
					if($scope.dataSource.ingection === "quick"){
						$scope.ingections = "Basic";
						$scope.dataHide = true;
					}else{
						$scope.ingections = "Advance";
						$scope.dataHide = false;
					}
					$scope.dataSource.availableToUse = "true";

					$scope.fileDataIngesterDetails = data.fileDataIngesterDetails;


					if($scope.dataSource !== null && 
							$scope.dataSource.dataSourceType === "RDBMS_DATA_SOURCE"){

						$scope.fileDataIngesterDetails = {
								"dataSourceType" : "RDBMS_DATA_SOURCE"
						};
					}

					if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
						$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
					};

					if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.delimiter === "\t"){
						$scope.fileDataIngesterDetails.delimiter = "\\t";
					}

					$rootScope.$broadcast("shareDataSourceToAllController", $scope.dataSource);
					
					//If File saved then reload list of datasources
					$rootScope.isDataSourceFileSaved = true;
					
					if(isSaveForVisualization){
						$scope.saveForVisualize();
					}
				});
			};
			
			$scope.cancel = function() {
				$modalInstance.dismiss("cancel");
			};
			
			$scope.companyDetails = function(type){
				$scope.detailsType = type ;
			}
			
			$scope.getCoustomColumn = function(){

				if(undefined != $scope.sapErp.tableName && null != $scope.sapErp.tableName && "" != $scope.sapErp.tableName && undefined !=$scope.sapErp.functionModule
						&& null !=$scope.sapErp.functionModule && "" !=$scope.sapErp.functionModule){
					
					$scope.externalData.connectorType= "SAPERP";
					$scope.externalData.sapErp = $scope.sapErp;
					
					$scope.externalData.sapErp.hostName= $scope.dataSource.config.sapJcoHostName;
					$scope.externalData.sapErp.instanceNo= $scope.dataSource.config.sapJcoInstanceNo;
					$scope.externalData.sapErp.client= $scope.dataSource.config.sapJcoClient;
					$scope.externalData.sapErp.user= $scope.dataSource.config.sapJcoUser;
					$scope.externalData.sapErp.password= $scope.dataSource.config.sapJcoPasswd;
					$scope.externalData.sapErp.language= $scope.dataSource.config.sapJcoLang;
					$scope.externalData.sapErp.connectionName= $scope.dataSource.config.conncetionName;
					
					$http({
						url: "/getSapErpCustomColumns",
						method: 'Post',
						data : $scope.externalData
					}).then(function(response) {
						$scope.columnList = response.data;
						
					});
					
			    }else{
			    	$scope.columnList = null;
			    }
				
			}
			
			$scope.checkAll = function(){
				if(true == $scope.sapErp.selectColumn && $scope.columnList != null){
					
					
						angular.forEach($scope.columnList, function(item) {
							$scope.sapErp.columnList.selected =true;
					    });

					
				}
			}
			
			$scope.saveSapErpData = function(){
				
				if($scope.sapErp.functionModule === undefined  || $scope.sapErp.functionModule === "" ||  $scope.sapErp.functionModule === null){
					logger.logError("Please select Function Module");
					return false;
			    }else if($scope.sapErp.tableName === undefined  || $scope.sapErp.tableName === "" ||  $scope.sapErp.tableName === null){
					logger.logError("Please enter Table Name");
					return false;
			    }else if($scope.sapErp.rowCount === undefined  || $scope.sapErp.rowCount === "" ||  $scope.sapErp.rowCount === null){
					logger.logError("Please enter Row Count");
					return false;
			    }
				
				$scope.externalData.connectorType= "SAPERP";
				$scope.externalData.sapErp = $scope.sapErp;
				
				$scope.externalData.sapErp.hostName= $scope.dataSource.config.sapJcoHostName;
				$scope.externalData.sapErp.instanceNo= $scope.dataSource.config.sapJcoInstanceNo;
				$scope.externalData.sapErp.client= $scope.dataSource.config.sapJcoClient;
				$scope.externalData.sapErp.user= $scope.dataSource.config.sapJcoUser;
				$scope.externalData.sapErp.password= $scope.dataSource.config.sapJcoPasswd;
				$scope.externalData.sapErp.language= $scope.dataSource.config.sapJcoLang;
				$scope.externalData.sapErp.connectionName= $scope.dataSource.config.conncetionName;
				
				$http({
					url: "/saveExternalDataSapErp",
					method: 'Post',
					data :  $scope.externalData
				}).success(function(data, status, headers, config) {
					$scope.dataSource = data;
					logger.logSuccess("Connection details saved successfully");
					refreshedDataSource();
					$scope.$emit("shareDataSourceToAllController", $scope.dataSource);
					$modalInstance.dismiss("cancel");
				}).error(function(data, status, headers, config) { 
					$rootScope.$broadcast("fieldMappingWarning", data);
					$modalInstance.dismiss("cancel");
				});
			};		
   }]);
}).call(this);

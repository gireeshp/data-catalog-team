(function(){
	"use strict";
	
	var dataSourceApiModule = angular.module("app.dataSourceApi",['angularFileUpload']);
	
	dataSourceApiModule.controller("dataSourceApiCtrl",["$scope" ,"$filter" , "$http", "$location" , "logger", "$modal", "$log", "$route","$interval","$cookieStore","$rootScope", function($scope  ,$filter , $http, $location , logger, $modal, $log, $route, $interval, $cookieStore, $rootScope){
		$scope.projectAccessType = $cookieStore.get("projectAccessType");
		$scope.getDataSourceWithApiList=function(){
			$http.post("/getDeployedDS").success(function(data, status, headers, config){
				return $scope.stores = data	
				, $scope.searchKeywords = "", 
				$scope.filteredStores = [],
				$scope.row = "", 
				$scope.select = function(page) {
					$scope.pageNum = page;
					var end, start;
					return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
				}, 
				$scope.onFilterChange = function() {
					return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
				}, $scope.onNumPerPageChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				}, $scope.search = function() {
					return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				}, $scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0;
				}, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				$scope.numPerPage = $scope.numPerPageOpt[4], 
				$scope.currentPage = 1, 
				$scope.currentPageStores = [],
				($scope.init = function() {
					return $scope.search(), $scope.select($scope.currentPage),$scope.order("-id");
				})();
			});	
		}
		
		
		$interval(function() {
			if($rootScope.isModelCancelled){
				$scope.getDataSourceWithApiList();
				$rootScope.isModelCancelled = false;
			}
		}, 100);
		
		$scope.viewModel = function(component) {
			var modalInstance;
			modalInstance = $modal.open({
				size: "lg",
				templateUrl: "/views/template/create-new-datasource-api-modal.html",
				controller: "datasourceApiModalCtrl",
				resolve : {
					showComponent : function(){
						return component; 
					},
					disable : function(){
						return true;
					},
					editable : function(){
						return true;
					}
				}

			}),modalInstance.result.then(function(reponame) {
				if(reponame === true){
					$scope.getDataSourceWithApiList();
				}
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		}


		$scope.open = function(component) {
			var modalInstance;
			modalInstance = $modal.open({
				size: "lg",
				templateUrl: "/views/template/create-new-datasource-api-modal.html",
				controller: "datasourceApiModalCtrl",
				resolve : {
					showComponent : function(){
						return component; 
					},
					disable : function(){
						return false;
					},
					editable : function(){
						return false;
					}
				}

			}),modalInstance.result.then(function(reponame) {
				if(reponame === true){
					$scope.getDataSourceWithApiList();
				}	
			}, function() {

				$log.info("Modal dismissed at: " + new Date);
			});
		};

		// unused method need to delete
		$scope.deleteRModel = function(store){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve : {
					showComponent : function(){
						return store; 
					},
					deleteObject: function(){
						return {name:store.modelName, type: "custom component"};
					}
				}
			}),modalInstance.result.then(function() {
				$http({
					url: "/deleteRModel?modelId="+store.modelId,
					method: "GET"
				}).success(function(data){
					logger.logSuccess("R Model deleted successfully");
					$route.reload();
				}).error(function(data){
					logger.logError(data);
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};
		
		$scope.createApiKey=function(data, $index){
			$scope.objTosend = data;
			delete $scope.objTosend.createdBy;
			
			if($scope.objTosend.status=="0"){
				$scope.objTosend.status = 1;
			}else{
				$scope.objTosend.status = 0;
			}
			
			if($scope.objTosend.modifiedDate=="null"){
				$scope.objTosend.modifiedDate = 0;
			}
			
			$http({
				url: "/toggleDeployUndeploy",
				method: "Post",
				data: $scope.objTosend
			}).success(function(data){
				$rootScope.isModelCancelled = true;
			}).error(function(data){
				logger.logError(data);
				if($scope.stores[$index].status==1){
					$scope.stores[$index].status=1;
				}else{
					$scope.stores[$index].status=0;
				}
				
			});
		}

	}]).controller("datasourceApiModalCtrl", ["$scope", "$modalInstance", "$http" , "$location", "logger" , "showComponent", "$upload", "$route","disable", "editable","$modal","$rootScope", function($scope, $modalInstance, $http , $location, logger, showComponent, $upload, $route,disable, editable, $modal, $rootScope){
		$scope.disabled = disable;
		$scope.disable = false;
		$scope.editable = editable;
		$scope.isUpdated = false;
		$scope.serverUrl = "";
		$scope.inputRequest = "";
		$scope.dsListToSelect  = [];
		$scope.dsListForSelection = [];
		$http.get("/fetchDataSourceForAPI").success(function(data, status, headers, config){
			$scope.dsListForSelection = data;
		});
		
		$scope.selectedDataSourceObj = {};
		
		$scope.onSelected = function (selectedItem) {
			$scope.selectedDataSourceObj = selectedItem;
			 if(undefined!=$scope.selectedDataSourceObj && 
						undefined!=$scope.selectedDataSourceObj.dataSourceName){
					$scope.inputRequest = " select * from " + $scope.selectedDataSourceObj.dataSourceName +" limit 10 ";	
				}	
				
		}
		
		$http({
			url: "/getMetaDataOfModel",
			method: 'POST'
		}).success(function(data){
			$scope.serverUrl=data.apiUrl;
		}).error(function(data){
			if(data){
				logger.logError(data);
			}else{
				logger.logError("Problem while getting response");
			}
		});

		if(showComponent){
			$scope.compObj = showComponent;
			$scope.flagNew = "NO";
			$scope.disable = true;
			$scope.inputRequest = showComponent.query;
			if($scope.compObj.apiKey!=undefined && $scope.compObj.apiKey!="" && $scope.compObj.apiKey!=null &&
					$scope.compObj.status!=undefined && $scope.compObj.status!=null 
					&&$scope.compObj.status==1){
				$scope.tglBtn = true;
				
			} 
		}else{
			$scope.flagNew = "YES";
			$scope.compObj = {};
			$scope.tglBtn = false;
			$scope.compObj.status=0;
			$scope.compObj.query= "select * from limit 10";
		}
		
		$scope.getApiUrl=function(model){
			return $scope.serverUrl+"/runDataSourceExposeQueryApI?id="+model.id+"&apiKey="+model.apiKey;
		}
		
		$scope.testAPI = function(object){
			var modalInstance;
			modalInstance = $modal.open({
				size: "lg",
				templateUrl: "/views/template/testApiSparkMLLib.html",
				controller: "TestAPISparkMLLibCtrl",
				resolve : {
					modelToTest : function(){
						return object; 
					}
				}
			}),modalInstance.result.then(function(reponame) {
				
			}, function() {

				$log.info("Modal dismissed at: " + new Date);
			});
		};
		
		var dataSourceName = "";
		if(undefined != $scope.compObj &&  undefined != $scope.compObj.dataSourceName){
			dataSourceName = $scope.compObj.dataSourceName;
		}else if(undefined!=$scope.selectedDataSourceObj && 
				undefined!=$scope.selectedDataSourceObj.dataSourceId &&
				undefined!=$scope.selectedDataSourceObj.dataSourceName){
			$scope.compObj.dataSourceId = $scope.selectedDataSourceObj.dataSourceId;
			$scope.compObj.dataSourceName = $scope.selectedDataSourceObj.dataSourceName;
			dataSourceName = dataSourceApi.dataSourceName;	
		}	
		
		
		$scope.showTable = "n";
		$scope.testQueryTableExistance = function(dataSourceApi){
			
			$scope.inputRequest = angular.copy($scope.inputRequest.replace(/\n/g, ' '));
			if($scope.inputRequest.toLowerCase().indexOf("limit") === -1){
				logger.logError("Query should contain a limit clause at the end");
				return false;
			}
			
			$scope.queryResult = [];
			$scope.headerRow = [];
			$scope.gridOptions = {
					enableSorting: true,
					columnDefs: [],
					data: []
			};
			
			var methodName = "/runDataSourceExposeQueryApI?id="+dataSourceApi.id+"&apiKey="+dataSourceApi.apiKey;
			
			$scope.inputDataRequest =  '{"query":"'+ $scope.inputRequest + '"}'; 
			
			$scope.responseRequest = "";
			//$scope.data = [["aj_test_28_aug_2017_1.field0","aj_test_28_aug_2017_1.field1","aj_test_28_aug_2017_1.field2","aj_test_28_aug_2017_1.field3","aj_test_28_aug_2017_1.jobinstanceid"],["firstName","lastName","email","phoneNumber","2295202"],["John","Doe","john@doe.com","0123456789","2295202"],["Jane","Doe","jane@doe.com","9876543210","2295202"],["James","Bond","james.bond@mi6.co.uk","0612345678","2295202"],["John","Doe","john@doe.com","0123456789","2295202"],["Jane","Doe","jane@doe.com","9876543210","2295202"],["James","Bond","james.bond@mi6.co.uk","0612345678","2295202"],["John","Doe","john@doe.com","0123456789","2295202"],["Jane","Doe","jane@doe.com","9876543210","2295202"],["James","Bond","james.bond@mi6.co.uk","0612345678","2295202"]];
			//$scope.showResult($scope.data);
			$http({
				url: methodName,
				method: 'POST',
				data : $scope.inputDataRequest  
			}).success(function(data){
				$scope.showTable = "y";
				$scope.showResult(data);
			}).error(function(data){
				if(data){
					logger.logError(data);
				}else{
					logger.logError("Problem while getting response");
				}
			});
		}
		
		$scope.selectedIndex = 0; // Whatever the default selected index is,
		// use -1 for no selection

		$scope.showResult = function (data) {
			$scope.value = "RunQuery";
			$scope.queryResult = data;

			var objectOfOutput = [];
			var columnCalculate = [];
			var widthCalculate = 100/$scope.queryResult[0].length;
			for(var i = 0 ; i < $scope.queryResult.length ; i++){
				if(i == 0){
					_.each($scope.queryResult[i] , function(values){
						var obj = {field : values.substring(values.lastIndexOf(".") + 1 , values.length) , width: widthCalculate+'%' , minWidth: 70 };
						obj.field = angular.lowercase(obj.field); 
						columnCalculate.push(obj);
					});
				}else{
					var j = 0;
					var obj = {};
					_.each($scope.queryResult[i] , function(values){
						obj[columnCalculate[j].field] = values;
						j++;
					});
					objectOfOutput.push(obj);
				}
			}
			
			$scope.gridOptions.columnDefs = columnCalculate; 
			$scope.gridOptions.data = objectOfOutput;

			if($scope.queryResult.length > 0){
				$scope.headerRow = data[0];
				$scope.queryResult.splice(0, 1);
			}
			if($scope.gridOptions.data.length < 1){
				logger.logError("No record found");
			}

		};

		$scope.getApiKeyByTogleBtn =function(compObj){
			if(compObj.status=='1'){
				$scope.tglBtn = true;
			}else{
				$scope.tglBtn = false;
			}
		},
		$scope.createApiKey=function(){
			
			if((undefined != $scope.compObj &&  undefined != $scope.compObj.dataSourceName) || (undefined!=$scope.selectedDataSourceObj && 
					undefined!=$scope.selectedDataSourceObj.dataSourceId &&
					undefined!=$scope.selectedDataSourceObj.dataSourceName)){
				
				if(!$scope.selectedDataSourceObj.dataSourceId ||  null==$scope.selectedDataSourceObj.dataSourceId){
					$scope.compObj.dataSourceId = showComponent.dataSourceId;
					$scope.compObj.dataSourceName = showComponent.dataSourceName;
				}else{
					$scope.compObj.dataSourceId = $scope.selectedDataSourceObj.dataSourceId;
					$scope.compObj.dataSourceName = $scope.selectedDataSourceObj.dataSourceName;
				}
				
				
			}else{
				if($scope.tglBtn){
					$scope.tglBtn = false;	
					$scope.compObj.status=0;
				}else{
					$scope.tglBtn = true;
					$scope.compObj.status=1;
				}
				logger.logError("Please select datasource");
				return ;
			}
			
			$scope.inputRequest = angular.copy($scope.inputRequest.replace(/\n/g, ' '));
			if(!$scope.inputRequest){
				$scope.tglBtn = false;
				logger.logError("Query should not blank.");
				return false;
			}
			if($scope.inputRequest.toLowerCase().indexOf("limit") === -1){
				$scope.tglBtn = false;
				logger.logError("Query should contain a limit clause at the end");
				return false;
			}
			
			if(!$scope.tglBtn){
				$scope.tglBtn = false;	
				$scope.compObj.status=0;
			}else{
				$scope.tglBtn = true;
				$scope.compObj.status=1;
			}

			delete $scope.compObj.createdBy;
			
			if($scope.compObj.modifiedDate=="null"){
				$scope.compObj.modifiedDate = 0;
			}
			$scope.compObj.query = $scope.inputRequest;
			$http({
				url: "/toggleDeployUndeploy",
				method: "Post",
				data: $scope.compObj
			}).success(function(data){
				$rootScope.isModelCancelled = true;
				$scope.compObj = data;
				$scope.getApiKeyByTogleBtn(data);
				$scope.isUpdated = true;
			}).error(function(data){
				logger.logError(data);
			});
		},
		$scope.cancel = function() {
			/*$route.reload();*/
			$modalInstance.dismiss($scope.isUpdated);
			$rootScope.isModelCancelled = true;
			
		};
	}]).controller('deleteAppDSCtrl', ["$scope", "$http", "$modal", "$log" ,"logger" , "$routeParams", "$interval","$modalInstance","deleteObject",   function($scope,$http,$modal,$log,logger, $routeParams, $interval,$modalInstance, deleteObject) {
		$scope.deleteObject = deleteObject;
		$scope.indicatorDelete = true;
		$scope.deleteds = {
				deleteNode : ""
		};
		$scope.deleteSelectedAppDs = function(){
			if($scope.deleteds.deleteNode != undefined && $scope.deleteds.deleteNode.toLowerCase() === "delete"){
				$modalInstance.close();
			}else{
				logger.logError("Please type delete.....");
				$scope.deleteds.deleteNode = "";
				return false;
			}
		};

		$scope.cancel = function() {
			$modalInstance.dismiss("cancel");
		};

	}]).controller("TestAPISparkMLLibCtrl", ["$scope", "$modalInstance", "$http" , "$location", "logger" , "$upload", "$route", "modelToTest", function($scope, $modalInstance, $http , $location, logger, $upload, $route, modelToTest){
		$scope.hideForHideForDsApi = true;
		if(modelToTest){
			$scope.model = modelToTest;
			if($scope.model.id){
				$scope.hideForHideForDsApi = false; 
			}
		}
		
		$http({
			url: "/getMetaDataOfModel",
			method: 'POST'
		}).success(function(data){
			$scope.url = data.apiUrl+methodName;
		}).error(function(data){
			if(data){
				logger.logError(data);
			}else{
				logger.logError("Problem while getting response");
			}
		});
		
		
		var methodName = "/runDataSourceExposeQueryApI?id="+$scope.model.id+"&apiKey="+$scope.model.apiKey;
		
		$scope.inputRequest = "select * from " + $scope.model.dataSourceName +" limit 10 ";
		
		$scope.inputDataRequest =  '{"query":"'+ $scope.inputRequest + '"}'; 
		
		$scope.responseRequest = "";
		
		$scope.sendRequest = function(){
			$http({
				url: methodName,
				method: 'POST',
				data : $scope.inputDataRequest  
			}).success(function(data){
				$scope.responseRequest = JSON.stringify(data,null,"    ");
			}).error(function(data){
				if(data){
					logger.logError(data);
				}else{
					logger.logError("Problem while getting response");
				}
			});
		};
		
		$scope.cancel = function() {
			/*$route.reload();*/
			$modalInstance.dismiss("cancel");
		};
	}]);
}).call(this);
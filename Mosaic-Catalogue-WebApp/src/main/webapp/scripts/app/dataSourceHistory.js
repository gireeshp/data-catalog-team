(function(){
	"user strict";
	var module = angular.module("app.dataSourceHistory" , ['app.datasource','app.dataSourceTable','angularFileUpload','checklist-model','FBAngular']);

	module.controller("dataSourceHistoryCtrl" , ["$anchorScroll","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "$loading","$cookieStore","$interval","DataSourceService", 
		function($anchorScroll,$route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $loading,$cookieStore, $interval,DataSourceService){
		$scope.isCalled = true;
		$scope.localActionToReload = false;
		$rootScope.dsHistorySnapshotId=undefined;
		$scope.restrict = true;
		
		$interval(function() {
			$scope.disbaled()
			if($rootScope.dsHistorySnapshotId){
				$scope.dataSourceId=$rootScope.dsHistorySnapshotId;
				if($scope.isCalled){
					$scope.getAllJobInstances();	
				}
			}
			
			if($rootScope.DsRestored || $scope.localActionToReload){
				$scope.getAllJobInstances();
				$scope.localActionToReload = false;
			}
		}, 1000);

		$scope.limitOfDelete = {	
		        name: 'default'
		};
		
		$scope.getTotalSize = function(list){
			if(list){
				var total = 0.0;
				angular.forEach(list, function (item) {
					total+=parseFloat(item.size);
				});
				return total;	
			}
		}
		
		$scope.getTotalRecords = function(list){
			if(list){
				var total = 0.0;
				angular.forEach(list, function (item) {
					total+=parseInt(item.totalRecords);
				});
				return total;
			}
		}
		
		
		$scope.changeActive=true
		$scope.changeHistory=true;
		
		$scope.checkAllHistory = function(historyCheckAll){
			angular.forEach($scope.historyDatasources, function (item) {
				item.check = historyCheckAll;
			});
			if(historyCheckAll){
				$scope.changeHistory=false;
			}else{
				$scope.changeHistory=true;
			}
		}
		
		$scope.checkAllActive = function(activeCheckAll){
			angular.forEach($scope.activeDatasources, function (item) {
				item.check = activeCheckAll;
			});
			if(activeCheckAll){
				$scope.changeActive=false;
			}else{
				$scope.changeActive=true;
			}
		}
		
		$scope.changeValueTofalseHistory = function(makeItFalse){
			if(makeItFalse){
				$scope.changeHistory=false;	
			}else{
				$scope.changeHistory=true;
			}
			
		}
		
		$scope.changeValueTofalseActive = function(makeItFalse){
			if(makeItFalse){
				$scope.changeActive=false;	
			}else{
				$scope.changeActive=true;
			}
		}
		
		
		$scope.getAllJobInstances = function(){
			
			
			
			
			
			
			
		}
		
		$scope.activateLimitOfDelete = function(type){
			if(type == 'button'){
				
			}else if(type == 'radio'){
				
			}
		}		
		
		$scope.checkFieldMapping = function(fm1, fm2){
			$scope.fieldMappingStatus = true;
			if(fm1.length==fm2.length){
				for(var i=0;i<fm1.length;i++){
					if(fm1[i].fieldName==fm2[i].fieldName && fm1[i].fieldDataType==fm2[i].fieldDataType){
						continue;
					}else{
						$scope.fieldMappingStatus=false;
						break;
					}
				}	
			}
			return $scope.fieldMappingStatus;
		};
		
		$scope.showWarningOfFieldMappingChange = function(jobInstance, dataSourceInstance, i){
			if(jobInstance){
				$scope.newDataSource = jobInstance; 
			}
			if(dataSourceInstance){
				$scope.dataSourceInstance = dataSourceInstance;
			}
			$scope.StatusFileTypeMatch = false;
			if(jobInstance.fileType === dataSourceInstance.fileType){
				$scope.StatusFileTypeMatch = true;
				
				if($scope.newDataSource && $scope.newDataSource.dataSourceId){
					var reqDataObject = {'dataSourceId':$scope.newDataSource.dataSourceId,'fieldMappingVersionId':$scope.newDataSource.fieldMappingVersionId};
					$http.post("/getFMConfigVersionByFieldMappingVersionId", reqDataObject).success(function(data, status, headers, config){
							$scope.newFieldMappings = data.dataSource.fieldMappings;
						$http.post("/getFMConfigVersionByFieldMappingVersionId", reqDataObject).success(function(data, status, headers, config){
								$scope.existingFieldMappings =data.dataSource.fieldMappings;	
								if(!$scope.StatusFileTypeMatch || (!$scope.checkFieldMapping($scope.newFieldMappings, $scope.existingFieldMappings))){
									var modalInstance;
									modalInstance = $modal.open({
										size: "lg",
										templateUrl: "/views/template/errorFieldMappingVersion.html",
										controller: "checkVersionFieldMappingController",
										resolve : {
											newFieldMappings : function(){
												return angular.copy($scope.newFieldMappings);
											},
											existingFieldMappings : function(){
												return angular.copy($scope.existingFieldMappings);
											},from : function() {
												return "H";
											},StatusFileTypeMatch : function() {
												return angular.copy($scope.StatusFileTypeMatch);
											}
										}

									}),modalInstance.result.then(function() {
									}, function() {
										$log.info("Modal dismissed at: " + new Date);
									});		
								}else{
									$scope.activeDatasources.push(jobInstance);
									$scope.temp.splice(i,1);
									$scope.historyDatasources= angular.copy($scope.temp);
									$scope.changeHistory=true;
									$scope.changeActive=true;
									$scope.activeTotalSize = $scope.getTotalSize($scope.activeDatasources);
									$scope.activeTotalRecords= $scope.getTotalRecords($scope.activeDatasources);
									$scope.inactiveTotalSize = $scope.getTotalSize($scope.historyDatasources);
									$scope.inactiveTotalRecords= $scope.getTotalRecords($scope.historyDatasources);
								}
						});
						
					});
					
				} 
			}
			else{
				logger.logError("File Type are not same. Cannot activate instances.");
			}
			
		}
		
		$scope.inactivateDataSources=function(){
			var i = $scope.activeDatasources.length;
			$scope.temp = angular.copy($scope.activeDatasources);
			while(i--){
				if ($scope.activeDatasources[i] != undefined){
					if($scope.activeDatasources[i].check==true){
						$scope.activeDatasources[i].check=false;
						$scope.historyDatasources.push($scope.activeDatasources[i]);
						$scope.temp.splice(i,1);
					}	
				}else
					break;
			}
			$scope.changeHistory=true;
			$scope.changeActive=true;
			$scope.activeDatasources= angular.copy($scope.temp);
			
			$scope.activeTotalSize = $scope.getTotalSize($scope.activeDatasources);
			$scope.activeTotalRecords= $scope.getTotalRecords($scope.activeDatasources);
			$scope.deletedTotalSize = $scope.getTotalSize($scope.deletedDatasources);
			$scope.deletedTotalRecords= $scope.getTotalRecords($scope.deletedDatasources);
			$scope.inactiveTotalSize = $scope.getTotalSize($scope.historyDatasources);
			$scope.inactiveTotalRecords= $scope.getTotalRecords($scope.historyDatasources);
		}

		$scope.activateDataSources=function(){
			
			$http({
				url: "/getDataSourceInstanceById",
				method: 'POST',
				data : $scope.dataSourceId
			}).then(function(response) {
				$scope.dataSourceInstance = response.data;
			}, 
			function(response) {
				if(response){
					logger.logError(response);	
				}else{
					logger.logError("Something went wrong");
				}
			});
			
			var i = $scope.historyDatasources.length;
			$scope.temp = angular.copy($scope.historyDatasources);
			while(i--){
				if ($scope.historyDatasources[i] != undefined){
					if($scope.historyDatasources[i].check==true){
						$scope.historyDatasources[i].check=false;
						if($scope.dataSourceInstance.fileType==$scope.historyDatasources[i].fileType){
/*						   if($scope.dataSourceInstance.fieldMappingVersionId==$scope.historyDatasources[i].fieldMappingVersionId){
							   $scope.activeDatasources.push($scope.historyDatasources[i]);
							   $scope.temp.splice(i,1);
						   }else{
							   $scope.showWarningOfFieldMappingChange($scope.historyDatasources[i], $scope.dataSourceInstance, i);	
						   }*/
							if($scope.dataSourceInstance.fieldMappingVersionId==$scope.historyDatasources[i].fieldMappingVersionId){

								if($scope.dataSourceInstance.fileType== "HBASE" && $scope.activeDatasources.length == 1){
									logger.logError("Cannot activate more than one instance for HBASE type.");
								}
								else
								{
									$scope.activeDatasources.push($scope.historyDatasources[i]);
									$scope.temp.splice(i,1);
								}
							}else{
								$scope.showWarningOfFieldMappingChange($scope.historyDatasources[i], $scope.dataSourceInstance, i);	

							}	
						
						}
						else{
							logger.logError("File Type are not same. Cannot activate instances.");
						}
					}
				}else
					break;
				
			}
			$scope.historyDatasources= angular.copy($scope.temp);
			$scope.changeHistory=true;
			$scope.changeActive=true;
			$scope.activeTotalSize = $scope.getTotalSize($scope.activeDatasources);
			$scope.activeTotalRecords= $scope.getTotalRecords($scope.activeDatasources);
			$scope.inactiveTotalSize = $scope.getTotalSize($scope.historyDatasources);
			$scope.inactiveTotalRecords= $scope.getTotalRecords($scope.historyDatasources);
		}

		$scope.deletDataSources=function(){
			var i = $scope.historyDatasources.length;
			$scope.temp = angular.copy($scope.historyDatasources);
			while(i--){
				if ($scope.historyDatasources[i] != undefined){
					if($scope.historyDatasources[i].check==true){
						$scope.historyDatasources[i].check=false;
						$scope.deletedDatasources.push($scope.historyDatasources[i]);
						$scope.temp.splice(i,1);
					}
				}else
					break;
			}
			$scope.historyDatasources=angular.copy($scope.temp);
			$scope.changeHistory=true;
			$scope.changeActive=true;
			$scope.activeTotalSize = $scope.getTotalSize($scope.activeDatasources);
			$scope.activeTotalRecords= $scope.getTotalRecords($scope.activeDatasources);
			$scope.inactiveTotalSize = $scope.getTotalSize($scope.historyDatasources);
			$scope.inactiveTotalRecords= $scope.getTotalRecords($scope.historyDatasources);
			$scope.deletedTotalSize = $scope.getTotalSize($scope.deletedDatasources);
			$scope.deletedTotalRecords= $scope.getTotalRecords($scope.deletedDatasources);
		}
		
		
		$scope.resetChanges = function(){
			$scope.historyDatasources = angular.copy($scope.historyDatasourcesBkp);
			$scope.deletedDatasources = angular.copy($scope.deletedDatasourcesBkp);
			$scope.activeDatasources = angular.copy($scope.activeDatasourcesBkp);
		}
		
		$scope.onDefaultDisable = false;
		$scope.saveNoOfInstances=function(type){
			if(type=="INSTANCES"){
				$scope.onDefaultDisable = false;
				$scope.disable = true;
			}
			if(type=="DAYS"){
				$scope.onDefaultDisable = false;
				$scope.disable = true;
			}
			if(type=="DEFAULT"){
				$scope.onDefaultDisable = true;
				$scope.disable = false;
				$scope.datasource.noOfActiveInstances=0;
			}
		}
		
		
		$scope.$on("myEventDataSource", function (event, args) {
			$scope.datasource = angular.copy(args);
			$scope.datasource_bk = angular.copy(args);
			if($scope.datasource.typeOfActiveInstances!=undefined||$scope.datasource.typeOfActiveInstances!=""){
				$scope.saveNoOfInstances($scope.datasource.typeOfActiveInstances);
			}
			
		});
		
		$scope.saveDataSourceHistory = function(){

			if($scope.datasource.typeOfActiveInstances==undefined||$scope.datasource.typeOfActiveInstances==""){
				logger.logError("Required input for No. of instances/days");	
			}else{
				if($scope.datasource.noOfActiveInstances<0){
					logger.logError("Required valid input for No. of instances/days");
				}
			}
			
			var dataObject = {
					"historyDataSources":$scope.historyDatasources,
					"deletedDataSources":$scope.deletedDatasources,
					"activeDataSources":$scope.activeDatasources
				};
				
				
			$http({
				url: "/saveDataSource",
				method: 'Post',
				data : $scope.datasource
			}).then(function(response) {
				
				var dataUpdateJobInstanceStatus = {'dataSourceId':$scope.dataSourceId,'updatedValue':dataObject};
				$http({
					url: "/updateJobInstancesStatus",
					method: 'POST',
					data: dataUpdateJobInstanceStatus
				}).then(function(response) {
					$rootScope.instanceSaved = true;
					$scope.localActionToReload = true;
					logger.logSuccess("Saved Successfully");
					DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
						$scope.datasource = data;

						if($scope.datasource.ingection === "quick"){
							$scope.ingections = "Basic";
							$scope.dataHide = true;
						}else{
							$scope.ingections = "Advance";
							$scope.dataHide = false;
						}
						$rootScope.instanceSaved = true;
						$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
						$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion);
						
						$scope.$emit("shareDataSourceToAllController", $scope.datasource);
					});
				}, 
				function(response) {
					if(response){
						logger.logError(response);	
					}else{
						logger.logError("Something went wrong");
					}
				});
			}, 
			function(response) {
				logger.logError("Something went wrong");
			});		
			
		}
		
		
		$scope.disbaled = function(){
			if(($scope.activeDatasources!=undefined &&  $scope.activeDatasources.length>0) || 
			($scope.historyDatasources!=undefined && $scope.historyDatasources.length>0 )){
				$scope.restrict = false;
			}else{
				$scope.restrict = true;
			}
		}
				
		
	}]);
	 	
}).call(this);
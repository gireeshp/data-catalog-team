(function(){
	"user strict";
	var dsModule = angular.module("app.datasourceinfo" , ['app.relations','app.datasource']);

	dsModule.filter("convertToBooleanToYesNo",function(){
		return function(type){
			if (type === undefined || type === null 
					|| type === "false" || type === false || type === "") {
				return "No";
			} else if (type === "true" || type === true) {
				return "Yes";
			}
		};
	});
	
	dsModule.factory('DataSourceFactory', function(){
		  return {
		    data: {
		      indicator : true
		    },
		    update: function(indicator) {
		      // Improve this method as needed
		      this.data.indicator = indicator;
		    }
		  };
	});
	
	
	dsModule.controller("mainDataSourceController",["DataSourceFactory","$anchorScroll","$route", "DataSourceService","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "$loading", function(DataSourceFactory, $anchorScroll,$route,DataSourceService,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $loading){
		
		$scope.hiddenIndicatorForRdbms = DataSourceFactory.data;
		var myParam = angular.copy($routeParams);
		$scope.selected  = $routeParams.param1,
		$scope.modelCheck  = "popUp",
		$scope.appId = $routeParams.param10;
		$scope.selectedFields = [],
		$scope.checkDataType;
		$scope.validationChkBox = [];
		$scope.transformationChkBox = [];
		$scope.distinctProfilePreChkBox = [];
		$scope.distinctProfilePostChkBox = [];
		$scope.distinctPreValueFreqChkBox = [];
		$scope.distinctPostValueFreqChkBox = [];
		$scope.appId = $routeParams.param10;
		$scope.appName = $routeParams.param20;
		$scope.decideDesabled = 0;
		
		$scope.list = [];
		$scope.hbase = {
				primary : null,
				secondary : null
		}
		
		var dataSourceId = "";
		if($scope.datasource && $scope.datasource.fileDataIngesterDetails){
			dataSourceId = $scope.datasource.fileDataIngesterDetails.dataSourceId;
		}else{
			dataSourceId=$routeParams.param7;
		}
		
		$scope.disableCheck = {preMyIndicator : false,postMyIndicator:false};
		
		DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
    	  	$scope.datasource = data;
    	  	if($scope.datasource.ingection === "quick"){
					$scope.ingections = "Basic";
					$scope.dataHide = true;
				}else{
					$scope.ingections = "Advance";
					$scope.dataHide = false;
				}
    	  	$scope.disableCheck.preMyIndicator = angular.copy(data.preIngestion);
			$scope.disableCheck.postMyIndicator = angular.copy(data.postIngestion);
			
			
			
			
			//This is for fetching data in HDFS and HBASE
			if($scope.datasource.fileStoreObject == undefined || $scope.datasource.fileStoreObject == null){ 
				$scope.datasource.fileStoreObject = "HDFS";
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
			}else if($scope.datasource != undefined && $scope.datasource != null && $scope.datasource.secondaryKeys == null){
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
			}
			var isHbaseIndicator = false;
			if($scope.datasource.fileStoreObject == "HDFS"){
				isHbaseIndicator = false;
			}else if($scope.datasource.fileStoreObject == "HBASE"){
				isHbaseIndicator = true;
			}
			
			var details = {"query" : "select * from " + $scope.datasource.dataSourceName + " limit 10", "isHbase" : isHbaseIndicator, "jobName" : "sampleRecordQuery"};
			
			$scope.sampleData = [];
				$http({
					method : "POST",
					url : "/runHiveQuery",
					data : details
				}).success(function(data, status, headers, config){
					if(data !== undefined && data !== null && data !== "" ){
						data.splice(0, 1)
						$scope.sampleData = data;
					}
				}).error(function(data, status, headers, config){
					$scope.sampleData = [];
				});
			
    	  	$scope.$emit("shareDataSourceToAllController", $scope.datasource);

    	  	
    	  	$scope.showDependentFlow(dataSourceId);
		});
		
		$scope.showDependentFlow = function(dataSourceId){
			$http({
				method: "Post",
				url: "/getDependentFlowForDataSourceByComponentId",
				data : dataSourceId
			}).success(function(data){
				$scope.dependentFlows = {"flows" : data};
				$http({
					method : "POST",
					url : "/getFlowDetails",
					data : $scope.dependentFlows 
				}).success(function(data){
					$scope.flowDetails = data;
				}).error(function(data){
					logger.logError(data);
				});
			}).error(function(data){
				logger.logError(data);
			});
		};
		
					
		$scope.delAppSettings = function(value , index){
			if(value == undefined){
				$scope.list[index] = undefined;
			}
		};
		
		$scope.saveAppSettings = function(){
			
			if($scope.appSetting.use == "default"){
				$scope.list = [];
			}
			
			
			var applicationData  = [{
				type : "DS",
				globalParamName : "",
				componantId : $scope.datasource.id,
				appId : $scope.appId,
				globalParamValue : ""
			}];
			
			if($scope.appSetting.globalParam != undefined && $scope.appSetting.use !== "default"){
				
				var BasePath = {
					type : "DS",
					globalParamName : "BasePath",
					componantId : $scope.datasource.id,
					appId : $scope.appId,
					globalParamValue : $scope.appSetting.globalParam.name
				};
				applicationData.push(BasePath);
			}
			
			
			if($scope.list.length > 0){
				var i=0;
				_.each($scope.list , function(val){
					if(val !== undefined){
						var appData = {
								type : "DS",
								globalParamName : $scope.datasource.dsLevelParams[i].paramName,
								componantId : $scope.datasource.id,
								appId : $scope.appId,
								globalParamValue : val.name
						};
						applicationData.push(appData);
					}
					i++;
				});
			}
			
			
			if(applicationData.length == 1 && $scope.appSetting.use == "Refreshed"){
				var BasePath = {
						type : "DS",
						globalParamName : "Refreshed",
						componantId : $scope.datasource.id,
						appId : $scope.appId,
						globalParamValue : "true"
					};
				applicationData.push(BasePath);
			}
			
			$http({
				method : "post",
				url : "/saveAppGlobal",
				data : applicationData
			}).success(function(data){
				logger.logSuccess("Saved successfully");
			}).error(function(status){
				logger.logError(status);
			});
		};
		
		$scope.nextPrevous = function(decideWhich){
			$scope.$broadcast("nextPrevous" , decideWhich);
			$scope.gotoAnchor("top");
		},
		
		$scope.gotoAnchor = function(repoName) {
			var id = $location.hash();
		    $location.hash(repoName);
		    $anchorScroll();
		    $location.hash(id);
	    };
		$scope.saveDataIngestion = function(){
			$scope.datasource.availableToUse = undefined;
			if(backUp !== $scope.datasource.ingection){
				if($scope.datasource.ingection  === "quick"){
					var modalInstance;
						modalInstance = $modal.open({ 
						templateUrl: "/views/template/saveAs.html",
						controller: "saveAsController",
						resolve : {	
						list:function(){
							return [];
						},
						messg : function(){
							return "The Transformation, Data-Profiling and Validations will be lost.";
						}, 
						ind : function(){
							return false;
						},
						headerTitle : function(){
							return "";
						},
						yesBtnLable : function() {
							return "Yes";
						},
						noBtnLable : function() {
							return "No";
						}
					}
				});
				modalInstance.result.then(function (param) {
						if(param === "yes"){
							$scope.datasource.advancedValidations = [];
							$scope.datasource.featureExtraction = [];
							$scope.datasource.advancedValidationsStore = [];
							$scope.datasource.transformations = [];
							$scope.datasource.dataCleansers = [];
							$scope.datasource.fieldMappingsStore = [];
							$scope.datasource.preIngestion = false;
							_.each($scope.datasource.fieldMappings , function (val){
								val.dataValidations = null;
								val.preIngestion = false;
								val.preValueFrequency = false;
							});
						
						
							if($scope.fileDataIngesterDetails != null){
								$scope.datasource.fileDataIngesterDetails.containsHeader = false+"";
								$scope.datasource.fileDataIngesterDetails.headerStarting = "";
							}
							$scope.datasource.availableToUse=undefined;
						}
					});
				}else{
					$scope.datasource.availableToUse=undefined;
				}
			}else{
				logger.logError("Please change data ingestion method");
				return false;
			}
		};
		
		if($routeParams.param6 === 1){
			$scope.myTbHide = false;
			$scope.myTbHide1 = false;
			$routeParams.param6++;
		}else{
			$scope.myTbHide = true;
		};
		
		if($routeParams.param6 === undefined){
			$scope.myTbHide1 = true;
		}
		
		$scope.check = [];
		
		
		$scope.$on("hourEvents", function (event, args) {
			$scope.explanation = args;
		});
		
		$scope.$on("shareDataSourceToAllController", function (event, args) {
			$scope.datasource = args;
			$scope.selectRejectValueForPrimary[0] = $scope.datasource.fieldMappings;
			$scope.selectRejectValueForSecondary[0] = $scope.datasource.fieldMappings;
			$scope.fetchFieldMappingForHbase();
			backUp = $scope.datasource.ingection;
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.dataHide = false;
			}
			
			var count = -1;
			
			_.each($scope.datasource.fieldMappings, function(key){
				count++;
				
				if(key.transformation !== null && key.transformation.length > 0){
					$scope.transformationChkBox[count] = true;
				}else{
					$scope.transformationChkBox[count] = false;
				}
				if(key.dataValidations !== null && key.dataValidations.length > 0){
					$scope.validationChkBox[count] = true;
				}else{					
					$scope.validationChkBox[count] = false;
				}				
				if(key.preIngestion !== null && key.preIngestion !== false){
					$scope.distinctProfilePreChkBox[count] = true;
				}else{
					$scope.distinctProfilePreChkBox[count] = false;
				}
				if(key.postIngestion !== null && key.postIngestion !== false){
					$scope.distinctProfilePostChkBox[count] = true;
				}else{
					$scope.distinctProfilePostChkBox[count] = false;
				}
				if(key.preValueFrequency !== null && key.preValueFrequency !== false){
					$scope.distinctPreValueFreqChkBox[count] = true;
				}else{
					$scope.distinctPreValueFreqChkBox[count] = false;
				}
				if(key.posValueFrequency !== null && key.posValueFrequency !== false){
					$scope.distinctPostValueFreqChkBox[count] = true;
				}else{
					$scope.distinctPostValueFreqChkBox[count] = false;
				}
				
			});
			
			$scope.disableCheck.preMyIndicator = angular.copy($scope.datasource.preIngestion);
			$scope.disableCheck.postMyIndicator = angular.copy($scope.datasource.postIngestion);
			if($scope.datasource.fileStoreObject === "HDFS"){

				if($scope.datasource.dataAtRestFileType === undefined){
					logger.logError("Please Select File Type.");
					return false;
				}
				
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
				
			}else {
				if(undefined != $scope.datasource.primaryKeysForIndex 
						&& $scope.datasource.primaryKeysForIndex.length === 0){
					logger.logError("Please add primary key");
					return false;
				}
				
				$scope.datasource.dataAtRestFileType = null;
				$scope.datasource.dataAtRestCompressionType = null;
			}
			$scope.$broadcast("myEventDataSourceInfo",$scope.datasource);
		});
		
		$scope.$on("myEvent1", function (event, args) {
			   $scope.activeTab = args;
		});
		$scope.$on("myEvent", function (event, args) {
			   $scope.checkDataType = args;
		});
		
		$scope.dismissModel = function(){
			$rootScope.isDataSourceInfoSaved = true;
			$routeParams.param3 = undefined;
			$routeParams.param1 = undefined;
			$scope.modelCheck = undefined;
			$routeParams.param6 = undefined;
			$routeParams.param4.close("");
		}; 
		$scope.AddFieldMapping = function(){
			if($scope.datasource.appId == null || $scope.datasource.appId == undefined || $scope.datasource.appId == ""){
				$scope.datasource.appId = 0;
				$scope.datasource.nodeId = 0;
			} 
		};
		
		/**
		 * service to get datasource based on data source name url :
		 * /getDataSource?datasourcename= param : $scope.selected
		 * 
		 */
		$scope.ingections = "";
		$scope.popUpValue = false; 
			
		var backUp;
		
		$scope.fetchFieldMappingForHbase = function(){
			if($scope.datasource.primaryKeysForIndex != null && $scope.datasource.primaryKeysForIndex.length > 0){
				var i = 1;
				_.each($scope.datasource.primaryKeysForIndex, function(value){
					$scope.selectRejectValueForPrimary[i] = _.reject($scope.selectRejectValueForPrimary[i - 1], function(val){
						return val.fieldName === value.fieldName; 
					})
					i++;
				});
			}
			if($scope.datasource.secondaryKeys != null && $scope.datasource.secondaryKeys.length > 0){
				var i = 1;
				_.each($scope.datasource.secondaryKeys, function(value){
					$scope.selectRejectValueForSecondary[i] = _.reject($scope.selectRejectValueForSecondary[i - 1], function(val){
						return val.fieldName === value.fieldName; 
					})
					i++;
				});
			}
		};
			
		$scope.categories = ["BANKING", "INSURANCE", "RETAIL", "MANUFACTURING", "IOT", "ENGERGY", "CROSS FUNCTIONAL", "SEARCH-DRIVEN ANALYTICS"]
		
		$scope.changeCategory = function(index , category){
		};
		
		$scope.changeSubCategory = function(index , subCategory){
		};

		/**
		 * Get Data source Instance
		 */
		
		
		$scope.activateFormat = function(index , fieldDataType){
			
			if(fieldDataType !== 'DATE'){
				$scope.check[index] = true;
			}else{
				$scope.check[index] = false;
			}
		};
		
		
		DataSourceService.getDataSource("/getDataSourceInstance" ,$scope.selected ).then(function(data){
			$scope.dataSourceInstance  = data;
			//To check status of datasource is requested or accepted
			$scope.GetRequestStatusOfDatasource($scope.dataSourceInstance);
			
			$scope.RepoName = $location.search().dataRepoName;
			
			if($scope.dataSourceInstance.totalRecords === 0){
				$scope.checked = false;
			}
			else{
				$scope.checked = true;
			}
		});
		
		$scope.$on("maxiqCommons", function (event, args) {
			$scope.maxiqCommonsAdv = args;
			$scope.maxiqCommonsAdvUnique = args;
			$scope.maxiqCommonsAdvUnique = _.uniq(args,function(obj){
				return obj.keyOf; 
			});
			$scope.maxiqCommonsAdv = _.uniq(args,function(obj){
				return obj.value;
			});
		});
		
		$scope.selectRejectValueForPrimary = [];
				
		$scope.addPrimaryKeyValue = function(primaryKey, index){
			
			$scope.selectRejectValueForPrimary[index+1] = _.reject($scope.selectRejectValueForPrimary[index] , function(data){
				return data.fieldName == primaryKey.fieldName;
			});
			
			if(primaryKey == undefined){
				logger.logError("Please select Primary key and add it.");
				return false;
			}
			$scope.datasource.primaryKeysForIndex.push(primaryKey);
			$scope.hbase.primary = null; 
		};
		
		$scope.deletePrimaryValue = function(index){
			var object = _.find($scope.selectRejectValueForPrimary[0] , function(values){
				return $scope.datasource.primaryKeysForIndex[index].fieldName == values.fieldName; 
			});
			
			var length = $scope.selectRejectValueForPrimary.length;
			
			for(var i = index ; i < length-1 ; i++){
				$scope.selectRejectValueForPrimary[i] = angular.copy($scope.selectRejectValueForPrimary[i+1]);
				$scope.selectRejectValueForPrimary[i].push(object);
			}
			$scope.selectRejectValueForPrimary.pop();
			$scope.datasource.primaryKeysForIndex.splice(index, 1);
		};
		$scope.selectRejectValueForSecondary = [];
		
		$scope.addSecondaryKeyValue = function(secondaryKey, index){
			
			$scope.selectRejectValueForSecondary[index+1] = _.reject($scope.selectRejectValueForSecondary[index] , function(data){
				return data.fieldName == secondaryKey.fieldName;
			});
			
			if(secondaryKey == undefined){
				logger.logError("Please select Secondary key and add it.");
				return false;
			}
			
			$scope.datasource.secondaryKeys.push(secondaryKey);
			$scope.hbase.secondary = null; 
		};
		$scope.deleteSecondaryValue = function(index){
			var object = _.find($scope.selectRejectValueForSecondary[0] , function(values){
				return $scope.datasource.secondaryKeys[index].fieldName == values.fieldName; 
			});
			
			var length = $scope.selectRejectValueForSecondary.length;
			
			for(var i = index ; i < length-1 ; i++){
				$scope.selectRejectValueForSecondary[i] = angular.copy($scope.selectRejectValueForSecondary[i+1]);
				$scope.selectRejectValueForSecondary[i].push(object);
			}
			$scope.selectRejectValueForSecondary.pop();
			$scope.datasource.secondaryKeys.splice(index, 1);
		};
		
		$scope.saveRestFile = function() {
			$scope.datasource.availableToUse = undefined;
			if($scope.datasource.fileStoreObject === "HDFS"){

				if($scope.datasource.dataAtRestFileType === undefined){
					logger.logError("Please Select File Type.");
					return false;
				}
				
				$scope.datasource.primaryKeysForIndex = [];
				$scope.datasource.secondaryKeys = [];
				
			}else {
				if($scope.datasource.primaryKeysForIndex.length === 0){
					logger.logError("Please add primary key");
					return false;
				}
				
				$scope.datasource.dataAtRestFileType = null;
				$scope.datasource.dataAtRestCompressionType = null;
			}
			$scope.datasource.availableToUse=undefined;
		}
		
		$scope.actInactAll = function(type , value){
			var count = 0;
			if(type.toUpperCase() == "PRE"){
				if(value == false)
				_.each($scope.datasource.fieldMappings , function(values ){
					
					if(values.fieldDataType.toUpperCase() != 'STRING' && values.fieldDataType.toUpperCase() != 'DATE'){
						$scope.distinctProfilePreChkBox[count] = value;
						$scope.datasource.fieldMappings[count].preIngestion = value;
					}
					$scope.distinctPreValueFreqChkBox[count] = value;
					$scope.datasource.fieldMappings[count].preValueFrequency = value;
					count++;
				});
			}else{
				if(value == false)
				_.each($scope.datasource.fieldMappings , function(values){
					
					if(values.fieldDataType.toUpperCase() != 'STRING' && values.fieldDataType.toUpperCase() != 'DATE'){
						$scope.distinctProfilePostChkBox[count] = value;
						$scope.datasource.fieldMappings[count].postIngestion = value;
					}
					$scope.distinctPostValueFreqChkBox[count] = value;
					$scope.datasource.fieldMappings[count].posValueFrequency = value;
					count++;
				});
			}
		},
		
		$scope.openTransformation = function(value){
			
			if($scope.popUpValue === true){
				return false;
			}
			
			if(_.isUndefined(value.fieldName)){
				logger.logError("Please check the Field Name");
				return;
			}
			
			var modalInstance;
			modalInstance = $modal.open({
	            templateUrl: "/views/template/advancedTransformationModal.html",
	            controller: "createNewAdvancedTransformationModalCntr",
	            resolve:{
	            	length : function(){
	            		if(value.transformation != null &&  value.transformation != undefined)
	            			return value.transformation.length;
	            		else
	            			return 0;
	            	},
	            	existingDataSource : function(){
	            		return $scope.datasource;
	            	},
	            	fieldMappings : function(){
            			return  value;
            		}
	            }
		     }),modalInstance.result.then(function() {
	         }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	         });
		},
		
		$scope.injunctionChange = function(type , index , value){
			if(type.toUpperCase() == "PRE")
				$scope.datasource.fieldMappings[index].preIngestion = value;
			else
				$scope.datasource.fieldMappings[index].postIngestion = value;
		},
		$scope.inValueFreqencyChange = function(type , index , value){
			if(type.toUpperCase() == "PRE")
				$scope.datasource.fieldMappings[index].preValueFrequency = value;
			else
				$scope.datasource.fieldMappings[index].posValueFrequency = value;
		},

		$scope.saveFieldMapping = function(fm){
			
			var bool = true;
			var bool1 = true ;
			var bool2 = true ;
			var i = 1;
			angular.forEach(fm,function(val){
				if(val.fieldName === null || val.fieldName === ""){
					bool = false;
					return false; 
				}else if(val.fieldName === undefined){
					bool2 = false;
					return false;
				}
				if(bool !== false && bool2 !== false)
					i++;
			});
			
			if(bool === false){
				logger.logError("All Field Names are mandatory");
				return false;
			}else if(bool2 === false){
				logger.logError("Invalid Field Name found at "+i);
				return false;
			}
			var bool2 = true;
			angular.forEach(fm,function(val){
				if(val.fieldName.indexOf(" ") !== -1){
					bool1 = false;
					return false; 
				}else if(val.fieldName.substring(0 , 3).toLowerCase() === "gp_"){
					bool2 = false;
					return false;
				}
			});
			
			if(bool1 === false){
				logger.logError("Spaces are not allowed in Field Name");
				return false;
			}else if(bool2 === false){
				logger.logError("Field Name starts with gp_ OR GP_ which is invalid");
				return false;
			};
			if($scope.datasource.nodeId !== null){
			}else{
				if(null == $scope.datasource.appId || undefined == $scope.datasource.appId){
					$scope.impactedApps();
				}else{
					$scope.saveAll();
				}
				
			};
		},
		
		$scope.validationTypes = ["NOT_NULL","LENGTH_CHECK"];
		$scope.open = function(fieldMapping){

			if($scope.popUpValue === true){
				return false;
			}
			if(_.isUndefined(fieldMapping.fieldName)){
				logger.logError("Please check the Field Name");
				return;
			}
			var modalInstance;
			
			modalInstance = $modal.open({
	            templateUrl: "/views/template/advancedValidationModal.html",
	            controller: "createValidationModalCntr",
	            resolve: {
	            	
	            	length : function(){
	            		if(fieldMapping.dataValidations != null && fieldMapping.dataValidations != undefined)
	            			return fieldMapping.dataValidations.length;
	            		else
	            			return 0;
	            	},
	            	validationType : function(){
	            		return $scope.validationTypes;
	            	},
	            	fieldMappings : function(){
            			return  fieldMapping;
            		}
	            }
		     }),modalInstance.result.then(function(returnValue) {
	         }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	         });
		};
		
		$scope.saveForVisualize = function(){
			$scope.datasource.availableToUse = undefined;
			
			$scope.indexDetails= {indexName : $scope.datasource.dataSourceName };
		};
		
		$scope.$on("myEventDataSourceInfo", function (event, args) {
			$scope.datasource = angular.copy(args);
			$scope.datasource_bk = angular.copy(args);
			$scope.fileDataIngesterDetails = $scope.datasource.fileDataIngesterDetails;
			
			
			if($scope.fileDataIngesterDetails == null || $scope.fileDataIngesterDetails == undefined){
				$scope.fileDataIngesterDetails = {
						"availableToUse" : true,
						"containsHeader" :  false+"",
						"dataSourceType" :  "",  
						"delimiter": "",
						"headerStarting" : "1",
						"headerTemplate" : "",
						"isDelimiterRegEx": null,
						"recordLoadLimit" : "",
						"recordStartLine" : "",
						"recordStartsWith" : false+"",
						"recordsIgnore" : "",
						"serverFilePath" :   "",  
						"startingRegularExpre" : "",
						"optionallyEnclosedInDoubleQuotes" : false+""
				};
			}
			
			if($scope.datasource.ingection === "quick"){
				$scope.ingections = "Basic";
				$scope.dataHide = true;
			}else{
				$scope.ingections = "Advance";
				$scope.fileDataIngesterDetails.recordStartsWith = false+"";
				$scope.dataHide = false;
			}
			
			
			if($scope.fileDataIngesterDetails != null && $scope.fileDataIngesterDetails.recordStartsWith !== null){
				$scope.fileDataIngesterDetails.recordStartsWith = $scope.fileDataIngesterDetails.recordStartsWith+"";
			};		
			
			
			if($scope.datasource !== null && 
					$scope.datasource.dataSourceType === "RDBMS_DATA_SOURCE"){
				
				$scope.fileDataIngesterDetails = {
						"dataSourceType" : "RDBMS_DATA_SOURCE"
				};
			}
			
		});
		
		$scope.saveProfilingOption = function(){
			$scope.datasource.availableToUse = undefined;
		};
		
		
		//DataRequest For
		var RFA = "Request For Access";		
		var PENDING = "Pending";
		var REJECT = "Reject";
		var ACCEPT = "Accept";
		var REMOVE = "Remove";
		var ADDTOCOLLECTION = "Add To Collection"
		var REMOVE_FRM_COLL = "Remove From My Collection";
		
		$scope.requestForDatasource = function(dataSourceInstance, justification, dataSourceRequestLabel) {
			if(dataSourceRequestLabel==RFA){
				var modalInstance;
				modalInstance = $modal.open({
					size : "lg",
					templateUrl: "/views/template/dataSourceRequestAccessModal.html",
					controller: "dataSourceRequestAccessController",
					resolve : {
						dataSourceName : function(){
							return dataSourceInstance.dataSourceName;
						}
					}
				}),modalInstance.result.then(function(justification) {
					$http({
						method : "post",
						url : "/createRequestForDatasource?datasourceid="+dataSourceInstance.dataSourceId+"&justification="+justification
					}).success(function(data){
						if(data=="success"){
							logger.logSuccess("Request sent for permission.");	
							$scope.dataSourceRequestLabel = "Requested";
							$scope.dataSourceRequestDisable = true;
						}else{
							logger.logError("failed to save");
						}
					}).error(function(status){
						logger.logError(status);
					});	
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			}else if(dataSourceRequestLabel == ADDTOCOLLECTION){
				//Add to My Collection
				var reqDataObject = { objectId : dataSourceInstance.dataSourceId , objectType : DATA_SOURCE};
				$http({
					method : "post",
					url : "/addToMyCollection",
					data : reqDataObject
				}).success(function(data){
					if(data=="success"){
						logger.logSuccess("Added successfully");	
						$scope.dataSourceRequestLabel = REMOVE_FRM_COLL;
						$scope.dataSourceRequestDisable = false;
					}else{
						logger.logError("failed to save");
					}
				}).error(function(status){
					logger.logError(status);
				});
			}else if(dataSourceRequestLabel == REMOVE_FRM_COLL){
				//Remove from My Collection
				var reqDataObject = { objectId : dataSourceInstance.dataSourceId , objectType : DATA_SOURCE};
				$http({
					method : "post",
					url : "/removeFromMyCollection",
					data : reqDataObject
				}).success(function(data){
					if(data=="success"){
						logger.logSuccess("Removed successfully");	
						$scope.dataSourceRequestLabel = ADDTOCOLLECTION;
						$scope.dataSourceRequestDisable = false;
						$rootScope.removeFromCollection = true;
						$rootScope.refreshCollectionList = true;
					}else{
						logger.logError("failed to save");
					}
				}).error(function(status){
					logger.logError(status);
				});
			}
			
		};
		
		$scope.GetRequestStatusOfDatasource = function(dataSourceInstance) {
			//Get Status of DataSource request - pending, rejected, approved
			var reqDataObject = {objectId : dataSourceInstance.dataSourceId, objectType : "DATA_SOURCE"};
			$http({
				method : "post",
				url : "/getCollectionAndRequestStatusOfDatasource",
				data : reqDataObject
			}).success(function(data){
				if(data == PENDING){
					$scope.dataSourceRequestLabel = "Requested";
					$scope.dataSourceRequestDisable = true;
				}else if(data == REJECT){
					$scope.dataSourceRequestLabel = "Rejected";
					$scope.dataSourceRequestDisable = true;
				}else if(data == ACCEPT){
					$scope.dataSourceRequestLabel = ADDTOCOLLECTION;
					$scope.dataSourceRequestDisable = false;
				}else if(data == REMOVE){
					$scope.dataSourceRequestLabel = REMOVE_FRM_COLL
				}else{
					$scope.dataSourceRequestLabel = RFA;
				}
								
			}).error(function(status){
				logger.logError(status);
			});
		};
		
			
	}]).controller("TabsCtroller", ["$route", "$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", function($route, $scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger){
			$scope.tabs = [{
				title: 'Overview',
				active : true
			}, {
				title: 'Fields',
				active : false
			}, {
				title: 'Data',
				active : false
			}, {
				title: 'Feedback',
				active : false
			}, {
				title: 'Dependencies',
				active : false
			},{
				title: 'Relationships',
				active : false
			}];
		
			if($routeParams.param6 === 1)
				$scope.currentTab = '/views/DataSource/dataSource.jsp';

			$scope.onClickTab = function (tab) {
				$scope.$emit("myEvent1", tab.title);
			};
    
			$scope.isActiveTab = function(tabUrl) {
				return tabUrl == $scope.currentTab;
			};
			
			$scope.$on("nextPrevous" , function(event , args){
				var tabSelect= null;
				if(args === "next"){
					for(var i = 0 ; i < $scope.tabs.length; i++){
						if($scope.tabs[i].active == true){
							$scope.tabs[i].active = false;
							if(i+1 == $scope.tabs.length){
								$scope.tabs[0].active = true;
								tabSelect = $scope.tabs[0]; 
							}
							else{
								$scope.tabs[i+1].active = true;
								tabSelect = $scope.tabs[i+1];
							}
							break;
						}
					}
				}else{
					for(var i = 0 ; i < $scope.tabs.length; i++){
						if($scope.tabs[i].active == true){
							$scope.tabs[i].active = false;
							if(i-1 == -1){
								$scope.tabs[$scope.tabs.length - 1].active = true;
								tabSelect = $scope.tabs[$scope.tabs.length - 1];
							}
							else{
								$scope.tabs[i - 1].active = true;
								tabSelect = $scope.tabs[i - 1];
							}
							break;
						}
					}
				} 
				$scope.onClickTab(tabSelect);
			});
			
			$scope.$emit("myEvent1", "Overview");
			
	}]).controller("feedbackCtrl",["$scope","$routeParams","$http","logger","$modal","$log","$interval","$rootScope", "DataSourceService",function($scope,$routeParams,$http,logger,$modal,$log,$interval,$rootScope, DataSourceService){
			$scope.obj = {
				question: '',
				answer: ''
			};
			
			$scope.star = {
				starOne: 0,
				starTwo: 0,
				starThree: 0,
				starFour: 0,
				starFive: 0
			};
			
			$scope.init = function(){
				
				$http({
					method: "POST",
					url: "/getAvgRatingForDataSource",
					data : $scope.datasource.id
				}).success(function(data){
					$scope.userDetailsForAverageRating = data;
					$scope.showRating(data);
				}).error(function(data){
					logger.logError(data);
				});
			};
			
			$scope.showRating = function(data){
				for(var i = 0; i< data.length; i++){
					if(data[i].rating == 1){
						$("#starOne").addClass("checked");
						
						$scope.star = {
								starOne: 1,
								starTwo: 0,
								starThree: 0,
								starFour: 0,
								starFive: 0
						};
						
						$("#starTwo").removeClass("checked");
						$("#starThree").removeClass("checked");
						$("#starFour").removeClass("checked");
						$("#starFive").removeClass("checked");
					}else if(data[i].rating == 2){
						$("#starOne").addClass("checked");
						$("#starTwo").addClass("checked");
						
						$scope.star = {
								starOne: 1,
								starTwo: 1,
								starThree: 0,
								starFour: 0,
								starFive: 0
						};
						
						$("#starThree").removeClass("checked");
						$("#starFour").removeClass("checked");
						$("#starFive").removeClass("checked");
					}else if(data[i].rating == 3){
						$("#starOne").addClass("checked");
						$("#starTwo").addClass("checked");
						$("#starThree").addClass("checked");
						
						$scope.star = {
								starOne: 1,
								starTwo: 1,
								starThree: 1,
								starFour: 0,
								starFive: 0
						};
						
						$("#starFour").removeClass("checked");
						$("#starFive").removeClass("checked");
					}else if(data[i].rating == 4){
						$("#starOne").addClass("checked");
						$("#starTwo").addClass("checked");
						$("#starThree").addClass("checked");
						$("#starFour").addClass("checked");
						
						$scope.star = {
								starOne: 1,
								starTwo: 1,
								starThree: 1,
								starFour: 1,
								starFive: 0
						};
						
						$("#starFive").removeClass("checked");
					}else if(data[i].rating == 5){
						$("#starOne").addClass("checked");
						$("#starTwo").addClass("checked");
						$("#starThree").addClass("checked");
						$("#starFour").addClass("checked");
						$("#starFive").addClass("checked");
						$scope.star = {
								starOne: 1,
								starTwo: 1,
								starThree: 1,
								starFour: 1,
								starFive: 1
						};
					}
				}
			}
			
			$scope.checkRating = function(value){
				if(value == "starOne"){
					$("#starTwo").removeClass("checked");
					$("#starThree").removeClass("checked");
					$("#starFour").removeClass("checked");
					$("#starFive").removeClass("checked");
					$scope.star = {
							starOne: 1,
							starTwo: 0,
							starThree: 0,
							starFour: 0,
							starFive: 0
					};
				}else if(value == "starTwo"){
					$("#starThree").removeClass("checked");
					$("#starFour").removeClass("checked");
					$("#starFive").removeClass("checked");
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 0,
							starFour: 0,
							starFive: 0
					};
				}else if(value == "starThree"){
					$("#starFour").removeClass("checked");
					$("#starFive").removeClass("checked");
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 1,
							starFour: 0,
							starFive: 0
					};
				}else if(value == "starFour"){
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 1,
							starFour: 1,
							starFive: 0
					};
					$("#starFive").removeClass("checked");
				} else {
					$scope.star = {
							starOne: 1,
							starTwo: 1,
							starThree: 1,
							starFour: 1,
							starFive: 1
					};
				}
			};
			
			$scope.showAvgRating = function(){
				$http({
					method: "POST",
					url: "/getEachUserRatingForDataSource",
					data : $scope.datasource.id
				}).success(function(data){
					var modalInstance;
					modalInstance = $modal.open({
			            templateUrl: "/views/template/showAvgRatingForDataSource.html",
			            controller: "showAvgRatingCtrl",
			            resolve:{
			            	showUserDetails : function(){
			            		return data;
			            	},
			            	dataSourceName : function(){
			            		return $routeParams.param1;
			            	}
			            }
				    }),modalInstance.result.then(function() {
			        }, function() {
			        	$log.info("Modal dismissed at: " + new Date);
			        });
				}).error(function(data){
					logger.logError(data);
				});
			};
			
			$scope.submitRating = function(value){
				$scope.isDisabled = false;
				var sum = 0;
				if(parseInt(value.starFive) > 0) {
					sum = 5;
				} else if (parseInt(value.starFour) > 0){
					sum = 4;
				} else if(parseInt(value.starThree) > 0) {
					sum = 3;
				} else if(parseInt(value.starTwo) > 0) {
					sum = 2;
				} else if(parseInt(value.starOne) > 0) {
					sum = 1;
				}
				
				var reqDataObject = {'dataSourceId': $scope.datasource.id,'rating':sum};
				$http({
					method: "POST",
					url: "/updateDataSourceRating",
					data: reqDataObject
				}).success(function(data){
					logger.logSuccess("Rating added successfully");
					$scope.init();
				}).error(function(data){
				});
				
			};
		
			$scope.fetchFeedback = function(){
				$http({
					method: "POST",
					url:"/fetchDataSourceFeedback",
					data : $scope.datasource.id
				}).success(function(data){
					$scope.feedbackObj = data;
				}).error(function(data){
				});
			};
			
			DataSourceService.getDataSource("/getDataSource" ,$scope.selected ).then(function(data){
				$scope.datasource = data;
				if($scope.datasource.dataSourceLoadStrategy == null){
		    		  $scope.dataSourceLoadStrategy = {
		    			  loadStategyType : "REPLACEALL" ,
		    			  updateLoadStategy : null
		    		 };
		    	} else {
		    		$scope.dataSourceLoadStrategy = angular.copy($scope.datasource.dataSourceLoadStrategy);
		    	}
				
				if($scope.datasource.ingection === "quick"){
						$scope.ingections = "Basic";
						$scope.dataHide = true;
		    	  }else{
						$scope.ingections = "Advance";
						$scope.dataHide = false;
		    	  }
				
				if($scope.datasource.dataSourceType !== "RDBMS_DATA_SOURCE"){
					$scope.types = ["FILEPATH","DATASOURCE"];
		    	}else{
		    		  $scope.types = ["RDBMS","DATASOURCE"];
		    	}
				$scope.fetchFeedback();
				$scope.init();
			});
		
			
			$scope.updateVote = function(vote, $index, item){
				
				var vote = parseInt(vote);
				var reqDataObject = {'dataSourceId': item.dataSourceId,'vote':+vote,'feedbackId':item.id};
				$http({
					method: "POST",
					url: "/updateDataSourceVote",
					data : reqDataObject
				}).success(function(data){
					
					if(data=="alreadyVoted"){
						logger.logError("You already voted");	
					}
					
					if(data=="0"){
						$scope.feedbackObj[$index].votingCount = parseInt(item.votingCount);
						$scope.feedbackObj[$index].votingCount = parseInt(item.votingCount) + vote;
						$scope.feedbackObj[$index].selfVote = 0;		

						if(vote==1){
							$scope.feedbackObj[$index].votingNegativeCount = parseInt($scope.feedbackObj[$index].votingNegativeCount) + vote;	
						}
						
						if(vote==-1){
							$scope.feedbackObj[$index].votingPositiveCount = parseInt($scope.feedbackObj[$index].votingPositiveCount) + vote;	
						}
					}
					
					if(data=="1" || data=="-1"){						
						$scope.feedbackObj[$index].votingCount = parseInt(item.votingCount);
						$scope.feedbackObj[$index].votingCount = parseInt(item.votingCount) + vote;
						$scope.feedbackObj[$index].selfVote = vote;
						
						if(vote==1){
							$scope.feedbackObj[$index].votingPositiveCount = parseInt($scope.feedbackObj[$index].votingPositiveCount) + vote;	
						}
						
						if(vote==-1){
							$scope.feedbackObj[$index].votingNegativeCount = parseInt($scope.feedbackObj[$index].votingNegativeCount) + vote;	
						}
						logger.logSuccess("Vote submitted.");
					}
					
				}).error(function(data){
					logger.logError(data);					
				});
			};
				
			$scope.submitPost = function(obj){
				if(obj.question == null || obj.question == undefined || obj.question == ""){
					logger.logError("Please enter title");
				}else if(obj.answer == null || obj.answer == undefined || obj.answer == ""){
					logger.logError("Please enter description");
				}else {
					var reqDataObject = {'dataSourceId': $scope.datasource.id, 'sourceFeedback' : obj};
					$http({
						method: "POST",
						url: "/saveDataSourceFeedback",
						data: reqDataObject
					}).success(function(data){
						$scope.obj.question = '';
						$scope.obj.answer = '';
						$scope.fetchFeedback();
					}).error(function(data){
					});
				}
			};
			
			$scope.on = true;
			$scope.off = false;
			$scope.submitReviews = function(value){
				if(value == 1){
					$scope.off = true;
					$scope.on = false;
				}else if(value == 2){
					$scope.off = true;
					$scope.on = false;
				}else if(value == 3){
					$scope.off = true;
					$scope.on = false;
				}else if(value == 4){
					$scope.off = true;
					$scope.on = false;
				}else if(value == 5){
					$scope.off = true;
					$scope.on = false;
				}
			};
			
		}]).controller("showAvgRatingCtrl",["$scope","$routeParams","$http","logger","$modalInstance","showUserDetails","dataSourceName",function($scope,$routeParams,$http,logger,$modalInstance,showUserDetails,dataSourceName){
			$scope.userDetails = showUserDetails;
			$scope.dsName = dataSourceName;
			$scope.allRating = true;
			
			$scope.cancel = function(){
				$modalInstance.dismiss();
			};
		}]);
}).call(this);
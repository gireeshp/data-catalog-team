(function(){
	"use strict";
	
	var dataSourceTableView = angular.module("app.dataSourceTable",['ui.bootstrap','app.datasource']);
	
	dataSourceTableView.constant('CONSTANTS',{
		INPUT: 'INPUT',
		TIMESTAMP: 'TIMESTAMP',
		GP_LAST_RUN_DATETIME: 'gp_lastRunDateTime',
		GP_CURRENT_RUN_DATETIME: 'gp_currentRunDateTime',
		GP_LAST_SUCCESSFULLY_RUN_DATETIME: 'gp_lastSuccessfullyRunDateTime',
		GLOABL_DEFAULT_TIMESTAMP: 'dd/MM/yyyy HH:mm:ss'
	});
	
	dataSourceTableView.filter("typeChecker",function(){
		return function(type){	
			if (type === "")
				return bytes + " Bytes";
			else if (bytes < 1048576)
				return Math.round(bytes / 1024) + " KB";
			else if (bytes < 1073741824)
				return Math.round(bytes / 1048576) + " MB";
			else
				return Math.round(bytes / 1073741824) + " GB"; 
		};
	});
	
	dataSourceTableView.filter("contentType",function(){
		return function(content){		
			if (content === "")
				return "";
			else if (content === "FILE_DATA_SOURCE")
				return "DELIMITED FILE";
			else if (content === "RDBMS_DATA_SOURCE")
				return "RDBMS";
			else  if (content === "TSV_DATA_SOURCE")
				return "TSV";
			else  if (content === "CSV_DATA_SOURCE")
				return "CSV";
			else  if (content === "EXCEL_DATA_SOURCE")
				return "EXCEL SHEET";
			else  if (content === "EXTERNAL_DATA")
				return "EXTERNAL DATA";
			else  if (content === "FTP_DATA")
				return "FTP_DATA";
			else  if (content === "REMOTE_DATA")
				return "REMOTE_DATA";
			else  if (content === "STREAM_DS")
                return "STREAM_DS";
			else  if (content === "FIXED_LENGTH_FORMAT")
                return "FIXED";
			if (content === "")
				return "";
			else
				return "";
		};
	});
	
	dataSourceTableView.filter("GroupNameFromGroupId",function(){
		return function(content, groupMap){		
			return groupMap[content];
		};
	});
	
	dataSourceTableView.filter('unsafe', function($sce) {
		  return function(val) {
	      return $sce.trustAsHtml(val);
	   };
	});
	
	dataSourceTableView.service('anchorSmoothScroll', function(){
	    this.scrollTo = function(eID) {

	        // This scrolling function 
	        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
	        
	        var startY = currentYPosition();
	        var stopY = elmYPosition(eID);
	        
	        var distance = stopY > startY ? stopY - startY : startY - stopY;
	        if (distance < 100) {
	            scrollTo(0, stopY); return;
	        }
	        var speed = Math.round(distance / 100);
	        if (speed >= 20) speed = 20;
	        var step = Math.round(distance / 25);
	        var leapY = stopY > startY ? startY + step : startY - step;
	        var timer = 0;
	        if (stopY > startY) {
	            for ( var i=startY; i<stopY; i+=step ) {
	                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
	                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
	            } return;
	        }
	        for ( var i=startY; i>stopY; i-=step ) {
	            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
	            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
	        }
	        
	        function currentYPosition() {
	            // Firefox, Chrome, Opera, Safari
	            if (self.pageYOffset) return self.pageYOffset;
	            // Internet Explorer 6 - standards mode
	            if (document.documentElement && document.documentElement.scrollTop)
	                return document.documentElement.scrollTop;
	            // Internet Explorer 6, 7 and 8
	            if (document.body.scrollTop) return document.body.scrollTop;
	            return 0;
	        }
	        
	        function elmYPosition(eID) {
	            var elm = document.getElementById(eID);
	            var y = elm.offsetTop;
	            var node = elm;
	            while (node.offsetParent && node.offsetParent != document.body) {
	                node = node.offsetParent;
	                y += node.offsetTop;
	            } return y;
	        }

	    };
	    
	});
	
	dataSourceTableView.directive('newDataSource',function($compile, logger,$http){
	    return {
	    	templateUrl : '/views/template/createNewDataSource.html',
	    	link: function (scope, element, attrs) {
    	       scope.remove = function () {
    	    	   if(scope.flip && scope.gridActive){
    	    		   element[0].firstElementChild.firstElementChild.style.transform = 'rotateY(180deg)';
        	    	   element[0].firstElementChild.firstElementChild.firstElementChild.style.display = 'none';
        	    	   element[0].firstElementChild.firstElementChild.lastElementChild.style.display = 'block';
    	    	   }
    	    	   scope.isDisable = false;
    	    	   if(scope.flip == 0){
    	    		   element.remove();
    	    	   }
    	           scope.flip = 0;
    	       };
    	    },
    	    controller: "dataSourceModelController"
	    };
	});
	
	dataSourceTableView.directive('ltaEnter', function () {
		return function ($scope, element, attrs) {
		    element.bind("keydown keypress", function (event) {
		        if(event.which === 13) {
		          // Create closure with proper command
		          var fn = function(command) {
		            var cmd = command;
		            return function() {
		              $scope.$eval(cmd);
		            };
		          }(attrs.ltaEnter.replace('()', '("'+ event.target.value +'")' ));

		          // Apply function
		          $scope.$apply(fn);

		          event.preventDefault();
		        }
		    });
		};
	});

	dataSourceTableView.controller("dataSourceTableCtrl" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll", "$cookieStore", "$interval", "$compile", "$timeout", function($anchorScroll ,$filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,anchorSmoothScroll, $cookieStore, $interval,$compile, $timeout){
		$scope.projectAccessType = $cookieStore.get("projectAccessType");
		
		$scope.dsLists = [];
		$scope.appLists = [];
		$scope.appId = $routeParams.param1;
		$scope.appName = $routeParams.param2;
		$scope.explanation = $routeParams.param15;
		$scope.hideTbl = true;
		$scope.oneAtATime = false;
		$scope.ratingDisabled = true;
		$scope.messageChanger = $routeParams.appIndicator;
		$scope.isStreamFlow = false;
		$scope.groupMap = {};
		$http({
			method : 'POST',
			url : "/groupConfig/getGroupsData"
		}).success(function(data, status, headers, config) {
			$scope.groupDetails = data.GROUPS;
			
			_.each($scope.groupDetails, function(group){
				$scope.groupMap[group.groupId] = group.groupName;
			});
			
		}).error(function(data, status, headers, config) {
			return false;
		});
		
		if($routeParams.param15 === undefined){
			$http.post("/getDataSourcesMsg").success(function(data, status, headers, config){
				$scope.explanation = data;
			});
		}
		
		$interval(function() {
			$scope.currentGroupId = $cookieStore.get("groupId");
			if($rootScope.isDataSourceFileSaved){
				$scope.getDataSourceList(true);
			}
		}, 100);

		$scope.dismissModel = function (){
			$routeParams.param1 = undefined;
			$routeParams.param15 = undefined;
			$routeParams.param4.dismiss('');
			$routeParams.param4 = undefined;
		},
		$scope.open = function (){
			var isExists = document.getElementsByClassName("newProject_create");
			if(isExists && isExists.length == 0){
				var divElement = angular.element(document.getElementById('projectListDiv'));
				var appendHtml = $compile('<div new-data-source class="newProject_create"></div>')($scope);
				divElement.prepend(appendHtml);
			}
		};
	
		$scope.dsDecide = true;
		$scope.dsSource = function(){
			$scope.dsDecide = true;
		};
		
		$scope.$on("deleteAppDs" ,function (event, args) {
			$scope.deleteAppsLevelDs(args);
		});
		
		$scope.$on("myEvent2", function (event, args) {
			$scope.selectDSources(args[0] , args[1]);
		});
		
		$scope.$on("appEvent", function (event, args) {
			$scope.selectApps(args[0] , args[1]);
		});
		$scope.checkHeader = "existHeader";
		$scope.$on("myEvent1", function (event, args) {
			if(args == "Data sources created in flow" || args == "Stream sources created in flow"){
				$scope.dsDecide = false;
				$scope.checkHeader = "appLevelDSHeader";
			}
			else if(args == "Existing data sources"){
				$scope.dsDecide = true;
				$scope.checkHeader = "existHeader";
			}
			else if(args == "Inputs file(File parameters)"){
				$scope.dsDecide = null;
				$scope.checkHeader = "inputDs";
			}
			else if(args == "Node from other flows as head"){
				$scope.dsDecide = "App-nodes";
				$scope.checkHeader = "nodeHeader";
			}
			else if(args == "Existing flows"){
				$scope.dsDecide = "Apps";
				$scope.checkHeader = "importApp";
			}
			$scope.$broadcast("broadcastEvent" , $scope.dsDecide);
		});
		
		$scope.selectDSources = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.dataSourceId === dataSource.dataSourceId;});
			}
		};
		
		$scope.selectApps = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.appName === dataSource.appName;});
			}
			
		};
		
		$scope.selectNodes = function(node){
			for(var i=0; i<$scope.dsLists.length;i++){
				
				if($scope.dsLists[i].nodeId==node.nodeId){
					$scope.dsLists[i]=node;
					return;
				}
			}
			$scope.dsLists.push(node);
			
		};
		
		
		$scope.confirmDsource= function(){
			if($scope.dsDecide=="Apps"){
				$routeParams.param4.close($scope.dsLists);
			}else{

				$routeParams.param1 = undefined;
				$routeParams.param4.close($scope.dsLists);
			}
		};
		
		$scope.gotoAnchor = function(repoName) {
			var id = $location.hash();
		    $location.hash(repoName);
		    $anchorScroll();
		    $location.hash(id);
	     };
		
	     $scope.deleteAppsLevelDs = function(dataSource){
	    	 $scope.dsLists = _.reject($scope.dsLists , function(val){return dataSource.id === val.id;});
	    	 $scope.deleteDataSource(null , dataSource.dataSourceId);
	     };
	     
	     
	     $scope.globalJob = function(dataSoourceToRun){
	    	 
	    	 if(dataSoourceToRun.dataSourceType == null || dataSoourceToRun.dataSourceType == "null" || dataSoourceToRun.dataSourceType == undefined){
	    		 logger.logError('Datasource is empty');
	    		 return;
	    	 }
	    	 
	    	 $http.post("/getRepoInstance",dataSoourceToRun.dataRepoName).success(function(data, status, headers, config){
	    		 data.dataSourceIds = [];
	    		 data.dataSourceIds.push(dataSoourceToRun.dataSourceId);
	    		 var modalInstance;
					modalInstance = $modal.open({
						size : "lg",
						templateUrl: "/views/template/runPipeLineTemplate.html",
						controller: "dataSourceRunPipeLineController",
						resolve : {
							store : function(){
								return data;
							}
						}
					}),modalInstance.result.then(function(dsList) {
					}, function() {
						$log.info("Modal dismissed at: " + new Date);
				});
	    	 });
	     },
	     $scope.startJob = function(){
	    	 if($scope.repoName != undefined){
	    		 $http.post("/getRepoInstance",$scope.repoName).success(function(data, status, headers, config){
	    			 data.dataSourceIds = [];
	    			 var i = 0;
	    			 _.each($scope.checkBox , function(val){
	    				 if(val == true){
	    					 data.dataSourceIds.push($scope.dataSourceOfRepo[i].dataSourceId);
	    				 } 
	    				 i++;
	    			 });
	    			 if(data.dataSourceIds.length > 0){
	    				 var modalInstance;
	 						modalInstance = $modal.open({
	 							size : "lg",
	 							templateUrl: "/views/template/runPipeLineTemplate.html",
	 							controller: "dataSourceRunPipeLineController",
	 							resolve : {
	 								store : function(){
	 									return data;
	 								}
	 							}	
	 						}),modalInstance.result.then(function(dsList) {

	 						}, function() {
	 							$log.info("Modal dismissed at: " + new Date);
	 						});
	    		 		}else{
	    		 			logger.logError("Please select at least one Data-source");
	    		 		}
	    	 		});
	    	
	    	 	}
		};
		
		$scope.repoListClick = function(repoName){
			$location.path("/dataRepoOperations/"+repoName);
		};
		
		$scope.openDataProfiling = function(ds){
			if(ds.dataSourceType == null || ds.dataSourceType == "null" || ds.dataSourceType == undefined){
				return;
			}
			var reqDataObject = {'profileType' : 'PreProf', 'dsId' : ds.dataSourceId };
			$http({
				method: 'POST',
				url : "/getDataProfileJobInstances",
				data : reqDataObject
			}).
			success(function(data, status, headers, config){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl : "/views/template/DataProfileModal.html",
			        controller: "dataProfileController",
			        resolve : {
			        	 store : function(){
			        		return data; 
			        	 },
			        	 profileType : function(){
							return null; 
						 }
				    }
				}),modalInstance.result.then(function(reponame) {
		        }, function() {
		           	$log.info("Modal dismissed at: " + new Date);
		        });
			}).error(function(data){
				logger.logError(data);
			});
		};
		
		$scope.renameDataSource = function(dataSource , indicator){
			if(indicator == "dataSource"){
				var ds = angular.copy(dataSource);
				ds.nodeIndicate = undefined; 
				
				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/rename.html",
		            controller: "renameController",
		            	resolve : {
		            		checkSource : function(){
			            	 	return indicator;
			            	},
			            	data : function(){
			            	 	return ds;
			            	}
			            }
				}),modalInstance.result.then(function(dataSourceInstance) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == dataSourceInstance.dataSourceId){
								$scope.dataSourceCol[i] = dataSourceInstance;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								break;
							}
							
						}
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
		
			}else{
			}
		},
		
		$scope.check = function(value){
			var i = 0;
			_.each($scope.checkBox , function(val){
				$scope.checkBox[i] = value;
				i++;
			});
		};
		
		$scope.finalSet = function(val){
			if(val === false)
				$scope.finalValue = false;
			else{
				var bool = true;
				for(var i = 0 ; i < $scope.checkBox.length ; i++){
					if($scope.checkBox[i] == false){
						bool = false;
						break;
					}
					
				}
				if(bool == true)
					$scope.finalValue = bool;
			}
				
		};
		
		$scope.removeDSFromProject= function(index, dataSourceId,dataSourceName){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/RemoveModal.html",
				controller: "removeDSMoalCtrl"

			}),modalInstance.result.then(function() {

				if($scope.appId === undefined){
						if(index !== null){
							dataSourceId = angular.copy($scope.dataSourceCol[index].dataSourceId);
							$scope.dataSourceCol.splice(index , 1);
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							if($scope.dataSourceOfRepo !== undefined || $scope.dataSourceOfRepo !== null || $scope.dataSourceOfRepo.length > 0)
								$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
						if(dataSourceId !== null && index == null){
							$scope.dataSourceCol = _.reject(angular.copy($scope.dataSourceCol) , function(val){return val.dataSourceId === dataSourceId;});
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
					}
				
					//Remove from Project
					$http({
						method : "post",
						url : "/removeObjectFromProject?objectId="+dataSourceId+"&objectType=DATA_SOURCE"
					}).success(function(data){
						logger.logSuccess("Removed successfully");	
					}).error(function(status){
						logger.logError(status);
					});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};
		
		$scope.assignDataSource = function(dataSource){
			var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/assignDataSource.html",
		            controller: "assignController",
		            	resolve : {
			            	data : function(){
			            	 	return angular.copy(dataSource);
			            	}
			            }
				}),modalInstance.result.then(function(updatedDataSource) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == updatedDataSource.dataSourceId){
								$scope.dataSourceCol[i] = updatedDataSource;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								if($scope.dataSourceOfRepo[i].dataRepoId === $scope.dataSourceCol[index].dataRepoId)
									$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								else
									$scope.dataSourceOfRepo.splice(i , 1);
								
								break;
							}		
						}
					
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
			});
		
		},
		$scope.dataSourcePopup = function(dataSourceName, dsGroupId){
			if($scope.appId !== undefined)
				return false;
			
			if($scope.currentGroupId !== dsGroupId) {
				$routeParams.param10 = 0;
			}
			
			 $routeParams.param1 = dataSourceName;
			 $routeParams.param6 = 1;
			 $location.path("/datasource/"+$routeParams.param1);
		},
		$scope.deleteDataSource = function(index , dataSourceId , dataSourceName){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:dataSourceName, type: "datasource"};
					}
				}
			}),modalInstance.result.then(function() {

				if($scope.appId === undefined){
						if(index !== null){
							dataSourceId = angular.copy($scope.dataSourceCol[index].dataSourceId);
							$scope.dataSourceCol.splice(index , 1);
							$scope.filteredStores = _.reject($scope.dataSourceCol , function(val){return val.dataSourceId === dataSourceId;});
							
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							if($scope.dataSourceOfRepo !== undefined || $scope.dataSourceOfRepo !== null || $scope.dataSourceOfRepo.length > 0)
								$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
						if(dataSourceId !== null && index == null){
							$scope.dataSourceCol = _.reject(angular.copy($scope.dataSourceCol) , function(val){return val.dataSourceId === dataSourceId;});
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
					}
					$http({
						method: 'POST',
						url : "/deleteDataSource",
						data : dataSourceId
					}).
					success(function(data, status, headers, config){
						logger.logSuccess("Data-source deleted successfully");
						$scope.$broadcast("deleteLevelAppDs",dataSourceId);
					}).error(function(data){
						logger.logError(data);
					});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		};
		
		$scope.filteredStores = [];
		$scope.searchKeywords = "";
		$scope.numPerPageOpt = [3, 5, 8, 10, 15, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.pageNum = 1;
        $scope.initMethodForCount = function(){
	        $http.post("/getDataSourceCount?dataSourceName="+$scope.searchKeywords).success(function(data){
	        	$scope.filteredStores.length = parseInt(data);
			});
        }
       
		$scope.select = function(page) {
			$scope.pageNum = page;
			$scope.initMethodForCount();
			$scope.getDataSourceList();
			var end, start;
			return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.dataSourceCol = $scope.filteredStores.slice(start, end);
        };
        
        $scope.initMethodForCount();

        var timeToElaspse;
        $scope.onKeyPress = function(){
			if(timeToElaspse){$timeout.cancel(timeToElaspse);}
			timeToElaspse =  $timeout(function () {
				if($scope.searchKeywords != undefined && null != $scope.searchKeywords){
					$scope.select(1);
				}				 
			}, 500);
		};
		
		$scope.userList = false;
		$scope.backUpForDataSourceList = [];
		$scope.getDataSourceList = function (indicator) {
			$rootScope.isDataSourceFileSaved = false;
			if(!$scope.userList) {
				if(undefined === $scope.searchKeywords || null === $scope.searchKeywords  ) {
					$scope.searchKeywords = "";
				}
				$scope.reqData = {"numberOfRecordsPerPage" : $scope.numPerPage, "pageNumber" : $scope.pageNum, "searchContent" : $scope.searchKeywords};
				$http.post("/getDataSourceList", $scope.reqData).success(function(data){
					//$scope.dataSourceCol = _.filter(data, function(val){if(val !== null){return val.appId == null;}});
					$scope.dataSourceCol = data;
					$scope.dataSourceColBkp = angular.copy($scope.dataSourceCol);
					$scope.backUpForDataSourceList = $scope.dataSourceCol;
					$scope.stores = $scope.dataSourceCol;
				});
			}
		}
		$scope.getDataSourceList();
		// Removed getDataSourceListWhichNotOurGroup call and Method because its no longer in use
		 	$scope.attachDataRepos = function(dataRepo){
		 		localStorage.setItem("repoName" , dataRepo);
		 		$location.path("/dataRepoView");
		 	};
			
			$scope.moreDetail = function(dataSource, dataSourceObj){
				 $routeParams.param1 = dataSourceObj.dataSourceName;
				 $routeParams.param3 = "popUp";
				 $routeParams.param6 = 1;
				 $routeParams.param7 = dataSourceObj.dataSourceId;
				 $routeParams.param4 = $modal.open({
					backdrop : 'static',
					keyboard : false, 
					size : "xl",
		            templateUrl: "/views/DataSource/dataSourceInfo.jsp",
		            resolve : {
		            	$routeParams : function(){
			           	 	return $routeParams;
			           	},
			           	myParam : function(){
			           	 	return $routeParams;
			           	}
			          }
				}),$routeParams.param4.result.then(function(reponame) {
					$scope.getDataSourceList(true);
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
			};
			
			$scope.moreDetailDr = function(dataRepo){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl: "/views/template/moreDetail.html",
					controller: "detailedPage",
	            		resolve : {
	            			dataSource : function(){
	            				return dataRepo;
	            			},
	            			indicator : function(){
	            				return "dataRepo";
	            			}
	            		}
				}),modalInstance.result.then(function(reponame) {
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			};
			
	}]).controller("dataSourceRunPipeLineController" , [ "$scope" , "store" , "$modalInstance" , "$http" , "logger", "$route", "$log", "$modal","$filter", "CONSTANTS", function($scope, store, $modalInstance ,$http,logger,$route, $log, $modal, $filter, CONSTANTS){
		
			$scope.dataTypes = ["INTEGER" , "STRING" , "DATE" , "BOOLEAN" , "DOUBLE" , "LONG", "TIMESTAMP"];
			$scope.repo = true;
			$scope.dataSources = [];
			$scope.disabled = [];
			$scope.disabledValues = [];
			$scope.dataSourceUpdate = [];
			$scope.dataRepoToRun = store;
			$scope.isDSLoadFirstTime = true;
			
			$scope.enableTextValue = function(index , parentIndex){
				$scope.disabledValues[parentIndex][index] = false;
			},
			$scope.data = {
				"cores" : null,
				"RAM" : null
			};
			
			_.each(store.dataSourceIds , function(key){
				$http.post("/getDataSourceById",key).success(function(data){
					if(data !== ""){
						$http({
							method : "post",
							url : "/getReducedBoostList",
							data :{'objectId': key, 'objectType' :'DataSource' }
						}).success(function(beta){
							data.reducedBoostList = beta;
							data.selectedBoostId = beta[0].id;
						}).error(function(beta){
							logger.logError(beta);
						});						
						
						
						var dsParams = [];
						
						$scope.dataSources.push(data);
						var value = [];
						_.each(data.dsLevelParams , function(val){
							value.push(true);
							if(val.paramType==CONSTANTS.TIMESTAMP && val.paramName ==CONSTANTS.GP_CURRENT_RUN_DATETIME){
								val.defaultVal = $filter('date')(new Date(), CONSTANTS.GLOABL_DEFAULT_TIMESTAMP);
							}
							dsParams.push(val);
						});
						$scope.disabledValues.push(value); 
						$scope.disabled.push(true);
					}
				}).error(function(data){
					logger.logError(data);
			    });
			});
			
			$scope.isContainsString = function(dsName) {
				$scope.isEqualFlag = false;
				if(undefined != $scope.sameInputParamDs 
						&& null != $scope.sameInputParamDs){
					for (var i = 0; i < $scope.sameInputParamDs.length; i++) {
						if(_.isEqual(dsName.toLowerCase(), $scope.sameInputParamDs[i].toLowerCase())){
							$scope.isEqualFlag =  true;
							break;
						}else{
							$scope.isEqualFlag = false;
						}
					}
				}
				return $scope.isEqualFlag;
			}
			
			$scope.removeLoadDs = function(index, dsId) {
				$scope.dataSources.splice(index, 1);
				store.dataSourceIds = _.reject(store.dataSourceIds, function(d){ return d == dsId; });
			}
			
			$scope.checkFieldMapping = function(fm1, fm2){
				$scope.fieldMappingStatus = true;
				if(fm1.length==fm2.length){
					for(var i=0;i<fm1.length;i++){
						if(fm1[i].fieldName==fm2[i].fieldName && fm1[i].fieldDataType==fm2[i].fieldDataType){
							continue;
						}else{
							$scope.fieldMappingStatus=false;
							break;
						}
					}	
				}
				return $scope.fieldMappingStatus;
			};
			
			$scope.getVersionStatusOfDs = function(dataSource){
				$http.post("/getVersionStatusOfDataSource",dataSource.id).success(function(data, status, headers, config){
					
					if(data.StatusFileTypeMatch==true){

						if(data.StatusFieldMappingVersion!=null && data.StatusFieldMappingVersion!=undefined && data.StatusFieldMappingVersion!=true){

							if(dataSource){
								$scope.newDataSource =dataSource;	
							}

							if($scope.newDataSource && $scope.newDataSource.fieldMappings){
								$scope.newFieldMappings= $scope.newDataSource.fieldMappings;

								$http.post("/getFieldMappingDSByDSIHistory",$scope.newDataSource.id).success(function(data, status, headers, config){
									$scope.existingFieldMappings =data.dataSource.fieldMappings;
									if(!$scope.checkFieldMapping($scope.newFieldMappings, $scope.existingFieldMappings)){
										$scope.openFieldMappingErrorModal(data, $scope.newFieldMappings, $scope.existingFieldMappings);
									}else{
										$scope.runJobWithAjax(dataSource);
									}
								});

							}

						}else{
							$scope.runJobWithAjax(dataSource);
						}
					}
					else{
						logger.logError("File Type are not same. You can continue with Load strategy - Replace All.");
					}
					
				});
			}
			
			$scope.openFieldMappingErrorModal = function(data, newFieldMappings, existingFieldMappings){
				var modalInstance;
				modalInstance = $modal.open({
					size: "lg",
					templateUrl: "/views/template/errorFieldMappingVersion.html",
					controller: "checkVersionFieldMappingController",
					resolve : {
						newFieldMappings : function(){
							return angular.copy(newFieldMappings);
						},
						existingFieldMappings : function(){
							return angular.copy(existingFieldMappings);
						},from : function() {
							return "D";
						},StatusFileTypeMatch : function() {
							return data.StatusFileTypeMatch;
						}
					}
				
				}),modalInstance.result.then(function() {
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			}
			
			var dataSt = [];
			
			$scope.runJobWithAjax=function(store){
				if(store){	
					$http({
						method: 'POST',
						url : "/startRepoJob",
						data : dataSt
					}).
					success(function(data, status, headers, config){
						$scope.sameInputParamDs = data;
						if($scope.sameInputParamDs.length>0){
							
							 var modalInstance;
								modalInstance = $modal.open({
									templateUrl: "/views/template/confirmLoadingSameInputParam.html",
									controller: "confirmLoadingSameInputParamCtrl",
									resolve : {
										dsList : function(){
											return $scope.sameInputParamDs;
										}
									}
								}),modalInstance.result.then(function(acknowledgement) {
									if("run" == acknowledgement){
										$scope.isDSLoadFirstTime = false;
										$scope.runPipe();
										$modalInstance.close('CLOSE_POPUP');
									}
								}, function() {
									$log.info("Modal dismissed at: " + new Date);
							});
							
						}else{
							logger.logSuccess("Started the job");
							$modalInstance.dismiss();
						}
						
					}).error(function(){
					logger.logError("Unable to start the job");
				});
			};
			}
			
			$scope.runPipe = function(){
				
				if($scope.Priority !== "Custom"){
					$scope.data.cores = null;
					$scope.data.RAM = null;
				}else
				if($scope.Priority == "Custom"){
					if(($scope.data.cores == "" 
							|| $scope.data.cores == undefined
								|| $scope.data.cores == null)){
						logger.logError("Please provide the cores values");
						return false;
					}else if(isNaN($scope.data.cores)){
						logger.logError("Please provide the numbers as cores values");
						return false;
					}
					
					if(($scope.data.RAM == "" 
						|| $scope.data.RAM == undefined
							|| $scope.data.RAM == null)){
						logger.logError("Please provide the RAM values");
						return false;
					}else if(isNaN($scope.data.RAM)){
						logger.logError("Please provide the numbers as RAM values");
						return false;
					}
				}
				
				var i = 0;
				_.each($scope.disabled , function(value){
						if(value == false){
							$scope.dataSources[i].availableToUse=undefined;
								$http({
					    	  		url: "/saveDataSource",
					    	  		method: 'Post',
					    	  		data : $scope.dataSources[i]
					    		}).then(function(response) {
					    	}, 
					    	function(response) {
					    		logger.logError("Something wends wrong");
					    	});
						}
						i++;
					});
					var ListOfInputs = [];
					var dsIdSelectedBoostIdMapping = [];
					var indicatorForBlank = false;
					var dataSourceCheck = false;
					var inAppendMode = false;
					_.each($scope.dataSources , function(source){

						var eachSelectedIdAndDsMap = {
								"dsId" : source.id,
								"paramName" : source.selectedBoostId									
						};
						dsIdSelectedBoostIdMapping.push(eachSelectedIdAndDsMap);
						
						_.each(source.dsLevelParams , function(values){
							
							
							
							var object = {
									"dsId" : source.id,
									"drId" : store.repoId,
									"type" : values.paramType,
									"paramName" : values.paramName,
									"val" : values.defaultVal
							};
							ListOfInputs.push(object);
						});
						
						if(source.dataSourceType === null 
								|| source.dataSourceType === undefined 
									|| source.dataSourceType === ""){
							dataSourceCheck = true;
							return ;
						}else if(source.dataSourceType !== "RDBMS_DATA_SOURCE" && source.dataSourceType !== "NOSQL"){
							if(source.fileDataIngesterDetails.serverFilePath === "" 
								|| source.fileDataIngesterDetails.serverFilePath === undefined 
									|| source.fileDataIngesterDetails.serverFilePath === null){
								indicatorForBlank = true; 
								return false;
							} 
						} else {
							if((source.config.queryToExecute === "" 
								|| source.config.queryToExecute === undefined 
									|| source.config.queryToExecute === null) &&  
									(source.config.noSqlSchema === undefined 
											|| source.config.noSqlSchema.trim() === "")){
								indicatorForBlank = true;
								return false;
							}
						}
						
						if(source.dataSourceLoadStrategy != undefined && source.dataSourceLoadStrategy != null
								&& source.dataSourceLoadStrategy.loadStategyType=="APPENDALL"){
							inAppendMode = true;
							$scope.getVersionStatusOfDs(source);	
						} 
						
						if(source.dataSourceLoadStrategy !== null && source.dataSourceLoadStrategy.updateLoadStategy !== null){
							if(source.dataSourceLoadStrategy.updateLoadStategy.appendenabled == true){
								var object = {
										"dsId" : source.id,
										"drId" : store.repoId,
										"type" : "Append",
										"paramName" : "Append_"+source.dataSourceName,
										"val" : source.dataSourceLoadStrategy.updateLoadStategy.appendInputValue
								};
								
								ListOfInputs.push(object);
							}
							if(source.dataSourceLoadStrategy.updateLoadStategy.deleteenabled == true){
								var object = {
										"dsId" : source.id,
										"drId" : store.repoId,
										"type" : "Delete",
										"paramName" : "Delete_"+source.dataSourceName,
										"val" : source.dataSourceLoadStrategy.updateLoadStategy.deletepath
								};
								ListOfInputs.push(object);
							}
							
							if(source.dataSourceLoadStrategy.updateLoadStategy.mergeenabled == true){
								var object = {
										"dsId" : source.id,
										"drId" : store.repoId,
										"type" : "Merge",
										"paramName" : "Merge_"+source.dataSourceName,
										"val" : source.dataSourceLoadStrategy.updateLoadStategy.mergePath
								};
								ListOfInputs.push(object);
							}
						}else {
							var object = null;
							if(source.dataSourceType !== "RDBMS_DATA_SOURCE"){
								object = {
									"dsId" : source.id,
									"drId" : store.repoId,
									"type" : "Replace",
									"paramName" : "Replace_"+source.dataSourceName,
									"val" : source.fileDataIngesterDetails.serverFilePath
								};
							}else{
								object = {
										"dsId" : source.id,
										"drId" : store.repoId,
										"type" : "Replace",
										"paramName" : "Replace_"+source.dataSourceName,
										"val" : source.config.queryToExecute
									};
							}
							ListOfInputs.push(object);
						}
					});
					
					
					dataSt[0] = store;  
					dataSt[1] = ListOfInputs;
					if(dataSourceCheck){
						logger.logError("Blank datasource selected for execution");
						return false;
					}
					else if(indicatorForBlank){
						logger.logError("Please provide path/query each datasource");
						return false;
					}

					var inputObject = {
							"repoId" : store.repoId,
							"isDSLoadFirstTime" : $scope.isDSLoadFirstTime
					}
					
					dataSt[2] = inputObject;
					dataSt[3]= dsIdSelectedBoostIdMapping
					
					if(!inAppendMode){
						$scope.runJobWithAjax(store);
					}
					
			},
			$scope.enableText = function($index){
				$scope.disabled[$index] = false;
			},
			$scope.cancel = function() {
		        $modalInstance.dismiss("cancel");
			};	
	}]).controller("confirmLoadingSameInputParamCtrl",["$modal", "$log" ,"$scope" , "$modalInstance" , "$http" , "logger", "dsList", function($modal, $log, $scope, $modalInstance ,$http, logger, dsList){
	    
		$scope.dataSourceNameList = dsList;
		$scope.selectAction = function() {
			$modalInstance.close('run');
		}
		
		$scope.dismissModel = function(){
			$modalInstance.dismiss('');
		};
		
	}]).controller("detailedPage" , ["$modal", "indicator", "$routeParams" ,  "$log" ,  "dataSource", "$scope" , "$modalInstance" , "$http" , "logger", "$route" , function($modal, indicator, $routeParams, $log, dataSource, $scope, $modalInstance ,$http,logger,$route){
		$scope.indicator =  indicator;
		if(indicator == "dataSource"){
			$scope.ds = dataSource;
		}else{
			$scope.dr = dataSource; 
		}
		$http({
			method: "Post",
			url: "/getDependentFlowForDataSourceByComponentId",
			data : dataSource.dataSourceId
		}).success(function(data){
			$scope.dsUsedInAppName = data;
		}).error(function(data){
			logger.logError(data);
		});
		$scope.dismissModel = function(){
			$modalInstance.dismiss('');
		};
		$scope.dataSourcePopup = function(dataSourceName){
			 $routeParams.param1 = dataSourceName;
			 $routeParams.param3 = "popUp";
			 $routeParams.param6 = 1;
			 $routeParams.param4 = $modal.open({
				backdrop : 'static',
				keyboard : false, 
				size : "xl",
	            templateUrl: "/views/DataSource/dataSource.jsp",
	            resolve : {
	            	$routeParams : function(){
		           	 	return $routeParams;
		           	},
		           	myParam : function(){
		           	 	return $routeParams;
		           	}
		          }
			}),$routeParams.param4.result.then(function(reponame) {
            }, function() {
             $log.info("Modal dismissed at: " + new Date);
           });
		};
	}]).controller("dataSourceModelController" , [ "$filter","$scope", "$http" , "$location","$routeParams", "$route", "logger","$rootScope","$modal",function($filter,$scope, $http , $location,$routeParams,$route, logger,$rootScope,$modal){
		$scope.obj={},
		$scope.Repository = false;
		
		
		
		$http.post("/repo").success(function(data, status, headers, config){
			$scope.dataRepoList = _.reject(data , function(val){return val.repoName.toLowerCase() == "globalrepo";});
		}).error(function(){
	    	logger.logError("Something went wrong.");
	    });
		
		$scope.cancel = function() {
	        //$modalInstance.dismiss("cancel");
			$scope.remove();
			$scope.remove();
			$scope.dataSource = "";
		}, 
		$scope.createDataSource = function(index){

			$scope.repoName = undefined;
			var repoNameForDs; 
			if($scope.obj.ingection == undefined || $scope.obj.ingection == null)
			{
				$scope.obj.ingection = "quick";
			}
			if($scope.repoName == undefined || $scope.repoName == null){
				repoNameForDs = "globalRepo";
			}else{
				repoNameForDs = $scope.repoName.repoName;
			}
			
			if($scope.dataSource && $scope.dataSource !== undefined && $scope.dataSource !== null && $scope.dataSource !== ""){
				if($scope.dataSource.length > 30){
					logger.logError("Data source name should not be greater than 30 characters.");
					return;
				}else{
					var reqData = {'datasourcename':$scope.dataSource,'repoName':repoNameForDs,'ingection':$scope.obj.ingection};
					$http.post("/createDataSource", reqData).success(function(data, status, headers, config){
						$scope.remove();
						$scope.remove();
						if(parseInt(data) >= 0){
							logger.logSuccess("Data source is created sucessfully");
							$rootScope.isDataSourceFileSaved = true;
				    	 	$routeParams.param1 = $scope.dataSource;
				    	 	$routeParams.param6 = 1;
							/*$routeParams.param4 = $modal.open({
								size : "xl",
					            templateUrl: "/views/DataSource/dataSource.jsp",
					            resolve : {
					            	$routeParams : function(){
						           	 	return $routeParams;
						           	},
						           	myParam : function(){
						           	 	return $routeParams;
						           	}
						        }
							}),$routeParams.param4.result.then(function(reponame) {
				           }, function() {
				            $log.info("Modal dismissed at: " + new Date);
				          });*/
				    	 	$location.path("/datasource/"+$routeParams.param1);
						} else {
							logger.logError(data);
						}
					}).error(function(data){
						logger.logError("Data Source : " + data);
					});
				}
			}
			else{
				logger.logError("Data source name should not start with numeric value.Also it cannot be blank and special characters/white spaces are not allowed.");
				return;
			}
		};
		
		$scope.attachDataSource = function(datasource){
			$http.post("/attachDataSource",{'datasourcename':datasource,'repoName' :$routeParams.param1}).success(function(data, status, headers, config){
				if(data <= 0) {
					logger.logError("data Source is already added");
					return false;
				} else {
					$modalInstance.close($scope.repoName);
				$route.reload();
			}
			}).error(function(){
				alert("not able to process yr request");
			});
		};
	
}]).controller("renameController" , ["data", "checkSource","$scope", "$modalInstance", "$http" ,"$routeParams", "$route", "logger",function(data ,checkSource ,$scope, $modalInstance, $http ,$routeParams,$route, logger){
	$scope.checkSource = checkSource;

	if($scope.checkSource === "dataSource"){
		$scope.dataSource = angular.copy(data);
		$scope.dataSource.dataSourceName = ""; 
		$scope.exName = data.dataSourceName;
		$scope.Name = data.dataSourceName;
	}
	$scope.dismissModel = function(){
		$modalInstance.dismiss('');
	};
	$scope.saveDataSource = function(){
		if($scope.dataSource.dataSourceName === "" || 
				$scope.dataSource.dataSourceName == null || 
					$scope.dataSource.dataSourceName == undefined){
			logger.logError("Data source name should not start with numeric value.Also it cannot be blank and special characters/white spaces are not allowed.");
			return false;
		}
		
		$scope.dataSource.nodeIndicate = undefined;
		$http({
	  			url: "/renameDataSource",
	  			method: "POST",
	  			data : $scope.dataSource
			}).success(function(data, status, headers, config){
				logger.logSuccess("Updated Successfully");
				$modalInstance.close($scope.dataSource);
		}).error(function(data){
			logger.logError(data);
			return false;
	    });	
	};
}]).controller("assignController" , ["data","$scope", "$modalInstance", "$http" ,"$routeParams", "$route", "logger",function(data,$scope, $modalInstance, $http ,$routeParams,$route, logger){

	$scope.repo = {"dataRepoName" : ""};
	$scope.Name = data.dataSourceName;
	$scope.dataSource = data; 
	$scope.dataRepoList = [];
	$scope.dataRepoList.push({"dataSourceName" : "Create New DataSource"});
	
	$http.post("/repo").success(function(data, status, headers, config){
		 _.each(data , function(val){if(val.repoName.toLowerCase() !== "globalrepo"){$scope.dataRepoList.push(val);}});
	}).error(function(){
    	logger.logError("Something wends wrong.");
    });
	
	$scope.assignDataSource = function(){
		if($scope.repoName === undefined){
			$scope.repoName = {"repoName" : "globalRepo"};
		}
		if($scope.repoName.repoName === "--Add New DS-Group--"){
			
			if($scope.repo.dataRepoName === "" || $scope.repo.dataRepoName === undefined || $scope.repo.dataRepoName === null){
				logger.logError("Please provide the proper DS-Group Name");
				return false;
			}
				$http.post("/addNewRepo", $scope.repo.dataRepoName).success(function(data, status, headers, config){
					$http.get("assignDataSource?dataSourceName="+$scope.Name+"&newRepoName="+$scope.repo.dataRepoName).success(function(data, status, headers, config){
						$scope.dataSource.dataRepoName = $scope.repo.dataRepoName;
						$scope.dataSource.dataRepoId = data;
						$http.get("getSingleDSInstance?dataSourceId="+$scope.dataSource.dataSourceId).success(function(childData, status, headers, config){
							logger.logSuccess("Updated Successfully");
							$modalInstance.close(childData);
						});
					}).error(function(data){
						logger.logError(data);
					});
				}).error(function(data){
					logger.logError(data);
		    	});
			}else{
				$http.get("assignDataSource?dataSourceName="+$scope.Name+"&newRepoName="+$scope.repoName.repoName).success(function(data, status, headers, config){
						$scope.dataSource.dataRepoName = $scope.repoName.repoName;
						$scope.dataSource.dataRepoId = data;
						$http.get("getSingleDSInstance?dataSourceId="+$scope.dataSource.dataSourceId).success(function(childData, status, headers, config){
							$modalInstance.close(childData);
							logger.logSuccess("Updated Successfully");
						});
					}).error(function(data){
						logger.logError(data);
					});
			}
	};
	$scope.dismissModel = function(){
		$modalInstance.dismiss('');
	};
}]).controller("TabsCtrlDS", ["$route", "DataSourceService","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", function($route,DataSourceService,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger){
	$scope.$emit("myEvent1", "Data-sources");
	
	$scope.appId = $routeParams.param1;
	$scope.appName = $routeParams.param2;
	
	if($routeParams.appIndicator){
		$scope.tabs = [{
			title: 'Existing flows'
		}];
		$scope.$emit("myEvent1", "Existing flows");
	}else{
		$scope.tabs = [{
			title: 'Existing data sources'
		}, {
			title: 'Data sources created in flow'
		}, {
			title: 'Inputs file(File parameters)'
		}, {
			title: 'Node from other flows as head'
		}];
	}
	
	$scope.flowDetails = null;
	if(undefined !== $scope.appId && null !== $scope.appId){
		$http.post("/getSingleApplication",$scope.appName).success(function(data, status, headers, config){
			$scope.flowDetails = data;
			if($scope.flowDetails.flowType == "STREAM"){
				$scope.tabs = [{
					title: 'Existing data sources'
				}, {
					title: 'Node from other flows as head'
				}];
			}
		});
	}
	
	if($routeParams.param6 === 1)
			$scope.currentTab = '/views/DataSource/dataSourceView.jsp';

		$scope.onClickTab = function (tab) {
			$scope.$emit("myEvent1", tab.title);
		};

		$scope.isActiveTab = function(tabUrl) {
			return tabUrl == $scope.currentTab;
		};
	}]).controller("appLevelDataSource" , ["$filter", "$scope", "$http" ,"$routeParams", "$route", "logger",function($filter , $scope,  $http ,$routeParams,$route, logger){
		$scope.appId = $routeParams.param1;
		$scope.appName = $routeParams.param2;
		
		$scope.$on("broadcastEvent", function (event , arg){
			$scope.dsDecide = arg;
		});
		
		
		$scope.flowDetails = null;
		if(undefined !== $scope.appId && null !== $scope.appId){
			$http.post("/getSingleApplication",$scope.appName).success(function(data, status, headers, config){
				$scope.flowDetails = data;
				if($scope.flowDetails.flowType == "STREAM"){
					$scope.isStreamFlow = true;
				}
			});
		}
		
		$scope.selectDSources = function(dataSource, check){
			$scope.$emit("myEvent2" , [dataSource , check]);
		};
	
		if($scope.appId !== undefined && $scope.appId != null && $scope.appId !== ""){
			$http.post("/getAppSourceList",$scope.appId).success(function(data, status, headers, config){
				data = _.filter(data, function(val){
					if(val !== null) {
							return !((val.dataSourceName.toLowerCase().indexOf(("temp_"+$scope.appName + "_gp_").toLowerCase()) === 0) 
								|| (val.dataSourceName.toLowerCase().indexOf(($scope.appName + "_gp_").toLowerCase()) === 0));
						}
					});
				
				if(data){
					return $scope.stores = data	
			        , $scope.searchKeywords = "", 
			        $scope.filteredStores = [],
			        $scope.row = "", 
			        $scope.select = function(page) {
						$scope.pageNum = page;
			            var end, start;
			            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.appLevelDS = $scope.filteredStores.slice(start, end);
			        }, 
			        $scope.onFilterChange = function() {
			            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
			        }, $scope.onNumPerPageChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.onOrderChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.search = function() {
			        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
			        }, $scope.order = function(rowName) {
			        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
			        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
			        $scope.numPerPage = $scope.numPerPageOpt[4], 
			        $scope.currentPage = 1, 
			        $scope.appLevelDS = [],
			        ($scope.init = function() {
			            return $scope.search(), $scope.select($scope.currentPage);
			        })();
				};
				
				console.log(data);
			});	
		}
		
		$scope.deleteAppsLevelDs = function(dataSource){
				$scope.$emit("deleteAppDs" , dataSource);
	     };
	     
	     $scope.$on("deleteLevelAppDs", function (event , arg){
	    	 deleteValues(arg);
		});
	     
	     function deleteValues(dataSourceId) {
	    	 $scope.appLevelDS = _.reject($scope.appLevelDS , function(val){return dataSourceId === val.dataSourceId;});
	     }
	     
	}]).controller("inputLevelDataSources" , ["$filter", "$scope", "$http" ,"$routeParams", "$route", "logger",function($filter , $scope,  $http ,$routeParams,$route, logger){
		$scope.appId = $routeParams.param1;
		$scope.appName = $routeParams.param2;
		
		$scope.$on("broadcastEvent", function (event , arg){
			$scope.dsDecide = arg;
		});
		
		$scope.flowDetails = null;
		if(undefined !== $scope.appId && null !== $scope.appId){
			$http.post("/getSingleApplication",$scope.appName).success(function(data, status, headers, config){
				$scope.flowDetails = data;
				if($scope.flowDetails.flowType == "STREAM"){
					$scope.isStreamFlow = true;
				}
			});
		}
		
		$scope.selectDSources = function(dataSource, check){
			$scope.$emit("myEvent2" , [dataSource , check]);
		};
		
		if($scope.appId !== undefined && $scope.appId != null && $scope.appId !== ""){
			$http.post("/getAppSourceList",$scope.appId).success(function(data, status, headers, config){
				data = _.filter(data , function(val){
					if(val !== null) {
						return ((val.dataSourceName.toLowerCase().indexOf(("temp_"+$scope.appName + "_gp_").toLowerCase()) === 0) 
								|| (val.dataSourceName.toLowerCase().indexOf(($scope.appName + "_gp_").toLowerCase()) === 0));
					}
				});
//				data = _.filter(data , function(val){return val.dataSourceName.indexOf("temp_") === 0;});
				
				if(data){
					return $scope.stores = data	
			        , $scope.searchKeywords = "", 
			        $scope.filteredStores = [],
			        $scope.row = "", 
			        $scope.select = function(page) {
						$scope.pageNum = page;
			            var end, start;
			            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.inputLevelDS = $scope.filteredStores.slice(start, end);
			        }, 
			        $scope.onFilterChange = function() {
			            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
			        }, $scope.onNumPerPageChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.onOrderChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.search = function() {
			        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
			        }, $scope.order = function(rowName) {
			        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
			        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
			        $scope.numPerPage = $scope.numPerPageOpt[4], 
			        $scope.currentPage = 1, 
			        $scope.inputLevelDS = [],
			        ($scope.init = function() {
			            return $scope.search(), $scope.select($scope.currentPage) , $scope.order("-id");
			        })();
				};
				
			});	
		}
	}]).controller("nodesListController" , ["$filter", "$scope", "$http" ,"$routeParams", "$route", "logger",function($filter , $scope,  $http ,$routeParams,$route, logger){
		$scope.appId = $routeParams.param1;
		$scope.appName = $routeParams.param2;
		
		$scope.$on("broadcastEvent", function (event , arg){
			$scope.dsDecide = arg;
		});
		
		$scope.selectDSources = function(dataSource, check){
			$scope.$emit("myEvent2" , [dataSource , check]);
		};
		
		
		if($scope.appId !== undefined && $scope.appId != null && $scope.appId !== ""){
			$http.post("/getNodesList").success(function(data, status, headers, config){
				 data = _.reject(data , function(val) {return $scope.appId ===  val.appId;});
				if(data){
					return $scope.stores = data	
			        , $scope.searchKeywords = "", 
			        $scope.filteredStores = [],
			        $scope.row = "", 
			        $scope.select = function(page) {
						$scope.pageNum = page;
			            var end, start;
			            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.nodesList = $scope.filteredStores.slice(start, end);
			        }, 
			        $scope.onFilterChange = function() {
			            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
			        }, $scope.onNumPerPageChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.onOrderChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.search = function() {
			        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
			        }, $scope.order = function(rowName) {
			        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
			        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
			        $scope.numPerPage = $scope.numPerPageOpt[4], 
			        $scope.currentPage = 1, 
			        $scope.nodesList = [],
			        ($scope.init = function() {
			            return $scope.search(), $scope.select($scope.currentPage) , $scope.order("-appId");
			        })();
				};
				
			});	
		}
	}]).controller("AppsListController" , ["$filter", "$scope", "$http" ,"$routeParams", "$route", "logger",function($filter , $scope,  $http ,$routeParams,$route, logger){
		$scope.currentPageStores = [];
		
		$scope.appId = $routeParams.param1;
		
		$scope.$on("broadcastEvent", function (event , arg){
			$scope.dsDecide = arg;
		});
		
		$scope.selectApps = function(selectApps, check){
			$scope.$emit("appEvent" , [selectApps, check]);
		};
		
		if($scope.appId !== undefined && $scope.appId != null && $scope.appId !== ""){
			$http.post("/getAllApplications","PERIODIC").success(function(data, status, headers, config){
				 data = _.reject(data , function(val){return $scope.appId ===  val.appId;});
					if(data){
				    	return $scope.stores = data	
				        , $scope.searchKeywords = "", 
				        $scope.filteredStores = [],
				        $scope.row = "", 
				        $scope.select = function(page) {
				    		$scope.pageNum = page;
				            var end, start;
				            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
				        }, 
				        $scope.onFilterChange = function() {
				            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
				        }, $scope.onNumPerPageChange = function() {
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.onOrderChange = function() {
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.search = function() {
				            return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				        }, $scope.order = function(rowName) {
				            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0;
				        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				        $scope.numPerPage = $scope.numPerPageOpt[4], 
				        $scope.currentPage = 1, 
				        $scope.currentPageStores = [],
				        ($scope.init = function() {
				            return $scope.search(), $scope.select($scope.currentPage),$scope.order("-appId");
				        })();
				}  
			});
		}
	}]).controller("dataSourceRequestCtrl" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll",  function($anchorScroll ,$filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,anchorSmoothScroll){
		
		$scope.dsLists = [];
		$scope.appLists = [];
		$scope.appId = $routeParams.param1;
		$scope.appName = $routeParams.param2;
		$scope.explanation = $routeParams.param15;
		$scope.hideTbl = true;
		$scope.oneAtATime = false;
		$scope.messageChanger = $routeParams.appIndicator;
		if($routeParams.param15 === undefined){
			$http.post("/getDataSourcesMsg").success(function(data, status, headers, config){
				$scope.explanation = data;
			});
		}

		$scope.dismissModel = function (){
			$routeParams.param1 = undefined;
			$routeParams.param15 = undefined;
			$routeParams.param4.dismiss('');
			$routeParams.param4 = undefined;
		},
	
		$scope.dsDecide = true;
		$scope.dsSource = function(){
			$scope.dsDecide = true;
		};
		
		$scope.$on("deleteAppDs" ,function (event, args) {
			$scope.deleteAppsLevelDs(args);
		});
		
		$scope.$on("myEvent2", function (event, args) {
			$scope.selectDSources(args[0] , args[1]);
		});
		
		$scope.$on("appEvent", function (event, args) {
			$scope.selectApps(args[0] , args[1]);
		});
		$scope.checkHeader = "existHeader";
		$scope.$on("myEvent1", function (event, args) {
			if(args == "Data sources created in flow" || args == "Stream sources created in flow"){
				$scope.dsDecide = false;
				$scope.checkHeader = "appLevelDSHeader";
			}
			else if(args == "Existing data sources"){
				$scope.dsDecide = true;
				$scope.checkHeader = "existHeader";
			}
			else if(args == "Inputs file(File parameters)"){
				$scope.dsDecide = null;
				$scope.checkHeader = "inputDs";
			}
			else if(args == "Node from other flows as head"){
				$scope.dsDecide = "App-nodes";
				$scope.checkHeader = "nodeHeader";
			}
			else if(args == "Existing flows"){
				$scope.dsDecide = "Apps";
				$scope.checkHeader = "importApp";
			}
			$scope.$broadcast("broadcastEvent" , $scope.dsDecide);
		});
		
		$scope.selectDSources = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.dataSourceId === dataSource.dataSourceId;});
			}
		};
		
		$scope.selectApps = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.appName === dataSource.appName;});
			}
			
		};
		
		$scope.selectNodes = function(node){
			for(var i=0; i<$scope.dsLists.length;i++){
				
				if($scope.dsLists[i].nodeId==node.nodeId){
					$scope.dsLists[i]=node;
					return;
				}
			}
			$scope.dsLists.push(node);
			
		};
		
		
		$scope.confirmDsource= function(){
			if($scope.dsDecide=="Apps"){
				$routeParams.param4.close($scope.dsLists);
			}else{

				$routeParams.param1 = undefined;
				$routeParams.param4.close($scope.dsLists);
			}
		};
		
		$scope.gotoAnchor = function(repoName) {
			var id = $location.hash();
		    $location.hash(repoName);
		    $anchorScroll();
		    $location.hash(id);
	     };
		
	     $scope.deleteAppsLevelDs = function(dataSource){
	    	 $scope.dsLists = _.reject($scope.dsLists , function(val){return dataSource.id === val.id;});
	    	 $scope.deleteDataSource(null , dataSource.dataSourceId);
	     };
	     
	     
	     $scope.globalJob = function(dataSoourceToRun){
	    	 
	    	 if(dataSoourceToRun.dataSourceType == null || dataSoourceToRun.dataSourceType == "null" || dataSoourceToRun.dataSourceType == undefined){
	    		 return;
	    	 }
	    	 
	    	 $http.post("/getRepoInstance",dataSoourceToRun.dataRepoName).success(function(data, status, headers, config){
	    		 data.dataSourceIds = [];
	    		 data.dataSourceIds.push(dataSoourceToRun.dataSourceId);
	    		 var modalInstance;
					modalInstance = $modal.open({
						size : "lg",
						templateUrl: "/views/template/runPipeLineTemplate.html",
						controller: "dataSourceRunPipeLineController",
						resolve : {
							store : function(){
								return data;
							}
						}
					}),modalInstance.result.then(function(dsList) {
					}, function() {
						$log.info("Modal dismissed at: " + new Date);
				});
	    	 });
	     },
	     $scope.startJob = function(){
	    	 if($scope.repoName != undefined){
	    		 $http.post("/getRepoInstance",$scope.repoName).success(function(data, status, headers, config){
	    			 data.dataSourceIds = [];
	    			 var i = 0;
	    			 _.each($scope.checkBox , function(val){
	    				 if(val == true){
	    					 data.dataSourceIds.push($scope.dataSourceOfRepo[i].dataSourceId);
	    				 } 
	    				 i++;
	    			 });
	    			 if(data.dataSourceIds.length > 0){
	    				 var modalInstance;
	 						modalInstance = $modal.open({
	 							size : "lg",
	 							templateUrl: "/views/template/runPipeLineTemplate.html",
	 							controller: "dataSourceRunPipeLineController",
	 							resolve : {
	 								store : function(){
	 									return data;
	 								}
	 							}	
	 						}),modalInstance.result.then(function(dsList) {

	 						}, function() {
	 							$log.info("Modal dismissed at: " + new Date);
	 						});
	    		 		}else{
	    		 			logger.logError("Please select at least one Data-source");
	    		 		}
	    	 		});
	    	
	    	 	}
		};
		
		$scope.repoListClick = function(repoName){
			$location.path("/dataRepoOperations/"+repoName);
		};
		
		$scope.openDataProfiling = function(ds){
			if(ds.dataSourceType == null || ds.dataSourceType == "null" || ds.dataSourceType == undefined){
				return;
			}
			
			$http({
				method: 'GET',
				url : "/getDataProfileJobInstances?profileType="+'PreProf'+"&dsId="+ds.dataSourceId
			}).
			success(function(data, status, headers, config){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl : "/views/template/DataProfileModal.html",
			        controller: "dataProfileController",
			        resolve : {
			        	 store : function(){
			        		return data; 
			        	 },
			        	 profileType : function(){
							return null; 
						 }
				    }
				}),modalInstance.result.then(function(reponame) {
		        }, function() {
		           	$log.info("Modal dismissed at: " + new Date);
		        });
			}).error(function(data){
				logger.logError(data);
			});
		};
		
		$scope.renameDataSource = function(dataSource , indicator){
			if(indicator == "dataSource"){
				var ds = angular.copy(dataSource);
				ds.nodeIndicate = undefined; 
				
				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/rename.html",
		            controller: "renameController",
		            	resolve : {
		            		checkSource : function(){
			            	 	return indicator;
			            	},
			            	data : function(){
			            	 	return ds;
			            	}
			            }
				}),modalInstance.result.then(function(dataSourceInstance) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == dataSourceInstance.dataSourceId){
								$scope.dataSourceCol[i] = dataSourceInstance;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								break;
							}
							
						}
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
		
			}else{
			}
		},
		
		$scope.check = function(value){
			var i = 0;
			_.each($scope.checkBox , function(val){
				$scope.checkBox[i] = value;
				i++;
			});
		};
		
		$scope.finalSet = function(val){
			if(val === false)
				$scope.finalValue = false;
			else{
				var bool = true;
				for(var i = 0 ; i < $scope.checkBox.length ; i++){
					if($scope.checkBox[i] == false){
						bool = false;
						break;
					}
					
				}
				if(bool == true)
					$scope.finalValue = bool;
			}
				
		};
		
		$scope.assignDataSource = function(dataSource){
			var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/assignDataSource.html",
		            controller: "assignController",
		            	resolve : {
			            	data : function(){
			            	 	return angular.copy(dataSource);
			            	}
			            }
				}),modalInstance.result.then(function(updatedDataSource) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == updatedDataSource.dataSourceId){
								$scope.dataSourceCol[i] = updatedDataSource;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								if($scope.dataSourceOfRepo[i].dataRepoId === $scope.dataSourceCol[index].dataRepoId)
									$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								else
									$scope.dataSourceOfRepo.splice(i , 1);
								
								break;
							}		
						}
					
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
			});
		
		},
		$scope.dataSourcePopup = function(dataSourceName, dsId){
			if($scope.appId !== undefined)
				return false;
				
			 $routeParams.param1 = dataSourceName;
			 $routeParams.param6 = 1;
			 $routeParams.param7 = dsId;
			 $routeParams.param4 = $modal.open({
				backdrop : 'static',
				keyboard : false, 
				size : "xl",
	            templateUrl: "/views/DataSource/dataSource.jsp",
	            resolve : {
	            	$routeParams : function(){
		           	 	return $routeParams;
		           	},
		           	myParam : function(){
		           	 	return $routeParams;
		           	}
		          }
			}),$routeParams.param4.result.then(function(reponame) {
				$scope.getDataSourceList(true);
           }, function() {
            $log.info("Modal dismissed at: " + new Date);
          });
		},
		$scope.deleteDataSource = function(index , dataSourceId , dataSourceName){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:dataSourceName, type: "datasource"};
					}
				}
			}),modalInstance.result.then(function() {

				if($scope.appId === undefined){
						if(index !== null){
							dataSourceId = angular.copy($scope.dataSourceCol[index].dataSourceId);
							$scope.dataSourceCol.splice(index , 1);
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							if($scope.dataSourceOfRepo !== undefined || $scope.dataSourceOfRepo !== null || $scope.dataSourceOfRepo.length > 0)
								$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
						if(dataSourceId !== null && index == null){
							$scope.dataSourceCol = _.reject(angular.copy($scope.dataSourceCol) , function(val){return val.dataSourceId === dataSourceId;});
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
					}

					$http({
						method: 'POST',
						url : "/deleteDataSource",
						data : dataSourceId
					}).
					success(function(data, status, headers, config){
						logger.logSuccess("Data-source deleted successfully");
						$scope.getDataSourceList();
					}).error(function(){
						logger.logError(data);
					});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
			
			
		};
		$scope.statusList = [];
		$scope.statusAndDsIdMap = {};
		$scope.getDataSourceList = function () {
			$http.post("/getAllRequestedObjects").success(function(data, status, headers, config){
					$scope.statusList = data.requestIndicator;
					$scope.dataSourceCol = _.filter(data.requestedObjects, function(val){if(val !== null){return val.appId == null;}});
					
					var indexIs = 0;
					_.each(data.requestedObjects, function(dsInst){
						$scope.statusAndDsIdMap[dsInst.id] = $scope.statusList[indexIs];
						indexIs++;
					});
					
					$scope.dataSourceColBkp = angular.copy($scope.dataSourceCol); 
					if(data){
						return $scope.stores = $scope.dataSourceCol	
				        , $scope.searchKeywords = "", 
				        $scope.filteredStores = [],
				        $scope.row = "", 
				        $scope.select = function(page) {
							$scope.pageNum = page;
							var end, start;
				            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.dataSourceCol = $scope.filteredStores.slice(start, end);
				        }, 
				        $scope.onFilterChange = function() {
				            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
				        }, $scope.onNumPerPageChange = function(val) {
				        	$scope.numPerPage = val;
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.onOrderChange = function() {
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.search = function() {
				        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				        }, $scope.order = function(rowName) {
				        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
				        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				        $scope.numPerPage = $scope.numPerPageOpt[4], 
				        $scope.currentPage = 1, 
				        $scope.dataSourceCol = [],
				        ($scope.init = function() {
				            return $scope.search(), $scope.select($scope.currentPage);
				        })();
					}
			   });
			}
		
		$scope.getStatusObjectByDSId = function(dsId){
			return $scope.statusAndDsIdMap[dsId];
		};
		
		$scope.getStatusOfDataSource=function(dsId){
			var status ="";		
			if($scope.statusAndDsIdMap[dsId].status == "1" || $scope.statusAndDsIdMap[dsId].status.substr(0,6) == "Accept") {
				status = "Accepted";
			}
			if($scope.statusAndDsIdMap[dsId].status == "0" || $scope.statusAndDsIdMap[dsId].status.substr(0,4) == "Pend") {
				status = "Pending";
			}
			if($scope.statusAndDsIdMap[dsId].status == "-1" || $scope.statusAndDsIdMap[dsId].status.substr(0,6) == "Reject") {
				status = "Rejected";
			}
			return status;
		};
		
			$scope.getDataSourceList();
		
		 	$scope.attachDataRepos = function(dataRepo){
		 		localStorage.setItem("repoName" , dataRepo);
		 		$location.path("/dataRepoView");
		 	};
			
			$scope.moreDetail = function(dataSource, dataSourceObj){
				$routeParams.param1 = dataSourceObj.dataSourceName;
				 $routeParams.param3 = "popUp";
				 $routeParams.param6 = 1;
				 $routeParams.param4 = $modal.open({
					backdrop : 'static',
					keyboard : false, 
					size : "xl",
		            templateUrl: "/views/DataSource/dataSourceInfo.jsp",
		            resolve : {
		            	$routeParams : function(){
			           	 	return $routeParams;
			           	},
			           	myParam : function(){
			           	 	return $routeParams;
			           	}
			          }
				}),$routeParams.param4.result.then(function(reponame) {
					$scope.getDataSourceList(true);
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
			};
			
			$scope.moreDetailDr = function(dataRepo){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl: "/views/template/moreDetail.html",
					controller: "detailedPage",
	            		resolve : {
	            			dataSource : function(){
	            				return dataRepo;
	            			},
	            			indicator : function(){
	            				return "dataRepo";
	            			}
	            		}
				}),modalInstance.result.then(function(reponame) {
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			};
			
	}]).controller("dataSourceToAcceptRequestsCtrl" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll",  function($anchorScroll ,$filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,anchorSmoothScroll){
		
		$scope.dsLists = [];
		$scope.appLists = [];
		$scope.appId = $routeParams.param1;
		$scope.appName = $routeParams.param2;
		$scope.explanation = $routeParams.param15;
		$scope.hideTbl = true;
		$scope.oneAtATime = false;
		$scope.messageChanger = $routeParams.appIndicator;
		if($routeParams.param15 === undefined){
			$http.post("/getDataSourcesMsg").success(function(data, status, headers, config){
				$scope.explanation = data;
			});
		}

		$scope.dismissModel = function (){
			$routeParams.param1 = undefined;
			$routeParams.param15 = undefined;
			$routeParams.param4.dismiss('');
			$routeParams.param4 = undefined;
		},
	
		$scope.dsDecide = true;
		$scope.dsSource = function(){
			$scope.dsDecide = true;
		};
		
		/*$scope.getStatusOfDataSource=function(dsId){
			var status ="";		
			if($scope.statusAndDsIdMap[dsId] == 1) {
				status = "Accepted";
			}
			if($scope.statusAndDsIdMap[dsId] == 0) {
				status = "Pending";
			}
			if($scope.statusAndDsIdMap[dsId] == -1) {
				status = "Rejected";
			}
			return status;
		};*/
		
		$scope.$on("deleteAppDs" ,function (event, args) {
			$scope.deleteAppsLevelDs(args);
		});
		
		$scope.$on("myEvent2", function (event, args) {
			$scope.selectDSources(args[0] , args[1]);
		});
		
		$scope.$on("appEvent", function (event, args) {
			$scope.selectApps(args[0] , args[1]);
		});
		$scope.checkHeader = "existHeader";
		$scope.$on("myEvent1", function (event, args) {
			if(args == "Data sources created in flow" || args == "Stream sources created in flow"){
				$scope.dsDecide = false;
				$scope.checkHeader = "appLevelDSHeader";
			}
			else if(args == "Existing data sources"){
				$scope.dsDecide = true;
				$scope.checkHeader = "existHeader";
			}
			else if(args == "Inputs file(File parameters)"){
				$scope.dsDecide = null;
				$scope.checkHeader = "inputDs";
			}
			else if(args == "Node from other flows as head"){
				$scope.dsDecide = "App-nodes";
				$scope.checkHeader = "nodeHeader";
			}
			else if(args == "Existing flows"){
				$scope.dsDecide = "Apps";
				$scope.checkHeader = "importApp";
			}
			$scope.$broadcast("broadcastEvent" , $scope.dsDecide);
		});
		
		$scope.selectDSources = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.dataSourceId === dataSource.dataSourceId;});
			}
		};
		
		$scope.selectApps = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.appName === dataSource.appName;});
			}
			
		};
		
		$scope.selectNodes = function(node){
			for(var i=0; i<$scope.dsLists.length;i++){
				
				if($scope.dsLists[i].nodeId==node.nodeId){
					$scope.dsLists[i]=node;
					return;
				}
			}
			$scope.dsLists.push(node);
			
		};
		
		$scope.approveRequestWithConfirmation = function(dsi){
			
			$scope.approveWithConfirmation(dsi, "");
			/*var modalInstance;
			modalInstance = $modal.open({
				size : "lg",
	            templateUrl: "/views/template/dataSourceRequestAccessModal.html",
	            controller: "dataSourceRequestAcceptModelController",
	            resolve : {
	            	dataSourceObj : function(){
	            		return dsi;
	            	}
	            }
		     }),modalInstance.result.then(function(rejectReason) {
		    	 $scope.approveWithConfirmation(dsi, rejectReason);
		     }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	         });*/
		};
		
		$scope.approveWithConfirmation = function(dsi, desc){
			var reqDataObject = { requestId : dsi.id , justification: desc};
			$http({
				method: 'POST',
				url : "/acceptObjectRequest",
				data : reqDataObject
			}).success(function(data, status, headers, config){
				logger.logSuccess("Request accepted successfully");
				$rootScope.$broadcast('notificationUpdateEvent');
				$scope.getDataSourceList();
			}).error(function(data, status, headers, config){
				logger.logError("Unable to approve request. Reson : " + data)
			});
		};
		
		$scope.rejectRequest = function(dsi){
			var modalInstance;
			modalInstance = $modal.open({
				size : "lg",
	            templateUrl: "/views/template/dataSourceRequestRejectModal.html",
	            controller: "dataSourceRequestRejectModelController",
	            resolve : {
	            	dataSourceObj : function(){
	            		return dsi;
	            	}
	            }
		     }),modalInstance.result.then(function(rejectReason) {
		    	 $scope.rejectWithConfirmation(dsi, rejectReason);
		     }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	         });
		};
		
		$scope.rejectWithConfirmation = function(dsi, rejectReason) {
			var reqDataObject = {requestId : dsi.id , justification : rejectReason};
			$http({
				method: 'POST',
				url : "/rejectObjectRequest",
				data : reqDataObject
			}).success(function(data, status, headers, config){
				logger.logSuccess("Request rejected successfully");
				$rootScope.$broadcast('notificationUpdateEvent');
				$scope.getDataSourceList();
			}).error(function(data, status, headers, config){
				logger.logError("Unable to reject request. Reson : " + data)
			})
		} 
		
		$scope.confirmDsource= function(){
			if($scope.dsDecide=="Apps"){
				$routeParams.param4.close($scope.dsLists);
			}else{

				$routeParams.param1 = undefined;
				$routeParams.param4.close($scope.dsLists);
			}
		};
		
		$scope.gotoAnchor = function(repoName) {
			var id = $location.hash();
		    $location.hash(repoName);
		    $anchorScroll();
		    $location.hash(id);
	     };
		
	     $scope.deleteAppsLevelDs = function(dataSource){
	    	 $scope.dsLists = _.reject($scope.dsLists , function(val){return dataSource.id === val.id;});
	    	 $scope.deleteDataSource(null , dataSource.dataSourceId);
	     };
	     
	     
	     $scope.globalJob = function(dataSoourceToRun){
	    	 
	    	 if(dataSoourceToRun.dataSourceType == null || dataSoourceToRun.dataSourceType == "null" || dataSoourceToRun.dataSourceType == undefined){
	    		 return;
	    	 }
	    	 
	    	 $http.post("/getRepoInstance",dataSoourceToRun.dataRepoName).success(function(data, status, headers, config){
	    		 data.dataSourceIds = [];
	    		 data.dataSourceIds.push(dataSoourceToRun.dataSourceId);
	    		 var modalInstance;
					modalInstance = $modal.open({
						size : "lg",
						templateUrl: "/views/template/runPipeLineTemplate.html",
						controller: "dataSourceRunPipeLineController",
						resolve : {
							store : function(){
								return data;
							}
						}
					}),modalInstance.result.then(function(dsList) {
					}, function() {
						$log.info("Modal dismissed at: " + new Date);
				});
	    	 });
	     },
	     $scope.startJob = function(){
	    	 if($scope.repoName != undefined){
	    		 $http.get("/getRepoInstance",$scope.repoName).success(function(data, status, headers, config){
	    			 data.dataSourceIds = [];
	    			 var i = 0;
	    			 _.each($scope.checkBox , function(val){
	    				 if(val == true){
	    					 data.dataSourceIds.push($scope.dataSourceOfRepo[i].dataSourceId);
	    				 } 
	    				 i++;
	    			 });
	    			 if(data.dataSourceIds.length > 0){
	    				 var modalInstance;
	 						modalInstance = $modal.open({
	 							size : "lg",
	 							templateUrl: "/views/template/runPipeLineTemplate.html",
	 							controller: "dataSourceRunPipeLineController",
	 							resolve : {
	 								store : function(){
	 									return data;
	 								}
	 							}	
	 						}),modalInstance.result.then(function(dsList) {

	 						}, function() {
	 							$log.info("Modal dismissed at: " + new Date);
	 						});
	    		 		}else{
	    		 			logger.logError("Please select at least one Data-source");
	    		 		}
	    	 		});
	    	
	    	 	}
		};
		
		$scope.repoListClick = function(repoName){
			$location.path("/dataRepoOperations/"+repoName);
		};
		
		$scope.openDataProfiling = function(ds){
			if(ds.dataSourceType == null || ds.dataSourceType == "null" || ds.dataSourceType == undefined){
				return;
			}
			
			$http({
				method: 'GET',
				url : "/getDataProfileJobInstances?profileType="+'PreProf'+"&dsId="+ds.dataSourceId
			}).
			success(function(data, status, headers, config){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl : "/views/template/DataProfileModal.html",
			        controller: "dataProfileController",
			        resolve : {
			        	 store : function(){
			        		return data; 
			        	 },
			        	 profileType : function(){
							return null; 
						 }
				    }
				}),modalInstance.result.then(function(reponame) {
		        }, function() {
		           	$log.info("Modal dismissed at: " + new Date);
		        });
			}).error(function(data){
				logger.logError(data);
			});
		};
		
		$scope.renameDataSource = function(dataSource , indicator){
			if(indicator == "dataSource"){
				var ds = angular.copy(dataSource);
				ds.nodeIndicate = undefined; 
				
				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/rename.html",
		            controller: "renameController",
		            	resolve : {
		            		checkSource : function(){
			            	 	return indicator;
			            	},
			            	data : function(){
			            	 	return ds;
			            	}
			            }
				}),modalInstance.result.then(function(dataSourceInstance) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == dataSourceInstance.dataSourceId){
								$scope.dataSourceCol[i] = dataSourceInstance;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								break;
							}
							
						}
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
		
			}else{
			}
		},
		
		$scope.check = function(value){
			var i = 0;
			_.each($scope.checkBox , function(val){
				$scope.checkBox[i] = value;
				i++;
			});
		};
		
		$scope.finalSet = function(val){
			if(val === false)
				$scope.finalValue = false;
			else{
				var bool = true;
				for(var i = 0 ; i < $scope.checkBox.length ; i++){
					if($scope.checkBox[i] == false){
						bool = false;
						break;
					}
					
				}
				if(bool == true)
					$scope.finalValue = bool;
			}
				
		};
		
		$scope.assignDataSource = function(dataSource){
			var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/assignDataSource.html",
		            controller: "assignController",
		            	resolve : {
			            	data : function(){
			            	 	return angular.copy(dataSource);
			            	}
			            }
				}),modalInstance.result.then(function(updatedDataSource) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == updatedDataSource.dataSourceId){
								$scope.dataSourceCol[i] = updatedDataSource;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								if($scope.dataSourceOfRepo[i].dataRepoId === $scope.dataSourceCol[index].dataRepoId)
									$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								else
									$scope.dataSourceOfRepo.splice(i , 1);
								
								break;
							}		
						}
					
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
			});
		
		},
		$scope.dataSourcePopup = function(dataSourceName, dsId){
			if($scope.appId !== undefined)
				return false;
				
			 $routeParams.param1 = dataSourceName;
			 $routeParams.param6 = 1;
			 $routeParams.param7 = dsId;
			 /*$routeParams.param4 = $modal.open({
				backdrop : 'static',
				keyboard : false, 
				size : "xl",
	            templateUrl: "/views/DataSource/dataSource.jsp",
	            resolve : {
	            	$routeParams : function(){
		           	 	return $routeParams;
		           	},
		           	myParam : function(){
		           	 	return $routeParams;
		           	}
		          }
			}),$routeParams.param4.result.then(function(reponame) {
				$scope.getDataSourceList(true);
           }, function() {
            $log.info("Modal dismissed at: " + new Date);
          });*/
			 $location.path("/datasource/"+$routeParams.param1);
		},
		$scope.deleteDataSource = function(index , dataSourceId , dataSourceName){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:dataSourceName, type: "datasource"};
					}
				}
			}),modalInstance.result.then(function() {

				if($scope.appId === undefined){
						if(index !== null){
							dataSourceId = angular.copy($scope.dataSourceCol[index].dataSourceId);
							$scope.dataSourceCol.splice(index , 1);
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							if($scope.dataSourceOfRepo !== undefined || $scope.dataSourceOfRepo !== null || $scope.dataSourceOfRepo.length > 0)
								$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
						if(dataSourceId !== null && index == null){
							$scope.dataSourceCol = _.reject(angular.copy($scope.dataSourceCol) , function(val){return val.dataSourceId === dataSourceId;});
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
					}
					$http({
						method: 'POST',
						url : "/deleteDataSource",
						data : dataSourceId
					}).
					success(function(data, status, headers, config){
						logger.logSuccess("Data-source deleted successfully");
					}).error(function(){
						logger.logError(data);
					});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
			
			
		};
		$scope.statusList = [];
		$scope.statusAndDsIdMap = {};
		$scope.getDataSourceList = function () {
			$http.post("/getApprovalObjects").success(function(data, status, headers, config){
					$scope.statusList = data.requestIndicator;
					var indexIs = 0;
					_.each(data.requestedObjects, function(dsInst){
						$scope.statusAndDsIdMap[dsInst.id] = $scope.statusList[indexIs];
						indexIs++;
					});
					$scope.dataSourceCol = _.filter(data.requestedObjects, function(val){if(val !== null){return val.appId == null;}});
					$scope.dataSourceColBkp = angular.copy($scope.dataSourceCol); 
					if(data){
						return $scope.stores = $scope.dataSourceCol	
				        , $scope.searchKeywords = "", 
				        $scope.filteredStores = [],
				        $scope.row = "", 
				        $scope.select = function(page) {
							$scope.pageNum = page;
							var end, start;
				            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.dataSourceCol = $scope.filteredStores.slice(start, end);
				        }, 
				        $scope.onFilterChange = function() {
				            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
				        }, $scope.onNumPerPageChange = function(val) {
				        	$scope.numPerPage = val;
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.onOrderChange = function() {
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.search = function() {
				        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				        }, $scope.order = function(rowName) {
				        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
				        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				        $scope.numPerPage = $scope.numPerPageOpt[4], 
				        $scope.currentPage = 1, 
				        $scope.dataSourceCol = [],
				        ($scope.init = function() {
				            return $scope.search(), $scope.select($scope.currentPage);
				        })();
					}
			   });
			}
		
			$scope.getDataSourceList();
			
			$scope.getStatusObjectByDSId = function(dsId){
				return $scope.statusAndDsIdMap[dsId];
			};
			
			$scope.getStatusOfDataSource=function(dsId){
				var status ="";
				if($scope.statusAndDsIdMap[dsId].status == "1" || $scope.statusAndDsIdMap[dsId].status.substr(0,6) == "Accept") {
					status = "Accepted";
				}
				if($scope.statusAndDsIdMap[dsId].status == "0" || $scope.statusAndDsIdMap[dsId].status.substr(0,4) == "Pend") {
					status = "Pending";
				}
				if($scope.statusAndDsIdMap[dsId].status == "-1" || $scope.statusAndDsIdMap[dsId].status.substr(0,6) == "Reject") {
					status = "Rejected";
				}
				return status;
			};
		
		 	$scope.attachDataRepos = function(dataRepo){
		 		localStorage.setItem("repoName" , dataRepo);
		 		$location.path("/dataRepoView");
		 	};
			
			$scope.moreDetail = function(dataSource, dataSourceObj){
				$routeParams.param1 = dataSourceObj.dataSourceName;
				 $routeParams.param3 = "popUp";
				 $routeParams.param6 = 1;
				 $routeParams.param4 = $modal.open({
					backdrop : 'static',
					keyboard : false, 
					size : "xl",
		            templateUrl: "/views/DataSource/dataSourceInfo.jsp",
		            resolve : {
		            	$routeParams : function(){
			           	 	return $routeParams;
			           	},
			           	myParam : function(){
			           	 	return $routeParams;
			           	}
			          }
				}),$routeParams.param4.result.then(function(reponame) {
					$scope.getDataSourceList(true);
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
			};
			
			$scope.moreDetailDr = function(dataRepo){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl: "/views/template/moreDetail.html",
					controller: "detailedPage",
	            		resolve : {
	            			dataSource : function(){
	            				return dataRepo;
	            			},
	            			indicator : function(){
	            				return "dataRepo";
	            			}
	            		}
				}),modalInstance.result.then(function(reponame) {
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			};
			
	}]).controller("myCollectionController" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll", "$interval", "$cookieStore",  function($anchorScroll ,$filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,anchorSmoothScroll, $interval, $cookieStore){
		
		//RELATIONSHIPS
		$scope.showCollectionsGraph=0;
		
		$scope.requestCollectionGraphData=function(){
			$scope.showCollectionsGraph=1;
			
			
			//dataSourceId
			var dsIds="";
			angular.forEach($scope.dataSourceCol, function(obj) {1 
				dsIds=dsIds+""+obj.dataSourceId+",";
			});
			
			$http.post("/relationship/getCollectionDsDagData",dsIds).success(function(data){
				$scope.collectionDagData=data;
			}).error(function(data){
			})
		}
		
		$scope.deleteCollectionDS=function(dataSourceId){
			//Remove from My Collection
			var reqDataObject = { objectId : dataSourceId , objectType : DATA_SOURCE};
			$http({
				method : "post",
				url : "/removeFromMyCollection",
				data : reqDataObject
			}).success(function(data){
				if(data=="success"){
					logger.logSuccess("Removed successfully");	
					$rootScope.removeFromCollection = true;
					$rootScope.refreshCollectionList = true;
				}else{
					logger.logError("failed to save");
				}
			}).error(function(status){
				logger.logError(status);
			});
		}
		
		
		
		$scope.dsLists = [];
		$scope.appLists = [];
		$scope.appId = $routeParams.param1;
		$scope.appName = $routeParams.param2;
		$scope.explanation = $routeParams.param15;
		$scope.hideTbl = true;
		$scope.oneAtATime = false;
		$scope.messageChanger = $routeParams.appIndicator;
		if($routeParams.param15 === undefined){
			$http.post("/getDataSourcesMsg").success(function(data, status, headers, config){
				$scope.explanation = data;
			});
		}

		$scope.dismissModel = function (){
			$routeParams.param1 = undefined;
			$routeParams.param15 = undefined;
			$routeParams.param4.dismiss('');
			$routeParams.param4 = undefined;
		};
	
		$interval(function() {
			$scope.currentGroupId = $cookieStore.get("groupId");
			if($rootScope.removeFromCollection || $rootScope.refreshCollectionList) {
				$rootScope.refreshCollectionList = false;
				$rootScope.removeFromCollection = false;
				$scope.getDataSourceList();
				$scope.getDataSourceList();
			}
		}, 1000)
	
		$scope.dsDecide = true;
		$scope.dsSource = function(){
			$scope.dsDecide = true;
		};
		
		$scope.$on("deleteAppDs" ,function (event, args) {			
			$scope.deleteAppsLevelDs(args);
		});
		
		$scope.$on("myEvent2", function (event, args) {
			$scope.selectDSources(args[0] , args[1]);
		});
		
		$scope.$on("appEvent", function (event, args) {
			$scope.selectApps(args[0] , args[1]);
		});
		$scope.checkHeader = "existHeader";
		$scope.$on("myEvent1", function (event, args) {
			if(args == "Data sources created in flow" || args == "Stream sources created in flow"){
				$scope.dsDecide = false;
				$scope.checkHeader = "appLevelDSHeader";
			}
			else if(args == "Existing data sources"){
				$scope.dsDecide = true;
				$scope.checkHeader = "existHeader";
			}
			else if(args == "Inputs file(File parameters)"){
				$scope.dsDecide = null;
				$scope.checkHeader = "inputDs";
			}
			else if(args == "Node from other flows as head"){
				$scope.dsDecide = "App-nodes";
				$scope.checkHeader = "nodeHeader";
			}
			else if(args == "Existing flows"){
				$scope.dsDecide = "Apps";
				$scope.checkHeader = "importApp";
			}
			$scope.$broadcast("broadcastEvent" , $scope.dsDecide);
		});
		
		$scope.selectDSources = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.dataSourceId === dataSource.dataSourceId;});
			}
		};
		
		$scope.selectApps = function(dataSource, check){
			if(check === true){
				$scope.dsLists.push(dataSource);
			}else{
				$scope.dsLists = _.reject($scope.dsLists , function(val){return val.appName === dataSource.appName;});
			}
			
		};
		
		$scope.selectNodes = function(node){
			for(var i=0; i<$scope.dsLists.length;i++){
				
				if($scope.dsLists[i].nodeId==node.nodeId){
					$scope.dsLists[i]=node;
					return;
				}
			}
			$scope.dsLists.push(node);
			
		};
		
		
		$scope.confirmDsource= function(){
			if($scope.dsDecide=="Apps"){
				$routeParams.param4.close($scope.dsLists);
			}else{

				$routeParams.param1 = undefined;
				$routeParams.param4.close($scope.dsLists);
			}
		};
		
		$scope.gotoAnchor = function(repoName) {
			var id = $location.hash();
		    $location.hash(repoName);
		    $anchorScroll();
		    $location.hash(id);
	     };
		
	     $scope.deleteAppsLevelDs = function(dataSource){
	    	 $scope.dsLists = _.reject($scope.dsLists , function(val){return dataSource.id === val.id;});
	    	 $scope.deleteDataSource(null , dataSource.dataSourceId);
	     };
	     
	     
	     $scope.globalJob = function(dataSoourceToRun){
	    	 
	    	 if(dataSoourceToRun.dataSourceType == null || dataSoourceToRun.dataSourceType == "null" || dataSoourceToRun.dataSourceType == undefined){
	    		 return;
	    	 }
	    	 
	    	 $http.post("/getRepoInstance",dataSoourceToRun.dataRepoName).success(function(data, status, headers, config){
	    		 data.dataSourceIds = [];
	    		 data.dataSourceIds.push(dataSoourceToRun.dataSourceId);
	    		 var modalInstance;
					modalInstance = $modal.open({
						size : "lg",
						templateUrl: "/views/template/runPipeLineTemplate.html",
						controller: "dataSourceRunPipeLineController",
						resolve : {
							store : function(){
								return data;
							}
						}
					}),modalInstance.result.then(function(dsList) {
					}, function() {
						$log.info("Modal dismissed at: " + new Date);
				});
	    	 });
	     },
	     $scope.startJob = function(){
	    	 if($scope.repoName != undefined){
	    		 $http.post("/getRepoInstance",$scope.repoName).success(function(data, status, headers, config){
	    			 data.dataSourceIds = [];
	    			 var i = 0;
	    			 _.each($scope.checkBox , function(val){
	    				 if(val == true){
	    					 data.dataSourceIds.push($scope.dataSourceOfRepo[i].dataSourceId);
	    				 } 
	    				 i++;
	    			 });
	    			 if(data.dataSourceIds.length > 0){
	    				 var modalInstance;
	 						modalInstance = $modal.open({
	 							size : "lg",
	 							templateUrl: "/views/template/runPipeLineTemplate.html",
	 							controller: "dataSourceRunPipeLineController",
	 							resolve : {
	 								store : function(){
	 									return data;
	 								}
	 							}	
	 						}),modalInstance.result.then(function(dsList) {

	 						}, function() {
	 							$log.info("Modal dismissed at: " + new Date);
	 						});
	    		 		}else{
	    		 			logger.logError("Please select at least one Data-source");
	    		 		}
	    	 		});
	    	
	    	 	}
		};
		
		$scope.repoListClick = function(repoName){
			$location.path("/dataRepoOperations/"+repoName);
		};
		
		$scope.openDataProfiling = function(ds){
			if(ds.dataSourceType == null || ds.dataSourceType == "null" || ds.dataSourceType == undefined){
				return;
			}
			
			$http({
				method: 'GET',
				url : "/getDataProfileJobInstances?profileType="+'PreProf'+"&dsId="+ds.dataSourceId
			}).
			success(function(data, status, headers, config){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl : "/views/template/DataProfileModal.html",
			        controller: "dataProfileController",
			        resolve : {
			        	 store : function(){
			        		return data; 
			        	 },
			        	 profileType : function(){
							return null; 
						 }
				    }
				}),modalInstance.result.then(function(reponame) {
		        }, function() {
		           	$log.info("Modal dismissed at: " + new Date);
		        });
			}).error(function(data){
				logger.logError(data);
			});
		};
		
		$scope.renameDataSource = function(dataSource , indicator){
			if(indicator == "dataSource"){
				var ds = angular.copy(dataSource);
				ds.nodeIndicate = undefined; 
				
				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/rename.html",
		            controller: "renameController",
		            	resolve : {
		            		checkSource : function(){
			            	 	return indicator;
			            	},
			            	data : function(){
			            	 	return ds;
			            	}
			            }
				}),modalInstance.result.then(function(dataSourceInstance) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == dataSourceInstance.dataSourceId){
								$scope.dataSourceCol[i] = dataSourceInstance;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								break;
							}
							
						}
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
		
			}else{
			}
		},
		
		$scope.check = function(value){
			var i = 0;
			_.each($scope.checkBox , function(val){
				$scope.checkBox[i] = value;
				i++;
			});
		};
		
		$scope.finalSet = function(val){
			if(val === false)
				$scope.finalValue = false;
			else{
				var bool = true;
				for(var i = 0 ; i < $scope.checkBox.length ; i++){
					if($scope.checkBox[i] == false){
						bool = false;
						break;
					}
					
				}
				if(bool == true)
					$scope.finalValue = bool;
			}
				
		};
		
		$scope.assignDataSource = function(dataSource){
			var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/assignDataSource.html",
		            controller: "assignController",
		            	resolve : {
			            	data : function(){
			            	 	return angular.copy(dataSource);
			            	}
			            }
				}),modalInstance.result.then(function(updatedDataSource) {
					var index = -1; 
					if($scope.dataSourceCol !== undefined && $scope.dataSourceCol !== null) 
						for(var i = 0; i < $scope.dataSourceCol.length ; i++){
							if($scope.dataSourceCol[i].dataSourceId == updatedDataSource.dataSourceId){
								$scope.dataSourceCol[i] = updatedDataSource;
								index = i;
								break;
							}
							
						}
					
					if($scope.dataSourceOfRepo !== undefined && $scope.dataSourceOfRepo !== null && index != -1) 
						for(var i = 0; i < $scope.dataSourceOfRepo.length ; i++){
							if($scope.dataSourceOfRepo[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								if($scope.dataSourceOfRepo[i].dataRepoId === $scope.dataSourceCol[index].dataRepoId)
									$scope.dataSourceOfRepo[i] = $scope.dataSourceCol[index];
								else
									$scope.dataSourceOfRepo.splice(i , 1);
								
								break;
							}		
						}
					
					if($scope.stores !== undefined && $scope.stores !== null && index != -1)
						for(var i = 0; i < $scope.stores.length ; i++){
							if($scope.stores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.stores[i] = $scope.dataSourceCol[index];
								break;
							}
						
						}
					
					if($scope.filteredStores !== undefined && $scope.filteredStores !== null && index != -1)
						for(var i = 0; i < $scope.filteredStores.length ; i++){
							if($scope.filteredStores[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.filteredStores[i] = $scope.dataSourceCol[index];
								break;
							}
						}
					
					if($scope.dataSourceColBkp !== undefined && $scope.dataSourceColBkp !== null && index != -1)
						for(var i = 0; i < $scope.dataSourceColBkp.length ; i++){
							if($scope.dataSourceColBkp[i].dataSourceId == $scope.dataSourceCol[index].dataSourceId){
								$scope.dataSourceColBkp[i] = $scope.dataSourceCol[index];
								break;
							}
						}
			});
		
		},
		$scope.dataSourcePopup = function(dataSourceName, dsId, groupId){
			if($scope.appId !== undefined)
				return false;
				
			
			if(groupId != $scope.currentGroupId)
				return false;
			
			 $routeParams.param1 = dataSourceName;
			 $routeParams.param6 = 1;
			 $routeParams.param7 = dsId;
			 $routeParams.param4 = $modal.open({
				backdrop : 'static',
				keyboard : false, 
				size : "xl",
	            templateUrl: "/views/DataSource/dataSourceWithoutPopup.jsp",
	            resolve : {
	            	$routeParams : function(){
		           	 	return $routeParams;
		           	},
		           	myParam : function(){
		           	 	return $routeParams;
		           	}
		          }
			}),$routeParams.param4.result.then(function(reponame) {
				$scope.getDataSourceList(true);
           }, function() {
            $log.info("Modal dismissed at: " + new Date);
          });
		},
		$scope.deleteDataSource = function(index , dataSourceId , dataSourceName){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:dataSourceName, type: "datasource"};
					}
				}
			}),modalInstance.result.then(function() {

				if($scope.appId === undefined){
						if(index !== null){
							dataSourceId = angular.copy($scope.dataSourceCol[index].dataSourceId);
							$scope.dataSourceCol.splice(index , 1);
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							if($scope.dataSourceOfRepo !== undefined || $scope.dataSourceOfRepo !== null || $scope.dataSourceOfRepo.length > 0)
								$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
						if(dataSourceId !== null && index == null){
							$scope.dataSourceCol = _.reject(angular.copy($scope.dataSourceCol) , function(val){return val.dataSourceId === dataSourceId;});
							$scope.filteredStores = _.reject($scope.filteredStores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.stores = _.reject($scope.stores , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceColBkp = _.reject($scope.dataSourceColBkp , function(val){return val.dataSourceId === dataSourceId;});
							$scope.dataSourceOfRepo = _.reject($scope.dataSourceOfRepo , function(val){return val.dataSourceId === dataSourceId;});
						}
					}
					$http({
						method: 'POST',
						url : "/deleteDataSource",
						data : dataSourceId
					}).
					success(function(data, status, headers, config){
						logger.logSuccess("Data-source deleted successfully");
					}).error(function(){
						logger.logError(data);
					});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
			
			
		};
		
		$scope.statusList = [];
		$scope.getDataSourceList = function () {
			$http.post("/getAllMyCollection").success(function(data, status, headers, config){
					$scope.dataSourceCol = _.filter(data, function(val){if(val !== null){return val.appId == null;}});
					$scope.dataSourceColBkp = angular.copy($scope.dataSourceCol); 
					if(data){
						return $scope.stores = $scope.dataSourceCol	
				        , $scope.searchKeywords = "", 
				        $scope.filteredStores = [],
				        $scope.row = "", 
				        $scope.select = function(page) {
							$scope.pageNum = page;
							var end, start;
				            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.dataSourceCol = $scope.filteredStores.slice(start, end);
				        }, 
				        $scope.onFilterChange = function() {
				            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
				        }, $scope.onNumPerPageChange = function(val) {
				        	$scope.numPerPage = val;
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.onOrderChange = function() {
				            return $scope.select(1), $scope.currentPage = 1;
				        }, $scope.search = function() {
				        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
				        }, $scope.order = function(rowName) {
				        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
				        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
				        $scope.numPerPage = $scope.numPerPageOpt[4], 
				        $scope.currentPage = 1, 
				        $scope.dataSourceCol = [],
				        ($scope.init = function() {
				            return $scope.search(), $scope.select($scope.currentPage) , $scope.order("-id") ;
				        })();
					}
			   });
			}
		
			$scope.getDataSourceList();
		
		 	$scope.attachDataRepos = function(dataRepo){
		 		localStorage.setItem("repoName" , dataRepo);
		 		$location.path("/dataRepoView");
		 	};
			
			$scope.moreDetail = function(dataSource, dataSourceObj){
				$routeParams.param1 = dataSourceObj.dataSourceName;
				 $routeParams.param3 = "popUp";
				 $routeParams.param6 = 1;
				 $routeParams.param7 = dataSourceObj.dataSourceId;
				 $routeParams.param4 = $modal.open({
					backdrop : 'static',
					keyboard : false, 
					size : "xl",
		            templateUrl: "/views/DataSource/dataSourceInfo.jsp",
		            resolve : {
		            	$routeParams : function(){
			           	 	return $routeParams;
			           	},
			           	myParam : function(){
			           	 	return $routeParams;
			           	}
			          }
				}),$routeParams.param4.result.then(function(reponame) {
					$scope.getDataSourceList(true);
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
			};
			
			$scope.moreDetailDr = function(dataRepo){
				var modalInstance;
				modalInstance = $modal.open({
					size : "xl",
					templateUrl: "/views/template/moreDetail.html",
					controller: "detailedPage",
	            		resolve : {
	            			dataSource : function(){
	            				return dataRepo;
	            			},
	            			indicator : function(){
	            				return "dataRepo";
	            			}
	            		}
				}),modalInstance.result.then(function(reponame) {
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			};
			
	}]).controller("dataSourceRequestRejectModelController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","dataSourceObj",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger,dataSourceObj){
		$scope.dsName = dataSourceObj.objectName;
		//$scope.dsParentGroup = dataSourceObj.fromGroup;
		
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
		};
		
		$scope.rejectDataSourceRequest = function(rejectReason){
			if(null != $scope.rejectReason && undefined != $scope.rejectReason && $scope.rejectReason.trim()!=""){
				$modalInstance.close(rejectReason);
			}else{
				$scope.rejectReason ="";
			}
		};
	}]).controller("dataSourceRequestAcceptModelController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","dataSourceObj",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger,dataSourceObj){
		$scope.dsName = dataSourceObj.dataSourceName;
		$scope.dsParentGroup = dataSourceObj.fromGroup;
		
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
		};
		
		$scope.approveDataSourceRequest = function(rejectReason){
			if(null != $scope.justification && undefined != $scope.justification){
				$modalInstance.close(rejectReason);
			}else{
				$scope.justification = "";
			}
		};
	}]).controller("checkVersionFieldMappingController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","newFieldMappings" , "existingFieldMappings", "from","StatusFileTypeMatch",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger,newFieldMappings , existingFieldMappings,from,StatusFileTypeMatch){
	
	$scope.fieldMappingStatus = false;
	if(from){
		$scope.from = from; 
	}
	if(newFieldMappings){
		$scope.newFieldMappings= newFieldMappings;	
	}
	if(existingFieldMappings){
		$scope.existingFieldMappings =existingFieldMappings;
	}

	if(StatusFileTypeMatch){
		$scope.statusFileTypeMatch = 1;
	}else{
		$scope.statusFileTypeMatch = 0;
	}
	$scope.cancel = function() {
        $modalInstance.close("OK");
	};

}]);

}).call(this);

function clearAllDSLinker($scope){
	$scope.fieldvalues = [0];
	$scope.fieldMapping1 = [];
	$scope.fieldMapping2 = [];
	$scope.datasourcelink1 = "";
	$scope.datasourcelink2 = "";
}

function containsinDs(id, dslist){
	notinlist = true;
	angular.forEach(dslist, function(key,value){
		if(key.id == id){
			notinlist = false;
		}
	});
	return notinlist;
}

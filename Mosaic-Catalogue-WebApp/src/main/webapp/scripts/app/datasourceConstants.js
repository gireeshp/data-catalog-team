(function() {
    'use strict';

    angular
        .module('app.datasource')
        .constant("FLOWCHART_CONSTANTS",{
			"DATABASE_FUNCTION_TYPES":{
				"MATH"		:1,
				"STRING"	:2,
				"DATE"		:3,
				"AGGREGATE"	:4,
				"NUMERIC"	:5, 
				"FX"		:6
			},
			
			"MODEL_INSTANCE" : {
				"TEMPLATE_URL" : "templateUrl",
				"CONTROLLER" : "controller"
			},
			
			"MATHEMATICAL_OPERATION":{
				"ADDITION"			:"+",
				"SUBSTRACTION"		:"-",
				"MULTIPLICATION"	:"*",
				"DIVISION"			:"/",
				"OPENING_BRACKET"	:"(",
				"CLOSING_BRACKET"	:")",
				"LESS_THAN"			:"<",
				"GREATER_THAN"		:">",
				"IS_EQUAL"			:"==",
				"IS_NOT_EQUAL"		:"!=",
				"GREATER_THAN_EQUAL":">=",
				"LESS_THAN_EQUAL"	:"<="
			},
			"NODES":{
				"NODE_FOLDER_LOCATION" 		: "Refactored/App/analysis-apps/workflow/process",
				"ADD_VARIABLE_BINNING"		:"add_var_binning",
				"ADD_VARIABLE_MATH"			:"add_var_math",
				"ADD_VARIABLE_SEGMENT"		:"add_var_segment",
				"CO_LOCATE"					:"node_co_locate",
				"CREATE_VARIABLES"			: 
				{
							NODE_TYPE 		: "node_create_variables",
							TEMPLATE_URL 	: "create-variable/create_variable_view.jsp",
							CONTROLLER		: "CreateVariableController"
				},
				"CUSTOMIZED_APPLICATION"	:"node_custom_application",
				"CUSTOMIZED_JAVA"			:"node_custom_java",
				"CUSTOMIZED_PROCESS"		:"node_custom_processes",
				"CUSTOMIZED_QUERY"			:"node_Custom_Query",
				"CUSTOMIZED_SHELL_SCRIPT"	:"node_custom_shell_script",
				"CUSTOMIZED_MR"				:"node_Custom_MR",
				"DATASOURCE"				:"node_datasource",
				"FILTERS"					:"node_filters",
				"GROUP_BY"					:"node_group_by",
				"JOIN"						:"node_join",
				"JOIN_M"					:"node_mJoin",
				"LOOKUP"					:"node_look_up",
				"RDBMS"						:"node_rdbms",
				"SAVE_TO_SERVER"			:"node_save_to_server",
				"SELECT"					:"node_select",
				"SORT"						:"node_sort",
				"TRANSPOSE"					:"node_transpose",
				"EVALUATE_EXPRESSION"		:"evaluate_expression"
			},
			"NODE_TYPES":{
				"ADD_VARIABLE_BINNING"		:"add_var_binning",
				"ADD_VARIABLE_MATH"			:"add_var_math",
				"ADD_VARIABLE_SEGMENT"		:"add_var_segment",
				"CO_LOCATE"					:"node_co_locate",
				"CREATE_VARIABLES"			:"node_create_variables",
				"CUSTOMIZED_APPLICATION"	:"node_custom_application",
				"CUSTOMIZED_JAVA"			:"node_custom_java",
				"CUSTOMIZED_PROCESS"		:"node_custom_processes",
				"CUSTOMIZED_QUERY"			:"node_Custom_Query",
				"CUSTOMIZED_SHELL_SCRIPT"	:"node_custom_shell_script",
				"CUSTOMIZED_MR"				:"node_Custom_MR",
				"DATASOURCE"				:"node_datasource",
				"FILTERS"					:"node_filters",
				"GROUP_BY"					:"node_group_by",
				"JOIN"						:"node_join",
				"JOIN_M"					:"node_mJoin",
				"LOOKUP"					:"node_look_up",
				"RDBMS"						:"node_rdbms",
				"SAVE_TO_SERVER"			:"node_save_to_server",
				"SELECT"					:"node_select",
				"SORT"						:"node_sort",
				"TRANSPOSE"					:"node_transpose",
				"EVALUATE_EXPRESSION"		:"evaluate_expression"
			},
			"PROCESS_TYPES":{
				"ADD_VARIABLE" : "add_variable"
			},
			"TEMPLATE_URL":{
				"APP_DEPENDENCIES_MODEL"	:"/views/template/AppDependenciesModal.html",
				"APP_NODE_CONFORMATION"		:"/views/template/applicationNodeConfirmation.html",
				"COLOCATE_MODEL_TEMPLATE"	:"/views/template/CoLocateModalTemplate.html",
				"CREATE_CANVAS_MODEL"		:"/views/template/",
				"CREATE_CONNECTION"			:"/views/template/createConnection.html",
				"DATASOURCE"				:"/views/DataSource/dataSource.jsp",
				"WORKFLOW"					:"/views/template/workflow.html",
				"CREATE_VARIABLE"			: "create-variable/create_variable_view.jsp"
			},
			"STRING":{
				"FIXED_VALUE" 	: "Fixed Value",
				"CALCULATED" 	: "calculated",
				"BINNING" 		: "binning",
				"MATH"			: "Math",
				"PROCESS_LOADING": "processLoading",
				"ARE_YOU_SURE"	: 'Sure?',
				"UP"			: 'up',
				"DOWN"			: 'down',
				"QUERY"			: 'Query'
			},

			"DATA_TYPES":{
				"DATE"		:"DATE",
				"STRING"	:"STRING",
				"INTEGER"	:"INTEGER",
				"DOUBLE"	:"DOUBLE",
				"LONG"		:"LONG",
				"BOOLEAN"	:"BOOLEAN",
				"NUMBER"	:"NUMBER"
			},
			
			"MESSAGES":{
				"VARIABLE_EXISTS" 			: "Variable with same name already exist.",
				"NO_SPACES"					: "Please dont provide any white spaces.",
				"ADD_OUTPUT_FIELDS"			: "Please add the output fields",
				"SELECT_CONNECTION"			: "Please select the connection details.",
				"SELECT_LOAD_STRATEGY_TYPE"	: "Please select the Load Strategy type",
				"SPECIFY_TABLE_NAME"		: "Please specify the table name for rdbms export",
				"NO_SPACE_TABLE_NAME"		: "Table name should not have a space",
				"NO_SPECIAL_CHARACTER"		: "Table name should not contain the special character",
				"NO_SPECIAL_CHARACTER_EXC_UNDERSCORE"
											: "No special characters allows other than underscore in variable name",
				"PROCESS_LOADING"			: "processLoading",
				"FIELD_ALREADY_SELECTED"	: "field is already selected for this operation."	
			}
		}
    );
})();
(function(){
	"use strict";

	var module = angular.module("app.detailDashboard",[]);
	module.service("DashboardService", function($http){
		function  getDashboard(url){
			return $http.post(url).then(function(response){
	            return response.data;
	        });
		};
		return {
			getAllDashboard : getDashboard
		};
	}),
	module.service("CleanHdfsFiles", function($http){
		function  cleanUpHdfsUnusedFiles(url){
			return $http.get(url).then(function(response){
	            return response.data;
	        });
		};
		return {
			cleanHdfs : cleanUpHdfsUnusedFiles
		};
	}),
	module.controller("DashboardCtrl" , ["$scope",  "DashboardService" , "logger"  ,"CleanHdfsFiles","$http",function($scope, DashboardService, logger,CleanHdfsFiles,$http){
		var target1 = document.getElementById('cpuChart'),
			target2 = document.getElementById('ramChart'),
			opts = {
				  lines: 12, 
				  angle: 0, 
				  lineWidth: 0.44, 
				  pointer: {
				    length: 0.6, 
				    strokeWidth: 0.03,
				    color: '#000000'
				  },
				  limitMax: 'false',  
				  colorStart: '#6FADCF',   
				  colorStop: '#8FC0DA',    
				  strokeColor: '#E0E0E0',  
				  generateGradient: true,
				  percentColors: [
				                    [0, "#7ACBEE"],
				                    [1, "#7ACBEE"]
				                ]
				};
				$scope.easypiechart3 = {
		            options: {
		                animate: {
		                    duration: 1e3,
		                    enabled: !0
		                },
		                barColor: "#2EC1CC",
		                lineCap: "square",
		                size: 180,
		                lineWidth: 20,
		                scaleLength: 0
		            }
		     };
				$scope.easypiechart4 = {
			            options: {
			                animate: {
			                    duration: 1e3,
			                    enabled: !0
			                },
			                barColor: "#2EC1CC",
			                lineCap: "square",
			                size: 180,
			                lineWidth: 20,
			                scaleLength: 0
			            }
			     };
		
		$scope.CleanUpHdfcSpace = function(){
			CleanHdfsFiles.cleanHdfs("/getCleanUpHadoopFileStats").then(function(data){
				logger.logSuccess("CleanUp of your hdfs file system has been completed.Please refresh.");
			});
		};
		
		$scope.cleanUpLogger =function(){
			$http({
				method: "Post",
				url:"/cleanupLogger"
			}).success(function(data){
				logger.logSuccess("CleanUp of your logger has been completed.Please refresh.");
			}).error(function(data){
				logger.logError(data);
			});
		};
		
		DashboardService.getAllDashboard("/getCountOfContents").then(function(data){
			$scope.totalCounts =  data;
		});
		
		$scope.getMyImagePath = function(type){
			if(type === "Data groups") {
				return "../../styles/images/landingImg/data_groups.png";
			} else if(type === "Data sources") {
				return "../../styles/images/landingImg/data_sources.png";
			} else if(type === "Connections") {
				return "../../styles/images/landingImg/connections.png";
			} else if(type === "Notebook") {
				return "../../styles/images/landingImg/notebook_icon.png";
			} else if(type === "Users") {
				return "../../styles/images/landingImg/users.png";
			} else if(type === "Data flows") {
				return "../../styles/images/landingImg/extend.png";
			} else if(type === "Size(GB)") {
				return "../../styles/images/landingImg/size.png";
			} else if(type === "Solutions") {
				return "../../styles/images/landingImg/solutions.png";
			}
		}
		
		DashboardService.getAllDashboard("/getDashBoardDetails").then(function(data){
			$scope.machine = data;
			
			$scope.easypiechart4['percent'] = Math.floor(($scope.machine.hadoopTotalSpace - $scope.machine.hadoopUsedSpace)/$scope.machine.hadoopTotalSpace*100);
			
			$scope.hadoopMachines = $scope.machine.hadoopMachines;
			
			
			
			if(!_.isUndefined($scope.hadoopMachines) && !_.isNull($scope.hadoopMachines)){
				var gauge1 = new Gauge(target1).setOptions(opts); 
				gauge1.animationSpeed = 32;
				gauge1.maxValue = 100;
				gauge1.set(parseInt($scope.hadoopMachines[0].cpuUsage));
				
				var gauge2 = new Gauge(target2).setOptions(opts); 
				gauge2.animationSpeed = 32;
				gauge2.maxValue = 100;
				var minValue =  Math.floor(($scope.hadoopMachines[0].ramUsed * 100) / $scope.hadoopMachines[0].totalRam);
				gauge2.set(minValue);
				$scope.cpu = $scope.hadoopMachines[0].cpuUsage;
				$scope.ram = minValue;
				
				$scope.percentage= Math.floor($scope.hadoopMachines[0].diskUsed/$scope.hadoopMachines[0].totakDisk*100);
				$scope.easypiechart3['percent'] = $scope.percentage;
			}
			
			
			$scope.nodeStat = function(node){
				$scope.selectedNodeName = node.machineName;
				var obj = _.filter($scope.hadoopMachines , function(object , key){
					if(object.machineName === node.machineName){
						return object;  
					}
				});
				gauge1.maxValue = 100;
				gauge1.set(Math.floor(obj[0].cpuUsage));
				
				var minValue = Math.floor((obj[0].ramUsed/obj[0].totalRam)*100);
				gauge2.maxValue = 100;
				gauge2.set(minValue);
				
				$scope.cpu = obj[0].cpuUsage;
				$scope.ram = minValue;
				
				$scope.diskused=obj[0].diskUsed;
				$scope.disktotal=obj[0].totakDisk;
				$scope.percentage= Math.floor($scope.diskused/$scope.disktotal*100);
				$scope.easypiechart3['percent'] = $scope.percentage;
				
			};
		});
	}]);
}).call(this);

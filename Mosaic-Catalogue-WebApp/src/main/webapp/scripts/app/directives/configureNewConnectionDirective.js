(function(){
	var connections = angular.module("app.connections");

	connections.directive('createConnection', function() {
			return {
		        restrict: 'E',
		        templateUrl: '/views/template/createConnection.html'
		    }
		});
}).call(this);
(function(){
	"use strict";
	
	var discover = angular.module("app.discover",['ui.bootstrap','app.datasource','app.appView']);
	
	discover.filter("typeChecker",function(){
		return function(type){	
			if (type === "")
				return bytes + " Bytes";
			else if (bytes < 1048576)
				return Math.round(bytes / 1024) + " KB";
			else if (bytes < 1073741824)
				return Math.round(bytes / 1048576) + " MB";
			else
				return Math.round(bytes / 1073741824) + " GB"; 
		};
	});
	
	discover.filter("GroupNameFromGroupId",function(){
		return function(content, groupMap){		
			return groupMap[content];
		};
	});
	
	discover.filter("contentType",function(){
		return function(content){		
			if (content === "")
				return "";
			else if (content === "FILE_DATA_SOURCE")
				return "Delimited File";
			else if (content === "RDBMS_DATA_SOURCE")
				return "Rdbms";
			else  if (content === "TSV_DATA_SOURCE")
				return "Tsv";
			else  if (content === "CSV_DATA_SOURCE")
				return "Csv";
			else  if (content === "EXCEL_DATA_SOURCE")
				return "Excel Sheet";
			else  if (content === "EXTERNAL_DATA")
				return "External Data";
			else  if (content === "FTP_DATA")
				return "Ftp Data";
			else  if (content === "REMOTE_DATA")
				return "Remote Data";
			else  if (content === "STREAM_DS")
                return "Stream Ds";
			else  if (content === "FIXED_LENGTH_FORMAT")
                return "Fixed";
			else  if (content === "MYSQL")
                return "MYSQL";
			else  if (content === "BLOB_DATA_SOURCE")
                return "BLOB";
			else  if (content === "NOSQL")
                return "NOSQL";
			else  if (content === "ORACLE")
                return "ORACLE";
			else  if (content === "POSTGRES")
                return "POSTGRES";
			else  if (content === "SFTP")
                return "SFTP";
			else  if (content === "HDFS")
                return "HDFS";
			else  if (content === "COGNOS_DATA_SOURCE")
                return "COGNOS";
			else  if (content === "AMAZONS3")
                return "AMAZONS3";
			if (content === "")
				return "";
			
			else
				return "";
		};
	});
	
	discover.filter('unsafe', function($sce) {
		  return function(val) {
	      return $sce.trustAsHtml(val);
	   };
	});
	
	discover.service('anchorSmoothScroll', function(){
	    this.scrollTo = function(eID) {

	        // This scrolling function 
	        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
	        
	        var startY = currentYPosition();
	        var stopY = elmYPosition(eID);
	        
	        var distance = stopY > startY ? stopY - startY : startY - stopY;
	        if (distance < 100) {
	            scrollTo(0, stopY); return;
	        }
	        var speed = Math.round(distance / 100);
	        if (speed >= 20) speed = 20;
	        var step = Math.round(distance / 25);
	        var leapY = stopY > startY ? startY + step : startY - step;
	        var timer = 0;
	        if (stopY > startY) {
	            for ( var i=startY; i<stopY; i+=step ) {
	                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
	                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
	            } return;
	        }
	        for ( var i=startY; i>stopY; i-=step ) {
	            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
	            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
	        }
	        
	        function currentYPosition() {
	            // Firefox, Chrome, Opera, Safari
	            if (self.pageYOffset) return self.pageYOffset;
	            // Internet Explorer 6 - standards mode
	            if (document.documentElement && document.documentElement.scrollTop)
	                return document.documentElement.scrollTop;
	            // Internet Explorer 6, 7 and 8
	            if (document.body.scrollTop) return document.body.scrollTop;
	            return 0;
	        }
	        
	        function elmYPosition(eID) {
	            var elm = document.getElementById(eID);
	            var y = elm.offsetTop;
	            var node = elm;
	            while (node.offsetParent && node.offsetParent != document.body) {
	                node = node.offsetParent;
	                y += node.offsetTop;
	            } return y;
	        }

	    };
	    
	});

	discover.controller("discoverController" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll", "$timeout", "$interval",  function($anchorScroll ,$filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger,anchorSmoothScroll, $timeout, $interval){

			$scope.urlToCall = "";
		
			$scope.numPerPageOpt = [5, 9, 20,50];
			$scope.numPerPage = $scope.numPerPageOpt[1];
			$scope.dataSourceCol;
			$scope.filteredStores = [];
			$scope.currentPage = 1;
			$scope.groupMap = {};
			$scope.accesses = [];
			//init variable
			$scope.types = [];
			$scope.createdBy = [];
			$scope.category = [];
			$scope.sub_category = [];
			$scope.created_date = [];
			$scope.tags = [];
			$scope.sourceType = [];
			$scope.avgRating = [];
			$scope.dataAtRest = [];
			$scope.dataRepo = [];
			$scope.loadStrategy = [];
			$scope.projectsame = [];
			$scope.usergroup = [];
			
			$scope.customValCheckValue = {};
			$scope.typeCheck = [];
			$scope.createdByCheck = [];
			$scope.categoryCheck  = [];
			$scope.subCategoryCheck = [];
			$scope.createdDateCheck = [];
			$scope.tagsCheck = [];
			$scope.sourceTypeCheck = [];
			$scope.avgRatingCheck = [];
			$scope.dataAtRestCheck = [];
			$scope.dataRepoCheck = [];
			$scope.loadStrategyCheck = [];
			$scope.projectsNameCheck = [];
			$scope.dataUserGroupCheck = [];
			$scope.serarchType = ['Data Source'];
			$scope.checked = true;
			$scope.toggleRequestAccess = false;
			$scope.toggleAddtoProject =	false;
			
			$scope.checkAllSourceType=false;
			$scope.checkAllCategory = false;
		    $scope.checkAllAvgRatingCheck = false;
		    $scope.checkAllProjectsName = false;
		    $scope.checkAllDataUserGrp = false;
		    $scope.checkAllCreatedBy = false;
		    $scope.checkAllTags = false;
		    $scope.checkAllLoadStrategy = false;
		    $scope.checkAllDataAtRest = false;
		    $scope.checkAllSubCategory = false;
		    $scope.checkAllSourceType = false;
		    
		    // variable for facets show / hide
		    $scope.facetsObject = {
		    	'sourceTypeShow': true,
		    	'categoryShow': false,
		    	'subCategoryShow': false,
		    	'dataAtRestShow': false,
		    	'loadStrategyShow': false,
		    	'tagsShow': false,
		    	'createdByShow': false,
		    	'dataUserGroupShow': false,
		    	'projectNameShow': false,
		    	'avgRatingShow': false
		    };
		    
		    $scope.showHideFacets = function(type){
		    	if($scope.facetsObject[type])		    		
		    		$scope.facetsObject[type] = false;
		    	else
		    		$scope.facetsObject[type] = true;
		    }
			
			$scope.checkAllSourceTypeCheck = function () {
		        if ($scope.checkAllSourceType) {
		            $scope.checkAllSourceType = false;
		        } else {
		            $scope.checkAllSourceType = true;
		        }
		        
		        for(var i=0;i<$scope.sourceType.length;i++){
		        	$scope.sourceTypeCheck[$scope.sourceType[i].key] = $scope.checkAllSourceType; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllCategoryFnc = function () {
		        if ($scope.checkAllCategory) {
		        	$scope.checkAllCategory = false;
		        } else {
		            $scope.checkAllCategory = true;
		        }
		        for(var i=0;i<$scope.category.length;i++){
		        	$scope.categoryCheck[$scope.category[i].key] = $scope.checkAllCategory; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllSubCategoryFnc = function () {
		        if ($scope.checkAllSubCategory) {
		            $scope.checkAllSubCategory = false;
		        } else {
		            $scope.checkAllSubCategory = true;
		        }
		        for(var i=0;i<$scope.sub_category.length;i++){
		        	$scope.subCategoryCheck[$scope.sub_category[i].key] = $scope.checkAllSubCategory; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllDataAtRestFnc = function () {
		        if ($scope.checkAllDataAtRest) {
		            $scope.checkAllDataAtRest = false;
		        } else {
		            $scope.checkAllDataAtRest = true;
		        }
		        for(var i=0;i<$scope.dataAtRest.length;i++){
		        	$scope.dataAtRestCheck[$scope.dataAtRest[i].key] = $scope.checkAllDataAtRest; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllLoadStrategyFnc = function () {
		        if ($scope.checkAllLoadStrategy) {
		            $scope.checkAllLoadStrategy = false;
		        } else {
		            $scope.checkAllLoadStrategy = true;
		        }
		        for(var i=0;i<$scope.loadStrategy.length;i++){
		        	$scope.loadStrategyCheck[$scope.loadStrategy[i].key] = $scope.checkAllLoadStrategy; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllTagsFnc = function () {
		        if ($scope.checkAllTags) {
		            $scope.checkAllTags = false;
		        } else {
		            $scope.checkAllTags = true;
		        }
		        for(var i=0;i<$scope.tags.length;i++){
		        	$scope.tagsCheck[$scope.tags[i].key] = $scope.checkAllTags; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllCreatedByFnc = function () {
		        if ($scope.checkAllCreatedBy) {
		            $scope.checkAllCreatedBy = false;
		        } else {
		            $scope.checkAllCreatedBy = true;
		        }
		        for(var i=0;i<$scope.createdBy.length;i++){
		        	$scope.createdByCheck[$scope.createdBy[i].key] = $scope.checkAllCreatedBy; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllDataUserGroup = function () {
		        if ($scope.checkAllDataUserGrp) {
		            $scope.checkAllDataUserGrp = false;
		        } else {
		            $scope.checkAllDataUserGrp = true;
		        }
		        for(var i=0;i<$scope.usergroup.length;i++){
		        	$scope.dataUserGroupCheck[$scope.usergroup[i].key] = $scope.checkAllDataUserGrp; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllProjectsNamesFnc = function () {
		        if ($scope.checkAllProjectsName) {
		            $scope.checkAllProjectsName = false;
		        } else {
		            $scope.checkAllProjectsName = true;
		        }
		        for(var i=0;i<$scope.projectsame.length;i++){
		        	$scope.projectsNameCheck[$scope.projectsame[i].key] = $scope.checkAllProjectsName; 
		        }
		        $scope.select(1);
		    };
		    
		    $scope.checkAllAvgRatingCheckFnc = function () {
		        if ($scope.checkAllAvgRatingCheck) {
		            $scope.checkAllAvgRatingCheck = false;
		        } else {
		            $scope.checkAllAvgRatingCheck = true;
		        }
		        for(var i=0;i<$scope.avgRating.length;i++){
		        	$scope.avgRatingCheck[$scope.avgRating[i].key] = $scope.checkAllAvgRatingCheck; 
		        }
		        $scope.select(1);
		    };
		    
			
			$scope.initialization = function(requestDsOrCollection){
				
				if(requestDsOrCollection!="" && requestDsOrCollection=="discover"){
					$scope.urlToCall = "/getSearchResult";
				}else if(requestDsOrCollection!="" && requestDsOrCollection=="collection"){
					$scope.urlToCall = "/getSearchCollectionResult";
				}
				
				//Loading created by list
				$http.post('/getCreatedByList').then(function(data) {
					$scope.createdByList=data.data;
					$scope.runOnce = false;
				});	
				
				$http.post("/getAllActiveTags").success(function(data){
					var tempTagNames = [];
					_.each(data, function(tag){
						tempTagNames.push(tag.name);
					});
					$scope.tagsNm=tempTagNames;
					$scope.runOnce = false;
				}).error(function(data){
					logger.logError(data);
				});
			
				
				$scope.searchData = {createdBy:[],fetchRecordsFrm:0,generalSearchString:"",searchInTypes:["DATASOURCE"], sizeOfRecords:9};
				$http({
					method : 'POST',
					url : $scope.urlToCall,
					data : $scope.searchData
				}).then(function(data){
					var data1 = data.data.globalSearchResult;
					$scope.searchResults = data1;
					if(null !== $scope.searchResults && undefined !== $scope.searchResults){
						_.each($scope.searchResults.dataSourceAndFlowType, function(val){
							$scope.types = val.type;
							$scope.createdBy = val.createdBy;
							$scope.category = _.reject(val.category, function(val){
								return val == null && val == "Null";
							});
							$scope.sub_category = _.reject(val.sub_category, function(val){
								return val == null && val == "Null" && val == "null";
							});
							$scope.created_date = val.created_date;
							$scope.tags = [];
							$scope.sourceType = val.source_type;
							$scope.avgRating = val.avg_rating;
							$scope.dataAtRest = val.data_at_rest;
							$scope.dataRepo = val.data_repo;
							$scope.loadStrategy = _.reject(val.load_strategy, function(val){
								return val == null && val == "Null" && val == "null";
							});
							$scope.projectsame = val.projects_name;
							$scope.usergroup = val.user_group;
						});
						$scope.dataSourceCol = data.data.records;
						$scope.accesses = data.data.requestIndicator;
						$scope.filteredStores.length = data.data.totalCount;
						$scope.totalRecords = data.data.totalCount;
						if($scope.pageNumBkp!=null && $scope.pageNumBkp!=undefined){
							$scope.pageNum = angular.copy($scope.pageNumBkp);
						}
						
					}
					$scope.showSelectedData($scope.dataSourceCol[0], 0);
				});
			}
			
			
			$http({
				method : 'POST',
				url : "/groupConfig/getGroupsData"
			}).success(function(data, status, headers, config) {
				$scope.groupDetails = data.GROUPS;
				
				_.each($scope.groupDetails, function(group){
					$scope.groupMap[group.groupId] = group.groupName;
				})
				
			}).error(function(data, status, headers, config) {
				return false;
			});
			
			$scope.isEnableRequest = false;
			$scope.disabledExpore = false;
			$interval(function(){
				if(null != $rootScope.searchDataResult && undefined != $rootScope.searchDataResult){
					$scope.searchData = angular.copy($rootScope.searchDataResult);
					$scope.isEnableRequest = true;
					$scope.getDataSourceList(true);
				};
				if($rootScope.isDataSourceInfoSaved!=null && $rootScope.isDataSourceInfoSaved!=undefined && $rootScope.isDataSourceInfoSaved){
					$rootScope.isDataSourceInfoSaved = undefined;
					$scope.getDataSourceList(true);
				}
			},100);

				$scope.onOrderChange = function() {
					return $scope.select(1), $scope.currentPage = 1;
				};
			
				$scope.select = function(page) {
					$scope.pageNum = page;
					$scope.searchData.fetchRecordsFrm = ((page-1) * $scope.numPerPage);
					$scope.searchData.sizeOfRecords = 9;
					$scope.pageNumBkp = angular.copy($scope.pageNum);
					$scope.searchDataBck = angular.copy($scope.searchData); 
					
					if($scope.customValCheckValue){
						$scope.searchData.searchInTypes = [];
						$scope.searchData.createdBy = [];
						$scope.searchData.sourceType = [];
						$scope.searchData.avgRating = [];
						$scope.searchData.dataAtRest = [];
						$scope.searchData.dataRepo = [];
						$scope.searchData.loadStrategy = [];
						$scope.searchData.projectsName = [];
						$scope.searchData.userGroup = [];
						$scope.searchData.category = [];
						$scope.searchData.subCategory = [];
						var keysForTypeCheck =  _.keys($scope.typeCheck)
						$scope.searchData.searchInTypes.push('DATASOURCE');
						/*_.each(keysForTypeCheck, function (val){
							if($scope.typeCheck[val] == true) {
								$scope.searchData.searchInTypes.push('DATASOURCE');
							}
						});*/
						
						var keysForCreatedByCheck =  _.keys($scope.createdByCheck)
						_.each(keysForCreatedByCheck, function (val){
							if($scope.createdByCheck[val] == true) {
								$scope.searchData.createdBy.push(val);
							}
						});
						
						var keysForCategoryCheck =  _.keys($scope.categoryCheck)
						_.each(keysForCategoryCheck, function (val){
							if($scope.categoryCheck[val] == true) {
								$scope.searchData.category.push(val);
							}
						});
						
						var keysForSubCategoryCheck =  _.keys($scope.subCategoryCheck)
						_.each(keysForSubCategoryCheck, function (val){
							if($scope.subCategoryCheck[val] == true) {
								$scope.searchData.subCategory.push(val);
							}
						});
						
						var keysForCreatedDateCheck =  _.keys($scope.createdDateCheck)
						_.each(keysForCreatedDateCheck, function (val){
							if($scope.createdDateCheck[val] == true) {
								$scope.searchData.createdDate.push(val);
							}
						});
						
						var keysForSourceTypeCheck =  _.keys($scope.sourceTypeCheck)
						_.each(keysForSourceTypeCheck, function (val){
							if($scope.sourceTypeCheck[val] == true) {
								$scope.searchData.sourceType.push(val);
							}
						});
						
						var keysForAvgRatingCheck =  _.keys($scope.avgRatingCheck)
						_.each(keysForAvgRatingCheck, function (val){
							if($scope.avgRatingCheck[val] == true) {
								$scope.searchData.avgRating.push(val);
							}
						});
						
						var keysFordataAtRestCheck =  _.keys($scope.dataAtRestCheck)
						_.each(keysFordataAtRestCheck, function (val){
							if($scope.dataAtRestCheck[val] == true) {
								$scope.searchData.dataAtRest.push(val);
							}
						});
						
						var keysFordataRepoCheck =  _.keys($scope.dataRepoCheck)
						_.each(keysFordataRepoCheck, function (val){
							if($scope.dataRepoCheck[val] == true) {
								$scope.searchData.dataRepo.push(val);
							}
						});
						var keysForloadStrategyCheck =  _.keys($scope.loadStrategyCheck)
						_.each(keysForloadStrategyCheck, function (val){
							if($scope.loadStrategyCheck[val] == true) {
								$scope.searchData.loadStrategy.push(val);
							}
						});
						var keysForprojectsNameCheck =  _.keys($scope.projectsNameCheck)
						_.each(keysForprojectsNameCheck, function (val){
							if($scope.projectsNameCheck[val] == true) {
								$scope.searchData.projectsName.push(val);
							}
						});
						var keysFordataUserGroupCheck =  _.keys($scope.dataUserGroupCheck)
						_.each(keysFordataUserGroupCheck, function (val){
							if($scope.dataUserGroupCheck[val] == true) {
								$scope.searchData.userGroup.push(val);
							}
						});
						
						$http({
							method : 'POST',
							url : $scope.urlToCall,
							data : $scope.searchData
						}).success(function(data, status, headers, config) {
							$scope.dataSourceCol = data.records;
							$scope.filteredStores.length = data.totalCount;
							if(undefined != $scope.dataSourceCol && null != $scope.dataSourceCol && $scope.dataSourceCol.length > 0){
								$scope.showSelectedData($scope.dataSourceCol[0], 0)
								$scope.disabledExpore = false;
							}else{
								$scope.showSelectedData($scope.dataSourceCol[-1], -1)
								$scope.disabledExpore = true;
							}
						}).error(function(data, status, headers, config) {
							return false;
						});

						var end, start;
						return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.dataSourceCol = $scope.filteredStores.slice(start, end);
					}
					
				};
				
				$scope.order = function(rowName) {
					return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
				};

				$scope.onNumPerPageChange = function(val) {
					$scope.numPerPage = val;
					return $scope.select(1), $scope.currentPage = 1;
				};
				
				$scope.selectedIndex = 0;
				$scope.requestStatus = 'NA';
				$scope.showSelectedData = function(data, $index){
					$scope.selectedIndex = $index;
					$scope.showDs = data;
					
					$scope.loginUserId = localStorage.getItem("userName");
					
					if(undefined == $scope.showDs){
						$scope.infoData = [];
						$scope.dsInstance = null;
						$scope.projectDetails = null;
					}else{
						$routeParams.param1 = data.dataSourceName;
						$http({
							url: "/getDataSource",
							method: "POST",
							data : $scope.showDs.dataSourceName
						}).success(function(data){
							$scope.infoData = data;
							if($scope.infoData.ingection == "typical"){
								$scope.infoData.ingection = "Advanced";
							}else{
								$scope.infoData.ingection = "Basic";
							}
							$scope.getRequestStatusOfDataSource();
						})	
						$http({
							url: "/getDataSourceInstance",
							method: "POST",
							data : $scope.showDs.dataSourceName
						}).success(function(data){
							$scope.dsInstance = data;
						});
						$http({
							method: "POST",
							url:"/fetchDataSourceFeedback",
							data : $scope.showDs.dataSourceId
						}).success(function(data){
							$scope.feedbackObj = data;
						}).error(function(data){
						});
						
						$scope.toggleRequestAccess = false;
						$scope.toggleAddtoProject =	false;
					}
				};
				$scope.getFavlist = function (){
					$http({
						method: "POST",
						url:"/getAllFavouriteDataSouceOfUser"
						
					}).success(function(response){
						$scope.myFavouritelist = response;
					}).error(function(response){
					});
				};
				$scope.myFavfilter = function (item){
					console.log("item",item);
					/*_.each($scope.myFavouritelist,function(value){
						console.log(value.dataSourceId,"value");
						if(value.dataSourceId= item){
							$scope.projectDetail.push({"isFav":true});
						}
					});*/
				}
				$scope.enableDisableFavDS = function(update,$event,dataSourceId){
					 $event.stopPropagation();
					 $http({
							method: "POST",
							url:"/markDataSourceAsFavourite",
							data:{"dataSourceId":""+dataSourceId+"","markedAsFavourite":update}
							
						}).success(function(response){debugger;
							logger.logSuccess(response.success);
						}).error(function(response){
							
						});
				}
				//$scope.getFavlist();
				$scope.getRequestStatusOfDataSource = function() {
					var reqDataObject = {objectId : $scope.infoData.id, objectType : "DATA_SOURCE"};
					$http({
						method : "post",
						url : "/getCollectionAndRequestStatusOfDatasource",
						data : reqDataObject
					}).success(function(data, status){
 						if(status == 200)
 							$scope.requestStatus = data;
 					}).error(function(data){
 					});
 				};
				
				$scope.infoDiv = true;
				$scope.requestDiv = false;
				$scope.feedbackDiv = false;
				$scope.rightPannel = true;
				$scope.openTab = function(tabVal){
					debugger;
					if(tabVal == 'request'){
						$scope.toggleRequestAccess = false;
						$scope.toggleAddtoProject =	false;
						$scope.infoDiv = false;
						$scope.requestDiv = true;
						$scope.feedbackDiv = false;
					}else if(tabVal == 'feedback'){
						$scope.infoDiv = false;
						$scope.requestDiv = false;
						$scope.exploreDiv = false;
						$scope.feedbackDiv = true;
					}else if(tabVal == 'rightPannel'){
						
						$scope.myFavPannel = false;
						$scope.rightPannel = true;
					}else if(tabVal == 'MyFav'){
						
						$scope.myFavPannel = true;
						$scope.rightPannel = false;
					}else{
						$scope.infoDiv = true;
						$scope.requestDiv = false;
						$scope.exploreDiv = false;
						$scope.feedbackDiv = false;
					}
				};
				
			$scope.showInformation = function (dataSourceObj) {
				 $routeParams.param1 = dataSourceObj.dataSourceName;
				 $routeParams.param3 = "popUp";
				 $routeParams.param6 = 1;
				 $routeParams.param7 = dataSourceObj.dataSourceId;
				 $routeParams.param4 = $modal.open({
					backdrop : 'static',
					keyboard : false, 
					size : "xl",
		            templateUrl: "/views/DataSource/dataSourceInfo.jsp",
		            resolve : {
		            	$routeParams : function(){
			           	 	return $routeParams;
			           	},
			           	myParam : function(){
			           	 	return $routeParams;
			           	}
			          }
				}),$routeParams.param4.result.then(function(reponame) {
					
	            }, function() {
	             $log.info("Modal dismissed at: " + new Date);
	           });
			};
			
			$scope.goToDiscoverDataSource = function () {
				$location.path("/discover/" + $routeParams.param1);
			};
			$scope.requestObj = {};
			$scope.requestObj.requestFor = "";
			$scope.requestObj.justification = "";
			$scope.requestObj.selectedRequestGroup = "";
			$scope.requestForAccess = function() {
				$scope.toggleAddtoProject =	false;
				if(!$scope.toggleRequestAccess)
				{
					$scope.requestObj.requestFor = "";
					$scope.requestObj.justification = "";
					var reqDataObject = {'objectId' : $scope.infoData.id, 'objectType' : "DATA_SOURCE" };
					$http({
						method : "POST",
						url : "/getListOfGroupsToForWhichObjectNeedsToBeShared",
						data : reqDataObject
					}).success(function(data, status){
						if(status == 200)
							$scope.requestGroups = data;
					}).error(function(data){
					});
					
					$scope.toggleRequestAccess = true;
				}
			};
			
			$scope.addToProject = function() {
				$scope.toggleRequestAccess = false;
				if(!$scope.toggleAddtoProject)
				{
					$scope.selectedRequestProject = "";
					$http({
						method : "POST",
						url : "/getListOfProjectInWhichObjectNeedsToBeUse",
						data : $scope.infoData.id
					}).success(function(data, status){
						if(status == 200){
							if(data && data.length > 0){
								$scope.requestProjects = data;
								$scope.toggleAddtoProject = true;
							}else{
								logger.logError("No projects available to add datasource");
							}
						}else{ 
							logger.logError(data);
						}
					}).error(function(data){
						logger.logError(data);	
					});
					
					
				}
			};
			
			$scope.selectedRequestProject = "";
			$scope.onProjectChange =  function(selectedRequestProject) {
				$scope.selectedRequestProject = selectedRequestProject;
			};
			
			
			$scope.onGroupChange = function(selectedRequestGroup) {
				$scope.requestObj.selectedRequestGroup = selectedRequestGroup;
			};
			
			
			$scope.addToProjectConfirmation = function() {
				if(!$scope.selectedRequestProject) {
					logger.logError("Please select project");
					return false;
				}
				
				$http({
					method : "POST",
					url : "/addObjectToProject",
					data :{'projectId': parseInt($scope.selectedRequestProject), 'objectId': $scope.infoData.id,'objectType':"DATA_SOURCE" }
				}).success(function(data){
					logger.logSuccess("Added successfully");
					//$scope.getRequestStatusOfDataSource();
					$scope.toggleAddtoProject = false;
				}).error(function(status){
					logger.logError(status);
				});
			};
			
			$scope.requestForAccessConfirmation = function(requestFor,justification){
				$scope.requestObj.requestFor = requestFor;
				$scope.requestObj.justification = justification;
				if(!$scope.requestObj.requestFor) {
					logger.logError("Please select request for");
					return false;
				}
				if($scope.requestObj.requestFor == 'Group' && !$scope.requestObj.selectedRequestGroup) {
					logger.logError("Please select group");
					return false;
				}
				if(!$scope.requestObj.justification) {
					logger.logError("Please enter message");
					return false;
				}
				if($scope.requestObj.requestFor == 'User' && $scope.requestStatus.toLowerCase() == 'pending') {
					logger.logError("Your request is pending for this user");
					return false;
				}
				
				var requestData={
						"objectId":$scope.infoData.id,
						"objectType":"DATA_SOURCE",
						"requestedAccess":"USE",
						"justification":$scope.requestObj.justification,
						"requestedUserId" : parseInt($scope.infoData.createdBy),
						"groupId": ($scope.requestObj.requestFor != 'User') ? parseInt($scope.requestObj.selectedRequestGroup) : null
				};
				
				var reqDataObject = { dataRequestDTO : requestData, isRequestedForGroup : (($scope.requestObj.requestFor == 'User') ? false : true) }
				
				$http({
					method : "POST",
					url : "/createRequestForObject",
					data : reqDataObject
				}).success(function(data){
					logger.logSuccess("Request sent for permission");
					$scope.getRequestStatusOfDataSource();
					$scope.toggleRequestAccess = false;
				}).error(function(status){
					logger.logError(status);
				});
			};
			
			$scope.addToCollection = function() {
				var reqDataObject = { objectId : $scope.infoData.id , objectType : "DATA_SOURCE"};
				$http({
					method : "post",
					url : "/addToMyCollection",
					data : reqDataObject
				}).success(function(data){
					logger.logSuccess("Added successfully");
					$scope.getRequestStatusOfDataSource();
				}).error(function(status){
					logger.logError(status);
				});
			};
			
			$scope.advanceSearchOpen = function(){
				$scope.searchData = {"searchInTypes" : ["DATASOURCE"]};
				$scope.searchData.fetchRecordsFrm = 0;
				$scope.searchData.sizeOfRecords = 9;
				$rootScope.searchData = $scope.searchData;
			}
			
			$scope.clearSearch = function (){
				$scope.searchData = {"searchInTypes" : ["DATASOURCE"]};
				$scope.searchData.fetchRecordsFrm = 0;
				$scope.searchData.sizeOfRecords = 9;
				$scope.isEnableRequest = false;
				$scope.getDataSourceList();
			};
			
			var timeToElaspse;
			$scope.onKeyPress = function(){
				if(timeToElaspse){$timeout.cancel(timeToElaspse);}
				timeToElaspse =  $timeout(function () {
					if($scope.searchKeywords != undefined && null != $scope.searchKeywords){
						if($scope.searchData === "") {
							$scope.searchData.generalSearchString = "*";
						} else {
							$scope.searchData.generalSearchString =  "*" + $scope.searchKeywords + "*";
						}
						
						
						$scope.select(1);
					}				 
				}, 500);
			};
			
	}]).controller("dataSourceRequestAccessController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","dataSourceName",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger,dataSourceName){
		$scope.dsName = dataSourceName;
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
		};
		
		$scope.approveDataSourceRequest = function(justification){
			if(null != $scope.justification && undefined != $scope.justification){
				$modalInstance.close(justification);
			}else{
				$scope.justification ="";				
			}
		};
		

	}]);
}).call(this);

function clearAllDSLinker($scope){
	$scope.fieldvalues = [0];
	$scope.fieldMapping1 = [];
	$scope.fieldMapping2 = [];
	$scope.datasourcelink1 = "";
	$scope.datasourcelink2 = "";
}

function containsinDs(id, dslist){
	notinlist = true;
	angular.forEach(dslist, function(key,value){
		if(key.id == id){
			notinlist = false;
		}
	});
	return notinlist;
}
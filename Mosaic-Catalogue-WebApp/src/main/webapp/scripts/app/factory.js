(function(){
	
	'use strict';
	
	angular.module("app.factory",[])
	.factory("D3factory",["$document", "$q" , "$rootScope",function($document, $q, $rootScope){
		var d = $q.defer();
		 function onScriptLoad() {
		    $rootScope.$apply(function() { d.resolve(window.d3); });
		  }
		 var scriptTag = $document[0].createElement('script');
	     scriptTag.type = 'text/javascript';
	     scriptTag.async = false;
         scriptTag.charset = 'UTF-8';
	     scriptTag.src = 'scripts/libs/d3.js';
	     scriptTag.onreadystatechange = function () {
	       if (this.readyState == 'complete') onScriptLoad();
	     }
	     scriptTag.onload = onScriptLoad;

	     var s = $document[0].getElementsByTagName('body')[0];
	     s.appendChild(scriptTag);

	     return {
	       d3: function() { return d.promise; }
	     };
	}]);
    
    	
}).call(this);


(function(){
	"use strict";
	
	var discover = angular.module("app.glossary",['ui.bootstrap']);

		discover.controller("glossaryTableCtrl" , ["$anchorScroll","$filter","$route","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "anchorSmoothScroll", "$timeout", "$interval",  
			function($anchorScroll, $filter, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $timeout, $interval){
			
			$http({
				method : "POST",
				url : "/getAllActiveTags"
			}).success(function(data){
				if(data){
					return $scope.stores = data,	
			        $scope.searchKeywords = "", 
			        $scope.filteredStores = [],
			        $scope.row = "", 
			        $scope.select = function(page) {
						$scope.pageNum = page;
						var end, start;
			            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.connections = $scope.filteredStores.slice(start, end);
			        }, 
			        $scope.onFilterChange = function() {
			            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
			        }, $scope.onNumPerPageChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.onOrderChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.search = function() {
			            return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
			        }, $scope.order = function(rowName) {
			            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
			        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
			        $scope.numPerPage = $scope.numPerPageOpt[4], 
			        $scope.currentPage = 1, 
			        $scope.connections = [],
			        ($scope.init = function() {
			            return $scope.search(), $scope.select($scope.currentPage);
			        })();
				}
			}).error(function(data){
				logger.logError(data);
			});
			
			$scope.createTag = function(connectionConfig){
				var modalInstance;
				modalInstance = $modal.open({
					size: "md",
		            templateUrl: "/views/template/addNewTagModal.html",
		            controller: "createTag",
		            resolve : {
		            	listOfConnections : function(){
		            		return $scope.stores;
		            	},
		            	config : function(){
		            		return angular.copy(connectionConfig);
		            	},
		            	disable : function(){
		            		return false;
		            	}
		            }
		            	
		         }),modalInstance.result.then(function() {
			    }, function() {
		             $log.info("Modal dismissed at: " + new Date);
		         });
			};
			$scope.viewTag = function(connectionConfig){
				var modalInstance;
				modalInstance = $modal.open({
					size: "md",
		            templateUrl: "/views/template/addNewTagModal.html",
		            controller: "createTag",
		            resolve : {
		            	listOfConnections : function(){
		            		return $scope.stores;
		            	},
		            	config : function(){
		            		return angular.copy(connectionConfig);
		            	},
		            	disable : function(){
		            		return true;
		            	}
		            }
		            	
		         }),modalInstance.result.then(function() {
			    }, function() {
		             $log.info("Modal dismissed at: " + new Date);
		         });
			};
			$scope.deleteTag = function(configData){
				$scope.bkpOfObj = angular.copy(configData);
				delete $scope.bkpOfObj.createdByName;
				delete $scope.bkpOfObj.modifiedByName;
				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/TagDeleteModal.html",
					controller: "deleteTagCtrl",
					size: "sm",
					resolve : {
		            	tag : function(){
		            		return angular.copy($scope.bkpOfObj);
		            	}
		            }					
				}),modalInstance.result.then(function() {
					$http({
						url:"/deleteTag",
						method: "POST",
						data : $scope.bkpOfObj
					}).success(function(data){
						logger.logSuccess("Tag deleted successfully");
						$route.reload();
					}).error(function(data){
						logger.logError(data);
					});
				}, function() {
					$log.info("Modal dismissed at: " + new Date);
				});
			};
			
			$scope.editTag = function(connectionConfig){
				var modalInstance;
				modalInstance = $modal.open({
					size: "md",
		            templateUrl: "/views/template/addNewTagModal.html",
		            controller: "createTag",
		            resolve : {
		            	listOfConnections : function(){
		            		return $scope.stores;
		            	},
		            	config : function(){
		            		return angular.copy(connectionConfig);
		            	},
		            	disable : function(){
		            		return false;
		            	}
		            }
		            	
		         }),modalInstance.result.then(function() {
			    }, function() {
		             $log.info("Modal dismissed at: " + new Date);
		         });
			};
			
		}]).controller("createTag" , ["disable","config","listOfConnections","$filter","$anchorScroll","$route", "DataSourceService","$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger" , "$modalInstance", function(disable, config , listOfConnections , $filter, $anchorScroll,$route,DataSourceService,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $modalInstance){
			$scope.disabled = disable;
			$scope.disableOnUpdate = false;
			var urlToCall = "/addTag";
			if(config != null){
				$scope.tag = config;
				if(!$scope.disabled){
					$scope.disableOnUpdate = true;
					urlToCall = "/updateTag";
				}
			} 
			
			
			$scope.createTag = function(){
				if($scope.tag == undefined || $scope.tag == undefined == null){
					logger.logError("Please provide the required values");
					return false;
				}else if($scope.tag.name == null || $scope.tag.name == undefined || $scope.tag.name == ""){
					logger.logError("Please provide the Tag Name");
					return false;
				}else if($scope.tag.shortDescription == null || $scope.tag.shortDescription == undefined || $scope.tag.shortDescription == ""){
					logger.logError("Please provide short description");
					return false;
				}else if($scope.tag.longDescription == null || $scope.tag.longDescription  == undefined || $scope.tag.longDescription  == ""){
					logger.logError("Please provide long description");
					return false;
				}
				
				delete $scope.tag.createdByName;
				delete $scope.tag.modifiedByName;
				
				$http({
					method : "post",
					url : urlToCall,
					data : $scope.tag
				}).success(function(data){
					logger.logSuccess("Tag saved successfully");
					$modalInstance.dismiss("cancel");
					$route.reload();
				}).error(function(data){
					if(data && data.indexOf("Duplicate entry") > -1){
						logger.logError("Tag with name "+$scope.tag.name+" is already exists");
						return;
					}
					logger.logError(data);
				});
				
			};
			
			
			$scope.updateTag = function(){
				$scope.createTag();
			}; 
			
			$scope.cancel = function(){
				$modalInstance.dismiss("cancel");
			};
		}]).controller('deleteTagCtrl', ["$scope", "$http", "$modal", "$log" ,"logger" , "$routeParams", "$interval","$modalInstance","tag",  function($scope,$http,$modal,$log,logger, $routeParams, $interval,$modalInstance,tag) {
			$scope.indicatorDelete = true;
			$scope.deleteds = {
					deleteNode : ""
			};
			
			$scope.showTable="hide";
			$scope.showDs=function(){
				if($scope.showTable=="show"){
					$scope.showTable="hide";	
				}else{
					$scope.showTable="show";
				}
			}
			
			$http({
				url:"/getAllListOfDSOfTag",
				method: "POST",
				data : tag
			}).success(function(data){
				$scope.list = data;
			}).error(function(data){
				logger.logError(data);
			});
			
			$scope.deleteSelectedAppDs = function(){
				if($scope.deleteds.deleteNode != undefined && $scope.deleteds.deleteNode.toLowerCase() === "delete"){
					$modalInstance.close();
				}else{
					logger.logError("Please type delete.....");
					$scope.deleteds.deleteNode = "";
					return false;
				}
			};

			$scope.cancel = function() {
				$modalInstance.dismiss("cancel");
			};

		}]);
	}).call(this);


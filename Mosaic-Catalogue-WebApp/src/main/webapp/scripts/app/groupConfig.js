(function(){
	
	"use strict";
	
	var app = angular.module('app.groupConfig',[]);
	
	
	app.service("UserGroupService",["$q","$http","logger",function($q,$http,logger){
		
		return{
			
			getConfigNewGroupDummy:function(){
				var configNewGroupDummy={
						groupId:"",
						groupName:"",
						description:"",
						insertedBy:"",
						insertTs:"",
						updatedBy:"",
						updateTs:""
				}
				return configNewGroupDummy;
			},
			
			getConfigNewSubgroupDummy:function(){
				var configNewGroupDummy={
						subGroupId:"",
						//groupName:"",
						subGroupName:"",
						subGroupDescription:"",
						usergroupId:"",
						insertedBy:"",
						insertTs:"",
						updatedBy:"",
						updateTs:"",
						queueMasterID:""
//						queueName:""
				}
				return configNewGroupDummy;
			},
			
			getGroupIdByGroupName:function(ListObj,groupName){
				var usergroupId="";
					angular.forEach(ListObj,function(obj){
					if(groupName==obj.groupName){
						usergroupId= obj.groupId;
						return usergroupId;
					}
				})			
				return usergroupId;
			},
			
			getQueueMasterIdByQueueName:function(ListObjForQueue,queueName){
				var queueMasterId="";
					angular.forEach(ListObjForQueue,function(obj){
					if(queueName==obj.queueName){
						queueMasterId= obj.queueMasterId;
						return queueMasterId;
					}
				})			
				return queueMasterId;
			},
			
			getWithoutObject:function(url){
				var deferred=$q.defer();
				$http.post(url).success(function(data){
					deferred.resolve(data);
				}).error(function(data){
					logger.logError(data);
					deferred.reject(data);
				});
				return deferred.promise;
			},
			
			findGroupByApp : function(list, appId){
				var outPutList = [];
				angular.forEach(list, function(obj){
					if(obj.appId === appId){
						outPutList.push(obj);
					}
				});
				return outPutList;
			},
			
			getAppId:function(appName, allApps) {
		    	for(var i = parseInt(0); i < allApps.length ; i++) {
		    		if(allApps[i].appName === appName){
		    			return allApps[i];
		    		}
		    	}
		    },
			
			postWithObject:function(url,object){
				var deferred=$q.defer();
				$http.post(url,object).success(function(data){
					deferred.resolve(data);
				}).error(function(data){
					logger.logError(data);
					deferred.reject(data);
				});
				return deferred.promise;
			}
			
		}
		
		
	}]);
	

	app.controller("groupConfigController" , ["$filter","$route","$scope", "$modal" , "$log", "logger","UserGroupService",  function($filter,$route,$scope,$modal,$log,logger,UserGroupService){
		
		//FETCH AVAILABLE GROUPS DATA
		UserGroupService.getWithoutObject('/groupConfig/getGroupsData').then(function(data){
			//console.log(data.SUBGROUPS);
			$scope.availableGroups=data.GROUPS;
		    $scope.stores = $scope.availableGroups	
	        , $scope.searchKeywords = "", 
	        $scope.filteredStores = [],
	        $scope.row = "", 
	        $scope.select = function(page) {
		    	$scope.pageNum = page;
				var end, start;
	            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
	        }, 
	        $scope.onFilterChange = function() {
	            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
	        }, $scope.onNumPerPageChange = function(val) {
	        	$scope.numPerPage = val;
	            return $scope.select(1), $scope.currentPage = 1;
	        }, $scope.onOrderChange = function() {
	            return $scope.select(1), $scope.currentPage = 1;
	        }, $scope.search = function() {
	        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
	        }, $scope.order = function(rowName) {
	        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
	        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
	        $scope.numPerPage = $scope.numPerPageOpt[4], 
	        $scope.currentPage = 1, 
	        $scope.currentPageStores = [], ($scope.init = function() {
                return $scope.search(), $scope.select($scope.currentPage), $scope.order("-groupId");
            })();
	        
	        
	        //SUB-GROUPS
	        $scope.availableSubGroups=data.SUBGROUPS;
			$scope.stores1 = $scope.availableSubGroups	
	        , $scope.searchKeywords1 = "", 
	        $scope.filteredStores1 = [],
	        $scope.row1 = "", 
	        $scope.select1 = function(page) {
				$scope.pageNum = page;
				var end, start;
	            return start = (page - 1) * $scope.numPerPage1, end = start + $scope.numPerPage1, $scope.currentPageStores1 = $scope.filteredStores1.slice(start, end);
	        }, 
	        $scope.onFilterChange1 = function() {
	            return $scope.select1(1), $scope.currentPage1 = 1, $scope.row1 = "";
	        }, $scope.onNumPerPageChange1 = function(val) {
	        	$scope.numPerPage1 = val;
	            return $scope.select1(1), $scope.currentPage1 = 1;
	        }, $scope.onOrderChange1 = function() {
	            return $scope.select1(1), $scope.currentPage1 = 1;
	        }, $scope.search1 = function() {
	        	return $scope.filteredStores1 = $filter("filter")($scope.stores1, $scope.searchKeywords1), $scope.onFilterChange1();
	        }, $scope.order1 = function(rowName) {
	        	return $scope.row1 !== rowName ? ($scope.row1 = rowName, $scope.filteredStores1 = $filter("orderBy")($scope.filteredStores1, rowName), $scope.onOrderChange1()) : void 0;
	        }, $scope.numPerPageOpt1 = [3, 5, 10, 15, 20],
	        $scope.numPerPage1 = $scope.numPerPageOpt1[3], 
	        $scope.currentPage1 = 1, 
	        $scope.currentPageStores1 = [], ($scope.init1 = function() {
                return $scope.search1(), $scope.select1($scope.currentPage1), $scope.order("-subGroupId");
            })();
		});
		
		
		
		//DELETE GROUP
		$scope.deleteGroup=function(groupId, name){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:name, type: "group"};
					}
				}
			}),modalInstance.result.then(function() {
				UserGroupService.postWithObject('/groupConfig/deleteGroup',groupId).then(function(data){
					$route.reload();
					logger.logSuccess("Group has been deleted successfully");
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
			
		}
		
		//DELETE SUBGROUP 
		$scope.deleteSubGroup=function(subgroupId, name){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:name, type: "sub-group"};
					}
				}
			}),modalInstance.result.then(function() {
				UserGroupService.postWithObject('/groupConfig/deleteSubgroup',subgroupId).then(function(data){
					$route.reload();
					logger.logSuccess("Subgroup has been deleted successfully");
			    });
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		}
		
		//EDITGROUP
		$scope.editGroup=function(group){
		 var modalInstance;
		 modalInstance = $modal.open({
	            templateUrl: "views/template/configureNewGroupModal.html",
	            controller: "configureGroupPopUpController",
	            resolve:{
	            	"group":function(){
		            	return group;
		            }
	            }
	     }),modalInstance.result.then(function(obj) {
	    	 $route.reload();
	    	 $log.info("Modal closed at: " + new Date);
	     }, function(obj) {
             $log.info("Modal dismissed at: " + new Date);
         });
		
		}
		
		
	
		//OPEN CONFIGURE NEW GROUP POPUP
		$scope.openConfigureGroupPopUp = function (){
			var modalInstance;
			modalInstance = $modal.open({
	            templateUrl: "views/template/configureNewGroupModal.html",
	            controller: "configureGroupPopUpController",
	            resolve:{
	            	"group":function(){
		            	return "";
		            }
	            }
	     }),modalInstance.result.then(function(obj) {
	    	 $route.reload();
	    	 $log.info("Modal closed at: " + new Date);
	     }, function(obj) {
             $log.info("Modal dismissed at: " + new Date);
         });
		};
		
		
		//CONFIGURE NEW SUBGROUP POP UP
		$scope.openConfigureSubgroupPopUp = function (){
			var modalInstance;
			modalInstance = $modal.open({
	            templateUrl: "views/template/configureNewSubgroupModal.html",
	            controller: "configureSubgroupPopUpController",
	            resolve:{
	            	"subgroup":function(){
		            	return "";
		            }
	            }
	     }),modalInstance.result.then(function(obj) {
	    	 $route.reload();
	    	 $log.info("Modal closed at: " + new Date);
	     }, function(obj) {
             $log.info("Modal dismissed at: " + new Date);
         });
		};
		
		

		//EDIT SUBGROUP
		$scope.updateSubGroupAction=function(subgroup){
			 var modalInstance;
			 modalInstance = $modal.open({
		            templateUrl: "views/template/configureNewSubgroupModal.html",
		            controller: "configureSubgroupPopUpController",
		            resolve:{
		            	"subgroup":function(){
			            	return subgroup;
			            }
		            }
		     }),modalInstance.result.then(function(obj) {
		    	 $route.reload();
		    	 $log.info("Modal closed at: " + new Date);
		     }, function(obj) {
	             $log.info("Modal dismissed at: " + new Date);
	         });
		}
		
		
	}]).controller("configureGroupPopUpController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","UserGroupService","group",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger, UserGroupService,group){
		
		$scope.isUpdate=0;
		$scope.configNewGroup=UserGroupService.getConfigNewGroupDummy();
		UserGroupService.getWithoutObject('/groupConfig/fetchAvailableQueues').then(function(data){
			$scope.availableQueues=data;
		});
		
		//FETCH LIST OF ALL APPS
		UserGroupService.getWithoutObject('/accessConfig/fetchAllApps').then(function(data){
			$scope.allApps=data;
		});
		
		if(group){
			$scope.isUpdate=1;
			var prevousGroupObj=angular.copy(group);
			$scope.configNewGroup=angular.copy(group);
			//console.log($scope.configNewGroup);
			$scope.updateGroupAction=function(){
				//console.log($scope.configNewGroup);
				if($scope.createGroup.$invalid){
					logger.logError("all fields are mandatory");
				}else if(angular.equals($scope.configNewGroup,prevousGroupObj)){
					logger.logError("This group profile already exists, Please update details...");
				}else if($scope.configNewGroup.groupName.indexOf("_") == -1){
					//$scope.configNewGroup.queueMasterId = undefined;
					
					var appId = (UserGroupService.getAppId($scope.configNewGroup.appName, $scope.allApps)).appId;
					console.log(appId);
					delete $scope.configNewGroup.appName;
					$scope.configNewGroup.appId = appId;
					delete $scope.configNewGroup.queueName;
					delete $scope.configNewGroup.queueMasterId;
					
					UserGroupService.postWithObject('/groupConfig/updateGroupAction',$scope.configNewGroup).then(function(data){
							logger.logSuccess("Group has been updated successfully");
							$modalInstance.close("cancel");
					});
				}else{
					logger.logError("Underscore is not allowed in the group name");
				}
			}
		}else{
			$scope.createGroupAction=function(){
				if($scope.createGroup.$invalid){
					logger.logError("all fields are mandatory");
				}else if($scope.configNewGroup.groupName.indexOf("_") == -1){
					$scope.configNewGroup.queueMasterId = undefined;
					
					var appId = (UserGroupService.getAppId($scope.configNewGroup.appName, $scope.allApps)).appId;
					delete $scope.configNewGroup.appName;
					$scope.configNewGroup.appId = appId;
					
					UserGroupService.postWithObject('/groupConfig/createGroupAction',$scope.configNewGroup).then(function(data){
							logger.logSuccess("Group has been created successfully");
							// add default subgroup
							$scope.configNewSubgroupDefault = {};
							$scope.configNewSubgroupDefault.subGroupName="Default";
							$scope.configNewSubgroupDefault.subGroupDescription="This subgroup was created by default";
							$scope.configNewSubgroupDefault.usergroupId=data;
							$http({
						        method : 'POST',
						        url : "/groupConfig/getQueueName"
								}).success(function(data, status, headers, config) {
									$scope.configNewSubgroupDefault.queueName = data;
									$scope.configNewSubgroupDefault.queueMasterID = UserGroupService.getQueueMasterIdByQueueName($scope.availableQueues,$scope.configNewSubgroupDefault.queueName);
									$scope.configNewSubgroupDefault.queueName = undefined;
									
									UserGroupService.postWithObject('/groupConfig/createSubgroupAction',$scope.configNewSubgroupDefault).then(function(data){
										logger.logSuccess("Default Subgroup has been created successfully");
								    });
									$modalInstance.close("cancel");
								}).error(function(data, status, headers, config) {
								logger.logError(status);
						        
							});
							
					});
				}else{
					logger.logError("Underscore is not allowed in the group name");
				}
			}
		}
		
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
	    } 
		
   }]).controller("configureSubgroupPopUpController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","UserGroupService", "subgroup",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger, UserGroupService, subgroup){
	   $scope.obj={
			   customGroupName:""
	   },
	   $scope.isUpdate=0;
	   $scope.configNewSubgroup=UserGroupService.getConfigNewSubgroupDummy();
	   
	   var allGroup = []; 


	   UserGroupService.getWithoutObject('/accessConfig/fetchAllApps').then(function(data){
		   $scope.allApps=data;
	   });
	   
	   UserGroupService.getWithoutObject('/groupConfig/fetchAvailableGroups').then(function(data){
			$scope.availableGroups=data;
			allGroup = angular.copy(data);
	   });
	   
	   UserGroupService.getWithoutObject('/groupConfig/fetchAvailableQueues').then(function(data){
		   $scope.availableQueues=data;
		   //$scope.configNewSubgroup.queueName=$scope.availableQueues.queueName;
	   });
	   
		$scope.changeApplication = function(appObject) {
			$scope.obj.customGroupName = "";
	    	var appObject1= _.findWhere($scope.allApps,{"appName" : appObject.appName});
	    	$scope.availableGroups = UserGroupService.findGroupByApp(allGroup,appObject1.appId);
		}
	   
	   if(subgroup){
		   $scope.isUpdate=1;
		   $scope.configNewSubgroup.subGroupName=subgroup.subGroupName;
		   $scope.configNewSubgroup.subGroupDescription=subgroup.subGroupDescription;
		   $scope.configNewSubgroup.subGroupId=subgroup.subGroupId;
		   $scope.configNewSubgroup.queueName=subgroup.queueName;
		   $scope.configNewSubgroup.appName=subgroup.appName;
		   $scope.obj.customGroupName=subgroup.groupName;
		   
		   var prevousSubGroupObject=angular.copy($scope.configNewSubgroup);
		   var prevousSubGroupName=angular.copy($scope.obj.customGroupName);
		   $scope.updateSubgroupAction=function(){
				if($scope.createSubgroup.$invalid){
					logger.logError("all fields are mandatory");
				}else if(angular.equals($scope.configNewSubgroup,prevousSubGroupObject) && angular.equals($scope.obj.customGroupName,prevousSubGroupName)){
					logger.logError("This group profile already exists, Please update details...");
				}else if($scope.configNewSubgroup.subGroupName.indexOf("_") == -1){
					var groupId=UserGroupService.getGroupIdByGroupName(allGroup, $scope.obj.customGroupName);
					var queueMasterId = UserGroupService.getQueueMasterIdByQueueName($scope.availableQueues,$scope.configNewSubgroup.queueName);
					$scope.configNewSubgroup.usergroupId=groupId;
					if(groupId){
						$scope.configNewSubgroup.usergroupId=groupId;
						$scope.configNewSubgroup.queueMasterID =queueMasterId;
						$scope.configNewSubgroup.queueName = undefined;
						delete $scope.configNewSubgroup.appName;
						delete $scope.configNewSubgroup.appId;
						UserGroupService.postWithObject('/groupConfig/updateSubgroupAction',$scope.configNewSubgroup).then(function(data){
							logger.logSuccess("Subgroup has been updated successfully");
							$modalInstance.close("cancel");
					    });
					}
				}else{
					logger.logError("Underscore is not allowed in the subgroup name");
				}
			}   
	   }else{
		   $scope.createSubgroupAction=function(){
				if($scope.createSubgroup.$invalid){
					logger.logError("all fields are mandatory");
				}else if($scope.configNewSubgroup.subGroupName.indexOf("_") == -1){
					
					var groupId=UserGroupService.getGroupIdByGroupName(allGroup, $scope.obj.customGroupName);
					var queueMasterId = UserGroupService.getQueueMasterIdByQueueName($scope.availableQueues,$scope.configNewSubgroup.queueName);
					//var appId = (UserGroupService.getAppId($scope.configNewSubgroup.appName, $scope.allApps)).appId;
					if(groupId){
						$scope.configNewSubgroup.usergroupId=groupId;
						$scope.configNewSubgroup.queueMasterID =queueMasterId;
						$scope.configNewSubgroup.queueName = undefined;
						//$scope.configNewSubgroup.appId = appId;
						delete $scope.configNewSubgroup.appName;
						delete $scope.configNewSubgroup.appId;
						UserGroupService.postWithObject('/groupConfig/createSubgroupAction',$scope.configNewSubgroup).then(function(data){
							logger.logSuccess("Subgroup has been created successfully");
							$modalInstance.close("cancel");
					    });
					}
				}else{
					logger.logError("Underscore is not allowed in the subgroup name");
				}
			}
	   }
	   
		
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
	    } 
		
  }]);	
		
}).call(this);



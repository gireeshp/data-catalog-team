(function() {
	"use strict";

	var dataExplorer = angular.module("app.hiveExplorer", ["ngCsv" , "treeControl","FBAngular"]);
	
	dataExplorer.filter('unsafe', function($sce) {
		  return function(val) {
			  return $sce.trustAsHtml(val);
		  };
	});
	
	dataExplorer.controller("treeViewController" , ["logger","$scope", "$http", "GrabDataForExplorer","$filter","$modal","$log","$window",function(logger, $scope, $http, GrabDataForExplorer, $filter, $modal, $log,$window){
		
		$scope.showType='DataProfiling';
		
		$http.get("getStatistics").success(function(data, status, headers, config){
			$scope.explanation = data;
		});
		
		$scope.corelationFields = [];
		$scope.dataHide = true;
		// corelation Api
		$scope.selectTab = function(type){
			$scope.showType=type;
		}
		$scope.onCorelationFieldMapping = function(fieldDomain,index) {
			$scope.corelationDomain.correlationFieldValues[index].pos = fieldDomain.position;
			$scope.corelationDomain.correlationFieldValues[index].fieldType = fieldDomain.fieldDataType;
			$scope.corelationDomain.correlationFieldValues[index].filedName = fieldDomain.filedName;
			$scope.corelationDomain.correlationFieldValues[index].precision = fieldDomain.precision;
			$scope.corelationDomain.correlationFieldValues[index].scale = fieldDomain.scale;
			
			var length = $scope.corelationFields.length;

			for(var i=index; i < length ; i++){
				$scope.corelationFields[i+1] = _.reject($scope.corelationFields[i] , function(values){
					if($scope.corelationDomain.correlationFieldValues[i] != undefined)
						return fieldDomain.filedName === values.filedName;
				});
			}
			
		};
		
		$scope.openCorrelationOutput = function(store){
			
			
			var modalInstance;
			modalInstance = $modal.open({
				size : "xl",
				templateUrl : "/views/template/correlationOutput.html",
		        controller: "correlationController",
		        resolve : {
		        	 
				        	 store : function(){
								return store; 
			        	   }
		        		}
	     }),modalInstance.result.then(function(reponame) {
             
         }, function() {
             $log.info("Modal dismissed at: " + new Date);
         });
			
			
		};
		
		$scope.onAddOneMoreField = function(domain , index) {
			
			if(domain.id == "" || domain.id == undefined || domain.id == null){
				logger.logError("Please select the fields before adding");
				return false;
			}

			if($scope.corelationDomain.correlationFieldValues.length < $scope.corelationFields[0].length)
				$scope.corelationDomain.correlationFieldValues.push({});
			else
				logger.logError("All fields are already selected");
		};
		$scope.onDeleteSelectedField = function(index) {
			
			var object = _.find($scope.corelationFields[0] , function(values){
				return $scope.corelationDomain.correlationFieldValues[index].filedName == values.filedName; 
			});
			
			var length = $scope.corelationFields.length;
			
			for(var i = index ; i < length-1 ; i++){
				$scope.corelationFields[i] = angular.copy($scope.corelationFields[i+1]);
				$scope.corelationFields[i].push(object);
			}
			
			$scope.corelationFields.pop();
			$scope.corelationDomain.correlationFieldValues.splice(index ,1) ;
		};
		
		$scope.onRunJobForCorelation = function(){
			
			if($scope.corelationDomain.correlationFieldValues.length >= 2){
				
				$http.post("/runCorelation",$scope.corelationDomain).success(function(response){
					if(response){
						
						jobTrackerGrid(response);
						logger.logSuccess("Job is started.");
					}
				});
			}else {
				logger.logError("Please select at two fields for corelation.");
			}
		};
		
		$scope.showSelected = function(node , isSelected){
			if($scope.corelationFields != undefined && $scope.corelationFields.length > 0){ 
				if(confirm("Do you want to set correlation for another data source?")){
					$scope.selectDataSourceForCorrelation(node , isSelected);
				}
			} else {
				$scope.selectDataSourceForCorrelation(node , isSelected);
			}
		};
		
		$scope.selectDataSourceForCorrelation = function(node,isSelected){
			
			if(isSelected && node.type == 'DS' && $scope.showType !== 'DataProfiling'){
				$http.get("/fieldMapping?dataSourceId="+node.id).success(function(response){
					$scope.corelationDomain = { "methodSelected" : "", "dataSourceId" : node.id,
							"correlationFieldValues" : [ {"pos" : "","filedName" : "","fieldType" : "" }]};
					if(response != null && response.length > 0){
						$scope.corelationFields[0] = _.filter(response,function(num){
							num.filedName = num.fieldName;
							return  num.fieldDataType == 'INTEGER' || num.fieldDataType == 'LONG' || num.fieldDataType == 'DOUBLE' || num.fieldDataType == 'DECIMAL';
					});
				 }
				 if($scope.corelationFields[0] === null || $scope.corelationFields[0].length === 0){
					logger.logError("Data source should contain more than one numeric fields for correlation");
					$scope.corelationDomain = null;
					return false;
				 }
				 
				 if($scope.corelationDomain !== null)
					 $scope.dataHide = false;
				 else 
					 $scope.dataHide = true;
				 
				});
			}
		};
		
		angular.element($window).bind("beforeunload", function (event) {
			 if($scope.corelationFields != undefined && $scope.corelationFields.length > 0
					 && $location.path() !== "/home"){
					return 'Sure?';
			 } 
		 });
		
		$scope.treeOptions = {
			    nodeChildren: "exploreTreeDomains",
			    dirSelectable: true,
			    injectClasses: {
			        ul: "animate-fade-up",
			        li: "a2",
			        liSelected: "a7",
			        iExpanded: "fa fa-caret-down",
			        iCollapsed: "fa fa-caret-right",
			        iLeaf: " fa fa-file",
			        label: "a6",
			        labelSelected: "a8"
			    }
			};
		
		GrabDataForExplorer.getExplorerData("/explorer/TreeLeaf?fromLeafType=ROOT").then(function(data){
			if(data){
				$scope.dataForTheTree = data;
			}
			
		});
		$scope.displayLeaf = function(node, expanded){
			if (expanded) {
				if (node.type == "APP_INST") {
					node.count=node.exploreTreeDomains.length;
				} else {
					var arrayObject = [];
					var objNode = new Object();
					objNode.label="Loading ...";
					arrayObject.push(objNode);
					node.exploreTreeDomains = arrayObject;
					$http.get("/explorer/TreeLeaf?id=" + node.id + "&dataSourceName=" + node.label + "&fromLeafType=" + node.type).success(function(data, status, headers, config){
						node.exploreTreeDomains = data;
						if (node.type != "Data" && node.type != "DR-HOLDER") {
							node.count=data.length;
						}
					});
				}
			}
		}

		
		
		$http.get("/getJobs").success(function(data, status, headers, config){
			
			jobTrackerGrid(data);
			
			/*if(data){ // Removed condition as if data in null or Empty then it will couse NaN in Filter records
				jobTrackerGrid(data);
			}*/
	    	
	    });
		
		function jobTrackerGrid(data){
			//if(data){
				return $scope.stores = data	
			        , $scope.searchKeywords = "", 
			        $scope.filteredStores = [],
			        $scope.row = "", 
			        $scope.select = function(page) {
						$scope.pageNum = page;
			            var end, start;
			            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores1 = $scope.filteredStores.slice(start, end);
			        }, 
			        $scope.onFilterChange = function() {
			            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
			        }, $scope.onNumPerPageChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.onOrderChange = function() {
			            return $scope.select(1), $scope.currentPage = 1;
			        }, $scope.search = function() {
			            return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
			        }, $scope.order = function(rowName) {
			            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0;
			        }, $scope.numPerPageOpt = [3, 5, 10, 20],
			        $scope.numPerPage = $scope.numPerPageOpt[1], 
			        $scope.currentPage = 1, 
			        $scope.currentPageStores1 = [],
			        ($scope.init = function() {
			            return $scope.search(), $scope.select($scope.currentPage) , $scope.order('-jobInstanceId');
			        })();
			//}
		}
		
	}]).controller("correlationController" , ["$scope" , "$modalInstance", "$http" , "logger","store", function($scope ,$modalInstance,$http,logger,store){
		
		var jobInstanceId = store.jobInstanceId;
		var offsetInfo = store.offsetValue;
		$scope.drName = store.drName;
		$scope.offsetInfo = offsetInfo;
		$scope.isActive  = 'page1';
		$scope.pearsons = true;
		
		
		$http.get("getStatistics").success(function(data, status, headers, config){
			$scope.explanation = data;
		});
		
		$http.get("/correlationOutput?jobInstanceId=" + jobInstanceId).success(function(data, status, headers, config){
			$scope.overAllResult = data;
			
			$scope.allResult = _.groupBy($scope.overAllResult.result, 'field1');
			for(var i = 0 ; i < $scope.overAllResult.fields.length ; i++){
				var name = $scope.overAllResult.fields[i];
				$scope.allResult[name] = _.sortBy($scope.allResult[name], 'field2');
				if($scope.overAllResult.totalRecord == undefined){
					$scope.overAllResult.totalRecord = $scope.allResult[name][0].measurement;
				}
		   }
			
		});
		
		$scope.closeAll = function(){
			$modalInstance.close("dismiss");
		};
		
		$scope.correlationData = function(value){
			$scope.isActive  = value;
			$scope.pearsons = false;
			$scope.spearman = false;
			
			if(value == "page1") {
				$scope.pearsons = true;
			} else if (value == "page2"){
				$scope.spearman = true;
			}
		};
	}]).controller("dataProfileCtrl" , ["$scope" , "$http" , "logger","$filter","$modal", "$log", function($scope, $http,logger,$filter, $modal, $log){
		
		$http.get("getStatistics").success(function(data, status, headers, config){
			$scope.explanation = data;
		});
	
		$scope.openDataProfiling = function(store){
			
			var modalInstance;
			modalInstance = $modal.open({
				size : "xl",
				templateUrl : "/views/template/DataProfileModal.html",
		        controller: "dataProfileController",
		        resolve : {
		        	store : function(){
		        		return store; 
		        	},
				    profileType : function(){
						return null; 
				    }
		        }
			}),modalInstance.result.then(function(reponame) {
            }, function() {
            	$log.info("Modal dismissed at: " + new Date);
            });
		};
		
		$http.get("/getAllPipelineJobs").success(function(data, status, headers, config){
	    	return $scope.stores = data	
	        , $scope.searchKeywords = "", 
	        $scope.filteredStores = [],
	        $scope.row = "", 
	        $scope.select = function(page) {
	    		$scope.pageNum = page;
	            var end, start;
	            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
	        }, 
	        $scope.onFilterChange = function() {
	            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
	        }, $scope.onNumPerPageChange = function() {
	            return $scope.select(1), $scope.currentPage = 1;
	        }, $scope.onOrderChange = function() {
	            return $scope.select(1), $scope.currentPage = 1;
	        }, $scope.search = function() {
	            return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
	        }, $scope.order = function(rowName) {
	            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.stores, rowName), $scope.onOrderChange()) : void 0;
	        }, $scope.numPerPageOpt = [3, 5, 10, 20],
	        $scope.numPerPage = $scope.numPerPageOpt[1], 
	        $scope.currentPage = 1, 
	        $scope.currentPageStores = [],
	        ($scope.init = function() {
	            return $scope.search(), $scope.select($scope.currentPage), $scope.order("-jobInstanceId");
	        })();
	    });
		
	}]).controller("dataProfileController" , ["$scope" , "$modalInstance" , "$http", "logger", "store","profileType", function($scope ,$modalInstance,$http,logger, store, profileType){
		var jobInstanceId = store.jobInstanceId;
		var drId = store.drId;
		/*$scope.drName = store.drName.substring(store.drName.indexOf('-')+10,store.drName.length);*/
		//$scope.drName = store.dataSourceName.replace("PROFILING - ","");
		$scope.drName = store.drName.replace("PROFILING - ","");
		$scope.profileType = profileType;
		$scope.dataprofiledata = true;
		$scope.isActive = 'data';
		$scope.isProfile = 'Pre';
		$scope.profileShow = true;
		
		$scope.openPreAndPostDataProfiling = function(profileType){
			
			if(profileType === 'PreProf'){
				$scope.isProfile = 'Pre';
			}else{
				$scope.isProfile = 'Post';
			}
			var reqDataObject = {'jobInstanceId': jobInstanceId, 'profileType': profileType, 'drId': drId};
			$http.post("/getDataProfileSummary", reqDataObject).success(function(data,status, headers, config){
				if(null == data.summary){
					logger.logError(profileType + " was not ran.");
					$scope.profileShow = false;
					return false;
				}
				
				$scope.profileShow = true;
				
				$scope.rows = data.summary.rows;
	        	$scope.dataRefreshDate = data.dataRefreshDate;
	        	$scope.recordCount = data.recordCount;
	        	// populating Summary Stats
	        	if($scope.rows != null && $scope.rows.length > 0) {
	               	$scope.headerFields = data.summary.header.fields;
	            	$scope.headerFields  =  _.sortBy($scope.headerFields, 'fieldName');
	         		for(var i = 0;i < $scope.rows.length ;i++) {
	    			if($scope.rows[i].fields.length != $scope.headerFields.length){
	    				for(var j = 0 ;j < $scope.headerFields.length ; j++) {
	    					var fieldDomain = {"fieldName": $scope.headerFields[j].fieldName };
	    					var checkStatus = _.findWhere($scope.rows[i].fields, fieldDomain);
	    					if(undefined == checkStatus){
	    						$scope.rows[i].fields.push(fieldDomain);
	    					}
	    				}
	    			}
	        		$scope.rows[i].fields = _.sortBy($scope.rows[i].fields, 'fieldName');
	        	  }
	        	}
	        	// populate Value Freq
	        	if(data.freq != null){
	                $scope.countfields = data.freq;
	                $scope.countfields.rows  = _.sortBy($scope.countfields.rows, 'fieldName');
	        	}
			}).error(function(data){
				logger.logError(data);
			});
		}
    
		$scope.openPreAndPostDataProfiling('PreProf');
		
		$scope.dataProfile = function(optionView) {
			$scope.isActive = optionView;
			if ('data' == optionView){
				$scope.dataprofiledata = true;
				$scope.valueFrequencydata = false;
			}else if('value1' == optionView){
				$scope.valueFrequencydata = true;
				$scope.dataprofiledata = false;
			}
       };
       $scope.closefun = function() {
    	   $modalInstance.close();
       }
      
	}]).controller("captureDelimiterController" , ["$scope" , "$modalInstance" , "$http", "logger","hbaseSelector",  function($scope ,$modalInstance,$http,logger,hbaseSelector){
		
		if(hbaseSelector === true){
			$scope.saveSelectOption = [",","\\t"];
			$scope.delimiter = ",";
			$scope.select = {
					delimiter : ","
				};
		}else{
			$scope.saveSelectOption = ["~","\\t",",","other"];
			$scope.delimiter = "~";
			$scope.select = {
				delimiter : "~"
			};
		}
		
		$scope.delimiterCapture = function(delimeter,opHeaderStatus){
			
			if($scope.select.delimiter.length !== 1 && !($scope.disabled)) {
					logger.logError("Please provide header with max-length 1");
					return false;
	
			}
			var hiveOutputDetails = {"headerStatus" : opHeaderStatus, "delimiter" : $scope.select.delimiter, "ishbase" : hbaseSelector};
			$modalInstance.close(hiveOutputDetails);
		};
		
		$scope.disabled = true;
		
		$scope.selectDelimeter = function(del){
			if(del !== "other"){
				$scope.select.delimiter = del;
				$scope.disabled = true;
			}
			else{
				$scope.select.delimiter = "";
				$scope.disabled = false;
			}
		},
		
		$scope.dismiss = function(delimiter){
			$modalInstance.dismiss();
		};
		
	}]);
}).call(this);
(function(){
	"use strict";
	
	var module = angular.module("app.contactController",[]);
	
	module./*controller("productOverviewController" , ["$routeParams", "$scope", "$http","$cookieStore", "$location","$cookies" ,function($routeParams, $scope,$http, $cookieStore, $location, $cookies){
		console.log($location.path());
		$scope.clickSignIn = function(){
    		 $scope.id = "content";
    		 $location.path("/signin");
    	};
	}]).*/controller("contactController" , ["$routeParams", "$scope", "$http","$cookieStore", "$location","logger" ,function($routeParams, $scope,$http, $cookieStore, $location, logger){
		var cookiesData = $cookieStore.get('emailCookie');
		if(!(angular.isUndefined(cookiesData))){
    		$scope.id = "content";
	    	$location.path("/landing");
	    }
		$scope.inquiryObject = {
				id : null,
				userName : "",
				companyName : "",
				cellNumber : "",
				businessEmail : "",
				comments : ""
		};
		$scope.saveFeedback = function(){
			if($scope.inquiryObject.userName == ""){
				logger.logError("Please fill user name.");
				return false;
			} 
			if($scope.inquiryObject.companyName == ""){
				logger.logError("Please fill company name.");
				return false;
			}
			if($scope.inquiryObject.cellNumber == ""){
				logger.logError("Please fill mobile number.");
				return false;
			}
			if($scope.inquiryObject.businessEmail == ""){
				logger.logError("Please fill email id.");
				return false;
			}
			if($scope.inquiryObject.comments == ""){
				logger.logError("Please fill comments.");
				return false;
			}
				
			$http({
				method : "post",
				url : "/saveContacts",
				data : $scope.inquiryObject
			}).success(function(data){
				logger.logSuccess("Contact information saved successfully");
				$scope.inquiryObject = "";
			}).error(function(status){
				logger.logError(status);
			});
		};
	}]).controller("requestDemoController" , ["$routeParams", "$scope", "$http","$cookieStore", "$location","logger" ,function($routeParams, $scope,$http, $cookieStore, $location, logger){
		var cookiesData = $cookieStore.get('emailCookie');
		if(!(angular.isUndefined(cookiesData))){
    		$scope.id = "content";
	    	$location.path("/landing");
	    }
		$scope.inquiryObject = {
				id : null,
				userName : "",
				companyName : "",
				cellNumber : "",
				businessEmail : "",
				comments : ""
		};
		$scope.saveFeedback = function(){
			if($scope.inquiryObject.userName == ""){
				logger.logError("Please fill user name.");
				return false;
			} 
			if($scope.inquiryObject.companyName == ""){
				logger.logError("Please fill company name.");
				return false;
			}
			if($scope.inquiryObject.cellNumber == ""){
				logger.logError("Please fill mobile number.");
				return false;
			}
			if($scope.inquiryObject.businessEmail == ""){
				logger.logError("Please fill email id.");
				return false;
			}
			if($scope.inquiryObject.comments == ""){
				logger.logError("Please fill comments.");
				return false;
			}
			$http({
				method : "post",
				url : "/saveContacts",
				data : $scope.inquiryObject
			}).success(function(data){
				logger.logSuccess("Contact information saved successfully");
				$scope.inquiryObject = "";
			}).error(function(status){
				logger.logError(status);
			});
		};
	}]);
}).call(this);
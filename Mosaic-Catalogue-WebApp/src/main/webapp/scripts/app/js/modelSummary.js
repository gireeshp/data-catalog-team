(function(){
	
	var module=angular.module("app.modelSummary",[]);
	
	module.controller("modelSummaryCtrl",["$scope","$routeParams",function($scope,$routeParams){
		
		var modelSummaryParams = {"appId" : $routeParams.param1, "nodeId" : $routeParams.param2, "jobInstanceId" : $routeParams.param3, "context" : 'ML_ALGORITHM_SUMMARY'};
		$scope.isHidden = true;
		$scope.readMoreSummary = true;
		$scope.readMoreDesc = true;
		$http.post("/getModelSummary",modelSummaryParams).success(function(data){
				$scope.MLModel=data["ML_ALGORITHM_SUMMARY"];
				if($scope.MLModel != null){
					if($scope.MLModel.data != null){
						$scope.barData=[];
						$scope.lengthOfsummary = $scope.MLModel.data.modelSummary.length;
						$scope.lengthOfDescription = $scope.MLModel.description.length;
						angular.forEach($scope.MLModel.data.featureImportance,function(value,key){
						  $scope.barData.push({"Positive":value,"key":key});	
						})
						
						/*for (var int = 0; int < 15; int++) {
							 $scope.barData.push({"Positive":int+1,"key":"key"+int});
						}*/
						
					}
				}
				
				if($scope.lengthOfsummary < 170 || $scope.MLModel == null || $scope.MLModel.data == null){
					$scope.readMoreSummary = false;
				}
				if($scope.lengthOfDescription < 170 || $scope.MLModel == null){
					$scope.readMoreDesc = false;
				}
				
		});
		
		$scope.checkIfObjectIsEmpty=function(obj){
			if(!obj)
				return true;
			return (Object.keys(obj).length==0 ? true : false);
		}
	
	}]).directive("singliBarGraph",["D3factory","$window", function(D3factory, $window) {
		return{
			restrict:"EA",
			scope : {
				data : "="
			},
			link:function(scope, element, attrs){
				D3factory.d3().then(function(d3) {

					scope.$watch('data', function(newValue, oldValue){
						scope.render(_.sortBy( angular.copy(scope.data), function( item ) { return -item.Positive; } ));
					}, true);					

					scope.render = function(data){

						if (!data) return;

						d3.select(element[0]).select("svg").remove();

						var margin = {
		                            top: 20,
		                            right: 20,
		                            bottom: 180,
		                            left: 40
		                },
						width = ((252.5*data.length)/5) - margin.left - margin.right,
						height = 600 - margin.top - margin.bottom;
						
						console.log("width new : "+width);
						
						if(width<1215){
							width=1215;
						}
						
						var x = d3.scale.ordinal()
						       .rangeRoundBands([0, width-50], .5);
					   /*var xScaleRange = data.length * 30;
						  var  x  = d3.scale.ordinal()
						    .rangeRoundBands([width / 5 - xScaleRange, width / 5 + xScaleRange], 0.2);*/

						var y = d3.scale.linear()
						.rangeRound([height, 0]);

						var color = d3.scale.category20c();

						d3.select("body").append("tooltip").remove();

						var tooltip = d3.select("body").append("tooltip")
						.attr("class", "tooltip_bar")
						.style("opacity", 1e-6);

						var xAxis = d3.svg.axis()
						.scale(x)
						.orient("bottom");

					    var yAxis = d3.svg.axis()
	                        .scale(y)
	                        .orient('left')
	                        .ticks(5)
	                        .tickFormat(function(d){return d+ "%"});

						var svg = d3.select(element[0]).append("svg")
						//.attr("width", width + margin.left + margin.right)
						.attr("width", width)
						.attr("height", height + margin.top + margin.bottom)
						.append("g")
						.attr("transform", "translate(" + margin.left + "," + margin.top + ")");


						color.domain(d3.keys(data[0]).filter(function(key) {return key !== "key"; }));

						data.forEach(function(d) {							
							var y0 = 0;
							d.ages = color.domain().map(function(name) { return {name: name, y0: y0, y1: y0 += +d[name]}; });
							d.total = d.ages[d.ages.length - 1].y1;
						});	
						x.domain(data.map(function(d) {return d.key; }));
						/*y.domain([0, 100]);*/
						y.domain([0, d3.max(data, function(d) {return d.total; })]);

						svg.append("g")
						.attr("class", "x axis")
						.attr("transform", "translate(0," + height + ")")
						.call(xAxis)
						.selectAll("text").call(wrap)
						.style("text-anchor", "end")
						.attr("dx", "-.4em")	
						.attr("dy", "-.15em")
						.attr("transform", "rotate(-45)" );

						svg.append("g")
						.attr("class", "y axis")
						.call(yAxis)
						.append("text")
						.text("Importance")
						.attr("transform", "rotate(-90)")
						.attr("y", 6)
						.attr("dy", ".71em")
						.style("text-anchor", "end");
						
						//TEXT WRAPPIG LOGIC
						function wrap(text) {
							  text.each(function() {
								  var text = d3.select(this);
								  words=text.text();
								  if(words.length>=38){
									  words=words.substr(0, 30)+"...";
								  }
								  text.text(words);
								  });
						}

						var state = svg.selectAll(".state")
						.data(data)
						.enter().append("g")
						.attr("class", "g")
						.attr("transform", function(d) { return "translate(" + x(d.key) + ",0)"; });

						state.selectAll("rect")
						.data(function(d) { return d.ages; })
						.enter().append("rect")
						.attr("width", Math.min(x.rangeBand()-2, 100))
						.attr("y", function(d) { return y(d.y1); })
						.attr("height", function(d) { return y(d.y0) - y(d.y1); })
						.style("fill", function(d) { return color(d.name); })
						.on("mouseover", function(d){
							tooltip.html(function(){
								return d.y1 - d.y0;
							});
							tooltip.style("opacity", 0.9)
							.style("top", (d3.event.pageY-10)+"px")
							.style("left", (d3.event.pageX+10)+"px");
						}).on("mouseout", function(d){
							tooltip.style("opacity", 0.0);
						});
					};
				});
			}
		}
	}])	

}).call(this)
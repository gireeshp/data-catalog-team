(function(){
	"use strict";
	
	var projects = angular.module("app.projectsData",['ui.bootstrap', 'app.datasource']);
	
	projects.directive('newProject',function($compile, logger,$http){
	    return {
	    	templateUrl : '/views/template/addNewProjectView.html',
	    	link: function (scope, element, attrs) {
    	       scope.remove = function () {
    	    	   if(scope.flip && scope.gridActive){
    	    		   element[0].firstElementChild.firstElementChild.style.transform = 'rotateY(180deg)';
        	    	   element[0].firstElementChild.firstElementChild.firstElementChild.style.display = 'none';
        	    	   element[0].firstElementChild.firstElementChild.lastElementChild.style.display = 'block';
    	    	   }
    	    	   scope.isDisable = false;
    	    	   if(scope.flip == 0){
    	    		   element.remove();
    	    	   }
    	           scope.flip = 0;
    	       };
    	    },
		    controller: function ( $scope, $element , logger,$http) {	
			   $scope.project = {
	        		projectname:'',
	    			description:'',
	    			type:"0"
		       };
			   $scope.validateProjectFields = function(){
	    			if($scope.project.projectname == undefined || $scope.project.projectname == undefined == null || $scope.project.projectname == "" ){
	    				logger.logError("Project name cannot be blank and only ( _ ) special characters are allowed ");
	    				return false;
	    			}
	    			if($scope.project.projectname.length > 30){
	    				logger.logError("Project name should not be more than 30 characters long");
	    				return false;
	    			}
	    			if($scope.project.description == undefined || $scope.project.description == undefined == null || $scope.project.description == ""){
	    				logger.logError("Project description cannot be blank");
	    				return false;
	    			}
	    			if($scope.project.type == undefined || $scope.project.type == undefined == null || $scope.project.type == "0"){
	    				logger.logError("Select project type");
	    				return false;
	    			}
	    			$scope.createNewProjectAajaxCall();
	    		}
			   $scope.setAccessTypeOwner= '';
				$scope.createNewProjectAajaxCall = function(){
					$http({
						method:"POST",
						url:"/saveProjectsDetails",
						data:$scope.project
					}).success(function(data){
						if(data){
							if(!data.accessType)
								data.accessType = "OWNER"; // Set access type as owner for newly created project
							$scope.projectDetail1 = data;
							if($scope.gridActive){
								$scope.flip = 1;
							}
							$scope.remove();
							setTimeout(function(){	
								document.getElementsByClassName("newProject_create")[0].remove();
								$scope.filteredProject.push(data);
								$scope.initData($scope.projectList);
								$scope.$apply();
							},2000);
							$scope.setAccessTypeOwner = 'OWNER'; // Set access type as owner for newly created project
							logger.logSuccess("Project details save successfully.");
						}else{
							logger.logError(data);
						}
					}).error(function(data){
						logger.logError(data);
					});
	    		}
				
				$scope.getProjectList = function(){
					$http({
						method:"POST",
						url:"/getAllProjectsDetails"
					}).success(function(data){
						
					}).error(function(data){
						logger.logError(data);
					});
				}
				
				$scope.initData = function(data){
					$scope.stores = data;
					$scope.filterByStatus();
				}
		    }
	    };
	});
	projects.controller("projectsController" , ["$anchorScroll","$route", "$scope", "$modal" , "$log" , "$location" , "$routeParams", "$http" , "$rootScope", "logger","$filter", "$cookieStore","$window","$document","$compile", "$interval", function($anchorScroll,$route, $scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $filter, $cookieStore,$window,$document,$compile,$interval){
		$(document).ready(function() {
		    $(".dropdown-toggle").dropdown();
		});
		$cookieStore.remove('projectId');
		$cookieStore.remove('projectName');
		$cookieStore.remove('projectAccessType');
		$scope.searchKeyWord = "";
		$scope.sortByAscDsc = "-createdDate";
		$scope.scrollTop = 0;
		$scope.flip = 0;
		var fabScrollElement = null;
		$scope.filteredProject = [];
		$scope.stores = [];
		$scope.selectedAccessTypes = [];
		$scope.initProject = function(){
			fabScrollElement = document.getElementById('projects');
			if(fabScrollElement){
				fabScrollElement.addEventListener("scroll", function(event){
					$scope.scrollTop = event.target.scrollTop;
		    	}, false);
			}
			
			$scope.setHeight = function(){
				var footer = $("#footer-bar").height();
				var header = $("#header").height();
				var scrollHeight = (($(document).height() - (header + footer)) - 20);
				$("#projects").css("max-height",scrollHeight+"px");
				$("#projects").css("height",scrollHeight+"px");
			}
			$scope.setHeight();
			
			$(window).resize(function(){
				$scope.setHeight();
			});
			
			 $scope.showListView = function(){
				 $scope.listActive = true;
				 $scope.gridActive = false;
			 }
			 $scope.showGridView = function(){
				 $scope.listActive = false;
				 $scope.gridActive = true;
			 }
			 $scope.showGridView();
	         $scope.stores = $scope.filteredProject;
	         $scope.searchKeywordsBkp = "";
	         $scope.search = function() {
	        	   $scope.searchKeywordsBkp = angular.copy($scope.searchKeywords);
	        	   if(typeof $scope.searchKeywordsBkp != "undefined" && $scope.searchKeywordsBkp != "") {
	        		   $scope.projectList = $filter("filter")($scope.stores, $scope.searchKeywordsBkp);
	        	   }else{
	        		   $scope.projectList = $scope.filteredProject;
	        	   }
	         },
	         $scope.filterByAccessType = function() {
	        	 if($scope.selectedAccessTypes.length == 0){
	        		 $scope.projectList = $scope.filteredProject;
	        	 } else {
	        		 $scope.projectList = $scope.stores.filter(function(e) {return $scope.selectedAccessTypes.indexOf(e.accessType.toLowerCase())!=-1});
	        	 }
	         },
	         ($scope.init = function() {
	        	   $scope.search();
	         })();     
		};
		
		$scope.initializeProjectList = function (){
        	$http({
    			method:"POST",
    			url:"/getAllProjectsDetails"
    		}).success(function(data){
    			$scope.projectList = data;
    			$scope.projectListBkp = angular.copy($scope.projectList);
    			$scope.filteredProject = $scope.projectList;
    			$scope.initProject();
    		}).error(function(data){
    			logger.logError(data);
    		});
        };
		
		$scope.getProjectType = function(){
			$http({
				method:"POST",
				url:"/getProjectsTypes"
			}).success(function(data){
				$scope.projectType = data.trim().split(",");
				$scope.initializeProjectList();
			}).error(function(data){
				logger.logError(data);
			});
		}
		$rootScope.projectFetchIndicator = false;
        $interval(function() {
        	if($rootScope.projectFetchIndicator) {
        		$scope.initializeProjectList();
        		$rootScope.projectFetchIndicator = false;
        	}
		}, 1000);
        	  
        $scope.getProjectType();
        $scope.isDisable = false;
		$scope.newProjectModalOpen = function(){
			var isExists = document.getElementsByClassName("newProject_create");
			if(isExists && isExists.length == 0){
				var divElement = angular.element(document.getElementById('projectListDiv'));
				var appendHtml = $compile('<div new-project class="newProject_create"></div>')($scope);
				divElement.prepend(appendHtml);
			}
			$scope.isDisable = true;
		};
		 
		$scope.onChangeAccessType = function(checkStatus, accessType) {
			if(checkStatus) {
				$scope.selectedAccessTypes.push(accessType);
			}
			else {
				var removalIndex = $scope.selectedAccessTypes.indexOf(accessType);
				if(removalIndex > -1){
					$scope.selectedAccessTypes.splice(removalIndex, 1);
				}
			}
			
			$scope.filterByAccessType();
		};
		
		$scope.filterByStatus = function(){
			//$scope.projectList = $scope.projectListBkp;
			if($scope.soryByActiveInactive == undefined || $scope.soryByActiveInactive == ""){
				$scope.sortByAscDsc = "-createdDate";
			} else {
				$scope.sortByAscDsc = "createdDate";
			}
			$scope.init();
		}
		
		$scope.getProjectResources = function(selectedProjectDetail){
			
			$cookieStore.put("projectId", parseInt(selectedProjectDetail.id));
			$cookieStore.put("projectName", selectedProjectDetail.projectName);
			$cookieStore.put("projectAccessType",selectedProjectDetail.accessType ? selectedProjectDetail.accessType.toLowerCase() : $scope.setAccessTypeOwner? $scope.setAccessTypeOwner.toLowerCase():''); //setting AccessType as "Owner" because newly created project doesnt have it 
			
			$routeParams.selectedProjectList = selectedProjectDetail;
			$location.path("/designUser");
			
			/*$http({
				method:"POST",
				url:"/getProjectsDetails?projectId="+selectedProjectDetail.id
			}).success(function(data){
				
			}).error(function(data){
				logger.logError(data);
			});*/
		};
				
		// move scroll to top
		$scope.moveToTop = function(){
			scrollTo(fabScrollElement, -10, 600);
		} 
		
		// smooth scroll script
		var scrollTo  = function(element, to, duration) {
		    if (duration <= 0) return;
		    var difference = to - element.scrollTop;
		    var perTick = difference / duration * 10;

		    setTimeout(function() {
		        element.scrollTop = element.scrollTop + perTick;
		        if (element.scrollTop === to) return;
		        scrollTo(element, to, duration - 10);
		    }, 10);
		};
		
		$scope.setHeight = function(){
			var footer = $("#footer-bar").height();
			var header = $("#header").height();
			var scrollHeight = ($(document).height() - (header + footer));
			$("#projects").css("max-height",scrollHeight+"px");
		};
	}]);
}).call(this);
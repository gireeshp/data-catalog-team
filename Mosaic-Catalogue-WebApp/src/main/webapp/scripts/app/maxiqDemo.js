(function(){
	"use strict";
	
	var demo = angular.module("app.demo",[]);
	
	demo.directive("zoomableSunburst",[function(){
		return {
			restrict : "E",
			scope:{
				data:"="
			},
			controller: "maxiqDemoController",
			link:function(scope,element){
			
			var width = 690,
			    height = 600,
			    radius = Math.min(width, height) / 2;

			var x = d3.scale.linear().range([0, 2 * Math.PI]);

			var y = d3.scale.linear().range([0, radius]);

			var color = d3.scale.category20c();

			var svg = d3.select(element[0]).append("svg")
			    .attr("width", width)
			    .attr("height", height)
			    .append("g")
			    .attr("transform", "translate(" + width / 2 + "," + (height / 2 + 10) + ")");

			var partition = d3.layout.partition().value(function(d) { return d.size; });

			var arc = d3.svg.arc()
			    .startAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x))); }).
			    endAngle(function(d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x + d.dx))); }).
			    innerRadius(function(d) { return Math.max(0, y(d.y)); }).
			    outerRadius(function(d) { return Math.max(0, y(d.y + d.dy)); });
			
			
			scope.render = function(data){
				 var root = data;
					 
					  var g = svg.selectAll("g").data(partition.nodes(root)).enter().append("g");
					  
					  
					  var path = g.append("path")
					    .attr("d", arc)
					    .style("fill", function(d) { return color((d.children ? d : d.parent).name); })
					    .on("click", click);

					  var text = g.append("text")
					    .attr("transform", function(d) { return "rotate(" + computeTextRotation(d) + ")"; })
					    .attr("x", function(d) { return y(d.y); })
					    .attr("dx", "4") // margin
					    .attr("dy", ".25em") // vertical-align
					    .text(function(d) { return d.name; });

					  function click(d) {
					    // fade out all text elements
					    text.transition().attr("opacity", 0);

					    path.transition()
					      .duration(750)
					      .attrTween("d", arcTween(d))
					      .each("end", function(e, i) {
					          // check if the animated element's data e lies within the visible angle span given in d
					          if (e.x >= d.x && e.x < (d.x + d.dx)) {
					            // get a selection of the associated text element
					            var arcText = d3.select(this.parentNode).select("text");
					            // fade in the text element and recalculate positions
					            arcText.transition().duration(750)
					              .attr("opacity", 1)
					              .attr("transform", function() { return "rotate(" + computeTextRotation(e) + ")" })
					              .attr("x", function(d) { return y(d.y); });
					          }
					      });
					  }
					
			}
			
			function arcTween(d) {
			  var xd = d3.interpolate(x.domain(), [d.x, d.x + d.dx]),
			      yd = d3.interpolate(y.domain(), [d.y, 1]),
			      yr = d3.interpolate(y.range(), [d.y ? 15 : 0, radius]);
			  return function(d, i) {
			    return i
			        ? function(t) { return arc(d); }
			        : function(t) { x.domain(xd(t)); y.domain(yd(t)).range(yr(t)); return arc(d); };
			  };
			}

			function computeTextRotation(d) {
			  return (x(d.x + d.dx / 2) - Math.PI / 2) / Math.PI * 180;
			}
			
			scope.$watch('data', function(){
	              scope.render(scope.data);
	        }, true); 
				
				
		  }
		};
	}]);
	
	demo.controller("maxiqDemoController", ["$scope" ,function($scope){
		$scope.customers= [
							{
								   custName : "Piyush Acharya",
								   custSex: "Male",
								   custPlace:"Pune",
								   custSince: "6 Years",
								   custType:"House-hold Customer",
								   custSell:Math.floor((Math.random() * 10) + 1)
								   
							},
							{
			                	   custName : "Poonam Bhise",
			                	   custSex: "Female",
			                	   custPlace:"Pune , India",
			                	   custSince: "2 Years",
								   custType:"House-hold Customer",
								   custSell:Math.floor((Math.random() * 10) + 1)
			                	   
			                },
							{
			                	   custName : "Sachin Vyas",
			                	   custSex: "Male",
			                	   custPlace:"Nagpur",
			                	   custSince: "6 Years",
								   custType:"House-hold Customer",
								   custSell:Math.floor((Math.random() * 10) + 1)
			                },
		                   
		                   {
		                	   custName : "Shweta Vyas",
		                	   custSex: "Female",
		                	   custPlace:"Chennai",
		                	   custSince: "3 Years",
							   custType:"House-hold Customer",
							   custSell:Math.floor((Math.random() * 10) + 1)
		                	   
		                   },
		                   {
		                	   custName : "Shivanand Pawar",
		                	   custSex: "Male",
		                	   custPlace:"Bangalore",
		                	   custSince: "2 Years",
							   custType:"House-hold Customer",
							   custSell:Math.floor((Math.random() * 10) + 1)
		                   }
		          ];
		
		
		
		$scope.grabCustInfo = function(customer){
			$scope.selectedRCust = customer;
			
			if(customer.custName == "Shivanand Pawar"){
				$scope.myData = {"name":"Shivanand","children":[{"name":"Card Spends","children":[{"name":"Type","children":[{"name":"Dining","children":[{"name":"Fine-Dine","size":12},{"name":"Pubs","size":12},{"name":"Resorts","size":2},{"name":"Others","size":12}],"size":0},{"name":"Travel","children":[{"name":"International","size":3},{"name":"National","size":12}],"size":0},{"name":"Health","children":[{"name":"Medicines","size":12},{"name":"Wellness","size":2},{"name":"Misc","size":3}],"size":0},{"name":"Shopping","children":[{"name":"Fashion","size":3},{"name":"Household","size":3},{"name":"Kids","size":3},{"name":"Gadgets","size":3},{"name":"Misc","size":3}],"size":0},{"name":"Other","size":2}],"size":0},{"name":"Period","children":[{"name":"Early Month","size":12},{"name":"Mid Month","size":2},{"name":"End Month","size":1}],"size":0},{"name":"Locations","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31},{"name":"Pin code3","size":31},{"name":" (can have more)","size":31}],"size":0}],"size":0},{"name":"Products","children":[{"name":"Cards","size":1},{"name":"Loans","size":2},{"name":"Insurance","size":31},{"name":"Savings","children":[{"name":"Income","size":31},{"name":"Fixed Outgo Avg","size":31}],"size":0}],"size":0},{"name":"Transactional Channels","children":[{"name":"Online Banking","size":31},{"name":"Mobile Banking","size":12},{"name":"ATM","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Merchant POS","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Branch","size":31},{"name":"Call Center","size":12}],"size":0},{"name":"Interactions","children":[{"name":"Satisfaction","children":[{"name":"Positive","size":12},{"name":"Negative","size":21},{"name":"Neutral","size":12},{"name":"Unknown","size":1}],"size":0},{"name":"Recency","children":[{"name":"Week","size":2},{"name":"Month","size":21},{"name":"3 Months","size":12}],"size":0},{"name":"Type","children":[{"name":"Outbound Mail","children":[{"name":"Responded","size":12},{"name":"Not-responded","size":21}],"size":0},{"name":"Product Application","children":[{"name":"approval","size":21},{"name":"rejected","size":2}],"size":0},{"name":"Customer Complaint","children":[{"name":"Resolved","size":12},{"name":"Open","size":21}],"size":0}],"size":0}],"size":0},{"name":"Profile","children":[{"name":"Personal","children":[{"name":"Interests","size":12}],"size":0},{"name":"Household","size":12},{"name":"Professional","size":12},{"name":"Socio-demog","size":21}],"size":0}],"size":0};
			}else if(customer.custName == "Sachin Vyas"){
				$scope.myData = {"name":"Sachin","children":[{"name":"Card Spends","children":[{"name":"Type","children":[{"name":"Dining","children":[{"name":"Fine-Dine","size":12},{"name":"Pubs","size":12},{"name":"Resorts","size":2},{"name":"Others","size":12}],"size":0},{"name":"Travel","children":[{"name":"International","size":3},{"name":"National","size":12}],"size":0},{"name":"Health","children":[{"name":"Medicines","size":12},{"name":"Wellness","size":2},{"name":"Misc","size":3}],"size":0},{"name":"Shopping","children":[{"name":"Fashion","size":3},{"name":"Household","size":3},{"name":"Kids","size":3},{"name":"Gadgets","size":3},{"name":"Misc","size":3}],"size":0},{"name":"Other","size":2}],"size":0},{"name":"Period","children":[{"name":"Early Month","size":12},{"name":"Mid Month","size":2},{"name":"End Month","size":1}],"size":0},{"name":"Locations","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31},{"name":"Pin code3","size":31},{"name":" (can have more)","size":31}],"size":0}],"size":0},{"name":"Products","children":[{"name":"Cards","size":1},{"name":"Loans","size":2},{"name":"Insurance","size":31},{"name":"Savings","children":[{"name":"Income","size":31},{"name":"Fixed Outgo Avg","size":31}],"size":0}],"size":0},{"name":"Transactional Channels","children":[{"name":"Online Banking","size":31},{"name":"Mobile Banking","size":12},{"name":"ATM","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Merchant POS","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Branch","size":31},{"name":"Call Center","size":12}],"size":0},{"name":"Interactions","children":[{"name":"Satisfaction","children":[{"name":"Positive","size":12},{"name":"Negative","size":21},{"name":"Neutral","size":12},{"name":"Unknown","size":1}],"size":0},{"name":"Recency","children":[{"name":"Week","size":2},{"name":"Month","size":21},{"name":"3 Months","size":12}],"size":0},{"name":"Type","children":[{"name":"Outbound Mail","children":[{"name":"Responded","size":12},{"name":"Not-responded","size":21}],"size":0},{"name":"Product Application","children":[{"name":"approval","size":21},{"name":"rejected","size":2}],"size":0},{"name":"Customer Complaint","children":[{"name":"Resolved","size":12},{"name":"Open","size":21}],"size":0}],"size":0}],"size":0},{"name":"Profile","children":[{"name":"Personal","children":[{"name":"Interests","size":12}],"size":0},{"name":"Household","size":12},{"name":"Professional","size":12},{"name":"Socio-demog","size":21}],"size":0}],"size":0};
			}else if(customer.custName == "Piyush Acharya"){
				$scope.myData = {"name":"Piyush","children":[{"name":"Card Spends","children":[{"name":"Type","children":[{"name":"Dining","children":[{"name":"Fine-Dine","size":12},{"name":"Pubs","size":12},{"name":"Resorts","size":2},{"name":"Others","size":12}],"size":0},{"name":"Travel","children":[{"name":"International","size":3},{"name":"National","size":12}],"size":0},{"name":"Health","children":[{"name":"Medicines","size":12},{"name":"Wellness","size":2},{"name":"Misc","size":3}],"size":0},{"name":"Shopping","children":[{"name":"Fashion","size":3},{"name":"Household","size":3},{"name":"Kids","size":3},{"name":"Gadgets","size":3},{"name":"Misc","size":3}],"size":0},{"name":"Other","size":2}],"size":0},{"name":"Period","children":[{"name":"Early Month","size":12},{"name":"Mid Month","size":2},{"name":"End Month","size":1}],"size":0},{"name":"Locations","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31},{"name":"Pin code3","size":31},{"name":" (can have more)","size":31}],"size":0}],"size":0},{"name":"Products","children":[{"name":"Cards","size":1},{"name":"Loans","size":2},{"name":"Insurance","size":31},{"name":"Savings","children":[{"name":"Income","size":31},{"name":"Fixed Outgo Avg","size":31}],"size":0}],"size":0},{"name":"Transactional Channels","children":[{"name":"Online Banking","size":31},{"name":"Mobile Banking","size":12},{"name":"ATM","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Merchant POS","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Branch","size":31},{"name":"Call Center","size":12}],"size":0},{"name":"Interactions","children":[{"name":"Satisfaction","children":[{"name":"Positive","size":12},{"name":"Negative","size":21},{"name":"Neutral","size":12},{"name":"Unknown","size":1}],"size":0},{"name":"Recency","children":[{"name":"Week","size":2},{"name":"Month","size":21},{"name":"3 Months","size":12}],"size":0},{"name":"Type","children":[{"name":"Outbound Mail","children":[{"name":"Responded","size":12},{"name":"Not-responded","size":21}],"size":0},{"name":"Product Application","children":[{"name":"approval","size":21},{"name":"rejected","size":2}],"size":0},{"name":"Customer Complaint","children":[{"name":"Resolved","size":12},{"name":"Open","size":21}],"size":0}],"size":0}],"size":0},{"name":"Profile","children":[{"name":"Personal","children":[{"name":"Interests","size":12}],"size":0},{"name":"Household","size":12},{"name":"Professional","size":12},{"name":"Socio-demog","size":21}],"size":0}],"size":0};
			}else if(customer.custName == "Shweta Vyas"){
				$scope.myData = {"name":"Shweta","children":[{"name":"Card Spends","children":[{"name":"Type","children":[{"name":"Dining","children":[{"name":"Fine-Dine","size":12},{"name":"Pubs","size":12},{"name":"Resorts","size":2},{"name":"Others","size":12}],"size":0},{"name":"Travel","children":[{"name":"International","size":3},{"name":"National","size":12}],"size":0},{"name":"Health","children":[{"name":"Medicines","size":12},{"name":"Wellness","size":2},{"name":"Misc","size":3}],"size":0},{"name":"Shopping","children":[{"name":"Fashion","size":3},{"name":"Household","size":3},{"name":"Kids","size":3},{"name":"Gadgets","size":3},{"name":"Misc","size":3}],"size":0},{"name":"Other","size":2}],"size":0},{"name":"Period","children":[{"name":"Early Month","size":12},{"name":"Mid Month","size":2},{"name":"End Month","size":1}],"size":0},{"name":"Locations","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31},{"name":"Pin code3","size":31},{"name":" (can have more)","size":31}],"size":0}],"size":0},{"name":"Products","children":[{"name":"Cards","size":1},{"name":"Loans","size":2},{"name":"Insurance","size":31},{"name":"Savings","children":[{"name":"Income","size":31},{"name":"Fixed Outgo Avg","size":31}],"size":0}],"size":0},{"name":"Transactional Channels","children":[{"name":"Online Banking","size":31},{"name":"Mobile Banking","size":12},{"name":"ATM","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Merchant POS","children":[{"name":"Pin code 1","size":12},{"name":"Pin code 2","size":31}],"size":0},{"name":"Branch","size":31},{"name":"Call Center","size":12}],"size":0},{"name":"Interactions","children":[{"name":"Satisfaction","children":[{"name":"Positive","size":12},{"name":"Negative","size":21},{"name":"Neutral","size":12},{"name":"Unknown","size":1}],"size":0},{"name":"Recency","children":[{"name":"Week","size":2},{"name":"Month","size":21},{"name":"3 Months","size":12}],"size":0},{"name":"Type","children":[{"name":"Outbound Mail","children":[{"name":"Responded","size":12},{"name":"Not-responded","size":21}],"size":0},{"name":"Product Application","children":[{"name":"approval","size":21},{"name":"rejected","size":2}],"size":0},{"name":"Customer Complaint","children":[{"name":"Resolved","size":12},{"name":"Open","size":21}],"size":0}],"size":0}],"size":0},{"name":"Profile","children":[{"name":"Personal","children":[{"name":"Interests","size":12}],"size":0},{"name":"Household","size":12},{"name":"Professional","size":12},{"name":"Socio-demog","size":21}],"size":0}],"size":0};
			}
			
			
		}
		
		
		
	}]);
	
}).call(this);
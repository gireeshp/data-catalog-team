(function() {
	
        "use strict";
        var maxIqApp = angular.module('app', ['ngRoute', "ngAnimate", 
                                             	"ui.bootstrap", "easypiechart",
                                             	"mgo-angular-wizard", "textAngular", 
                                             	"angular-loading-bar", "app.ui.ctrls", 
                                             	"app.dataSourceTable", "app.datasource",
                                             	"app.hiveExplorer",
                                             	"app.ui.directives", 
                                             	"app.ui.services", "app.controllers", 
                                             	"app.directives", "app.form.validation", 
                                             	"app.ui.form.ctrls", "app.ui.form.directives", 
                                             	"app.tables", "app.task", "app.localization" , 
                                             	"app.appView", 
                                             	 
                                             	"app.signin", "app.signup", "app.detailCookies", 
                                             	
                                           
              								"app.rdbmsTestConnection",
              								"app.userProfile",
                                             	"logToServer" , 'ngTouch', 'ui.grid', 'ui.grid.resizeColumns', 
                                             	'ui.grid.moveColumns',"darthwade.loading","app.groupConfig",
                                             	"ngSanitize","ui.select","app.accessConfig","app.userManagement", "app.visualize",
                                             	"app.contactController",
                                             	"app.D3factory","app.discover","app.datasourceinfo", "app.customComponentRCode",
                                             	"app.dataSourceHistory",
                                             	"app.glossary","app.profileData" , "app.dataSourceApi",
                                          	"app.category","app.connector", "ivh.treeview","app.connections", "app.users",
                                          	"app.treeChart","mj.scrollingTabs"
                                             	// Extra packages
                          ]);

            	
		var blocked_Urls = null;
        maxIqApp.factory('myService', function() {
            return {
            	loggedInComplete: function(local_blocked_Urls) {
            		blocked_Urls = local_blocked_Urls;
 
            		var urlFlavour = window.location.hash;
                	urlFlavour = urlFlavour.substring(1, urlFlavour.length);            	
                	if (blocked_Urls.indexOf(urlFlavour) > -1) {
                		window.location.href  = window.location.href.substring(0, window.location.href.indexOf("/#/")+2) + "/licenseCheck";
                	}            	
            	}
            };
        });
     
        function applyLeftNavForNewTab() {        	
        	if (null == blocked_Urls) {
        		setTimeout(applyLeftNavForNewTab, 100);
        	} else {
            	var urlFlavour = window.location.hash;
            	urlFlavour = urlFlavour.substring(1, urlFlavour.length);            	
            	if (blocked_Urls.indexOf(urlFlavour) > -1) {
            		window.location.href  = window.location.href.substring(0, window.location.href.indexOf("/#/")+2) + "/licenseCheck";
            	}
        	}
        }

        function applyLeftNavForCurrentTab(jspViewName) {
        	var urlFlavour = window.location.hash;
        	urlFlavour = urlFlavour.substring(1, urlFlavour.length);            	
        	if (null != blocked_Urls && blocked_Urls.indexOf(urlFlavour) > -1) {
        		return "unauthorized.jsp";
        	} else {
        		return jspViewName;
        	}
        }
        	
          maxIqApp.config(function(ivhTreeviewOptionsProvider) {
        	 ivhTreeviewOptionsProvider.set({
        	   defaultSelectedState: false,
        	   validate: true,
        	   twistieLeafTpl: ''
        	 });
        	});

       		maxIqApp.config(["$routeProvider", function($routeProvider) {

       			//getBlockedList();
       			applyLeftNavForNewTab();
       			 
       			return $routeProvider.when("/", {
	                redirectTo: "/signin"
	            }).when("/home", {
	                templateUrl: "/signin.jsp",
	                title: "Home"
	            }).when("/catalogHome", {
	                templateUrl: "views/catalog/catalogHome.html",
	                title: "Home"
	            }).when("/subSolutionDetails", {
	                templateUrl: "/views/home/subSolutionDetails.jsp",
	                title: "Home"
	            }).when("/landing", {
	            	redirectTo: "/discover"
	            }).when("/homepage", {
	            	redirectTo: "/businessUser"
	            }).when("/requestDemo", {
	                templateUrl: "/requestdemo.jsp",
	                title: "Request Demo"
	            }).when("/connectorList", {
	                templateUrl: "/connectorList.jsp",
	                title: "Connector List"
	            }).when("/publish", {
	                templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/Connection/configNewConnection.html");
	            	 }
	            }).when("/signin", {
	                templateUrl: "/signin.jsp"
	            }).when("/signup", {
	                templateUrl: "/signup.jsp"
	            }).when("/userProfile", {
	                templateUrl: "views/user/userProfile.jsp"
	            }).when("/resetPassword", {
	                templateUrl: "views/template/resetPassword.html"
	            }).when("/pages/profile", {
	                templateUrl: "views/pages/profile.html"
	            }).when("/dataSourceView" , {
	            	templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/DataSource/dataSourceView.jsp");
	            	 }
	            }).when("/connectors" , {
	            	templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/connectors/connectors.jsp");
	            	 }
	            }).when("/subconnectors" , {
	            	templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/connectors/subConnectors.jsp");
	            	 }
	            }).when("/dataSourceApiList" , {		
		            templateUrl: function() {  		
		            	 return applyLeftNavForCurrentTab("views/setup/datasourceApiList.jsp");		
		            }		
		        }).when("/licenseCheck" , {
	            	 templateUrl: "unauthorized.jsp" 
	            }).when("/dataSourceToAccept" , {
	            	templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/DataSource/dataSourceToAccept.jsp");
	            	 }
	            }).when("/dataSourceRequests" , {
	            	templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/DataSource/dataSourceRequestView.jsp");
	            	 }
	            }).when("/myCollection" , {
	            	templateUrl: "views/DataSource/myCollection.jsp"
	            }).when("/glossaryView" , {
	            	templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/DataSource/glossaryView.jsp");
	            	 }
	            }).when("/hiveExplore" , {
	            	 templateUrl: "views/explore/hiveExplore.jsp"
	            }).when("/visualize" , {
	            	 templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/explore/visualize.jsp");
	            	 }
	            }).when("/discover/:param1",{
	            	templateUrl: "views/DataSource/dataSourceWithoutPopup.jsp"
	            }).when("/modelsView", {
	                templateUrl: "views/setup/analyticsModels.jsp"
	            }).when("/modelSummary/:param1/:param2/:param3", {
	                templateUrl: "views/setup/modelSummary.jsp"
	            }).when("/connections", {		
					templateUrl: function() {  
	            		 return applyLeftNavForCurrentTab("views/Connection/connectionView.jsp");
	            	 }
			    }).when("/expressions", {		
					templateUrl: "views/expression/expressionView.jsp"         // Mercer code change
			    }).when("/groupConfig" , {
	            	 templateUrl: "views/setup/groupConfig.jsp"
	            }).when("/accessConfig" , {
	            	 templateUrl: "views/setup/accessConfig.jsp"
	            }).when("/discover" , {
	            	 templateUrl: "views/DataSource/discover.jsp"
	            }).when("/404", {
	                templateUrl: "views/pages/404.html"
	            }).when("/demo", {
	                templateUrl: "views/demo/demo.jsp"
	            }).when("/test", {
	                templateUrl: "views/demo/test.jsp"
	            }).when("/categoryView" , {		
		            	templateUrl: "views/DataSource/categoryView.jsp"		
		        }).when("/unauthorized" , {
	            	 templateUrl: "views/unAthorizesAccess/unAthorizesAccess.jsp"
	            }).when("/userManagement" , {
	            	 templateUrl: "views/setup/settings_view.jsp"
	            }).otherwise({
	                redirectTo: "/404"	
	            });
       }]);  	
}).call(this);;
(function() {
	"user strict";
	 var profileDataModule = angular.module("app.profileData" , []);
	 
	 profileDataModule.filter('unsafe', function($sce) {
		  return function(val) {
	      return $sce.trustAsHtml(val);
	   };
	 });

	 profileDataModule.filter('commaToDecimal', function(){
	    return function(value) {
	        return value ? parseFloat(value).toFixed(2).toString().replace(',', '') : null;
	    };
	 });
	 
	profileDataModule.factory("cloudWordLayout",function(){
		function cloud() {
		    var size = [256, 256],
		        text = cloudText,
		        font = cloudFont,
		        fontSize = cloudFontSize,
		        rotate = cloudRotate,
		        padding = cloudPadding,
		        spiral = archimedeanSpiral,
		        words = [],
		        timeInterval = Infinity,
		        event = d3.dispatch("word", "end"),
		        timer = null,
		        cloud = {};

		    cloud.start = function() {
		      var board = zeroArray((size[0] >> 5) * size[1]),
		          bounds = null,
		          n = words.length,
		          i = -1,
		          tags = [],
		          data = words.map(function(d, i) {
		        return {
		          text: text.call(this, d, i),
		          font: font.call(this, d, i),
		          rotate: rotate.call(this, d, i),
		          size: ~~fontSize.call(this, d, i),
		          padding: cloudPadding.call(this, d, i)
		        };
		      }).sort(function(a, b) { return b.size - a.size; });

		      if (timer) clearInterval(timer);
		      timer = setInterval(step, 0);
		      step();

		      return cloud;

		      function step() {
		        var start = +new Date,
		            d;
		        while (+new Date - start < timeInterval && ++i < n && timer) {
		          d = data[i];
		          d.x = (size[0] * (Math.random() + .5)) >> 1;
		          d.y = (size[1] * (Math.random() + .5)) >> 1;
		          cloudSprite(d, data, i);
		          if (place(board, d, bounds)) {
		            tags.push(d);
		            event.word(d);
		            if (bounds) cloudBounds(bounds, d);
		            else bounds = [{x: d.x + d.x0, y: d.y + d.y0}, {x: d.x + d.x1, y: d.y + d.y1}];
		            // Temporary hack
		            d.x -= size[0] >> 1;
		            d.y -= size[1] >> 1;
		          }
		        }
		        if (i >= n) {
		          cloud.stop();
		          event.end(tags, bounds);
		        }
		      }
		    }

		    cloud.stop = function() {
		      if (timer) {
		        clearInterval(timer);
		        timer = null;
		      }
		      return cloud;
		    };

		    cloud.timeInterval = function(x) {
		      if (!arguments.length) return timeInterval;
		      timeInterval = x == null ? Infinity : x;
		      return cloud;
		    };

		    function place(board, tag, bounds) {
		      var perimeter = [{x: 0, y: 0}, {x: size[0], y: size[1]}],
		          startX = tag.x,
		          startY = tag.y,
		          maxDelta = Math.sqrt(size[0] * size[0] + size[1] * size[1]),
		          s = spiral(size),
		          dt = Math.random() < .5 ? 1 : -1,
		          t = -dt,
		          dxdy,
		          dx,
		          dy;

		      while (dxdy = s(t += dt)) {
		        dx = ~~dxdy[0];
		        dy = ~~dxdy[1];

		        if (Math.min(dx, dy) > maxDelta) break;

		        tag.x = startX + dx;
		        tag.y = startY + dy;

		        if (tag.x + tag.x0 < 0 || tag.y + tag.y0 < 0 ||
		            tag.x + tag.x1 > size[0] || tag.y + tag.y1 > size[1]) continue;
		        // TODO only check for collisions within current bounds.
		        if (!bounds || !cloudCollide(tag, board, size[0])) {
		          if (!bounds || collideRects(tag, bounds)) {
		            var sprite = tag.sprite,
		                w = tag.width >> 5,
		                sw = size[0] >> 5,
		                lx = tag.x - (w << 4),
		                sx = lx & 0x7f,
		                msx = 32 - sx,
		                h = tag.y1 - tag.y0,
		                x = (tag.y + tag.y0) * sw + (lx >> 5),
		                last;
		            for (var j = 0; j < h; j++) {
		              last = 0;
		              for (var i = 0; i <= w; i++) {
		                board[x + i] |= (last << msx) | (i < w ? (last = sprite[j * w + i]) >>> sx : 0);
		              }
		              x += sw;
		            }
		            delete tag.sprite;
		            return true;
		          }
		        }
		      }
		      return false;
		    }

		    cloud.words = function(x) {
		      if (!arguments.length) return words;
		      words = x;
		      return cloud;
		    };

		    cloud.size = function(x) {
		      if (!arguments.length) return size;
		      size = [+x[0], +x[1]];
		      return cloud;
		    };

		    cloud.font = function(x) {
		      if (!arguments.length) return font;
		      font = d3.functor(x);
		      return cloud;
		    };

		    cloud.rotate = function(x) {
		      if (!arguments.length) return rotate;
		      rotate = d3.functor(x);
		      return cloud;
		    };

		    cloud.text = function(x) {
		      if (!arguments.length) return text;
		      text = d3.functor(x);
		      return cloud;
		    };

		    cloud.spiral = function(x) {
		      if (!arguments.length) return spiral;
		      spiral = spirals[x + ""] || x;
		      return cloud;
		    };

		    cloud.fontSize = function(x) {
		      if (!arguments.length) return fontSize;
		      fontSize = d3.functor(x);
		      return cloud;
		    };

		    cloud.padding = function(x) {
		      if (!arguments.length) return padding;
		      padding = d3.functor(x);
		      return cloud;
		    };

		    return d3.rebind(cloud, event, "on");
		  }

		  function cloudText(d) {
		    return d.text;
		  }

		  function cloudFont() {
		    return "serif";
		  }

		  function cloudFontSize(d) {
		    return Math.sqrt(d.value);
		  }

		  function cloudRotate() {
		    return (~~(Math.random() * 6) - 3) * 30;
		  }

		  function cloudPadding() {
		    return 1;
		  }

		  // Fetches a monochrome sprite bitmap for the specified text.
		  // Load in batches for speed.
		  function cloudSprite(d, data, di) {
		    if (d.sprite) return;
		    c.clearRect(0, 0, (cw << 5) / ratio, ch / ratio);
		    var x = 0,
		        y = 0,
		        maxh = 0,
		        n = data.length;
		    di--;
		    while (++di < n) {
		      d = data[di];
		      c.save();
		      c.font = ~~((d.size + 1) / ratio) + "px " + d.font;
		      var w = c.measureText(d.text + "m").width * ratio,
		          h = d.size << 1;
		      if (d.rotate) {
		        var sr = Math.sin(d.rotate * cloudRadians),
		            cr = Math.cos(d.rotate * cloudRadians),
		            wcr = w * cr,
		            wsr = w * sr,
		            hcr = h * cr,
		            hsr = h * sr;
		        w = (Math.max(Math.abs(wcr + hsr), Math.abs(wcr - hsr)) + 0x1f) >> 5 << 5;
		        h = ~~Math.max(Math.abs(wsr + hcr), Math.abs(wsr - hcr));
		      } else {
		        w = (w + 0x1f) >> 5 << 5;
		      }
		      if (h > maxh) maxh = h;
		      if (x + w >= (cw << 5)) {
		        x = 0;
		        y += maxh;
		        maxh = 0;
		      }
		      if (y + h >= ch) break;
		      c.translate((x + (w >> 1)) / ratio, (y + (h >> 1)) / ratio);
		      if (d.rotate) c.rotate(d.rotate * cloudRadians);
		      c.fillText(d.text, 0, 0);
		      c.restore();
		      d.width = w;
		      d.height = h;
		      d.xoff = x;
		      d.yoff = y;
		      d.x1 = w >> 1;
		      d.y1 = h >> 1;
		      d.x0 = -d.x1;
		      d.y0 = -d.y1;
		      x += w;
		    }
		    var pixels = c.getImageData(0, 0, (cw << 5) / ratio, ch / ratio).data,
		        sprite = [];
		    while (--di >= 0) {
		      d = data[di];
		      var w = d.width,
		          w32 = w >> 5,
		          h = d.y1 - d.y0,
		          p = d.padding;
		      // Zero the buffer
		      for (var i = 0; i < h * w32; i++) sprite[i] = 0;
		      x = d.xoff;
		      if (x == null) return;
		      y = d.yoff;
		      var seen = 0,
		          seenRow = -1;
		      for (var j = 0; j < h; j++) {
		        for (var i = 0; i < w; i++) {
		          var k = w32 * j + (i >> 5),
		              m = pixels[((y + j) * (cw << 5) + (x + i)) << 2] ? 1 << (31 - (i % 32)) : 0;
		          if (p) {
		            if (j) sprite[k - w32] |= m;
		            if (j < w - 1) sprite[k + w32] |= m;
		            m |= (m << 1) | (m >> 1);
		          }
		          sprite[k] |= m;
		          seen |= m;
		        }
		        if (seen) seenRow = j;
		        else {
		          d.y0++;
		          h--;
		          j--;
		          y++;
		        }
		      }
		      d.y1 = d.y0 + seenRow;
		      d.sprite = sprite.slice(0, (d.y1 - d.y0) * w32);
		    }
		  }

		  // Use mask-based collision detection.
		  function cloudCollide(tag, board, sw) {
		    sw >>= 5;
		    var sprite = tag.sprite,
		        w = tag.width >> 5,
		        lx = tag.x - (w << 4),
		        sx = lx & 0x7f,
		        msx = 32 - sx,
		        h = tag.y1 - tag.y0,
		        x = (tag.y + tag.y0) * sw + (lx >> 5),
		        last;
		    for (var j = 0; j < h; j++) {
		      last = 0;
		      for (var i = 0; i <= w; i++) {
		        if (((last << msx) | (i < w ? (last = sprite[j * w + i]) >>> sx : 0))
		            & board[x + i]) return true;
		      }
		      x += sw;
		    }
		    return false;
		  }

		  function cloudBounds(bounds, d) {
		    var b0 = bounds[0],
		        b1 = bounds[1];
		    if (d.x + d.x0 < b0.x) b0.x = d.x + d.x0;
		    if (d.y + d.y0 < b0.y) b0.y = d.y + d.y0;
		    if (d.x + d.x1 > b1.x) b1.x = d.x + d.x1;
		    if (d.y + d.y1 > b1.y) b1.y = d.y + d.y1;
		  }

		  function collideRects(a, b) {
		    return a.x + a.x1 > b[0].x && a.x + a.x0 < b[1].x && a.y + a.y1 > b[0].y && a.y + a.y0 < b[1].y;
		  }

		  function archimedeanSpiral(size) {
		    var e = size[0] / size[1];
		    return function(t) {
		      return [e * (t *= .1) * Math.cos(t), t * Math.sin(t)];
		    };
		  }

		  function rectangularSpiral(size) {
		    var dy = 4,
		        dx = dy * size[0] / size[1],
		        x = 0,
		        y = 0;
		    return function(t) {
		      var sign = t < 0 ? -1 : 1;
		      // See triangular numbers: T_n = n * (n + 1) / 2.
		      switch ((Math.sqrt(1 + 4 * sign * t) - sign) & 3) {
		        case 0:  x += dx; break;
		        case 1:  y += dy; break;
		        case 2:  x -= dx; break;
		        default: y -= dy; break;
		      }
		      return [x, y];
		    };
		  }

		  // TODO reuse arrays?
		  function zeroArray(n) {
		    var a = [],
		        i = -1;
		    while (++i < n) a[i] = 0;
		    return a;
		  }

		  var cloudRadians = Math.PI / 180,
		      cw = 1 << 11 >> 5,
		      ch = 1 << 11,
		      canvas,
		      ratio = 1;

		  if (typeof document !== "undefined") {
		    canvas = document.createElement("canvas");
		    canvas.width = 1;
		    canvas.height = 1;
		    ratio = Math.sqrt(canvas.getContext("2d").getImageData(0, 0, 1, 1).data.length >> 2);
		    canvas.width = (cw << 5) / ratio;
		    canvas.height = ch / ratio;
		  } else {
		    // node-canvas support
		    var Canvas = require("canvas");
		    canvas = new Canvas(cw << 5, ch);
		  }

		  var c = canvas.getContext("2d"),
		      spirals = {
		        archimedean: archimedeanSpiral,
		        rectangular: rectangularSpiral
		      };
		  c.fillStyle = "red";
		  c.textAlign = "center";

		return {
			getCloudWord : cloud
		};
	}); 
	 
	profileDataModule.controller("profileDataCtrl" , ["$anchorScroll","$filter","$route",'$routeParams', "$scope", "$modal" , "$log"  , "$location" , "$http" , "$rootScope", "logger","$cookieStore", "$interval","$q","$timeout","cloudWordLayout",  function($anchorScroll ,$filter, $route,$routeParams, $scope, $modal, $log , $location ,$http, $rootScope, logger, $cookieStore, $interval,$q,$timeout, cloudWordLayout ){
		 $scope.profileDataArray = [""];
		 $scope.showRightPanel = false;
		 $scope.scaleTypeList = ['categorical', 'text', 'constant','continuous'];
		 $scope.sampleSize = ['5000', '10000', '20000','50000','100000'];
		 $scope.jobName = "sample_Profile";
		 $scope.scale = "default";
		 $scope.limit = "5000";
		 $scope.dsName = $routeParams.param1;
		 $scope.columnName = "*";
		 $scope.selectedObj = null;
		 $scope.loader = false;
		 $scope.scaleChangeDataFetch = false;
		 $scope.selectedRow = "";
		 $scope.closeDropDown = "N";
		 
		 // promise object
		 var deferred = $q.defer();
		 
		 $scope.loadProfileData = function(){
//			 var data = {"scale":$scope.scale, "jobName":$scope.jobName, "limit":$scope.limit, "tableName": $scope.dsName, "columnName": $scope.columnName};
//			 $scope.loader = true;
//			 $http({
//    			method:"POST",
//    			url:"/getProfilingResult",
//    			data:data
//			 }).success(function(data){
//				 if(data != undefined && data != "" && data != null && $scope.profileDataArray.length > 0){
//					if($scope.scaleChangeDataFetch){
//	    				 if(angular.lowercase(data[0].scale) == "error"){
//	    					 logger.logError("No data available for selected variable.");
//	    				 }else{
//	    					 _.extend(_.findWhere($scope.profileDataArray , { fieldName: data[0].fieldName }), data[0]);
//		    				 $timeout(function(){
//		    					 angular.element("#"+data[0].fieldName.split(".")[1]+"_row").triggerHandler('click');
//		    				 }, 300);
//	    				 }	 
//	    			}else{
//	    				$scope.profileDataArray = data;
//	    				$timeout(function(){
//	    					angular.element("#"+$scope.profileDataArray[0].fieldName.split(".")[1]+"_row").triggerHandler('click');
//	    				}, 300);	
//	    			}
//				}
//			 	$scope.scaleChangeDataFetch = false;
//    			deferred.resolve(true);
//    			$scope.loader = false;
//    		 }).error(function(data){
//    			$scope.profileDataArray = [];
//    			deferred.reject(true);
//    			$scope.loader = false;
//    		 });
//			 
//			 return deferred.promise;
			 	 
		 } 
		 
		 $scope.loadProfileDataByLimit = function(){
			 $scope.showRightPanel = false;
			 $scope.scaleChangeDataFetch = false;
			 $scope.columnName = "*";
			 $scope.scale = "default";
			 $scope.loadProfileData();
		 }
		 
		 $scope.getScaleWiseProfileData = function($event, selectedScale, columnName){
			 $event.stopPropagation();
			 $event.preventDefault();
			 $scope.showRightPanel = false;
			 $scope.scaleChangeDataFetch = true;
			 $scope.limit = "5000";
			 $scope.columnName = columnName;
			 $scope.scale = selectedScale;
			 $scope.loadProfileData();
			 $event.target.offsetParent.offsetParent.className = "dropdown";
		 }
		 
		 $scope.profileFieldSelected = function($event, selectedObj){
			 $event.stopPropagation(); 
			 $scope.showRightPanel = false;
			 $scope.selectedObj = selectedObj;
			 $scope.selectedRow = selectedObj.fieldName;
			 if(selectedObj.scale == "continuous"){
				 $scope.showRightPanel = true;
				 if($scope.selectedObj.continuousStats != undefined && $scope.selectedObj.continuousStats != null && $scope.selectedObj.continuousStats != ""){
					 $timeout(function(){
				    	 $scope.loader = true;
				    	 $scope.renderBoxPlot();
						 $scope.renderHistogram();
					 },50);
				 }
			 }else if(selectedObj.scale == "categorical"){
				 $scope.loader = true;
				 $scope.showRightPanel = true;
				 $scope.loader = false;
			 }else if(selectedObj.scale == "text"){
				 $scope.showRightPanel = true;
				 if($scope.selectedObj.wordCloud != undefined && $scope.selectedObj.wordCloud != null && $scope.selectedObj.wordCloud != ""){
					 $timeout(function(){
						 $scope.loader = true;
						 $scope.renderWordCloudGraph();
					 },50);
				 }
			 }
		 }
		 
		 $scope.renderBoxPlot = function(){
			 			 
			 d3.selection.prototype.watchTransition = function(renderWatch){
			    var args = [this].concat([].slice.call(arguments, 1));
			    return renderWatch.transition.apply(renderWatch, args);
			 };
			 
			 var boxPlotEle = angular.element("#boxPlotGraph");
			 var boxPlotDivEle = angular.element("#boxplotDiv");
			 
			 boxPlotEle.empty();
			 
			 nv.addGraph(function() {
				  var maxLengthForY = 0;
				  var q1 = $scope.selectedObj.continuousStats.q1;
				  var q3 = $scope.selectedObj.continuousStats.q3;
				  var median = $scope.selectedObj.continuousStats.median;
				  var iqr = $scope.selectedObj.continuousStats.iqr;
				  var whiskerLow = ($scope.selectedObj.continuousStats.q1 - (1.5 * iqr));
				  var whiskerHigh = ($scope.selectedObj.continuousStats.q3 + (1.5 * iqr));
				  
				  var data = [
			        {
				          values: {
				            Q1: q1,
				            Q2: median,
				            Q3: q3,
				            whisker_low: whiskerLow,
				            whisker_high: whiskerHigh,
				            outliers: []
				          }
				        }
				  ];
				  
				  if(q1.toString().length > maxLengthForY ){
					  maxLengthForY = q1.toString().length;
				  }
				  
				  if(q3.toString().length > maxLengthForY ){
					  maxLengthForY = q3.toString().length;
				  }
				  
				  if(median.toString().length > maxLengthForY ){
					  maxLengthForY = median.toString().length;
				  }
				  
				  if(whiskerLow.toString().length > maxLengthForY ){
					  maxLengthForY = whiskerLow.toString().length;
				  }
				  
				  if(whiskerHigh.toString().length > maxLengthForY ){
					  maxLengthForY = whiskerHigh.toString().length;
				  }
				  
				  var margin = {top: 20, right: 20, bottom: 20, left: 2 + (maxLengthForY * 10)};
			      var chart = nv.models.boxPlotChart()
			          .maxBoxWidth(100) // prevent boxes from being incredibly wide
			          .yDomain([ q1 - (1.5 * iqr), q3  + (1.5 * iqr)])
			          .margin(margin);
			      d3.select('#boxPlotGraph')
			          .datum(data)
			          .call(chart);
			      nv.utils.windowResize(chart.update);
			     
			      return chart;
			    });
		 }
		 		 
		 $scope.renderHistogram = function(){
			 var element = angular.element("#profileHistogram");
			 element.empty();
			 
			 var histogramDiv = angular.element("#histogramDiv");
			 
			 var keys = Object.keys($scope.selectedObj.continuousStats.histogram.dataPointsList);
			 keys.sort(function(a, b){
				return a - b;
			 });
			 
			 var highestXLength = 0;
	         var highestYLength = 0;
	         
	         var data = keys.map(function (key) {

	        	var n = $scope.selectedObj.continuousStats.histogram.dataPointsList[key];
	        	var bucket = Number(key);
	        	if(bucket.toString().length > highestYLength){
	        		highestYLength = bucket.toString().length;
	        	} 
	        	
	        	if(n.toString().length > highestXLength){
	        		highestXLength = n.toString().length;
	        	} 
	            return {bucket: bucket,
	            	N: n};
	    	 });
	         		  
			 var width = histogramDiv[0].clientWidth,
			    height = 170,
			    pad = 15, 
			    left_pad = (35 + (  highestXLength * 7));
			  
			var x = d3.scale.ordinal().rangeRoundBands([left_pad - 5, width - 0], 0.1);
			var y = d3.scale.linear().range([height - pad, pad]);

			var xAxis = d3.svg.axis().scale(x).orient("bottom");
			var yAxis = d3.svg.axis().scale(y).orient("left");
			
			var color = d3.scale.category20c();
			var svg = d3.select(element[0])
	        .attr("height", (200 + (highestYLength * 6)))
			x.domain(data.map(function (d) { return d.bucket; }));
		    y.domain([0, d3.max(data, function (d) { return d.N; })]);
		    
		    svg.append("g")
	        .attr("class", "axis")
	        .attr("transform", "translate(0, "+(height - pad)+")")
	        .call(xAxis)
	        .selectAll("text")
		    .attr("y", 0)
		    .attr("x", 9)
		    .attr("dy", ".35em")
		    .attr("transform", "rotate(90)")
		    .style("text-anchor", "start");
		    
		    svg.append("g")
	        .attr("class", "axis")
	        .attr("transform", "translate("+(left_pad - pad)+", 0)")
	        .call(yAxis);
		    
		    svg.selectAll('rect')
	        .data(data)
	        .enter()
	        .append('rect')
	        .attr('class', 'bar')
	        .attr("fill", 'steelblue')
	        .attr('x', function (d) { return x(d.bucket); })
	        .attr('width', x.rangeBand())
	        .attr('y', height-pad)
	        .attr('y', function (d) { return y(d.N); })
	        .attr('height', function (d) { return height-pad - y(d.N); })
	        
	        
	       var bars = svg.selectAll(".bar")
	       	 bars.data(data)	
	        .on("mouseover", function() {
	            d3.select(this)
	            	.attr("fill", "rgb(21, 168, 209)");
	        })
	        .on("mouseout", function(d, i) {
	            d3.select(this).attr("fill", function() {
	                return "" + color(this.id) + "";
	            });
	        });

		    bars.append("title")
	        .text(function(d) {
		            return d.N;
	        });
		    $scope.loader = false;
		 }
		 
		 $scope.renderWordCloudGraph = function(){
			 	var graphEleId = angular.element("#wordCloudeGraph");
			    graphEleId.empty();
				  
				var fill = (d3.hasOwnProperty('scale')) ? d3.scale.category20() : d3.scaleOrdinal(d3.schemeCategory20);
				
				/*
				  	layout grnerator by d3 and use drawListener to generator word cloud.
				 */
			     var words = $scope.selectedObj.wordCloud.wordFreqList;
				 
				 
			     // min/max word size
			     var maxSize = d3.max(words, function(d) { return d.size; });
			     var minSize = d3.min(words, function(d) { return d.size; });

			     var fontScale = d3.scale.linear() // scale algo which is used to map the domain to the range
			       .domain([minSize, maxSize]) //set domain which will be mapped to the range values
			       .range([8,70]); // set min/max font size (so no matter what the size of the highest word it will be set to 40 in this case)
			     
		     	var layout = cloudWordLayout.getCloudWord()
		     		.size([(graphEleId[0].clientWidth - 30 ), 400])
				    .words(words)
				    .padding(5)
				    .rotate(function() {return (~~(Math.random() * 6) - 3) * 30; })
				    .font("Impact")
				    .fontSize(function(d) {return fontScale(d.size) })
				    .on("end", draw);
		     		     	
				layout.start();

				function draw(words) { 
				  d3.select(graphEleId[0]).append("svg")
				    .attr("width", layout.size()[0])
				    .attr("height", layout.size()[1])
				    .append("g")
				    .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2+ ")")
				    .selectAll("text")
				    .data(words)
				    .enter().append("text")
				      .style("font-size", function(d) { return d.size + "px"; })
				      .style("font-family", "Impact")
				      .style("fill", function(d, i) { return fill(i); })
				      .attr("text-anchor", "middle")
				      .attr("transform", function(d) {
				        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			        })
			        .text(function(d, i) { if( words.length - 1 == i ){$scope.loader = false;}  return d.text })
			        	
				}					
		 }
		 
		 $scope.$on('mySubMenuEvents', function(event, args){
			 if(args == "Profile"){
				 if($scope.profileDataArray.length > 0){
					 $timeout(function(){
						 angular.element("#"+$scope.profileDataArray[0].fieldName.split(".")[1]+"_row").triggerHandler('click');
					 }, 300);
					   
				 }
			 }
		 });
	 
	 }]);		
}).call(this);
(function(){
	"use strict";
	
	var rdbmsTestConnection = angular.module("app.rdbmsTestConnection",[]);
	
	rdbmsTestConnection.controller("rdbmsFieldMapping",["$scope" , "$http", "$log","fieldMapping", "$modalInstance", function($scope  , $http,  $log,fieldMapping,$modalInstance){
		
		$scope.fieldDetails= fieldMapping;
		
		$scope.closePopUp = function(){
			$modalInstance.dismiss('');
		};
	}]);

})();	
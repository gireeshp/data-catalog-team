(function(){
	
	
	var app=angular.module("app.relations",[]);
	
	app.service("Relations",function($q,$http,logger){
		return{
			
			getWithoutObject:function(url){
				var deferred=$q.defer();
				$http.post(url).success(function(data){
					deferred.resolve(data);
				}).error(function(data){
					logger.logError(data);
					deferred.reject(data);
				});
				return deferred.promise;
			},
			
			postWithObject:function(url,object){
				var deferred=$q.defer();
				$http.post(url,object).success(function(data){
					deferred.resolve(data);
				}).error(function(data){
					logger.logError(data);
					deferred.reject(data);
				});
				return deferred.promise;
			},
			
			getConfigureRelationDummy:function(){
				return{
					"sourceDsItem":"",
					"targetDsItem":"",
					"sourceDsList":[],
					"targetDsList":[],
					"dataSource":{}
				}
			}
			
		}
	})
	
	
	//relations pop up controller
	app.controller("relPopupCtrl",["$scope","$modalInstance","nodeData","$http","Relations",function($scope,$modalInstance,nodeData,$http,Relations){
			
		$scope.node=nodeData
		//MAX-2450 - Under data source click on Relationships not showing sample data.
		Relations.postWithObject("/relationship/getDataFields",$scope.node.id).then(function(data){
			$scope.datasource=data;
			console.log($scope.datasource);
		});
		
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
	    }
		
	}]);
	
	
	app.controller("relOverviewCtrl",["$scope","$modalInstance","$http","relationData",function($scope,$modalInstance,$http,relationData){
		
		$scope.linkData=relationData;
		
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
	    }
	}]);
	
	app.controller("crtRelController",["$scope","$http","Relations","$modal","$log","logger",function($scope,$http,Relations,$modal,$log,logger){
	
		
		$scope.init=function(){
			Relations.getWithoutObject("/relationship/fetchAvailableRelations").then(function(data){
				$scope.allRelationsDagData=data;
			});
		}();
		
		$scope.openConfigureRelationPopup=function(){

			var modalInstance;
			modalInstance = $modal.open({
	            templateUrl: "views/template/configureRelationPopup.html",
	            controller: "createRelationController",
	            size:'lg'
	     }),modalInstance.result.then(function(obj) {
	    	 logger.logSuccess("field mapping saved successfully")
	    	 $log.info("Modal closed at: " + new Date);
	    	 $scope.init();
	     }, function(obj) {
             $log.info("Modal dismissed at: " + new Date);
         });
			
		}
		
		
		
	}]).controller("createRelationController",["$scope", "$modalInstance", "$http","Relations","logger", "$rootScope",function($scope,$modalInstance,$http,Relations,logger, $rootScope){
		
		
		$scope.init=function(){
			$scope.configureRelations=Relations.getConfigureRelationDummy();
			
			var reqData = {'dataSourceId':$scope.$parent.dataSourceId};
			Relations.postWithObject("/relationship/fetchDataSourceName",reqData).then(function(data){
				$scope.configureRelations.sourceDsList=data;
			})
			Relations.getWithoutObject("/relationship/fetchDataSource").then(function(data){
				$scope.configureRelations.targetDsList=data;
			})
		}
		
		$scope.requestFieldMapping=function(){
			
			if(!$scope.configureRelations.sourceDsItem || !$scope.configureRelations.targetDsItem){
				logger.logError("Please select source and targer datasources !");
			}else{
				var obj={
						"sourceDsId":$scope.configureRelations.sourceDsItem.dataSourceId,
						"targetDsId":$scope.configureRelations.targetDsItem.dataSourceId
				}
				
				Relations.postWithObject("/relationship/requestMapping",obj).then(function(data){
					if(!data.allTargetFields){
						logger.logError("field mapping does not exist for target datasource");
						$scope.configureRelations.dataSource=[];
					}else{
						$scope.configureRelations.dataSource=data;
					}
				});
			}
		}
		
		
		
		$scope.saveFieldMapping=function(){
			
			if($scope.configureRelations.dataSource.mapping){	
				var obj={
						"sourceDsItem":$scope.configureRelations.sourceDsItem,
						"targetDsItem":$scope.configureRelations.targetDsItem,
						"dataSource":$scope.configureRelations.dataSource
			     }
				
				Relations.postWithObject("/relationship/saveFieldMapping",obj).then(function(data){
					$scope.init();
					$scope.ok();
				})
				
			}
		}
		
		
		$scope.deleteRelation=function(){
			
			if(!$scope.configureRelations.sourceDsItem || !$scope.configureRelations.targetDsItem){
				logger.logError("Please select source and targer datasources !");
			}else{
				var obj={
						"sourceDsId":$scope.configureRelations.sourceDsItem.dataSourceId,
						"targetDsId":$scope.configureRelations.targetDsItem.dataSourceId
				}
				
				Relations.postWithObject("/relationship/deleteRelation",obj).then(function(data){
					$scope.init();
					logger.logSuccess("This relation will no longer exists..!");
				})
				
				
			}
			
		}
		
		$scope.ok=function(){
			$modalInstance.close("success");
		}
	   
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
	    }
		
	}]);
	
	
	app.filter("reject",function(){
		return function(list,value){
			return _.reject(list, function(obj){ return obj.dataSourceId==value.dataSourceId; });
		}
	});
	
	
}).call(this)
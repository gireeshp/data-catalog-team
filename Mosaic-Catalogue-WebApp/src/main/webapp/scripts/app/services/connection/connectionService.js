(function(){
	var connections= angular.module("app.connections", ["app.users", "app.glossary"]);
	
	connections.service("connectionService",["$q","$http","logger",function($q,$http,logger){
		return {
			 getConnectionsList: function(connectionSources) {
		    	 return $http.post('/listAllSubSourceConnection',connectionSources).then(function(result) {
			           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     },
		     saveConnection : function(reqParam){
		     	return $http.post('/saveNewSubSourceConnection',reqParam).then(function(result) {
			           return result.data;
		    	 }).catch(function(error) {
		    		 return {"error":"Opps something went wrong!"};
		    	 });
		     },
		     testConnection : function(reqParam){
		     	return $http.post('/testConnection',reqParam).then(function(result) {
		     		 return result.data;
		    	 }).catch(function(error) {
		    		 return {"error":"Opps something went wrong!"};
		    	 });
		     },
		     getAllSchema : function(connectionDetails){
		    	 return $http.post('/listAllSchema',connectionDetails).then(function(result) {
			           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     },
		     publishDataSources : function(connectorConfig){
		    	 return $http.post('/publishDataSources',connectorConfig).then(function(result) {
			           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     },
		     deleteConnection : function(connectorConfig){
		    	 return $http.post('/deleteExistingSubSourceConnection',connectorConfig).then(function(result) {
			           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     },
		     checkDuplicateDatasources : function(dsObjects){
		    	 return $http.post('/checkDuplicateDSNames',dsObjects).then(function(result) {
			           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     }
		     
		   }
		
	}]);
	
	connections.factory("getterSetterFactory", function (){
		var connectionId  = {};
		
		return {
			setConnectionDetails: function (value){
				connectionId = value;
			},
			getConnectionDetails  : function (){
				return connectionId;
			}
		}
	});
	
}).call(this);
(function(){
	var connector= angular.module("app.connector",[]);
	
	connector.service("connectorService",["$q","$http","logger",function($q,$http,logger){
		return {
			getConnectorTypeList: function() {
		       
		    	 return $http.post('/listConnectionSource').then(function(result) {
		           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     },
		     getConnectorSubTypeList: function(sourceId) {
		    	 return $http.post('/listConnectionSourceAndSubSource?sourceId='+sourceId).then(function(result) {
			         return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     }
		     
		   }
		
	}]);

}).call(this);
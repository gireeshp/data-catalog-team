(function(){
	var discover = angular.module("app.discover", ['ui.bootstrap','app.datasource','app.appView']);
	
	discover.service('anchorSmoothScroll', function(){
	    this.scrollTo = function(eID) {

	        // This scrolling function 
	        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
	        
	        var startY = currentYPosition();
	        var stopY = elmYPosition(eID);
	        
	        var distance = stopY > startY ? stopY - startY : startY - stopY;
	        if (distance < 100) {
	            scrollTo(0, stopY); return;
	        }
	        var speed = Math.round(distance / 100);
	        if (speed >= 20) speed = 20;
	        var step = Math.round(distance / 25);
	        var leapY = stopY > startY ? startY + step : startY - step;
	        var timer = 0;
	        if (stopY > startY) {
	            for ( var i=startY; i<stopY; i+=step ) {
	                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
	                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
	            } return;
	        }
	        for ( var i=startY; i>stopY; i-=step ) {
	            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
	            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
	        }
	        
	        function currentYPosition() {
	            // Firefox, Chrome, Opera, Safari
	            if (self.pageYOffset) return self.pageYOffset;
	            // Internet Explorer 6 - standards mode
	            if (document.documentElement && document.documentElement.scrollTop)
	                return document.documentElement.scrollTop;
	            // Internet Explorer 6, 7 and 8
	            if (document.body.scrollTop) return document.body.scrollTop;
	            return 0;
	        }
	        
	        function elmYPosition(eID) {
	            var elm = document.getElementById(eID);
	            var y = elm.offsetTop;
	            var node = elm;
	            while (node.offsetParent && node.offsetParent != document.body) {
	                node = node.offsetParent;
	                y += node.offsetTop;
	            } return y;
	        }

	    };
	    
	});
	
	discover.service('discoverRestCalls', function(){
		return{

			getCatalogueData: function(url, data){
				return $http.post(url, data).then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			getCatlogueCreatedByList : function(){
				return $http.post('/getCreatedByList').then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			getDataSource : function(dataSourceName){
				return $http.post('/getDataSource', dataSourceName).then(function(response){
					return response.data;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			getDataSourceInstance : function(dataSourceName){
				return $http.post('/getDataSourceInstance', dataSourceName).then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			fetchDataSourceFeedback : function(dataSourceId){
				return $http.post('/fetchDataSourceFeedback', dataSourceId).then(function(response){
					return response.data;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			getAllFavouriteDataSouceOfUser : function(){
				return $http.post('/getAllFavouriteDataSouceOfUser').then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			markDataSourceAsFavourite : function(object){
				return $http.post('/markDataSourceAsFavourite',object).then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			getCollectionAndRequestStatusOfDatasource : function(object){
				return $http.post('/getCollectionAndRequestStatusOfDatasource',object).then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			getAddedMycollectionDataSource : function(object){
				return $http.post('/getAddedMycollectionDataSource',object).then(function(response){
					return response.data;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			getListOfGroupsToForWhichObjectNeedsToBeShared: function(object){
				return $http.post('/getListOfGroupsToForWhichObjectNeedsToBeShared',object).then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			requestForAccessConfirmation: function(object){
				return $http.post('/createRequestForObject',object).then(function(response){
					return response;
				}).catch(function(error){
					 if(error.data === "Request already raised For this group."){
						 return {"error":"Request already raised For this group."};
					 }else{
						 return {"error":"Opps something went wrong!"};
					 }
				 });
			},
			
			addToMyCollection:function(object){
				return $http.post('/addToMyCollection',object).then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			saveFeedback: function(object){
				return $http.post('/saveDataSourceFeedback',object).then(function(response){
					return response.data;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			
			getRating: function(id){
				return $http.post('/getAvgRatingForDataSource',id).then(function(response){
					return response.data;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			},
			updateDataSourceRating: function(object){
				return $http.post('/updateDataSourceRating',object).then(function(response){
					return response.data;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
			}
			
		}
	})
	
}).call(this);
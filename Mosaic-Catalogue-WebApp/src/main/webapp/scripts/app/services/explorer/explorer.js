(function(){
	var dataExplorer = angular.module("app.hiveExplorer");
	
	dataExplorer.service("GrabDataForExplorer", function($http , $q){
		return {
			getExplorerData : function(){
				return $http.post('getPublishedDS').then(function(response){
					return response;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				});
			},
			
			runPrestoQuery: function(query){
				return $http.post('runPrestoQuery', query).then(function(response){
					return response.data;
				}).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				});
				
			}
		}
	})

}).call(this);
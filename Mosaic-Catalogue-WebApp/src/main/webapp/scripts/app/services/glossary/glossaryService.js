(function(){
	var glossary= angular.module("app.glossary");
	
	glossary.service("glossaryService",["$q","$http","logger",function($q,$http,logger){
		return {
			 getTagList: function() {
		    	 return $http.post('/getAllActiveTags').then(function(result) {
			           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     }
		   }
		
	}]);

	
}).call(this);
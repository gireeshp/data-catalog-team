(function(){
	var users= angular.module("app.users",[]);
	
	users.service("usersService",["$q","$http","logger",function($q,$http,logger){
		return {
			getAllUsers: function() {
		      
		    	 return $http.get('/userManagement/getUserDetails').then(function(result) {
		           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     },
		     getGroupsData: function() {
		    	 return $http.post('/groupConfig/getGroupsData').then(function(result) {
		           return result.data;
		    	 }).catch(function(error){
		    		 return {"error":"Opps something went wrong!"};
				 });
		     }
		   }
		
	}]);

}).call(this);
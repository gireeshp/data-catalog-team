(function() {
	"use strict";
		var module = angular.module("app.signin" , ["ngCookies"]);

		module.service("csrfservice",function($http,$q,$window){
			var userDetails = {				
				emailId : "",
				password  : "",
				appId :"",
				userId:""
			}	
			return{				
				getUserObject:getUserObject						
			}					
			function getUserObject(){			
				return userDetails;
			}			
		});
        
		module.controller("signinController" , ["$rootScope","$scope","$routeParams", "$http","$location", "$cookieStore", "UserService", "FlavourService","$modal","$log","logger", "GetAccessData", "myService", "$route","csrfservice","$timeout", function($rootScope, $scope, $routeParams,  $http, $location, $cookieStore, UserService,FlavourService,$modal,$log,logger, GetAccessData, myService, $route,csrfservice,$timeout){
		
			$scope.viewHeight = 0;
			
			$scope.setViewHeight = function(){
				$timeout(function(){
					angular.element("#content").css('bottom', '0px');
					$scope.viewHeight = angular.element("#content").height();
				},500)
				
			}
			
			$scope.sessionAlreadyExist = function (appId, data) {
				
				var modalInstance;
				modalInstance = $modal.open({
					templateUrl: "/views/template/confirmationBox.html",
				    controller: "confirmationBoxController",
				    resolve : {
				       	isConfirm : function(){
				      		return true;
				       	},
				    	confirmMsg : function(){
				       		return data;
				       	}
				     }
				}),modalInstance.result.then(function(obj) {
					$scope.killExistingSession();
					}, function(obj) {
				});
			}
			
			
			$scope.getUpdatedKey = function (data) {
				$http({
					url:"/getUpdatedKey?test=test",							
					method:'GET'
					}).success(function(datausername, status, headers, config){
						$scope.signIn(data);
					}).error(function(data, status, headers, config) {
						
					});
			}
			
			$scope.killExistingSession = function () {
				$http({
					method : 'POST',
			        url : "/killActiveSessionByEmailId",
			        data: $scope.emailAndPassword
				}).success(function(data, status, headers, config) {
					$scope.submitFormForLogin(1);					
				}).error(function(data, status, headers, config) {
					
				});
			}
			
		    $scope.submitFormForLogin = function(appId) {
		    	
		    	$scope.setcsrfobj = csrfservice.getUserObject();
		    	$scope.setcsrfobj.emailId = $scope.emailId;
		    	$scope.setcsrfobj.password = $scope.password;
		    	$scope.setcsrfobj.appId = appId+"";
		    	$scope.emailAndPassword = {"emailId" : $scope.emailId, "password" : $scope.password, "appId" : appId+""};
		    	
		    	
		    	$http({
			        method : 'POST',
			        url : "/signin",
			        data: $scope.emailAndPassword 
					}).success(function(data, status, headers, config) {
						if(data.RESULT != "ALREADYSESSION"){
							$scope.getUpdatedKey(data);
						}else{
							$scope.sessionAlreadyExist(1, "Another session active. Do you want to continue? This will end the other active session.");
						}
					}).error(function(data, status, headers, config) {
						logger.logError(status);
			        });
			};

			$scope.signIn = function (data) {
				UserService.setUser(data)
				if(data.RESULT=="Success"){
					$http.post("/getUserNameFromId",data.userId).success(function(data, status, headers, config){
						localStorage.setItem("userName" , data);
					});
					
					$http.post("/getGlobalData").success(function(data, status, headers, config){
						localStorage.setItem("GLOBAL_DATA" , JSON.stringify(data));
					});
					
					localStorage.setItem("EmailId", $scope.emailId);
					var location= $scope.getHomeForLand();
					$scope.getResetTheSizeOfLeft();
					$location.path(location.substring(1, location.length + 1));
					$rootScope.$broadcast('notificationStartEvent');
					$http({
						url:"/getPasswordExpiryDays",
						data : $scope.emailId,
						method : "POST"
					}).success(function(data, status, headers, config){
						//$scope.changeNavSize();
						if(data <= 7 && data >= 0){
							var modalInstance;
							modalInstance = $modal.open({
								size : "md",
								templateUrl: "/views/template/passwordExpiryDaysWarningBox.html",
								controller: "passwordExpiryDaysWarningController",
								resolve:{
									expiryDays : function(){
										return data;
									}
					            }
							}),modalInstance.result.then(function(obj) {
					    			
							}, function(result) {
								$log.info("Modal dismissed at: " + new Date);
							});
						}
					}).error(function(data){
						logger.logError("Not able to get user details: "+data);
					});
	    			 if (undefined != data && null != data) {
						$scope.getFlavour(data.userId);
						$scope.getMenu(data.userId);
						FlavourService.getFlavourPersonaObjectRequest().then(function(data){
	                  	    FlavourService.setPersonaObject(data);
	                  	    UserService.setMaxPersonaName(data);
	                    });
                  	    $scope.value = UserService.getMaxPersonaName();
                  		$scope.showDataAccess($scope.value);
						$rootScope.profileMenuInterval();
	    			 }
				}else{
					if(data.RESULT === "Your Password got expired please change your password"){
						GetAccessData.setSessionExpire(true);
						GetAccessData.setSessionErrorMsg("Your Password is expired. Please reset password to continue.");
						localStorage.setItem("emailId" , $scope.emailId);
						
						$location.path("/resetPassword");
					}else if(data.RESULT == "ResetPassword"){
						GetAccessData.setSessionExpire(true);
						GetAccessData.setSessionErrorMsg("Please reset your default password.");
						localStorage.setItem("emailId" , $scope.emailId);
						$location.path("/resetPassword");
					}else if(data.RESULT == "SessionAlreadyExist"){
						$scope.sessionAlreadyExist(1, "Another session active. Do you want to continue? This will end the other active session.");
					} else {
						$scope.error=data.RESULT;
					}
				}
			}; 
			
			$scope.forgetPassword = function(){
				var modalInstance;
				modalInstance = $modal.open({
					size : "md",
					templateUrl: "/views/template/forgotpassword.html",
					controller: "forgotpasswordController"
				}),modalInstance.result.then(function(obj) {
					$modalInstance.dismiss();
				}, function(result) {
					$log.info("Modal dismissed at: " + new Date);
				});
			};
			
			//This will get called at the time of login only.
			$scope.getFlavour = function (userId) {
				$http({
					url: "/getActions",
					method: "POST"
				}).success(function(data){
					$scope.updateFlavour(data);
				});				
			}
			$scope.getMenu = function(userId) {
				$http({
					url: "/getBlockedUrlList",
					method: "POST"
				}).success(function(data){
					myService.loggedInComplete(data);
				});
			}
		}]).controller("resetPasswordController",["$scope","$log","$http","logger","$routeParams","$location", "GetAccessData",function($scope,$log,$http,logger,$routeParams,$location, GetAccessData){
			$scope.emailId = localStorage.getItem("emailId");
			$scope.obj = {
					oldPassword : "",
					newPassword : "",
					confirmPassword : ""
			};
			/*/^[a-zA-Z]\w{8,16}$/*/
			$scope.respattern = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$()%^&*-]).{8,16}$/;
			$scope.comparePass = function(repass) {
				$scope.isconfirm = ($scope.obj.newPassword == repass && $scope.obj.newPassword !== undefined && $scope.obj.newPassword !== null && $scope.obj.newPassword !== "") ? true : false;
		    };
		    
			$scope.resetPassword = function(objectVal){
				$http({
					method: "POST",
					url: "/resetPasswordUser?emailId="+$scope.emailId,
					data : objectVal
				}).success(function(data){
					localStorage.removeItem("emailId");
					GetAccessData.setResetPassWord(true);
					GetAccessData.setSessionExpire(true);
					GetAccessData.setSessionErrorMsg("Password changed successfully. Please re-login to continue.");
				}).error(function(data){
					logger.logError(data);
				});
			};
		}]).controller("passwordExpiryDaysWarningController",["$scope","$log","$http","logger","$routeParams","$location","$modalInstance","expiryDays","$cookieStore",function($scope,$log,$http,logger,$routeParams,$location,$modalInstance,expiryDays,$cookieStore){
			$scope.days = expiryDays;
			
			$scope.resetUserPassword = function(){
				$scope.emailId = $cookieStore.get('emailCookie');
				localStorage.setItem("emailId" , $scope.emailId);
				$location.path("/resetPassword");
				$modalInstance.dismiss();
			};
			
			$scope.cancel = function(){
				$modalInstance.dismiss();
			};
		}]).controller("forgotpasswordController",["$scope","$log","$http","logger","$routeParams","$location","$modalInstance","$cookieStore",function($scope,$log,$http,logger,$routeParams,$location,$modalInstance,$cookieStore){
			$scope.cancel = function(){
				$modalInstance.dismiss();
			};
		}]);
}).call(this);
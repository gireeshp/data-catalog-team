(function() {
	"use strict";

		var module = angular.module("app.signup" , []);
		module.controller("signupController" , ["$scope","$http","$location","logger",function($scope, $http, $location,logger){
		
		$scope.passwordPolicy = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,16}$/;
		$scope.checkPassword = function (confirmPwd) {
			$scope.isconfirm = $scope.firstPassword == confirmPwd ? true : false;
		    //$scope.signupForm.secondPassword.$error.dontMatch = $scope.firstPassword !== $scope.secondPassword;
		};
		
		$scope.checkUserExists= function(emailId){
			$scope.errorEmail="";
			if(!_.isUndefined(emailId)){console.log(emailId);
			$http({
				method: 'POST',
				url : "/getUser?email="+emailId
			}).
			success(function(data, status, headers, config){
				$scope.errorEmail=	data;
				
			}).error(function(data){
				//logger.logError(data);
			});
			
			}
		};
		
		 $scope.submitFormForSignup = function() {
			 if(!$scope.signupForm.$invalid){
				 var _serverObj = {
							emailId : $scope.emailId,
							password : $scope.firstPassword,
							firstName : $scope.firstName,
							lastName : $scope.lastName,
							userRangerId : '0'
						};
					
					$http.post("/signup",_serverObj).success(function(data){
						//$scope.error= data;
						$location.path("/signin");
						logger.logSuccess("Account created successfuly");
						/*$http({
							method: 'POST',
							url : "/getUserDetails?email="+_serverObj.emailId
						}).success(function(data, status, headers, config){
							$scope.newUser= data.newCreated;
							$scope.emailId = data.emailId;
							if($scope.newUser == false){
								localStorage.setItem("emailId" , $scope.emailId);
								$location.path("/resetPassword");
							}else{
								return;
							}
						}).error(function(data){
							logger.logError(data);
						});*/
					}).error(function(data,status){
						 logger.logError(data);

					});
			 }else{
				 logger.logError("All fields are mandatory");
			 }
			};
		}]);
}).call(this);
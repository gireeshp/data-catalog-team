(function(){
	"use strict";
	
	var module = angular.module("app.detailCookies",["ngCookies"]);
		
	module.controller("UseCookieCtrl" , ["$routeParams", "$scope", "$http","$cookieStore", "$location","$cookies", "UserService", "$rootScope" ,function($routeParams, $scope,$http, $cookieStore, $location, $cookies, UserService, $rootScope){
		$scope.searchGroup = "";
		if($cookieStore.get('userId') !== undefined && $cookieStore.get('userId') !== null && 
				$location.path() !="/signin" && $location.path() != "/home" && $location.path() != "/contact" 
					&& $location.path() != "/requestDemo" && $location.path() != "/productoverview"){
			$http.post("/getUserNameFromId",$cookieStore.get('userId')).success(function(data, status, headers, config){
				localStorage.setItem("userName" , data);
			});

			$http.post("/getGlobalData").success(function(data, status, headers, config){
				localStorage.setItem("GLOBAL_DATA" , JSON.stringify(data));
			});
		}
		
		$scope.userGuideLink = "";
		$scope.whatsNewReleaseNotes = "";
		
		$scope.openUrl = function(condition){
			if(localStorage.getItem("GLOBAL_DATA")!=undefined && localStorage.getItem("GLOBAL_DATA")!=null){
				if(condition == 'USER_GUIDE'){
					window.open(JSON.parse(localStorage.getItem("GLOBAL_DATA")).USER_GUIDE_LINK, '_blank')
				}else if(condition == 'WHAT_NEW'){
					window.open(JSON.parse(localStorage.getItem("GLOBAL_DATA")).WHATS_NEW_RELEASE_NOTES, '_blank')
				}
			}

		}
		
		$scope.checkCookie=function(){
			$scope.loginUserId = localStorage.getItem("userName");
			
			if($location.path() !="/signin" && $location.path() != "/home" && $location.path() != "/contact" 
				&& $location.path() != "/requestDemo" && $location.path() != "/productoverview"){
			if(localStorage.getItem("GLOBAL_DATA")!=undefined && localStorage.getItem("GLOBAL_DATA")!=null){
				$scope.userGuideLink = JSON.parse(localStorage.getItem("GLOBAL_DATA")).USER_GUIDE_LINK;
				$scope.whatsNewReleaseNotes = JSON.parse(localStorage.getItem("GLOBAL_DATA")).WHATS_NEW_RELEASE_NOTES;
			}
			}
		};	
	
		$scope.profile =function(){
			$location.path("/userProfile");
		};  
		$scope.updateGroupName=function(){
			_.filter($scope.user.GROUP_DETAILS, function(group){
				if(group.isSelected == true){
					console.log(group.groupId+" "+group.isSelected);
					if(group.groupId!=$cookieStore.get('groupId')){
						$scope.loginAs($cookieStore.get('groupId'));
						return;
					}
				}
				return;
			})
			
		}
		$scope.groupChange = function(value){
			console.log(value);
		};
		
		$scope.saveLastUpdatedGroup = function(groupId){
			$http({
				method: "POST",
				url: "/updateGroupNameInApplicationUser",
				data : groupId
			}).success(function(data){
			}).error(function(data){
				logger.logError(data);
			});
		};
		
		$scope.loginAs=function(groupId){
    		$cookieStore.put("groupId",parseInt(groupId));
    		$scope.user.GROUP_DETAILS=UserService.getUpdatedGroupMap(groupId, $scope.user.GROUP_DETAILS);
    		$cookieStore.put("groupMap",$scope.user.GROUP_DETAILS);
    		$scope.saveLastUpdatedGroup(groupId);
    		$rootScope.projectFetchIndicator = true;
    		var location= $scope.getHomeForLand();
			$location.path(location.substring(1, location.length + 1));
    	}
	
		$scope.$watch(function() { return $cookies.emailCookie; }, function(newValue) {
			if(undefined == $cookieStore.get('emailCookie') && $location.path() !="/signin" &&
						$location.path() != "/home" && $location.path() != "/contact" 
							&& $location.path() != "/requestDemo" && $location.path() != "/productoverview"){
	    	   $location.path("/signin");
			}
		});
	 
	}]);
}).call(this);
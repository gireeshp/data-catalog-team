(function(){
	"use strict";
	var app = angular.module('app.userManagement',[]);
	app.service("ManageUserService",["$q","$http","logger",function($q,$http,logger){
		
		return{	
			
			getListFromObject:function(object, element){
				var list=[];
				angular.forEach(object, function(obj){
					list.push(obj[element]);
				})
				return list;
			},
			removeWhiteSpacesFromList:function(inputList){
				var trimedObject=[];
				angular.forEach(inputList,function(obj){
					trimedObject.push(obj.trim());
				});
				return trimedObject;
			},
			
		    getIdListFromParamAndObject:function(selectedFieldList, AvailableObject, param, idParam){
				var idList=[];
				angular.forEach(AvailableObject,function(obj){
					for (var i = 0; i < selectedFieldList.length; i++) {
						if(obj[param]==selectedFieldList[i]){
							idList.push(obj[idParam]);
						}	
					}
				});
				return idList;
			},
			
			filterSelectedGroups : function (selectedAppList,groupObject,allApps,selectedGroups){
				var list=[];
				angular.forEach(groupObject, function(obj){
					for (var i = 0; i < selectedAppList.length; i++) {
						var findAppId = getAppId(selectedAppList[i],allApps);
						if(obj["appId"] == findAppId){
							if(_.contains(selectedGroups,obj["groupName"])){
								list.push(obj["groupName"]);
							}
						}
					}
				})
				return list;
			},

			filterRoles : function (selectedAppList,roleObject,allApps){
				var list=[];
				angular.forEach(roleObject, function(obj){
					for (var i = 0; i < selectedAppList.length; i++) {
						var findAppId = getAppId(selectedAppList[i],allApps);
						if(obj["appId"] == findAppId){
							list.push(obj["roleName"]);
						}
					}
				})
				return list;
			},
			
			filterGroups : function (selectedAppList,groupObject,allApps){
				var list=[];
				angular.forEach(groupObject, function(obj){
					for (var i = 0; i < selectedAppList.length; i++) {
						var findAppId = getAppId(selectedAppList[i],allApps);
						if(obj["appId"] == findAppId){
							list.push(obj["groupName"]);
						}
					}
				})
				return list;
			},
			
			filterSubgroups:function(selectedGroupList, subgroupObject){
				var list=[];
				angular.forEach(subgroupObject, function(obj){
					for (var i = 0; i < selectedGroupList.length; i++) {
						if(obj["groupName"]==selectedGroupList[i]){
							list.push(obj["subGroupName"]);
						}
					}
				})
				return list;
			},
			
			filterSelectedSubgroups:function(selectedSubgroupsList, selectedGroupList){
				var list=[];
				angular.forEach(selectedSubgroupsList, function(subgroupObject){
					var group=subgroupObject.split("_")[1];
					for (var i = 0; i < selectedGroupList.length; i++) {
						if(group==selectedGroupList[i]){
							list.push(subgroupObject);
						}
					}
				})
				return list;
			},
			
			filterRemainingSubgroups:function(availableSubGroupList, selectedSubGroupList){
				var list = availableSubGroupList;
				angular.forEach(selectedSubGroupList, function(subgroupObject){
					var group=subgroupObject.split("_")[1];
					list = _.reject(list,function(subgroup){
						return subgroup.split("_")[1] == group;
					});
				})
				return list;
			},
			

			checkForSubgroupsPresent:function(groups,subgroups){
				var list = groups;
				var flag = false;
				angular.forEach(subgroups, function(subgroupObject){
					var groupOfSubgroup=subgroupObject.split("_")[1];
					list = _.reject(list,function(group){
						return group == groupOfSubgroup;
					});
				})
				if(list.length > 0)
					return list;
				return flag;
			},
			
			getAppName:function(appId, allApps){
				for(var i = parseInt(0); i < allApps.length ; i++) {
		    		if(allApps[i].appId.trim() == appId.trim()){
		    			return allApps[i].appName;
		    		}
		    	}
		    },
		    
		    getAppId:function(appName, allApps) {
		    	for(var i = parseInt(0); i < allApps.length ; i++) {
		    		if(allApps[i].appName === appName){
		    			return allApps[i].appId;
		    		}
		    	}
		    },
		    
		    getSolutionId:function (solutionName, allSolutions) {
				for(var i = parseInt(0); i < allSolutions.length ; i++) {
		    		if(allSolutions[i].SOLUTIONNAME === solutionName){
		    			return i;
		    		}
		    	}
		    },
			
			getWithoutObject:function(url){
				var deferred=$q.defer();
				$http.get(url).success(function(data){
					deferred.resolve(data);
				}).error(function(data){
					logger.logError(data);
					deferred.reject(data);
				});
				return deferred.promise;
			},
			
			postWithObject:function(url,object){
				var deferred=$q.defer();
				$http.post(url,object).success(function(data){
					deferred.resolve(data);
				}).error(function(data){
					logger.logError(data);
					deferred.reject(data);
				});
				return deferred.promise;
			},
			
			validateUserDetails:function(user){
				
				var flag = false; 
				if(user === undefined){
					flag = true;
					logger.logError("All Fields are mandatory Please fill all the details and select appropriate Solutions");
				}
				if(user.firstName === undefined || user.firstName == ""){
					flag = true;
					logger.logError("Please insert first name of user");
				}
				if(user.lastName === undefined || user.lastName == ""){
					flag = true;
					logger.logError("Please insert last name of user");
				}
				if(user.status === undefined || user.status == ""){
					flag = true;
					logger.logError("Please select status of user");
				}
				if(user.emailId === undefined || user.emailId == "" ){
					flag = true;
					logger.logError("Please insert emailId of user");
					
				}else if(user.emailId.length > 40){
					flag = true;
					logger.logError("Email id length exceeds the upper limit 40");	
					
 				}else{
					var x = user.emailId;
				    var atpos = x.indexOf("@");
				    var dotpos = x.lastIndexOf(".");
				    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
				    	flag = true;
				        logger.logError("Not a valid e-mail address");
				    }
				}
				if(user.password === undefined || user.password == ""){
					flag = true;
					logger.logError("Password should be 8 to 16 characters long. Must have atleast one capital letter. Must have atleast one digit. Must have atleast one special character");
				}
				return flag;
			}
		}
		
	}]);
	

	app.controller("userManagementController" , ["$filter","$route","$scope", "$modal" , "$log", "logger","ManageUserService",  function($filter,$route,$scope,$modal,$log,logger,ManageUserService){
		
		ManageUserService.getWithoutObject('/userManagement/getUserDetails').then(function(data){
			
			$scope.availableUsers=data;
		    $scope.stores = $scope.availableUsers	
	        , $scope.searchKeywords = "", 
	        $scope.filteredStores = [],
	        $scope.row = "", 
	        $scope.select = function(page) {
		    	$scope.pageNum = page;
				var end, start;
	            return start = (page - 1) * $scope.numPerPage, end = start + $scope.numPerPage, $scope.currentPageStores = $scope.filteredStores.slice(start, end);
	        }, 
	        $scope.onFilterChange = function() {
	            return $scope.select(1), $scope.currentPage = 1, $scope.row = "";
	        }, $scope.onNumPerPageChange = function(val) {
	        	$scope.numPerPage = val;
	            return $scope.select(1), $scope.currentPage = 1;
	        }, $scope.onOrderChange = function() {
	            return $scope.select(1), $scope.currentPage = 1;
	        }, $scope.search = function() {
	        	return $scope.filteredStores = $filter("filter")($scope.stores, $scope.searchKeywords), $scope.onFilterChange();
	        }, $scope.order = function(rowName) {
	        	return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.filteredStores, rowName), $scope.onOrderChange()) : void 0;
	        }, $scope.numPerPageOpt = [3, 5, 10, 15, 20],
	        $scope.numPerPage = $scope.numPerPageOpt[4], 
	        $scope.currentPage = 1, 
	        $scope.currentPageStores = [], ($scope.init = function() {
                return $scope.search(), $scope.select($scope.currentPage), $scope.order("-unqUserId");
            })();
			
		});
		
		
		$scope.openConfigureUserPopUp=function(){
			var modalInstance;
			modalInstance = $modal.open({
	            templateUrl: "views/template/configureNewUserModal.html",
	            controller: "configureNewUserModalController",
	            size:'lg',
	            resolve:{
			       "user":function(){
				         return "";
				    }
			    }
	     }),modalInstance.result.then(function(obj) {
	    	 $route.reload();
	    	 $log.info("Modal closed at: " + new Date);
	     }, function(obj) {
             $log.info("Modal dismissed at: " + new Date);
         });
		}
		
		
		$scope.editUser=function(user){
			var modalInstance;
			modalInstance = $modal.open({
	            templateUrl: "views/template/configureNewUserModal.html",
	            controller: "configureNewUserModalController",
	            size:'lg',
	            resolve:{
			       "user":function(){
				         return user;
				    }
			    }
	     }),modalInstance.result.then(function(obj) {
	    	 $route.reload();
	    	 $log.info("Modal closed at: " + new Date);
	     }, function(obj) {
             $log.info("Modal dismissed at: " + new Date);
         });
		}
		
		
	
		$scope.deleteUser=function(userId, firstName, lastName){
			var modalInstance;
			modalInstance = $modal.open({
				templateUrl: "/views/template/AbortAndDeleteModal.html",
				controller: "deleteAppDSCtrl",
				resolve:{
					deleteObject: function(){
						return {name:firstName+" "+lastName, type: "user"};
					}
				}
			}),modalInstance.result.then(function() {
				ManageUserService.postWithObject('/userManagement/deleteUser',userId).then(function(data){
					logger.logSuccess("User has been deleted successfully");
					$route.reload();
				});
			}, function() {
				$log.info("Modal dismissed at: " + new Date);
			});
		}
		
		
		
	}]).controller("configureNewUserModalController" , [ "$filter","$scope", "$modalInstance", "$http" , "$location","$routeParams", "$route", "logger","ManageUserService", "user",function($filter,$scope, $modalInstance, $http , $location,$routeParams,$route, logger, ManageUserService, user){
		
			$http({
		        method : 'POST',
		        url : "/userManagement/fetchAllPersonaDetails"
			 }).success(function(data, status, headers, config) {
					$scope.personaInfo = data;
			 }).error(function(data, status, headers, config) {
					logger.logError(status);
			 });
		
	    $scope.isUser=0;
	    $scope.isPassWordEnable=true;
	    var previousUserObject="";
	    $scope.respattern = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,16}$/;
	    $scope.checkPwd = function(pwd) {
			$scope.isconfirm = $scope.respattern.test(pwd) ? true : false;
	    };
	    
	    $http({
	        method : 'POST',
	        url : "/accessConfig/fetchAllApps"
			}).success(function(data, status, headers, config) {
				$scope.allApps=data;
			}).error(function(data, status, headers, config) {
			logger.logError(status);
	        
		});
	    
	    $scope.changeIndicator = function(){
	    	$scope.isPassWordEnable = false;
	    	$scope.manageUser.userDetails.password = "";
	    };
	    
	    $scope.statusList=["ACTIVE", "INACTIVE"];
	    $scope.lockStatusList = ["LOCK", "UNLOCK"];
	    $scope.masterRoleObj = {"selectedPeople" : []};
	    $scope.groupList = {};
	    $scope.subGroupList = {};
	    $scope.roleList = {};
	    
		ManageUserService.getWithoutObject('/userManagement/fetchGroupSubgroupRoles').then(function(data){
				$scope.manageUsersForDetails=data;
			    $scope.fetchedGroupSubgroupRoles=data;
				$scope.selectedGroups = {};
				$scope.selectedSubGroups = {};
				$scope.selectedRoles = {};
				_.each($scope.manageUsersForDetails , function(user){
					index = ManageUserService.getSolutionId(user.SOLUTIONNAME,$scope.manageUsersForDetails);
					var list = _.pluck(user.GROUP, "groupName");
					$scope.groupList[user.SOLUTIONNAME] = list;
					
					var subList = _.pluck(user.SUBGROUP, "subGroupName");
					$scope.subGroupList[user.SOLUTIONNAME] = subList;
					
					var rolesList = _.pluck(user.ROLES, "roleName");
					$scope.roleList[user.SOLUTIONNAME] = rolesList;
					
				});
				if(user){
					$scope.callMethodAfterAllDone();
				}
		});
		
		
		$scope.manageUser = {applicationDetails : {}};		
		$scope.addToSelectedApp = function(SOLUTIONNAME, value){
			if($scope.manageUser.applicationDetails["selectedApps"] == undefined || $scope.manageUser.applicationDetails["selectedApps"] == null){
			
				if(value)
					$scope.manageUser.applicationDetails["selectedApps"] = [];
			}
			
			if(value){
				$scope.manageUser.applicationDetails["selectedApps"].push(SOLUTIONNAME);
				$scope.selectedGroups[SOLUTIONNAME] = [];
				$scope.selectedSubGroups[SOLUTIONNAME]=[];
				$scope.selectedRoles[SOLUTIONNAME]=[];
			}else {
				$scope.manageUser.applicationDetails["selectedApps"] = _.reject($scope.manageUser.applicationDetails["selectedApps"], function(app){
					return app === SOLUTIONNAME;
				});
				$scope.selectedGroups[SOLUTIONNAME] = undefined;
				$scope.selectedSubGroups[SOLUTIONNAME]= undefined;
				$scope.selectedRoles[SOLUTIONNAME]=undefined;
			}
				
		};
		
		// manage group added here 
		$scope.manageAvailableGroups = function (solutionName, index) {
			if($scope.manageUser.applicationDetails.selectedApps.length==0) {
				$scope.selectedGroups[solutionName]=[];
				$scope.groupList[solutionName]=ManageUserService.getListFromObject($scope.manageUsersForDetails[index].GROUP,"groupName");
			} else {
				$scope.groupList[solutionName]=ManageUserService.filterGroups($scope.manageUser.applicationDetails.selectedApps,  $scope.manageUsersForDetails[index].GROUP,$scope.allApps);
			}
			$scope.manageSelectedGroups(solutionName, index);
			$scope.manageAvailableSubGroups(solutionName, index);
		}
		
		
		$scope.manageSelectedGroups=function(solutionName, index) {
			if($scope.selectedGroups[solutionName] && $scope.manageUser.applicationDetails.selectedApps.length>0) {
			   $scope.selectedGroups[solutionName]=ManageUserService.filterSelectedGroups($scope.manageUser.applicationDetails.selectedApps, $scope.manageUsersForDetails[index].GROUP, $scope.allApps,$scope.selectedGroups[solutionName]);
			}
			$scope.manageSelectedSubGroups(solutionName, index);
		}
		
		$scope.manageSelectedSubGroups=function(solutionName, index) {
			if($scope.selectedSubGroups[solutionName] && $scope.selectedGroups[solutionName].length > 0){
			   $scope.selectedSubGroups[solutionName]=ManageUserService.filterSelectedSubgroups($scope.selectedSubGroups[solutionName],$scope.selectedGroups[solutionName]);
			}
		}
		
		$scope.manageAvailableSubGroupsForThatGroup=function(solutionName, index){
			$scope.subGroupList[solutionName]=ManageUserService.filterSubgroups($scope.selectedGroups[solutionName],  $scope.manageUsersForDetails[index].SUBGROUP);
			$scope.subGroupList[solutionName]=ManageUserService.filterRemainingSubgroups(angular.copy($scope.subGroupList[solutionName]),$scope.selectedSubGroups[solutionName]);
		}
		
		$scope.manageAvailableSubGroups=function(solutionName, index) {
			if($scope.selectedGroups[solutionName].length==0){
				
				$scope.selectedSubGroups[solutionName]=[];
				$scope.subGroupList[solutionName]=ManageUserService.getListFromObject($scope.manageUsersForDetails[index].SUBGROUP,"subGroupName");
			}else{
				$scope.subGroupList[solutionName]=ManageUserService.filterSubgroups($scope.selectedGroups[solutionName],  $scope.manageUsersForDetails[index].SUBGROUP);
			}
			$scope.manageSelectedSubGroups(solutionName, index);
			$scope.manageAvailableSubGroupsForThatGroup(solutionName, index);
		}
		
		//manage available roles added here
		$scope.manageAvailableRoles = function (solutionName, index){
			if($scope.manageUser.applicationDetails.selectedApps.length==0) {
				$scope.selectedRoles[solutionName]=[];
				$scope.roleList[solutionName]=ManageUserService.getListFromObject($scope.manageUsersForDetails[index].ROLES,"roleName");
			}else{
				$scope.manageUser.roleDetails.availableRoles=ManageUserService.filterRoles($scope.manageUser.applicationDetails.selectedApps,  $scope.fetchedGroupSubgroupRoles.ROLES,$scope.allApps);
			}
		}
		
		$scope.createSolutionList = function(){
			$scope.globalError = false;
			var indicator = false;
			var roleindicator = false;
			var solutionList = [];
			var key = _.keys($scope.selectedGroups);
			var selectedAppIs;
			
			_.each(key, function(get){
				var groups = $scope.selectedGroups[get];
				var subgroups = $scope.selectedSubGroups[get];
				var roles = $scope.selectedRoles[get];
				for(var i = 0; i < $scope.manageUsersForDetails.length; i++ ){
					if($scope.manageUsersForDetails[i].SOLUTIONNAME === get){
						selectedAppIs = $scope.manageUsersForDetails[i];
						break;
					};
				};
				if(roles != undefined && roles.length == 0){
					if(!roleindicator){
						roleindicator = true;
					}
				}
				if(groups != undefined && groups.length > 0){
					var selectedSolutionId = ManageUserService.getAppId(get,$scope.allApps);
					var solution = {appId : selectedSolutionId};
					var groupIds = [];
					var subGroupIds = [];
					var roleIds = [];
					_.each(groups, function(group){
						var selectedGroup = _.filter(selectedAppIs.GROUP, function(name){
							return name.groupName == group; 
						});
						
						groupIds.push(selectedGroup[0].groupId);
					});
					
					_.each(subgroups, function(subgroup){
						var selectedSubGroups = _.filter(selectedAppIs.SUBGROUP, function(name){
							return name.subGroupName == subgroup; 
						});
						
						subGroupIds.push(selectedSubGroups[0].subGroupId);
					});
					if(!indicator){
						var remainingGroup = ManageUserService.checkForSubgroupsPresent(angular.copy(groups),angular.copy(subgroups));
						if(remainingGroup != false){
							$scope.globalError = true;
							logger.logError("Select SubGroups for "+ remainingGroup +" in "+get);
							indicator = true;
						}
							
					}
					
					_.each(roles, function(role){
						var selectedRoles = _.filter(selectedAppIs.ROLES, function(name){
							return name.roleName == role; 
						});
						
						roleIds.push(selectedRoles[0].unqRoleMasterId);
					});
					
					solution["selectedGroupsIds"] = groupIds;
					solution["selectedSubgroupsIds"] = subGroupIds;
					solution["selectedRolesIds"] = roleIds;
					
					solutionList.push(solution);
				}else if(groups != undefined){
					$scope.globalError = true;
					logger.logError("Select Groups for "+get);
					indicator = true;
				}
				
			});
			if(indicator || roleindicator)
				solutionList = [];
			return solutionList;
		}
		
		if(user){
			
		    $http({
		        method : 'POST',
		        url : "/accessConfig/fetchAllApps"
				}).success(function(data, status, headers, config) {
					$scope.allApps=data;
					if(user.appIdList){
						var list = user.appIdList.split(",");
						var appNames = "";
						angular.forEach(list,function(obj){
							if(appNames == ""){
								appNames +=	ManageUserService.getAppName(obj,$scope.allApps);
							}else {
								appNames +=	"," + ManageUserService.getAppName(obj,$scope.allApps);

							}
						});
						$scope.manageUser.applicationDetails.selectedApps=ManageUserService.removeWhiteSpacesFromList(appNames.split(","));
					}
				}).error(function(data, status, headers, config) {
				logger.logError(status);
		        
			});
		    
			$scope.isUser=1;
			$scope.manageUser.userDetails=angular.copy(user);
			var selectedUserId = user.unqUserId;
			
			$scope.checkbox = [];
			var index = 0;
			
			$scope.callMethodAfterAllDone = function (){
				$http({
			        method : 'POST',
			        url : "/userManagement/fetchSolutionsForUser",
			        data : selectedUserId
					}).success(function(data, status, headers, config) {
						$scope.solutions = data;
						$scope.selectedGroups = {};
						$scope.selectedSubGroups = {};
						$scope.selectedRoles = {};
						_.each($scope.solutions , function(solution){
							index = ManageUserService.getSolutionId(solution.SOLUTIONNAME,$scope.manageUsersForDetails);
							$scope.checkbox[index] = true;
							$scope.addToSelectedApp(solution.SOLUTIONNAME, 1);
							
							var list = _.pluck(solution.GROUP, "groupName");
							$scope.selectedGroups[solution.SOLUTIONNAME] = list;
							
							var subList = _.pluck(solution.SUBGROUP, "subGroupName");
							$scope.selectedSubGroups[solution.SOLUTIONNAME] = subList;
							
							var rolesList = _.pluck(solution.ROLES, "roleName");
							$scope.selectedRoles[solution.SOLUTIONNAME] = rolesList;
							
							$scope.masterRoleObj.selectedPeople = solution.PERSONA_DETAILS;
							
							$scope.manageAvailableSubGroups(solution.SOLUTIONNAME,index);
							
						});
						$scope.backupSelectedGroups = angular.copy($scope.selectedGroups);
						$scope.backupSelectedSubgroups = angular.copy($scope.selectedSubGroups);
						$scope.backupSelectedPeople = angular.copy($scope.masterRoleObj.selectedPeople);
						$scope.backupSelectedRoles = angular.copy($scope.selectedRoles);
						
					}).error(function(data, status, headers, config) {
					logger.logError(status);
			        // called asynchronously if an error occurs
			        // or server returns response with an error status.
				});
			}
			
			$scope.updateUserAction=function(){
				var userDetailsSolutionDetails = [];
				//delete user.$$hashKey;
				if(angular.equals(user,$scope.manageUser.userDetails) && angular.equals($scope.backupSelectedGroups,$scope.selectedGroups) && angular.equals($scope.backupSelectedSubgroups,$scope.selectedSubGroups) && angular.equals($scope.backupSelectedRoles,$scope.selectedRoles) && angular.equals($scope.backupSelectedPeople, $scope.masterRoleObj.selectedPeople)){
					logger.logError("No changes made");
					
				}else{
					var solutionList = $scope.createSolutionList();
					if(ManageUserService.validateUserDetails($scope.manageUser.userDetails) == true){
						//error in user details
					}else if(solutionList.length === 0){
						if($scope.globalError){
							//do nothing
						}else{
							logger.logError("All Fields are mandatory Please Select Solutions for user");
						}
						
					}else{
						delete $scope.manageUser.userDetails.solutionNameList;
						userDetailsSolutionDetails = {userDetails : $scope.manageUser.userDetails};
						userDetailsSolutionDetails["solutions"] = solutionList;
						userDetailsSolutionDetails["personaMasters"] = $scope.masterRoleObj.selectedPeople;
						
						$http({
							method : "post",
							url : "/userManagement/updateUser?indicator="+$scope.isPassWordEnable,
							data : userDetailsSolutionDetails
						}).success(function(data){
							logger.logSuccess("Saved successfully");
							$modalInstance.close("success");
						}).error(function(status){
							logger.logError(status);
						});
					}
					
					
				}
	
			}
		}else{
			
			$scope.createUserAction=function(){
				var solutionList = $scope.createSolutionList();
				if(ManageUserService.validateUserDetails($scope.manageUser.userDetails) == true){
					//error in user details
				}else if(solutionList.length === 0){
					if($scope.globalError){
						//do nothing
					}else{
						logger.logError("All Fields are mandatory Please Select Solutions for user");
					}
				}else{
					var userDetailsSolutionDetails = [];
					userDetailsSolutionDetails = {userDetails : $scope.manageUser.userDetails};
					userDetailsSolutionDetails["solutions"] = solutionList;
					userDetailsSolutionDetails["personaMasters"] = $scope.masterRoleObj.selectedPeople;
					
					$http({
						method : "post",
						url : "/userManagement/createUserAction",
						data : userDetailsSolutionDetails
					}).success(function(data){
						logger.logSuccess("Saved successfully");
						$modalInstance.close("success");
					}).error(function(status){
						logger.logError(status);
					});
				}
				
			}
			
		}
		
		$scope.cancel = function() {
	        $modalInstance.dismiss("cancel");
	    } 
		
   }])
		
}).call(this);



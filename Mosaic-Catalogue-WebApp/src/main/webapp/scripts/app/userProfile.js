(function(){
	"use strict";
	
	var userProfile = angular.module("app.userProfile", []);
	
	userProfile.controller("userProfileCtrl",["$scope" , "$http", "logger", "$cookieStore","$location", function($scope , $http, logger, $cookieStore,$location){
		
		if(undefined != $cookieStore.get('userId')){
			  $scope.userId = $cookieStore.get('userId');
	  	}
		if(undefined != $cookieStore.get('emailCookie')){
			  $scope.email = $cookieStore.get('emailCookie');
	  	}
		if(undefined != $cookieStore.get('groupId')){
			  $scope.groupId = $cookieStore.get('groupId');
	  	}
		
		$scope.resetPasswordForUser = function(){
			$scope.emailId = $cookieStore.get('emailCookie');
			localStorage.setItem("emailId" , $scope.emailId);
			$location.path("/resetPassword");
		};
		var reqDataObject = {'email': $scope.email, 'groupId' : $scope.groupId};
		$http({
			url:"/getUserInfo",
			method : "POST",
			data : reqDataObject
		}).success(function(data, status, headers, config){
			$scope.applicationUser = data;
			var currentTimeMillis = new Date().getTime();
			
			var elapsedTime  = currentTimeMillis - $scope.applicationUser.passwordRefreshedDate;
			
			var passwordexpirydays = $scope.applicationUser.passwordexpirydays; 
			
			var elapsedDays   = elapsedTime / (1000 * 60 * 60 * 24);
			
			$scope.pwdExpiryDay = (passwordexpirydays - elapsedDays).toFixed(0); 
			
			$scope.pwdExpiryHr = (((passwordexpirydays - elapsedDays)-$scope.pwdExpiryDay)*60).toFixed(0);
			
			$scope.loginUserId = localStorage.getItem("userName");
			
		}).error(function(data, status, request){
			logger.logError("Not able to get user details: "+data);
		});
	}]);
}).call(this);


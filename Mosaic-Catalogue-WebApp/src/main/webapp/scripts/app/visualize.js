(function(){
	"user strict";
	var module = angular.module("app.visualize" , ['app.dataSourceTable','angularFileUpload','checklist-model','app.dataSourceTable','FBAngular']);
	
	module.controller("VisCtrl" , ["$sce","$route", "$scope", "$modal" , "$log"  , "$location" , "$routeParams", "$http" , "$rootScope", "logger", "$loading", function($sce, $route,$scope, $modal, $log , $location, $routeParams ,$http, $rootScope, logger, $loading){
		
		$http.post("/getVisualizePath").success(function(data, status, headers, config){
			$scope.visualize  = $sce.trustAsResourceUrl(data);;
		});
	
	}]);
}).call(this);
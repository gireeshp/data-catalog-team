(function(){
	
	'use strict';
	
	angular.module("app.D3factory",[])
	.factory("D3factory",["$document", "$q" , "$rootScope",function($document, $q, $rootScope){
		var d = $q.defer();
		 function onScriptLoad() {
		    //$rootScope.$apply(function() { d.resolve(window.d3); });
		    $rootScope.$evalAsync(function() { d.resolve(window.d3); });
		  }
		 
		 var scriptTag = $document[0].createElement('script');
	     scriptTag.type = 'text/javascript';
	     scriptTag.async = false;
         scriptTag.charset = 'UTF-8';
	     scriptTag.src = '/scripts/libs/d3.js';
	     scriptTag.onreadystatechange = function () {
	       if (this.readyState == 'complete') onScriptLoad();
	     }
	     scriptTag.onload = onScriptLoad;

	     var s = $document[0].getElementsByTagName('body')[0];
	     s.appendChild(scriptTag);

	     return {
	       d3: function() { return d.promise; }
	     };
	}]).
	factory("logger", [function() {
        var logIt;
        return toastr.options = {
            closeButton: !0,
            positionClass: "toast-bottom-right",
            timeOut: "3000"
        }, logIt = function(message, type) {
            return toastr[type](message)
        }, {
            log: function(message) {
                logIt(message, "info")
            },
            logWarning: function(message) {
                logIt(message, "warning")
            },
            logSuccess: function(message) {
                logIt(message, "success")
            },
            logError: function(message) {
            	if(message != "" && message != undefined && message != null && message != "null") {
            		logIt(message, "error")
            	}
            }
        }
    }]).directive("dsDag", ["D3factory", "$modal", "$log", "$rootScope", "$window","$routeParams", function(D3factory, $modal, $log, $rootScope, $window, $routeParams) {

        return {
            restrict: "EA",
            scope: {
                data: "="
            },
            replace: true,
            transclude: true,
            link: function(scope, element, attrs) {

                D3factory.d3().then(function(d3) {

                    //WATCHER
                    scope.$watch('data', function(newVals, oldVals) {
                        if (newVals) {
                        	scope.dagData=angular.copy(newVals);
                        	render(scope.dagData);
                        }
                    }, true);

                    
                    //RENDER
                    function render(data){
                    	
                    	var svg=document.getElementById("svg");
                    	if(svg!=null)
                    		svg.outerHTML="";
                    	
                    	var graph=data;
                    
                             var margin = {
                                 top: -5,
                                 right: -5,
                                 bottom: -5,
                                 left: -5
                             };
                             /* var width = 500 - margin.left - margin.right,
         		        			    height = 400- margin.top - margin.bottom;*/
                            /* var width = 1000 - margin.left - margin.right,
                                 height = 500 - margin.top - margin.bottom;*/
                             
                             //var width = 1133 - margin.left - margin.right,
                             //height = 497 - margin.top - margin.bottom;
                        
                             var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
                             width=((width*80/100) + 161.2)- 10;
                             var height = 497 - margin.top - margin.bottom;
                             
                             var color = d3.scale.category20();

                             var force = d3.layout.force()
                                 .charge(-200)
                                 .linkDistance(80)
                                 .size([width + margin.left + margin.right, height + margin.top + margin.bottom]);

                             var zoom = d3.behavior.zoom()
                                 .scaleExtent([1, 10])
                                 .on("zoom", zoomed);

                             var drag = d3.behavior.drag()
                                 .origin(function(d) {
                                     return d;
                                 })
                                 .on("dragstart", dragstarted)
                                 .on("drag", dragged)
                                 .on("dragend", dragended);


                             var svg = d3.select(element[0]).append("svg")
                                 .attr("id","svg")
                                 .attr("width", width + margin.left + margin.right)
                                 .attr("height", height + margin.top + margin.bottom)
                                 .append("g")
                                 .attr("transform", "translate(" + margin.left + "," + margin.right + ")")
                                 .call(zoom);

                             var rect = svg.append("rect")
                                 .attr("width", width)
                                 .attr("height", height)
                                 .style("fill", "none")
                                 .style("pointer-events", "all");

                             var container = svg.append("g");

                             force
                                 .nodes(graph.nodes)
                                 .links(graph.links)
                                 .start();
                             
                             var tooltip = d3.select("div.dag-tooltip");

                             var link = container.append("g")
                                 .attr("class", "links")
                                 .selectAll(".link")
                                 .data(graph.links)
                                 .enter().append("line")
                                 .attr("class", "link")
                                 .style("stroke-width", function(d) {
                                     return Math.sqrt(d.value);
                                 });
                             
                             //node
                             var node = container.append("g")
                                 .attr("class", "nodes")
                                 .selectAll(".node")
                                 .data(graph.nodes)
                                 .enter().append("g")
                                 .attr("class", "node")
                                 .attr("cx", function(d) {
                                     return d.x;
                                 })
                                 .attr("cy", function(d) {
                                     return d.y;
                                 })
                                 .call(drag);

                             node.append("circle")
                                 .attr("r", function(d) {
                                	 var radius=d.weight * 2 + 12;
                                	 d.radius=radius;
                                     return radius;
                                 })
                                 .style("fill", function(d) {
                                     return color(1 / d.rating);
                                 });

                             force.on("tick", function() {
                                 link.attr("x1", function(d) {
                                         return d.source.x;
                                     })
                                     .attr("y1", function(d) {
                                         return d.source.y;
                                     })
                                     .attr("x2", function(d) {
                                         return d.target.x;
                                     })
                                     .attr("y2", function(d) {
                                         return d.target.y;
                                     });

                                 node.attr("transform", function(d) {
                                     return "translate(" + d.x + "," + d.y + ")";
                                 });
                             });

                            
                             node.append("text")
                                 .attr("text-anchor", "middle")
                                 .attr("style", "stroke-width: 0.0px; font-size: 10px;")
                                 .attr("dy", function(d){
                                	return (2* parseInt(d.radius))+2;})
                                 .text(function(d) {
                                     return (d.name.length > 15 ? d.name.substring(0, 15) + '...' : d.name);
                                 });

                             var linkedByIndex = {};
                             graph.links.forEach(function(d) {
                                 linkedByIndex[d.source.index + "," + d.target.index] = 1;
                             });

                             function isConnected(a, b) {
                                 return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index];
                             }


                             //NODE EVENTS 
                             node.on("mouseover", function(d) {

                            	    var testDiv = document.getElementById("dag-tooltip");
                            	    testDiv.innerHTML = "<span><h5 color='black'>"+"&nbsp;"+d.name+"&nbsp;"+"</h5></span>";
                            	    tooltip.style("visibility", "visible");
                                   
                                     node.classed("node-active", function(o) {
                                         var thisOpacity = isConnected(d, o) ? true : false;
                                         this.setAttribute('fill-opacity', thisOpacity);
                                         return thisOpacity;
                                     });

                                     link.classed("link-active", function(o) {
                                         return o.source === d || o.target === d ? true : false;
                                     });

                                     d3.select(this).classed("node-active", true);
                                     d3.select(this).select("circle").transition()
                                         .duration(750)
                                         .attr("r", (d.weight * 2 + 12) * 1.5);
                                     
                                 }).on("mousemove", function () {
                                     tooltip.style("left", function() {
                                         return (d3.event.layerX + 20) + "px";
                                     }).style("top", function(){
                                    	 return (getOffset(d3.event).y - 100) + "px";
                                     })
                                 }).on("mouseout", function(d) {

                                     node.classed("node-active", false);
                                     link.classed("link-active", false);

                                     d3.select(this).select("circle").transition()
                                         .duration(750)
                                         .attr("r", d.weight * 2 + 12);
                                     tooltip.style("visibility", "hidden");
                                     
                                 }).on("dblclick", function(d) {
                                	 var modalInstance;
                                     modalInstance = $modal.open({
                                         templateUrl: "/views/template/relationDetailsPopup.html",
                                         controller: "relPopupCtrl",
                                         size: "xl",
                                         resolve: {
                                             "nodeData": function() {
                                                 return d;
                                             }
                                         }
                                     }), modalInstance.result.then(function(reponame) {
                                     	 render(scope.dagData);
                                     }, function() {
                                     	 render(scope.dagData);
                                         $log.info("Modal dismissed at: " + new Date);
                                     });
                             });
                             
                             //LINK EVENTS
                             link.on("dblclick", function(d) {	 
                                 var modalInstance;
                                 modalInstance = $modal.open({
                                     templateUrl: "/views/template/relationOverviewPopup.html",
                                     controller: "relOverviewCtrl",
                                     //size: "xl",
                                     resolve: {
                                         "relationData": function() {
                                             return d;
                                         }
                                     }
                                 }), modalInstance.result.then(function(reponame) {
                                	 var svg=document.getElementById("svg");
                                 	 if(svg!=null)
                                 		svg.outerHTML="";
                                 	 render(scope.dagData);
                                 }, function() {
                                	 var svg=document.getElementById("svg");
                                 	 if(svg!=null)
                                 		svg.outerHTML="";
                                 	 render(scope.dagData);
                                     $log.info("Modal dismissed at: " + new Date);
                                 });
                             
                             }).on("mouseout", function(d) {
                            	 this.style.strokeOpacity = '';
                            	 //tooltip.style("visibility", "hidden");
                             }).on("mouseover", function(d) {
                            	 this.style.strokeOpacity = '1';
                            	 //var testDiv = document.getElementById("dag-tooltip");
                        	     //testDiv.innerHTML = "<span><h5>"+"&nbsp;"+d.label+"&nbsp;"+"</h5></span>";
                        	     //tooltip.style("visibility", "visible");
                             }).on("mousemove",function(d){
                            	  //tooltip.style("left", (getOffset(d3.event).x) + "px")
                            	 //.style("top", (getOffset(d3.event).y+50) + "px");
                             });
                             
                             
                             function getOffset(e) {
                                 var posx = 0;
                                 var posy = 0;
                                 if (!e) var e = window.event;
                                 if (e.pageX || e.pageY) {
                                     posx = e.pageX;
                                     posy = e.pageY;
                                 } else if (e.clientX || e.clientY) {
                                     posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                                     posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                                 }
                                 return {
                                     x: posx,
                                     y: posy
                                 };
                             }

                             function dottype(d) {
                                 d.x = +d.x;
                                 d.y = +d.y;
                                 return d;
                             }

                             function zoomed() {
                                 container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                             }

                             function dragstarted(d) {
                                 d3.event.sourceEvent.stopPropagation();

                                 d3.select(this).classed("dragging", true);
                                 force.start();
                             }

                             function dragged(d) {

                                 d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);

                             }

                             function dragended(d) {

                                 d3.select(this).classed("dragging", false);
                             }
                    	
                    }
                    
                    
                    render();
                    

                });
            }
        }



    }]).directive("collectionDag", ["D3factory", "$modal", "$log", "$rootScope", "$window","$routeParams", function(D3factory, $modal, $log, $rootScope, $window, $routeParams) {

        return {
            restrict: "EA",
            scope: {
                data: "="
            },
            replace: true,
            transclude: true,
            link: function(scope, element, attrs) {

                D3factory.d3().then(function(d3) {

                    //WATCHER
                    scope.$watch('data', function(newVals, oldVals) {
                        if (newVals) {
                        	scope.dagData=angular.copy(newVals);
                        	render(scope.dagData);
                        }
                    }, true);

                    
                    //RENDER
                    function render(data){
                    	
                    	var svg=document.getElementById("collection-svg");
                    	if(svg!=null)
                    		svg.outerHTML="";
                    	
                    	var graph=data;
                    
                             var margin = {
                                 top: -5,
                                 right: -5,
                                 bottom: -5,
                                 left: -5
                             };
                             /* var width = 500 - margin.left - margin.right,
         		        			    height = 400- margin.top - margin.bottom;*/
                            /* var width = 1000 - margin.left - margin.right,
                                 height = 500 - margin.top - margin.bottom;*/
                             
                             //var width = 1133 - margin.left - margin.right,
                             //height = 497 - margin.top - margin.bottom;
                        
                             var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
                             width=width*90/100;
                             var height = 497 - margin.top - margin.bottom;
                             
                             var color = d3.scale.category20();

                             var force = d3.layout.force()
                                 .charge(-200)
                                 .linkDistance(80)
                                 .size([width + margin.left + margin.right, height + margin.top + margin.bottom]);

                             var zoom = d3.behavior.zoom()
                                 .scaleExtent([1, 10])
                                 .on("zoom", zoomed);

                             var drag = d3.behavior.drag()
                                 .origin(function(d) {
                                     return d;
                                 })
                                 .on("dragstart", dragstarted)
                                 .on("drag", dragged)
                                 .on("dragend", dragended);


                             var svg = d3.select(element[0]).append("svg")
                                 .attr("id","collection-svg")
                                 .attr("width", width + margin.left + margin.right)
                                 .attr("height", height + margin.top + margin.bottom)
                                 .append("g")
                                 .attr("transform", "translate(" + margin.left + "," + margin.right + ")")
                                 .call(zoom);

                             var rect = svg.append("rect")
                                 .attr("width", width)
                                 .attr("height", height)
                                 .style("fill", "none")
                                 .style("pointer-events", "all");

                             var container = svg.append("g");

                             force
                                 .nodes(graph.nodes)
                                 .links(graph.links)
                                 .start();
                             
                             var tooltip = d3.select("div.collection-dag-tooltip");

                             var link = container.append("g")
                                 .attr("class", "links")
                                 .selectAll(".link")
                                 .data(graph.links)
                                 .enter().append("line")
                                 .attr("class", "link")
                                 .style("stroke-width", function(d) {
                                     return Math.sqrt(d.value);
                                 });
                             
                             //node
                             var node = container.append("g")
                                 .attr("class", "nodes")
                                 .selectAll(".node")
                                 .data(graph.nodes)
                                 .enter().append("g")
                                 .attr("class", "node")
                                 .attr("cx", function(d) {
                                     return d.x;
                                 })
                                 .attr("cy", function(d) {
                                     return d.y;
                                 })
                                 .call(drag);

                             node.append("circle")
                                 .attr("r", function(d) {
                                	 var radius=d.weight * 2 + 12;
                                	 d.radius=radius;
                                     return radius;
                                 })
                                 .style("fill", function(d) {
                                     return color(1 / d.rating);
                                 });

                             force.on("tick", function() {
                                 link.attr("x1", function(d) {
                                         return d.source.x;
                                     })
                                     .attr("y1", function(d) {
                                         return d.source.y;
                                     })
                                     .attr("x2", function(d) {
                                         return d.target.x;
                                     })
                                     .attr("y2", function(d) {
                                         return d.target.y;
                                     });

                                 node.attr("transform", function(d) {
                                     return "translate(" + d.x + "," + d.y + ")";
                                 });
                             });

                            
                             node.append("text")
                                 .attr("text-anchor", "middle")
                                 .attr("style", "stroke-width: 0.0px; font-size: 10px;")
                                 .attr("dy", function(d){
                                	return (2* parseInt(d.radius))+2;})
                                 .text(function(d) {
                                     return (d.name.length > 15 ? d.name.substring(0, 15) + '...' : d.name);
                                 });

                             var linkedByIndex = {};
                             graph.links.forEach(function(d) {
                                 linkedByIndex[d.source.index + "," + d.target.index] = 1;
                             });

                             function isConnected(a, b) {
                                 return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index];
                             }


                             //NODE EVENTS 
                             node.on("mouseover", function(d) {

                            	    var testDiv = document.getElementById("collection-dag-tooltip");
                            	    testDiv.innerHTML = "<span><h5 color='black'>"+"&nbsp;"+d.name+"&nbsp;"+"</h5></span>";
                            	    tooltip.style("visibility", "visible");
                                   
                                     node.classed("node-active", function(o) {
                                         var thisOpacity = isConnected(d, o) ? true : false;
                                         this.setAttribute('fill-opacity', thisOpacity);
                                         return thisOpacity;
                                     });

                                     link.classed("link-active", function(o) {
                                         return o.source === d || o.target === d ? true : false;
                                     });

                                     d3.select(this).classed("node-active", true);
                                     d3.select(this).select("circle").transition()
                                         .duration(750)
                                         .attr("r", (d.weight * 2 + 12) * 1.5);
                                     
                                 }).on("mousemove", function () {
                                     tooltip.style("left", function() {
                                         return (d3.event.layerX + 20) + "px";
                                     }).style("top", function(){
                                    	 return (getOffset(d3.event).y - 100) + "px";
                                     })
                                 }).on("mouseout", function(d) {

                                     node.classed("node-active", false);
                                     link.classed("link-active", false);

                                     d3.select(this).select("circle").transition()
                                         .duration(750)
                                         .attr("r", d.weight * 2 + 12);
                                     tooltip.style("visibility", "hidden");
                                     
                                 }).on("dblclick", function(d) {
                                	   
                                	 var modalInstance;
                                     modalInstance = $modal.open({
                                         templateUrl: "/views/template/relationDetailsPopup.html",
                                         controller: "relPopupCtrl",
                                         size: "xl",
                                         resolve: {
                                             "nodeData": function() {
                                                 return d;
                                             }
                                         }
                                     }), modalInstance.result.then(function(reponame) {
                                     	 render(scope.dagData);
                                     }, function() {
                                     	 render(scope.dagData);
                                         $log.info("Modal dismissed at: " + new Date);
                                     });
                             });
                             
                             //LINK EVENTS
                             link.on("dblclick", function(d) {	 
                                 var modalInstance;
                                 modalInstance = $modal.open({
                                     templateUrl: "/views/template/relationOverviewPopup.html",
                                     controller: "relOverviewCtrl",
                                     //size: "xl",
                                     resolve: {
                                         "relationData": function() {
                                             return d;
                                         }
                                     }
                                 }), modalInstance.result.then(function(reponame) {
                                	 var svg=document.getElementById("svg");
                                 	 if(svg!=null)
                                 		svg.outerHTML="";
                                 	 render(scope.dagData);
                                 }, function() {
                                	 var svg=document.getElementById("svg");
                                 	 if(svg!=null)
                                 		svg.outerHTML="";
                                 	 render(scope.dagData);
                                     $log.info("Modal dismissed at: " + new Date);
                                 });
                             
                             }).on("mouseout", function(d) {
                            	 this.style.strokeOpacity = '';
                            	 //tooltip.style("visibility", "hidden");
                             }).on("mouseover", function(d) {
                            	 this.style.strokeOpacity = '1';
                            	 //var testDiv = document.getElementById("dag-tooltip");
                        	     //testDiv.innerHTML = "<span><h5>"+"&nbsp;"+d.label+"&nbsp;"+"</h5></span>";
                        	     //tooltip.style("visibility", "visible");
                             }).on("mousemove",function(d){
                            	  //tooltip.style("left", (getOffset(d3.event).x) + "px")
                            	 //.style("top", (getOffset(d3.event).y+50) + "px");
                             });
                             
                             
                             function getOffset(e) {
                                 var posx = 0;
                                 var posy = 0;
                                 if (!e) var e = window.event;
                                 if (e.pageX || e.pageY) {
                                     posx = e.pageX;
                                     posy = e.pageY;
                                 } else if (e.clientX || e.clientY) {
                                     posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                                     posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                                 }
                                 return {
                                     x: posx,
                                     y: posy
                                 };
                             }

                             function dottype(d) {
                                 d.x = +d.x;
                                 d.y = +d.y;
                                 return d;
                             }

                             function zoomed() {
                                 container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                             }

                             function dragstarted(d) {
                                 d3.event.sourceEvent.stopPropagation();

                                 d3.select(this).classed("dragging", true);
                                 force.start();
                             }

                             function dragged(d) {

                                 d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);

                             }

                             function dragended(d) {

                                 d3.select(this).classed("dragging", false);
                             }
                    	
                    }
                    
                    
                    render();
                    

                });
            }
        }



    }]).directive("allRelations", ["D3factory", "$modal", "$log", "$rootScope", "$window","$routeParams", function(D3factory, $modal, $log, $rootScope, $window, $routeParams) {

        return {
            restrict: "EA",
            scope: {
                data: "="
            },
            replace: true,
            transclude: true,
            link: function(scope, element, attrs) {

                D3factory.d3().then(function(d3) {

                    //WATCHER
                    scope.$watch('data', function(newVals, oldVals) {
                        if (newVals) {
                        	scope.dagData=angular.copy(newVals);
                        	render(scope.dagData);
                        }
                    }, true);

                    
                    //RENDER
                    function render(data){
                    	
                    	var svg=document.getElementById("allrelation-svg");
                    	if(svg!=null)
                    		svg.outerHTML="";
                    	
                    	var graph=data;
                    
                             var margin = {
                                 top: -5,
                                 right: -5,
                                 bottom: -5,
                                 left: -5
                             };
                             /* var width = 500 - margin.left - margin.right,
         		        			    height = 400- margin.top - margin.bottom;*/
                            /* var width = 1000 - margin.left - margin.right,
                                 height = 500 - margin.top - margin.bottom;*/
                             
                             //var width = 1133 - margin.left - margin.right,
                             //height = 497 - margin.top - margin.bottom;
                        
                             var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
                             width=width*90/100;
                             var height = 497 - margin.top - margin.bottom;
                             
                             var color = d3.scale.category20();

                             var force = d3.layout.force()
                                 .charge(-200)
                                 .linkDistance(80)
                                 .size([width + margin.left + margin.right, height + margin.top + margin.bottom]);

                             var zoom = d3.behavior.zoom()
                                 .scaleExtent([1, 10])
                                 .on("zoom", zoomed);

                             var drag = d3.behavior.drag()
                                 .origin(function(d) {
                                     return d;
                                 })
                                 .on("dragstart", dragstarted)
                                 .on("drag", dragged)
                                 .on("dragend", dragended);

                             
                             var svg = d3.select(element[0]).append("svg")
                                 .attr("id","allrelation-svg")
                                 .attr("width", width + margin.left + margin.right)
                                 .attr("height", height + margin.top + margin.bottom)
                                 .append("g")
                                 .attr("transform", "translate(" + margin.left + "," + margin.right + ")")
                                 .call(zoom);

                             var rect = svg.append("rect")
                                 .attr("width", width)
                                 .attr("height", height)
                                 .style("fill", "none")
                                 .style("pointer-events", "all");

                             var container = svg.append("g");

                             force
                                 .nodes(graph.nodes)
                                 .links(graph.links)
                                 .start();
                             
                             var tooltip = d3.select("div.allrelation-dag-tooltip");

                             var link = container.append("g")
                                 .attr("class", "links")
                                 .selectAll(".link")
                                 .data(graph.links)
                                 .enter().append("line")
                                 .attr("class", "link")
                                 .style("stroke-width", function(d) {
                                     return Math.sqrt(d.value);
                                 });
                             
                             //node
                             var node = container.append("g")
                                 .attr("class", "nodes")
                                 .selectAll(".node")
                                 .data(graph.nodes)
                                 .enter().append("g")
                                 .attr("class", "node")
                                 .attr("cx", function(d) {
                                     return d.x;
                                 })
                                 .attr("cy", function(d) {
                                     return d.y;
                                 })
                                 .call(drag);

                             node.append("circle")
                                 .attr("r", function(d) {
                                	 var radius=d.weight * 2 + 12;
                                	 d.radius=radius;
                                     return radius;
                                 })
                                 .style("fill", function(d) {
                                     return color(1 / d.id);
                                 });

                             force.on("tick", function() {
                                 link.attr("x1", function(d) {
                                         return d.source.x;
                                     })
                                     .attr("y1", function(d) {
                                         return d.source.y;
                                     })
                                     .attr("x2", function(d) {
                                         return d.target.x;
                                     })
                                     .attr("y2", function(d) {
                                         return d.target.y;
                                     });

                                 node.attr("transform", function(d) {
                                     return "translate(" + d.x + "," + d.y + ")";
                                 });
                             });

                            
                             node.append("text")
                                 .attr("text-anchor", "middle")
                                 .attr("style", "stroke-width: 0.0px; font-size: 10px;")
                                 .attr("dy", function(d){
                                	return (2* parseInt(d.radius))+2;})
                                 .text(function(d) {
                                     return (d.name.length > 15 ? d.name.substring(0, 15) + '...' : d.name);
                                 });

                             var linkedByIndex = {};
                             graph.links.forEach(function(d) {
                                 linkedByIndex[d.source.index + "," + d.target.index] = 1;
                             });

                             function isConnected(a, b) {
                                 return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index];
                             }


                             //NODE EVENTS 
                             node.on("mouseover", function(d) {

                            	    var testDiv = document.getElementById("allrelation-dag-tooltip");
                            	    testDiv.innerHTML = "<span><h5 color='black'>"+"&nbsp;"+d.name+"&nbsp;"+"</h5></span>";
                            	    tooltip.style("visibility", "visible");
                                   
                                     node.classed("node-active", function(o) {
                                         var thisOpacity = isConnected(d, o) ? true : false;
                                         this.setAttribute('fill-opacity', thisOpacity);
                                         return thisOpacity;
                                     });

                                     link.classed("link-active", function(o) {
                                         return o.source === d || o.target === d ? true : false;
                                     });

                                     d3.select(this).classed("node-active", true);
                                     d3.select(this).select("circle").transition()
                                         .duration(750)
                                         .attr("r", (d.weight * 2 + 12) * 1.5);
                                     
                                 }).on("mousemove", function () {
                                     tooltip.style("left", function() {
                                         return (d3.event.layerX + 20) + "px";
                                     }).style("top", function(){
                                    	 return (getOffset(d3.event).y - 100) + "px";
                                     })
                                 }).on("mouseout", function(d) {

                                     node.classed("node-active", false);
                                     link.classed("link-active", false);

                                     d3.select(this).select("circle").transition()
                                         .duration(750)
                                         .attr("r", d.weight * 2 + 12);
                                     tooltip.style("visibility", "hidden");
                                     
                                 }).on("dblclick", function(d) {
                                	   
                                	 var modalInstance;
                                     modalInstance = $modal.open({
                                         templateUrl: "/views/template/relationDetailsPopup.html",
                                         controller: "relPopupCtrl",
                                         size: "xl",
                                         resolve: {
                                             "nodeData": function() {
                                                 return d;
                                             }
                                         }
                                     }), modalInstance.result.then(function(reponame) {
                                     	 render(scope.dagData);
                                     }, function() {
                                     	 render(scope.dagData);
                                         $log.info("Modal dismissed at: " + new Date);
                                     });
                             });
                             
                             //LINK EVENTS
                             link.on("dblclick", function(d) {	 
                                 var modalInstance;
                                 modalInstance = $modal.open({
                                     templateUrl: "/views/template/relationOverviewPopup.html",
                                     controller: "relOverviewCtrl",
                                     //size: "xl",
                                     resolve: {
                                         "relationData": function() {
                                             return d;
                                         }
                                     }
                                 }), modalInstance.result.then(function(reponame) {
                                	 var svg=document.getElementById("svg");
                                 	 if(svg!=null)
                                 		svg.outerHTML="";
                                 	 render(scope.dagData);
                                 }, function() {
                                	 var svg=document.getElementById("svg");
                                 	 if(svg!=null)
                                 		svg.outerHTML="";
                                 	 render(scope.dagData);
                                     $log.info("Modal dismissed at: " + new Date);
                                 });
                             
                             }).on("mouseout", function(d) {
                            	 this.style.strokeOpacity = '';
                            	 //tooltip.style("visibility", "hidden");
                             }).on("mouseover", function(d) {
                            	 this.style.strokeOpacity = '1';
                            	 //var testDiv = document.getElementById("dag-tooltip");
                        	     //testDiv.innerHTML = "<span><h5>"+"&nbsp;"+d.label+"&nbsp;"+"</h5></span>";
                        	     //tooltip.style("visibility", "visible");
                             }).on("mousemove",function(d){
                            	  //tooltip.style("left", (getOffset(d3.event).x) + "px")
                            	 //.style("top", (getOffset(d3.event).y+50) + "px");
                             });
                             
                             
                             function getOffset(e) {
                                 var posx = 0;
                                 var posy = 0;
                                 if (!e) var e = window.event;
                                 if (e.pageX || e.pageY) {
                                     posx = e.pageX;
                                     posy = e.pageY;
                                 } else if (e.clientX || e.clientY) {
                                     posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                                     posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                                 }
                                 return {
                                     x: posx,
                                     y: posy
                                 };
                             }

                             function dottype(d) {
                                 d.x = +d.x;
                                 d.y = +d.y;
                                 return d;
                             }

                             function zoomed() {
                                 container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                             }

                             function dragstarted(d) {
                                 d3.event.sourceEvent.stopPropagation();

                                 d3.select(this).classed("dragging", true);
                                 force.start();
                             }

                             function dragged(d) {

                                 d3.select(this).attr("cx", d.x = d3.event.x).attr("cy", d.y = d3.event.y);

                             }

                             function dragended(d) {

                                 d3.select(this).classed("dragging", false);
                             }
                    	
                    }
                    
                    
                    render();
                    

                });
            }
        }



    }]);;
    
 
	
}).call(this);


(function(){
	"use strict";
	
	var loggerModule  = angular.module("logToServer",[]) ;
	
	loggerModule.service('$log', function() {
		this.log = function(msg) {
			JL('Angular').trace(msg);
		};
		this.debug = function(msg) {
			JL('Angular').debug(msg);
		};
		this.info = function(msg) {
			JL('Angular').info(msg);
		};
		this.warn = function(msg) {
			JL('Angular').warn(msg);
		};
		this.error = function(msg) {
			JL('Angular').error(msg);
		};
		
		JL.setOptions({
			'defaultAjaxUrl' : '/logRequest',
			'requestId' : window.location.pathname.split("#").pop(),
			"bufferSize": 20,
			serverSideMessageFormat:"{ 'Message': %message }"
		});
	});
	
	loggerModule.factory('$exceptionHandler', function() {
		return function(exception, cause) {
			JL("Angular").fatalException({'error': exception.message });
			throw exception;
		};
	});
	
	
	loggerModule.factory('logToServerInterceptor', ['$q', function ($q) {
	    var myInterceptor = {
	            // The request function is called before the AJAX request is sent
	            'request': function (config) {
	                config.msBeforeAjaxCall = new Date().getTime();
	                return config;
	            },
	            // The response function is called after receiving a good response from the server
	            'response': function (response) {
	                var msAfterAjaxCall = new Date().getTime();
	                var timeTakenInMs = msAfterAjaxCall - response.config.msBeforeAjaxCall;
	                if (timeTakenInMs > response.config.warningAfter) {
	                    JL('Angular.Ajax').warn({ 
	                      timeTakenInMs: timeTakenInMs, 
	                      config: response.config, 
	                      data: response.data });
	                }
	                return response;
	            },
	            // The responseError function is called when an error response was received, or when a timeout happened.
	            'responseError': function (rejection) {
	                var errorMessage = "unknown";
	                
	                if (rejection.status != 0) {
	                    errorMessage = rejection.data.ExceptionMessage;
	                }
	                
	                JL('Angular.Ajax').fatalException({
	                	errorMessage: errorMessage,
	                	status: rejection.status,
	                	config: rejection.config},rejection.data);
	                
	                return $q.reject(rejection);
	            }
	        };
	        return myInterceptor;
	    }]);
	
	
}).call(this);





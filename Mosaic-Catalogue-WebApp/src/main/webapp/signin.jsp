<div class="row loginBackground" data-ng-controller="signinController">
	<div class="col-md-6" ng-init="setViewHeight();" >
		<img style="height: {{viewHeight}}px;" ng-src="../styles/images/publish_catalog/login_page_lefft_catlog_image.png"  alt="Logo">
	</div>
	<div class="col-md-6" >
		<div class="col-md-8" style="margin-left: 15%;
    margin-top: 35%;">
			<form class="form-horizontal" ng-submit="submitFormForLogin(1);">
				<fieldset>
					<div class="form-group">
						<div class="input-group input-group-lg">
							<span class="input-group-addon icon-color-css"> 
							<i class="fa fa-user"></i>
							</span> <input type="email" class="form-control"
								placeholder="Email Id" ng-model="emailId">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group input-group-lg">
							<span class="input-group-addon icon-color-css"> <i class="fa fa-lock"></i>
							</span> <input type="password" class="form-control"
								placeholder="Password" ng-model="password">
						</div>
					</div>
					<div class="form-group">
						<span style="color: red">{{error}}</span>
					</div>
					<div class="form-group">
						<button type="submit"
							class="btn btn-w-md btn-gap-v btn-line-primary btn-lg btn-block">Submit</button>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>

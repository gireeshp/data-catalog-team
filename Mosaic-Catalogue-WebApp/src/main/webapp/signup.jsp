<html>
<head>
<link rel="stylesheet" href="styles/main.css">
<link rel="stylesheet" href="styles/customCss/custom-icons.css">
<style type="text/css">
.red {
	color: red
}
.green {
	color: green
}
</style>
</head>
<body>
	<div class="page-signin opacity" data-ng-controller="signupController">
		<div class="page-signup">
			<div class="signin-header">
				<div class="container text-center">
					<section class="logo">
						<a href="javascript:;" class="sub-css"> <img class="logoCss"
							src="../styles/images/logo.png"
							style="padding: 0px 12px 15px 0px;">MOSAIC<!-- <sup>TM</sup><span
							style="font-weight: normal; font-size: 30px;">&nbsp;Decisions --></span>
						</a>
					</section>
				</div>
			</div>

			<div class="signup-body">
				<div class="container">
					<div class="form-container">
						<form name="signupForm" class="form-horizontal"
							ng-submit="submitFormForSignup();" method="post">
							<div class="form-group">
								<div class="input-group input-group-lg">
									<span class="input-group-addon icon-color-css"> <span
										class="glyphicon glyphicon-envelope"></span>
									</span> <input type="email" name="email" class="form-control"
										ng-model="emailId" placeholder="Email Id"
										ng-blur="checkUserExists(emailId)" ng-keyup="errorEmail=''"
										required>
								</div>
								<span style="color: red" ng-show="signupForm.email.$error.email">Invalid
									email address.</span> <span style="color: red">{{errorEmail}}</span>
							</div>

							<div class="form-group">
								<div class="input-group input-group-lg">
									<span class="input-group-addon icon-color-css"> <span
										class="glyphicon glyphicon-lock"></span>
									</span> <input type="password" class="form-control"
										name="firstPassword" data-ng-model="firstPassword"
										placeholder="Password" ng-pattern="passwordPolicy" required>
								</div>
								<span ng-show="signupForm.firstPassword.$error.pattern"
									class="red">Not a valid password.</span>
							</div>
							<div class="form-group">
								<div class="input-group input-group-lg">
									<span class="input-group-addon icon-color-css"> <span
										class="glyphicon glyphicon-lock "></span>
									</span> <input type="password" class="form-control"
										name="secondPassword" data-ng-model="secondPassword"
										placeholder="Confirm Password"
										ng-keyup="checkPassword(secondPassword)"
										ng-pattern="passwordPolicy" required>
								</div>
								<span ng-show="isconfirm || signupForm.secondPassword.$dirty"
									ng-class="{true:'green',false:'red'}[isconfirm]">{{
									isconfirm==true? '':'Passwords do not match'}} </span>
								<!-- <span class="text-error" style="color: red"
									ng-show="signupForm.secondPassword.$dirty.pattern && signupForm.secondPassword.$error.dontMatch">Passwords
									don't match</span> -->
							</div>
							<div class="form-group">
								<div class="input-group input-group-lg">
									<span class="input-group-addon icon-color-css"> <span
										class="glyphicon glyphicon-user"></span>
									</span> <input type="text" class="form-control"
										data-ng-model="firstName" placeholder="First Name" required>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group input-group-lg">
									<span class="input-group-addon icon-color-css"> <span
										class="glyphicon glyphicon-user"></span>
									</span> <input type="text" class="form-control"
										data-ng-model="lastName" placeholder="Last Name" required>
								</div>
							</div>
							<div class="form-group">
								<p class="text-muted text-small">
									By clicking on Sign up, you agree to
									<!-- <a href="javascript:;"> -->
									<b>terms & conditions</b>
									<!-- </a> -->
									and
									<!-- <a href="javascript:;"> -->
									<b>privacy policy</b>
									<!-- </a> -->
								</p>
								<div class="divider"></div>
								<div>
									<span style="color: red">{{error}}</span>
								</div>
							</div>
							<div class="form-group">
								<button type="submit"
									class="btn btn-w-md btn-gap-v btn-line-primary btn-lg btn-block"
									data-ng-disabled="signupForm.$invalid">Sign up</button>
							</div>
							<div class="form-container">
								<div class="form-group">
									<div class="alert  alert-warning alert-dismissible">
										<ol>
											<li>Password should be 8 to 16 characters long</li>
											<li>Must have atleast one capital letter</li>
											<li>Must have atleast one digit</li>
											<li>Must have atleast one special character</li>
										</ol>
									</div>
								</div>
							</div>
						</form>
						<section>
							<p class="text-center text-muted">
								Already have an account? <a href="#/signin"><strong>
										Log in now </strong></a>
							</p>
						</section>
					</div>
				</div>
			</div>
			<footer>
				<div id="footerCss">
					<div class="container">
						<div class="row">
							<!-- <div class="col-md-3" style="text-align: left;">
								<ul class="footerMenu">
									<li><a class="colorMenu" href="#/home">Home</a></li>
									<li><a class="colorMenu" href="#/productoverview">Product
											Overview</a></li>
									<li><a class="colorMenu" href="#/contact">Contact</a></li>
								</ul>
							</div> -->
							<div class="col-md-12 text-center">
								<p
									class="footerCss footer-copyright col-md-12 text-muted">
									&copy;<a href="https://www.lntinfotech.com" target="_blank"
										class="cursurPoint default-color"> 2017 Larsen &amp; Toubro
										Infotech Ltd.</a> All rights reserved.
								</p>
							</div>
							<!-- <div class="col-md-3" style="text-align: right;">
								<ul class="socialNetwork" style="margin-bottom: 0px;">
									<li><a href="https://www.facebook.com/AugumentIQ"
										target="_blank" class="tips" title="Facebook"
										data-original-title="follow me on Facebook"><i
											class="icon-facebook-1 iconRounded"></i></a></li>
									<li><a href="https://twitter.com/AugmentIQData/followers"
										target="_blank" class="tips" title="Twitter"
										data-original-title="follow me on Twitter"><i
											class="icon-twitter-bird iconRounded"></i></a></li>
									<li><a
										href="https://www.linkedin.com/company/augmentiq-data-sciences"
										class="tips" title="" target="_blank"
										data-original-title="follow me on Linkedin"><i
											class="icon-linkedin-1 iconRounded"></i></a></li>
								</ul>
							</div> -->
						</div>
					</div>
				</div>
				<!-- <a href="#" id="nekoToTop" style="display: inline;"><i class="icon-up-open"></i></a> -->
			</footer>
		</div>
	</div>
</body>
</html>
<div data-ng-controller="connectionsViewCtlr">
	<div class="col-md-12 margin-top15 padding0">
		<div class="col-md-7"  style="display: {{isConnectionCreateEdit ? 'none' : 'block' }}">
			<span class="text-left">
				<button type="button"  class="btn catalog-btn fL" data-ng-click="createConnection()">
							<span class="fa fa-plus"></span>New Connection
				</button>
			</span>
		</div>
		<div class="col-md-5 padding0" style="display: {{isConnectionCreateEdit ? 'none' : 'block' }}" >
			<span class="timeline-div-search-box">
				<i class="search-glyphicon glyphicon glyphicon-search "></i>
				<input type="text" class="form-control"  ng-model="searchText" placeholder="Search connections" aria-haspopup="true" aria-expanded="true" />
			</span>
		</div>
		
		<div ng-if="isConnectionCreateEdit" class="margin-bottom-0 ">
			<div class="col-md-12 padding-left30">
				<h4>New connnection</h4>
			</div>	
		</div>
	</div>
	<div ng-init="setViewHeight()" class="col-md-12 timeline-div-body padding0 discover-scroll" style="height:{{viewHeight - 60}}px;" ng-if="!isConnectionCreateEdit && connectionsList && connectionsList.length > 0">
		<div class="col-md-3 grid-padding-top" data-ng-repeat="store in connectionsList | filter:searchText">
			<div class="gridCard grid-height-card-type-1 padding0 col-md-12" ng-click="selectConnection($event, store)">
                <div class="col-md-12">
               		<div class="col-md-2 padding0">
                   		<img alt="{{datasourceAltNames[connectorSTName]}}" ng-src="{{datasourceImages[connectorSTName]}}"
                    		class="connection-image">
                    		
                    </div>
               		<div class="col-md-10 padding0">
               			<h4 class="col-md-12 text-elipse" style="color:#000;" tooltip="{{store.conncetionName}}" tooltip-placement="top">{{store.conncetionName}}</h4>
                    	<span class="col-md-12 text-elipse paddingRight0">{{store.createdDate | date : 'MMM dd, yyyy'}}&nbsp;|&nbsp;<img ng-src="../../styles/images/projectImages/user-grid.png" />&nbsp;{{store.createdBy}}</span>
                    </div>
                </div>
                <div class="col-md-12 padding0 margin-top15" ng-if="['ORACLE', 'MYSQL', 'POSTGRES'].indexOf(connectorSTName) > -1">
                	<div class="col-md-6 text-elipse padding0">
						<span class="col-md-12 text-elipse"> <b>Ip address</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.ipAddress}}">{{store.ipAddress}}</span>
					</div>
					<div class="col-md-6 text-elipse padding0 text-right">
						<span class="col-md-12 text-elipse"> <b>Port</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.port}}">{{store.port}}</span>
					</div>
					<div class="text-elipse padding0" ng-class="{'col-md-12' : connectorSTName != 'ORACLE' , 'col-md-6' : connectorSTName == 'ORACLE'}">
						<span class="col-md-12 text-elipse"> <b>Username</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.dbUserName}}">{{store.dbUserName}}</span>
					</div>
					<div class="text-elipse padding0 col-md-6 text-right" ng-if="connectorSTName == 'ORACLE'">
						<span class="col-md-12 text-elipse"> <b>SID</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.sid}}">{{store.sid}}</span>
					</div>
               	</div>
               	             	
               	<!-- FTP and SFTP -->
               	<div class="col-md-12 padding0 margin-top15" ng-if="['FTP SERVER', 'SFTP SERVER'].indexOf(connectorSTName) > -1">
                	<div class="col-md-6 text-elipse padding0">
						<span class="col-md-12 text-elipse"> <b>Ip address</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.ipAddress}}">{{store.ipAddress}}</span>
					</div>
					<div class="col-md-6 text-elipse padding0 text-right">
						<span class="col-md-12 text-elipse"> <b>Port</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.port}}">{{store.port}}</span>
					</div>
					<div class="text-elipse padding0 col-md-6" >
						<span class="col-md-12 text-elipse"> <b>Username</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.dbUserName}}">{{store.dbUserName}}</span>
					</div>
					<div class="text-elipse padding0 col-md-6 text-right" >
						<span class="col-md-12 text-elipse"> <b>Root path</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.ftpfilePath}}">{{store.ftpfilePath}}</span>
					</div>
               	</div>
               	<!-- FTP and SFTP end -->
               	
               	<!-- COGNOS -->
               	<div class="col-md-12 padding0 margin-top15" ng-if="['COGNOS'].indexOf(connectorSTName) > -1">
                	<div class="col-md-6 text-elipse padding0">
						<span class="col-md-12 text-elipse"> <b>Ip address</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.ipAddress}}">{{store.ipAddress}}</span>
					</div>
					<div class="col-md-6 text-elipse padding0 text-right">
						<span class="col-md-12 text-elipse"> <b>Port</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.port}}">{{store.port}}</span>
					</div>
					<div class="text-elipse padding0 col-md-6" >
						<span class="col-md-12 text-elipse"> <b>Username</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.dbUserName}}">{{store.dbUserName}}</span>
					</div>
					<div class="text-elipse padding0 col-md-6 text-right" >
						<span class="col-md-12 text-elipse"> <b>Namespace</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.ftpfilePath}}">{{store.nameSpace}}</span>
					</div>
               	</div>
               	<!-- COGNOS end -->
               	
               <!-- BLOB -->
               	<div class="col-md-12 padding0 margin-top15" ng-if="['BLOB'].indexOf(connectorSTName) > -1">
                	<div class="col-md-6 text-elipse padding0">
						<span class="col-md-12 text-elipse"> <b>Account Name</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.accountName}}">{{store.accountName}}</span>
					</div>
					<div class="col-md-6 text-elipse padding0 text-right">
						<span class="col-md-12 text-elipse"> <b>Protocol</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.defaultEndPointsProtocol}}">{{store.defaultEndPointsProtocol}}</span>
					</div>
               	</div>
               	<!-- BLOB end -->
               	
               	 <!-- AMAZONS3 -->
               	<div class="col-md-12 padding0 margin-top15" ng-if="['AMAZONS3'].indexOf(connectorSTName) > -1">
                	<div class="col-md-6 text-elipse padding0">
						<span class="col-md-12 text-elipse"> <b>Bucket name</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.amazons3AccessId}}">{{store.amazons3AccessId}}</span>
					</div>
					<div class="col-md-6 text-elipse padding0 text-right">
						<span class="col-md-12 text-elipse"> <b>Folder</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.s3folder}}">{{store.s3folder}}</span>
					</div>
               	</div>
               	<!-- AMAZONS3 end-->
               	
               	<!-- HDFS -->
               	<div class="col-md-12 padding0 margin-top15" ng-if="['HDFS'].indexOf(connectorSTName) > -1">
                	<div class="col-md-6 text-elipse padding0">
						<span class="col-md-12 text-elipse"> <b>Ip address</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.ipAddress}}">{{store.ipAddress}}</span>
					</div>
					<div class="col-md-6 text-elipse padding0 text-right">
						<span class="col-md-12 text-elipse"> <b>Port</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.port}}">{{store.port}}</span>
					</div>
					<div class="text-elipse padding0 col-md-6" >
						<span class="col-md-12 text-elipse"> <b>Username</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.dbUserName}}">{{store.dbUserName}}</span>
					</div>
					<div class="text-elipse padding0 col-md-6 text-right" >
						<span class="col-md-12 text-elipse"> <b>Root folder</b></span>
						<span class="col-md-12 text-elipse" data-toogle="tooltip" title="{{store.ftpfilePath}}">{{store.nameSpace}}</span>
					</div>
               	</div>
               	<!-- HDFS end -->
               	
               	<div class="card-footer item-footer text-right">
               		<div style="border-top: 1px solid #16a6af;">
						<p>
							<a href="javascript:;"  style="margin-right: 10px;" class="actionMenu"
								data-ng-click="deleteConnection($event, store)"><i
								class="fa fa-trash" tooltip-placement="top"
								tooltip="Delete connection"></i></a>
						</p>
					</div>	
				</div>					
			</div>
			
		</div>
	</div>	
	<div class="col-md-12 timeline-div-body text-center" style="height:{{viewHeight - 60}}px;" ng-if="!isConnectionCreateEdit &&  connectionsList && connectionsList.length === 0" > 
		<div class="col-md-12">No connection's configured.</div>
	</div>
	<div class="col-md-12 timeline-div-body" style="height:{{viewHeight - 60}}px;" ng-if="isConnectionCreateEdit">
		<create-connection></create-connection>
	</div>
</div>
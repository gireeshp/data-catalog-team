<div class="page page-profile" ng-controller="dataSourceInformer"
	ng-init="editableInput='true'" dw-loading="loadingDS"
	dw-loading-options="{className: 'custom-loading', spinnerOptions: {className: 'custom-spinner'}}">
	<div class="row" id="top">
		<div class="panel panel-profile modal-header ds-header-css">
			<div class="mini-box workflow-head bg-primary">
				<div class="col-md-12">
					<div class="box-info col-md-1 pad-zero">
						<span class="box-icon bg-primary icon-line-height-monitor">
							<img class="img-width" alt="Data"
							src="../styles/images/projectImages/data_icon.png">
						</span>
					</div>
					<div class="col-md-7 pad-left-zero">
						<div class="col-md-12">
							<font class="size-h3"> {{selected}} </font><i
								class="glyphicon glyphicon-check-icon"></i> <span
								style="float: right;"> </span>
						</div>
						<div class="col-md-12">Created on :
							{{dataSourceInstance.createdDate | date : 'medium'}}</div>
					</div>
					<div class="col-md-3">
						<div class="col-md-12">
							<font color="white" class="size-h4"> Record count :
								{{dataSourceInstance.totalRecords}} </font>
						</div>
						<div class="col-md-12">Type : {{datasource.dataSourceType |
							contentType}}</div>

					</div>
					<div class="col-md-1">
						<div class="closePopUp">
							<i class="fa fa-times-circle icon-margin"
								ng-click="dismissModel();"></i>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div ng-if='!myTbHide' class="col-md-12">

			<button ng-if="datasource.appId==null"
				class="btn btn-primary pull-right"
				ng-disabled="dataSourceRequestDisable"
				ng-click="requestForDatasource(dataSourceInstance,'justification', dataSourceRequestLabel)">{{dataSourceRequestLabel}}</button>

			<div id="tabs" ng-controller="TabsCtrl">
				<tabset> <tab ng-click="onClickTab(tab)"
					ng-repeat="tab in tabs" style="cursor : pointer;"
					heading="{{tab.title == 'Others' ? 'Advance' : tab.title == 'Connections' ? 'Connections' :  tab.title == 'FieldMappings' ? 'Fields' : tab.title}}"
					active="tab.active" class="pad-zero">
				<div ng-include="currentTab"></div>
				</tab> </tabset>
			</div>
		</div>

		<div class="col-md-12" ng-if="subMenu.length > 0">
			<div id="tabs" class="tab-size">
				<tabset> <tab ng-click="onClickSubMenuTab(tab)"
					ng-repeat="tab in subMenu" style="cursor : pointer;"
					heading="{{tab.title}}" active="tab.active" class="pad-zero">
				<div ng-include="currentTab"></div>
				</tab> </tabset>
			</div>
		</div>
		<br /> <br /> <br />

		<fieldset ng-disabled="popUpValue || projectAccessType == 'reviewer'" dw-loading="loadingDS"
			dw-loading-options="{className: 'custom-loading', spinnerOptions: {className: 'custom-spinner'}}">
			<div class="row" id="top">
				<div class="col-md-12" ng-if='!myTbHide1'>&nbsp;</div>
				<br />
				<!-- Info page code start -->
			<div class="col-md-12 padding-one"
				ng-hide="myTbHide1 && !(activeTab == 'Overview')">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="col-md-12 pad-zero tab-right">
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>ID</b> 
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.dataSourceId}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Name</b> 
								</div>
								<div class="col-md-6 info-content">
									{{dataSourceInstance.dataSourceName}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Data source group</b> 
								</div>
								<div class="col-md-6">{{dataSourceInstance.dataRepoName == 'null' ? "-" : dataSourceInstance.dataRepoName }}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-content">
									<b>Ingestion method</b> 
								</div>
								<div class="col-md-6">{{datasource.ingection == 'quick' ? 'Basic' : 'Advanced'}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Project Name</b> 
								</div>
								<div class="col-md-6 info-content">
								{{projectName}}
								</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Category</b> 
								</div>
								<div class="col-md-6 info-content">
									<!-- {{datasource.category == 'null' ? "-" : datasource.category}} -->
									<span class="ui-select" style="height: 34px; left: -3px;">
										<select class="dropdown-width" ng-model="datasource.category"
										ng-options="category for category in categories"
										ng-change="changeCategory($index,datasource.category)">
											<option value="">Select category type</option>
									</select>
									</span>
								</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Sub-Category</b> 
								</div>
								<div class="col-md-6 info-content">
									<!-- {{datasource.subCategory == 'null' ? "-" : datasource.subCategory}} -->
									<span class="ui-select"
										style="height: 34px;left: -3px; width: 100%;"> <select
										class="dropdown-width" ng-model="datasource.subCategory"
										ng-options="category for category in categoriesDetails[datasource.category]"
										ng-change="changeSubCategory($index,datasource.subCategory)">
											<option value="">Select sub-category type</option>
									</select>
									</span>
								</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Description</b> 
								</div>
								<div class="col-md-6 info-content"><!-- {{dataSourceInstance.description == 'null' ? "-" : dataSourceInstance.description}} -->
									<textarea class="form-control" rows="1" cols="10"
										placeholder="Description as data source"
										ng-model="datasource.description"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-md-12 pad-zero">
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Total records</b> 
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.totalRecords}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Type</b> 
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.dataSourceType |
									contentType}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Created by</b> 
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.createdBy}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Created on</b> 
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.createdDate |
									date : 'medium'}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Refreshed by</b> 
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.lastRunBy == 'null' ? "-" : dataSourceInstance.lastRunBy}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Last refreshed on</b>
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.updatedDate == 'null' ? " 0" : dataSourceInstance.updatedDate |
									date : 'medium'}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Size</b> 
								</div>
								<div class="col-md-6 info-content">{{dataSourceInstance.size |
									sizeConvert}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-6 info-header">
									<b>Tags</b>
								</div><!-- datasource.tags -->
								<div class="col-md-6 info-content">
									<div class="col-md-12 pad-zero">
										<ui-select multiple ng-model="datasource.tags"
											theme="bootstrap" ng-disabled="disabled" style="width: 100%;">
										<ui-select-match placeholder="Select persona">{{$item}}</ui-select-match>
										<ui-select-choices
											repeat="tag in tagNames">
										<div
											ng-bind-html="tag | highlight: $select.search"></div>
										</ui-select-choices> </ui-select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">&nbsp;</div>
					<div class="col-md-12" style="right: 30px !important;" ng-if="datasource.appId==null">
						<div class="col-md-12">
							<button class="btn btn-primary saveCss" ng-click="saveFieldMapping(datasource.fieldMappings)">Save</button>					
						</div>
					</div>
				</div>
			</div>
			<!-- Info page code End -->
				<!-- File Format Start -->
				<div class="col-md-12 padding-one"
					ng-hide="!myTbHide1 && !(activatedSubMenu == 'Adv. Settings')">
					<div class="col-md-12">
						<section class="panel panel-default">
							<div class="panel-heading lan-heading">
								<strong> <span class="glyphicon glyphicon-th"></span>
									Define data-at-rest
								</strong>
							</div>
							<div class="panel-body">
								<div class="col-md-12 noteCss" role="alert">
									<span
										ng-bind-html='explanation["headingForFileFormat"] | unsafe'></span>
								</div>
								<div class="form-group"
									ng-init="datasource.fileStoreObject = 'HDFS'">
									<fieldset class="col-sm-7">
										<label class="ui-radio" tooltip-placement="right"
											tooltip='Data-Rest file stored in HDFS'> <input
											value="HDFS" ng-model="datasource.fileStoreObject"
											name="fileStore" type="radio" checked="checked"> <span>
												HDFS</span>
										</label> <label class="ui-radio" tooltip-placement="right"
											tooltip='Data-Rest file stored in Hbase'> <input
											value="HBASE" ng-model="datasource.fileStoreObject"
											name="fileStore" type="radio"> <span> HBASE </span>
										</label>
										<label class="ui-radio" ng-if='fileDataIngesterDetails.dataSourceType == "RDBMS_DATA_SOURCE"' tooltip-placement="right"
											tooltip='Data processed directly from source'> <input
											value="SOURCE" ng-model="datasource.fileStoreObject"
											name="fileStore" type="radio"> <span> SOURCE </span>
										</label>


									</fieldset>
									<div class="col-md-12">&nbsp;</div>
									<div class="col-sm-12"
										ng-if="datasource.fileStoreObject == 'HDFS'"
										style="margin-bottom: 8px;">
										<label class="col-sm-2" style="margin-left: -10px;">File
											type: </label>
										<div class="col-sm-8">
											<div class="col-sm-3 element">
												<span class="ui-select"> <select name="fileType"
													ng-options="maxiqCommonObj.keyOf as maxiqCommonObj.keyOf for maxiqCommonObj in maxiqCommonsAdvUnique "
													ng-model="datasource.dataAtRestFileType"
													tooltip-placement="right" class="fontSize"
													tooltip='{{explanation["dropDownFileType"]}}' required>
														<option value="">Select file type</option>
												</select>
												</span>
											</div>
										</div>
									</div>
									<br />
									<div class="col-sm-12"
										ng-if="datasource.fileStoreObject == 'HDFS'">
										<label class="col-sm-2" style="margin-left: -10px;"
											ng-if="datasource.dataAtRestFileType!=undefined || datasource.dataAtRestFileType!=null">Compression
											type: </label>
										<div class="col-sm-8" style="margin-left: -27px;">
											<div class="col-sm-3"
												ng-if="datasource.dataAtRestFileType!=undefined || datasource.dataAtRestFileType!=null">
												<span class="ui-select"> <select
													name="compressionType"
													ng-options="maxiqCommonObj.value as maxiqCommonObj.value for maxiqCommonObj in maxiqCommonsAdv"
													ng-model="datasource.dataAtRestCompressionType"
													tooltip-placement="right" class="fontSize"
													tooltip='{{explanation["dropDownCompressionType"]}}'
													required>
														<option value="">Select Compress Type</option>
												</select>
												</span>
											</div>
										</div>
									</div>

									<div class="col-sm-12"
										ng-if="datasource.fileStoreObject != 'HDFS' && datasource.fileStoreObject != 'SOURCE'"
										ng-repeat="index in datasource.primaryKeysForIndex"
										style="margin-bottom: 8px;">
										<div class="col-sm-3" ng-if="$index == 0"
											style="margin-left: -24px;">
											<label class="col-sm-12"> Select primary index : </label>
										</div>
										<div class="col-sm-3" ng-if="$index != 0"
											style="margin-left: -24px;">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-3"
											style="margin-left: -63px; margin-right: -35px;">
											<span class="ui-select"> <select class="disabledPoint"
												name="selectPrimaryKey" ng-disabled="true"
												ng-options="value.fieldName for value in selectRejectValueForPrimary[$index] track by value.fieldName"
												ng-model="index" tooltip-placement="right" tooltip=''
												required>
													<option value="">Select primary key</option>
											</select></span>
										</div>
										<div class="col-md-1">
											<a href="javascript:;" style="line-height: 3;"> <i
												class="fa fa-minus-circle fa-lg"
												ng-click="deletePrimaryValue($index)"></i>
											</a>
										</div>

									</div>

									<div class="col-sm-12"
										ng-if="datasource.fileStoreObject != 'HDFS' && datasource.fileStoreObject != 'SOURCE'"
										style="margin-bottom: 8px;">
										<div class="col-sm-3" style="margin-left: -24px;"
											ng-if="datasource.primaryKeysForIndex.length == 0">
											<label class="col-sm-12">Select primary key: </label>
										</div>
										<div class="col-sm-3" style="margin-left: -24px;"
											ng-if="datasource.primaryKeysForIndex.length != 0">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-3"
											style="margin-left: -64px; margin-right: -35px;">
											<span class="ui-select"> <select
												ng-options="value.fieldName for value in selectRejectValueForPrimary[selectRejectValueForPrimary.length - 1] track by value.fieldName"
												ng-model="hbase.primary" tooltip-placement="right"
												tooltip='' required>
													<option value="">Select primary key</option>
											</select></span>
										</div>
										<div class="col-sm-1">
											<a style="line-height: 3;" href="javascript:;"
												ng-click="addPrimaryKeyValue(hbase.primary, selectRejectValueForPrimary.length - 1)">
												<i class="fa fa-plus-circle fa-lg"></i>
											</a>
										</div>
									</div>

									<div class="col-sm-12"
										ng-if="datasource.fileStoreObject != 'HDFS' && datasource.fileStoreObject != 'SOURCE'"
										ng-repeat="index in datasource.secondaryKeys"
										style="margin-bottom: 8px;">
										<div class="col-sm-3" ng-if="$index == 0"
											style="margin-left: -24px;">
											<label class="col-sm-12"> Add secondary index : </label>
										</div>
										<div class="col-sm-3" ng-if="$index != 0"
											style="margin-left: -24px;">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-3"
											style="margin-left: -63px; margin-right: -35px;">
											<span class="ui-select"> <select class="disabledPoint"
												name="selectSecondaryKey" ng-disabled="true"
												ng-options="value.fieldName for value in selectRejectValueForSecondary[$index] track by value.fieldName"
												ng-model="index" tooltip-placement="right" tooltip=''
												style="cursor: not-allowed;" required>
													<option value="">Select secondary key</option>
											</select></span>
										</div>
										<div class="col-md-1">
											<a href="javascript:;" style="line-height: 3;"> <i
												class="fa fa-minus-circle fa-lg"
												ng-click="deleteSecondaryValue($index)"></i>
											</a>
										</div>

									</div>
									<div class="col-sm-12"
										ng-if="datasource.fileStoreObject != 'HDFS' && datasource.fileStoreObject != 'SOURCE'"
										style="margin-bottom: 8px;">
										<div class="col-sm-3" style="margin-left: -24px;"
											ng-if="datasource.secondaryKeys.length == 0">
											<label class="col-sm-12"> Add secondary index : </label>
										</div>
										<div class="col-sm-3" style="margin-left: -24px;"
											ng-if="datasource.secondaryKeys.length != 0">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-3"
											style="margin-left: -64px; margin-right: -35px;">
											<span class="ui-select"> <select
												name="selectSecondaryKey"
												ng-options="value.fieldName for value in selectRejectValueForSecondary[selectRejectValueForSecondary.length - 1] track by value.fieldName"
												ng-model="hbase.secondary" tooltip-placement="right"
												tooltip='' required>
													<option value="">Select secondary key</option>
											</select></span>
										</div>
										<div class="col-sm-1">
											<a style="line-height: 3;" href="javascript:;"
												ng-click="addSecondaryKeyValue(hbase.secondary, selectRejectValueForSecondary.length - 1)">
												<i class="fa fa-plus-circle fa-lg"></i>
											</a>
										</div>
									</div>
									<div class="col-md-12">&nbsp;</div>
									<div class="col-md-12">
										<label class="ui-checkbox"> <input type="checkbox"
											class="toggle-task" ng-model="datasource.saveForVisualize">
											<span> Save for visualization</span>
										</label>
									</div>
									<div class="col-sm-12 form-group">
										<button type="submit" class="btn btn-primary saveCss"
											ng-click="saveRestFile()">Save</button>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>

				<!-- File Format End -->

				<!-- Profiling Option starts here-->
				<div class="col-md-12 padding-one" ng-hide="true">
					<div class="col-md-12">
						<section class="panel panel-default">
							<div class="panel-heading lan-heading">
								<strong> <span class="glyphicon glyphicon-th"></span>
									Profiling option
								</strong>
							</div>
							<div class="panel-body">
								<div class="col-md-12 noteCss" role="alert">
									<span ng-bind-html='explanation["profilingHeader"] | unsafe'></span>
								</div>
								<div class="form-group">
									<div class="col-md-2" tooltip-placement="right"
										tooltip='{{explanation["profilingCheck"]}}'>
										<div>
											<label class="ui-checkbox"> <input type="checkbox"
												class="toggle-task" ng-model="datasource.preIngestion"
												ng-disabled="datasource.ingection == 'quick' || checkDataType  == 'FIXED_LENGTH_FORMAT'">
												<!-- ng-change="actInactAll('pre' , datasource.preIngestion)" -->
												<!-- <div class="space"></div> --> <span>
													Pre-ingestion</span>
											</label>
										</div>
										<div>
											<label class="ui-checkbox"> <input type="checkbox"
												class="toggle-task" ng-model="datasource.postIngestion">
												<!-- ng-change="actInactAll('post' , datasource.postIngestion)" -->
												<!-- <div class="space"></div> --> <span>Post-ingestion</span>
											</label>
										</div>
									</div>
									<div class="col-md-12">&nbsp;</div>
									<div class="form-group">
										<button type="submit" class="btn btn-primary"
											style="float: right;" ng-click="saveProfilingOption()">Save</button>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
				<!-- <div class="col-md-12"
                    ng-hide="!myTbHide1 && !(activeTab == 'Others')">
                    <section class="panel panel-default">
                         <div class="panel-heading">
                              <strong> <span class="glyphicon glyphicon-th"></span>
                                   Visualization
                              </strong>
                         </div>
                         <div class="panel-body">
                              <div class="col-md-12 noteCss" role="alert">
                                   <span ng-bind-html='explanation["profilingHeader"] | unsafe'></span>
                              </div>
                              <div class="form-group">
                                   <div class="col-md-3" 
                                             tooltip-placement="right">
                                        <div>
                                             <label class="ui-checkbox"> <input type="checkbox"
                                                  class="toggle-task" ng-model="datasource.saveForVisualize">
                                                  <span> Save for visualize</span>
                                             </label>
                                        </div>
                                   </div>
                                   <div class="col-md-3" ng-if='datasource.saveForVisualize' 
                                             tooltip-placement="right">
                                        <div>
                                             <input type="text" class="form-control" id="label-focus"
                                                  ng-model="indexDetails.indexName" placeholder="Index Name" required />
                                        </div>
                                   </div>
                                   <div class="col-md-12">&nbsp;</div>
                                   <div class="form-group">
                                        <button type="submit" class="btn btn-primary"
                                             style="float: right;" ng-click="saveForVisualize()">Save</button>
                                   </div>
                              </div>
                         </div>
                    </section>
               </div> -->

				<div class="col-md-12 padding-one"
					data-ng-controller="connectionController"
					ng-hide="!myTbHide1 && !(activatedSubMenu == 'Connections')">
					<div class="col-md-12">
						<section class="panel panel-default table-dynamic">
							<div class="panel-heading">
								<strong> <span class="glyphicon glyphicon-th"></span>
									Connection
								</strong>
							</div>
							<div class="panel-body">
								<div class="noteCss" role="alert">
									<span ng-bind-html='explanation["connHeader"] | unsafe'></span>
								</div>
								<form id="connectionForm" name="connectionForm"
									class="form-horizontal form-validation" novalidate>
									<fieldset>
										<div class="col-md-12">
											<div class="col-md-8" tooltip-placement="right"
												tooltip='{{explanation["dataSourceType"]}}'>
												<div class="form-group"
													ng-if="dataSourceInstance.flowType !== 'STREAM'">
													<div class="col-md-4">
														<label style="float: left;">Content type</label>
													</div>
													<div class="col-md-8">
														<span class="ui-select"
															style="margin-left: -1%; width: 102%;"> <select
															ng-model="fileDataIngesterDetails.dataSourceType"
															ng-change="myTest(fileDataIngesterDetails.dataSourceType)"
															required>
																<option value=''>Select data source type</option>
																<option value="FILE_DATA_SOURCE">Delimited file</option>
																<option value="RDBMS_DATA_SOURCE">RDBMS</option>
																<option value="TSV_DATA_SOURCE">TSV</option>
																<option value="CSV_DATA_SOURCE">CSV</option>
																<option value="EXCEL_DATA_SOURCE">Excel sheet</option>
																<option value="FIXED_LENGTH_FORMAT">Fixed
																	length format</option>
																<option value="EXTERNAL_DATA">External data</option>
																<option value="FTP_DATA">FTP/SFTP</option>
																<option value="REMOTE_DATA">REMOTE_DATA</option>
														</select></span>
													</div>
												</div>
											</div>
										</div>

										<div class="divider"></div>
										<div class="divider"></div>
										<!-- <div class = "col-md-2">&nbsp;</div> -->
										<div class="col-md-12">
											<div id="firlDataSource" class="something  animate-fade-up"
												ng-show="fileDataIngesterDetails.dataSourceType == 'FILE_DATA_SOURCE' || 
                                             fileDataIngesterDetails.dataSourceType == 'FIXED_LENGTH_FORMAT' || 
                                             fileDataIngesterDetails.dataSourceType == 'CSV_DATA_SOURCE' || 
                                             fileDataIngesterDetails.dataSourceType == 'TSV_DATA_SOURCE' || 
                                             fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'
                                             || fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["serverFilePath"]}}'>
													<div class="form-group">
														<div class="col-md-12">
															<div class="col-md-4" style="padding-left: 0px;">
																<label for="label-focus" style="float: left;">File
																	path</label>
															</div>
															<div class="col-md-8 padding-textBox">
																<input input type="text" style="margin-left: 6px;"
																	class="form-control filePath-textBox" name="filepath"
																	ng-model="fileDataIngesterDetails.serverFilePath"
																	placeholder="File path" id="label-focus"><br>
																<span style="color: red">{{filePathError}}</span>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-8"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
													<div class="col-md-5">&nbsp;</div>
													<div class="col-md-3"
														style="float: left; height: 2px; width: 25%; background-color: rgb(236, 234, 234)">
													</div>
													<span style="float: left; margin-top: -10px;">OR</span>
													<div class="col-md-3"
														style="float: left; height: 2px; width: 25%; background-color: rgb(236, 234, 234)">
													</div>
												</div>
												<div class="col-md-12"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">&nbsp;</div>
												<div class="col-md-8"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
													<div class="form-group">
														<div class="col-md-4">
															<label style="float: left;">Upload file</label>
														</div>
														<div class="col-md-8">
															<input class="btn btn-default btn-block-data" type="file"
																id="uploadFile" title="Choose File" ng-model="myFile"
																ng-file-select="" resetOnClick="true"
																style="width: 102%; margin-left: -3px; padding-right: 210px;"
																ng-change="resetFilePath();" ng-multiple="false"
																ng-model-rejected="rejFiles" />
														</div>
													</div>
												</div>

												<!-- Header Options -->
												<div class="col-md-8" tooltip-placement="right"
													tooltip='{{explanation["containsHeader"]}}'
													ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE' && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA'">
													<div class="form-group">
														<div class="col-md-4">
															<label style="float: left;">Contains header</label>
														</div>
														<div class="col-md-8">
															<label class="ui-radio"> <input id="No"
																type="radio" name="radio"
																ng-model="fileDataIngesterDetails.containsHeader"
																value="false" ng-disabled='dataHide' required> <span>
																	No </span>
															</label> <label class="ui-radio"> <input id="Yes"
																type="radio" name="radio"
																ng-model="fileDataIngesterDetails.containsHeader"
																value="true" ng-disabled='dataHide'> <span>
																	Yes </span>
															</label><span style="color: red"></span>
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-show="fileDataIngesterDetails.containsHeader == 'true' && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE'"
													tooltip='{{explanation["headerStarting"]}}'>
													<div class="form-group">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">Header
																at<span class="field-warning">*</span>
															</label>
														</div>
														<div class="col-md-8">
															<input type="text" class="form-control" id="label-focus"
																ng-model="fileDataIngesterDetails.headerStarting"
																required />
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-show="(fileDataIngesterDetails.containsHeader == 'false' && 
                                                            fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT')"
													tooltip='{{explanation["headerTemplate"]}}'>
													<div class="form-group">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">Header
																template</label>
														</div>
														<div class="col-sm-8"
															style="padding-left: 16px; padding-right: 5px;">
															<textarea id="" class="form-control ng-pristine ng-valid"
																name="headerTemplate" rows="8"
																style="margin-left: -3px;"
																ng-model="fileDataIngesterDetails.headerTemplate"
																placeholder="field1[Integer],field2[String],field3[Date],..."
																required></textarea>
														</div>
													</div>
												</div>
												<div class="col-md-8"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
													<div class="form-group"
														ng-if="fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
														<div class="" ng-repeat="xls in xlsObj">
															<div
																ng-repeat="excel in fileDataIngesterDetails.excelRanges track by $index">
																<div class="col-md-4">
																	<label style="float: left;">Excel range</label>
																</div>
																<div class="row">
																	<div class="col-md-6">
																		<input type="text" class="form-control" ng-trim="true"
																			ng-pattern="/^\S+$/" name="xlsSheetName"
																			placeholder="Sheet name"
																			ng-model="excel.xlsSheetName" />
																	</div>
																</div>
																<div>&nbsp;</div>
																<div class="col-md-4">&nbsp;</div>
																<div class="col-md-2">
																	<input type="text" class="form-control" ng-trim="true"
																		ng-pattern="/^\S+$/" name="xlsFilePathFrom"
																		placeholder="From" ng-model="excel.xlsFilePathFrom" />
																</div>
																<div class="col-md-2">
																	<input type="text" class="form-control" ng-trim="true"
																		ng-pattern="/^\S+$/" name="xlsFilePathTo"
																		placeholder="To" ng-model="excel.xlsFilePathTo" />
																</div>
																<div class="col-md-3">
																	<input type="checkbox" class="toggle-task"
																		ng-model="excel.xlsHeaderOptions"> <label>Has
																		header</label>
																</div>
																<div class="col-md-1">
																	<a style="line-height: 3;" tooltip="Remove"
																		tooltip-placement="right"
																		ng-click="removeXlsField($index)"> <i
																		class="fa fa-trash fa-lg"></i>
																	</a>
																</div>
															</div>
															<div>&nbsp;</div>

															<div class="col-md-4">
																<label style="float: left;">Excel range</label>
															</div>
															<!-- <div class="row"> -->
															<div class="col-md-8"
																style="padding-left: 14px; padding-right: 7px;">
																<input type="text" class="form-control" ng-trim="true"
																	ng-pattern="/^\S+$/" name="xlsSheetName"
																	placeholder="Sheet name" ng-model="values.xlsSheetName" />
															</div>
															<!-- </div> -->
															<div>&nbsp;</div>
															<div class="col-md-4">&nbsp;</div>
															<div class="col-md-2">
																<input type="text" class="form-control" ng-trim="true"
																	ng-pattern="/^\S+$/" name="xlsFilePathFrom"
																	placeholder="From" ng-model="values.xlsFilePathFrom" />
															</div>
															<div class="col-md-2">
																<input type="text" class="form-control" ng-trim="true"
																	ng-pattern="/^\S+$/" name="xlsFilePathTo"
																	placeholder="To" ng-model="values.xlsFilePathTo" />
															</div>
															<div class="col-md-3">
																<input type="checkbox" class="toggle-task"
																	ng-model="values.headerChkBox"> <label>Has
																	header</label>
															</div>
															<div class="col-md-1">
																<a tooltip="Add" href="javascript:;"
																	tooltip-placement="right" ng-click="addXlsField()">
																	<i class="fa fa-plus-circle fa-lg"></i>
																</a>
															</div>
															<div cass="col-md-1">
																<a style="line-height: 3;" href="javascript:;"
																	ng-if="xlsObj.length>1" ng-click="removeXlsField(xls)">
																	<i class="fa fa-trash fa-lg"></i>
																</a>
															</div>
														</div>
														<span style="color: red">{{xlsPathError}}</span>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["containsHeader"]}}'>
													<div class="form-group"
														ng-show="fileDataIngesterDetails.containsHeader == 'false' && 
                                                                 fileDataIngesterDetails.dataSourceType == 'FIXED_LENGTH_FORMAT'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">Header
																template</label>
														</div>
														<div class="col-sm-8"
															style="padding-left: 11px !important; padding-right: 7px !important;">
															<textarea id="" class="form-control ng-pristine ng-valid"
																name="headerTemplate" rows="4"
																ng-model="fileDataIngesterDetails.headerTemplate"
																placeholder="field1[Integer][length1],field2[String][length2],field3[Date][length3],..."
																required></textarea>
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["recordStarts"]}}'>
													<div class="form-group"
														ng-hide="dataHide || fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
														<div class="col-md-4">
															<label style="float: left;">Records starts</label>
														</div>
														<div class="col-md-8">
															<label class="ui-radio"> <input id="Line"
																type="radio" name="radio2"
																ng-model="fileDataIngesterDetails.recordStartsWith"
																value="false" required> <span> Line </span>
															</label> <label class="ui-radio"> <input id="RegExpre"
																type="radio" name="radio2"
																ng-model="fileDataIngesterDetails.recordStartsWith"
																value="true"> <span> Regular expression </span>
															</label><span style="color: red"></span>
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["recordStartLine"]}}'>
													<div class="form-group"
														ng-show="fileDataIngesterDetails.recordStartsWith == 'false' && !dataHide && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;"> At
																line#<span class="field-warning">*</span>
															</label>
														</div>
														<div class="col-md-8 textboxCss">
															<input type="text" class="form-control" id="label-focus"
																ng-model="fileDataIngesterDetails.recordStartLine">
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-show="(fileDataIngesterDetails.recordStartsWith == 'true' || !'dataHide' ) 
                                                  && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE' && fileDataIngesterDetails.dataSourceType !== 'REMOTE_DATA'"
													tooltip='{{explanation["startingRegularExpre"]}}'>
													<div class="form-group">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">
																Regular expression</label>
														</div>
														<div class="col-md-8">
															<input type="text" class="form-control" id="label-focus"
																ng-model="fileDataIngesterDetails.startingRegularExpre"
																required>

														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["recordsIgnore"]}}'>
													<div class="form-group"
														ng-hide="dataHide || fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">
																Records to ignore(Reg ex) </label>
														</div>
														<div class="col-md-8 textboxCss">
															<input type="text" class="form-control" id="label-focus"
																name="recordtoIgnore"
																ng-model="fileDataIngesterDetails.recordsIgnore"
																required>
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["recordLoadLimit"]}}'>
													<div class="form-group"
														ng-hide="dataHide || fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">
																Records load limit</label>
														</div>
														<div class="col-md-8 textboxCss">
															<input type="text" class="form-control" id="label-focus"
																name="recordLimit"
																ng-model="fileDataIngesterDetails.recordLoadLimit"
																required>
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["delimiter"]}}'>
													<div class="form-group"
														ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' && 
                                                            fileDataIngesterDetails.dataSourceType != 'TSV_DATA_SOURCE' && 
                                                            fileDataIngesterDetails.dataSourceType != 'CSV_DATA_SOURCE'
                                                            && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">Delimiter<span
																class="field-warning">*</span></label>
														</div>
														<div class="col-md-8 textboxCss">
															<input type="text" class="form-control" name="delimiter"
																placeholder="Delimiter"
																ng-model="fileDataIngesterDetails.delimiter" required>
															<span style="color: red">{{delimiterError}}</span>
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["delimiter"]}}'>
													<div class="form-group"
														ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' && 
                                                            fileDataIngesterDetails.dataSourceType == 'CSV_DATA_SOURCE'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">Delimiter<span
																class="field-warning">*</span></label>
														</div>
														<div class="col-md-8 textboxCss"
															ng-init="fileDataIngesterDetails.delimiter = ','">
															<input type="text" class="form-control" name="delimiter"
																placeholder="Delimiter"
																ng-model="fileDataIngesterDetails.delimiter"
																ng-disabled="true" required> <span
																style="color: red">{{delimiterError}}</span>
														</div>
													</div>
												</div>
												<div class="col-md-8" tooltip-placement="right"
													ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
													tooltip='{{explanation["delimiter"]}}'>
													<div class="form-group"
														ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' && 
                                                            fileDataIngesterDetails.dataSourceType == 'TSV_DATA_SOURCE'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">Delimeter<span
																class="field-warning">*</span></label>
														</div>
														<div class="col-md-8 textboxCss"
															ng-init="fileDataIngesterDetails.delimiter = '\\t'">
															<input type="text" class="form-control" name="delimiter"
																placeholder="Delimiter"
																ng-model="fileDataIngesterDetails.delimiter"
																ng-disabled="true" required> <span
																style="color: red">{{delimiterError}}</span>
														</div>
													</div>
												</div>
												<div class="col-md-8 form-group"
													tooltip='{{explanation["optionallyEnclosedInDoubleQuotes"]}}'
													tooltip-placement="right"
													ng-if="'EXCEL_DATA_SOURCE' != fileDataIngesterDetails.dataSourceType && 'FIXED_LENGTH_FORMAT' != fileDataIngesterDetails.dataSourceType && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA' && 'RDBMS_DATA_SOURCE' != fileDataIngesterDetails.dataSourceType || fileDataIngesterDetails.dataSourceType == 'FTP_DATA'">
													<div class="col-md-4"></div>
													<div class="col-md-8 checkBoxAlignment">
														<label class="ui-checkbox"> <input type="checkbox"
															class="toggle-task"
															ng-model="fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes">
															<span>Optionally enclosed in double quotes ?</span></label>
													</div>
												</div>
												<div class="col-md-8 form-group" tooltip-placement="right"
													ng-if="fileDataIngesterDetails.dataSourceType == 'FILE_DATA_SOURCE' && !dataHide && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA' && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE'"
													tooltip='{{explanation["isDelimete"]}}'>
													<div class="col-md-4"></div>
													<div class="col-md-8 checkBoxAlignment">
														<label class="ui-checkbox"> <input type="checkbox"
															class="toggle-task"
															ng-model="fileDataIngesterDetails.isDelimiterRegEx">
															<span>Is delimiter a regular expression ?</span></label>
													</div>
												</div>
											</div>
										</div>

										<div class="col-md-8"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
											<div id="rdbmsDataSource" class="something2 animate-fade-up"
												ng-show="fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA' ">
												<div class="col-md-12">
													<div class="form-group">
														<label class="col-sm-4">External connection type </label>
														<div class="col-sm-8">
															<span class="ui-select"
																style="margin-left: -1px; width: 103.6%;"> <select
																name="connectorType"
																ng-model="datasource.externalDataType"
																ng-change="getExternalConnecton(datasource.externalDataType)"
																tooltip-placement="right" tooltip='' required>
																	<option value="">Select connection Type</option>
																	<option value="SALESFORCE">SALESFORCE</option>
																	<option value="AMAZONS3">AMAZONS3</option>
																	<option value="FACEBOOKPAGE">FACEBOOK PAGE</option>
																	<option value="DROPBOX">DROPBOX</option>
																	<option value="HACKYL">HACKYL</option>
															</select>
															</span>
														</div>
													</div>
													<!-- datasource.externalDataType -->
													<div class="form-group">
														<label class="col-sm-4">External connection</label>
														<div class="col-sm-8">
															<span class="ui-select"
																style="margin-left: -1px; width: 103.6%;"> <select
																name="datasource.config"
																ng-options="connection.conncetionName for connection in externalConnections track by connection.conncetionName"
																ng-model="datasource.config" tooltip-placement="right"
																tooltip='' required>
																	<option value="">Select external connection</option>
															</select>
															</span>
														</div>
													</div>

												</div>
											</div>
										</div>
										<div class="col-md-8">
											<div id="rdbmsDataSource" class="something2 animate-fade-up"
												ng-if="dataSourceInstance.flowType === 'STREAM'">
												<div class="form-group">
													<div class="col-md-4">
														<label style="float: left;">Content type</label>
													</div>
													<div class="col-md-8">
														<span class="ui-select"
															style="margin-left: 0%; width: 103%;"> <select
															ng-model="fileDataIngesterDetails.dataSourceType"
															required>
																<option value=''>Select data source type</option>
																<option value='STREAM_DS'>STREAM_DS</option>
														</select></span>
													</div>
												</div>
												<div class="form-group"
													ng-if="fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
													<div class="col-md-4">
														<label style="float: left;">Stream type</label>
													</div>
													<div class="col-md-8">
														<span class="ui-select"
															style="margin-left: 0%; width: 103%;"> <select
															name="connectorType"
															ng-options="connection for connection in connectionsDetails 
                                                                      track by connection"
															ng-change="selectTheStreamType(datasource.streamType)"
															ng-model="datasource.streamType" required>
																<option value="">Select stream type</option>
														</select>
														</span>
													</div>
												</div>
												<div class="form-group"
													ng-if="datasource.streamType != null && datasource.streamType != undefined
                                                       && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
													<div class="col-md-4">
														<label style="float: left;">Connection name</label>
													</div>
													<div class="col-md-8">
														<span class="ui-select"
															style="margin-left: 0%; width: 103%;"> <select
															name="connectorName"
															ng-options="connection.conncetionName for connection in connectionsNameDetails 
                                                                 track by connection.conncetionName"
															ng-change="setConncetionName(connObj.connectionName)"
															ng-model="datasource.config" tooltip-placement="right"
															tooltip='{{explanation["dropDownForConnection"]}}'
															required>
																<option value="">Select connection</option>
														</select>
														</span>
													</div>
												</div>
												<!-- <div class="form-group"
                                                       ng-if="datasource.config !== '' 
                                                  && datasource.config !== undefined && datasource.config !== null
                                                  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
                                                       <div class="col-md-4">
                                                            <label style="float: left;">IP</label>
                                                       </div>
                                                       <div class="col-md-8">{{datasource.config.ip}}</div>
                                                  </div>
                                                  <div class="form-group"
                                                       ng-if="datasource.config !== '' 
                                                  && datasource.config !== undefined && datasource.config !== null
                                                  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
                                                       <div class="col-md-4">
                                                            <label style="float: left;">Port</label>
                                                       </div>
                                                       <div class="col-md-8">{{datasource.config.port}}</div>
                                                  </div> -->
												<div class="form-group"
													ng-if="datasource.config !== '' 
                                                  && datasource.config !== undefined && datasource.config !== null
                                                  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
													<div class="col-md-4">
														<label style="float: left;">URL</label>
													</div>
													<div class="col-md-8">{{datasource.config.url}}</div>
												</div>
												<div class="form-group"
													ng-if="datasource.config !== '' 
                                                  && datasource.config !== undefined && datasource.config !== null
                                                  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
													<div class="col-md-4">
														<label for="label-focus" style="float: left;">Header
															template</label>
													</div>
													<div class="col-sm-8"
														style="padding-right: 1px !important;">
														<textarea id="" class="form-control ng-pristine ng-valid"
															name="headerTemplate" rows="4"
															ng-model="fileDataIngesterDetails.headerTemplate"
															placeholder="field1[Integer],field2[String],field3[Date],..."
															required></textarea>
													</div>
												</div>
												<div class="form-group"
													ng-if="datasource.config !== '' 
                                                  && datasource.config !== undefined && datasource.config !== null
                                                  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
													<label class="col-sm-4">Topic name </label>
													<div class="col-sm-8 rdbmsCss">
														<input type="text" id=""
															class="form-control ng-pristine ng-valid topic-css"
															name="primaryKey" rows="4"
															ng-model="fileDataIngesterDetails.topicName"
															placeholder="Enter topic name" required />
													</div>
												</div>


												<div class="form-group"
													ng-if="datasource.config !== '' 
                                                  && datasource.config !== undefined && datasource.config !== null
                                                  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
													<div class="col-md-4">
														<label for="label-focus" style="float: left;">Delimeter<span
															class="field-warning">*</span></label>
													</div>
													<div class="col-md-8 textboxCss">
														<input type="text" class="form-control delimiter-css"
															name="delimiter" placeholder="Delimiter"
															ng-model="fileDataIngesterDetails.delimiter"
															ng-disabled="false" required> <span
															style="color: red">{{delimiterError}}</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-8">
											<div id="rdbmsDataSource" class="something2 animate-fade-up"
												ng-show="fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE' || fileDataIngesterDetails.dataSourceType == 'FTP_DATA'">
												<div class="col-md-12">
													<div class="form-group">
														<label class="col-sm-4">Connection </label>
														<div class="col-sm-8">
															<span class="ui-select"
																style="margin-left: 0%; width: 103%;"> <select
																name="connectorType"
																ng-options="connection.conncetionName for connection in connectionsDetails track by connection.conncetionName"
																ng-model="datasource.config" tooltip-placement="right"
																tooltip='{{explanation["dropDownForConnection"]}}'
																required>
																	<option value="">Select connection</option>
															</select>
															</span>
														</div>
													</div>

													<!-- File Format Start -->

													<div class="form-group"
														ng-if="fileDataIngesterDetails.dataSourceType != 'FTP_DATA'">
														<label class="col-sm-4">File format </label>
														<div class="col-sm-4" style="padding-left: 1.7%;">
															<span class="ui-select"> <select name="fileType"
																ng-options="maxiqCommonObj.keyOf as maxiqCommonObj.keyOf for maxiqCommonObj in maxiqCommonsUnique"
																ng-model="datasource.config.inputFileType"
																tooltip-placement="right"
																tooltip='{{explanation["dropDownForFileType"]}}'
																ng-disabled='true' required>
																	<option value="">Select file type</option>
															</select>
															</span>
														</div>
														<div class="col-sm-4"
															ng-if="datasource.config.inputFileType!=undefined || datasource.config.inputFileType!=null"
															style="padding-left: 4%;">
															<span class="ui-select"> <select
																name="compressionType"
																ng-options="maxiqCommonObj.value as maxiqCommonObj.value for maxiqCommonObj in maxiqCommons | filter:datasource.config.inputFileType"
																ng-model="datasource.config.inputCompressionType"
																tooltip-placement="right"
																tooltip='{{explanation["dropDownForCompression"]}}'
																ng-disabled='true' required>
																	<option value="">Select compress type</option>
															</select>
															</span>
														</div>
													</div>

													<!-- File Format End -->

													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
														<label class="col-sm-4">Connector type </label>
														<div class="col-sm-8">
															{{datasource.config.connectorType}}</div>
													</div>
													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
														<label class="col-sm-4">IP </label>
														<div class="col-sm-8">{{datasource.config.ip}}</div>
													</div>
													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
														<label class="col-sm-4">Port </label>
														<div class="col-sm-8">{{datasource.config.port}}</div>
													</div>

													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
														<label class="col-sm-4">User name </label>
														<div class="col-sm-8">
															{{datasource.config.dbUserName}}</div>
													</div>

													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'>
														<label class="col-sm-4">DB name </label>
														<div class="col-sm-8">{{datasource.config.dbName}}</div>
													</div>

													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && fileDataIngesterDetails.dataSourceType == "FTP_DATA"'>
														<label class="col-sm-4">Default location </label>
														<div class="col-sm-8 textboxCss">
															<input type="text" class="form-control"
																name="defaultLocation" placeholder="File Path"
																ng-model="datasource.config.defaultLocationForFTP"
																required>
														</div>
													</div>
													<div class="form-group" tooltip-placement="right"
														tooltip='{{explanation["delimiter"]}}'
														ng-if="datasource.config != null && datasource.config != undefined && fileDataIngesterDetails.dataSourceType == 'FTP_DATA'">
														<div class="col-md-4">
															<label for="label-focus" style="float: left;">Delimeter<span
																class="field-warning">*</span></label>
														</div>
														<div class="col-md-8 textboxCss">
															<input type="text" class="form-control" name="delimiter"
																placeholder="Delimiter"
																ng-model="fileDataIngesterDetails.delimiter"
																ng-disabled="false" required> <span
																style="color: red">{{delimiterError}}</span>
														</div>
													</div>

													<div class="col-md-12 form-group" tooltip-placement="right"
														ng-if="fileDataIngesterDetails.dataSourceType == 'FTP_DATA' && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA'"
														tooltip='{{explanation["optionallyEnclosedInDoubleQuotes"]}}'>
														<div class="col-md-4"></div>
														<div class="col-md-8">
															<label class="ui-checkbox"> <input
																type="checkbox" class="toggle-task"
																ng-model="datasource.config.optionallyEnclosedInDoubleQuotes">
																<span>Optionally enclosed in double quotes ?</span></label>
														</div>
													</div>

													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'>
														<label class="col-sm-4">Split key </label>
														<div class="col-sm-8 rdbmsCss">
															<input type="text" id=""
																class="form-control ng-pristine ng-valid"
																name="primaryKey" rows="4"
																ng-model="datasource.config.dbPrimaryKey"
																placeholder="Enter primary key" required />
														</div>
													</div>

													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'>
														<div class="col-sm-4"></div>
														<div class="col-sm-8" style="margin-left: 35.5%;">
															<button
																style="float: right; margin-bottom: 3px; margin-top: -5px ! important;"
																ng-click="fullTextarea('customQuery');"
																class="btn btn-default btn-fullscreen ng-scope"
																type="button" tooltip="Fullscreen"
																tooltip-placement="top" id="fullscreen-button">
																<i class="fa fa-arrows-alt"></i>
															</button>
															<button
																style="float: right; margin-right: 2px ! important; margin-top: -5px;"
																ng-click="resizeText(4);"
																class="btn btn-default btn-textarea ng-scope"
																type="button" tooltip="Grow Result"
																tooltip-placement="top">
																<i class="fa fa-plus"></i>
															</button>
															<button
																style="float: right; margin-right: 2px ! important; margin-top: -5px;"
																ng-click="sizeChanger(-4);"
																class="btn btn-default btn-minus ng-scope" type="button"
																tooltip="Shrink Result" tooltip-placement="top">
																<i class="fa fa-minus"></i>
															</button>
														</div>
													</div>
													<div class="form-group"
														ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'
														style="margin-top: -16px !important;">
														<label class="col-sm-4">Query </label>
														<div class="col-sm-8"
															style="padding-left: 12px; padding-right: 0px;">
															<textarea id="rdbmsNode" style="height: {{myCssApply"
																class="form-control ng-pristine ng-valid" name="query"
																rows='{{rowSize}}'
																ng-model="datasource.config.queryToExecute"
																placeholder="Write your query here" required></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- <div class = "col-md-2">&nbsp;</div> -->

										<div class="form-group col-md-9"
											ng-show="fileDataIngesterDetails.dataSourceType != ''
                                                  || fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
											<div
												class="pull-right {{fileDataIngesterDetails.dataSourceType == 'STREAM_DS' ? 'stream-css': 'all-css'}}"
												ng-class="{'col-sm-offset-8 col-sm-4': datasource.dataSourceType == 'RDBMS_DATA_SOURCE',
                                                                 'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'FILE_DATA_SOURCE',
                                                                 'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'CSV_DATA_SOURCE',
                                                                 'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'TSV_DATA_SOURCE',
                                                                 'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'EXCEL_DATA_SOURCE',
                                                                 'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'EXTERNAL_DATA',
                                                                 'col-sm-offset-8 col-sm-4' : fileDataIngesterDetails.dataSourceType == 'STREAM_DS',
                                                                 'col-sm-offset-8 col-sm-4' : fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'
                                                                 }">
												<button type="button"
													ng-if="fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE'
                                                       || fileDataIngesterDetails.dataSourceType == 'STREAM_DS'"
													class="btn btn-default btn-block-data"
													ng-click="validateRdbmsConnection()">Test</button>
												<div class="space"></div>
												<button type="button" class="btn btn-default btn-block-data"
													ng-click="clearFileDataSource()">Clear</button>
												<div class="space"></div>
												<!--  -->
												<button type="submit" class="btn btn-primary"
													ng-if="fileDataIngesterDetails.dataSourceType != 'EXTERNAL_DATA'
                                                            || fileDataIngesterDetails.dataSourceType == 'STREAM_DS'"
													ng-click="inputParamCall('connection')">Save</button>
												<button type="submit"
													ng-if="fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA'"
													class="btn btn-primary"
													ng-click="configured(datasource.externalDataType,datasource.config)">Configure</button>
											</div>
										</div>
										<div class="col-md-2">&nbsp;</div>
									</fieldset>
								</form>
							</div>
						</section>
					</div>
				</div>

				<div class="col-md-12 padding-one"
					ng-hide="!myTbHide1 && !(activatedSubMenu == 'Connection') || (appId != undefined && !popUpValue)">
					<div class="col-md-12">
						<section class="panel panel-default table-dynamic">
							<div class="panel-heading">
								<strong> <span class="glyphicon glyphicon-th"></span>
									Data ingestion
								</strong>
							</div>
							<div class="panel-body" style="min-height: 112px;">
								<div class="col-md-12 noteCss" role="alert">
									<span ng-bind-html='explanation["ingestion"] | unsafe'></span>
								</div>
								<div class="col-md-2">
									<label>Data ingestion method</label>
								</div>
								<div class="col-md-9" ng-init="datasource.user=2;">
									<label class="ui-radio" tooltip-placement="top"
										tooltip='{{explanation["advance"]}}'> <input
										name="radio15" ng-model="datasource.ingection" type="radio"
										value="typical"> <span> Advance </span>
									</label> <label class="ui-radio" tooltip-placement="top"
										tooltip='{{explanation["basic"]}}'> <input
										name="radio15" ng-model="datasource.ingection" type="radio"
										value="quick"> <span> Basic </span>
									</label> <span style="color: red"></span>
								</div>
								<div class="form-group">
									<button ng-click="saveDataIngestion()"
										style="float: right; margin-right: 0px"
										class="btn btn-primary" type="submit">Save</button>
								</div>
							</div>
						</section>
					</div>
				</div>
				<div class="col-md-12 padding-one"
					ng-hide='(((hiddenIndicatorForRdbms.indicator) && dataHide || (!myTbHide1 && !(activatedSubMenu  == "Connection"))))'>
					<div data-ng-include="'views/template/inputParameters.jsp'"></div>
				</div>
				<!-- Data tab code start -->
				<div class="col-md-12 padding-one"
					ng-hide="!(activatedSubMenu == 'Fields')">
					<div class="col-md-12">
						<section class="panel panel-default"
							style="overflow-y: auto; max-height: 500px;">
							<div class="panel-heading lan-heading" style="height: 40px;">
								<div class="panel-bttn" style="padding-left: 25px;">
									<button type="button" class="btn btn-primary"
										tooltip-placement="bottom" class="btn btn-new"
										tooltip='{{explanation["addEditField"]}}'
										ng-if="modelCheck == undefined || modelCheck == null || modelCheck == ''"
										ng-click="AddFieldMapping()" ng-disabled='dataHide'>Add/edit
										new fields</button>
									<button type="button" class="btn btn-new"
										tooltip-placement="bottom"
										tooltip='{{explanation["saveAllField"]}}'
										ng-if="modelCheck == undefined || modelCheck == null || modelCheck == ''"
										ng-click="saveFieldMapping(datasource.fieldMappings)"
										text-align="right">Save all</button>
									<button type="button" class="btn btn-new"
										tooltip-placement="bottom" class="btn btn-default"
										tooltip='{{explanation["addEditField"]}}'
										ng-if="modelCheck != undefined && modelCheck != null && modelCheck != ''"
										ng-click="AddFieldMapping()" ng-disabled='dataHide'>Add/edit
										new fields</button>
									<button type="button" class="btn btn-new"
										tooltip-placement="bottom"
										tooltip='{{explanation["saveAllField"]}}'
										ng-if="modelCheck != undefined && modelCheck != null && modelCheck != ''"
										ng-click="saveFieldMapping(datasource.fieldMappings)"
										text-align="right">Save all</button>
								</div>
							</div>
							<div class="panel-body">
								<div class="dsFont" role="alert">
									<span ng-bind-html='explanation["fieldMapping"] | unsafe'></span>
								</div>
								<br>
								<div class="row">
									<div class="col-md-2">
										<span class="ui-select"
											style="height: 34px; line-height: 15px; width: 100%;">
											<select class="dropdown-width" ng-model="datasource.category"
											ng-options="category for category in categories"
											ng-change="changeCategory($index,datasource.category)">
												<option value="">Select category type</option>
										</select>
										</span>
									</div>
									<div class="col-md-2">
										<span class="ui-select"
											style="height: 34px; line-height: 15px; width: 100%;">
											<select class="dropdown-width"
											ng-model="datasource.subCategory"
											ng-options="category for category in categoriesDetails[datasource.category]"
											ng-change="changeSubCategory($index,datasource.subCategory)">
												<option value="">Select sub-category type</option>
										</select>
										</span>
									</div>
									<div class="col-md-4">
										<textarea class="form-control" rows="1" cols="20"
											placeholder="Description as data source"
											ng-model="datasource.description"></textarea>
									</div>
									<div class="col-md-4">
										<ui-select multiple ng-model="datasource.tags"
											theme="bootstrap" class="select-formatter"
											style="width: 100%;"> <ui-select-match
											placeholder="Tags">{{$item}}</ui-select-match> <ui-select-choices
											repeat="tag in tagNames | filter:$select.search ">
										{{tag}} </ui-select-choices> </ui-select>
									</div>
								</div>
								<div class="col-md-12" style="margin-bottom: 15px;">&nbsp;</div>
								<div class="table-dynamic">
									<table
										class="table table-bordered table-striped table-responsive table-repo pad-popup">
										<thead>
											<tr ng-hide="datasource.fieldMappings.length == 0">
												<th ng-hide="datasource.fieldMappings.length == 0"><div
														class="th">#</div></th>
												<th><div class="th">Field name</div></th>
												<th><div class="th">Data type</div></th>
												<th ng-init='check = []'><div class="th">Format</div></th>
												<th><div class="th">Glossary</div></th>
												<th ng-init='check = []'><div class="th">Precision
														and Scale</div></th>
												<th><div class="th">Description</div></th>
												<th><div class="th">Key</div></th>
												<th><div class="th">Validation</div></th>
												<th><div class="th">Transformation</div></th>
												<th><div class="th">Advanced profile</div></th>
												<th><div class="th">Value frequency</div></th>
												<th><div class="th">PII</div></th>
											</tr>
										</thead>
										<tbody>
											<tr
												ng-repeat="fm in datasource.fieldMappings track by $index">
												<td>{{$index + 1}}</td>
												<td width="15%;"><span> <span></span> <input
														ng-pattern="/^[a-zA-Z]{1,}[a-zA-Z0-9_]{0,}$/" type="text"
														name="fieldName" class="form-control" ng-trim="true"
														ng-disabled="(fm.addVarIndicator == true)"
														ng-model="fm.fieldName" style="height: 34px;" required>
												</span></td>
												<td width="10%;"
													ng-hide="datasource.fieldMappings.length == 0"><span
													class="ui-select" style="height: 34px; line-height: 15px;">
														<select class="dropdown-width" ng-model="fm.fieldDataType"
														ng-disabled="(fm.addVarIndicator == true)"
														ng-change="activateFormat($index , fm.fieldDataType)">
															<option value="">Select data type</option>
															<option value="STRING">STRING</option>
															<!-- <option value="NUMERIC">NUMERIC</option> -->
															<option value="INTEGER">INTEGER</option>
															<option value="LONG">LONG</option>
															<option value="DECIMAL">DECIMAL</option>
															<option value="DATE">DATE</option>
															<option value="BOOLEAN">BOOLEAN</option>
															<option value="DOUBLE">DOUBLE</option>
															<option value="TIMESTAMP">TIMESTAMP</option>
													</select>
												</span></td>
												<td width="5%;"><input type="text" class="form-control"
													ng-disabled='!(fm.fieldDataType == "DATE" || fm.fieldDataType == "TIMESTAMP")'
													ng-model="fm.format" style="height: 34px;" disabled>
												</td>
												<td><ui-select multiple ng-model="fm.tags"
														theme="bootstrap" class="select-formatter"
														style="width: 100%;"> <ui-select-match
														placeholder="Tags">{{$item}}</ui-select-match> <ui-select-choices
														repeat="tag in tagNames | filter:$select.search ">
													{{tag}} </ui-select-choices> </ui-select></td>
												<td>
													<div ng-if='(fm.fieldDataType == "DECIMAL")'
														class="dataFieldMappingPrecision">
														<input type="number" class="form-control"
															ng-model="fm.precision" min="10" max="38" /> <span
															class="fL">,</span> <input type="number"
															class="form-control" ng-model="fm.scale" min="0" max="38" />
													</div>
												</td>
												<td width="10%"><textarea class="form-control" cols="2"
														rows="1" ng-model="fm.descriptions"
														placeholder="Description"></textarea></td>
												<td><label class="ui-checkbox"> <input
														type="checkbox" ng-model="fm.key"> <span></span>
												</label></td>
												<td><label class="ui-checkbox"> <input
														ng-hide='fm.addVarIndicator' name="validationChkBox"
														type="checkbox" value="option2"
														class="toggle-task pull-left"
														ng-model="validationChkBox[$index]" disabled="disabled">
														<span></span> <span ng-hide='fm.addVarIndicator'
														href="javascript:;" ng-click="open(fm)"
														class="glyphicon glyphicon-pencil"></span>
												</label></td>
												<td><label class="ui-checkbox"> <input
														ng-hide='fm.addVarIndicator' class="toggle-task pull-left"
														name="transformationChkbox" type="checkbox"
														value="option1" ng-model="transformationChkBox[$index]"
														disabled="disabled"> <span></span> <span
														ng-hide='fm.addVarIndicator' href="javascript:;"
														ng-click="openTransformation(fm)"
														class="glyphicon glyphicon-pencil"> </span>
												</label></td>
												<td width="30%;"><label class="ui-checkbox"> <input
														type="checkbox"
														ng-disabled='!((fm.fieldDataType == "INTEGER" || fm.fieldDataType == "LONG" || fm.fieldDataType == "DOUBLE") && disableCheck.preMyIndicator) || fm.addVarIndicator == true'
														ng-change="injunctionChange('pre' , $index , distinctProfilePreChkBox[$index])"
														name="distinctProfilePreChkBox[$index]"
														class="toggle-task"
														ng-model="distinctProfilePreChkBox[$index]"
														value="option1"> <span>Pre</span>
												</label> <label class="ui-checkbox"> <input type="checkbox"
														ng-disabled='!((fm.fieldDataType == "INTEGER" || fm.fieldDataType == "LONG" || fm.fieldDataType == "DOUBLE") && disableCheck.postMyIndicator) || fm.addVarIndicator == true'
														ng-change="injunctionChange('post' , $index , distinctProfilePostChkBox[$index])"
														name="distinctProfilePostChkBox[$index]"
														class="toggle-task"
														ng-model="distinctProfilePostChkBox[$index]"
														value="option2"> <span> Post</span>
												</label></td>
												<td width="30%;"><label class="ui-checkbox"> <input
														type="checkbox" ng-disabled='!disableCheck.preMyIndicator'
														ng-change="inValueFreqencyChange('pre' , $index, distinctPreValueFreqChkBox[$index])"
														name="distinctPreValueFreqChkBox[$index]"
														class="toggle-task"
														ng-model="distinctPreValueFreqChkBox[$index]"
														value="option1"> <span>Pre</span>
												</label> <label class="ui-checkbox"> <input type="checkbox"
														ng-disabled='!disableCheck.postMyIndicator'
														ng-change="inValueFreqencyChange('post' , $index , distinctPostValueFreqChkBox[$index])"
														name="distinctPostValueFreqChkBox[$index]"
														class="toggle-task"
														ng-model="distinctPostValueFreqChkBox[$index]"
														value="option2"> <span> Post </span>
												</label></td>
												<td><label class="ui-checkbox"> <input
														type="checkbox" ng-model="fm.pIIId"> <span></span>
												</label></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>
				<!-- Data tab code end -->
				<div class="col-md-12 padding-one"
					ng-hide="!myTbHide1 && !(activatedSubMenu == 'Sample')">
					<div class="col-md-12">
						<section class="panel panel-default"
							style="overflow: auto; max-width: 1127px;">
							<div class="panel-heading lan-heading" style="height: 36px;"></div>
							<div class="panel-body"
								ng-if="datasource.fieldMappings.length > 0">
								<div class="dsFont" role="alert">
									<span ng-bind-html='explanation["dataTab"] | unsafe'></span>
								</div>
								<br />
								<div class="table-dynamic" style="padding-top: 15px;">
									<table
										class="table table-bordered table-striped table-responsive table-repo pad-popup">
										<thead>
											<tr ng-hide="datasource.fieldMappings.length == 0">
												<th><div class="th">#</div></th>
												<th ng-repeat="fm in datasource.fieldMappings"><div
														class="th">{{fm.fieldName}}</div></th>
											</tr>
										</thead>

										<tbody>
											<tr>
												<td>1</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord1 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord1}}</td>
											</tr>
											<tr>
												<td>2</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord2 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord2}}</td>
											</tr>
											<tr>
												<td>3</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord3 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord3}}</td>
											</tr>
											<tr>
												<td>4</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord4 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord4}}</td>
											</tr>
											<tr>
												<td>5</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord5 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord5}}</td>
											</tr>
											<tr>
												<td>6</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord6 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord6}}</td>
											</tr>
											<tr>
												<td>7</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord7 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord7}}</td>
											</tr>
											<tr>
												<td>8</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord7 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord8}}</td>
											</tr>
											<tr>
												<td>9</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord7 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord9}}</td>
											</tr>
											<tr>
												<td>10</td>
												<td ng-repeat="fm in datasource.fieldMappings">{{(fm.pIIId
													== true || fm.pIIId == "true") && fm.sampleRecord7 != null
													&& appId == 0 ? 'xxxxxxxxxx' : fm.sampleRecord10}}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>

				<!-- <div class="col-md-6">
               <div data-ng-include="'views/template/inputParameters.jsp'"></div>              
          </div> -->
				<!-- <div class="col-md-12"
                    ng-hide="!myTbHide1 && !(activeTab == 'Others')">
                    <section class="panel panel-default" ng-hide='dataHide'
                         data-ng-controller="advancedValidationController">
                         <div class="panel-heading">
                              <span class="glyphicon glyphicon-th"></span> Advanced Validation
                              <div class="panel-bttn">
                                   <div ng-controller="createNewAdvancedValidation">

                                        <button type="button" class="btn btn-danger btn-block repo-btn"
                                             ng-click="addValidation()" style="padding-top: 2px;">Add new validation</button>
                                   </div>
                              </div>
                         </div>
                         <div class="panel-body" style="min-height: 112px;">
                              <div class="col-md-12 noteCss" role="alert">
                                   <span ng-bind-html='explanation["addAdvanceVal"] | unsafe'></span>
                              </div>
                              <accordion close-others="oneAtATime" class="ui-accordion"
                                   ng-init="validators = []"> <accordion-group
                                   heading="{{validator.title}}" is-open="oneAtATime"
                                   ng-repeat="validator in validators track by $index">
                              <div class="row"
                                   ng-repeat="selectField in  validator.selectFields track by $index">
                                   <div class="col-md-10">
                                        <span class="ui-select" style="width: 100%;"> <select
                                             ng-model="validator.selectFields[$index]"
                                             ng-options="fm as fm.fieldName for fm in validator.dropdown[$index]"
                                             ng-change="changingSelect(validator.selectFields[$index], $parent.$index, $index)">
                                                  <option value="">--Select Field--</option>
                                        </select>
                                        </span>
                                   </div>
                                   <div class="col-md-1">
                                        <a href="javascript:;" style="line-height: 3;"
                                             ng-click="addSelect(validator.selectFields[$index] ,$parent.$index, $index)">
                                             <i class="fa fa-plus-circle fa-lg selectedCurser"></i>
                                        </a> <a href="javascript:;" style="line-height: 3;"
                                             ng-hide='$index == validator.selectFields.length - 1'
                                             ng-click="removeSelects($parent.$index , $index)"> <i
                                             class="fa fa-trash fa-lg selectedCurser"></i>
                                        </a>
                                   </div>
                              </div>
                              <div class="col-md-12">&nbsp;</div>
                              <div class="col-md-12 text-right">
                                   <button type="button"
                                        class="btn btn-w-md btn-gap-v btn-line-default"
                                        ng-click="openModal($parent.$index , $index)">Add
                                        Validation</button>
                                   <button type="button"
                                        class="btn btn-w-md btn-gap-v btn-line-default"
                                        ng-click="deleteValidation($index)">Delete</button>
                              </div>
                              </accordion-group> </accordion>
                         </div>
                    </section>
                    // second widget end

                    <section class="panel panel-default table-dynamic" ng-hide='true'>
                         <div class="panel-heading">
                              <strong> <span class="glyphicon glyphicon-th"></span>
                                   Load Strategy
                              </strong>
                         </div>
                         <div class="panel-body">
                              <dl>
                                   <dd>
                                        <label class="ui-radio"><input name="radio2"
                                             type="radio" value="" checked="checked"><span>
                                                  Replace All </span></label><br /> <label class="ui-radio"><input
                                             name="radio2" disabled="disabled" type="radio" value=""><span>
                                                  Append </span></label>
                                   </dd>
                              </dl>
                              <button type="button" class="btn btn-primary"
                                   style="float: right; margin-right: 18px;">Save</button>
                         </div>
                    </section>
               </div> -->

				<!-- <div class="col-md-12"
                    ng-hide="!myTbHide1 && !(activeTab == 'Others')">
                    <div data-ng-include="'views/DataSource/metaData.jsp'"></div>
               </div> -->

				<div class="col-md-12 padding-one"
					ng-hide='dataHide || (!myTbHide1 && !(activatedSubMenu == "Adv. Settings"))'>
					<div class="col-md-12">
						<section class="panel panel-default"
							data-ng-controller="featureExtractionController">
							<div class="panel-heading lan-heading">
								<span class="glyphicon glyphicon-th"></span> Feature extraction
								<div class="panel-bttn" style="padding-top: 2px;">

									<button type="button" class="btn btn-danger btn-block repo-btn"
										ng-click="openModal()">Select feature extraction</button>
								</div>

							</div>
							<div class="panel-body" style="min-height: 112px;">
								<div class="col-md-12">
									<div class="callout callout-info"
										ng-repeat="store in datasource.featureExtraction">
										<p>
											{{store.featureExtractionType}} <span class="rightAlign">
												<a a href="javascript:;"> <i class="fa fa-trash fa-lg"
													ng-click="deleteFeatureExtraction(store.id)"></i>
											</a>
											</span>
										</p>
									</div>
								</div>

							</div>
						</section>
					</div>
				</div>
				<!-- <div class="col-md-12"
                    ng-hide="!myTbHide1 && !(activeTab == 'Others')">
                    <section class="panel panel-default" ng-hide='dataHide'
                         data-ng-controller="advancedTransformationController">
                         <div class="panel-heading">
                              <span class="glyphicon glyphicon-th"></span> Advanced
                              Transformations
                         </div>

                         <div class="panel-body" style="min-height: 112px;">
                              <form name="advancedTransformationForm"
                                   class="form-horizontal form-validation" novalidate>
                                   <fieldset>
                                        <div class="form-group">
                                             <div class="col-md-12 fullwidthselect">
                                                  <span class="ui-select"> <select
                                                       ng-model="advancedTransformationTypes">
                                                            <option value="" selected="selected">-- Select
                                                                 Transformation Type --</option>
                                                            <option value="GEO_TAGGING">Geo Tagging</option>
                                                            <option value="Named_Entity_Recognition">Named
                                                                 Entity Recognition</option>
                                                            <option value="Part_Of_Speech">Part Of Speech</option>
                                                            <option value="Sentiment_Score">Sentiment Score</option>
                                                            <option value="Bi_gram">Bi-gram</option>
                                                            <option value="Tri_Gram">Tri-Gram</option>
                                                            <option value="Equiphone">Equiphone</option>
                                                            <option value="Name_cleanser">Name - cleanser</option>
                                                            <option value="Cleansing">Cleansing</option>
                                                            <option value="Stop_word_removal">Stop-word-removal</option>
                                                            <option value="Stemming">Stemming</option>
                                                  </select>
                                                  </span>
                                             </div>
                                        </div>
                                        <div class="animate-fade-up"
                                             ng-show="advancedTransformationTypes === 'GEO_TAGGING' ">
                                             <div class="form-group">
                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for="">Address Line1</label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field1"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id">
                                                                 <option value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>
                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for=""> Address Line2 </label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field2"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id"><option
                                                                      value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for=""> Address Line3 </label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field3"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id"><option
                                                                      value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>
                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for=""> Address Line4 </label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field4"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id"><option
                                                                      value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for=""> Address Line5 </label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field5"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id"><option
                                                                      value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>
                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for=""> City </label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field6"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id"><option
                                                                      value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>
                                             </div>

                                             <div class="form-group">
                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for=""> Pincode </label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field7"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id"><option
                                                                      value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>

                                                  <div class="col-sm-6 fullwidthselect">
                                                       <label for=""> State </label> <span class="ui-select">
                                                            <select class="ui-select" ng-model="field8"
                                                            ng-options="fieldMapping as fieldMapping.fieldName for fieldMapping in  fieldMappings track by fieldMapping.id"><option
                                                                      value="">--Select--</option>
                                                       </select>
                                                       </span>
                                                  </div>
                                             </div>
                                             <div class="divider"></div>
                                             <div class="form-group">
                                                  <div class="col-md-offset-5 col-md-7 text-right">
                                                       <button type="submit"
                                                            class="btn btn-w-md btn-gap-v btn-default"
                                                            ng-click="saveAdvancedTransformation()">Save</button>
                                                       <div class="space"></div>
                                                       <button type="button"
                                                            class="btn btn-w-md btn-gap-v btn-default"
                                                            ng-click="deleteValidation()">Cancel</button>
                                                  </div>
                                             </div>
                                        </div>
                                   </fieldset>
                              </form>
                              <div class="col-md-12">
                                   <div class="callout callout-info"
                                        ng-repeat="transformation in transformations">
                                        <p>Transformation Added: Geo Tagging</p>
                                        {{transformation.apiDomain.advancedTransformationTypes}}
                                   </div>
                              </div>
                         </div>
                    </section>
               </div> -->
				<!-- Data Cleansing starts here -->

				<div class="col-md-12"
					ng-hide="!myTbHide1 && !(activatedSubMenu == 'Adv. Settings')">
					<div class="col-md-12">
						<section class="panel panel-default" ng-hide='true'
							data-ng-controller="DataCleansingCntr">
							<div class="panel-heading lan-heading">
								<span class="glyphicon glyphicon-th"></span> Data cleansing
							</div>
							<form name="dataCleansingForm"
								class="form-horizontal form-validation">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-5">
												<input type="text" class="form-control" placeholder="From"
													ng-model="dataCleansingFrom" />
											</div>

											<div class="col-md-5">
												<input type="text" class="form-control" placeholder="To"
													ng-model="dataCleansingTo" />
											</div>

											<div class="col-md-1">
												<a style="line-height: 3;" ng-click="addDataCleansing()">
													<i class="fa fa-plus-circle fa-lg"></i>
												</a>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="callout callout-info"
											ng-repeat="notification in  dataCleansingFields track by $index">
											<p>
												{{notification.fromValue}} {{notification.toValue}} <span
													class="rightAlign"> <i
													class="fa fa-trash-o addCursor"
													ng-click="removeCleaseing(notification,  $index)"></i>
												</span>
											</p>
										</div>
									</div>

									<div class="row">
										<div class="row col-md-12 text-right">
											<button type="button"
												class="btn btn-w-md btn-gap-v btn-line-default"
												ng-click="saveDataCleansing()">Save</button>
										</div>
									</div>
								</div>
							</form>
						</section>
					</div>
				</div>

			</div>
		</fieldset>
		<!-- <fieldset ng-disabled="!popUpValue"> -->
		<!-- Application Setting start code -->
		<fieldset ng-disabled='myApplicationTab'>
			<div class="row" ng-hide='!(activeTab == "Application settings")'>
				<div class="col-md-12 padding-one">
					<section class="panel panel-default table-dynamic">
						<div class="panel-heading lan-heading">
							<strong> <span class="glyphicon glyphicon-th"></span>
								Application settings
							</strong>
						</div>
						<div class="panel-body" ng-init='appSetting = {use:"default"}'>
							<div class="dsFont" role="alert">
								<span ng-bind-html='explanation["appSetting"] | unsafe'></span>
							</div>
							<div class="col-md-12">&nbsp;</div>
							<div class="col-md-8">
								<label class="ui-radio" tooltip-placement="right"
									tooltip='{{explanation["useAsIt"]}}'> <input name="use"
									ng-model="appSetting.use" type="radio" value="default">
									<span>Use as it is </span>
								</label> <label class="ui-radio" tooltip-placement="top"
									tooltip='{{explanation["refresh"]}}'> <input name="use"
									ng-model="appSetting.use" type="radio" value="Refreshed">
									<span> Refresh </span>
								</label> <span style="color: red"></span>
							</div>
							<div class="col-md-12"></div>

							<div class="form-group" ng-if='appSetting.use == "Refreshed"'>
								<div class="col-md-3">
									<label style="float: left;">File path</label>
								</div>
								<div class="col-md-8">
									<span class="ui-select" style="margin-left: -4px; width: 102%;">
										<select
										ng-options="item.name for item in globleParam track by item.name"
										ng-model="appSetting.globalParam">
											<option value="">Select global param</option>
									</select>
									</span>
								</div>
							</div>
							&nbsp;
							<div class="form-group"
								ng-repeat="item in datasource.dsLevelParams track by $index"
								ng-if='appSetting.use == "Refreshed"'>
								<div class="col-md-3">
									<label style="float: left;">{{item.paramName}}</label>
								</div>
								<div class="col-md-8">
									<span class="ui-select" style="margin-left: -4px; width: 102%;">
										<select
										ng-options="item.name for item in globleParam track by item.name"
										ng-change="delAppSettings(list[$index] , $index)"
										ng-model="list[$index]">
											<option value="">Select input param</option>
									</select>
									</span>
								</div>
							</div>


							<div class="form-group" style="margin-right: -17px;">
								<button ng-click="saveAppSettings();"
									style="float: right; margin-right: 18px"
									class="btn btn-primary" type="submit">Save</button>
							</div>
						</div>
					</section>
				</div>
			</div>
		</fieldset>
		<!-- Application Setting end code -->

		<!-- History Start -->
		<fieldset ng-disabled="popUpValue">
			<div class="col-md-12 padding-one"
				ng-hide="!myTbHide1 && !(activatedSubMenu == 'History')"
				data-ng-controller="dataSourceHistoryCtrl">
				<div class="form-group"></div>
				<div class="form-group">
					<label class="ui-radio" tooltip-placement="top"
						tooltip='limit by No.'> <input type="radio"
						ng-model="datasource.typeOfActiveInstances" value="INSTANCES"
						ng-disabled="restrict"  
						ng-click="saveNoOfInstances('INSTANCES')"> <span>
							No. of Active Instances: </span>
					</label> <label class="ui-radio" tooltip-placement="top"
						tooltip='Limit by days'> <input type="radio"
						ng-disabled="restrict"  
						ng-model="datasource.typeOfActiveInstances" value="DAYS"
						ng-click="saveNoOfInstances('DAYS')"> <span> No. of
							Active Days: </span>
					</label> <label class="ui-radio" tooltip-placement="top"
						tooltip='Limit by days'> <input type="radio"
						ng-disabled="restrict"  
						ng-model="datasource.typeOfActiveInstances" value="DEFAULT"
						ng-click="saveNoOfInstances('DEFAULT')"> <span>
							Default: </span>
					</label> <input type="number" ng-model="datasource.noOfActiveInstances"  
						value="0" ng-disabled="onDefaultDisable ||restrict">

					<div style="float: right;">
						<button ng-click="saveDataSourceHistory()"
						ng-disabled="restrict"  
							style="margin-right: 18px" class="btn btn-primary" type="submit">Save</button>
						<button ng-click="resetChanges()" style="margin-right: 18px"
						ng-disabled="restrict"  
							class="btn btn-primary" type="submit">Reset</button>
						<button ng-click="dismissModel();" style="margin-right: 18px"
						ng-disabled="restrict"  
							class="btn btn-primary" type="submit">Cancel</button>
					</div>
				</div>

				<section class="panel panel-default table-dynamic"
					ng-disabled="disable">
					<div class="panel-heading">
						<strong> <span class="glyphicon glyphicon-th"></span>
							Active
						</strong>
					</div>
					<div class="panel-body" style="min-height: 112px;">
						<!-- <div class="col-md-12 noteCss" role="alert">
                              <span ng-bind-html='explanation["ingestion"] | unsafe'></span>
                         </div> -->

						<div class="col-md-12" style="margin-bottom: 15px;">&nbsp;</div>
						<div class="table-dynamic">
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup">
								<thead>
									<tr ng-hide="datasource.activeDatasources.length == 0">
										<th ng-hide="datasource.activeDatasources.length == 0"><div
												class="th">
												<label class="ui-checkbox"> <input type="checkbox"
													ng-model="activeCheckAll" tooltip-placement="top"
													tooltip="Check All" ng-disabled="restrict"  
													ng-change="checkAllActive(activeCheckAll)"> <span></span>
												</label>
											</div></th>
										<th><div class="th">JobInstId</div></th>
										<th><div class="th">Run By</div></th>
										<th><div class="th">Run At</div></th>
										<th><div class="th">Size</div></th>
										<th><div class="th">#Recs</div></th>
										<th><div class="th">Path</div></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="fm in activeDatasources track by $index">
										<td width="5%;"><label class="ui-checkbox"> <input
												type="checkbox" ng-model="fm.check"
												ng-change="changeValueTofalseActive(fm.check)"
												ng-disabled="disable"> <span></span>
										</label></td>
										<td width="10%;">{{fm.jobInstId}}</td>
										<td width="20%">{{fm.createdBy}}</td>
										<td width="15%">{{fm.createdDate | date:'medium'}}</td>
										<td width="10%">{{fm.size | sizeConvert}}</td>
										<td width="10%">{{fm.totalRecords}}</td>
										<td width="30%;">{{fm.filePath}}</td>
									</tr>
								</tbody>
							</table>
							<div class="form-group">
								<label class="col-md-2">Total Size:<span class="fontD">&nbsp;{{activeTotalSize
										| sizeConvert}}</span></label> <label class="col-md-2">Total Records:<span
									class="fontD">&nbsp;{{activeTotalRecords}}</span></label>
							</div>
							<div class="form-group">
								<span ng-click="inactivateDataSources()"
									style="float: right; margin-right: 18px"
									class="btn btn-primary pull-right" type="button"
									ng-disabled="changeActive">Inactivate</span>
							</div>
						</div>
					</div>
				</section>
				<div style="margin-bottom: 5px;">&nbsp;</div>
				<section class="panel panel-default table-dynamic">
					<div class="panel-heading">
						<strong> <span class="glyphicon glyphicon-th"></span>
							History
						</strong>
					</div>
					<div class="panel-body" style="min-height: 112px;">
						<!-- <div class="col-md-12 noteCss" role="alert">
                        <span ng-bind-html='explanation["ingestion"] | unsafe'></span>
                    </div> -->

						<div class="col-md-12" style="margin-bottom: 15px;">&nbsp;</div>
						<div class="table-dynamic">
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup">
								<thead>
									<tr ng-hide="datasource.historyDatasources.length == 0">
										<th ng-hide="datasource.historyDatasources.length == 0"><div
												class="th  ">
												<label class="ui-checkbox "> <input type="checkbox"
													ng-model="historyCheckAll" tooltip-placement="top"
													tooltip="Check All" ng-disabled="restrict"  
													ng-change="checkAllHistory(historyCheckAll)"> <span></span>
												</label>
											</div></th>
										<th><div class="th">JobInstId</div></th>
										<th><div class="th">Run By</div></th>
										<th><div class="th">Run At</div></th>
										<th><div class="th">Size</div></th>
										<th><div class="th">#Recs</div></th>
										<th><div class="th">Path</div></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="fm in historyDatasources track by $index">
										<td width="5%;"><label class="ui-checkbox "> <input
												type="checkbox" ng-model="fm.check"
												ng-change="changeValueTofalseHistory(fm.check)"
												ng-disabled="disable"> <span></span>
										</label></td>
										<td width="10%;">{{fm.jobInstId}}</td>
										<td width="20%">{{fm.createdBy}}</td>
										<td width="15%">{{fm.createdDate | date:'medium'}}</td>
										<td width="10%">{{fm.size | sizeConvert}}</td>
										<td width="10%">{{fm.totalRecords}}</td>
										<td width="30%;">{{fm.filePath}}</td>
									</tr>
								</tbody>
							</table>
							<div class="form-group">
								<label class="col-md-2">Total Size:<span class="fontD">&nbsp;{{inactiveTotalSize
										| sizeConvert}}</span></label> <label class="col-md-2">Total Records:<span
									class="fontD">&nbsp;{{inactiveTotalRecords}}</span></label>
							</div>
							<div class="form-group">
								<span ng-click="activateDataSources()"
									style="float: right; margin-right: 18px"
									class="btn btn-primary pull-right" ng-disabled="changeHistory"
									type="button">Activate</span> <span
									ng-click="deletDataSources()"
									style="float: right; margin-right: 18px"
									class="btn btn-primary pull-right" ng-disabled="changeHistory"
									type="button">Delete</span>
							</div>
						</div>
					</div>
				</section>
				<div style="margin-bottom: 5px;">&nbsp;</div>

				<section class="panel panel-default table-dynamic">
					<div class="panel-heading">
						<strong> <span class="glyphicon glyphicon-th"></span>
							Deleted
						</strong>
					</div>
					<div class="panel-body" style="min-height: 112px;">
						<!-- <div class="col-md-12 noteCss" role="alert">
                        <span ng-bind-html='explanation["ingestion"] | unsafe'></span>
                    </div> -->

						<div class="col-md-12" style="margin-bottom: 15px;">&nbsp;</div>
						<div class="table-dynamic">
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup">
								<thead>
									<tr ng-hide="datasource.deletedDatasources.length == 0">
										<th><div class="th">JobInstId</div></th>
										<th><div class="th">Run By</div></th>
										<th><div class="th">Run At</div></th>
										<th><div class="th">Deleted By</div></th>
										<th><div class="th">Deletion Date</div></th>
										<th><div class="th">Size</div></th>
										<th><div class="th">#Recs</div></th>
										<th><div class="th">Path</div></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="fm in deletedDatasources track by $index">
										<td width="">{{fm.jobInstId}}</td>
										<td width="">{{fm.createdBy}}</td>
										<td width="">{{fm.createdDate | date:'medium'}}</td>
										<td width="">{{fm.updatedBy}}</td>
										<td width="">{{fm.updatedDate | date:'medium'}}</td>
										<td width="">{{fm.size | sizeConvert}}</td>
										<td width="">{{fm.totalRecords}}</td>
										<td width="">{{fm.filePath}}</td>
									</tr>
								</tbody>
							</table>
							<div class="form-group">
								<label class="col-md-2">Total Size:<span class="fontD">&nbsp;{{deletedTotalSize
										| sizeConvert}}</span></label> <label class="col-md-2">Total Records:<span
									class="fontD">&nbsp;{{deletedTotalRecords}}</span></label>
							</div>
						</div>
					</div>
				</section>

			</div>
			<!-- History END -->
		</fieldset>


		<div class="modal-footer"
			ng-if="modelCheck != undefined && modelCheck != null">
			<button class="btn btn-primary" ng-click="dismissModel();"
				style="width: 200px">Okay</button>
			<button class="btn btn-default btn-block-data"
				ng-disabled='decideDesabled == 0'
				ng-click="nextPrevous('previous');">Previous</button>
			<button class="btn btn-default btn-block-data"
				ng-disabled='decideDesabled == 3' ng-click="nextPrevous('next');">Next</button>
		</div>
	</div>
</div>

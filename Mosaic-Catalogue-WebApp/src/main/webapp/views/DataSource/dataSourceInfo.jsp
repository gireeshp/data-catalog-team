<div class="page page-profile" ng-controller="mainDataSourceController">
	<fieldset ng-disabled='popUpValue' dw-loading="loadingDS"
		dw-loading-options="{className: 'custom-loading', spinnerOptions: {className: 'custom-spinner'}}">
		<div class="modal-header">
			<div class="size-h3 dsHead pad-zero">
				<div class="col-md-12">
					<font class="size-h3"> {{selected}} </font><i
						class="glyphicon glyphicon-check-icon"></i> <span
						style="float: right;"> </span>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<div class="col-md-12">
				<button class="btn btn-primary pull-right" ng-disabled="dataSourceRequestDisable" ng-click="requestForDatasource(dataSourceInstance,'justification', dataSourceRequestLabel)">{{dataSourceRequestLabel}}</button>
				<div id="tabs" ng-controller="TabsCtroller">
					<tabset> <tab ng-click="onClickTab(tab)"
						ng-repeat="tab in tabs" style="cursor : pointer;"
						heading="{{tab.title}}"
						active="tab.active">
					<div ng-include="currentTab"></div>
					</tab> </tabset>
				</div>
			</div>

			<div class="col-md-12" ng-if='!myTbHide1'>&nbsp;</div>
						
			<!-- Overview code start -->
			<div class="col-md-12 padding-one"
				ng-hide="!(activeTab == 'Overview')">
				<section class="row" style="overflow: auto; max-width: 1127px;">
					<div class="col-md-12 text-center">
						<h4><strong>Description</strong></h4>
						<div class="col-md-12">{{datasource.description}}</div>
					</div>
					<div class="col-md-12">&nbsp;</div>
					<div class="col-md-12 pad-zero">
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading" style="height: 36px;"></div>
								<div class="panel-body pad-zero">
									<div class="col-md-12 overview-css" style="padding-bottom: 7px;">
										<strong>ID: </strong> {{dataSourceInstance.dataSourceId}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Data source group: </strong> {{dataSourceInstance.dataRepoName}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Type: </strong> {{dataSourceInstance.dataSourceType
											| contentType}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Size: </strong> {{dataSourceInstance.size
											| sizeConvert}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Created on: </strong> {{dataSourceInstance.createdDate
											| date : 'medium'}}
									</div>
									<div class="col-md-12" style="padding-top: 7px;">
										<strong>Last refreshed on</strong> {{dataSourceInstance.updatedDate
											| date : 'medium'}}
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading lan-heading" style="height: 36px;"></div>
								<div class="panel-body pad-zero">
									<div class="col-md-12 overview-css" style="padding-bottom: 7px;">
										<strong>Category: </strong> {{datasource.category}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Sub-Category: </strong> {{datasource.subCategory}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Ingestion method: </strong> {{dataSourceInstance.ingection}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Created by: </strong> {{dataSourceInstance.createdBy}}
									</div>
									<div class="col-md-12 overview-css" style="padding-bottom: 7px; padding-top: 7px;">
										<strong>Refreshed by: </strong> {{dataSourceInstance.lastRunBy}}
									</div>
									<div class="col-md-12" style="padding-top: 7px;">
										<strong>Total records: </strong> {{dataSourceInstance.totalRecords}}
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!-- Overview code end -->			
			
			<!-- Data tab code start -->
			<div class="col-md-12 padding-one" ng-hide="!(activeTab == 'Fields')">
				<section class="panel panel-default" style="overflow-y: auto">
					<div class="panel-heading lan-heading" style="height: 36px;"></div>
					<div class="panel-body" style="overflow-y: auto;max-height: 1000px;">
						<div class="col-md-12">&nbsp;</div>
						<div class="table-dynamic">
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup">
								<thead>
									<tr ng-hide="datasource.fieldMappings.length == 0">
										<th ng-hide="datasource.fieldMappings.length == 0"><div class="th">
											# </div></th>
										<th><div class="th">
											Field name </div></th>
										<th><div class="th">
											Data type </div></th>
										<th ng-init='check = []'><div class="th">
											Format </div></th>
										<th><div class="th">
											Description </div></th>
										<th><div class="th">
											Key </div></th>
										<th><div class="th">
											PII </div></th>
										</tr>
								</thead>
								<tbody>
									<tr ng-repeat="fm in datasource.fieldMappings track by $index">
										<td>{{$index + 1}}</td>
										<td><span>{{fm.fieldName}}</span></td>
										<td><span>{{fm.fieldDataType}}</span></td>
										<td><span>{{fm.format}}</span></td>
										<td><span>{{fm.descriptions}}</span></td>
										<td><span>{{fm.key | convertToBooleanToYesNo}}</span></td>
										<td><span>{{fm.pIIId | convertToBooleanToYesNo}}</span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
			<!-- Dependecies start code -->
			<div class="col-md-12 padding-one" ng-hide="!(activeTab == 'Dependencies')">
				<section class="panel panel-default" style="overflow-y: auto">
					<div class="panel-heading lan-heading" style="height: 36px;">
						<strong ng-if="null != flowDetails && undefined != flowDetails && flowDetails.length > 0">
							Dependent flows</strong>
						<strong ng-if="null == flowDetails || undefined == flowDetails || flowDetails.length <= 0">
							No dependent flows</strong>
					</div>
					<div class="panel-body" style="overflow-y: auto;max-height: 1000px;">
						<div class="col-md-12">&nbsp;</div>
						<div class="table-dynamic" ng-if="null != flowDetails && undefined != flowDetails && flowDetails.length > 0">
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup">
								<thead>
									<tr>
										<th><div class="th">
											# </div></th>
										<th><div class="th">
											Flow name </div></th>
										<th><div class="th">
											Created on </div></th>
										<th ng-init='check = []'><div class="th">
											Last run on </div></th>
										<th><div class="th">
											Created by </div></th>
										<th><div class="th">
											Status </div></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="fm in flowDetails track by $index">
										<td>{{fm.appId}}</td>
										<td><span>{{fm.appName}}</span></td>
										<td><span>{{fm.createSt | date : 'medium'}}</span></td>
										<td><span>{{fm.lastRun | date : 'medium'}}</span></td>
										<td><span>{{fm.createdBy}}</span></td>
										<td><span>{{fm.statusEnum}}</span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
			<!-- Dependecies end code -->
			<!-- Feedback code start -->
			<div class="col-md-12 padding-one"
				ng-hide="!myTbHide1 && !(activeTab == 'Feedback')" ng-controller="feedbackCtrl">
				<section class="panel panel-default">
					<div class="panel-heading lan-heading" style="height: 36px;"></div>
					<div class="panel-body">
						<div class="col-md-12">
							<div class="col-md-1"></div>
							<div class="col-md-7">
								<div class="col-md-12">
									<input type="text" class="form-control" placeholder="Enter title" ng-model="obj.question">
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-10">
									<textarea class="form-control" rows="2" cols="10" ng-model="obj.answer" placeholder="Description"></textarea>
								</div>
								<div class="col-md-2" style="top: 20px;">
									<button class="btn btn-primary" style="width: 67px;" type="button" ng-click="submitPost(obj);">Submit</button>
								</div>
							</div> 
							<div class="col-md-4">
								<div class="col-md-12">
									<!-- <a href="javascript:;" class="pull-right"
										ng-click="showAvgRating()">Overall ratings</a><br /> <br /> -->
									<fieldset class="rating">
										<input type="radio" id="star5" ng-click="submitRating(star)" name="rating" value="1" ng-model="star.starFive"/>
										<label for="star5" title="" id="starFive" ng-mouseout="showRating(userDetailsForAverageRating)" ng-mouseover="checkRating('starFive')">5 stars</label> 
										<input type="radio" id="star4" ng-click="submitRating(star)" name="rating" value="1" ng-model="star.starFour" />
										<label for="star4" title="" id="starFour" ng-mouseout="showRating(userDetailsForAverageRating)" ng-mouseover="checkRating('starFour')">4 stars</label> 
										<input type="radio" id="star3" ng-click="submitRating(star)" name="rating" value="1" ng-model="star.starThree" />
										<label for="star3" title="" id="starThree" ng-mouseout="showRating(userDetailsForAverageRating)" ng-mouseover="checkRating('starThree')">3 stars</label> 
										<input type="radio" id="star2" ng-click="submitRating(star)" name="rating" value="1" ng-model="star.starTwo" />
										<label for="star2" title="" id="starTwo" ng-mouseout="showRating(userDetailsForAverageRating)" ng-mouseover="checkRating('starTwo')">2 stars</label> 
										<input type="radio" id="star1" ng-click="submitRating(star)" name="rating" value="1" ng-model="star.starOne"/>
										<label for="star1" title="" id="starOne" ng-mouseout="showRating(userDetailsForAverageRating)" ng-mouseover="checkRating('starOne')">1 star</label>
									</fieldset>
									<br /> 
								</div>
							</div>
							
							<div class="col-md-12">&nbsp;</div>
							<div class="col-md-12 feedback-css" ng-repeat="item in feedbackObj track by $index">
								<div class="col-md-1">
									<div class="col-md-12">
									<div class="col-md-1" style="color:#606060; text-align: center;">
										<button class="btn btn-default votingButton" data-ng-click="updateVote(+1, $index, item)">
											<i class="fa fa-caret-up fa-2x pull-left"
												data-ng-class="item.selfVote==1 ? 'voted':'voting'"></i> 
												<span class="vote-positive">{{item.votingPositiveCount}}</span>
										</button>
										<button class="btn btn-default votingButton">{{item.votingCount}}</button>
										<button class="btn btn-default votingButton" data-ng-click="updateVote(-1, $index, item)">
											<i class="fa fa-caret-down fa-2x"
												data-ng-class="item.selfVote==-1 ? 'voted':'voting'"></i>
												<span class="vote-negative">{{item.votingNegativeCount}}</span>
										</button>
										<!-- <span style="height: 30px;" class="col-md-12">{{item.votingPositiveCount}}</span> --> 
										<!-- <button class="btn btn-primary votingButton"><i class="fa fa-caret-up fa-2x pull-left" ng-class="item.selfVote==1 ? 'voted':'voting'" ng-click="updateVote(+1, $index, item)"></i>{{item.votingPositiveCount}}</button>
										<span style="height: 30px;" class="col-md-12"><i class="fa fa-2x">{{item.votingCount}}</i></span>
										<button class="btn btn-primary votingButton"><i class="fa fa-caret-down fa-2x" ng-class="item.selfVote==-1 ? 'voted':'voting'"  ng-click="updateVote(-1, $index, item)"> </i>{{item.votingNegativeCount}}</button> -->
										<!-- <span style="height: 30px;" class="col-md-12"> {{item.votingNegativeCount}}</span> -->
									</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="col-md-12">
										<span style="color: #17b9e7;font-size: 16px;">{{item.question}}</span><br/> 
										<span>{{item.answer}}</span>
									</div>
									<div class="col-md-12">
										<span class="pull-right">&nbsp; {{item.insertTS | date}} &nbsp;&nbsp; <span style="color: #17b9e7;">{{item.unqUserId}}</span></span>
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!-- Feedback code end -->
			<!-- Data tab code start -->
			<div class="col-md-12 padding-one"
				ng-hide="!myTbHide1 && !(activeTab == 'Data')">
				<section class="panel panel-default" style="overflow: auto; max-width: 1127px;">
					<div class="panel-heading lan-heading" style="height: 36px;"></div>
					<div class="panel-body" ng-if="datasource.fieldMappings.length > 0">
						<div class="table-dynamic" style="padding-top: 15px;" ng-if="sampleData.length > 0">
							<div class="dsFont" role="alert">
								<span ng-bind-html='explanation["fieldMapping"] | unsafe'></span>
							</div><br>
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup">
								<thead>
									<tr ng-hide="datasource.fieldMappings.length == 0">
										<th ng-repeat="fm in datasource.fieldMappings"><div class="th">
												{{fm.fieldName}}
										</div></th>
									</tr>
								</thead>

								<tbody>
									<tr ng-repeat="sample in sampleData track by $index">
										<td ng-repeat="data in sample track by $index">{{(datasource.fieldMappings[$index].pIIId == true || datasource.fieldMappings[$index].pIIId == "true")  &&  data != null ? 'xxxxxxxxxx' : data}} </td>
										{{data}}
									</tr>
								</tbody>
							</table>
						</div>
						<div ng-if="sampleData.length == 0">
							No records available.
						</div>
					</div>
				</section>
			</div>
			<!-- Data code end -->
			<!-- RELATIONSHIPS -->
			<div class="col-md-12"
				ng-hide="!myTbHide1 && !(activeTab == 'Relationships')">
				<div data-ng-include="'views/template/relationContainer.html'"></div>
			</div>
		</div>
			<!-- Relationship code end -->
	</fieldset>
	<div class="modal-footer textTop">
		<button class="btn btn-primary" ng-click="dismissModel();"
			style="width: 200px">Okay</button>
		<!-- <button class="btn btn-default btn-block-data"
			ng-disabled='decideDesabled == 0' ng-click="nextPrevous('previous');">Previous</button>
		<button class="btn btn-default btn-block-data"
			ng-disabled='decideDesabled == 3' ng-click="nextPrevous('next');">Next</button> -->
	</div>
</div>

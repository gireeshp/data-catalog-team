<div ng-controller="connectorCtlr">
<div class="row">
	<div class="col-md-12" >
	<div class ="col-md-12">Select Connection Type</div>
	<!-- csv  -->
		<div class='col-sm-2 grid-padding-top' ng-repeat="connector in connectorTypeList">
			<div class='gridCard grid-height' ng-click="selectConnector(connector.sourceId,connector.connectionType)">
				<div class="col-md-12">
					<div class='col-md-12 text-center gridCard-image grid-margin-top'>
						<img alt='{{connector.connectionType}}'
							ng-src="{{connector.sourceImagePath}}"/>
					</div>
					<div class="col-md-12"></div>
					<div class="col-md-12 text-center" style="padding: 15px;">{{connector.connectionType}}</div>
				</div>
			</div>
		</div>
		<!-- csv  -->
	</div>
</div>

<div class="row">
	<div class="col-md-12" ng-if="connectorSubTypeList">
	<div class ="col-md-12 connection-title">Select Connection Sub Type</div>
	<!-- csv  -->
		<div class='col-sm-2 grid-padding-top' ng-repeat="connectorST in connectorSubTypeList">
			<div class='gridCard grid-height' ng-click="selectConnectorSubType(connectorST.subSourceId,connectorST.subConnectionType)">
				<div class="col-md-12">
					<div class='col-md-12 publish-img-height text-center gridCard-image grid-margin-top'>
						<img class="_img-responsive" alt='{{connectorST.subConnectionType}}' ng-src="{{connectorST.sourceImagePath}}"/>
					</div>
					<div class="col-md-12"></div>
					<div class="col-md-12 text-center" style="padding: 15px;">{{connectorST.subConnectionType}}</div>
				</div>
			</div>
		</div>
		<!-- csv  -->
	</div>
</div>
</div>

<div id="top" data-ng-controller=dataSourceRequestCtrl>
	<div class="modal-header" ng-if="appId != undefined && messageChanger">
		<h4>Select from existing flows</h4>
	</div>

	<div class="modal-header" ng-if="appId != undefined && !messageChanger">
		<h4>Select existing data source/ node</h4>
	</div>

	<div ng-if="appId != undefined">&nbsp;</div>
	<div id="tabs" ng-controller="TabsCtrlDS" ng-if="appId != undefined">
		<tabset> <tab ng-click="onClickTab(tab)"
			ng-repeat="tab in tabs" heading="{{tab.title}}">
		<div ng-include="currentTab"></div>
		</tab> </tabset>
	</div>
	<section class="table-dynamic footerMarginCss" ng-hide="dsDecide != true">
		<div class="table-filters padTable">
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">

					</form>
				</div>

				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span>Showing {{filteredStores.length == 0 ? 0 : ((pageNum
						-1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) +
						dataSourceCol.length}} of {{filteredStores.length}} entries </span>
				</div>
			</div>
		</div>
		<div class="modal-body" ng-if="dsDecide == true">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th><div class="th">
								# <span class="fa fa-angle-up" data-ng-click=" order('id') "
									data-ng-class="{active: row == 'id'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-id') "
									data-ng-class="{active: row == '-id'}"></span>
							</div></th>
						<th id="selectds"><div id="thdsname"></div>
							<div class="th">
								Data source name <span class="fa fa-angle-up"
									data-ng-click=" order('objectName') "
									data-ng-class="{active: row == 'objectName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-objectName') "
									data-ng-class="{active: row == '-objectName'}"></span>
							</div></th>
						<!-- <th id="dsgroupname"><div></div>
							<div class="th">
								Group name <span class="fa fa-angle-up"
									data-ng-click=" order('fromGroup') "
									data-ng-class="{active: row == 'fromGroup'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-fromGroup') "
									data-ng-class="{active: row == '-fromGroup'}"></span>
							</div></th> -->
						<th><div class="th">
								Requested by <span class="fa fa-angle-up"
									data-ng-click=" order('requestedBy') "
									data-ng-class="{active: row == 'requestedBy'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-requestedBy')"
									data-ng-class="{active: row == '-requestedBy'}"></span>
							</div></th>
						<th><div class="th">
								Requested To <span class="fa fa-angle-up"
									data-ng-click=" order('requestedGroup') "
									data-ng-class="{active: row == 'requestedGroup'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-requestedGroup') "
									data-ng-class="{active: row == '-requestedGroup'}"></span>
							</div></th>
						<th><div class="th">
								Status <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceType') "
									data-ng-class="{active: row == 'dataSourceType'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-dataSourceType')"
									data-ng-class="{active: row == '-dataSourceType'}"></span>
							</div></th>
						<th><div class="th">
								Category <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceType') "
									data-ng-class="{active: row == 'dataSourceType'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-dataSourceType')"
									data-ng-class="{active: row == '-dataSourceType'}"></span>
							</div></th>
						<th><div class="th">
								SubCategory<span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceType') "
									data-ng-class="{active: row == 'dataSourceType'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-dataSourceType')"
									data-ng-class="{active: row == '-dataSourceType'}"></span>
							</div></th>
						<th><div class="th">
								Justification <span class="fa fa-angle-up"
									data-ng-click=" order('justification') "
									data-ng-class="{active: row == 'justification'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-justification') "
									data-ng-class="{active: row == '-justification'}"></span>
							</div></th>
						<th><div class="th">
								Requested date <span class="fa fa-angle-up"
									data-ng-click=" order('lastRunBy')"
									data-ng-class="{active: row == 'requestedDate'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-requestedDate') "
									data-ng-class="{active: row == '-requestedDate'}"></span>
							</div></th>
							<th><div class="th">
								Rejection Reason  <span class="fa fa-angle-up"
									data-ng-click=" order('rejectedJustification') "
									data-ng-class="{active: row == 'rejectedJustification'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-rejectedJustification') "
									data-ng-class="{active: row == '-rejectedJustification'}"></span>
							</div></th> 
							
							
						<!-- <th>
							<div class="th" id="dsaction">Action</div>
						</th> -->
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" id="selectdsclick"
						data-ng-repeat="ds in dataSourceCol"
						ng-init="ds.nodeIndicate = 'datasource'">
						<td>{{ds.id}}</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_DS"><a
							href="javascript:;" class="anchorColor" data-tooltip='{{ds.objectName}}'>
							<!-- data-ng-click="dataSourcePopup(ds.dataSourceName,ds.dataSourceId)"> -->{{ds.objectName.length
								> 40 ? ds.objectName.substring(0, 40).concat("...") :
								ds.objectName}}</a></td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_DS"><a
							href="javascript:;" data-tooltip='{{ds.dataSourceName}}'>{{ds.dataSourceName}}</a></td>
						<!-- <td><a href="javascript:;" class="anchorColor" tooltip='{{ds.fromGroup}}'
							ng-click="attachDataRepos(ds.fromGroup)">{{ds.fromGroup}}</a>
						</td> -->
						<td>{{(ds.requestedBy == 'null' ? "" : ds.requestedBy).replace(" ","_")}}</td>
						<td><a href="javascript:;" class="anchorColor" tooltip='{{ds.requestedUser}}'
							ng-click="attachDataRepos(ds.requestedUser)">{{(ds.requestedUser == 'null' ? "" : ds.requestedUser).replace(" ","_")}}</a>
						</td>
						<td>{{getStatusOfDataSource(ds.id)}}</td>
						<td>{{getStatusObjectByDSId(ds.id).category == 'null' ? "" : getStatusObjectByDSId(ds.id).category}}</td>
						<td>{{getStatusObjectByDSId(ds.id).subCategory == 'null' ? "" : getStatusObjectByDSId(ds.id).subCategory}}</td>
						<td>{{(ds.justification == 'null'||ds.justification =='undefined') ? "" : ds.justification}}</td>
						<td>{{(ds.requestedDate == 'null' ? "" : ds.requestedDate) | date : 'medium'}}</td>
						<td>{{(ds.rejectedJustification == 'null' ? "" : ds.rejectedJustification)}}</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange(numPerPage)">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
	<div class="modal-footer"
		ng-if="appId != undefined && appId != null">
		<button class="btn btn-primary" ng-click="confirmDsource();"
			style="width: 90px">Okay</button>
		<button class="btn btn-warning" ng-click="dismissModel();"
			style="width: 90px">Cancel</button>
	</div>
	
	<!-- </div> -->
</div>

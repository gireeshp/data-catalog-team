<div id="top" data-ng-controller="dataSourceTableCtrl">
	<div class="modal-header" ng-if="appId != undefined && messageChanger">
		<h4>Select from existing flows</h4>
	</div>

	<div class="modal-header" ng-if="appId != undefined && !messageChanger">
		<h4>Select existing data source/ node</h4>
	</div>

	<div ng-if="appId != undefined">&nbsp;</div>
	<div id="tabs" ng-controller="TabsCtrlDS" ng-if="appId != undefined" style="padding: 0px 0px 0 20px;">
		<tabset> <tab ng-click="onClickTab(tab)" style="cursor : pointer;"
			ng-repeat="tab in tabs" heading="{{tab.title}}">
		<div ng-include="currentTab"></div>
		</tab> </tabset>
	</div>
	<div class="col-md-12 noteCss" role="alert" ng-if="appId == undefined">
		<span ng-bind-html='explanation["DsHeader"] | unsafe'></span>
	</div>
	<section class="table-dynamic footerMarginCss" ng-hide="dsDecide != true">
		<div class="table-filters padTable">
			<div class="col-md-12 noteCss popMargin" role="alert"
				ng-if="appId != undefined">
				<span ng-bind-html='explanation[checkHeader] | unsafe'></span>
			</div>
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">

					</form>
				</div>

				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span>Showing {{filteredStores.length == 0 ? 0 : ((pageNum
						-1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) +
						dataSourceCol.length}} of {{filteredStores.length}} entries </span>
				</div>
				<div class="col-sm-3 col-xs-6 left-css">
					<label class="ui-checkbox checkbox-margin"> <input type="checkbox"
						ng-model="userList" ng-change="showFilterDataSourceList(userList)"> <span></span> <span>Data source outside group</span>
					</label>
				</div>
				<div class="col-sm-3 col-xs-6 btntop">
					<div
						ng-if="appId == undefined && user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CONFIG_NEW_DS">
						<button type="button" id="createdsclick"
							class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
							ng-click="open()">
							Configure new data source
							<!-- <i class="fa fa-plus"></i> -->
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-body" ng-if="dsDecide == true">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th><div class="th">
								# <span class="fa fa-angle-up" data-ng-click="order('dataSourceId') "
									data-ng-class="{active: row == 'dataSourceId'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-dataSourceId') "
									data-ng-class="{active: row == '-dataSourceId'}"></span>
							</div></th>
						<th width="15%" id="selectds"><div id="thdsname"></div>
							<div class="th">
								Data source name <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceName') "
									data-ng-class="{active: row == 'dataSourceName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-dataSourceName') "
									data-ng-class="{active: row == '-dataSourceName'}"></span>
							</div></th>
						<th id="dsgroupname" width="15%"><div></div>
							<div class="th">
								Data source group <span class="fa fa-angle-up"
									data-ng-click=" order('dataRepoName') "
									data-ng-class="{active: row == 'dataRepoName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-dataRepoName') "
									data-ng-class="{active: row == '-dataRepoName'}"></span>
							</div></th>
						<th><div class="th">
								Type <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceType') "
									data-ng-class="{active: row == 'dataSourceType'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-dataSourceType')"
									data-ng-class="{active: row == '-dataSourceType'}"></span>
							</div></th>
						<th width="12%"><div class="th">
								Refreshed on <span class="fa fa-angle-up"
									data-ng-click=" order('updatedDate') "
									data-ng-class="{active: row == 'updatedDate'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-updatedDate') "
									data-ng-class="{active: row == '-updatedDate'}"></span>
							</div></th>
						<th><div class="th">
								Category<span class="fa fa-angle-up"
									data-ng-click=" order('category')"
									data-ng-class="{active: row == 'category'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-category') "
									data-ng-class="{active: row == '-category'}"></span>
							</div></th>
						<th width="12%"><div class="th">
								Sub-category<span class="fa fa-angle-up"
									data-ng-click=" order('subCategory')"
									data-ng-class="{active: row == 'subCategory'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-subCategory') "
									data-ng-class="{active: row == '-subCategory'}"></span>
									</div>
							</th>
						<th width="12%"><div class="th">
								User group <span class="fa fa-angle-up"
									data-ng-click=" order('lastRunBy')"
									data-ng-class="{active: row == 'lastRunBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-lastRunBy') "
									data-ng-class="{active: row == '-lastRunBy'}"></span>
							</div></th>
						<th><div class="th">
									Count <span class="fa fa-angle-up"
									data-ng-click=" order('totalRecords') "
										data-ng-class="{active: row == 'totalRecords'}"></span> <span
										class="fa fa-angle-down"
										data-ng-click=" order('-totalRecords') "
										data-ng-class="{active: row == '-totalRecords'}"></span>
								</div></th>
						<th width="15%"><div class="th">
									Avg rating <span class="fa fa-angle-up"
										data-ng-click=" order('rating') "
										data-ng-class="{active: row == 'rating'}"></span> <span
										class="fa fa-angle-down"
										data-ng-click=" order('-rating') "
										data-ng-class="{active: row == '-rating'}"></span>
								</div></th>
						<th width="16%">
							<div class="th" id="dsaction">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" id="selectdsclick"
						data-ng-repeat="ds in dataSourceCol track by $index"
						ng-init="ds.nodeIndicate = 'datasource'">
						<td>{{ds.dataSourceId}}</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_DS"><a
							href="javascript:;" class="anchorColor" data-tooltip='{{ds.dataSourceName}}'
							data-ng-click="dataSourcePopup(ds.dataSourceName, ds.groupId)">{{ds.dataSourceName.length
								> 30 ? ds.dataSourceName.substring(0, 30).concat("...") :
								ds.dataSourceName}}</a></td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_DS"><a
							href="javascript:;" data-tooltip='{{ds.dataSourceName}}'>{{ds.dataSourceName}}</a></td>
						<td>{{currentGroupId == ds.groupId ? ds.dataRepoName : ''}}</td>
						<td>{{ds.dataSourceType | contentType | titleCase}}</td>

						<td>{{(ds.updatedDate == 'null' ? "" : ds.updatedDate) | date : 'medium'}}</td>
						<td>{{(ds.category == 'null' ? "" : ds.category).replace("
							","_")}}</td>
						<td>{{(ds.subCategory == 'null' ? "" : ds.subCategory).replace("
							","_")}}</td>
						<td>{{ds.groupId | GroupNameFromGroupId:groupMap}}</td>
						<td>{{(ds.totalRecords == 'null' ? "" : ds.totalRecords)}}</td>
						<td ng-disabled="ratingDisabled">
							<fieldset class="testRating pull-left rating-click">
								<input type="radio" id="fiveStar" name="rating" value="5" style="cursor: not-allowed;"/>
								<label for="star5" title="" id="" class='max-font-size {{ds.avgRating > 4 ? "checked" : ""}}'>5 stars</label> 
								<input type="radio" id="fourStar" name="rating" value="4"/>
								<label for="star4" title="" id="" class='max-font-size {{ds.avgRating > 3 ? "checked" : ""}}'>4 stars</label> 
								<input type="radio" id="threeStar" name="rating" value="3"/>
								<label for="star3" title="" id="" class='max-font-size {{ds.avgRating > 2 ? "checked" : ""}}'>3 stars</label> 
								<input type="radio" id="twoStar" name="rating" value="2"/>
								<label for="star2" title="" id="" class='max-font-size {{ds.avgRating > 1 ? "checked" : ""}}'>2 stars</label> 
								<input type="radio" id="oneStar" name="rating" value="1"/>
								<label for="star1" title="" id="" class='max-font-size {{ds.avgRating > 0 ? "checked" : ""}}'>1 star</label>
							</fieldset>
						</td>
						<td>
							<div ng-if="appId != undefined && appId != null">
								<label class="ui-checkbox"> <input type="checkbox"
									ng-model="ds.checkBoxIs"
									ng-change="selectDSources(ds , ds.checkBoxIs)"><span></span>
								</label>
							</div>
							<ul class="nav-right ulMargin list-unstyled"
								ng-if="appId == undefined || appId == null">
								<li class=" {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									data-ng-if="(ds.dataRepoName != 'null' &amp;&amp; user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.LOAD_DS_DATA) && ds.accessType == 'LOAD_EDIT' && 'REMOTE_DATA' != ds.dataSourceType">
									<a href="javascript:;" class="actionMenu {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
										style="cursor: {{ds.dataSourceType == null || ds.dataSourceType == 'null' ? 'not-allowed' : 'pointer'}}" data-ng-click="globalJob(ds)"><i
										class="fa fa-play ng-scope" tooltip-placement="top"
										tooltip="Load data source"></i></a>
								</li>
								<li class=" {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									data-ng-if="(user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DELETE_DS) && ds.accessType == 'LOAD_EDIT' && 'REMOTE_DATA' != ds.dataSourceType"><a
									href="javascript:;" class="actionMenu {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									data-ng-click="deleteDataSource($index , null , ds.dataSourceName)"
									tooltip-placement="top" tooltip="Delete data source"><i
										class="fa fa-trash"></i></a></li>
								<li
									data-ng-if="(ds.dataRepoName != 'null' && user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.RENAME_DS) && ds.accessType == 'LOAD_EDIT' && 'REMOTE_DATA' != ds.dataSourceType">
									<a href="javascript:;" tooltip-placement="top"
									tooltip="Rename data source" class="actionMenu {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									data-ng-click="renameDataSource( dataSourceCol[$index], 'dataSource')"><i
										class="fa fa-pencil"></i></a>
								</li> 
								<li class=" {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									data-ng-if="(user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.ASSGN_DS_GROUP) && ds.accessType == 'LOAD_EDIT' && 'REMOTE_DATA' != ds.dataSourceType"><a
									href="javascript:;" class="actionMenu {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									data-ng-click="assignDataSource(ds)"><i
										class="fa fa-th-large" tooltip-placement="top"
										tooltip="Assign data source to group"></i> </a></li>
								<li class=" {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									data-ng-if="(user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DETAILS_DS) && ds.accessType == 'LOAD_EDIT' && 'REMOTE_DATA' != ds.dataSourceType">
									<a href="javascript:;" class="actionMenu {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}"
									style='color: {{ds.dataSourceType== null|| ds.dataSourceType== "null"|| ds.dataSourceType== undefined? "#777777": ""}}'
									data-ng-click="openDataProfiling(ds);"><i
										class="fa fa-magic ng-scope" tooltip-placement="top"
										tooltip="View profiling results"></i></a>
								</li>
								<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DETAILS_DS">
									<a href="javascript:;" class="actionMenu"
									data-ng-click="moreDetail(dataSourceCol[$index],ds,ds.dataSourceId)"><i
										class="fa fa-info-circle ng-scope" tooltip-placement="top"
										tooltip="Details"></i></a>
								</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange(numPerPage)">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
	<section class="table-dynamic" ng-controller="appLevelDataSource"
		ng-hide="dsDecide != false">
		<!-- <div class="panel-heading">
			<strong> <span class="glyphicon glyphicon-th"></span>
				App Sources
			</strong>
		</div> -->
		<!-- <fieldset ng-disabled='true'> -->
		<div class="table-filters padTable">
			<div class="col-md-12 noteCss popMargin headText" role="alert"
				ng-if="appId != undefined">
				<span ng-bind-html='explanation[checkHeader] | unsafe'></span>
			</div>
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span>Showing {{filteredStores.length == 0 ? 0 : ((pageNum
						-1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) +
						appLevelDS.length}} of {{filteredStores.length}} entries </span>
				</div>
				<div class="col-sm-6 col-xs-6 btntop" ng-if="appId == undefined">
					<button type="button"
						class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
						ng-click="open()">Create New Data Source</button>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th><div class="th">
								# <span class="fa fa-angle-up" data-ng-click=" order('dataSourceId') "
									data-ng-class="{active: row == 'dataSourceId'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-dataSourceId') "
									data-ng-class="{active: row == '-dataSourceId'}"></span>
							</div></th>

						<th>
							<div class="th">
								Name <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceName') "
									data-ng-class="{active: row == 'dataSourceName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-dataSourceName') "
									data-ng-class="{active: row == '-dataSourceName'}"></span>
							</div>
						</th>
						<th><div class="th">
								Flow name <span class="fa fa-angle-up"
									data-ng-click=" order('dataRepoName') "
									data-ng-class="{active: row == 'dataRepoName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-dataRepoName') "
									data-ng-class="{active: row == '-dataRepoName'}"></span>
							</div></th>
						<th><div class="th">
								Type <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceType') "
									data-ng-class="{active: row == 'dataSourceType'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-dataSourceType')"
									data-ng-class="{active: row == '-dataSourceType'}"></span>
							</div></th>
						<th><div class="th">
								Refreshed on <span class="fa fa-angle-up"
									data-ng-click=" order('updatedDate') "
									data-ng-class="{active: row == 'updatedDate'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-updatedDate') "
									data-ng-class="{active: row == '-updatedDate'}"></span>
							</div></th>
						<th><div class="th">
								Refreshed by <span class="fa fa-angle-up"
									data-ng-click=" order('lastRunBy') "
									data-ng-class="{active: row == 'lastRunBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-lastRunBy') "
									data-ng-class="{active: row == '-lastRunBy'}"></span>
							</div></th>
						<th><div class="th">
								Total records <span class="fa fa-angle-up"
									data-ng-click=" order('totalRecords') "
									data-ng-class="{active: row == 'totalRecords'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-totalRecords') "
									data-ng-class="{active: row == '-totalRecords'}"></span>
							</div></th>
						<th>
							<div class="th">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" data-ng-repeat="ds in appLevelDS track by $index"
						ng-init="ds.nodeIndicate = 'data_source_apps'">
						<td>{{ds.dataSourceId}}</td>
						<td><a href="javascript:;" class="anchorColor" tooltip='{{ds.dataSourceName}}'>{{ds.dataSourceName}}</a></td>
						<td><a href="javascript:;" class="anchorColor" tooltip='{{appName}}'>{{appName}}</a>
						</td>
						<td>{{ds.dataSourceType | contentType}}</td>
						<td>{{(ds.updatedDate == 'null' ? "" : ds.updatedDate) | date : 'medium'}}</td>
						<td>{{(ds.lastRunBy == 'null' ? "" : ds.lastRunBy).replace("
							","_")}}</td>
						<td>{{(ds.totalRecords == 'null' ? "" : ds.totalRecords)}}</td>
						<td>
							<div ng-if="appId != undefined && appId != null">
								<label class="ui-checkbox padCheck"> <input
									type="checkbox" ng-model="ds.checkBoxIs"
									ng-change="selectDSources(ds , ds.checkBoxIs)"><span></span>
								</label> <a href="javascript:;" style="line-height: 3;"
									ng-click="deleteAppsLevelDs(ds);"> <i
									class="fa fa-trash fa-lg selectedCurser"></i>
								</a>
							</div> <!-- <div class="btn-group" dropdown is-open="status.isopen1"
								ng-if="appId == undefined || appId == null">
								<button type="button" class="btn btn-default dropdown-toggle">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu" style="left: -85px;">
									<li
										ng-if="ds.dataRepoName.toLowerCase().lastIndexOf('_globalrepo') != -1"><a
										href="javascript:;" ng-click="startJob(store)">Start Job</a></li>
									<li><a href="javascript:;" ng-click="assignDataSource(ds)">Assign
											DS-Group</a></li>
									<li><a href="javascript:;"
										ng-click="renameDataSource( dataSourceCol[$index], 'dataSource')">Rename</a></li>
									<li><a href="javascript:;"
										ng-click="deleteDataSource($index , null , ds.dataSourceName)">Delete</a></li>
									<li><a href="javascript:;"
										ng-click="moreDetail(dataSourceCol[$index])">Details</a></li>
								</ul>
							</div> -->
						</td>
					</tr>
				</tbody>
			</table>

			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange()">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
		<!-- </fieldset> -->
	</section>
	<section class="table-dynamic" ng-controller="inputLevelDataSources"
		ng-hide="dsDecide != null || isStreamFlow">
		<div class="table-filters padTable">
			<div class="col-md-12 noteCss popMargin headText" role="alert"
				ng-if="appId != undefined">
				<span ng-bind-html='explanation[checkHeader] | unsafe'></span>
			</div>
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span>Showing {{filteredStores.length == 0 ? 0 : ((pageNum
						-1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) +
						inputLevelDS.length}} of {{filteredStores.length}} entries </span>
				</div>
				<div class="col-sm-6 col-xs-6 btntop" ng-if="appId == undefined">
					<button type="button"
						class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
						ng-click="open()">Create New Data Source</button>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th><div class="th">
								# <span class="fa fa-angle-up" data-ng-click=" order('id') "
									data-ng-class="{active: row == 'id'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-id') "
									data-ng-class="{active: row == '-id'}"></span>
							</div></th>

						<th>
							<div class="th">
								Name <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceName') "
									data-ng-class="{active: row == 'dataSourceName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-dataSourceName') "
									data-ng-class="{active: row == '-dataSourceName'}"></span>
							</div>
						</th>
						<th><div class="th">
								Flow name <span class="fa fa-angle-up"
									data-ng-click=" order('dataRepoName') "
									data-ng-class="{active: row == 'dataRepoName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-dataRepoName') "
									data-ng-class="{active: row == '-dataRepoName'}"></span>
							</div></th>
						<th><div class="th">
								Type <span class="fa fa-angle-up"
									data-ng-click=" order('dataSourceType') "
									data-ng-class="{active: row == 'dataSourceType'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-dataSourceType')"
									data-ng-class="{active: row == '-dataSourceType'}"></span>
							</div></th>
						<th><div class="th">
								Refreshed on <span class="fa fa-angle-up"
									data-ng-click=" order('updatedDate') "
									data-ng-class="{active: row == 'updatedDate'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-updatedDate') "
									data-ng-class="{active: row == '-updatedDate'}"></span>
							</div></th>
						<th><div class="th">
								Refreshed by <span class="fa fa-angle-up"
									data-ng-click=" order('lastRunBy') "
									data-ng-class="{active: row == 'lastRunBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-lastRunBy') "
									data-ng-class="{active: row == '-lastRunBy'}"></span>
							</div></th>
						<th><div class="th">
								Total records <span class="fa fa-angle-up"
									data-ng-click=" order('totalRecords') "
									data-ng-class="{active: row == 'totalRecords'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-totalRecords') "
									data-ng-class="{active: row == '-totalRecords'}"></span>
							</div></th>
						<th>
							<div class="th">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" data-ng-repeat="ds in inputLevelDS track by $index"
						ng-init="ds.nodeIndicate = ''">
						<td>{{ds.id}}</td>
						<td><a href="javascript:;" class="anchorColor" tooltip="{{ds.dataSourceName}}">{{ds.dataSourceName}}</a></td>
						<td><a href="javascript:;" class="anchorColor" tooltip='{{appName}}'>{{appName}}</a>
						</td>
						<td>{{ds.dataSourceType | contentType}}</td>
						<td>{{(ds.updatedDate == 'null' ? "" : ds.updatedDate) | date : 'medium'}}</td>
						<td>{{(ds.lastRunBy == 'null' ? "" : ds.lastRunBy).replace("
							","_")}}</td>
						<td>{{(ds.totalRecords == 'null' ? "" : ds.totalRecords)}}</td>
						<td>
							<div ng-if="appId != undefined && appId != null">
								<label class="ui-checkbox padCheck"> <input
									type="checkbox" ng-model="ds.checkBoxIs"
									ng-change="selectDSources(ds , ds.checkBoxIs)"><span></span>
								</label>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange()">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
		<!-- </fieldset> -->
	</section>
	<section class="table-dynamic" ng-controller="nodesListController"
		ng-hide="dsDecide != 'App-nodes'">
		<!-- <div class="panel-heading">
			<strong> <span class="glyphicon glyphicon-th"></span>
				App Nodes
			</strong>
		</div> -->
		<div class="table-filters padTable">
			<div class="col-md-12 noteCss popMargin headText" role="alert"
				ng-if="appId != undefined">
				<span ng-bind-html='explanation[checkHeader] | unsafe'></span>
			</div>
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Search..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span>Showing {{filteredStores.length == 0 ? 0 : ((pageNum
						-1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) +
						nodesList.length}} of {{filteredStores.length}} entries</span>
				</div>
				<div class="col-sm-6 col-xs-6 btntop" ng-if="appId == undefined">
					<button type="button"
						class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
						ng-click="open()">Create New Data Source</button>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th><div class="th">
								# <span class="fa fa-angle-up" data-ng-click=" order('id') "
									data-ng-class="{active: row == 'id'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-id') "
									data-ng-class="{active: row == '-id'}"></span>
							</div></th>

						<th>
							<div class="th">
								Name <span class="fa fa-angle-up"
									data-ng-click=" order('nodeName') "
									data-ng-class="{active: row == 'nodeName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-nodeName') "
									data-ng-class="{active: row == '-nodeName'}"></span>
							</div>
						</th>
						<th><div class="th">
								Flow name <span class="fa fa-angle-up"
									data-ng-click=" order('appName') "
									data-ng-class="{active: row == 'appName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-appName') "
									data-ng-class="{active: row == '-appName'}"></span>
							</div></th>
						<th><div class="th">
								Refreshed on <span class="fa fa-angle-up"
									data-ng-click=" order('lastRun') "
									data-ng-class="{active: row == 'lastRun'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-lastRun') "
									data-ng-class="{active: row == '-lastRun'}"></span>
							</div></th>
						<th><div class="th">
								Refreshed by <span class="fa fa-angle-up"
									data-ng-click=" order('lastrefreshBy') "
									data-ng-class="{active: row == 'lastrefreshBy'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-lastrefreshBy') "
									data-ng-class="{active: row == '-lastrefreshBy'}"></span>
							</div></th>
						<th>
							<div class="th">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" data-ng-repeat="ds in nodesList track by $index"
						ng-init="ds.nodeIndicate = 'node'">
						<td>{{ds.id}}</td>
						<td><a href="javascript:;" class="anchorColor" tooltip='{{ds.nodeName}}'>{{ds.nodeName}}</a></td>
						<td><a href="javascript:;" class="anchorColor" tooltip='{{ds.nodeName}}'>{{ds.appName}}</a>
						</td>
						<td>{{((((ds.lastRun == 'null' ? "" : ds.lastRun) | date :
							'medium').replace(" ","")).replace(" ",""))}}</td>
						<td>{{(ds.lastrefreshBy == 'null' ? "" :
							ds.lastrefreshBy).replace(" ","_")}}</td>
						<td>
							<div ng-if="appId != undefined && appId != null">
								<label class="ui-checkbox padCheck"> <input
									type="checkbox" ng-model="ds.checkBoxIs"
									ng-change="selectDSources(ds , ds.checkBoxIs)"><span></span>
								</label>
							</div> <!-- <div class="btn-group" dropdown is-open="status.isopen1"
								ng-if="appId == undefined || appId == null">
								<button type="button" class="btn btn-default dropdown-toggle">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu" style="left: -85px;">
									<li
										ng-if="ds.dataRepoName.toLowerCase().lastIndexOf('_globalrepo') != -1"><a
										href="javascript:;" ng-click="startJob(store)">Start Job</a></li>
									<li><a href="javascript:;" ng-click="assignDataSource(ds)">Assign
											DS-Group</a></li>
									<li><a href="javascript:;"
										ng-click="renameDataSource( dataSourceCol[$index], 'dataSource')">Rename</a></li>
									<li><a href="javascript:;"
										ng-click="deleteDataSource($index , null)">Delete</a></li>
									<li><a href="javascript:;"
										ng-click="moreDetail(dataSourceCol[$index])">Details</a></li>
								</ul>
							</div> -->
						</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange()">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
	<section class="table-dynamic" ng-controller="AppsListController"
		ng-hide="dsDecide != 'Apps'">
		<!-- <div class="panel-heading">
			<strong> <span class="glyphicon glyphicon-th"></span>
				Existing Applications
			</strong>
		</div> -->
		<div class="table-filters padTable">
			<div class="col-md-12 noteCss popMargin headText" role="alert"
				ng-if="appId != undefined">
				<span ng-bind-html='explanation[checkHeader] | unsafe'></span>
			</div>
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries"
					ng-hide="currentPageStores.length == 0">
					<span> Showing {{filteredStores.length == 0 ? 0 : ((pageNum
						-1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) +
						currentPageStores.length}} of {{filteredStores.length}} entries </span>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th><div class="th">
								# <span class="fa fa-angle-up" data-ng-click=" order('appId') "
									data-ng-class="{active: row == 'appId'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-appId') "
									data-ng-class="{active: row == '-appId'}"></span>
							</div></th>

						<th>
							<div class="th">
								Flow name <span class="fa fa-angle-up"
									data-ng-click=" order('appName') "
									data-ng-class="{active: row == 'appName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-appName') "
									data-ng-class="{active: row == '-appName'}"></span>
							</div>
						</th>
						<th><div class="th">
								Created on <span class="fa fa-angle-up"
									data-ng-click=" order('createSt') "
									data-ng-class="{active: row == 'createSt'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-createSt') "
									data-ng-class="{active: row == '-createSt'}"></span>
							</div></th>
						<th><div class="th">
								Refreshed on <span class="fa fa-angle-up"
									data-ng-click=" order('lastRun') "
									data-ng-class="{active: row == 'lastRun'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-lastRun') "
									data-ng-class="{active: row == '-lastRun'}"></span>
							</div></th>
						<th><div class="th">
								Created by <span class="fa fa-angle-up"
									data-ng-click=" order('createdBy') "
									data-ng-class="{active: row == 'createdBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-createdBy') "
									data-ng-class="{active: row == '-createdBy'}"></span>
							</div></th>

						<th><div class="th">
								Status <span class="fa fa-angle-up"
									data-ng-click=" order('statusEnum') "
									data-ng-class="{active: row == 'statusEnum'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-statusEnum') "
									data-ng-class="{active: row == '-statusEnum'}"></span>
							</div></th>
						<th>
							<div class="th">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp"
						data-ng-repeat="application in currentPageStores track by $index">
						<td>{{application.appId}}</td>
						<td><a href="javascript:;" class="anchorColor" tooltip='{{application.appName}}'>{{application.appName}}</a></td>
						<td><a class="anchorColor">{{application.createSt | date : 'medium' }}</a></td>
						<td>{{application.lastRun | date : 'medium' }}</td>
						<td>{{application.createdBy}}</td>
						<td>{{application.statusEnum | uppercase}}</td>
						<td><label class="ui-checkbox padCheck"> <input
								type="checkbox" ng-model="application.checkBoxIs"
								ng-change="selectApps(application, application.checkBoxIs)"><span></span>
						</label></td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange()">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
	<div class="modal-footer"
		ng-if="appId != undefined && appId != null">
		<button class="btn btn-primary" ng-click="confirmDsource();"
			style="width: 90px">Okay</button>
		<button class="btn btn-warning" ng-click="dismissModel();"
			style="width: 90px">Cancel</button>
	</div>
	
	<!-- </div> -->
</div>

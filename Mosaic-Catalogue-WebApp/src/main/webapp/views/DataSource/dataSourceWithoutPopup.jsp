<div class="row" ng-controller="dataSourceInformer"
	ng-init="editableInput='true'" style="padding-top: 2px !important;">
	<fieldset
		ng-disabled="popUpValue || projectAccessType == 'reviewer' || disableDataSource == '1'"
		dw-loading="loadingDS"
		dw-loading-options="{className: 'custom-loading', spinnerOptions: {className: 'custom-spinner'}}">
		<div class="col-md-12" id="top">
			<div class="">
				<div class="mini-box ds-without-popup bg-primary ds-tab-color">
					<div class="col-md-12">
						<font class="ds-name"> {{selected}} </font>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div id="tabs" ng-controller="TabsCtrl" class="tab-size">
					<tabset> <tab ng-click="onClickTab(tab)" ng-if="!(dataSourceObject.dataSourceType === 'COGNOS_DATA_SOURCE' && tab.title === 'Sample records')"
						ng-repeat="tab in tabs" style="cursor : pointer;"
						heading="{{tab.title}}"
						active="tab.active" class="pad-zero">
					<div ng-include="currentTab"></div>
					</tab> </tabset>
				</div>
			</div>
		</div>
		<div class="col-md-12" ng-if="subMenu.length > 0">&nbsp;</div>
		<div class="col-md-12" ng-if="subMenu.length > 0">
			<div class="col-md-12">
				<div id="tabs" class="tab-size">
					<tabset> <tab ng-click="onClickSubMenuTab(tab)"
						ng-repeat="tab in subMenu" style="cursor : pointer;"
						heading="{{tab.title}}" active="tab.active" class="pad-zero">
					<div ng-include="currentTab"></div>
					</tab> </tabset>
				</div>
			</div>
		</div>

		<div class="col-md-12" id="top">
			<div class="col-md-12" ng-if='myTbHide1'>&nbsp;</div>
			<br />
			<!-- Info page code start -->
			<div class="col-md-12 padding-one"
				ng-hide="myTbHide1 && !(activeTab == 'Overview')">
				<fieldset ng-disabled='disableOverview || disableDataSource == "1"'>
					<div class="col-md-6">
						<div class="col-md-12 pad-zero tab-right">
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>ID</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.dataSourceId}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Name</b>
								</div>
								<div class="col-md-8 info-content">
									{{dataSourceInstance.dataSourceName}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Data source group</b>
								</div>
								<div class="col-md-8">{{dataSourceInstance.dataRepoName ==
									'null' ? "-" : dataSourceInstance.dataRepoName }}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-content">
									<b>Ingestion method</b>
								</div>
								<div class="col-md-8">{{datasource.ingection == 'quick' ?
									'Basic' : 'Advanced'}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Project Name</b>
								</div>
								<div class="col-md-8 info-content">{{projectName}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Category</b>
								</div>
								<div class="col-md-8 info-content">
									<!-- {{datasource.category == 'null' ? "-" : datasource.category}} -->
									<span class="ui-select" style="left: -5px; width: 100%;">
										<select class="dropdown-width" ng-model="datasource.category"
										ng-options="category for category in categories | orderBy: 'toString()'" 
										ng-change="changeCategory($index,datasource.category)">
											<option value="">Select category type</option>
									</select>
									</span>
								</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Sub-Category</b>
								</div>
								<div class="col-md-8 info-content">
									<!-- {{datasource.subCategory == 'null' ? "-" : datasource.subCategory}} -->
									<span class="ui-select" style="left: -5px; width: 100%;">
										<select class="dropdown-width" ng-model="datasource.subCategory"
										ng-options="category for category in categoriesDetails[datasource.category] | orderBy: 'toString()'"
										ng-change="changeSubCategory($index,datasource.subCategory)">
											<option value="">Select sub-category type</option>
									</select>
									</span>
								</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Description</b>
								</div>
								<div class="col-md-8 info-content">
									<!-- {{dataSourceInstance.description == 'null' ? "-" : dataSourceInstance.description}} -->
									<textarea class="form-control" rows="1" cols="20"
										placeholder="Description"
										style="width: 100%; resize: vertical;"
										ng-model="datasource.description"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="col-md-12 pad-zero">
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Total records</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.totalRecords}}</div>
							</div>

							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Type</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.dataSourceType
									| contentType}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Created by</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.createdBy}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Created on</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.createdDate
									| date : 'medium'}}</div>
							</div>
							<div class="col-md-12 height-info">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Refreshed by</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.lastRunBy
									== 'null' ? "-" : dataSourceInstance.lastRunBy}}</div>
							</div>
							<div class="col-md-12">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Last refreshed on</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.updatedDate
									== 'null' ? " 0" : dataSourceInstance.updatedDate | date :
									'medium'}}</div>
							</div>
							<div class="col-md-12">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Size</b>
								</div>
								<div class="col-md-8 info-content">{{dataSourceInstance.size
									| sizeConvert}}</div>
							</div>
							<div class="col-md-12">&nbsp;</div>
							<div class="col-md-12">
								<div class="col-md-4 info-header">
									<b>Tags</b>
								</div>
								<!-- datasource.tags -->
								<div class="col-md-8 info-content">
									<div class="col-md-12 pad-zero">
										<ui-select multiple ng-model="datasource.tags"
											style="width: 100%;" theme="bootstrap"
											class="select-formatter" data-ng-required="true"
											ng-disabled='disabled'> <ui-select-match
											placeholder="Add tags">{{$item}}</ui-select-match> <ui-select-choices
											repeat="tag in tagNames | filter: $select.search">
										<div ng-bind-html="tag"></div>
										</ui-select-choices> </ui-select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">&nbsp;</div>
					<div class="col-md-12" style="right: 30px !important;">
						<div class="col-md-12">
							<button class="btn btn-primary saveCss"
								ng-click="saveFieldMapping(datasource.fieldMappings,'Details')">Save</button>
						</div>
					</div>
				</fieldset>
			</div>
			<!-- Info page code End -->

			<!-- Fields start -->
			<div class="col-md-12 padding-one"
				ng-show="activeTab === 'Meta data'">
				<div class="col-md-12" ng-if="dataSourceObject.dataSourceType !== 'COGNOS_DATA_SOURCE'">
					<!-- other connector meta data html start -->
					<div  class="table-dynamic"
						style="overflow-y: auto; max-height: 500px; padding-top: 26px;">
						<table
							class="table table-bordered table-striped table-responsive table-repo pad-popup">
							<thead>
								<tr ng-hide="datasource.fieldMappings.length == 0">
									<th ng-hide="datasource.fieldMappings.length == 0"><div
											class="th">#</div></th>
									<th><div class="th">Field name</div></th>
									<th><div class="th">Data type</div></th>
									<th ng-init='check = []'><div class="th">Format</div></th>
									<th><div class="th">Description</div></th>
									<th><div class="th">Key</div></th>
									<th><div class="th">PII</div></th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="fm in datasource.fieldMappings track by $index">
									<td>{{$index + 1}}</td>
									<td><span>{{fm.fieldName}}</span></td>
									<td><span>{{fm.fieldDataType}}</span></td>
									<td><span>{{fm.format}}</span></td>
									<td><span>{{fm.descriptions}}</span></td>
									<td><span>{{fm.key | convertToBooleanToYesNo}}</span></td>
									<td><span>{{fm.pIIId | convertToBooleanToYesNo}}</span></td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- other connector meta data html end -->
					
				</div>
				
				
				<div class="col-md-12" ng-if="dataSourceObject.dataSourceType === 'COGNOS_DATA_SOURCE'">
					<div class="col-md-12" >
						<h5>Tables</h5>
						<div  class="table-dynamic"
							style="overflow-y: auto; max-height: 500px; padding-top: 26px;">
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup">
								<thead>
									<tr ng-hide="datasource.fieldMappings.length == 0">
										<th><div
												class="th">#</div></th>
										<th><div
												class="th">Table name</div></th>
										<th><div class="th">Column name</div></th>
										<th><div class="th">Business name</div></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="(key , value) in datasource.fieldMappings[0].reportPublishData.tables track by $index">
										<td>{{$index + 1}}</td>
										<td>{{key}}</td>
										<td>
											<table>
									        	<tr ng-repeat="obj in value">
									            	<td>{{obj.columnName}}</td>
									            </tr>
									        </table>
										</td>
										<td>
											<table>
									        	<tr ng-repeat="obj in value">
									            	<td>{{obj.columnBusinessName ? obj.columnBusinessName : '-'}}</td>
									            </tr>
									        </table>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12" ng-if="dataSourceObject.dataSourceType === 'COGNOS_DATA_SOURCE' && joinRelationChartTreeData.length > 0" >
						<h5>Joins</h5>
						<div ng-if="dataSourceObject.dataSourceType === 'COGNOS_DATA_SOURCE' && dataSourceObject.fieldMappings.length > 0" tree-view chart-data="joinRelationChartTreeData"></div>
					</div>	
				</div>
			</div>
			<!-- Fields End -->

			

			<!-- profile Page Tab -->
			<div class="col-md-12 padding-one"
				ng-if="activeTab === 'Profiling' ">
				<div ng-include="'views/explore/profileData.jsp'"></div>
			</div>
			<!-- end of Profile Page Tab -->

			<!-- File Format Start -->
			<div class="col-md-12 padding-one"
				ng-hide="myTbHide1 && !(activatedSubMenu == 'Adv. Settings')">
				<section class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<strong> Define data-at-rest </strong>
						</div>
						<div class="row border-line"></div>
						<div class="col-md-12">
							<!-- <div class="col-md-12 noteCss" role="alert">
                                                                <span
                                                                        ng-bind-html='explanation["headingForFileFormat"] | unsafe'></span>
                                                        </div> -->
							<!-- tooltip-placement="right"
                                                                                tooltip='Data-Rest file stored in Hdfs' -->
							<div class="form-group"
								ng-init="datasource.fileStoreObject = '{{storageStrategy}}'">
								<fieldset class="col-sm-7">
									<label class="ui-radio"> <input
										value="{{storageStrategy}}"
										ng-model="datasource.fileStoreObject" name="fileStore"
										type="radio" checked="checked"> <span>
											{{storageStrategy}} </span>
									</label> <label class="ui-radio" tooltip-placement="right"
										tooltip='Data-Rest file stored in Hbase'
										ng-hide="datasource.config.configConnectionType == 'NOSQL'">
										<input value="HBASE" ng-model="datasource.fileStoreObject"
										name="fileStore" type="radio"> <span> HBASE </span>
									</label> <label class="ui-radio"
										ng-if='datasource.dataSourceType == "RDBMS_DATA_SOURCE" || datasource.dataSourceType =="NOSQL"'
										tooltip-placement="right"
										tooltip='Data processed directly from source'> <input
										value="SOURCE" ng-model="datasource.fileStoreObject"
										name="fileStore" type="radio"> <span> SOURCE </span>
									</label>
								</fieldset>
								<div class="col-md-12">&nbsp;</div>
								<!-- tooltip-placement="right" tooltip='{{explanation["dropDownFileType"]}}' -->
								<div class="col-sm-12"
									ng-if="datasource.fileStoreObject != 'HBASE' && datasource.fileStoreObject != 'SOURCE'"
									style="margin-bottom: 8px;">
									<label class="col-sm-2" style="margin-left: -10px;">File
										type: </label>
									<div class="col-sm-4">
										<div class="col-sm-3 element">
											<span class="ui-select"> <select name="fileType"
												ng-options="maxiqCommonObj.keyOf as maxiqCommonObj.keyOf for maxiqCommonObj in maxiqCommonsAdvUnique "
												ng-model="datasource.dataAtRestFileType" class="fontSize"
												required>
													<option value="">Select file type</option>
											</select>
											</span>
										</div>
									</div>
									<label class="col-sm-2" style="margin-left: -10px;"
										ng-if="datasource.dataAtRestFileType!=undefined || datasource.dataAtRestFileType!=null">Compression
										type: </label>
									<div class="col-sm-4" style="margin-left: -27px;">
										<div class="col-sm-3"
											ng-if="datasource.dataAtRestFileType!=undefined || datasource.dataAtRestFileType!=null">
											<span class="ui-select"> <select
												name="compressionType"
												ng-options="maxiqCommonObj.value as maxiqCommonObj.value for maxiqCommonObj in maxiqCommonsAdv"
												ng-model="datasource.dataAtRestCompressionType" required>
													<option value="">Select Compress Type</option>
											</select>
											</span>
										</div>
									</div>
								</div>
								<!-- tooltip-placement="right" class="fontSize"
                                                                                                tooltip='{{explanation["dropDownCompressionType"]}}' -->
								<div class="col-md-12">
									<div class="col-md-6"
										ng-if="datasource.fileStoreObject == 'HBASE'"
										ng-repeat="index in datasource.primaryKeysForIndex"
										style="margin-bottom: 8px;">
										<div class="col-sm-5" ng-if="$index == 0"
											style="margin-left: -24px;">
											<label class="col-sm-12"> Select primary index : </label>
										</div>
										<div class="col-sm-5" ng-if="$index != 0"
											style="margin-left: -24px;">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-6"
											style="margin-left: -63px; margin-right: -35px;">
											<span class="ui-select"> <select class="disabledPoint"
												name="selectPrimaryKey" ng-disabled="true"
												ng-options="value.fieldName for value in selectRejectValueForPrimary[$index] track by value.fieldName"
												ng-model="index" required>
													<option value="">Select primary key</option>
											</select></span>
										</div>
										<div class="col-md-1">
											<a href="javascript:;" style="line-height: 3;"> <i
												class="fa fa-minus-circle fa-lg"
												ng-click="deletePrimaryValue($index)"></i>
											</a>
										</div>

									</div>

									<div class="col-md-6"
										ng-if="datasource.fileStoreObject == 'HBASE'"
										style="margin-bottom: 8px;">
										<div class="col-sm-5" style="margin-left: -24px;"
											ng-if="datasource.primaryKeysForIndex.length == 0">
											<label class="col-sm-12">Select primary key: </label>
										</div>
										<div class="col-sm-5" style="margin-left: -24px;"
											ng-if="datasource.primaryKeysForIndex.length != 0">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-6"
											style="margin-left: -64px; margin-right: -35px;">
											<span class="ui-select"> <select
												ng-options="value.fieldName for value in selectRejectValueForPrimary[selectRejectValueForPrimary.length - 1] track by value.fieldName"
												ng-model="hbase.primary" tooltip-placement="right"
												tooltip='' required>
													<option value="">Select primary key</option>
											</select></span>
										</div>
										<div class="col-sm-1">
											<a style="line-height: 3;" href="javascript:;"
												ng-click="addPrimaryKeyValue(hbase.primary, selectRejectValueForPrimary.length - 1)">
												<i class="fa fa-plus-circle fa-lg"></i>
											</a>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<div class="col-md-6"
										ng-if="datasource.fileStoreObject == 'HBASE'"
										ng-repeat="index in datasource.secondaryKeys"
										style="margin-bottom: 8px;">
										<div class="col-sm-5" ng-if="$index == 0"
											style="margin-left: -24px;">
											<label class="col-sm-12"> Add secondary index : </label>
										</div>
										<div class="col-sm-5" ng-if="$index != 0"
											style="margin-left: -24px;">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-6"
											style="margin-left: -63px; margin-right: -35px;">
											<span class="ui-select"> <select class="disabledPoint"
												name="selectSecondaryKey" ng-disabled="true"
												ng-options="value.fieldName for value in selectRejectValueForSecondary[$index] track by value.fieldName"
												ng-model="index" tooltip-placement="right" tooltip=''
												style="cursor: not-allowed;" required>
													<option value="">Select secondary key</option>
											</select></span>
										</div>
										<div class="col-md-1">
											<a href="javascript:;" style="line-height: 3;"> <i
												class="fa fa-minus-circle fa-lg"
												ng-click="deleteSecondaryValue($index)"></i>
											</a>
										</div>

									</div>
									<div class="col-sm-6"
										ng-if="datasource.fileStoreObject == 'HBASE'"
										style="margin-bottom: 8px;">
										<div class="col-sm-5" style="margin-left: -24px;"
											ng-if="datasource.secondaryKeys.length == 0">
											<label class="col-sm-12"> Add secondary index : </label>
										</div>
										<div class="col-sm-5" style="margin-left: -24px;"
											ng-if="datasource.secondaryKeys.length != 0">
											<label class="col-sm-2"> &nbsp;</label>
										</div>
										<div class="col-sm-6"
											style="margin-left: -64px; margin-right: -35px;">
											<span class="ui-select"> <select
												name="selectSecondaryKey"
												ng-options="value.fieldName for value in selectRejectValueForSecondary[selectRejectValueForSecondary.length - 1] track by value.fieldName"
												ng-model="hbase.secondary" tooltip-placement="right"
												tooltip='' required>
													<option value="">Select secondary key</option>
											</select></span>
										</div>
										<div class="col-sm-1">
											<a style="line-height: 3;" href="javascript:;"
												ng-click="addSecondaryKeyValue(hbase.secondary, selectRejectValueForSecondary.length - 1)">
												<i class="fa fa-plus-circle fa-lg"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="col-md-12 visual-chk"
									ng-if="datasource.fileStoreObject != 'SOURCE'">
									<label class="ui-checkbox"> <input type="checkbox"
										class="toggle-task" ng-model="datasource.saveForVisualize">
										<span> Save for visualization</span>
									</label>
								</div>
								<div class="col-sm-12 form-group">
									<button type="submit" class="btn btn-primary saveCss"
										ng-click="saveRestFile()">Save</button>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>

			<!-- File Format End -->

			<!-- Profiling Option starts here-->
			<div class="col-md-12 padding-one" ng-hide="true">
				<section class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<strong> Profiling option </strong>
						</div>
						<div class="row border-line"></div>
						<div class="col-md-12">
							<!-- <div class="col-md-12 noteCss" role="alert">
                                           <span ng-bind-html='explanation["profilingHeader"] | unsafe'></span>
                                      </div> -->
							<!-- tooltip-placement="right"
                                                tooltip='{{explanation["profilingCheck"]}}' -->
							<div class="form-group">
								<div class="row">
									<div class="col-md-4">
										<label class="ui-checkbox"> <input type="checkbox"
											class="toggle-task" ng-model="datasource.preIngestion"
											ng-disabled="datasource.ingection == 'quick' || checkDataType  == 'FIXED_LENGTH_FORMAT'">
											<!-- ng-change="actInactAll('pre' , datasource.preIngestion)" -->
											<!-- <div class="space"></div> --> <span>
												Pre-ingestion</span>
										</label> <label class="ui-checkbox"> <input type="checkbox"
											class="toggle-task" ng-model="datasource.postIngestion">
											<!-- ng-change="actInactAll('post' , datasource.postIngestion)" -->
											<!-- <div class="space"></div> --> <span>Post-ingestion</span>
										</label>
									</div>
									<div></div>
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary"
										style="float: right;" ng-click="saveProfilingOption()">Save</button>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-12 padding-one"
				data-ng-controller="connectionController"
				ng-hide="myTbHide1 && !(activatedSubMenu == 'Connection')">
				<section class="col-md-12">
					<!-- <div class="panel-heading">
                              <strong> <span class="glyphicon glyphicon-th"></span>
                                   Connection
                              </strong>
                         </div> -->
					<!-- <div class="col-md-12 noteCss" role="alert">
                             <span ng-bind-html='explanation["connHeader"] | unsafe'></span>
                         </div> -->
					<div class="col-md-6">
						<form id="connectionForm" name="connectionForm"
							class="form-horizontal form-validation" novalidate>
							<fieldset>
								<!--<div class="col-md-12">
                                              <div class="col-md-8" tooltip-placement="right"
                                                  tooltip='{{explanation["dataSourceType"]}}'> -->

								<div class="col- md-12 form-group"
									ng-if="dataSourceInstance.flowType !== 'STREAM'">
									<div class="col-md-4">
										<label class="pull-left">Content type</label>
									</div>
									<div class="col-md-8">
										<span class="ui-select ds-dropdown"> <select
											ng-model="fileDataIngesterDetails.dataSourceType"
											ng-change="myTest(fileDataIngesterDetails.dataSourceType)"
											required>
												<option value=''>Select data source type</option>
												<option value="FILE_DATA_SOURCE">Delimited file</option>
												<option value="RDBMS_DATA_SOURCE">RDBMS</option>
												<option value="TSV_DATA_SOURCE">TSV</option>
												<option value="CSV_DATA_SOURCE">CSV</option>
												<option value="EXCEL_DATA_SOURCE">Excel sheet</option>
												<option value="FIXED_LENGTH_FORMAT">Fixed length
													format</option>
												<option ng-if="datasource.ingection == 'typical'"
													value="EXTERNAL_DATA">External data</option>
												<option value="FTP_DATA">FTP/SFTP</option>
												<option value="REMOTE_DATA">REMOTE_DATA</option>
												<option ng-if="datasource.ingection != 'typical'"
													value="NOSQL">NOSQL</option>
										</select></span>
									</div>
								</div>

								<div class="col-md-12 form-group"
									ng-if="dataSourceInstance.flowType === 'STREAM'">
									<div class="col-md-4">
										<label style="float: left;">Content type</label>
									</div>
									<div class="col-md-8">
										<span class="ui-select" style="margin-left: 0%; width: 103%;">
											<select ng-model="fileDataIngesterDetails.dataSourceType"
											ng-change="" required>
												<option value=''>Select data source type</option>
												<option value='STREAM_DS'>STREAM_DS</option>
										</select>
										</span>
									</div>
								</div>
								<div class="col-md-12 form-group"
									ng-if="fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
									<div class="col-md-4">
										<label style="float: left;">Stream type</label>
									</div>
									<div class="col-md-8">
										<span class="ui-select" style="margin-left: 0%; width: 103%;">
											<select name="connectorType"
											ng-change="selectTheStreamType(datasource.streamType);"
											ng-options="connection for connection in connectionsDetails
												  track by connection"
											ng-model="datasource.streamType" required>
												<option value="">Select stream type</option>
										</select>
										</span>
									</div>
								</div>
								<div class="col-md-12 form-group"
									ng-if="datasource.streamType != null && datasource.streamType != undefined
								   && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
									<div class="col-md-4">
										<label style="float: left;">Connection name</label>
									</div>
									<!-- tooltip-placement="right"
											tooltip='{{explanation["dropDownForConnection"]}}'  -->
									<div class="col-md-8">
										<span class="ui-select" style="margin-left: 0%; width: 103%;">
											<select name="connectorName"
											ng-options="connection.conncetionName for connection in connectionsNameDetails 
											 track by connection.conncetionName"
											ng-change="setConncetionName(connObj.connectionName)"
											ng-model="datasource.config" required>
												<option value="">Select connection</option>
										</select>
										</span>
									</div>
								</div>
								<div class="col-md-12 form-group"
									ng-if="datasource.config !== '' 
							  			&& datasource.config !== undefined && datasource.config !== null
							  			&& fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
									<div class="col-md-4">
										<label style="float: left;">URL</label>
									</div>
									<div class="col-md-8">{{datasource.config.url}}</div>
								</div>
								<!-- </div> 
                                        </div>-->
								<div class="col-md-12"
									ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
									<div id="rdbmsDataSource" class="something2 animate-fade-up"
										ng-show="fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA' ">
										<div class="row">
											<div class="form-group">
												<label class="col-sm-4">External connection type </label>
												<div class="col-sm-8">
													<span class="ui-select external-css"> <select
														name="datasource.externalDataType"
														ng-model="datasource.externalDataType"
														ng-change="getExternalConnecton(datasource.externalDataType)"
														tooltip-placement="right" tooltip='' required>
															<option value="">Select connection Type</option>
															<option value="GoogleAnalytics">Google Analytics</option>
															<option value="SALESFORCE">SALESFORCE</option>
															<option value="AMAZONS3">AMAZONS3</option>
															<option value="FACEBOOKPAGE">FACEBOOK PAGE</option>
															<option value="DROPBOX">DROPBOX</option>
															<option value="HECKYL">HECKYL</option>
															<option value="PROBE42">PROBE42</option>
															<option value="SAPERP">SAPERP</option>
													</select>
													</span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4">External connection</label>
												<div class="col-sm-8">
													<span class="ui-select external-css"> <select
														name="conncetionName"
														ng-options="connection.conncetionName for connection in externalConnections track by connection.conncetionName"
														ng-model="datasource.config" tooltip-placement="right"
														tooltip='' required>
															<option value="">Select external connection</option>
													</select>
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div id="rdbmsDataSource" class="something2 animate-fade-up"
										ng-show="fileDataIngesterDetails.dataSourceType == 'NOSQL' ">
										<div class="row">
											<div class="form-group">
												<label class="col-sm-4">NOSQL Type </label>
												<div class="col-sm-8">
													<span class="ui-select external-css"> <select
														name="NoSqlConnectionType" ng-model="NoSqlConnectionType"
														ng-change="getNoSqlConnections(NoSqlConnectionType)"
														tooltip-placement="right" tooltip='' required>
															<option value="EMPTY">Select NoSQL Type</option>
															<option value="MONGODB">MONGO DB</option>
													</select>
													</span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4">Connections</label>
												<div class="col-sm-8">
													<span class="ui-select external-css"> <select
														name="datasource.connectionName"
														ng-model="datasource.config"
														ng-options="connection.conncetionName for connection in NoSqlConnections track by connection.conncetionName"
														tooltip-placement="right" required>
															<option value="">Select NoSql connections</option>
													</select>
													</span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4">Schema List</label>
												<div class="col-sm-8">
													<span class="ui-select external-css"> <input
														type="text" ng-model="schema"
														placeholder="Mongo Collections" class="form-control">
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="divider-six-height"></div>
								<div class="col-md-12">
									<div id="rdbmsDataSource" class="something2 animate-fade-up"
										ng-show="fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE' 
										|| fileDataIngesterDetails.dataSourceType == 'FTP_DATA'">
										<div class="row">
											<div class="form-group">
												<label class="col-sm-4">Connection </label>
												<div class="col-sm-8">
													<span class="ui-select input-width-left"> <select
														name="connectorType"
														ng-options="connection.conncetionName for connection in connectionsDetails track by connection.conncetionName"
														ng-model="datasource.config" required>
															<option value="">Select connection</option>
													</select>
													</span>
												</div>
											</div>
											<!--  tooltip-placement="right"
														tooltip='{{explanation["dropDownForConnection"]}}' -->

											<!-- File Format Start -->
											<div class="col-md-12">&nbsp;</div>
											<div class="form-group"
												ng-if="fileDataIngesterDetails.dataSourceType != 'FTP_DATA'">
												<label class="col-sm-4">File format </label>
												<div class="col-sm-8" style="padding-left: 6px;">
													<span class="ui-select dropdown-file"> <select
														name="fileType"
														ng-options="maxiqCommonObj.keyOf as maxiqCommonObj.keyOf for maxiqCommonObj in maxiqCommonsUnique"
														ng-model="datasource.config.inputFileType"
														tooltip-placement="right"
														tooltip='{{explanation["dropDownForFileType"]}}'
														ng-disabled='true' required>
															<option value="">Select file type</option>
													</select>
													</span>
												</div>
												<div class="col-md-12">&nbsp;</div>
												<div class="col-md-4"></div>
												<div class="col-sm-8" style="padding-left: 6px;"
													ng-if="datasource.config.inputFileType!=undefined || datasource.config.inputFileType!=null"
													style="padding-left: 4%;">
													<span class="ui-select dropdown-file"> <select
														name="compressionType"
														ng-options="maxiqCommonObj.value as maxiqCommonObj.value for maxiqCommonObj in maxiqCommons | filter:datasource.config.inputFileType"
														ng-model="datasource.config.inputCompressionType"
														tooltip-placement="right"
														tooltip='{{explanation["dropDownForCompression"]}}'
														ng-disabled='true' required>
															<option value="">Select compress type</option>
													</select>
													</span>
												</div>
											</div>

											<!-- File Format End -->

											<div class="form-group"
												ng-if='datasource.config != null && datasource.config != undefined && fileDataIngesterDetails.dataSourceType == "FTP_DATA"'>
												<label class="col-sm-4">Default location </label>
												<div class="col-sm-8 textboxCss">
													<input type="text" class="form-control"
														name="defaultLocation" placeholder="File Path"
														ng-model="datasource.config.defaultLocationForFTP"
														required>
												</div>
											</div>
											<div class="form-group"
												ng-if="datasource.config != null && datasource.config != undefined && fileDataIngesterDetails.dataSourceType == 'FTP_DATA'">
												<div class="col-md-4">
													<label for="label-focus" style="float: left;">Delimeter<span
														class="field-warning">*</span></label>
												</div>
												<div class="col-md-8 textboxCss">
													<input type="text" class="form-control" name="delimiter"
														placeholder="Delimiter"
														ng-model="fileDataIngesterDetails.delimiter"
														ng-disabled="false" required> <span
														style="color: red">{{delimiterError}}</span>
												</div>
											</div>
											<!-- tooltip-placement="right" tooltip='{{explanation["optionallyEnclosedInDoubleQuotes"]}}'-->
											<div class="col-md-12 form-group"
												ng-if="fileDataIngesterDetails.dataSourceType == 'FTP_DATA' && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA'">
												<div class="col-md-4"></div>
												<div class="col-md-8">
													<label class="ui-checkbox"> <input type="checkbox"
														class="toggle-task"
														ng-model="datasource.config.optionallyEnclosedInDoubleQuotes">
														<span>Optionally enclosed in double quotes ?</span></label>
												</div>
											</div>
											<div class="col-md-12">&nbsp;</div>
											<div class="form-group"
												ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'>
												<label class="col-sm-4">Split key </label>
												<div class="col-sm-8 rdbmsCss">
													<input type="text" id=""
														class="form-control split-key-width" name="primaryKey"
														rows="4" ng-model="datasource.config.dbPrimaryKey"
														placeholder="Enter primary key" required />
												</div>
											</div>


											<div class="form-group"
												ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'>
												<div class="col-sm-4"></div>
												<div class="col-sm-8" style="margin-left: 35.5%;"></div>
											</div>
											<div class="form-group"
												ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'
												style="margin-top: -16px !important;">
												<label class="col-sm-4">Query </label>
												<div class="col-sm-8"
													style="padding-left: 12px; padding-right: 0px;">
													<textarea id="rdbmsNode" style='height: {{myCssApply}}; width: 76%;'
														class="form-control ng-pristine ng-valid" name="query"
														rows='{{rowSize}}'
														ng-model="datasource.config.queryToExecute"
														placeholder="Write your query here" required></textarea>
													<button ng-click="fullTextarea('customQuery');"
														class="btn btn-default btn-fullscreen max-btn"
														style="margin-top: -27px;" type="button"
														tooltip="Fullscreen" tooltip-placement="top"
														id="fullscreen-button">
														<i class="fa fa-arrows-alt"></i>
													</button>
													<button ng-click="resizeText(4);"
														class="btn btn-default btn-textarea max-btn" type="button"
														tooltip="Grow Result" tooltip-placement="top">
														<i class="fa fa-plus"></i>
													</button>
													<button ng-click="sizeChanger(-4);"
														class="btn btn-default btn-minus min-btn" type="button"
														tooltip="Shrink Result" tooltip-placement="top">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div id="firlDataSource" class="something  animate-fade-up"
										ng-show="fileDataIngesterDetails.dataSourceType == 'FILE_DATA_SOURCE' || 
                                             fileDataIngesterDetails.dataSourceType == 'FIXED_LENGTH_FORMAT' || 
                                             fileDataIngesterDetails.dataSourceType == 'CSV_DATA_SOURCE' || 
                                             fileDataIngesterDetails.dataSourceType == 'TSV_DATA_SOURCE' ||
                                             fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'|| 
                                             fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
										<!-- <div class="col-md-8" tooltip-placement="right" ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
                                                       tooltip='{{explanation["serverFilePath"]}}'>
                                                       <div class="form-group"> -->
										<div class="col-md-12 form-group"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
											<div class="col-md-4">
												<label for="label-focus pull-left">File path</label>
											</div>
											<div class="col-md-8 padding-textBox">
												<input input type="text" style="margin-left: 6px;"
													class="form-control filePath-textBox" name="filepath"
													ng-model="fileDataIngesterDetails.serverFilePath"
													placeholder="File path" id="label-focus"><br>
												<span style="color: red">{{filePathError}}</span>
											</div>
										</div>
										<div class="col-md-12"
											style="top: -23px; margin-bottom: -35px !important;"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
											<div class="col-md-4">&nbsp;</div>
											<div class="col-md-3 left-line"></div>
											<span class="or-Css">OR</span>
											<div class="col-md-3 right-line"></div>
											<div class="col-md-2"></div>
										</div>
										<!-- <div class="col-md-12" ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">&nbsp;</div> -->
										<div class="col-md-12 form-group" style="margin-top: -8px;"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
											<div class="col-md-4">
												<label class="pull-left">Upload file</label>
											</div>
											<div class="col-md-8">
												<input class="btn btn-default btn-block-data ds-upload"
													type="file" id="uploadFile" title="Choose File"
													ng-model="myFile" ng-file-select="" resetOnClick="true"
													ng-change="resetFilePath();" ng-multiple="false"
													ng-model-rejected="rejFiles" />
											</div>
										</div>

										<!-- Header Options -->
										<!-- tooltip-placement="right"
                                                       tooltip='{{explanation["containsHeader"]}}' -->
										<div class="col-md-12 form-group" style="margin-bottom: 0px;"
											ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE' && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA'">
											<div class="col-md-4">
												<label style="float: left;">Contains header</label>
											</div>
											<div class="col-md-8" style="left: 5px;">
												<label class="ui-radio"> <input id="No" type="radio"
													name="radio"
													ng-model="fileDataIngesterDetails.containsHeader"
													value="false" ng-disabled='dataHide' required> <span>
														No </span>
												</label> <label class="ui-radio"> <input id="Yes"
													type="radio" name="radio"
													ng-model="fileDataIngesterDetails.containsHeader"
													value="true" ng-disabled='dataHide'> <span>
														Yes </span>
												</label><span style="color: red"></span>
											</div>
										</div>
										<!-- tooltip-placement="right" tooltip='{{explanation["containsHeader"]}}' -->
										<div class="col-md-12"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
											<div class="form-group"
												ng-show="fileDataIngesterDetails.containsHeader == 'false' &&
                                                                 fileDataIngesterDetails.dataSourceType == 'FIXED_LENGTH_FORMAT'">
												<div class="col-md-4">
													<label for="label-focus pull-left">Header template</label>
												</div>
												<div class="col-sm-8" style="padding-left: 11px !important;">
													<textarea id="" class="form-control ng-pristine ng-valid"
														name="headerTemplate" rows="4" style="width: 80%;"
														ng-model="fileDataIngesterDetails.headerTemplate"
														placeholder="field1[Integer][length1],field2[String][length2],field3[Date][length3],..."
														required></textarea>
												</div>
											</div>
										</div>


										<div class="col-md-12 form-group" tooltip-placement="right"
											ng-show="(fileDataIngesterDetails.recordStartsWith == 'true' || !'dataHide' ) 

                                                  && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE' && fileDataIngesterDetails.dataSourceType !== 'REMOTE_DATA'">
											<!-- tooltip='{{explanation["startingRegularExpre"]}}' -->


											<div class="col-md-4">
												<label for="label-focus" style="float: left;">
													Regular expression</label>
											</div>
											<div class="col-md-8 padding-textBox">
												<input type="text" class="form-control" id="label-focus"
													style="margin-left: 6px; width: 103%;"
													ng-model="fileDataIngesterDetails.startingRegularExpre"
													required>

											</div>

										</div>
										<!-- tooltip='{{explanation["delimiter"]}}' -->
										<div class="col-md-12"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
											<div class="form-group"
												ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' && 
                                                            fileDataIngesterDetails.dataSourceType != 'TSV_DATA_SOURCE' && 
                                                            fileDataIngesterDetails.dataSourceType != 'CSV_DATA_SOURCE'
                                                            && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE'">
												<div class="col-md-4">
													<label for="label-focus pull-left">Delimiter<span
														class="field-warning">*</span></label>
												</div>
												<div class="col-md-8 textboxCss">
													<input type="text" class="form-control" name="delimiter"
														placeholder="Delimiter"
														ng-model="fileDataIngesterDetails.delimiter" required>
													<span style="color: red">{{delimiterError}}</span>
												</div>
											</div>
										</div>
										<div class="col-md-12" tooltip-placement="right"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'"
											tooltip='{{explanation["delimiter"]}}'>
											<div class="form-group"
												ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' &&
                                                            fileDataIngesterDetails.dataSourceType == 'CSV_DATA_SOURCE'">
												<div class="col-md-4">
													<label for="label-focus" style="float: left;">Delimiter<span
														class="field-warning">*</span></label>
												</div>
												<div class="col-md-8 textboxCss"
													ng-init="fileDataIngesterDetails.delimiter = ','">
													<input type="text" class="form-control" name="delimiter"
														placeholder="Delimiter"
														ng-model="fileDataIngesterDetails.delimiter"
														ng-disabled="true" required> <span
														style="color: red">{{delimiterError}}</span>
												</div>
											</div>
										</div>
										<!--tooltip='{{explanation["delimiter"]}}'  tooltip-placement="right"-->
										<div class="col-md-12"
											ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA'">
											<div class="form-group"
												ng-if="fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' &&
                                                            fileDataIngesterDetails.dataSourceType == 'TSV_DATA_SOURCE'">
												<div class="col-md-4">
													<label for="label-focus" style="float: left;">Delimeter<span
														class="field-warning">*</span></label>
												</div>
												<div class="col-md-8 textboxCss"
													ng-init="fileDataIngesterDetails.delimiter = '\\t'">
													<input type="text" class="form-control" name="delimiter"
														placeholder="Delimiter"
														ng-model="fileDataIngesterDetails.delimiter"
														ng-disabled="true" required> <span
														style="color: red">{{delimiterError}}</span>
												</div>
											</div>
										</div>
										<!-- tooltip='{{explanation["optionallyEnclosedInDoubleQuotes"]}}'
                                                            tooltip-placement="right" -->
										<div class="col-md-12 form-group"
											style="margin-bottom: -10px;"
											ng-if="'EXCEL_DATA_SOURCE' != fileDataIngesterDetails.dataSourceType && 'FIXED_LENGTH_FORMAT' != fileDataIngesterDetails.dataSourceType && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA' && 'RDBMS_DATA_SOURCE' != fileDataIngesterDetails.dataSourceType || fileDataIngesterDetails.dataSourceType == 'FTP_DATA'">
											<div class="col-md-4"></div>
											<div class="col-md-8 checkBoxAlignment">
												<label class="ui-checkbox"> <input type="checkbox"
													class="toggle-task"
													ng-model="fileDataIngesterDetails.optionallyEnclosedInDoubleQuotes">
													<span>Optionally enclosed in double quotes ?</span></label>
											</div>
										</div>
										<!--tooltip-placement="right" tooltip='{{explanation["isDelimete"]}}' -->
										<div class="col-md-12 form-group"
											style="margin-bottom: -10px;"
											ng-if="fileDataIngesterDetails.dataSourceType == 'FILE_DATA_SOURCE' && !dataHide && fileDataIngesterDetails.dataSourceType != 'REMOTE_DATA' && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE'">
											<div class="col-md-4"></div>
											<div class="col-md-8 checkBoxAlignment">
												<label class="ui-checkbox"> <input type="checkbox"
													class="toggle-task"
													ng-model="fileDataIngesterDetails.isDelimiterRegEx">
													<span>Is delimiter a regular expression ?</span></label>
											</div>
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="col-md-6">
						<!-- tooltip-placement="right" tooltip='{{explanation["headerStarting"]}}'-->
						<div class="col-md-12 form-group"
							ng-if="datasource.config !== '' 
							  && datasource.config !== undefined && datasource.config !== null
							  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
							<div class="col-md-4">
								<label for="label-focus" style="float: left;">Header
									template</label>
							</div>
							<div class="col-sm-8" style="padding-right: 1px !important;">
								<textarea id="" class="form-control ng-pristine ng-valid"
									name="headerTemplate" rows="4"
									ng-model="fileDataIngesterDetails.headerTemplate"
									placeholder="field1[Integer],field2[String],field3[Date],..."
									required></textarea>
							</div>
						</div>
						<div class="col-md-12 form-group"
							ng-if="datasource.config !== '' 
							  && datasource.config !== undefined && datasource.config !== null
							  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
							<label class="col-sm-4">Topic name </label>
							<div class="col-sm-8 rdbmsCss">
								<input type="text" id=""
									class="form-control ng-pristine ng-valid topic-css"
									name="primaryKey" rows="4"
									ng-model="fileDataIngesterDetails.topicName"
									placeholder="Enter topic name" required />
							</div>
						</div>
						<div class="col-md-12 form-group"
							ng-if="datasource.config !== '' 
							  && datasource.config !== undefined && datasource.config !== null
							  && fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
							<div class="col-md-4">
								<label for="label-focus" style="float: left;">Delimeter<span
									class="field-warning">*</span></label>
							</div>
							<div class="col-md-8 textboxCss">
								<input type="text" class="form-control delimiter-css"
									name="delimiter" placeholder="Delimiter"
									ng-model="fileDataIngesterDetails.delimiter"
									style="width: 100% !important; margin-left: 4px !important;"
									ng-disabled="false" required> <span style="color: red">{{delimiterError}}</span>
							</div>
						</div>
						<div class="col-md-12">
							<div id="rdbmsDataSource" class="something2 animate-fade-up"
								ng-show="fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE' 
								|| fileDataIngesterDetails.dataSourceType == 'FTP_DATA'
								|| fileDataIngesterDetails.dataSourceType == 'NOSQL'">
								<div class="row form-group"
									ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
									<label class="col-sm-4">Connector type </label>
									<div class="col-sm-8">
										{{datasource.config.connectorType}}</div>
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="row form-group"
									ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
									<label class="col-sm-4">IP </label>
									<div class="col-sm-8">{{datasource.config.ip}}</div>
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="row form-group"
									ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
									<label class="col-sm-4">Port </label>
									<div class="col-sm-8">{{datasource.config.port}}</div>
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="row form-group"
									ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined'>
									<label class="col-sm-4">User name </label>
									<div class="col-sm-8">{{datasource.config.dbUserName}}</div>
								</div>
								<div class="col-md-12">&nbsp;</div>
								<div class="row form-group"
									ng-if='datasource.config != null && datasource.config != undefined && datasource.config.conncetionName != null && datasource.config.conncetionName != undefined && fileDataIngesterDetails.dataSourceType != "FTP_DATA"'>
									<label class="col-sm-4">DB name </label>
									<div class="col-sm-8">{{datasource.config.dbName}}</div>
								</div>
							</div>
						</div>
						<div class="col-md-12"
							ng-show="fileDataIngesterDetails.containsHeader == 'true' && 
                                 fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE' &&
                                 fileDataIngesterDetails.dataSourceType != 'EXTERNAL_DATA'">
							<div class="row form-group">
								<div class="col-md-4">
									<label for="label-focus pull-left">Header at<span
										class="field-warning">*</span>
									</label>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" id="label-focus"
										ng-model="fileDataIngesterDetails.headerStarting" required />
								</div>
							</div>
						</div>
						<!--tooltip-placement="right" tooltip='{{explanation["headerTemplate"]}}'-->
						<div class="col-md-12"
							ng-show="(fileDataIngesterDetails.containsHeader == 'false' &&
                                        fileDataIngesterDetails.dataSourceType != 'FIXED_LENGTH_FORMAT' &&
                                        fileDataIngesterDetails.dataSourceType != 'EXTERNAL_DATA' &&
                                        fileDataIngesterDetails.dataSourceType != 'RDBMS_DATA_SOURCE' &&
                                        fileDataIngesterDetails.dataSourceType != 'FTP_DATA' &&
                                        fileDataIngesterDetails.dataSourceType != 'STREAM_DS' &&
                                        fileDataIngesterDetails.dataSourceType != 'NOSQL')">
							<div class="row form-group">
								<div class="col-md-4">
									<label for="label-focus" style="float: left;">Header
										template</label>
								</div>
								<div class="col-sm-8"
									style="padding-left: 16px; padding-right: 5px;">
									<textarea id="" class="form-control" name="headerTemplate"
										rows="1" style="margin-left: -3px;"
										ng-model="fileDataIngesterDetails.headerTemplate"
										placeholder="field1[Integer],field2[String],field3[Date],..."
										required></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-12">&nbsp;</div>
						<div class="col-md-12"
							ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA' &&
                              		fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
							<div class="row form-group"
								ng-if="fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
								<div class="" ng-repeat="xls in xlsObj">
									<div
										ng-repeat="excel in fileDataIngesterDetails.excelRanges track by $index">
										<div class="col-md-4">
											<label style="float: left;">Excel range</label>
										</div>
										<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control" ng-trim="true"
													ng-pattern="/^.+$/" name="xlsSheetName"
													placeholder="Sheet name" ng-model="excel.xlsSheetName" />
											</div>
										</div>
										<div>&nbsp;</div>
										<div class="col-md-4">&nbsp;</div>
										<div class="col-md-2">
											<input type="text" class="form-control" ng-trim="true"
												ng-pattern="/^\S+$/" name="xlsFilePathFrom"
												placeholder="From" ng-model="excel.xlsFilePathFrom" />
										</div>
										<div class="col-md-2">
											<input type="text" class="form-control" ng-trim="true"
												ng-pattern="/^\S+$/" name="xlsFilePathTo" placeholder="To"
												ng-model="excel.xlsFilePathTo" />
										</div>
										<div class="col-md-3">
											<input type="checkbox" class="toggle-task"
												ng-model="excel.xlsHeaderOptions"> <label>Has
												header</label>
										</div>
										<div class="col-md-1">
											<a style="line-height: 3;" tooltip="Remove"
												tooltip-placement="right" ng-click="removeXlsField($index)">
												<i class="fa fa-trash fa-lg"></i>
											</a>
										</div>
									</div>
									<div>&nbsp;</div>

									<div class="col-md-4">
										<label style="float: left;">Excel range</label>
									</div>
									<!-- <div class="row"> -->
									<div class="col-md-8"
										style="padding-left: 14px; padding-right: 7px;">
										<input type="text" class="form-control" ng-trim="true"
											ng-pattern="/^.+$/" name="xlsSheetName"
											placeholder="Sheet name" ng-model="values.xlsSheetName" />
									</div>
									<!-- </div> -->
									<div>&nbsp;</div>
									<div class="col-md-4">&nbsp;</div>
									<div class="col-md-2">
										<input type="text" class="form-control" ng-trim="true"
											ng-pattern="/^\S+$/" name="xlsFilePathFrom"
											placeholder="From" ng-model="values.xlsFilePathFrom" />
									</div>
									<div class="col-md-2">
										<input type="text" class="form-control" ng-trim="true"
											ng-pattern="/^\S+$/" name="xlsFilePathTo" placeholder="To"
											ng-model="values.xlsFilePathTo" />
									</div>
									<div class="col-md-3">
										<input type="checkbox" class="toggle-task"
											ng-model="values.headerChkBox"> <label>Has
											header</label>
									</div>
									<div class="col-md-1">
										<a tooltip="Add" href="javascript:;" tooltip-placement="right"
											ng-click="addXlsField()"> <i
											class="fa fa-plus-circle fa-lg"></i>
										</a>
									</div>
									<div cass="col-md-1">
										<a style="line-height: 3;" href="javascript:;"
											ng-if="xlsObj.length>1" ng-click="removeXlsField(xls)"> <i
											class="fa fa-trash fa-lg"></i>
										</a>
									</div>
								</div>
								<span style="color: red">{{xlsPathError}}</span>
							</div>
						</div>
						<!-- tooltip-placement="right" tooltip='{{explanation["recordStarts"]}}'-->
						<div class="col-md-12"
							ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA' || 
                              	fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE' ||
                              	fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA' ||
                              	fileDataIngesterDetails.dataSourceType == 'FTP_DATA'||
                              	fileDataIngesterDetails.dataSourceType == 'NOSQL'">
							<div class="row form-group"
								ng-hide="dataHide || fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
								<div class="col-md-4">
									<label class="pull-left">Records starts</label>
								</div>
								<div class="col-md-8">
									<label class="ui-radio"> <input id="Line" type="radio"
										name="radio2"
										ng-model="fileDataIngesterDetails.recordStartsWith"
										ng-change="changeRegularExp()"
										value="false" required> <span> Line </span>
									</label> <label class="ui-radio"> <input id="RegExpre"
										type="radio" name="radio2"
										ng-model="fileDataIngesterDetails.recordStartsWith"
										value="true"> <span> Regular expression </span>
									</label><span style="color: red"></span>
								</div>
							</div>
						</div>
						<div class="col-md-12">&nbsp;</div>
						<!-- tooltip-placement="right" tooltip='{{explanation["recordStartLine"]}}' -->
						<div class="col-md-12"
							ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA' || 
                              	fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE' ||
                              	fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA' ||
                              	fileDataIngesterDetails.dataSourceType == 'FTP_DATA'||
                              	fileDataIngesterDetails.dataSourceType == 'NOSQL'">
							<div class="row form-group"
								ng-show="fileDataIngesterDetails.recordStartsWith == 'false' && !dataHide && fileDataIngesterDetails.dataSourceType != 'EXCEL_DATA_SOURCE'">
								<div class="col-md-4">
									<label for="label-focus pull-left"> At line#<span
										class="field-warning">*</span>
									</label>
								</div>
								<div class="col-md-8 textboxCss"
									style="width: 121% !important; margin-left: 5px;">
									<input type="text" class="form-control" id="label-focus"
										ng-model="fileDataIngesterDetails.recordStartLine">
								</div>
							</div>
						</div>
						<div class="col-md-12">&nbsp;</div>
						<!--tooltip-placement="right"  tooltip='{{explanation["recordsIgnore"]}}' -->
						<div class="col-md-12"
							ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA' || 
                             	fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE' || 
                             	fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA' ||
                             	fileDataIngesterDetails.dataSourceType == 'FTP_DATA'||
                             	fileDataIngesterDetails.dataSourceType == 'NOSQL'">
							<div class="row form-group"
								ng-hide="dataHide || fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
								<div class="col-md-4">
									<label for="label-focus pull-left"> Records to
										ignore(Reg ex) </label>
								</div>
								<div class="col-md-8 textboxCss">
									<input type="text" class="form-control rec-ignore"
										id="label-focus" name="recordtoIgnore"
										ng-model="fileDataIngesterDetails.recordsIgnore" required>
								</div>
							</div>
						</div>
						<div class="col-md-12">&nbsp;</div>
						<!--tooltip-placement="right" tooltip='{{explanation["recordLoadLimit"]}}'-->
						<div class="col-md-12"
							ng-hide="fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA' || 
                             	fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE' ||
                             	fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA' ||
                             	fileDataIngesterDetails.dataSourceType == 'FTP_DATA'||
                             	fileDataIngesterDetails.dataSourceType == 'NOSQL'">
							<div class="row form-group"
								ng-hide="dataHide || fileDataIngesterDetails.dataSourceType == 'EXCEL_DATA_SOURCE'">
								<div class="col-md-4">
									<label for="label-focus pull-left"> Records load limit</label>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" id="label-focus"
										name="recordLimit" style="width: 83%; margin-left: 5px;"
										ng-model="fileDataIngesterDetails.recordLoadLimit" required>
								</div>
							</div>
						</div>
					</div>
				</section>

				<div
					class="col-md-12 {{fileDataIngesterDetails.dataSourceType == 'STREAM_DS' ? 'stream-ds' : ''}}">
					<div class="col-md-12 form-group"
						ng-show="fileDataIngesterDetails.dataSourceType != ''
                                          || fileDataIngesterDetails.dataSourceType == 'STREAM_DS'">
						<div
							class="{{fileDataIngesterDetails.dataSourceType == 'STREAM_DS' ? 'stream-css': 'all-css'}}"
							ng-class="{'col-sm-offset-8 col-sm-4': datasource.dataSourceType == 'RDBMS_DATA_SOURCE',
                                                         'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'FILE_DATA_SOURCE',
                                                         'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'CSV_DATA_SOURCE',
                                                         'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'TSV_DATA_SOURCE',
                                                         'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'EXCEL_DATA_SOURCE',
                                                         'col-sm-offset-8 col-sm-4' : datasource.dataSourceType == 'EXTERNAL_DATA',
                                                         'col-sm-offset-8 col-sm-4' : fileDataIngesterDetails.dataSourceType == 'STREAM_DS',
                                                         'col-sm-offset-8 col-sm-4' : fileDataIngesterDetails.dataSourceType == 'REMOTE_DATA',
							 'col-sm-offset-8 col-sm-4' : fileDataIngesterDetails.dataSourceType == 'NOSQL'	
                                                         }">
							<button type="button"
								ng-if="fileDataIngesterDetails.dataSourceType == 'RDBMS_DATA_SOURCE'
                                               || fileDataIngesterDetails.dataSourceType == 'STREAM_DS'
					       || fileDataIngesterDetails.dataSourceType == 'NOSQL'"
								class="btn btn-default btn-block-data"
								ng-click="validateRdbmsConnection()">Test</button>
							<div class="space"></div>
							<button type="button" class="btn btn-default btn-block-data"
								ng-click="clearFileDataSource()">Clear</button>
							<div class="space">
								<button type="submit" class="btn btn-primary"
									ng-if="fileDataIngesterDetails.dataSourceType != 'EXTERNAL_DATA'
                                                    || fileDataIngesterDetails.dataSourceType == 'STREAM_DS'"
									ng-click="inputParamCall('connection')">Save</button>
								<button type="submit"
									ng-if="fileDataIngesterDetails.dataSourceType == 'EXTERNAL_DATA'"
									class="btn btn-primary"
									ng-click="configured(datasource.externalDataType)">Configure</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Relationship start  -->
			<div class="col-md-12 padding-one"
				ng-hide="myTbHide1 && !(activeTab == 'Relationship') || (appId != undefined && !popUpValue)">
				<!-- Relationship add start -->
				<div class="col-sm-6 col-xs-6 btntop">
					<div style="float: left;">
						<button type="button" id="createdsclick"
							class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
							data-ng-click="openConfigureRelationPopup()">Configure
							Relations</button>
					</div>
				</div>
				<!-- Relationship add end -->
				<div data-ng-include="'views/template/relationContainer.html'"></div>
			</div>
			<!-- Relationship end -->

			<div class="col-md-12 padding-one"
				ng-if="activeTab === 'Approval' ">
				<div data-ng-include="'views/template/approval.html'"></div>
			</div>
			<!-- Discussion Start -->
			<div class="col-md-12 padding-one"
				ng-hide="myTbHide1 && !(activeTab == 'Discussion')">
				<!-- <div data-ng-include="'views/DataSource/discussion.html'"></div> -->
			</div>
			<!-- Discussion Ends -->
			<div class="col-md-12 padding-one"
				ng-hide="myTbHide1 && !(activeTab == 'Dashboard')">
				<div data-ng-include="'views/template/dataDashboard.html'"></div>
			</div>
			
			<div class="col-md-12 padding-one"
				style="margin-top: -20px !important;"
				ng-hide="myTbHide1 && !(activatedSubMenu == 'Connection') || (appId != undefined && !popUpValue)">
				<section class="col-md-12">
					<div class="col-md-12">
						<strong> Data ingestion </strong>
					</div>
					<div class="row border-line"></div>
					<div class="col-md-12" style="margin-bottom: 10px;">
						<!-- <div class="row noteCss" role="alert" style="padding-left: 9px;">
							<span ng-bind-html='explanation["ingestion"] | unsafe'></span>
						</div> -->
						<div class="col-md-2" style="left: -13px;">
							<label>Data ingestion method</label>
						</div>
						<div class="col-md-9" ng-init="datasource.user=2;">
							<label class="ui-radio" tooltip-placement="top"
								tooltip='{{explanation["advance"]}}'> <input
								name="radio15" ng-model="datasource.ingection" type="radio"
								value="typical" ng-disabled="advancedDisabled"> <span>
									Advance </span>
							</label> <label class="ui-radio" tooltip-placement="top"
								tooltip='{{explanation["basic"]}}'> <input
								name="radio15" ng-model="datasource.ingection" type="radio"
								value="quick" ng-disabled='basicDataIngection'> <span>
									Basic </span>
							</label> <span style="color: red"></span>
						</div>
						<div class="form-group">
							<button ng-click="saveDataIngestion()"
								style="float: right; margin-right: 0px" class="btn btn-primary"
								type="submit">Save</button>
						</div>
					</div>
				</section>
			</div>



			<div class="row padding-one"
				ng-if="activeTab === 'Sample records'">
				<section class="col-md-12">
					<div class="col-md-12"></div>
					<div class="col-md-12" ng-if="datasource.fieldMappings.length > 0"
						style="overflow-x: auto;">

						<br />
						<div class="table-dynamic" style="padding-top: 15px;">
							<table
								class="table table-bordered table-striped table-responsive table-repo pad-popup" ng-if="datasource.fieldMappings.length > 0 && datasource.fieldMappings[0].sampleRecords.length  > 0">
								<thead>
									<tr>
										<th><div class="th">#</div></th>
										<th ng-repeat="fm in datasource.fieldMappings"><div
												class="th">{{fm.fieldName}}</div></th>
									</tr>
									<tr>
										<th></th>
										<th ng-repeat="fm in datasource.fieldMappings"><div
												class="th">{{fm.fieldDataType}}</div></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-if="datasource.fieldMappings.length > 0" ng-repeat="field in datasource.fieldMappings[0].sampleRecords track by $index">
										<td>{{$index + 1}}</td>
										<td ng-repeat="sampleData in datasource.fieldMappings ">
											{{sampleData.sampleRecords[$parent.$index]}}
										</td>
									</tr>
									<tr ng-if="datasource.fieldMappings.length == 0">
										<td>No records.</td>
									</tr>
								</tbody>
							</table>
							<div ng-if="datasource.fieldMappings.length > 0 && datasource.fieldMappings[0].sampleRecords.length  === 0">No records</div>
						</div>
					</div>
				</section>
			</div>
			
			<!-- Data Cleansing starts here -->
			<div class="col-md-12"
				ng-hide="myTbHide1 && !(activatedSubMenu == 'Adv. Settings')">
				<section class="panel panel-default" ng-hide='true'
					data-ng-controller="DataCleansingCntr">
					<div class="panel-heading lan-heading">
						<span class="glyphicon glyphicon-th"></span> Data cleansing
					</div>
					<form name="dataCleansingForm"
						class="form-horizontal form-validation">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-5">
										<input type="text" class="form-control" placeholder="From"
											ng-model="dataCleansingFrom" />
									</div>

									<div class="col-md-5">
										<input type="text" class="form-control" placeholder="To"
											ng-model="dataCleansingTo" />
									</div>

									<div class="col-md-1">
										<a style="line-height: 3;" ng-click="addDataCleansing()">
											<i class="fa fa-plus-circle fa-lg"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="callout callout-info"
									ng-repeat="notification in  dataCleansingFields track by $index">
									<p>
										{{notification.fromValue}} {{notification.toValue}} <span
											class="rightAlign"> <i class="fa fa-trash-o addCursor"
											ng-click="removeCleaseing(notification,  $index)"></i>
										</span>
									</p>
								</div>
							</div>

							<div class="row">
								<div class="row col-md-12 text-right">
									<button type="button"
										class="btn btn-w-md btn-gap-v btn-line-default"
										ng-click="saveDataCleansing()">Save</button>
								</div>
							</div>
						</div>
					</form>
				</section>
			</div>

		</div>
	</fieldset>


	<div class="col-md-12 padding-one" ng-controller="feedbackCtrl"
		ng-hide="myTbHide1 && !(activeTab == 'Feedback') || (appId != undefined && !popUpValue)">
		<!--  <fieldset ng-disabled='disableOverview'>-->
		<fieldset ng-disabled="projectAccessType == 'reviewer'">
			<section class="co-md-12">
				<div class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-2">
									<label>Title</label>
								</div>
								<div class="col-md-6">
									<input type="text" class="form-control"
										placeholder="Enter title" ng-model="obj.question">
								</div>
							</div>
							<div class="col-md-12">&nbsp;</div>
							<div class="row">
								<div class="col-md-2">
									<label>Feedback</label>
								</div>
								<div class="col-md-6">
									<textarea class="form-control" rows="2" cols="10"
										ng-model="obj.answer" placeholder="Description"></textarea>
								</div>
							</div>
							<div class="col-md-12">&nbsp;</div>
							<div class="row">
								<div class="col-md-2">
									<label>Ratings</label>
								</div>
								<div class="col-md-10">
									<fieldset class="rating">
										<input type="radio" id="star5" ng-click="submitRating(star)"
											name="rating" value="1" ng-model="star.starFive" /> <label
											for="star5" title="" id="starFive"
											ng-mouseout="showRating(userDetailsForAverageRating)"
											ng-mouseover="checkRating('starFive')">5 stars</label> <input
											type="radio" id="star4" ng-click="submitRating(star)"
											name="rating" value="1" ng-model="star.starFour" /> <label
											for="star4" title="" id="starFour"
											ng-mouseout="showRating(userDetailsForAverageRating)"
											ng-mouseover="checkRating('starFour')">4 stars</label> <input
											type="radio" id="star3" ng-click="submitRating(star)"
											name="rating" value="1" ng-model="star.starThree" /> <label
											for="star3" title="" id="starThree"
											ng-mouseout="showRating(userDetailsForAverageRating)"
											ng-mouseover="checkRating('starThree')">3 stars</label> <input
											type="radio" id="star2" ng-click="submitRating(star)"
											name="rating" value="1" ng-model="star.starTwo" /> <label
											for="star2" title="" id="starTwo"
											ng-mouseout="showRating(userDetailsForAverageRating)"
											ng-mouseover="checkRating('starTwo')">2 stars</label> <input
											type="radio" id="star1" ng-click="submitRating(star)"
											name="rating" value="1" ng-model="star.starOne" /> <label
											for="star1" title="" id="starOne"
											ng-mouseout="showRating(userDetailsForAverageRating)"
											ng-mouseover="checkRating('starOne')">1 star</label>
									</fieldset>
								</div>
							</div>
							<div class="col-md-2" style="top: 20px;">
								<button class="btn btn-primary pull-left" style="width: 67px;"
									type="button" ng-click="submitPost(obj);">Submit</button>
							</div>
						</div>
						<div class="col-md-6 scroll-feedback">
							<div class="col-md-12 feedback-css"
								ng-repeat="item in feedbackObj track by $index">
								<div class="col-md-12">
									<div class="col-md-1"
										style="color: #606060; text-align: center;">
										<button class="btn btn-default votingButton"
											data-ng-click="updateVote(+1, $index, item)">
											<i class="fa fa-caret-up fa-2x pull-left"
												data-ng-class="item.selfVote==1 ? 'voted':'voting'"></i> <span
												class="vote-positive">{{item.votingPositiveCount}}</span>
										</button>
										<button class="btn btn-default votingButton">{{item.votingCount}}</button>
										<button class="btn btn-default votingButton"
											data-ng-click="updateVote(-1, $index, item)">
											<i class="fa fa-caret-down fa-2x"
												data-ng-class="item.selfVote==-1 ? 'voted':'voting'"></i> <span
												class="vote-negative">{{item.votingNegativeCount}}</span>
										</button>
									</div>
									<div class="col-md-11">
										<div class="col-md-12 feedBackPadding">
											<span style="color: #17b9e7; font-size: 16px;">{{item.question}}</span><br />
											<span>{{item.answer}}</span>
										</div>
										<div class="col-md-12">
											<span class="pull-right feedBackUserName">&nbsp;
												{{item.insertTS | date}} &nbsp;&nbsp; <span
												style="color: #17b9e7;">{{item.unqUserId}}</span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</fieldset>
	</div>
	<!-- <fieldset ng-disabled="!popUpValue"> -->
	<!-- Application Setting start code -->
	<fieldset
		ng-disabled='myApplicationTab || disableDataSource == "1" || projectAccessType == "reviewer"'>
		<div class="row" ng-hide='!(activeTab == "Application settings")'>
			<div class="col-md-12 padding-one">
				<section class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-12">
							<strong> Application settings </strong>
						</div>
						<div class="col-md-12 border-line"></div>
						<div class="col-md-12" ng-init='appSetting = {use:"default"}'>
							<!-- <div class="dsFont" role="alert">
							<span ng-bind-html='explanation["appSetting"] | unsafe'></span>
						</div> -->
							<div class="col-md-12">&nbsp;</div>
							<div class="col-md-8">
								<label class="ui-radio" tooltip-placement="right"
									tooltip='{{explanation["useAsIt"]}}'> <input name="use"
									ng-model="appSetting.use" type="radio" value="default">
									<span>Use as it is </span>
								</label> <label class="ui-radio" tooltip-placement="top"
									tooltip='{{explanation["refresh"]}}'> <input name="use"
									ng-model="appSetting.use"
									ng-disabled="disableApplicationSetting" type="radio"
									value="Refreshed"> <span> Refresh </span>
								</label> <span style="color: red"></span>
							</div>
							<div class="col-md-12"></div>

							<div class="col-md-6 form-group"
								ng-if='appSetting.use == "Refreshed"'>
								<div class="col-md-3">
									<label style="float: left;">File path</label>
								</div>
								<div class="col-md-8">
									<span class="ui-select" style="margin-left: -4px; width: 102%;">
										<select
										ng-options="item.name for item in globleParam | filter: {type: 'STRING'} track by item.name"
										ng-model="appSetting.globalParam">
											<option value="">Select global param</option>
									</select>
									</span>
								</div>
							</div>
							&nbsp;
							<div class="col-md-6 form-group"
								ng-repeat="item in datasource.dsLevelParams track by $index"
								ng-if='appSetting.use == "Refreshed"'>
								<div class="col-md-3">
									<label style="float: left;">{{item.paramName}}</label>
								</div>
								<div class="col-md-8">
									<span class="ui-select" style="margin-left: -4px; width: 102%;">
										<select
										ng-options="item.name for item in globleParam | filter: {type: item.paramType} track by item.name "
										ng-change="delAppSettings(list[$index] , $index)"
										ng-model="list[$index]">
											<option value="">Select input param</option>
									</select>
									</span>
								</div>
							</div>


							<div class="form-group" style="margin-right: -17px;">
								<button ng-click="saveAppSettings();"
									style="float: right; margin-right: 18px"
									class="btn btn-primary" type="submit">Save</button>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</fieldset>
	<!-- Application Setting end code -->
	<!-- History Start -->
	<fieldset
		ng-disabled="disableDataSource == '1' || projectAccessType == 'reviewer'">
		<div class="col-md-12 padding-one" ng-hide="myTbHide1 && !(activatedSubMenu == 'History')"
			data-ng-controller="dataSourceHistoryCtrl">
			<div class="col-md-12 form-group">
				<div class="col-md-12">
					<label class="ui-radio" tooltip-placement="top"
						tooltip='limit by No.'> <input type="radio"
						ng-model="datasource.typeOfActiveInstances" value="INSTANCES"
						ng-click="saveNoOfInstances('INSTANCES')"> <span>
							No. of Active Instances: </span>
					</label> <label class="ui-radio" tooltip-placement="top"
						tooltip='Limit by days'> <input type="radio"
						ng-model="datasource.typeOfActiveInstances" value="DAYS"
						ng-click="saveNoOfInstances('DAYS')"> <span> No. of
							Active Days: </span>
					</label> <label class="ui-radio" tooltip-placement="top"
						tooltip='Limit by days'> <input type="radio"
						ng-model="datasource.typeOfActiveInstances" value="DEFAULT"
						ng-click="saveNoOfInstances('DEFAULT')"> <span>
							Default: </span>
					</label> <input type="number" ng-model="datasource.noOfActiveInstances"
						value="0" ng-disabled="onDefaultDisable">

					<div style="float: right;">
						<button ng-click="saveDataSourceHistory()"
							style="margin-right: 18px" class="btn btn-primary" type="submit">Save</button>
						<button ng-click="resetChanges()" style="margin-right: 18px"
							class="btn btn-primary" type="submit">Reset</button>
						<button ng-click="dismissModel();" style="margin-right: 18px"
							class="btn btn-primary" type="submit">Cancel</button>
					</div>
				</div>
			</div>

			<section class="col-md-12" ng-disabled="disable">
				<div class="col-md-12">
					<h5>
						<strong> Active </strong>
					</h5>
				</div>
				<div class="row border-line"></div>
				<div class="col-md-12" style="min-height: 112px;">
					<!-- <div class="col-md-12 noteCss" role="alert">
                              <span ng-bind-html='explanation["ingestion"] | unsafe'></span>
                         </div> -->

					<div class="col-md-12" style="margin-bottom: 15px;">&nbsp;</div>
					<div class="table-dynamic">
						<table class="table table-striped">
							<thead>
								<tr ng-hide="datasource.activeDatasources.length == 0">
									<th ng-hide="datasource.activeDatasources.length == 0"><div
											class="th">
											<label class="ui-checkbox"> <input type="checkbox"
												ng-model="activeCheckAll" tooltip-placement="top"
												tooltip="Check All" ng-disabled="disable"
												ng-change="checkAllActive(activeCheckAll)"> <span></span>
											</label>
										</div></th>
									<th><div class="th">JobInstId</div></th>
									<th><div class="th">Run By</div></th>
									<th><div class="th">Run At</div></th>
									<th><div class="th">Size</div></th>
									<th><div class="th">#Recs</div></th>
									<th><div class="th">Path</div></th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="fm in activeDatasources track by $index">
									<td width="5%;"><label class="ui-checkbox"> <input
											type="checkbox" ng-model="fm.check"
											ng-change="changeValueTofalseActive(fm.check)"
											ng-disabled="disable"> <span></span>
									</label></td>
									<td width="10%;">{{fm.jobInstId}}</td>
									<td width="20%">{{fm.createdBy}}</td>
									<td width="15%">{{fm.createdDate | date:'medium'}}</td>
									<td width="10%">{{fm.size | sizeConvertForHistoryTab }}</td>
									<td width="10%">{{fm.totalRecords}}</td>
									<td width="30%;">{{fm.filePath}}</td>
								</tr>
							</tbody>
						</table>
						<div class="form-group">
							<label class="col-md-2">Total Size:<span class="fontD">&nbsp;{{activeTotalSize
									| sizeConvertForHistoryTab}}</span></label> <label class="col-md-2">Total Records:<span
								class="fontD">&nbsp;{{activeTotalRecords}}</span></label>
						</div>
						<div class="form-group">
							<span ng-click="inactivateDataSources()"
								style="float: right; margin-right: 18px"
								class="btn btn-primary pull-right" type="button"
								ng-disabled="changeActive">Inactivate</span>
						</div>
					</div>
				</div>
			</section>
			<div style="margin-bottom: 5px;">&nbsp;</div>
			<section class="col-md-12">
				<div class="col-md-12">
					<h5>
						<strong> History </strong>
					</h5>
				</div>
				<div class="row border-line"></div>
				<div class="col-md-12" style="min-height: 112px;">
					<!-- <div class="col-md-12 noteCss" role="alert">
                        <span ng-bind-html='explanation["ingestion"] | unsafe'></span>
                    </div> -->

					<div class="col-md-12" style="margin-bottom: 15px;">&nbsp;</div>
					<div class="table-dynamic">
						<table class="table table-striped">
							<thead>
								<tr ng-hide="datasource.historyDatasources.length == 0">
									<th ng-hide="datasource.historyDatasources.length == 0"><div
											class="th  ">
											<label class="ui-checkbox "> <input type="checkbox"
												ng-model="historyCheckAll" tooltip-placement="top"
												tooltip="Check All" ng-disabled="disable"
												ng-change="checkAllHistory(historyCheckAll)"> <span></span>
											</label>
										</div></th>
									<th><div class="th">JobInstId</div></th>
									<th><div class="th">Run By</div></th>
									<th><div class="th">Run At</div></th>
									<th><div class="th">Size</div></th>
									<th><div class="th">#Recs</div></th>
									<th><div class="th">Path</div></th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="fm in historyDatasources track by $index">
									<td width="5%;"><label class="ui-checkbox "> <input
											type="checkbox" ng-model="fm.check"
											ng-change="changeValueTofalseHistory(fm.check)"
											ng-disabled="disable"> <span></span>
									</label></td>
									<td width="10%;">{{fm.jobInstId}}</td>
									<td width="20%">{{fm.createdBy}}</td>
									<td width="15%">{{fm.createdDate | date:'medium'}}</td>
									<td width="10%">{{fm.size | sizeConvertForHistoryTab }}</td>
									<td width="10%">{{fm.totalRecords}}</td>
									<td width="30%;">{{fm.filePath}}</td>
								</tr>
							</tbody>
						</table>
						<div class="form-group">
							<label class="col-md-2">Total Size:<span class="fontD">&nbsp;{{inactiveTotalSize
									|  sizeConvertForHistoryTab }}</span></label> <label class="col-md-2">Total Records:<span
								class="fontD">&nbsp;{{inactiveTotalRecords}}</span></label>
						</div>
						<div class="form-group">
							<span ng-click="activateDataSources()"
								style="float: right; margin-right: 18px"
								class="btn btn-primary pull-right" ng-disabled="changeHistory"
								type="button">Activate</span> <span
								ng-click="deletDataSources()"
								style="float: right; margin-right: 18px"
								class="btn btn-primary pull-right" ng-disabled="changeHistory"
								type="button">Delete</span>
						</div>
					</div>
				</div>
			</section>
			<div style="margin-bottom: 5px;">&nbsp;</div>

			<section class="col-md-12">
				<div class="col-md-12">
					<strong> Deleted </strong>
				</div>
				<div class="row border-line"></div>
				<div class="col-md-12" style="min-height: 112px;">
					<!-- <div class="col-md-12 noteCss" role="alert">
                        <span ng-bind-html='explanation["ingestion"] | unsafe'></span>
                    </div> -->

					<div class="col-md-12" style="margin-bottom: 15px;">&nbsp;</div>
					<div class="table-dynamic">
						<table class="table table-striped">
							<thead>
								<tr ng-hide="datasource.deletedDatasources.length == 0">
									<th><div class="th">JobInstId</div></th>
									<th><div class="th">Run By</div></th>
									<th><div class="th">Run At</div></th>
									<th><div class="th">Deleted By</div></th>
									<th><div class="th">Deletion Date</div></th>
									<th><div class="th">Size</div></th>
									<th><div class="th">#Recs</div></th>
									<th><div class="th">Path</div></th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="fm in deletedDatasources track by $index">
									<td width="">{{fm.jobInstId}}</td>
									<td width="">{{fm.createdBy}}</td>
									<td width="">{{fm.createdDate | date:'medium'}}</td>
									<td width="">{{fm.updatedBy}}</td>
									<td width="">{{fm.updatedDate | date:'medium'}}</td>
									<td width="">{{fm.size | sizeConvertForHistoryTab}}</td>
									<td width="">{{fm.totalRecords}}</td>
									<td width="">{{fm.filePath}}</td>
								</tr>
							</tbody>
						</table>
						<div class="form-group">
							<label class="col-md-2">Total Size:<span class="fontD">&nbsp;{{deletedTotalSize
									| sizeConvertForHistoryTab }}</span></label> <label class="col-md-2">Total Records:<span
								class="fontD">&nbsp;{{deletedTotalRecords}}</span></label>
						</div>
					</div>
				</div>
			</section>

		</div>
		<!-- History END -->
	</fieldset>

	
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-12">
		<div class="modal-footer"
			ng-if="modelCheck != undefined && modelCheck != null">
			<button class="btn btn-primary" ng-click="dismissModel();"
				style="width: 200px">Okay</button>
			<button class="btn btn-default btn-block-data"
				ng-disabled='decideDesabled == 0'
				ng-click="nextPrevous('previous');">Previous</button>
			<button class="btn btn-default btn-block-data"
				ng-disabled='decideDesabled == 3' ng-click="nextPrevous('next');">Next</button>
		</div>
	</div>
</div>


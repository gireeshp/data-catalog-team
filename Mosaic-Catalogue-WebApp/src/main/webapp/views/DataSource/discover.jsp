<div id="top" data-ng-controller="discoverController" data-ng-init="initialization('discover')">
	<!-- left faseet panel -->
	<div class="discover padding0 col-md-2 discover-border-right" style="height:{{viewHeight ? viewHeight+'px;' : 'auto;'}}">
		<!-- catlogue faset div start -->
		
		<!-- source type faset -->
		<div class="discor-faset">
			<div class="discover-faset-heading">
				<span class="fL"class="ui-checkbox mar-left-checkbox">Source Type</span>
				<span class="fR">
					<span class="discover-faset-heading-accordian" ng-click="showHideFacets('sourceTypeShow');">
						<i class="glyphicon" ng-class="facetsObject.sourceTypeShow ? 'glyphicon-chevron-down' :  'glyphicon-chevron-right'"></i>
					</span>
				</span>
			</div>
			<div ng-if="facetsObject.sourceTypeShow" class="clear_both discover-faset-body">
				<div class="displayBlock">
					<div  class="input-group input-group-sm">
					  <span class="input-group-addon discover-faset-body-serach-div" id="sourceType">
					  	<input type="checkbox" ng-model="checkAllSourceType" ng-change="checkAllSourceTypeCheck()" class="toggle-task discover-check-all-check-box discover-faset-search-box-input" />	
				  	  </span>
					  <input  type="text" ng-model="searchByType" placeholder="Search source types" class="discover-faset-search-box discover-faset-search-box-input"  aria-describedby="sourceType"/>
				  	</div>	
					<div class="list-group discover-list-group">					
						<ul class="padding5 margin-bottom-0" id="style-1">
							<li class="displayBlock" data-ng-repeat="bucket in  filtered = (sourceType | filter: searchByType | orderBy :'key' )">
								<label
									class="ui-checkbox mar-left-checkbox"> 
									<input
										type="checkbox" data-ng-change="select(1)"
										value="{{bucket.key}}" class="toggle-task"
										data-ng-model="sourceTypeCheck[bucket.key]" /> 
									<span tooltip="{{bucket.key | contentType}}"  tooltip-placement="bottom">{{(bucket.key) | contentType}} {{'('+ bucket.docCount +')'}}</span>
								</label>
							</li>
							<p ng-if="filtered.length ==0">No Results found</p>	
						</ul>
					</div>	
				</div>								
			</div>
		</div>
		
		<!-- createdby type faset -->
		<div class="discor-faset">
			<div class="discover-faset-heading">
				<span class="fL"class="ui-checkbox mar-left-checkbox">Created By</span>
				<span class="fR">
					<span class="discover-faset-heading-accordian" ng-click="showHideFacets('createdByShow');">
						<i class="glyphicon" ng-class="facetsObject.createdByShow ? 'glyphicon-chevron-down' :  'glyphicon-chevron-right'"></i>
					</span>
				</span>
			</div>
			<div ng-if="facetsObject.createdByShow" class="clear_both discover-faset-body">
				<div class="displayBlock">
					<div  class="input-group input-group-sm">
					  <span class="input-group-addon discover-faset-body-serach-div" id="checkAllCreatedBy">
					  	<input type="checkbox" ng-model="checkAllCreatedBy" ng-change="checkAllCreatedByFnc()" class="toggle-task discover-check-all-check-box discover-faset-search-box-input" />	
				  	  </span>
				  	  <input type="text" ng-model="searchCreatedBy" class="discover-faset-search-box discover-faset-search-box-input" placeholder="Search created by" aria-describedby="checkAllCreatedBy"/>
				  	</div>	
					<div class="list-group discover-list-group">					
						<ul class="padding5 margin-bottom-0" id="style-1">
							<li class="displayBlock" data-ng-repeat="bucket in filtered = ( createdBy | filter: searchCreatedBy | orderBy :'key' )">
								<label
									class="ui-checkbox mar-left-checkbox"> 
									<input
										type="checkbox" data-ng-change="select(1)"
										value="{{bucket.key}}" class="toggle-task"
										data-ng-model="createdByCheck[bucket.key]" /> 
									<span tooltip="{{bucket.key}}"  tooltip-placement="bottom">{{(bucket.key.length
										> 10 ? bucket.key.substring(0, 9).concat("...") : bucket.key)
										| titleCase}} {{'('+ bucket.docCount +')'}}</span>
								</label>
							</li>
							<p ng-if="filtered.length ==0">No Results found</p>	
						</ul>
					</div>	
				</div>								
			</div>
		</div>
		
		<!-- Category type faset -->
		<div class="discor-faset">
			<div class="discover-faset-heading">
				<span class="fL"class="ui-checkbox mar-left-checkbox">Category</span>
				<span class="fR">
					<span class="discover-faset-heading-accordian" ng-click="showHideFacets('categoryShow');">
						<i class="glyphicon" ng-class="facetsObject.categoryShow ? 'glyphicon-chevron-down' :  'glyphicon-chevron-right'"></i>
					</span>
				</span>
			</div>
			<div ng-if="facetsObject.categoryShow" class="clear_both discover-faset-body">
				<div class="displayBlock">
					<div  class="input-group input-group-sm">
					  <span class="input-group-addon discover-faset-body-serach-div" id="checkAllCategory">
					  	<input type="checkbox" ng-model="checkAllCategory" ng-change="checkAllCategoryFnc()" class="toggle-task discover-check-all-check-box discover-faset-search-box-input" />	
				  	  </span>
				  	  <input type="text" ng-model="searchCategory" class="discover-faset-search-box discover-faset-search-box-input" placeholder="Search category" aria-describedby="checkAllCategory"/>
				  	</div>	
					<div class="list-group discover-list-group">					
						<ul class="padding5 margin-bottom-0" id="style-1">
							<li class="displayBlock" data-ng-repeat="bucket in filtered = (category | filter: searchCategory | orderBy :'key')">
								<label
									class="ui-checkbox mar-left-checkbox"> 
									<input
										type="checkbox" data-ng-change="select(1)"
										value="{{bucket.key}}" class="toggle-task"
										data-ng-model="categoryCheck[bucket.key]" /> 
									<span tooltip="{{bucket.key}}"  tooltip-placement="bottom">{{(bucket.key.length
											> 10 ? bucket.key.substring(0, 10).concat("...") : bucket.key)
											| titleCase}} {{'('+ bucket.docCount +')'}}</span>
								</label>
							</li>
							<p ng-if="filtered.length ==0">No Results found</p>	
						</ul>
					</div>	
				</div>								
			</div>
		</div>
		
		<!-- subCategory type faset -->
		<div class="discor-faset">
			<div class="discover-faset-heading">
				<span class="fL"class="ui-checkbox mar-left-checkbox">Sub Category</span>
				<span class="fR">
					<span class="discover-faset-heading-accordian" ng-click="showHideFacets('subCategoryShow');">
						<i class="glyphicon" ng-class="facetsObject.subCategoryShow ? 'glyphicon-chevron-down' :  'glyphicon-chevron-right'"></i>
					</span>
				</span>
			</div>
			<div ng-if="facetsObject.subCategoryShow" class="clear_both discover-faset-body">
				<div class="displayBlock">
					<div  class="input-group input-group-sm">
					  <span class="input-group-addon discover-faset-body-serach-div" id="checkAllSubCategory">
					  	<input type="checkbox" ng-model="checkAllSubCategory" ng-change="checkAllSubCategoryFnc()" class="toggle-task discover-check-all-check-box discover-faset-search-box-input" />	
				  	  </span>
				  	  <input type="text" ng-model="searchSubCategory" class="discover-faset-search-box discover-faset-search-box-input" placeholder="Search sub category" aria-describedby="checkAllSubCategory"/>
				  	</div>	
					<div class="list-group discover-list-group">					
						<ul class="padding5 margin-bottom-0" id="style-1">
							<li class="displayBlock" data-ng-repeat="bucket in filtered = (sub_category | filter: searchSubCategory | orderBy :'key')">
								<label
									class="ui-checkbox mar-left-checkbox"> 
									<input
										type="checkbox" data-ng-change="select(1)"
										value="{{bucket.key}}" class="toggle-task"
										data-ng-model="subCategoryCheck[bucket.key]" /> 
									<span tooltip="{{bucket.key}}"  tooltip-placement="bottom">{{(bucket.key.length
											> 10 ? bucket.key.substring(0, 10).concat("...") : bucket.key)
											| titleCase}} {{'('+ bucket.docCount +')'}}</span>
								</label>
							</li>
							<p ng-if="filtered.length ==0">No Results found</p>	
						</ul>
					</div>	
				</div>								
			</div>
		</div>
		
		<!-- Tags type faset -->
		<div class="discor-faset" ng-if="tags.length > 0">
			<div class="discover-faset-heading">
				<span class="fL"class="ui-checkbox mar-left-checkbox">Tags</span>
				<span class="fR">
					<span class="discover-faset-heading-accordian" ng-click="showHideFacets('tagsShow');">
						<i class="glyphicon" ng-class="facetsObject.tagsShow ? 'glyphicon-chevron-down' :  'glyphicon-chevron-right'"></i>
					</span>
				</span>
			</div>
			<div ng-if="facetsObject.tagsShow" class="clear_both discover-faset-body">
				<div class="displayBlock">
					<div  class="input-group input-group-sm">
					  <span class="input-group-addon discover-faset-body-serach-div" id="checkAllTags">
					  	<input type="checkbox" ng-model="checkAllTags" ng-change="checkAllTagsFnc()" class="toggle-task discover-check-all-check-box discover-faset-search-box-input" />	
				  	  </span>
				  	  <input type="text" ng-model="searchTags" class="discover-faset-search-box discover-faset-search-box-input" placeholder="Search tags" aria-describedby="checkAllTags"/>
				  	</div>	
					<div class="list-group discover-list-group">					
						<ul class="padding5 margin-bottom-0" id="style-1">
							<li class="displayBlock" data-ng-repeat="bucket in filtered = ( tags  | filter: searchTags | orderBy :'key' )">
								<label
									class="ui-checkbox mar-left-checkbox"> 
									<input
										type="checkbox" data-ng-change="select(1)"
										value="{{bucket.key}}" class="toggle-task"
										data-ng-model="tagsCheck[bucket.key]" /> 
									<span tooltip="{{bucket.key}}"  tooltip-placement="bottom">{{(bucket.key.length
											> 10 ? bucket.key.substring(0, 10).concat("...") : bucket.key)
											| titleCase}} {{'('+ bucket.docCount +')'}}</span>
								</label>
							</li>
							<p ng-if="filtered.length ==0">No Results found</p>	
						</ul>
					</div>	
				</div>								
			</div>
		</div>
		
		<!-- User Group type faset -->
		<div class="discor-faset">
			<div class="discover-faset-heading">
				<span class="fL"class="ui-checkbox mar-left-checkbox">User Group</span>
				<span class="fR">
					<span class="discover-faset-heading-accordian" ng-click="showHideFacets('dataUserGroupShow');">
						<i class="glyphicon" ng-class="facetsObject.dataUserGroupShow ? 'glyphicon-chevron-down' :  'glyphicon-chevron-right'"></i>
					</span>
				</span>
			</div>
			<div ng-if="facetsObject.dataUserGroupShow" class="clear_both discover-faset-body">
				<div class="displayBlock">
					<div  class="input-group input-group-sm">
					  <span class="input-group-addon discover-faset-body-serach-div" id="checkAllDataUserGrp">
					  	<input type="checkbox" ng-model="checkAllDataUserGrp" ng-change="checkAllDataUserGroup()" class="toggle-task discover-check-all-check-box discover-faset-search-box-input" />	
				  	  </span>
				  	  <input type="text" ng-model="searchUsergroup" class="discover-faset-search-box discover-faset-search-box-input" placeholder="Search data by user group" aria-describedby="checkAllDataUserGrp"/>
				  	</div>	
					<div class="list-group discover-list-group">					
						<ul class="padding5 margin-bottom-0" id="style-1">
							<li class="displayBlock" data-ng-repeat="bucket in filtered = ( usergroup  | filter: searchUsergroup | orderBy :'key' )">
								<label
									class="ui-checkbox mar-left-checkbox"> 
									<input
										type="checkbox" data-ng-change="select(1)"
										value="{{bucket.key}}" class="toggle-task"
										data-ng-model="dataUserGroupCheck[bucket.key]" /> 
									<span tooltip="{{bucket.key}}"  tooltip-placement="bottom">{{(bucket.key.length
											> 10 ? bucket.key.substring(0, 10).concat("...") : bucket.key)
											| titleCase}} {{'('+ bucket.docCount +')'}}</span>
								</label>
							</li>
							<p ng-if="filtered.length ==0">No Results found</p>	
						</ul>
					</div>	
				</div>								
			</div>
		</div>
		
		<!-- Average Rating type faset -->
		<div class="discor-faset">
			<div class="discover-faset-heading">
				<span class="fL"class="ui-checkbox mar-left-checkbox">Average Rating</span>
				<span class="fR">
					<span class="discover-faset-heading-accordian" ng-click="showHideFacets('avgRatingShow');">
						<i class="glyphicon" ng-class="facetsObject.avgRatingShow ? 'glyphicon-chevron-down' :  'glyphicon-chevron-right'"></i>
					</span>
				</span>
			</div>
			<div ng-if="facetsObject.avgRatingShow" class="clear_both discover-faset-body">
				<div class="displayBlock">
					<div  class="input-group input-group-sm">
					  <span class="input-group-addon discover-faset-body-serach-div" id="checkAllAvgRatingCheck">
					  	<input type="checkbox" ng-model="checkAllAvgRatingCheck"  style="margin-top: 0px !important;margin-left:3px;" ng-change="checkAllAvgRatingCheckFnc()" class="fL toggle-task discover-check-all-check-box discover-faset-search-box-input" />	
				  	  </span>
				  	</div>	
					<div class="list-group discover-list-group">					
						<ul class="padding5 margin-bottom-0" id="style-1">
							<li class="displayBlock" data-ng-repeat="bucket in avgRating | orderBy :'-key'"">
								<label
									class="ui-checkbox mar-left-checkbox"> 
									<input
										type="checkbox" data-ng-change="select(1)"
										value="{{bucket.key}}" class="toggle-task"
										data-ng-model="avgRatingCheck[bucket.key]" /> 
										<span>
											<span ng-repeat="val in [1,2,3,4,5] track by $index"> <img
													alt="rating" ng-if="val <= bucket.key"
													src="../../styles/images/projectImages/filledstar.png">
													<img alt="rating" ng-if="val > bucket.key"
													src="../../styles/images/projectImages/blankstar.png"></span>
										</span>
										{{'('+ bucket.docCount +')'}}
								</label>
							</li>
							<p ng-if="filtered.length ==0">No Results found</p>	
						</ul>
					</div>	
				</div>								
			</div>
		</div>
			
	</div>
	
	<!-- discover body panel -->
	<div class="discover padding0 discover-side-panel-animate {{showDs == null ? 'col-md-10' : 'col-md-7'}}" style="height:{{viewHeight}}px">
		<div class="col-md-12 dicover-search-panel padding10">
			<div class="col-md-5 paddingLeft0 paddingRight0">
				<input type="text" data-ng-trim="true"
					class="form-control search-input search-box-css dropdown langs text-normal dropdown-toggle input-box-css"
					data-toggle="dropdown"
					data-ng-model="searchKeywords"
					data-ng-change="onKeyPress()" data-ng-focus="focus=true"
					autocomplete="off" ng-blur="focus=false"
					placeholder="Search...">
					<i class="glyphicon glyphicon-search search-box-discover"></i>
			</div>
			<div class="col-md-7 paddingRight0">
				<div style="display:inline-flex" class="fR">
					<span class="marginTop5 fontBold margin-right10">Sort</span>
					<span class="">
						<select ng-model="sortCatlogDataValue" class="form-control" style="padding:4px;" ng-change="sortCatlogData(sortCatlogDataValue)">
							<option value="-createdDate">Created date</option>
							<option value="createdBy">Created by</option>
							<option value="category">Category</option>
							<option value="-avgRating">Avg. Rating</option>
						</select>
					</span>
				</div>
			</div>
		</div>	
		<div class="col-md-12 padding0 border-bottom1"  ng-if="dataSourceCol.length > 0" >
			<div class="col-md-12" style="padding: 4px;font-size: 14px;">
				<div class=" paddingLeft0" style="float:left;width:35%;display:block;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<div class=" paddingLeft0" style="float:left;width:10%">Expert</div>
				<div class=" paddingLeft0" style="float:left;width:10%">Tags</div>
				<div class=" paddingLeft0" style="float:left;width:10%">Category</div>
				<div class=" paddingLeft0" style="float:left;width:10%">Ratings</div>
				<div class=" paddingLeft0"style="float:left;width:10%">Size</div>
				<div class=" paddingLeft0"style="float:left;width:15%">User group</div>
			</div>
		</div>
		<div class="col-md-12 padding0 discover-scroll"  ng-if="dataSourceCol.length > 0"  style="height:{{viewHeight - 128}}px;overflow-x:hidden;overflow-y:auto;">
			
			<div class="col-md-12 padding0 clear_both border-bottom1" style="padding: 5px 0px 5px 0px;" ng-class="{'selected':selectedIndex === $index}" ng-repeat='datasourceObject in dataSourceCol track by $index | orderBy	 : discoverDataSort()' ng-click="showSelectedData(datasourceObject, $index, $event)">
				<div class="padding0" style="float:left;width:35%">
					<div class="col-md-2 padding0">
						<img  alt='{{datasourceAltNames[datasourceObject.dataSourceType]}}' 
						ng-src="{{datasourceImages[datasourceObject.dataSourceType]}}" class='discover-image-div' style="width:30px;height:30px"/>
					</div>
					<div class="col-md-10 padding-left5" >
						<h5 class="col-md-12 padding0 discover-ds-name eplise-text paddingLeft0 sogoi-bold" data-toggle="tooltip" title="{{datasourceObject.dataSourceName}}">{{datasourceObject.dataSourceName}}</h5>
						
						<div class="col-md-12 padding0">
							<span class="paddingLeft0 eplise-text fontSize12 marginRight5" data-toggle="tooltip" title="{{datasourceObject.createdBy}}"><i class="glyphicon glyphicon-user"></i>&nbsp;{{datasourceObject.createdBy}}</span>
							<span class="eplise-text fontSize11" data-toggle="tooltip" title="{{datasourceObject.createdDate | date:'MMM-dd-yyyy'}}"><i class="glyphicon glyphicon-calendar"></i>&nbsp;{{datasourceObject.createdDate | date:'MMM-dd-yyyy'}}</span>
						</div>
					</div>	
				</div>
				<h5 style="float:left;width:10%" class=" paddingLeft0 text-capitalize eplise-text discover-card-padding" data-toggle="tooltip" title="{{datasourceObject.category ? datasourceObject.category  : '-' }}">{{datasourceObject.category && datasourceObject.category != "null" ? ( datasourceObject.category  | lowercase ): '-'  }}</h5>
				<h5 style="float:left;width:10%"class=" paddingLeft0 text-capitalize eplise-text discover-card-padding" data-toggle="tooltip" title="{{datasourceObject.category ? datasourceObject.category  : '-' }}">{{datasourceObject.category && datasourceObject.category != "null" ? (datasourceObject.category | lowercase) : '-'  }}</h5>
				<h5 style="float:left;width:10%" class=" paddingLeft0 text-capitalize eplise-text discover-card-padding" data-toggle="tooltip" title="{{datasourceObject.subCategory ? datasourceObject.subCategory  : '-' }}">{{datasourceObject.subCategory && datasourceObject.subCategory != "null" ? ( datasourceObject.subCategory | lowercase)  : '-'  }}</h5>
				<h5 style="float:left;width:10%"class=" paddingLeft0 text-capitalize eplise-text discover-card-padding" data-toggle="tooltip" title="{{datasourceObject.avgRating }}"><span class="glyphicon glyphicon-star" aria-hidden="true"></span>&nbsp;{{datasourceObject.avgRating ? datasourceObject.avgRating : 0 }}</h5>
				<h5 style="float:left;width:10%" class=" paddingLeft0 text-capitalize eplise-text discover-card-padding" data-toggle="tooltip" title="{{datasourceObject.size ? (datasourceObject.size | bytes) : 0 }}">{{datasourceObject.size ? (datasourceObject.size | bytes ) : 0+' B' }}</h5>
				<h5 style="float:left;width:12%" class="paddingLeft0 text-capitalize eplise-text discover-card-padding" data-toggle="tooltip" title="">{{datasourceObject.groupId |
											GroupNameFromGroupId:groupMap}}</h5>	
				<div style="float:right;width:3%" class="dicvoer-action-buttons" ng-if="selectedIndex === $index">
					<a ng-if="(loginUserId == datasourceObject.createdBy) && (myCollectionStatus !='Accept')"  tooltip="Add to collection" tooltip-placement="left" href="javascript:void(0);"  data-ng-click="addToCollection($event)">
						<i class="fa fa-cart-plus ng-scope"></i>
					</a>
					<a ng-if="(loginUserId == datasourceObject.createdBy)" tooltip="Delete" tooltip-placement="left"  href="javascript:void(0);" ng-click="deleteDataSource($event, $index,datasourceObject.dataSourceId,datasourceObject.dataSourceName);"">
						<i class="glyphicon glyphicon-trash"></i>
					</a>
					<a tooltip="Explore" tooltip-placement="left" href="javascript:void(0);" ng-click="goToDiscoverDataSource($event);">
						<i class="glyphicon glyphicon-share"></i>
					</a>
				</div>									
			</div>
		</div>
		<div class="col-md-12 icon-bottom pad-left-five" ng-if="dataSourceCol.length <= 0">
			<h4>No results found</h4>
		</div>
		<div class="col-md-12" ng-if="dataSourceCol.length != 0" style="border-top: 1px solid #f7f7f7;">
			<div class="fR">
				<pagination class="pagination-sm" ng-model="currentPage"
						total-items="filteredStores.length" max-size="4" style="margin: 5px 0 !important;"
						ng-change="select(currentPage)" items-per-page="numPerPage"
						rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
						boundary-links="true"></pagination>
			</div>			
		</div>
	</div>
	
	<!-- discover action panel -->
	<div class="discover col-md-3 padding0 discover-side-panel-animate discover-border-left" style="height:{{viewHeight}}px;" ng-show="showDs != null" >
		<div class="san-font" >
			<div scrolling-tabs-wrapper ng-if="showafterLoad" >
				<tabset> 
					<tab ng-click="openTab(tab)"
						ng-repeat="tab in discoverTabs" style="cursor : pointer;"
						heading="{{tab.title}}"
						active="tab.active" >
						<div ng-include="currentTab"></div>
					</tab> 
				</tabset>
			</div>	
		</div>

		<!-- feedback tab -->
		<div class="col-md-12 padding0" ng-if="activeTab === 'Feedback'" >
			<div class="col-md-12 padding0" id="createFeedbackDiv">
				<div class="col-md-12 paddingRight15 margin-top10">
					<h5 class="discover-input-field-headeing eplise-text sogoi-bold">Title</h5>
					<input type="text" class="form-control" ng-model="feedback.question" placeholder="Enter title" />
				</div>
				<div class="col-md-12 paddingRight15 margin-top10">
					<h5 class="discover-input-field-headeing eplise-text sogoi-bold">Feedback</h5>
					<textarea  class="form-control" type="text" ng-model="feedback.answer" placeholder="Enter description" col="3"></textarea> 
				</div>
				<div class="col-md-12 paddingRight15 margin-top10" >
					<h5 class="col-md-12 padding0 discover-input-field-headeing eplise-text sogoi-bold">Ratings</h5>
					<div class="rating displayBlock">
						<input type="radio" id="star5" ng-click="submitRating(star)"
								name="rating" value="1" ng-model="star.starFive" /> <label
								for="star5" title="" id="starFive"
								ng-mouseout="showRating(userDetailsForAverageRating)"
								ng-mouseover="checkRating('starFive')">5 stars</label> <input
								type="radio" id="star4" ng-click="submitRating(star)"
								name="rating" value="1" ng-model="star.starFour" /> <label
								for="star4" title="" id="starFour"
								ng-mouseout="showRating(userDetailsForAverageRating)"
								ng-mouseover="checkRating('starFour')">4 stars</label> <input
								type="radio" id="star3" ng-click="submitRating(star)"
								name="rating" value="1" ng-model="star.starThree" /> <label
								for="star3" title="" id="starThree"
								ng-mouseout="showRating(userDetailsForAverageRating)"
								ng-mouseover="checkRating('starThree')">3 stars</label> <input
								type="radio" id="star2" ng-click="submitRating(star)"
								name="rating" value="1" ng-model="star.starTwo" /> <label
								for="star2" title="" id="starTwo"
								ng-mouseout="showRating(userDetailsForAverageRating)"
								ng-mouseover="checkRating('starTwo')">2 stars</label> <input
								type="radio" id="star1" ng-click="submitRating(star)"
								name="rating" value="1" ng-model="star.starOne" /> <label
								for="star1" title="" id="starOne"
								ng-mouseout="showRating(userDetailsForAverageRating)"
								ng-mouseover="checkRating('starOne')">1 star</label>
					</div>			
				</div>
				<div class="col-md-12 display-grid paddingRight15">
					<button class="btn btn-sm btn-primary fR" type="button" ng-click="submitPost(feedback);">Submit</button>
				</div>
			</div>
			
			<div class="col-md-12 padding0  borderTop  margin-top10" id="listFeedbackDiv" >
				<div class="col-md-12 border-bottom1 marginTop5 discover-feedback-list-card" ng-repeat="feedObj in feedbackList track by $index | orderBy : feedbackSort() ">
					<div>
						<div class="col-md-12 padding0 ">
							<h5 class="col-md-12 padding0 eplise-text discover-feebback-headeing" data-toggle="tooltip" title="{{feedObj.question}}">
								 {{feedObj.question}}
							</h5>	
							<div class="col-md-12 padding0 eplise-text fontSize12">
								<span class="discover-feebback-h5" data-toggle="tooltip" title="{{feedObj.unqUserId}}">
									<i class="glyphicon glyphicon-user"></i>&nbsp;{{feedObj.unqUserId}}
								</span>
								<span class="discover-feebback-h5 marginLeft8" data-toggle="tooltip" title="{{feedObj.insertTS | date : 'MM-dd-yyyy'}}">
									<i class="glyphicon glyphicon-calendar"></i>&nbsp;{{feedObj.insertTS | date : 'MM-dd-yyyy'}}
								</span>
							</div>
						</div>
						<div class="col-md-12 padding0 fontSize14">
							<p data-toogle="tooltip" title="{{feedObj.answer}}" style="margin: 0 0 0px;">{{feedObj.answer}}</p>
						</div>
						<div class="col-md-12 padding0 eplise-text">
							<span class="displayBlock fL feedbak-rating">
								<label ng-repeat="n in [1,2,3,4,5]" ng-class="{'rating-checked': n <= ratingCount }"></label> 
							</span>	
							<span class="eplise-text marginLeft8 fR marginTop3 cursor" data-toggle="tooltip" title="{{feedObj.votingPositiveCount}}" data-ng-click="updateVote(+1, $index, feedObj)">
								<i class="glyphicon glyphicon-thumbs-up" data-ng-class="feedObj.selfVote==1 ? 'voted':'voting'"></i>&nbsp;{{feedObj.votingPositiveCount}}
							</span>
							<span class="eplise-text fR marginTop3 cursor" data-toggle="tooltip" title="{{feedObj.votingNegativeCount}}" data-ng-click="updateVote(-1, $index, feedObj)">
								<i class="glyphicon glyphicon-thumbs-down" data-ng-class="feedObj.selfVote==-1 ? 'voted':'voting'"></i>&nbsp;{{feedObj.votingNegativeCount}}
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- feedback tab end-->
		
		<!-- request access tab -->
			<div class="col-md-12 padding0" ng-if="activeTab === 'Request access'" >	
				<div class="col-md-12 padding0" id="requestAccessDiv">
					<div class="col-md-12 paddingRight15 margin-top10">
						<h5 class="col-md-12 padding0  discover-input-field-headeing eplise-text sogoi-bold">Id</h5>
						<span class="col-md-12 padding0 eplise-text">{{infoData.id}}</span>
					</div>
					<div class="col-md-12 paddingRight15 margin-top10">
						<h5 class="col-md-12 padding0  discover-input-field-headeing eplise-text sogoi-bold">Name</h5>
						<span class="col-md-12 padding0 eplise-text">{{infoData.dataSourceName}}</span>
					</div>
					<div class="col-md-12 paddingRight15 margin-top10">
						<h5 class="col-md-12 padding0  discover-input-field-headeing eplise-text sogoi-bold margin-bottom-five">Request For</h5>
						<span class="col-md-12 padding0">
							<select class="form-control padding4" data-ng-model="requestObj.requestFor" ng-change="requestForAccess()">
								<option value="">-- Select --</option>
								<option value="User" ng-hide="loginUserId == showDs.createdBy || requestStatus=='Accept'">User</option>
								<option value="Group">Group</option>
							</select>
						</span>
					</div>
					<div class="col-md-12 paddingRight15 margin-top10" data-ng-if="requestObj.requestFor == 'Group'">
						<h5 class="col-md-12 padding0  discover-input-field-headeing eplise-text sogoi-bold margin-bottom-five">Group</h5>
						<span class="col-md-12 padding0">
							<select class="form-control padding4" data-ng-model="requestObj.selectedRequestGroup" >
								<option value="">-- Select group --</option>
								<option data-ng-repeat="value in requestGroups" value="{{value.groupId}}">{{value.groupName}}</option>
		                    </select>
						</span>
					</div>
					<div class="col-md-12 paddingRight15 margin-top10">
						<h5 class="col-md-12 padding0  discover-input-field-headeing eplise-text sogoi-bold margin-bottom-five">Message</h5>
						<textarea rows="4" cols="50"  data-ng-model="requestObj.justification"  placeholder="Message to owner" class="form-control" style="resize:none;">
							</textarea>
					</div>
					<div class="col-md-12 paddingRight15 margin-top10">
						<button class="btn btn-primary width100" data-ng-click="requestForAccessConfirmation(requestObj.requestFor,requestObj.justification)">Submit</button>
					</div>
				</div>
			</div>	
		<!-- end of request access tab -->
		
		<!-- approval tab -->
			<div class="col-md-12 padding0" ng-if="activeTab === 'Approval'" ng-init="showApprover()">	
				<div data-ng-include="'views/template/approval.html'" ></div>
			</div>	
		<!-- end of approval tab -->
		
	</div>
</div>

<section class="animate-fade-up ng-scope monitor-page-css">
     <div data-ng-controller="glossaryTableCtrl">
          <div class="col-md-12 noteCss" role="alert">
               <!-- <span ng-bind-html='explanation.connectionHeader | unsafe'></span> -->
          </div>
          <!-- <div class="col-sm-12"></div> -->
          <section class="table-dynamic">
               <!-- <div class="panel-heading">
                    <strong>
                         <span class="glyphicon glyphicon-th"></span>RDBMS
                         Connections
                    </strong>
               </div> -->
               <div class="table-filters padTable">
                    <div class="row">
                         <div class="col-sm-3 col-xs-6 padding-right-none">
                              <form>
                                   <input type="text" placeholder="Filter..." class="form-control"
                                        data-ng-model="searchKeywords" data-ng-keyup="search()">
                              </form>
                         </div>
                         <div class="col-sm-3 col-xs-6 filter-result-info entries">
                              <span> Showing {{filteredStores.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + connections.length}} of {{filteredStores.length}}
                              entries </span>
                         </div>
                         <div class="col-sm-6 col-xs-6 btntop">
                              <button type="button"
                                   class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
                                   data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.ADD_NEW_TAG"
                                   data-ng-click="createTag(null)">Add new
                                   tag</button>
                         </div>
                    </div>
               </div>
               <div class="modal-body footerMarginCss">
                    <table
                         class="table table-bordered table-striped table-responsive table-repo marginSearch">
                         <thead>
                              <tr>
                                   <th>
                                        <div class="th ">#</div>
                                   </th>
                                   <th>
                                        <div class="th ">
                                             Tag name <span class="fa fa-angle-up"
                                                  data-ng-click=" order('name') "
                                                  data-ng-class="{active: row == 'name'}"></span> <span
                                                  class="fa fa-angle-down"
                                                  data-ng-click=" order('-name') "
                                                  data-ng-class="{active: row == '-name'}"></span>
                                        </div>
                                   </th>
                                   <th>
                                        <div class="th">
                                             Short Description <span class="fa fa-angle-up"
                                                  data-ng-click=" order('shortDescription') "
                                                  data-ng-class="{active: row == 'shortDescription'}"></span> <span
                                                  class="fa fa-angle-down"
                                                  data-ng-click=" order('-shortDescription') "
                                                  data-ng-class="{active: row == '-shortDescription'}"></span>
                                        </div>
                                   </th>
                                   <!-- <th>
                                        <div class="th">
                                             Long Description <span class="fa fa-angle-up"
                                                  data-ng-click=" order('longDescription') "
                                                  data-ng-class="{active: row == 'longDescription'}"></span> <span
                                                  class="fa fa-angle-down"
                                                  data-ng-click=" order('-longDescription') "
                                                  data-ng-class="{active: row == '-longDescription'}"></span>
                                        </div>
                                   </th> -->
                                   <th>
                                        <div class="th">
                                             Created by <span class="fa fa-angle-up"
                                                  data-ng-click=" order('createdBy') "
                                                  data-ng-class="{active: row == 'createdBy'}"></span> <span
                                                  class="fa fa-angle-down" data-ng-click=" order('-createdBy') "
                                                  data-ng-class="{active: row == '-createdBy'}"></span>
                                        </div>
                                   </th>
                                   <th>
                                        <div class="th">
                                             Created on <span class="fa fa-angle-up"
                                                  data-ng-click=" order('createdDate') "
                                                  data-ng-class="{active: row == 'createdDate'}"></span> <span
                                                  class="fa fa-angle-down"
                                                  data-ng-click=" order('-createdDate') "
                                                  data-ng-class="{active: row == '-createdDate'}"></span>
                                        </div>
                                   </th>
                                   <th>
                                        <div class="th">
                                             Updated by <span class="fa fa-angle-up"
                                                  data-ng-click=" order('modifiedBy') "
                                                  data-ng-class="{active: row == 'modifiedBy'}"></span> <span
                                                  class="fa fa-angle-down" data-ng-click=" order('-modifiedBy') "
                                                  data-ng-class="{active: row == '-modifiedBy'}"></span>
                                        </div>
                                   </th>
                                   <th>
                                        <div class="th">
                                             Updated on <span class="fa fa-angle-up"
                                                  data-ng-click=" order('modifiedDate') "
                                                  data-ng-class="{active: row == 'modifiedDate'}"></span> <span
                                                  class="fa fa-angle-down"
                                                  data-ng-click=" order('-modifiedDate') "
                                                  data-ng-class="{active: row == '-modifiedDate'}"></span>
                                        </div>
                                   </th>
                                   <th>
                                        <div class="th">Action</div>
                                   </th>
                              </tr>
                         </thead>
                         <tbody>
                              <tr class="maxLengthProp" data-ng-repeat="store in connections">
                                   <td><b>{{$index + ((currentPage * numPerPage) -
                                             numPerPage)+ 1}}</b></td>
                                   <td><a href="javascript:;" class="anchorColor"
                                        ng-click="viewTag(store)">{{store.name}}</a></td>
                                   <td>{{store.shortDescription.length
								> 25 ? store.shortDescription.substring(0, 25).concat("...") :
								store.shortDescription}}</td>
                                   <!-- <td>{{store.longDescription}}</td> -->
                                   <td>{{store.createdByName}}</td>
                                   <td>{{store.createdDate == 'null' || store.createdDate == null ? "" : store.createdDate | date : 'medium'}}</td>
                                   <td>{{store.modifiedByName}}</td>
                                   <td>{{store.modifiedDate == 'null' || store.modifiedDate == null ? "" : store.modifiedDate | date : 'medium'}}</td>
                                   <td>
                                        <ul class="nav-right ulMargin list-unstyled">
                                             <li
                                                  data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_TAG">
                                                  <a href="javascript:;" class="actionMenu"
                                                  data-ng-click="editTag(store)"><i
                                                       class="fa fa-pencil" tooltip-placement="top"
                                                       tooltip="Edit Tag"></i></a>
                                             </li>
                                             <li
                                                  data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DLT_TAG">
                                                  <a href="javascript:;" class="actionMenu"
                                                  data-ng-click="deleteTag(store)"><i
                                                       class="fa fa-trash" tooltip-placement="top"
                                                       tooltip="Delete Tag"></i></a>
                                             </li>
                                        </ul>
                                   </td>
                              </tr>
                         </tbody>
                    </table>
                    <footer class="table-footer">
                         <div class="row">
                              <div class="col-md-6 page-num-info marginCss">
                                   <span> Show <select data-ng-model="numPerPage"
                                        data-ng-options="num for num in numPerPageOpt"
                                        data-ng-change="onNumPerPageChange()">
                                   </select> entries per page
                                   </span>
                              </div>
                              <div class="col-md-6 text-right pagination-container zindex">
                                   <pagination class="pagination-sm" ng-model="currentPage"
                                        total-items="filteredStores.length" max-size="4"
                                        ng-change="select(currentPage)" items-per-page="numPerPage"
                                        rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
                                        boundary-links="true"></pagination>
                              </div>
                         </div>
                    </footer>
               </div>
          </section>
     </div>
</section>

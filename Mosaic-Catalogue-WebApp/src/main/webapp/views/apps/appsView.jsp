<div class="monitor-page-css" id="top" data-ng-controller="appDataCtrl">
	<div class="col-md-12 noteCss" role="alert">
		<span ng-bind-html='explanation["AnalysisAppHeader"] | unsafe'></span>
	</div>
	<section class="table-dynamic">
		<!-- <div class="panel-heading">
			<strong>Analysis Apps</strong>
		</div> -->

		<div class="table-filters padTable">
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span>Showing {{ filteredStores.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + currentPageStores.length}} of {{filteredStores.length}}
						entries </span>
				</div>
				<!-- <div class="col-sm-4 col-xs-4 btntop">
					style="margin-right: -85px;"
					<div>
						<button type="button" 
							class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize">
							<i ng-click="showLineage(stores)" class="fa fa-info-circle movOpField"></i>
						</button> &nbsp;
					</div> 
				</div>	 -->
				<div class="col-sm-6 col-xs-6 btntop" ng-hide="projectAccessType == 'reviewer'">
					<div data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CONFIG_NEW_ANLS_APP">
						<button id='createapp' type="button"
							class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
							ng-click="open('PERIODIC')">Configure new flow</button>
					</div>
					<div data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CONFIG_NEW_ANLS_APP">
						<button id='createapp' type="button"
							class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
							style="margin-right: 8px"
							ng-click="importFlow()">Import flow</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-body footerMarginCss">
			<table class="table table-bordered table-striped table-responsive marginSearch">
				<thead>
					<tr>
						<th>
							<div class="th">
								# <span class="fa fa-angle-up" data-ng-click=" order('appId') "
									data-ng-class="{active: row == 'appId'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-appId') "
									data-ng-class="{active: row == '-appId'}"></span>
							</div>
						</th>
						<th id="selectapp">
							<div class="th">
								Flow name <span class="fa fa-angle-up"
									data-ng-click=" order('appName') "
									data-ng-class="{active: row == 'appName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-appName') "
									data-ng-class="{active: row == '-appName'}"></span>
							</div>
						</th>
						<th>
							<div class="th">
								Created on <span class="fa fa-angle-up"
									data-ng-click=" order('createSt') "
									data-ng-class="{active: row == 'createSt'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-createSt') "
									data-ng-class="{active: row == '-createSt'}"></span>
							</div>
						</th>

						<th>
							<div class="th">
								Last run on <span class="fa fa-angle-up"
									data-ng-click=" order('lastRun') "
									data-ng-class="{active: row == 'lastRun'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-lastRun') "
									data-ng-class="{active: row == '-lastRun'}"></span>
							</div>
						</th>


						<th>
							<div class="th">
								Created by <span class="fa fa-angle-up"
									data-ng-click=" order('createdBy') "
									data-ng-class="{active: row == 'createdBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-createdBy') "
									data-ng-class="{active: row == '-createdBy'}"></span>
							</div>
						</th>


						<th>
							<div class="th">
								Status <span class="fa fa-angle-up"
									data-ng-click=" order('statusEnum') "
									data-ng-class="{active: row == 'statusEnum'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-statusEnum') "
									data-ng-class="{active: row == '-statusEnum'}"></span>
							</div>
						</th>

						<th id="appaction">
							<div class="th">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr id="selectappclick" class="maxLengthProp"
						data-ng-repeat="application in currentPageStores track by $index">
						<td>{{application.appId}}</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_ANLS_APP"><a
							id="selectappclick" href="javascript:;" class="ng-binding anchorColor"
							tooltip='{{application.appName}}'
							ng-click="showAppData(application.appName,'PERIODIC')">
								{{application.appName.length > 40 ?
								application.appName.substring(0,40).concat("...") :
								application.appName}} </a></td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_ANLS_APP"><a
							id="selectappclick" href="javascript:;"
							class="label-link ng-binding anchorColor"
							data-tooltip='{{application.appName}}'>
								{{application.appName}} </a></td>
						<td>{{application.createSt | date : 'medium' }}</td>
						<td>{{application.lastRun | date : 'medium' }}</td>
						<td>{{application.createdBy}}</td>
						<td>{{application.statusEnum | titleCase}}</td>
						<td width='8%' class=" {{projectAccessType == 'reviewer' ? 'disabledPoint' : ''}}">
							<ul class="nav-right ulMargin list-unstyled {{projectAccessType == 'reviewer' ? 'disabledActionIcon' : ''}}">
								<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.RUN_ANLS_APP"><a
									href="javascript:;" class="actionMenu"
									data-ng-click="checkLeafNode('spark',application)" tooltip-placement="top" tooltip="Run flow"><i
										class="fa fa-play"></i></a></li>
									<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE"><a
									href="javascript:;" class="actionMenu"
									data-ng-click="createClone(application)" tooltip-placement="top" tooltip="Clone a flow"><i
										class="fa fa-clone"></i></a></li>
									<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DELETE_ANLS_APP"><a
									href="javascript:;" class="actionMenu"
									data-ng-click="exportApp(application)" tooltip-placement="top" tooltip="Export flow"><i
										class="fa fa-upload"></i></a></li>
									<li
										data-ng-if="(user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE) && (storageStrategy == 'HDFS')"><a
										href="javascript:;" class="actionMenu" id="{{application.appName}}"
										ng-class="{'sched_enable': application.scheduled=='true'}"
										data-ng-click="scheduleJob(application)" tooltip-placement="top" tooltip="Schedule Job"><i
										class="fa fa-clock-o"></i></a></li>
									
								<!-- <li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE"><a
									href="javascript:;" class="actionMenu"
									data-ng-click="exploreApp(application.appId)" tooltip-placement="top" tooltip="Data In Hive"><i
										class="fa fa-eye"></i></a></li>
								<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_ANLS_APP"><a
									href="javascript:;" class="actionMenu"
									data-ng-click="showAppData(application.appName)" tooltip-placement="top" tooltip="Show flow"><i
										class="fa fa-pencil"></i></a></li> -->
								<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DELETE_ANLS_APP"><a
									href="javascript:;" class="actionMenu"
									data-ng-click="deleteApp(application)" tooltip-placement="top" tooltip="Delete flow"><i
										class="fa fa-trash"></i></a></li>
										
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numShowPage"
							data-ng-change="onNumPerPageChange()">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
</div>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="breadcrumb-main" style="padding-left: {{ breadcrumb }} !important;" ng-if="hideBreadCrumb()">
	<ul
		class="list-unstyled padBread breadcrumb header-css ul-border-breadcrumb breadcrumb-nav">
		
		<li class="padding-For-tab"
			ng-if='subMenuAccess(USER_ROLE_CONST.GLOSSARY) && (isInactive == "Category" || isInactive == "Tags") && (isLastParam == undefined || isLastParam == "" || isLastParam == null) '
			class="padding-For-tab"><a
			href='{{checkAccess(USER_ROLE_CONST.GLOSSARY) ? getLocation() : "#/glossaryView"}}'
			class='{{isInactive == "Tags" ? "activeTab": ""}} {{checkAccess(USER_ROLE_CONST.GLOSSARY) ? "disabledPoint" : ""}}'>Tags</a></li>

		<li class="padding-For-tab"
			ng-if='subMenuAccess(USER_ROLE_CONST.GLOSSARY) && (isInactive == "Category" || isInactive == "Tags") && (isLastParam == undefined || isLastParam == "" || isLastParam == null) '
			class="padding-For-tab"><a
			href='{{checkAccess(USER_ROLE_CONST.GLOSSARY) ? getLocation() : "#/categoryView"}}'
			class='{{isInactive == "Category" ? "activeTab": ""}} {{checkAccess(USER_ROLE_CONST.GLOSSARY) ? "disabledPoint" : ""}}'>Category</a></li>


		<!-- Discovery details started here -->
		<li class="padding-For-tab" data-ng-hide="getNotificationStatus()"
			ng-if='subMenuAccess(USER_ROLE_CONST.DISCOVERS) && isActive == "Catalog" && (isLastParam == undefined || isLastParam == "" || isLastParam == null)'><a
			href='{{checkAccess(USER_ROLE_CONST.DISCOVERS) ? getLocation() : "#/discover"}}'
			class='{{isInactive == "Discover" ? "activeTab" : ""}} {{checkAccess(USER_ROLE_CONST.DISCOVERS) ? "disabledPoint" : ""}}'>
				Discover</a></li>



		<li class="padding-For-tab has-feedback"
			ng-if='subMenuAccess(USER_ROLE_CONST.MY_APPROVALS) && isActive == "Notification"  && (isLastParam == undefined || isLastParam == "" || isLastParam == null)'><a
			href='{{checkAccess(USER_ROLE_CONST.MY_APPROVALS) ? getLocation() : "#/dataSourceToAccept"}}'
			class='{{isInactive == "Approvals" ? "activeTab" : ""}} {{checkAccess(USER_ROLE_CONST.MY_APPROVALS) ? "disabledPoint" : ""}}'>
				My Approvals <span data-ng-if="myApprovalNotificationCount != 0"
				class="badge badge-notify-my-approval">{{myApprovalNotificationCount}}</span>
		</a></li>

		<li class="padding-For-tab has-feedback"
			ng-if='subMenuAccess(USER_ROLE_CONST.MY_REQUEST) && isActive == "Notification" && (isLastParam == undefined || isLastParam == "" || isLastParam == null)'><a
			href='{{checkAccess(USER_ROLE_CONST.MY_REQUEST) ? getLocation() : "#/dataSourceRequests"}}'
			class='{{isInactive == "Requests" ? "activeTab" : ""}} {{checkAccess(USER_ROLE_CONST.MY_REQUEST) ? "disabledPoint" : ""}}'
			ng-click="myRequestReset();"> My Requests <span
				data-ng-if="myRequestNotificationCount != 0"
				class="badge badge-notify-my-request">{{myRequestNotificationCount}}</span>
		</a></li>


		<!-- My Collections details start here -->
		<li class="padding-For-tab" data-ng-hide="getNotificationStatus()"
			ng-if='subMenuAccess(USER_ROLE_CONST.MY_COLLECTIONS) && isActive == "Catalog" && (isLastParam == undefined || isLastParam == "" || isLastParam == null)'><a
			href='{{checkAccess(USER_ROLE_CONST.MY_COLLECTIONS) ? getLocation() : "#/myCollection"}}'
			class='{{isInactive == "Collections" ? "activeTab" : ""}} {{checkAccess(USER_ROLE_CONST.MY_COLLECTIONS) ? "disabledPoint" : ""}}'>
				My Collections</a></li>
	
		<!-- My Collections details end here -->
		
		<li class="sep-line-css"
			ng-if="isActive !== null && isActive !== undefined && isActive != 'Marketplace' "><img
			src="../styles/images/projectImages/sepration_line.png" height="27px"></li>

	<!-- Breadcrum start --><!-- ng-class="getLink(item)==" -->
		<li ng-repeat="item in headercontent track by $index" class="breadcrumb-li-normal" ng-if="item">
			
		    <a ng-if='$index != 0 && $index != headercontent.length - 1 && item' href="\#{{getLink(item)}}" ng-class="getLink(item) != currentPath ? '' : 'not-active'" >
				<span class="fontC" >{{item.length > 40 ? item.substring(0,40).concat("...") : item}}</span>
			</a> 
			
			<span ng-if='$index == 0 || $index == headercontent.length - 1 && item' class="fontD">
				 {{ item =='Home'? "" : (item.length > 40 ? item.substring(0,40).concat("...") : item)}}
			</span>
		</li>
	<!-- Breadcrum End -->
	
		<li
			class="langs text-normal li-height breadcrumb-li-before pull-right"
			ng-if="showOnlyProjectPage">
			<div class="col-md-12 breadcrumb-search">
				<div class="input-group">
					<input type="text" class="form-control input-md search"
						placeholder="Search" ng-model="projectNameSearch"
						data-ng-keyup='searchProjectByName(projectNameSearch);'>
					<div>
						<i class="glyphicon glyphicon-search projectSearchGliphy"></i>
					</div>
				</div>
			</div>
		</li>

	</ul>
</div>
<div ng-controller="connectorCtlr">
	<div class="row">
		
		<!-- fixed timeline view -->
		<%-- <div class="col-md-3">
			  <timeline class="col-md-12">
			  
			  	<!-- view select connector -->
			    <timeline-event side="left">
			      <timeline-badge class="bounce-in {{ timelineState > 0 ? 'success' :'info'}}" >
			        <a class="glyphicon" ng-class="{'disabledLink': disableAllLink, 'glyphicon-check': timelineState > 0}"   href="javascript:void(0)" ng-click="updateTimelineState(0)"></a>
			      </timeline-badge>
			      <timeline-panel class="info bounce-in">
			        <timeline-heading>
			          <h4><a href="javascript:void(0)" ng-class="{'disabledLink': (disableAllLink || timelineState < 1)}" ng-click="updateTimelineState(0)">Source</a></h4>
			        </timeline-heading>
			        <p class="text-overflow-elipse" data-toogle='tooltip'  title="{{connectorTypeName}}">{{connectorTypeName}}</p>
			      </timeline-panel>
			    </timeline-event>
			    
			    <!-- view select sub connector -->
			    <timeline-event side="left">
			      <timeline-badge class="bounce-in {{ timelineState > 1 ? 'success' :'info'}}">
			        <a class="glyphicon" ng-class="{'disabledLink': disableAllLink, 'glyphicon-check': timelineState > 1}"  href="javascript:void(0)" ng-click="updateTimelineState(1)"></a>
			      </timeline-badge>
			      <timeline-panel class="info bounce-in">
			        <timeline-heading>
			          <h4><a href="javascript:void(0)"  ng-class="{'disabledLink': (disableAllLink || timelineState < 2)}" ng-click="updateTimelineState(1)">Sub-source</a></h4>
			        </timeline-heading>
			        <p class="text-overflow-elipse" data-toogle='tooltip'  title="{{connectorSTName}}">{{connectorSTName}}</p>
			      </timeline-panel>
			    </timeline-event>
			    
			    <!-- view select sub connector -->
			    <timeline-event side="left">
			      <timeline-badge class="bounce-in {{ timelineState > 2 ? 'success' :'info'}}">
			      	<a class="glyphicon" ng-class="{'disabledLink': disableAllLink, 'glyphicon-check': timelineState > 2}" href="javascript:void(0)" ng-click="updateTimelineState(2)"></a>
			      </timeline-badge>
			      <timeline-panel class="info bounce-in">
			        <timeline-heading>
			          <h4><a href="javascript:void(0)" ng-class="{'disabledLink': (disableAllLink || timelineState < 3)}" ng-click="updateTimelineState(2)">Connection</a></h4>
			        </timeline-heading>
			         <p class="text-overflow-elipse" data-toogle='tooltip'  title="{{connectionDetails && connectionDetails.conncetionName ? connectionDetails.conncetionName : ''}}">{{connectionDetails && connectionDetails.conncetionName ? connectionDetails.conncetionName : ''}}</p>
			       	 <p class="text-overflow-elipse" data-toogle='tooltip' title="{{connectionDetails && connectionDetails.ipAddress ? connectionDetails.ipAddress : ''}}">{{connectionDetails && connectionDetails.ipAddress ? connectionDetails.ipAddress : ''}}</p>
			      </timeline-panel>
			    </timeline-event>
			    
			    <!-- view select sub connector -->
			    <timeline-event side="left">
			      <timeline-badge class="bounce-in {{ timelineState > 3 ? 'success' :'info'}}">
			        <i class="glyphicon" ng-class="{'glyphicon-check': timelineState > 3 }"></i>
			      </timeline-badge>
			      <timeline-panel class="info bounce-in">
			        <timeline-heading>
			          <h4 ng-disabled="disableAllLink" ng-class="{'disabledLink': (disableAllLink || timelineState < 4)}">Publish</h4>
			        </timeline-heading>
			        <div class="substps" ng-if="timelineState > 2 ">
				        <h5>
				        	<a ng-class="{'active' : timelinePublishSubState == 0 ,  'disabledLink': (disableAllLink || timelinePublishSubState < 1)}" href="javascript:void(0)" ng-click="updateTimelinePublishState(0)" ng-disabled="disableAllLink">
				        		<i class="glyphicon padding-right-5 publish-sub-step-clear-icon" ng-class="{'glyphicon-ok' : timelinePublishSubState > 0}"></i>
				        		Select datasources
			        		</a>
			        	</h5>
				        <h5>
				        	<a ng-class="{'active' : timelinePublishSubState == 1 ,  'disabledLink': (disableAllLink || timelinePublishSubState < 2)}" href="javascript:void(0)" ng-click="updateTimelinePublishState(2)" ng-disabled="disableAllLink">
				        		<i class="glyphicon padding-right-5 publish-sub-step-clear-icon" ng-class="{'glyphicon-ok' : timelinePublishSubState > 1}"></i>
				        		 Expert and tags
			        		</a>
		        		</h5>
				    </div>    
			      </timeline-panel>
			    </timeline-event>			   
		</div> --%>
		
		<!-- publish select source connector view -->
		<div class="col-md-12">
			<div id="connectors" >
				<div class="col-md-12">
					<div class="timeline-div-search-box margin-top15 text-right">
						<i class="search-glyphicon glyphicon glyphicon-search "></i>
						<input type="text" class="form-control"  ng-model="searchText" placeholder="Search sources" aria-haspopup="true" aria-expanded="true" />
					</div>
				</div>
				<div class="col-md-12 timeline-div-body padding0 discover-scroll"  ng-init="setViewHeight()" style="height:{{viewHeight - 60}}px;overflow-y:auto" ng-if="connectorTypeList && connectorTypeList.length > 0" >
				    <!-- csv  -->
					<div class='col-sm-2 grid-padding-top' ng-repeat="connector in connectorTypeList | filter:searchText">
						<div class='gridCard grid-height' ng-click="selectConnector(connector.sourceId,connector.connectionType)">
							<div class=" col-md-12 text-center gridCard-image grid-margin-top">
								<img class="img-responsive connectorImage" alt='{{connector.connectionType}}'
										ng-src="{{connector.sourceImagePath}}"/>
							</div>
							<div class="col-md-12 text-center margin-top15" >{{connector.connectionType}}</div>
						</div>
					</div>
					<!-- csv  -->
				</div>	
				<div class="col-md-12 timeline-div-body" style="height:{{viewHeight - 60}}px;" ng-if="!connectorTypeList || connectorTypeList.length === 0">
					<div class="col-md-12">
						No connectors configured.
					</div>
				</div>				
			</div>
	   </div>
	</div>
</div>
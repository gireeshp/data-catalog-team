<div ng-controller="subConnectorCtlr">
	<div class="row">
		<!-- publish select source connector view -->
		<div class="col-md-12">
			<div id="subconnectors" >
				<div class="col-md-12">
					<div class="timeline-div-search-box margin-top15 text-right">
						<i class="search-glyphicon glyphicon glyphicon-search "></i>
						<input type="text" class="form-control"  ng-model="searchText" placeholder="Search subsources" aria-haspopup="true" aria-expanded="true" />
					</div>
				</div>
				<div ng-init="setViewHeight()" class="col-md-12 timeline-div-body padding0 discover-scroll"  style="height:{{viewHeight - 60}}px;overflow-y:auto" ng-if="connectorSubTypeList && connectorSubTypeList.length > 0" >
				    <!-- csv  -->
					<div class='col-sm-2 grid-padding-top'ng-repeat="connectorST in connectorSubTypeList | filter:searchText">
						<div class='gridCard grid-height' ng-click="selectConnectorSubType(connectorST.subSourceId,connectorST.subConnectionType)">
							<div class=" col-md-12 text-center gridCard-image grid-margin-top">
								<img class="img-responsive connectorImage" alt='{{connectorST.subConnectionType}}' ng-src="{{connectorST.sourceImagePath}}"/>
							</div>
							<div class="col-md-12 text-center margin-top15" >{{connectorST.subConnectionType}}</div>
						</div>
					</div>
					<!-- csv  -->
				</div>	
				<div class="col-md-12 timeline-div-body" style="height:{{viewHeight - 60}}px;" ng-if="!connectorSubTypeList || connectorSubTypeList.length == 0">
					<div class="col-md-12">
						No sub-connectors configured.
					</div>
				</div>				
			</div>
	   </div>
	</div>
</div>
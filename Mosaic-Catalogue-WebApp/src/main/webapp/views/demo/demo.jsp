<div class="page page-profile" ng-controller="maxiqDemoController">
	<div class="row">
		<div class="col-md-2">
			<section class="panel panel-default mail-categories " >
				<div class="panel-heading ">
					Customers
				</div>
				<ul class="list-group">
	           		<li class="list-group-item"  ng-repeat="customer in customers">
	           			<a href="javascript:;" ng-class="{'selected-repo' : customer.custName == selectedRCust.custName}" ng-click="grabCustInfo(customer)">
	           			   {{customer.custName}}
	           			</a>
	           		</li>
           		</ul>
			</section>
		</div>
		<div class="col-md-8">
			<section class="panel panel-default" >
				<div class="panel-heading col-md-12" style="text-transform: capitalize;">
					<div class="col-md-5" >
						{{selectedRCust.custName}} 
						<span ng-if="selectedRCust.custSex">
							,</br>
						</span> 
						{{selectedRCust.custSex}} 
						<span ng-if="selectedRCust.custPlace">
							,</br>
						</span> 
						{{selectedRCust.custPlace}}
					</div>
					<div class="col-md-4" ng-if="selectedRCust.custSince">
						<span>Customer Since {{selectedRCust.custSince}}</span></br>
						<span>{{selectedRCust.custType}}</span>
					</div>	
					<div class="col-md-3" ng-if="selectedRCust">
						<a href="javascript:;">{{selectedRCust.custSell}} Potential Cross-sell</a>
					</div>				
				</div>
				<div class="panel-body">
					<zoomable-sunburst  data="myData"></zoomable-sunburst>
				</div>
			</section>
		</div>
		<div class="col-md-2">
			<section class="panel panel-default" >
				<div class="panel-heading " style="text-transform: capitalize;">Next Best Offer</div>
				<div class="panel-body">
					
				</div>
			</section>
		</div>
	</div>
</div>

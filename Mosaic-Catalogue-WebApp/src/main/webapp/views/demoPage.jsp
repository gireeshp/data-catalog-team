<div class="container" ng-controller="treeViewController">
    <div class="row">
      <div class="col-lg-6">
        <h3>Tree view
          <a href="" class="btn btn-default pull-right" ng-click="collapseAll()">Collapse all</a>
          <a href="" class="btn btn-default pull-right" ng-click="expandAll()">Expand all</a>
        </h3>
			<script type="text/ng-template" id="items_renderer.html">
                <div ui-tree-handle>
                  <a class="btn btn-success btn-xs" data-nodrag ng-click="toggle(this)"><span class="fa" ng-class="{'fa-plus': collapsed, 'fa-minus': !collapsed}"></span></a>
                  {{item.title}}
                </div>
                <ol ui-tree-nodes="options" ng-model="item.items" ng-class="{hidden: collapsed}">
                  <li ng-repeat="item in item.items" ui-tree-node ng-include="'items_renderer.html'">
                  </li>
                </ol>
              </script>
              <div ui-tree="options" data-drag-enabled="tree.enabled" data-max-depth="5" data-drag-delay="500">
                <ol ui-tree-nodes ng-model="list"  >
                  <li ng-repeat="item in list" ui-tree-node ng-include="'items_renderer.html'"></li>
                </ol>
              </div>
  


     <!--  <div class="col-lg-6">
        <h3>Data binding</h3>
        <div class="info">
            {{info}}
        </div>
        <pre class="code">{{ data | json }}</pre>
      </div>
    </div> -->

  </div>
<div ng-controller="exploreHiveController">
	<span ng-bind-html="explanation['QueryEditorHeader'] | unsafe"></span>
	<div class="col-md-12">
		<!-- tree html -->
		<div class="col-md-4">
			<div class="panel panel-catalog" style="height:{{viewHeight - 25}}px;" ng-init="setViewHeight()">
                <div class="panel-heading">
                    Data
                </div>
                <div class="panel-body" ng-if="!isDsFetch" >
                	<div class="margin-15-minus"><i class="glyphicon glyphicon-search search-glyphicon-publish"></i><input style="padding:2px;" type="text" class="tree-search-box" ng-model="dsSearch" placeholder="Search sources"/></div>
               		<div ivh-treeview="dataForTheTree" ivh-treeview-filter="dsSearch" ivh-treeview-expand-to-depth="1"
               			ivh-treeview-on-cb-change="onTreeSelectionChange()">
               			<script type="text/ng-template">
        					<div title="{{trvw.label(node)}}">
          						<span ivh-treeview-toggle>
            						<span ivh-treeview-twistie></span>
         						</span>
          						<span ng-if="!node._genericField && node.label !== 'Columns'" ivh-treeview-checkbox>
          						</span>
								<span class="ivh-treeview-node-label" ivh-treeview-toggle ng-if="!node._genericField">
           							{{trvw.label(node)}}
         						</span>
								<span class="ivh-treeview-node-label" ivh-treeview-toggle ng-if="node._genericField && node.label != 'Columns'">
									<img ng-src="../styles/images/publish_catalog/property.png"/>
           							{{trvw.label(node)}}
         						</span>
         						<div ivh-treeview-children></div>
       						</div>
      					</script>
               		</div>
                </div>
                <div class="panel-body tree-loader" ng-if="isDsFetch">
              	</div>
            </div>
		</div>
		<!-- tree html end-->
		
		<!-- query panel html -->
		<div class="col-md-8 margin-top15 discover-scroll" style="height:{{viewHeight - 25}}px;border-left:1px solid #ccc;overflow-y:auto;overflow-x:hidden;padding-right:25px;">
			<div ng-if="selectedDatasources.length > 0"><h5>Selected sources</h5></div>
			<div class="row  padding-left15 margin-bottom10 height70" ng-if="selectedDatasources.length > 0 " >
				<div class="hive-selected-div">
					<div class="card" ng-repeat="obj in selectedDatasources">
						<div class="display-inline-flex">
							<div><img class="hive-selected-div-img" alt="{{datasourceAltNames[obj.dataSourceType]}}" ng-src="{{datasourceImages[obj.dataSourceType]}}"/></div>
							<div class="hive-selected-text-div eplise-text" data-toogle="tooltip" title="{{obj.label}}">{{obj.label}}</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-left15 margin-bottom10">
				<div class="fL">
					<span class="col-md-4 ui-select selectMargin"> <select style="line-height: 15px !important;"
						ng-model="executeSelectedQueryCombo"
						ng-disabled='abortSaveOutput'
						ng-options="query for query in executeSelectedQuery"
						data-ng-change="onQueryNameChange()" class="selectCss"><option
								value="">--Select query--</option></select>
					</span>
					
					<span class="col-md-6">
						<input type="text" ng-model="query.queryName"
							ng-disabled='abortSaveOutput'
							tooltip='{{explanation["QueryName"]}}' tooltip-placement="bottom"
							class="form-control" placeholder="New query name" ng-trim="true">
					</span>
					
					<span>
						<button type="button"
						ng-disabled='abortSaveOutput'
						class="btn  btn-line-default" style="margin-top: 1px;"
						data-ng-show="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE_SAVE_QUERY"
						ng-click="saveQuery(query.queryName);">Save</button>
					</span>
					
					
				</div>
				<div class="pull-right btn-inner">
					<button type="button"
						class="col-md-1 btn btn-default btn-minus defaultB"
						ng-click="sizeChanger(-4);" tooltip="Shrink Editor"
						tooltip-placement="top">
						<i class="fa fa-minus"></i>
					</button>
					<button type="button"
						class="col-md-1 btn btn-default btn-textarea btnCss"
						ng-click="resizeText(4);" tooltip="Grow Editor"
						tooltip-placement="top">
						<i class="fa fa-plus"></i>
					</button>
					<button type="button"
						class="col-md-1 btn btn-default btn-fullscreen"
						ng-click="fullTextarea();" tooltip="Fullscreen"
						tooltip-placement="top">
						<i class="fa fa-arrows-alt"></i>
					</button>
				</div>
			</div>
			
			<div class="row padding-left15 margin-bottom10">
				<textarea name="" id="queryeditor" class="form-control"
						rows='{{rowSize}}' ng-model="queryeditor"
						ng-disabled='abortSaveOutput'
						placeholder="Write your query here"
						style='width: 100%; height: {{myCssApply}}'>
							</textarea>
			</div>
			
			<div class="row padding-left15 pull-right  margin-bottom10">
				<button type="button" class="btn  btn-gap-v btn-line-default"
					ng-disabled='abortSaveOutput'
					data-ng-show="(user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE_SAVE_QUERY) || 
						        (user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE_RUN_QUERY) || 
						        (user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE_SAVE_OP)"
					ng-click="clearQuery();">Clear</button>
				<button type="button" class="btn  btn-gap-v btn-line-default"
					data-ng-show="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DATA_EXPLORE_RUN_QUERY"
					ng-click="runQuery();"
					ng-disabled="abortSaveOutput">Run</button>
			</div>
			
			<div class="row padding-left15  margin-bottom10 clear_both" ng-if="gridOptions && gridOptions.data.length > 0">
				<button class="btn btn-primary fR" csv-header="headerRow"
					ng-csv="queryResult" filename="Export Query Result.csv"
					field-separator="," data-toogle="tooltip" title="export to csv">
					<span class="fa fa-download"></span>&nbsp; Export
				</button>
				<div ui-grid="gridOptions" class="grid-table-margin grid gridHeight" ui-grid-resize-columns ui-grid-move-columns></div>	
			</div>
		</div>
		<!-- query panel html end -->
	</div>
</div>

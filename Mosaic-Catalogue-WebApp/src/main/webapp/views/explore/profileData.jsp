<div id="profileDataCtrl" data-ng-controller="profileDataCtrl" ng-init="loadProfileData();" >
	<!-- <script src="scripts/libs/d3.layout.cloud.js"></script> -->
	<div class="col-md-12">
		<div class="col-md-12">
			<div class="fR">
			  <div class="displayInline">
			  	 <label class="selectBoxLabelMarginSamleData">Sample Size: </label>
			  	 <span>
			  	 	<select id="limit" class="form-control" ng-model="limit" ng-change="loadProfileDataByLimit();" ng-options="size as size for size in sampleSize track by size">
				 	</select>
			  	 </span>
			  </div>
			</div>
		</div>
		<div class="col-md-12 ">&nbsp</div>
		<div id="profileFieldsTable"  ng-class="{'col-sm-7': showRightPanel === true, 'col-sm-12': showRightPanel === false}">
			<div class="row">
				<!-- <div class="mini-box minoBox40px bg-primary">
					<div class="box-info col-md-8">	
						<p style="margin: 7px 0 0px !important;">
							<font color="white" class="workflow-name"
								tooltip-placement="right" tooltip='{{appName}}'> 
								{{dsName.length > 60 ? dsName.substring(0, 60).concat("...") :
								dsName}} </font> 
								
								<i class="glyphicon glyphicon-check-icon"></i> &nbsp;<a
								href="javascript:;" ng-click="edit()" tooltip-placement="right"
								 tooltip="Edit flow"><i class="fa fa-pencil fa-lg editBtn"></i></a>
						</p>
						<p class="">Version : Current</p>
					</div>
					<div class="col-md-4">
						<div class="fR">
						  <div class="displayInline">
						  	 <label class="selectBoxLabelMarginSamleData">Sample Size: </label>
						  	 <span>
						  	 	<select id="limit" class="form-control" ng-model="limit" ng-change="loadProfileDataByLimit();" ng-options="size as size for size in sampleSize track by size">
							 	</select>
						  	 </span>
						  </div>
						</div>
					</div>
				</div>	 -->
			</div>
			
			<div class="table-dynamic" class="profileLeftSidePanel">
				<div ng-if="profileDataArray.length > 0">						
					<div id="profileTableData">
						<table class="table table-striped table-hover table-responsive">
							<thead>
							    <tr>
									<td class="fontBlack"><b>Field Name</b></td>
									<td class="fontBlack" ><b>Data Type</b></td>
									<td class="fontBlack"><b>Scale Type</b></td>
									<td class="fontBlack"><b>Distinct Count</b></td>
									<td class="fontBlack"><b>Unique Count(%)</b></td>
									<td class="fontBlack"><b>Missing(%)</b></td>
								</tr>
							</thead>
							<tbody>
								<tr id="{{obj.fieldName.split('.')[1]}}_row" ng-repeat="obj in profileDataArray" ng-click="profileFieldSelected($event, obj);" ng-class="{'selectedRow': selectedRow == obj.fieldName}">
									<td>{{obj.fieldName.split(".")[1]}}</td>
									<td>{{obj.fieldType != undefined && obj.fieldType != null ?  (obj.fieldType | uppercase) : '-'}}</td>
									<td>
										<div class="dropdown">
											<a href="javascript:void(0);"  ng-click="$event.stopPropagation()" class="dropdown-toggle" data-toggle="dropdown">
												{{obj.scale}}
												<span class="caret"></span>
											</a>
											
											<ul class="dropdown-menu" ng-click="$event.stopPropagation()">
											    <li ng-repeat="scale in scaleTypeList" ng-if="scale !== obj.scale" ><a ng-click="getScaleWiseProfileData($event, scale, obj.fieldName.split('.')[1]);" href="javascript:void(0);"  >{{scale}}</a></li>
											 </ul>
										</div>
									</td> 
									<td>{{obj.distinctCount  ?  obj.distinctCount : '0'}}</td>
									<td>{{obj.distinctCountPct ? ( obj.distinctCountPct | commaToDecimal) : '0'}}</td>
									<td>{{obj.missingPct ?  ( obj.missingPct | commaToDecimal)  : '0'}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div id="rightSidePanelInfo" ng-if="showRightPanel" class="col-sm-5 profileRightSidePanel" ng-class="{'animate-fade-up':showRightPanel === true , 'animate-fade-up':showRightPanel === false }">
			<div ng-if="selectedObj.scale === 'categorical'">
				<div class="row borderBottom1px">
					<div class="col-sm-4">
						<h4>Summary</h4>
					</div>
					<div class="col-sm-8 profileSummaryRghtPanel">
						<span>Column Name: </span>
						<sapn><b class="fontBlack">{{selectedObj.fieldName.split(".")[1]}}</b></sapn>
					</div>
				</div>
				<div class="row marginTopBottom10px">
					<div class="col-sm-4">
						<h3 class="noMarginBottom fontBlack">{{selectedObj.categoricalStats.distinct}}</h3>
						<h5 class="noMarginTop">DISTINCT</h5>
					</div>
					<div class="col-sm-4">
						<h3 class="noMarginBottom fontBlack">{{selectedObj.categoricalStats.missing}}</h3>
						<h5 class="noMarginTop">MISSING</h5>
					</div>
					<div class="col-sm-4">
						<h3 class="noMarginBottom fontBlack">{{selectedObj.categoricalStats.mode}}</h3>
						<h5 class="noMarginTop">MODE</h5>
					</div>
				</div>
				<div class="col-sm-12 borderBottom1px paddingLeft0 paddingRight0" ng-if="selectedObj.categoricalStats.frequencyTable.length > 0">
					<h5 class="fontBlack" style="margin-bottom:5px;"><b>Frequency Table</b></h5>
				</div>
				<div class="col-sm-12 paddingLeft0 paddingRight0 marginTop10" ng-if="selectedObj.categoricalStats.frequencyTable.length > 0">
					<table class="table table-borderless">
						<thead>
							<tr>
								<td><b class="fontBlack">Data</b></td>
								<td><b class="fontBlack">Frequency</b></td>
								<td><b class="fontBlack">Percentage</b></td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="row in selectedObj.categoricalStats.frequencyTable">
								<td>{{row.data}}</td>
								<td>{{row.frequency}}</td>
								<td>
									<span class="profileSummaryProgressBarText">{{(row.percentage | commaToDecimal) }}%</span>
									<div class="progress" style='height: 10px;clear:both;'>
									  <div class="progress-bar" role="progressbar" style="width: {{row.percentage}}%;" aria-valuenow="{{row.percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div ng-if="selectedObj.scale === 'continuous'" >
				<div class="row borderBottom1px">
					<div class="col-sm-4">
						<h4>Summary</h4>
					</div>
					<div class="col-sm-8 profileSummaryRghtPanel">
						<span>Column Name: </span>
						<sapn><b class="fontBlack">{{selectedObj.fieldName.split(".")[1]}}</b></sapn>
					</div>
				</div>	
				<div class="row">
						<div class="col-sm-6">
							<div class="col-sm-12"><h5 class="blueTextColor">Quantile Statistics</h5></div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Minimum</span>
								<span class="fR">{{selectedObj.continuousStats.minimum  ? ( selectedObj.continuousStats.minimum | commaToDecimal  ) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">5-th Percentile</span>
								<span class="fR">{{selectedObj.continuousStats.fifthPercentile  ? ( selectedObj.continuousStats.fifthPercentile | commaToDecimal )  : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Q1</span>
								<span class="fR">{{selectedObj.continuousStats.q1  ? (selectedObj.continuousStats.q1 | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">Median</span>
								<span class="fR">{{selectedObj.continuousStats.median  ? (selectedObj.continuousStats.median | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Q3</span>
								<span class="fR">{{selectedObj.continuousStats.q3  ? ( selectedObj.continuousStats.q3 | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">95-th Percentile</span>
								<span class="fR">{{selectedObj.continuousStats.nintyFifthPercentile  ? ( selectedObj.continuousStats.nintyFifthPercentile | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Maximum</span>
								<span class="fR">{{selectedObj.continuousStats.maximum  ? (selectedObj.continuousStats.maximum | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">Range</span>
								<span class="fR">{{selectedObj.continuousStats.range ? (selectedObj.continuousStats.range | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Interquartile range</span>
								<span class="fR">{{selectedObj.continuousStats.iqr ? (selectedObj.continuousStats.iqr | commaToDecimal) : '0'}}</span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="col-sm-12"><h5 class="blueTextColor">Descriptive Statistics</h5></div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Standard deviation</span>
								<span class="fR">{{selectedObj.continuousStats.stdDeviation ? ( selectedObj.continuousStats.stdDeviation | commaToDecimal)  : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">Coef of variation</span>
								<span class="fR">{{selectedObj.continuousStats.cooefOfVariation ? (selectedObj.continuousStats.cooefOfVariation | commaToDecimal) : '0' }}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Kurtosis</span>
								<span class="fR">{{selectedObj.continuousStats.kurtosis ? (selectedObj.continuousStats.kurtosis | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">Mean</span>
								<span class="fR">{{selectedObj.continuousStats.mean  ? (selectedObj.continuousStats.mean | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">MAD</span>
								<span class="fR">{{selectedObj.continuousStats.mad  ? (selectedObj.continuousStats.mad | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">Skewness</span>
								<span class="fR">{{selectedObj.continuousStats.skewness  ? (selectedObj.continuousStats.skewness | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Sum</span>
								<span class="fR">{{selectedObj.continuousStats.sum ? (selectedObj.continuousStats.sum | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoEven">
								<span class="fL">Variance</span>
								<span class="fR">{{selectedObj.continuousStats.variance ? (selectedObj.continuousStats.variance | commaToDecimal) : '0'}}</span>
							</div>
							<div class="col-sm-12 profileSummaryFiledInfoOdd">
								<span class="fL">Memory size</span>
								<span class="fR">{{selectedObj.continuousStats.memorySize  ? (selectedObj.continuousStats.memorySize | commaToDecimal) : '0'}}</span>
							</div>
						</div>
					</div>
				<div class="row">
					<div class="col-sm-12">
						<h5 class="grapghHeaderProfilePage">HISTOGRAM</h5>
						<div id="histogramDiv">
							<svg id="profileHistogram" width="100%" height="100%" style="margin-top: 12px;"></svg>
 						</div>
					</div>
					<div class="col-sm-12">
						<h5 class="grapghHeaderProfilePage">BOXPLOT</h5>
						<div>	
							<svg id="boxPlotGraph" width="100%" height="100%" style="margin-top: 12px;"></svg>
						</div>	
					</div>
				</div>
			</div>	
			<div ng-if="selectedObj.scale === 'text'">
				<div class="row borderBottom1px">
					<!-- <div class="col-sm-4">
						<h4>Summary</h4>
					</div> -->
					<div class="col-sm-12 profileSummaryRghtPanel">
						<span>Column Name: </span>
						<sapn><b class="fontBlack">{{selectedObj.fieldName.split(".")[1]}}</b></sapn>
					</div>
				</div>
				<div class="row marginTopBottom10px">
					<div id="wordCloudeGraph"></div>
				</div>
			</div>	
		</div>
	</div>
	<div class="dw-loading dw-loading-overlay custom-loading" style="display: {{ loader ?  'block' : 'none'}}"><div class="dw-loading-body"><div class="dw-loading-spinner"></div>
		<div class="dw-loading-text">Loading Data...</div>
		</div>
	</div>
</div>

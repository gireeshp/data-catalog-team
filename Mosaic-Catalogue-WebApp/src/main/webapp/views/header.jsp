<header class="navbar navbar-inverse navbar-xs">
	<div class="container-fluid" data-ng-app="detailCookies">
		<div class="navbar-header">
			<span class="navbar-brand navbar-brand-xs"><img alt="catlog" ng-src="../styles/images/publish_catalog/Mosaic_catalog.png" style="    margin-top: -6px;height: 25px;"></span>
		</div>
		<div id="trigger"
			class="menu-button hamburger {{ isNavHideTest() ? '' : left && left == '6px' ? 'hamburger-arrow-left' : 'hamburger-arrow-right'}}"
			ng-click="changeWidth(left)" style="left: {{ isNavHideTest() ? '84px' : left && left != '5px' ? '240px': '5px'}} !important;">
			<span class="icon"></span>
		</div>
		<ul class="nav navbar-nav navbar-nav-xs">
			
			
		</ul>
		<ul class="nav navbar-nav navbar-nav-xs navbar-right work-Margin">
			
			<!-- User Guide Menu -->
			<li class="dropdown langs text-normal" data-ng-controller="notificationController">
				<a href="javascript:;" ng-click="getMyApprovals()" class="dropdown-toggle user-css width-32px">
					<img src="../styles/images/projectImages/notification.png">
					<span data-ng-if="notificationCount != 0" class="badge badge-notify">{{notificationCount}}</span>
				</a>
				
			</li>
			
			
			<!-- User Profile Menu -->
			<li class="dropdown langs text-normal user-profile"
				data-ng-controller="UseCookieCtrl"><a href="javascript:;"
				dropdown is-open="status.isopen1" class="dropdown-toggle user-css"
				data-toggle="dropdown"> <img alt="users" src="../styles/images/landingImg/user.png" data-ng-click="updateGroupName();"> <span>
						<span data-i18n="" data-ng-show="checkCookie();"></span>
				</span>
			</a>
			<!-- logout-dropdown -->
				<ul class="dropdown-menu with-arrow pull-right dropMenu fadeInDown">
					<li class="userImg"><a href="javascript:;" data-ng-click="profile();">
							<span class="head-menu">{{loginUserId}}</span></a></li>

					<li data-ng-if="group.isSelected" class="loginImg" data-ng-repeat="group in user.GROUP_DETAILS track by $index">
						<a href="javascript:;"> <img>
							<span class="head-menu">Logged in as <b>{{group.groupName}}</b></span>
					</a>
					</li>

					<li
						class="groupImg filter-scroll-css mrg-tp-btm-5px full-width-block"
						id="style-1" data-ng-click="$event.stopPropagation();"
						data-ng-if="user.GROUP_DETAILS.length != 1">
						<div class="row" data-ng-show="user.GROUP_DETAILS.length > 5">
							<i
								class="glyphicon glyphicon-search col-md-2 search-group-margin"></i>
							<div class="col-md-9">
								<input type="text" class="form-control input-sm"
									placeholder="Search Group ..."
									data-ng-model='$parent.searchGroup'
									data-ng-click="$event.stopPropagation();">
							</div>
						</div> <a data-ng-if="!group.isSelected" class=" addCursor"
						data-ng-repeat="group in user.GROUP_DETAILS | filter:{groupName:$parent.searchGroup} track by $index"
						data-ng-click="loginAs(group.groupId);"> <img> <span
							href="javascript:;" class="head-menu">Log in as <i>{{group.groupName}}</i>
						</span>
					</a>
					</li>

					<li class="logoutImg"><a href="javascript:;" data-ng-click="logoutCookie(true);">
							<img> <span class="head-menu" data-i18n="Log out">Log out</span>
					</a></li>

				</ul></li>
				
		</ul>
		<div data-ng-controller="LangCtrl"> </div>
	</div>
</header>

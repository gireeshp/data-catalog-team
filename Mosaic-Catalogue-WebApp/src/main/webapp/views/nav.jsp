<style>

#Menu li:hover > ul {display:block;}

#Menu li{width: 100%;}

/* Menu Sorces Dropdown classes start */
#Menu .dropdown-menu-source {
	position: absolute;
	top: 26%;
	left: 100%;
	z-index: 1000;
	display: none;
	float: left;
	min-width: 60px;
	padding: 5px 0;
	margin: 2px 0 0;
	list-style: none;
	font-size: 14px;
	text-align: left;
	background-color: #464644;
	border: 1px solid #ccc;
	border: 1px solid rgba(0, 0, 0, 0.15);
	border-radius: 2px;
	-webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
	box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
	background-clip: padding-box;
}

#Menu .dropdown-menu-source.pull-right {
	right: 0;
	left: auto;
}

#Menu .dropdown-menu-source .divider {
	height: 1px;
	margin: 1px 0px;
	overflow: hidden;
	background-color: #e5e5e5;
}
#Menu .dropdown-menu-source .divider:HOVER{}

#Menu .dropdown-menu-source>li>a {
	display: block;
    padding: 17px 20px;
    clear: both;
    font-weight: normal;
    line-height: 22px;
    color: #fff;
    white-space: nowrap;
    text-align: center;
}
#Menu .dropdown-menu-source > li a:hover {
	background-color: #333 !important;
}
#Menu .dropdown-menu-source>li a:focus{
	text-decoration: none;
	color: #fff;
	background-color: #464644;
}

#Menu .dropdown-menu-source>.active>a, .dropdown-menu-source>.active>a:hover, .dropdown-menu-source>.active>a:focus
	{
	color: #fff;
	text-decoration: none;
	outline: 0;
	background-color: #1C7EBB;
}

#Menu .dropdown-menu-source>.disabled>a, .dropdown-menu-source>.disabled>a:hover,
	.dropdown-menu>.disabled>a:focus {
	color: #777777;
}
.show {display:block;}
/* Menu Sorces Dropdown classes close */


</style>
<nav id="Menu" ng-if="!isNavHideTest()">
	<div id="nav-wrapper" style=" transition: {{transition}}; transform: {{transform}}; width: {{width}};">
		<div id="sidebar" class="sidebar">
			<div class="sidebar-outer">
				<div class="sidebar-main" data-panel="Workspace">
					<ul class="sidebar-ul">
						<!-- DISCOVER -->
						<li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.SEARCH_APPS) "
							class='{{!checkAccess(USER_ROLE_CONST.SEARCH_APPS) ? "" : "disabledPoint"}}'><a
							href='{{!checkAccess(USER_ROLE_CONST.DISCOVERS) ? getForDiscover() : getLocation()}}'
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.DISCOVERS) ? "addCursor" : "disabledIcon"}} {{isInactive == "Discover"  || isInactive == "Collections" ? "sidebar-element-active" : ""}}'
							data-name="Jobs">
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="DISCOVER"
											src="../styles/images/projectImages/discover.png">
									</div>
								</div> <span class="menuTextColor">Discover</span>
						</a></li>
						
						<!-- Explore -->
						<li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.EXPLORE)"
							class='{{!checkAccess(USER_ROLE_CONST.EXPLORE) ? "" : "disabledPoint"}}'><a
							href='{{!checkAccess(USER_ROLE_CONST.EXPLORE) ? getForExplore() : getLocation()}}'
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.EXPLORE) ? "addCursor" : "disabledIcon"}} {{isActive == "Explore" ? "sidebar-element-active" : ""}}'
							data-name="Clusters">
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="Explore"
											src="../styles/images/projectImages/leftsidebar-visualize.png">
									</div>
								</div> <span class="menuTextColor">Explore</span>
						</a></li>
						
						<!-- HOME -->
						<!-- <li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.HOME)"
							class='{{!checkAccess(USER_ROLE_CONST.HOME) ? "" : "disabledPoint"}}'><a
							href='{{!checkAccess(USER_ROLE_CONST.HOME) ? getForCatalogHome() : getLocation()}}'
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.HOME) ? "addCursor" : "disabledIcon"}} {{isInactive == "Home" ? "sidebar-element-active" : ""}}'
							data-name="Home">
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="HOME"
											src="../styles/images/projectImages/home_icon.png">
									</div>
								</div> <span class="menuTextColor">Home</span>
						</a></li> -->
						
						<!-- PUBLISH -->
						
						<li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.CONNECTOR)"
							class='{{!checkAccess(USER_ROLE_CONST.CONNECTOR) ? "" : "disabledPoint"}}'><a
							href='{{!checkAccess(USER_ROLE_CONST.CONNECTOR) ? getForConnctorHome() : getLocation()}}'
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.CONNECTOR) ? "addCursor" : "disabledIcon"}} {{isInactive == "Connectors" ? "sidebar-element-active" : ""}}'
							data-name="Publish">
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="Publish"
											src="../styles/images/projectImages/publish.png">
									</div>
								</div> <span class="menuTextColor">Publish</span>
						</a></li>							
						
						
									
						
						<li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.CONNECTIONS) && !marketplace && adminMenu"
							class='{{!checkAccess(USER_ROLE_CONST.CONNECTIONS) ? "" : "disabledPoint"}}'><a
							href='{{!checkAccess(USER_ROLE_CONST.CONNECTIONS) ? getForConnection() : getLocation()}}'
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.CONNECTIONS) ? "addCursor" : "disabledIcon"}} 
							{{isInactive == "Connections" ? "sidebar-element-active" : ""}}' data-name="Connections">
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="Search App"
											src="../styles/images/projectImages/Connection.png">
									</div>
								</div> <span class="menuTextColor">Connections</span>
						</a></li>
						

						<li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.GLOSSARY)"
							class='font-color-white myColl {{!checkAccess(USER_ROLE_CONST.GLOSSARY) ? "" : "disabledPoint"}}'><a
							href="{{!checkAccess(USER_ROLE_CONST.GLOSSARY) ? getForGlossary() : getLocation()}}"
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.GLOSSARY) ? "addCursor" : "disabledIcon"}} {{isInactive == "Tags"  || isInactive == "Category" ?"sidebar-element-active" : ""}}'>
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="User management"
											src="../styles/images/projectImages/glossary_icon_left_menu.png">
									</div>
								</div> <span class="menuTextColor">Data dictionary</span>
						</a></li>

						<!-- API -->
						 <li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.ANLS_APPS)"
							class='{{!checkAccess(USER_ROLE_CONST.ANLS_APPS) ?  "" : "disabledPoint"}}'><a
							href='{{!checkAccess(USER_ROLE_CONST.ANLS_APPS) ? getForApi() : getLocation()}}'
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.ANLS_APPS) ? "addCursor" : "disabledIcon"}} {{isInactive == "API" ? "sidebar-element-active" : ""}}'
							data-name="Flow">
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="API"
											src="../styles/images/projectImages/api_menu.png">
									</div>
								</div> <span class="menuTextColor">API</span>
						</a></li>
						<!-- SETTING -->
						<li
							ng-if="(user.MASTERS_CODES | isRoleAssigned: USER_ROLE_CONST.SEARCH_APPS)"
							class='{{!checkAccess(USER_ROLE_CONST.SEARCH_APPS) ? "" : "disabledPoint"}}'><a
							class='navigation-bar-border sidebar-element {{!checkAccess(USER_ROLE_CONST.ADMIN_DASHBOARD) ? "addCursor" : "disabledIcon"}} {{(isInactive == "User Groups" || isInactive == "User Management" || isInactive == "User Roles") ? "sidebar-element-active" : ""}}'
							href='{{!checkAccess(USER_ROLE_CONST.GROUP_CONFIG) ? "#/userManagement" : getLocation()}}'
							data-name="Admin">
								<div class="sidebar-element-icon nav-subtext">
									<div>
										<img class="img-width" alt="User management"
											src="../styles/images/projectImages/management_icon.png">
									</div>
								</div> <span class="menuTextColor">Users</span>
						</a></li>

					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>
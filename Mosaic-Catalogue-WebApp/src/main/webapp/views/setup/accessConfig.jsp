<div id="top" class="monitor-page-css" data-ng-controller="accessConfigController">

	<section class="table-dynamic">
		<!-- <div class="panel-heading">
			<strong>User Roles</strong>
		</div> -->
		<div class="table-filters padTable">
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span> Showing {{filteredStores.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + currentPageStores.length}} of {{filteredStores.length}}
						entries </span>
				</div>
				<div class="col-sm-6 col-xs-6 btntop">
					<button type="button" id="configureNewGroup"
						class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
						data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CONFIG_NEW_ROLE"
						data-ng-click="openConfigureRolePopUp();">Configure new
						role</button>
				</div>
			</div>
		</div>
		<div class="modal-body footerMarginCss">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th id="selectds"><div id="thdsname"></div>
							<div class="th">
								# <span class="fa fa-angle-up"
									data-ng-click=" order('unqRoleMasterId') "
									data-ng-class="{active: row == 'unqRoleMasterId'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-unqRoleMasterId') "
									data-ng-class="{active: row == '-unqRoleMasterId'}"></span>
							</div></th>
						<th id="appname"><div></div>
							<div class="th">
								Solution <span class="fa fa-angle-up"
									data-ng-click=" order('appName') "
									data-ng-class="{active: row == 'appName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-appName') "
									data-ng-class="{active: row == '-appName'}"></span>
							</div></th>
						<th id="dsgroupname"><div></div>
							<div class="th">
								Role <span class="fa fa-angle-up"
									data-ng-click=" order('roleName') "
									data-ng-class="{active: row == 'roleName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-roleName') "
									data-ng-class="{active: row == '-roleName'}"></span>
							</div></th>
						<th><div class="th">
								Description <span class="fa fa-angle-up"
									data-ng-click="order('roleDescription')"
									data-ng-class="{active: row == 'roleDescription'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click="order('-roleDescription')"
									data-ng-class="{active: row == '-roleDescription'}"></span>
							</div></th>
						<th><div class="th">
								Created by<span class="fa fa-angle-up"
									data-ng-click="order('createdBy')"
									data-ng-class="{active: row == 'createdBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-createdBy')"
									data-ng-class="{active: row == '-createdBy'}"></span>
							</div></th>
						<th><div class="th">
								Created on<span class="fa fa-angle-up"
									data-ng-click="order('createdTs')"
									data-ng-class="{active: row == 'createdTs'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-createdTs')"
									data-ng-class="{active: row == '-createdTs'}"></span>
							</div></th>
						<th><div class="th">
								Updated by<span class="fa fa-angle-up"
									data-ng-click="order('updatedBy')"
									data-ng-class="{active: row == 'updatedBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-updatedBy')"
									data-ng-class="{active: row == '-updatedBy'}"></span>
							</div></th>
						<th><div class="th">
								Update on<span class="fa fa-angle-up"
									data-ng-click="order('updatedTs')"
									data-ng-class="{active: row == 'updatedTs'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-updatedTs')"
									data-ng-class="{active: row == '-updatedTs'}"></span>
							</div></th>
						<th>
							<div class="th" id="groupAction">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" id="selectdsclick" data-ng-repeat="access in currentPageStores">
						<td class="maxLengthProp">{{access.unqRoleMasterId}}</td>
						<td>{{access.appName}}</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_ROLE">
								<a href="javascript:;" class="anchorColor" data-ng-click="editAccess(access)"
									data-tooltip='{{access.roleName}}'>{{access.roleName}}</a>
							</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_ROLE">
								<a href="javascript:;" class="anchorColor" data-tooltip='{{access.roleName}}'>{{access.roleName}}</a>
							</td>
						<td>{{access.roleDescription}}</td>
						<td>{{access.createdBy}}</td>
						<td>{{access.createdTs == 'null' || access.createdTs == null ? "" : access.createdTs | date:'medium' }}</td>
						<td>{{access.updatedBy}}</td>
						<td>{{access.updatedTs == 'null' || access.updatedTs == null ? "" : access.updatedTs | date:'medium' }}</td>
						<td>
							<ul class="nav-right ulMargin list-unstyled">
								<!-- <li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_ROLE">
									<a href="javascript:;" class="actionMenu"
									ng-click="editAccess(access)"><i class="fa fa-pencil"
										tooltip-placement="top" tooltip="Edit role"></i></a>
								</li> -->
								<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DELETE_ROLE">
									<a href="javascript:;" class="actionMenu"
									ng-click="deleteAccess(access);"><i class="fa fa-trash"
										tooltip-placement="top" tooltip="Delete role"></i></a>
								</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange(numPerPage)">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
</div>

<style>
.switch {
    position: relative;
    display: inline-block;
    width: 35px;
    height: 20px;
}

.switch input {
    display: none;
}

.slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 1px;
    right: 0;
    bottom: 0;
    background-color: #d9534f;
    -webkit-transition: .4s;
    transition: .4s;
}

.slider:before {
    position: absolute;
    content: "";
    height: 15px;
    width: 9px;
    left: 2px;
    bottom: 3px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
}

input:checked+.slider {
    background-color: #5cb85c;
}

input:focus+.slider {
    box-shadow: 0 0 1px #2196F3;
}

input:checked+.slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
    border-radius: 4px;
    width: 39px;
}

.slider.round:before {
    border-radius: 5px;
}

.model-add-field-btn {
    float: right;
    margin-top: 10px;
    text-align: center;
}

.model-remove-field-btn {
    float: right;
}
</style>
<div class="monitor-page-css" data-ng-controller="customComponentRCodeCtrl" ng-init="getAllTypeOfModels()">
    <div class="col-md-12 noteCss" role="alert">
        <span ng-bind-html='explanation.customHeader | unsafe'></span>
    </div>
    <div class="col-sm-12"></div>
    <section class="table-dynamic">
        <!-- <div class="panel-heading">
            <strong>Custom Component</strong>
        </div> -->

        <div class="table-filters padTable">
            <div class="row">
                <div class="col-sm-3 col-xs-6 padding-right-none">
                    <form>
                        <input type="text" placeholder="Filter..." class="form-control"
                            data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form>
                </div>
                <div class="col-sm-3 col-xs-6 filter-result-info entries">
                    <span> Showing {{filteredStores.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + currentPageStores.length}} of {{filteredStores.length}}
                        entries  </span>
                </div>
                <div class="col-sm-6 col-xs-6 btntop" ng-hide="projectAccessType == 'reviewer'">
                    <button type="button" class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
                        data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CREATE_CUSTOM_COMPONENT"
                        ng-click="open(undefined)">Register a New Model</button>

                </div>
            </div>
        </div>
        <div class="modal-body">
            <table
                class="table table-bordered table-striped table-responsive table-repo marginSearch">
                <thead>
                    <tr>
                        <th>
                            <div class="th">
                                # <span class="fa fa-angle-up"
                                    data-ng-click=" order('modelId') "
                                    data-ng-class="{active: row == 'modelId'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('-modelId') "
                                    data-ng-class="{active: row == '-modelId'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th ">
                               Name <span class="fa fa-angle-up"
                                    data-ng-click=" order('modelName') "
                                    data-ng-class="{active: row == 'modelName'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('-modelName') "
                                    data-ng-class="{active: row == '-modelName'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th ">
                                Type <span class="fa fa-angle-up"
                                    data-ng-click=" order('modelType') "
                                    data-ng-class="{active: row == 'modelType'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('-modelType') "
                                    data-ng-class="{active: row == '-modelType'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th ">
                                Algorithm <span class="fa fa-angle-up"
                                    data-ng-click=" order('modelId') "
                                    data-ng-class="{active: row == 'modelId'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('modelId') "
                                    data-ng-class="{active: row == 'modelId'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th">
                                Deploy<span class="fa fa-angle-up"
                                    data-ng-click=" order('modelId') "
                                    data-ng-class="{active: row == 'modelId'}"></span> <span
                                    class="fa fa-angle-down" data-ng-click=" order('-modelId') "
                                    data-ng-class="{active: row == '-modelId'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th">
                                Owner Name <span class="fa fa-angle-up"
                                    data-ng-click=" order('createdByName') "
                                    data-ng-class="{active: row == 'createdByName'}"></span> <span
                                    class="fa fa-angle-down" data-ng-click=" order('-createdByName') "
                                    data-ng-class="{active: row == '-createdByName'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th">
                                Created on <span class="fa fa-angle-up"
                                    data-ng-click=" order('createdDate') "
                                    data-ng-class="{active: row == 'createdDate'}"></span>
                                <span class="fa fa-angle-down"
                                    data-ng-click=" order('-createdDate') "
                                    data-ng-class="{active: row == '-createdDate'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th">
                                Updated on <span class="fa fa-angle-up"
                                    data-ng-click=" order('modifiedDate') "
                                    data-ng-class="{active: row == 'modifiedDate'}"></span>
                                <span class="fa fa-angle-down"
                                    data-ng-click=" order('-modifiedDate') "
                                    data-ng-class="{active: row == '-modifiedDate'}"></span>
                            </div>
                        </th>

                        <th>
                            <div class="th">Action</div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="store in currentPageStores">
                        <td>{{store.modelId}}</td>
                        <td
                            data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_CUSTOM_COMPONENT"
                            data-ng-click="open(store)"><a href="javascript:;" class="label-link anchorColor">{{store.modelName}}</a></td>
                        <td
                            data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_CUSTOM_COMPONENT"><a
                            class="label-link anchorColor">{{store.modelName}}</a></td>
                        <td>{{store.modelType}}</td>
                        <td>{{(store.data===undefined || store.data===null || store.data.displayName===undefined) ?  'NA' : store.data.displayName}}</td>
                        <td><div ng-if="store.modelType=='SparkMLlib'"><label class="switch toggle-btn"
                                        style="top: 0px !important;"> <input type="checkbox"
                                        ng-checked="toggleButtonSwitch(store)" ng-change="createApiKey(store, $index)"
                                        ng-model="tglBtn" ng-dis>
                                        <div class="slider round"></div>
                                    </label>
                             </div>
                        </td>
                        <td>{{store.createdByName}}</td>
                        <td>{{store.createdDate | date : 'medium'}}</td>
                        <td>{{store.modifiedDate | date : 'medium'}}</td>
                        <td>
                            <ul class="nav-right ulMargin list-unstyled">
                                <!-- <li
                                    data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_CUSTOM_COMPONENT">
                                    <a href="javascript:;" class="actionMenu"
                                    ng-click="open(store)"><i class="fa fa-pencil"
                                        tooltip-placement="top" tooltip="Edit Custom Component"></i></a>
                                </li> -->
                                <li
                                    data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.VIEW_CUSTOM_COMPONENT">
                                    <a href="javascript:;" class="actionMenu"
                                    ng-click="deleteRModel(store)"><i class="fa fa-trash"
                                        tooltip-placement="top" tooltip="Delete Custom Component"></i></a>
                               </li>
                               <li
                                    data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.VIEW_CUSTOM_COMPONENT">
                                    <a href="javascript:;" class="actionMenu"
                                    ng-click="viewModel(store)"><i class="fa fa-eye"
                                        tooltip-placement="top" tooltip="View Model"></i></a>
                               </li>  
                               <li
                                    data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.VIEW_CUSTOM_COMPONENT">
                                    <a href="javascript:;" class="actionMenu" 
                                    ng-click="redirect(store);"><i class="fa fa-newspaper-o"
                                        tooltip-placement="top" tooltip="Summary"></i></a>
                               </li>
                                
                                <!-- <li
                                    data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.VIEW_CUSTOM_COMPONENT">
                                    <a href="javascript:;" class="actionMenu"
                                    ng-click="viewModalStatistics()"><i class="fa fa-bar-chart"
                                        tooltip-placement="top" tooltip="View Model"></i></a>
                                </li> -->
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            <footer class="table-footer">
                <div class="row">
                    <div class="col-md-6 page-num-info">
                        <span> Show <select data-ng-model="numPerPage"
                            data-ng-options="num for num in numPerPageOpt"
                            data-ng-change="onNumPerPageChange()">
                        </select> entries per page
                        </span>
                    </div>
                    <div class="col-md-6 text-right pagination-container zindex">
                        <pagination class="pagination-sm" ng-model="currentPage"
                            total-items="filteredStores.length" max-size="4"
                            ng-change="select(currentPage)" items-per-page="numPerPage"
                            rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
                            boundary-links="true"></pagination>
                    </div>
                </div>
            </footer>
        </div>
    </section>
</div>

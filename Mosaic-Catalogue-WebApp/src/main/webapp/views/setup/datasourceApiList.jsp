<style>

</style>
<div class="monitor-page-css" data-ng-controller="dataSourceApiCtrl" ng-init="getDataSourceWithApiList()">
    <div class="col-md-12 noteCss" role="alert">
        <span ng-bind-html='explanation.customHeader | unsafe'></span>
    </div>
    <div class="col-sm-12"></div>
    <section class="table-dynamic">
        <!-- <div class="panel-heading">
            <strong>Custom Component</strong>
        </div> -->

        <div class="table-filters padTable">
            <div class="row">
                <div class="col-sm-3 col-xs-6 padding-right-none">
                    <form>
                        <input type="text" placeholder="Filter..." class="form-control"
                            data-ng-model="searchKeywords" data-ng-keyup="search()">
                    </form>
                </div>
                <div class="col-sm-3 col-xs-6 filter-result-info entries">
                    <span> Showing {{filteredStores.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + currentPageStores.length}} of {{filteredStores.length}}
                        entries  </span>
                </div>
                <div class="col-sm-6 col-xs-6 btntop" >
                    <button type="button" class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
                        data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CNFG_NW_AN"
                        ng-click="open(undefined)">Config a New Datasource API</button>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <table
                class="table table-bordered table-striped table-responsive table-repo marginSearch">
                <thead>
                    <tr>
                        <th>
                            <div class="th">
                                # <span class="fa fa-angle-up"
                                    data-ng-click=" order('id') "
                                    data-ng-class="{active: row == 'id'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('-id') "
                                    data-ng-class="{active: row == '-id'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th ">
                               DS Name <span class="fa fa-angle-up"
                                    data-ng-click=" order('dataSourceName') "
                                    data-ng-class="{active: row == 'dataSourceName'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('-dataSourceName') "
                                    data-ng-class="{active: row == '-dataSourceName'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th ">
                                Created By <span class="fa fa-angle-up"
                                    data-ng-click=" order('createdBy') "
                                    data-ng-class="{active: row == 'createdBy'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('-createdBy') "
                                    data-ng-class="{active: row == '-createdBy'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th ">
                                API Key <span class="fa fa-angle-up"
                                    data-ng-click=" order('apiKey') "
                                    data-ng-class="{active: row == 'apiKey'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('apiKey') "
                                    data-ng-class="{active: row == 'apiKey'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th ">
                                API Status <span class="fa fa-angle-up"
                                    data-ng-click=" order('status') "
                                    data-ng-class="{active: row == 'status'}"></span> <span
                                    class="fa fa-angle-down"
                                    data-ng-click=" order('status') "
                                    data-ng-class="{active: row == 'status'}"></span>
                            </div>
                        </th>
                        <th>
                            <div class="th">
                                Created on <span class="fa fa-angle-up"
                                    data-ng-click=" order('createdDate') "
                                    data-ng-class="{active: row == 'createdDate'}"></span>
                                <span class="fa fa-angle-down"
                                    data-ng-click=" order('-createdDate') "
                                    data-ng-class="{active: row == '-createdDate'}"></span>
                            </div>
                        </th>
                       <!--  <th>
                            <div class="th">
                                Updated on <span class="fa fa-angle-up"
                                    data-ng-click=" order('modifiedDate') "
                                    data-ng-class="{active: row == 'modifiedDate'}"></span>
                                <span class="fa fa-angle-down"
                                    data-ng-click=" order('-modifiedDate') "
                                    data-ng-class="{active: row == '-modifiedDate'}"></span>
                            </div>
                        </th> -->

                        <!-- <th>
                            <div class="th">Action</div>
                        </th> -->
                    </tr>
                </thead>
                <tbody>
                    <tr data-ng-repeat="store in currentPageStores">
                        <td>{{$index+1}}</td>
                        <td
                            data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_ANLS_"
                            data-ng-click="open(store)"><a href="javascript:;" class="label-link anchorColor">{{store.dataSourceName}}</a></td>
                        <td>{{store.createdBy}}</td>
                        <td>{{store.apiKey}}</td>
                        <td
                            data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_ANLS_"><a
                            class="label-link anchorColor">{{store.dataSourceName}}</a></td>
                        <td><div ng-if="store!=undefined || store!=null"><label class="switch toggle-btn"
                                        style="top: 0px !important;"> <input type="checkbox"
                                        ng-checked="store.status=='1'" ng-change="createApiKey(store, $index)"
                                        ng-model="tglBtn" ng-dis>
                                        <div class="slider round"></div>
                                    </label>
                             </div>
                        </td>
                        <td>{{store.createdDate | date : 'medium'}}</td>
                        <!-- <td>{{store.modifiedDate | date : 'medium'}}</td> -->
                        <!-- <td>
                            <ul class="nav-right ulMargin list-unstyled">
                                <li
                                    data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.VIEW_CUSTOM_COMPONENT">
                                    <a href="javascript:;" class="actionMenu"
                                    ng-click="deleteRModel(store)"><i class="fa fa-trash"
                                        tooltip-placement="top" tooltip="Delete Datasource Api"></i></a>
                                </li>
                                <li
                                    data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.VIEW_CUSTOM_COMPONENT">
                                    <a href="javascript:;" class="actionMenu"
                                    ng-click="viewModel(store)"><i class="fa fa-eye"
                                        tooltip-placement="top" tooltip="View Datasource Api"></i></a>
                                </li>
                            </ul>
                        </td> -->
                    </tr>
                </tbody>
            </table>
            <footer class="table-footer">
                <div class="row">
                    <div class="col-md-6 page-num-info">
                        <span> Show <select data-ng-model="numPerPage"
                            data-ng-options="num for num in numPerPageOpt"
                            data-ng-change="onNumPerPageChange()">
                        </select> entries per page
                        </span>
                    </div>
                    <div class="col-md-6 text-right pagination-container zindex">
                        <pagination class="pagination-sm" ng-model="currentPage"
                            total-items="filteredStores.length" max-size="4"
                            ng-change="select(currentPage)" items-per-page="numPerPage"
                            rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
                            boundary-links="true"></pagination>
                    </div>
                </div>
            </footer>
        </div>
    </section>
</div>

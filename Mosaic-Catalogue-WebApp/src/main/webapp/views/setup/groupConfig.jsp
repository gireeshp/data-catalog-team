<div id="top" class="monitor-page-css" data-ng-controller="groupConfigController">
	<section class="table-dynamic">
		<!-- <div class="panel-heading">
			<strong>User Groups</strong>
		</div> -->
		<div class="table-filters padTable">
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span> Showing {{filteredStores.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + currentPageStores.length}} of {{filteredStores.length}}
						entries </span>
				</div>
				<div class="col-sm-6 col-xs-6 btntop">
					<button type="button" id="configureNewGroup"
						class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
						data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CONFG_NEW_GROUP"
						data-ng-click="openConfigureGroupPopUp();">Configure new
						group</button>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th id="selectds"><div id="thdsname"></div>
							<div class="th">
								# <span class="fa fa-angle-up"
									data-ng-click=" order('groupId') "
									data-ng-class="{active: row == 'groupId'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-groupId') "
									data-ng-class="{active: row == '-groupId'}"></span>
							</div></th>
						<th id="appname"><div></div>
							<div class="th">
								Solution <span class="fa fa-angle-up"
									data-ng-click=" order('appName') "
									data-ng-class="{active: row == 'appName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-appName') "
									data-ng-class="{active: row == '-appName'}"></span>
							</div></th>
						<th id="dsgroupname"><div></div>
							<div class="th">
								Group <span class="fa fa-angle-up"
									data-ng-click=" order('groupName') "
									data-ng-class="{active: row == 'groupName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-groupName') "
									data-ng-class="{active: row == '-groupName'}"></span>
							</div></th>
						<th><div class="th">
								Created by<span class="fa fa-angle-up"
									data-ng-click="order('insertedBy')"
									data-ng-class="{active: row == 'insertedBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-insertedBy')"
									data-ng-class="{active: row == '-insertedBy'}"></span>
							</div></th>
						<th><div class="th">
								Created on<span class="fa fa-angle-up"
									data-ng-click="order('insertTs')"
									data-ng-class="{active: row == 'insertTs'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-insertTs')"
									data-ng-class="{active: row == '-insertTs'}"></span>
							</div></th>
						<th><div class="th">
								Updated by<span class="fa fa-angle-up"
									data-ng-click="order('updatedBy')"
									data-ng-class="{active: row == 'updatedBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-updatedBy')"
									data-ng-class="{active: row == '-updatedBy'}"></span>
							</div></th>
						<th><div class="th">
								Update on<span class="fa fa-angle-up"
									data-ng-click="order('updateTs')"
									data-ng-class="{active: row == 'updateTs'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-updateTs')"
									data-ng-class="{active: row == '-updateTs'}"></span>
							</div></th>
						<th>
							<div class="th" id="groupAction">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" id="selectdsclick"
						data-ng-repeat="group in currentPageStores track by $index">
						<td>{{group.groupId}}</td>
						<td>{{group.appName}}</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_GROUP">
							<a href="javascript:;" class="anchorColor" data-ng-click="editGroup(group)"
							data-tooltip='{{group.groupName}}'>{{group.groupName}}</a>
						</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_GROUP">
							<a href="javascript:;" class="anchorColor" data-tooltip='{{group.groupName}}'>{{group.groupName}}</a>
						</td>
						<td>{{group.insertedBy}}</td>
						<td>{{group.insertTs == 'null' || group.insertTs == null ? "" : group.insertTs | date:'medium'}}</td>
						<td>{{group.updatedBy}}</td>
						<td>{{group.updateTs == 'null' || group.updateTs == null ? "" : group.updateTs | date:'medium'}}</td>
						<td>
							<ul class="nav-right ulMargin list-unstyled">
								<!-- <li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_GROUP">
									<a href="javascript:;" class="actionMenu"
									ng-click="editGroup(group)"><i class="fa fa-pencil"
										tooltip-placement="top" tooltip="Edit group"></i></a>
								</li> -->
								<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DELETE_GROUP">
									<a href="javascript:;" class="actionMenu"
									ng-click="deleteGroup(group.groupId, group.appName);"><i
										class="fa fa-trash" tooltip-placement="top"
										tooltip="Delete group"></i></a>
								</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange(numPerPage)">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>


	<!-- SUBGROUP -->
	<section class="table-dynamic">
		<div class="panel-heading custFont"><span class="colorHeader margin-left-sub">Subgroups</span></div>
		<div class="table-filters padTable">
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords1" data-ng-keyup="search1()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span> Showing {{filteredStores1.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + currentPageStores1.length}} of {{filteredStores1.length}}
						entries </span>
				</div>
				<div class="col-sm-6 col-xs-6 btntop">
					<button type="button" id="configureNewSubgroup"
						class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
						data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CONFG_NEW_SUBGROUP"
						data-ng-click="openConfigureSubgroupPopUp();">Configure
						new subgroup</button>
				</div>
			</div>
		</div>
		<div class="modal-body footerMarginCss">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th id="selectds"><div id="thdsname"></div>
							<div class="th">
								# <span class="fa fa-angle-up"
									data-ng-click=" order1('subGroupId') "
									data-ng-class="{active: row == 'subGroupId'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order1('-subGroupId') "
									data-ng-class="{active: row == '-subGroupId'}"></span>
							</div></th>
						<th id="dsgroupname"><div></div>
							<div class="th">
								Subgroup name <span class="fa fa-angle-up"
									data-ng-click=" order1('subGroupName') "
									data-ng-class="{active: row == 'subGroupName'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order1('-subGroupName')"
									data-ng-class="{active: row == '-subGroupName'}"></span>
							</div></th>
						<th><div class="th">
								Group name<span class="fa fa-angle-up"
									data-ng-click=" order1('groupName') "
									data-ng-class="{active: row == 'groupName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order1('-groupName') "
									data-ng-class="{active: row == '-groupName'}"></span>
							</div></th>
						<th><div class="th">
								Queue name<span class="fa fa-angle-up"
									data-ng-click=" order('queueMasterID') "
									data-ng-class="{active: row == 'queueMasterID'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-queueMasterID') "
									data-ng-class="{active: row == '-queueMasterID'}"></span>
							</div></th>
						<th><div class="th">
								Created by<span class="fa fa-angle-up"
									data-ng-click="order1('insertedBy')"
									data-ng-class="{active: row == 'insertedBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order1('-insertedBy')"
									data-ng-class="{active: row == '-insertedBy'}"></span>
							</div></th>
						<th><div class="th">
								Created on<span class="fa fa-angle-up"
									data-ng-click="order1('insertTs')"
									data-ng-class="{active: row == 'insertTs'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order1('-insertTs')"
									data-ng-class="{active: row == '-insertTs'}"></span>
							</div></th>
						<th><div class="th">
								Updated by<span class="fa fa-angle-up"
									data-ng-click="order1('updatedBy')"
									data-ng-class="{active: row == 'updatedBy'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order1('-updatedBy')"
									data-ng-class="{active: row == '-updatedBy'}"></span>
							</div></th>
						<th><div class="th">
								Update on<span class="fa fa-angle-up"
									data-ng-click="order1('updateTs')"
									data-ng-class="{active: row == 'updateTs'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order1('-updateTs')"
									data-ng-class="{active: row == '-updateTs'}"></span>
							</div></th>
						<th>
							<div class="th" id="groupAction">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" id="selectdsclick"
						data-ng-repeat="subGroup in currentPageStores1 track by $index">
						<td>{{subGroup.subGroupId}}</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_SUBGROUP">
								<a href="javascript:;" class="anchorColor"
									data-ng-click="updateSubGroupAction(subGroup);"
									data-tooltip='{{subGroup.subGroupName}}'>{{subGroup.subGroupName}}</a>
							</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_SUBGROUP">
								<a href="javascript:;" class="anchorColor" data-tooltip='{{subGroup.subGroupName}}'>{{subGroup.subGroupName}}</a>
						</td>
						<td>{{subGroup.groupName}}</td>
						<td>{{subGroup.queueName}}</td>
						<td>{{subGroup.insertedBy}}</td>
						<td>{{subGroup.insertTs == 'null' || subGroup.insertTs == null ? "" : subGroup.insertTs | date:'medium'}}</td>
						<td>{{subGroup.updatedBy}}</td>
						<td>{{subGroup.updateTs == 'null' || subGroup.updateTs == null ? "" : subGroup.updateTs | date:'medium'}}</td>
						<td>
							<ul class="nav-right ulMargin list-unstyled">
								<!-- <li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_SUBGROUP">
									<a href="javascript:;" class="actionMenu"
									data-ng-click="updateSubGroupAction(subGroup);"><i
										class="fa fa-pencil" tooltip-placement="top"
										tooltip="Edit sub-group"></i></a>
								</li> -->
								<li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DELETE_SUBGROUP">
									<a href="javascript:;" class="actionMenu"
									data-ng-click="deleteSubGroup(subGroup.subGroupId, subGroup.groupName);"><i
										class="fa fa-trash" tooltip-placement="top"
										tooltip="Delete sub-group"></i></a>
								</li>
							</ul>
						</td>
					</tr>

				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage1"
							data-ng-options="num for num in numPerPageOpt1"
							data-ng-change="onNumPerPageChange1(numPerPage1)">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage1"
							total-items="filteredStores1.length" max-size="4"
							ng-change="select1(currentPage1)" items-per-page="numPerPage1"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
</div>

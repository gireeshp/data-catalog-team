<div class="setting-section" ng-controller="settingsTabCtlr"
	ng-init="editableInput='true'" ngg-if="hideSettingsTab();">
	<div class="col-md-12 subtop-menu"  ng-if="subTabs.length > 0">
		<div class="">
			<div id="tabs" class="tab-size">
				<tabset> <tab ng-click="changSubTab(tab)"
					ng-repeat="tab in subTabs" style="cursor : pointer;"
					heading="{{tab.title}}" actigve="tab.active" class="pad-zero">
				</tab> </tabset>
			</div>
		</div>
	</div>
	
	
	<!-- user list display  -->
	<!-- user Group display  -->
	<div ng-if="activeSubTab == 'User group'">
		<div ng-include="'views/setup/groupConfig.jsp'"></div>
	</div>
	
	<!-- User Role -->
	<div ng-if="activeSubTab == 'User role'">
		<div ng-include="'views/setup/accessConfig.jsp'"></div>
	</div>
	
	<!-- user User Management -->
	<div ng-if="activeSubTab == 'User management'">
		<div ng-include="'views/setup/userManagement.jsp'"></div>
	</div>
	
</div>


<div id="top" class="monitor-page-css" data-ng-controller="userManagementController">
	<section class="table-dynamic">
		<!-- <div class="panel-heading">
			<strong>Users</strong>
		</div> -->
		<div class="table-filters padTable">
			<div class="row">
				<div class="col-sm-3 col-xs-6 padding-right-none">
					<form>
						<input type="text" placeholder="Filter..." class="form-control"
							data-ng-model="searchKeywords" data-ng-keyup="search()">
					</form>
				</div>
				<div class="col-sm-3 col-xs-6 filter-result-info entries">
					<span> Showing {{filteredStores.length == 0 ? 0 : ((pageNum -1) * numPerPage) + 1}} - {{(numPerPage * (pageNum - 1)) + currentPageStores.length}} of {{filteredStores.length}}
						entries </span>
				</div>
				<div class="col-sm-6 col-xs-6 btntop">
					<button type="button" id="configureNewGroup"
						class="btn btn-danger btn-block-dataRepo repo-btn pull-right btnSize"
						data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.CONFIG_NEW_USER"
						data-ng-click="openConfigureUserPopUp();">Configure new
						user</button>
				</div>
			</div>
		</div>
		<div class="modal-body footerMarginCss">
			<table
				class="table table-bordered table-striped table-responsive table-repo marginSearch">
				<thead>
					<tr>
						<th id="selectds"><div id="thdsname"></div>
							<div class="th">
								# <span class="fa fa-angle-up"
									data-ng-click=" order('unqUserId') "
									data-ng-class="{active: row == 'unqUserId'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-unqUserId') "
									data-ng-class="{active: row == '-unqUserId'}"></span>
							</div></th>
						<th id="dsgroupname"><div></div>
							<div class="th">
								First name <span class="fa fa-angle-up"
									data-ng-click=" order('firstName') "
									data-ng-class="{active: row == 'firstName'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-firstName') "
									data-ng-class="{active: row == '-firstName'}"></span>
							</div></th>
						<th><div class="th">
								Last name <span class="fa fa-angle-up"
									data-ng-click="order('lastName')"
									data-ng-class="{active: row == 'lastName'}"></span> <span
									class="fa fa-angle-down" data-ng-click="order('-lastName')"
									data-ng-class="{active: row == '-lastName'}"></span>
							</div></th>
						<th><div class="th">
								Email<span class="fa fa-angle-up"
									data-ng-click=" order('emailId') "
									data-ng-class="{active: row == 'emailId'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-emailId') "
									data-ng-class="{active: row == '-emailId'}"></span>
							</div></th>
						<th><div class="th">
								Solutions<span class="fa fa-angle-up"
									data-ng-click=" order('solutionNameList') "
									data-ng-class="{active: row == 'solutionNameList'}"></span> <span
									class="fa fa-angle-down"
									data-ng-click=" order('-solutionNameList') "
									data-ng-class="{active: row == '-solutionNameList'}"></span>
							</div></th>
						<th><div class="th">
								User status<span class="fa fa-angle-up"
									data-ng-click=" order('status') "
									data-ng-class="{active: row == 'status'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-status') "
									data-ng-class="{active: row == '-status'}"></span>
							</div></th>
						<th><div class="th">
								Updated on<span class="fa fa-angle-up"
									data-ng-click=" order('updatedTs') "
									data-ng-class="{active: row == 'updatedTs'}"></span> <span
									class="fa fa-angle-down" data-ng-click=" order('-updatedTs') "
									data-ng-class="{active: row == '-updatedTs'}"></span>
							</div></th>
						<th>
							<div class="th" id="groupAction">Action</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="maxLengthProp" id="selectdsclick"
						data-ng-repeat="store in currentPageStores">
						<td>{{store.unqUserId}}</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_USER">
							<a href="javascript:;" data-ng-click="editUser(store)" class="anchorColor"
							data-tooltip='{{store.firstName}}'>{{store.firstName}}</a>
						</td>
						<td
							data-ng-if="user.ACTIONS_CODES | isRoleNotAssigned: USER_ROLE_CONST.EDIT_USER">
							<a href="javascript:;" class="anchorColor" data-tooltip='{{store.firstName}}'>{{store.firstName}}</a>
						</td>
						<td>{{store.lastName}}</td>
						<td>{{store.emailId}}</td>
						<td>{{store.solutionNameList}}</td>
						<td>{{store.status | titleCase}}</td>
						<td>{{store.updatedTs == 'null' || store.updatedTs == null ? "" : store.updatedTs | date:'medium'}}</td>
						<td>
							<ul class="nav-right ulMargin list-unstyled">
								<!-- <li
									data-ng-if="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.EDIT_USER">
									<a href="javascript:;" class="actionMenu"
									data-ng-click="editUser(store)"><i class="fa fa-pencil"
										tooltip-placement="top" tooltip="Edit user"></i></a>
								</li> -->
								<li
									data-ng-show="user.ACTIONS_CODES | isRoleAssigned: USER_ROLE_CONST.DELETE_USER">
									<a href="javascript:;" class="actionMenu"
									data-ng-click="deleteUser(store.unqUserId, store.firstName , store.lastName);"><i
										class="fa fa-trash" tooltip-placement="top"
										tooltip="Delete user"></i></a>
								</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
			<footer class="table-footer">
				<div class="row">
					<div class="col-md-6 page-num-info marginCss">
						<span> Show <select data-ng-model="numPerPage"
							data-ng-options="num for num in numPerPageOpt"
							data-ng-change="onNumPerPageChange(numPerPage)">
						</select> entries per page
						</span>
					</div>
					<div class="col-md-6 text-right pagination-container zindex">
						<pagination class="pagination-sm" ng-model="currentPage"
							total-items="filteredStores.length" max-size="4"
							ng-change="select(currentPage)" items-per-page="numPerPage"
							rotate="false" previous-text="&lsaquo;" next-text="&rsaquo;"
							boundary-links="true"></pagination>
					</div>
				</div>
			</footer>
		</div>
	</section>
</div>

<section class="col-md-12"
	data-ng-controller="inputParamController">
	<div class="col-md-12">
		<strong> Input parameters </strong>
		<div class="row border-line"></div>
		<!-- <div class="col-md-12 noteCss" role="alert" style="left: -6px;">
			<span ng-bind-html='explanation["inputParamHeader"] | unsafe'></span></div>  -->
		<div class="col-md-2 margin-btn">
			<button type="button" class="btn btn-primary btn-block repo-btn"
				ng-click="openModal()">Add input parameters</button>
		</div>
	</div>
	<div class="col-md-12" style="min-height: 112px;">
	
		<div class="col-md-12 height-info">&nbsp;</div>
		<div class="col-md-12">
			<label><strong>System generated input parameters</strong></label><br>
			<div class="col-md-3 info-header">
				gp_currentRunDateTime
			</div>
			<div class="col-md-9">
				This is an auto generated parameters by system which will have live
				timestamp.
			</div>
			<div class="col-md-3 info-header">
				gp_lastRunDateTime
			</div>
			<div class="col-md-9">
				This is an auto generated parameters by system which will have timestamp of last run job failed or succeeded
			</div>
			<div class="col-md-3 info-header">
				gp_lastSuccessfullyRunDateTime
			</div>
			<div class="col-md-9">
				This is an auto generated parameters by system which will have timestamp of last succeeded job.
			</div>
		</div>
		<div class="col-md-12" data-ng-init='date = getTodaysDate();'>
			<div class="callout callout-info"
				ng-repeat="values in datasource.dsLevelParams">
				<p>Name : {{values.paramName}} | Type : {{values.paramType}} |
					DefaultValue :{{values.paramName =='gp_currentRunDateTime'?(date | date:'dd/MM/yyyy HH:mm:ss'): values.defaultVal}}
					</p>
			</div>
		</div>
	</div>
</section>


<div class="page page-profile" data-ng-controller="userProfileCtrl">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-profile">
				<div class="panel-heading bg-primary clearfix">
					<a href="" class="pull-left profile"> <img alt=""
						src="../../styles/images/g1.jpg" class="img-circle img80_80">

					</a>
					<h3>{{loginUserId}}</h3>
					<!-- <p>Project Manager</p -->
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="media">

							<ul class="list-unstyled list-info col-md-12">
								<li class="row"><span
									class="icon glyphicon glyphicon-user col-md-1"></span> <span
									class="col-md-4"><b>User name</b></span> <span class="col-md-6">{{loginUserId}}</span>
								</li>
								<li class="row"><span
									class="icon glyphicon glyphicon-envelope col-md-1"></span> <span
									class="col-md-4"><b>Email</b></span> <span class="col-md-6">{{email}}</span>
								</li>
								<li class="row"><span
									class="icon glyphicon glyphicon-home col-md-1"></span> <span
									class="col-md-4"><b>Logged in group</b></span> <span
									class="col-md-6"><span
										data-ng-repeat="group in user.GROUP_DETAILS track by $index"
										data-ng-if="group.isSelected">{{group.groupName}}</span></span></li>
								<li class="row"><span
									class="icon glyphicon glyphicon-home col-md-1"></span> <span
									class="col-md-4"><b>All groups</b></span> <span
									class="col-md-6"><span
										data-ng-repeat="group in user.GROUP_DETAILS track by $index"><span
											data-ng-if="$index!=0">, </span>{{group.groupName}}</span></span></li>

								<li class="row"><span
									class="icon glyphicon glyphicon-refresh col-md-1"></span> <span
									class="col-md-4"><b>Created on</b></span> <span
									class="col-md-6">{{applicationUser.createdTs | date}}</span></li>

								<li class="row"><span
									class="icon glyphicon glyphicon-lock col-md-1"></span> <span
									class="col-md-4"><b>Password expires in</b></span> <span
									class="col-md-6"> {{applicationUser.passwordexpirydays}}
								</span></li>
							</ul>
							<div class="pull-right">
								<a href="javascript:;" ng-click="resetPasswordForUser()"><font
									color="#1C7EBB">Reset password</font></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
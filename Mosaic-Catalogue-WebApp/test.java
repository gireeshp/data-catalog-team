package com.augmentiq.maxiq.interfaces.source.scanner.ds.scanner;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.augmentiq.datasource.connection.beans.ConnectionConfig;
import com.augmentiq.datasource.connection.beans.ConnectionSources;
import com.augmentiq.datasource.connection.beans.ConnectionSubSources;
import com.augmentiq.maxiq.implementation.source.scanner.ds.scanner.RDBMSScanner;
import com.google.gson.Gson;

public class test {
	

	
	
	public static void main(String[] args) {
		saveConnectionData();
		/*Base64 b= new Base64();
		String password1 ="abc125";
        String password = "abc125";
        byte[]  b1=b.encode(password.getBytes());
        System.out.println();
        System.out.println(new String(b.decode(b1).toString()));*/
        /*BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(password);
        System.out.println(passwordEncoder.matches(password1, encodedPassword));
        System.out.println(encodedPassword);*/
  
       
        
		/*test test = new test();
		//test.getAllConnectionSources();
		saveConnectionData();*/
		//getConnectionData();
		/*try {	
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
	/*	ApplicationContext context = new ClassPathXmlApplicationContext("resource/spring.xml");
		Scanner scanner = (Scanner)context.getBean("rdbmsScanner");
		RDBMSScanner sc = new RDBMSScanner();
		
			try {
				scanner.extractInfo();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				
				sc.getFieldMapping();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		*/
		
	}
		
		
		
		
		//controller calls
		
		//test test = new test();
		//ConnectionSources connectionSource = test.getAllConnectionSourceAndItsSubSources(1L);
		//System.out.println(new Gson().toJson(connectionSource));
		
		
		
	
	
	public static void saveConnectionData(){
		
		ConnectionSources rdbms = new ConnectionSources();
		ConnectionSources ftp = new ConnectionSources();
		//creating sub source
		ConnectionSubSources mysql = new ConnectionSubSources();
		ConnectionSubSources oracle = new ConnectionSubSources();
		ConnectionSubSources postgres = new ConnectionSubSources();
		
		//creating connections
		ConnectionConfig mysqlcon1 = new ConnectionConfig();
		mysqlcon1.setIpAddress("127.0.0.1");
		mysqlcon1.setConncetionName("localhost connection");
		mysqlcon1.setDbUserName("root");
		mysqlcon1.setDbPassword("root");
		mysqlcon1.setPort(3306);
		mysqlcon1.setCreatedDate(new Date());
		
		//creating connections
				ConnectionConfig mysqlcon2 = new ConnectionConfig();
				mysqlcon2.setIpAddress("111.1.1.1");
				mysqlcon2.setConncetionName("dummy connection");
				mysqlcon2.setDbUserName("root1");
				mysqlcon2.setDbPassword("root1");
				mysqlcon2.setPort(3306);
				mysqlcon2.setCreatedDate(new Date());
		//
		//creating connections
				ConnectionConfig oraclecon1 = new ConnectionConfig();
				oraclecon1.setIpAddress("111.221.91.167");
				oraclecon1.setDbUserName("mercer");
				oraclecon1.setSid("test1");
				oraclecon1.setConncetionName("oracle mercer connection");
				oraclecon1.setDbPassword("mercer");
				oraclecon1.setPort(1521);
				oraclecon1.setCreatedDate(new Date());
		
		ftp.setSourceImagePath("../../styles/images/publish_catalog/publish-home/ftp-publish.png");
		ftp.setConnectionType("FTP");
		rdbms.setConnectionType("RDBMS");
		rdbms.setSourceImagePath("../../styles/images/publish_catalog/publish-home/data-source-publish.png");
		oracle.setSubConnectionType("ORACLE");
		oracle.setSourceImagePath("../../styles/images/publish_catalog/rdms/oracle-publish.png");
		mysql.setSubConnectionType("MYSQL");
		mysql.setSourceImagePath("../../styles/images/publish_catalog/rdms/mysql-publish.png");
		postgres.setSubConnectionType("POSTGRES");
		postgres.setSourceImagePath("../../styles/images/publish_catalog/rdms/pl-publish.png");
		mysql.getConnectionConfig().add(mysqlcon1);
		mysql.getConnectionConfig().add(mysqlcon2);
		oracle.getConnectionConfig().add(oraclecon1);
		rdbms.getConnectionSubSources().add(mysql);
		rdbms.getConnectionSubSources().add(oracle);
		rdbms.getConnectionSubSources().add(postgres);
		ApplicationContext context = new ClassPathXmlApplicationContext("resource/spring.xml");
		SessionFactory sessionFactory = (SessionFactory)context.getBean("sessionFactory");
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		session.save(rdbms);
		session.save(ftp);
		/*ConnectionSources cn = (ConnectionSources)session.get(ConnectionSources.class, 1L);
		System.out.println(new Gson().toJson(cn));*/
		/*ConnectionSubSources subsources = (ConnectionSubSources)session.get(ConnectionSubSources.class, 1L);
		Hibernate.initialize(subsources.getConnectionConfig());
		subsources.getConnectionConfig().add(ccfg);
		session.update(subsources);*/
		//session.save(cs);
		
		session.getTransaction().commit();
		session.close();
		sessionFactory.close();
		
	}
	
public static void getConnectionData(){
		
		ConnectionSources cs = null;
		ApplicationContext context = new ClassPathXmlApplicationContext("resource/spring.xml");
		SessionFactory sessionFactory = (SessionFactory)context.getBean("sessionFactory");
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		cs =(ConnectionSources)session.get(ConnectionSources.class,1L);
		Hibernate.initialize(cs.getConnectionSubSources());
		System.out.println(new Gson().toJson(cs));
		//ConnectionSubSources cc= cs.getConnectionSubSources().get(0);
		session.getTransaction().commit();
		session.close();
		sessionFactory.close();
		
		
	}

//Controller Code Starts here


//This method is for fetching All Sources
public List<ConnectionSources> getAllConnectionSources(){
	
	List<ConnectionSources> connectionSourcesList = null;
	ApplicationContext context = new ClassPathXmlApplicationContext("resource/spring.xml");
	SessionFactory sessionFactory = (SessionFactory)context.getBean("sessionFactory");
	Session session = sessionFactory.openSession();
	session.getTransaction().begin();
	Criteria criteria = session.createCriteria(ConnectionSources.class);
	ProjectionList projectionList = Projections.projectionList();
	projectionList.add(Projections.property("sourceId"));
	projectionList.add(Projections.property("connectionType"));
	criteria.setProjection(projectionList);
	//Query query = session.createQuery("select connectionId,connectionType from ConnectionSources");
	//connectionSourcesList = query.list();
	connectionSourcesList = criteria.list();
	session.getTransaction().commit();
	session.close();
	sessionFactory.close();
	System.out.println(new Gson().toJson(connectionSourcesList));
	return connectionSourcesList;
	
}

//This method is for fetching all sub Sources 
public ConnectionSources getAllConnectionSourceAndItsSubSources(Long id){
	
	ConnectionSources connectionSources = null;
	ApplicationContext context = new ClassPathXmlApplicationContext("resource/spring.xml");
	SessionFactory sessionFactory = (SessionFactory)context.getBean("sessionFactory");
	Session session = sessionFactory.openSession();
	session.getTransaction().begin();
	connectionSources = (ConnectionSources)session.get(ConnectionSources.class, id);
	Hibernate.initialize(connectionSources.getConnectionSubSources());
	session.getTransaction().commit();
	session.close();
	sessionFactory.close();
	return connectionSources;
	
}


	
	
}

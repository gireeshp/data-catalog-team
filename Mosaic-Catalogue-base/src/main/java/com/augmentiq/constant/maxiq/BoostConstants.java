package com.augmentiq.constant.maxiq;

public class BoostConstants {
    public static final Long NIL = -1L;
    public static final String FLOW_DATASOURCE = "node_datasource";
    public static final char MEMORY_FOR_RUN = 'g';
    public static final String YES = "YES";
    public static final String NO = "NO";
    public static final String Level = "Level";
    public static final String BAAP_JOB_INSTANCE_ID = "baapJobInsId";
    public static final String PARAM_NAME = "paramName";
    public static final String VAL = "val";
    public static final String TYPE = "type";
    public static final String DS_ID = "dsId";
    public static final String LINE = "<br>";
    public static final String BOOST_ID_REPLACE = "<boostId>";
    public static final String STRING = "STRING";
    public static final String ACTIVE = "active";
    public static final String INACTIVE = "inactive";
    public static final String INDEX = "Index";
    public static final String FLOW = "Flow";
    public static final String NEW_BOOST_LEVEL_ID_REPLACE = "<newBoostLevelId>";
    public static final String OLD_BOOST_ID_REPLACE = "<oldboostid>";
    public static final String JOB_INSTANCE_ID_REPLACE = "<jobInstanceId>";
    public static final String ORDER_LEVEL_REPLACE = "<orderLevel>";
    public static final String DELETED_OREDR_LEVEL_REPLACE = "<deletedOrderLevel>";

    public class DOMAIN_CONSTANTS {
	public static final String ID = "id";
	public static final String ORDER_LEVEL = "orderLevel";
	public static final String STATUS = "status";
	public static final String IS_DEFAULT = "isDefault";
	public static final String BOOST_ID = "boostId";
	public static final String OBJECT_ID = "objectId";
	public static final String OBJECT_TYPE = "objectType";
    }

    public class Query {
	public static final String GET_BOOSTS = "select * from boost where status='" + ACTIVE + "' order by cast(orderLevel as unsigned)";
	public static final String GET_DYNAMIC_LEVELS = "select * from boost where status='" + ACTIVE + "' order by  cast(orderLevel as unsigned) desc limit 1";
	public static final String FIND_IF_BOOSTS_IS_IN_USE = "select CONCAT ('Data Source Name: ', dataSourceName) as jobInstanceId, 'a' as boostId from datasourceinstance where dataSourceId in (select objectId from boost_mapping where objectType ='DataSource' and boostId='" + BOOST_ID_REPLACE + "') " + "	union " + " select CONCAT ('Indxe Name: ', indexName) as jobInstanceId, 'a' as boostId from index_defination where id in (select objectId from boost_mapping where objectType ='" + INDEX
		+ "' and boostId='" + BOOST_ID_REPLACE + "') " + " union " + " select CONCAT ('Flow Name: ', appName) as jobInstanceId, 'a' as boostId from maxiq_apps where appId in (select objectId from boost_mapping where objectType ='" + FLOW + "' and boostId='" + BOOST_ID_REPLACE + "') ";

	public static final String GET_BOOST_CONF_FOR_SHELL_SCRIPT = "select * from boost where id = (select boostId from boost_mapping_reduce where jobInstanceId='<jobInstanceId>')";
	public static final String CASCADE_INCREMENT = "update boost set orderLevel=orderLevel+1 where orderLevel='" + ORDER_LEVEL_REPLACE + "' and status='" + ACTIVE + "'";
	public static final String UPDATE_BOOST_MAPPING = "update boost_mapping set boostid=" + NEW_BOOST_LEVEL_ID_REPLACE + " where boostid=" + OLD_BOOST_ID_REPLACE;
	public static final String SQL_NEXT_HIGHER_BOOST_SQL = "select * from boost where orderLevel > <deletedOrderLevel> and status='" + ACTIVE + "' order by cast(orderLevel as unsigned) limit 1";
	public static final String HIGHEST_BOOST_SQL = "select * from boost where status='" + ACTIVE + "' order by cast(orderLevel as unsigned) desc  limit 1";
	public static final String REDUCED_BOOST_LIST = "select * from boost where status='" + ACTIVE + "' and orderLevel <= <orderLevel> order by cast(orderLevel as unsigned) desc";
    }
}

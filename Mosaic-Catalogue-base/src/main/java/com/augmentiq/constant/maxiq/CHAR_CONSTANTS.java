package com.augmentiq.constant.maxiq;

public class CHAR_CONSTANTS {
    public static final String SPACE = " ";
    public static final String COMMA = ",";
    public static final String NO_SPACE = "";
    public static final String PIPE = "|";
    public static final String EQUALS = "=";
    public static final String SINGLE_QUOTE = "\'";
    public static final String DOUBLE_QUOTE = "\"";
    public static final String COLON = ":";
    public static final String UNDERSCORE = "_";
    public static final String UNDER_SCORE = UNDERSCORE;
    public static final String FORWARD_SLASH = "/";
    public static final String ASTERISK = "*";
    public static final String OPENING_BRACES = "(";
    public static final String CLOSING_BRACES = ")";
    public static final String QUESTION_MARK = "?";
    public static final String SEMI_COLON = ";";
    public static final String AMPERSAND = "&";
    public static final String DOLLER = "$";
    public static final String ATTHERATE = "@";
    public static final String NEW_LINE = "\n";
    public static final String HYPHEN = "-";
    public static final String BLANK = NO_SPACE;

    public static final String BACKSLASH_DOUBLE_QUOTE = "\\\"";
    public static final String OPENING_BRACKET = "[";
    public static final String CLOSING_BRACKET = "]";
    public static final String DOT = ".";
    public static final String OPENING_CURLY_BRACES = "{";
    public static final String CLOSING_CURLY_BRACES = "}";
    public static final String HIVE_COLUMN_NAME_PREFIX = "`";
    public static final String PERCENTAGE ="%";
}


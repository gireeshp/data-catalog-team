package com.augmentiq.constant.maxiq;

public class DATACONSTANTS {
    public static class DATA_REPOSITORY {
	public static final String REPO_NAME = "repoName";
	public static final String REPO_ID = "repoId";
    }

    public static class DATA_SOURCE {
	public static final String DATA_SOURCE_NAME = "dataSourceName";
	public static final String DATA_SOURCE_ID = "dataSourceId";
    }

    public static class TIME_SERIES_COMPONENTS {
	public static final String componentType = "componentType";
    }

    public static class SHARE_ACCESS_HISTORY_ACESS_ACTION {
	public static final String REVOKE = "Revoke";
	public static final String GRANT = "Grant";
	public static final String[] ASSET_TYPE = { "Project", "DataSource" };
    }
}

package com.augmentiq.constant.maxiq;

import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;

public class GENERAL_CONSTANTS {

    // Integer constants
    public static int ZERO = 0;

    public static String CONTAINS_HEADER_TRUE = "1";
    public static String CONTAINS_HEADER_FALSE = "0";

    public static String HIVE_STRING = "string";
    public static String HIVE_NUMERIC = "bigint";
    public static String HIVE_DATE = "date";
    public static String HIVE_BOOLEAN = "boolean";
    public static String HIVE_FLOAT = "float";
    public static String EXCEL_FILE_NAME = "excel1.txt";
    public static String BREAK_LINE = "\n";
    public static String GROUPNAME = "groupName";
    public static String ISSELECTED = "isSelected";
    public static String KEYS = "keys";
    public static String DEPENDENCIES = "dependencies";

    public static String HOST_NAME = "hostName";
    public static String H_BASE_ROW_KEY = "hbaseRowKey";

    public static String REPLACE = "Replace";
    public static String MERGE = "Merge";
    public static String APPEND = "Append";
    public static String DELETE = "Delete";
    public static String MESSAGE = "message";
    public static String STATUS = "status";
    public static String ERROR = "ERROR";
    public static String SUCCESS = "success";
    public static String NULL = "null";
    public static String EXCEPTION = "EXCEPTION";

    public static String INJECTION_TYPE_QUICK = "quick";
    public static String INJECTION_TYPE_TYPICAL = "typical";

    public static String ACCEPTED_RECORDS = "A";
    public static String REJECTED_RECORDS = "R";
    public static String IGNORED_RECORDS = "I";

    public static String HIVE_QUERY_IS_HBASE = "isHbase";
    public static String HIVE_QUERY = "query";
    public static String HIVE_QUERY_GROUP_ID = "groupId";

    public static String TEST_CONNECTION_STATUS = "status";
    public static String TEST_CONNECTION_SUCCESS = "success";
    public static String TEST_CONNECTION_FAILED = "failed";
    public static String TEST_CONNECTION_MESSAGE = "message";
    public static String TEST_CONNECTION_FIELD_DETAILS = "fieldsDetails";

    public static String KILL_JOB_USER_ID = "userId";
    public static String KILL_JOB_COMMENTS = "comments";
    public static String KILL_JOB_TASK_TYPE = "taskType";
    public static String KILL_JOB_JOBINSTID = "jobInstId";
    public static String JOB_NAME = "jobName";
    public static String PROJECTID = "projectId";
    public static final String TRUE = "true";
    public static final String FALSE = "false";

    public static String POST_FIX_FOR_DUPLICATE_FIELD_NAME = "_1";
    public static String FLOWS = "Flows";
    public static String ANALYSIS_APPS = "Analysis Apps";
    public static String APP = "APP";

    public static String ModelId = "modelId";
    public static String COMPONENTID = "componentId";

    // Datasource -Jobinstance History Start
    public static final String UPDATE_JOBINSTANCES = "UPDATEJOBINSTANCES";
    public static final String RESTORE_JOBINSTANCES = "RESTOREJOBINSTANCES";

    public static final String HISTORY_DATASOURCES = "historyDataSources";
    public static final String DELETED_DATASOURCES = "deletedDataSources";
    public static final String ACTIVE_DATASOURCES = "activeDataSources";

    public static final String INACTIVE = "Inactive";
    public static final String ACTIVE = "Active";
    public static final String DELETED = "Deleted";

    public static final String INSTANCES = "INSTANCES";
    public static final String DAYS = "DAYS";
    public static final String DEFAULT = "DEFAULT";

    public static final String JOBINSTANCE_SUCCESS_STATUS = "SUCCEEDED";
    // Datasource -Jobinstance History END

    // For Expression Evaluator
    public static String MONTH_1 = "MM";
    public static String MONTH_2 = "MONTH";
    public static String MONTH_3 = "MON";
    public static String YEAR_1 = "YY";
    public static String YEAR_2 = "YEAR";
    public static String YEAR_3 = "YYYY";
    public static String YEAR_4 = "YR";
    public static String DATE_1 = "DD";
    public static String HOUR_1 = "HH";
    public static String MINUTE_1 = "MI";
    public static String SECOND_1 = "SS";

    public static String QUERY_PARAM_NAME = "name";
    public static String QUERY_PARAM_TYPE = "type";
    public static String QUERY_PARAM_BAAPJOBINSID = "baapJobInsId";
    public static String QUERY_PARAM_APPID = "appId";
    public static String QUERY_PARAM_PARAMNAME = "paramName";
    public static String VALIDATE_MSG_SUCCESS = "success";
    public static String EXPRESSION = "expression";

    // salesforce connector constant
    public static String LOGINURL = "https://login.salesforce.com";
    public static String GRANTSERVICE = "/services/oauth2/token?grant_type=password";
    public static String SALESFORCE = "SALESFORCE";
    public static String USER_NAME = "USER_NAME";
    public static String PASSWORD_SECURITYTOKEN = "PASSWORD_SECURITYTOKEN";
    public static String CLIENT_ACCESS_ID = "CLIENT_ACCESS_ID";
    public static String CLIENT_SECRETE_ID = "CLIENT_SECRETE_ID";
    public static String ACCESS_TOKEN = "access_token";
    public static String INSTANCE_URL = "instance_url";
    public static String QUERY_PATH = "/services/data/v39.0/query/";
    public static String AUTHORIZATION = "Authorization";
    public static String BEARER = "Bearer ";
    public static String RECORDS = "records";
    public static String ATTRIBUTES = "attributes";
    public static String TYPE = "type";
    public static String SELECT = "SELECT ";
    public static String FROM = " FROM ";
    public static String OUT_FILE_LOCATION = "OUT_FILE_LOCATION";

    public static final String KEY_FILE_LOCATION = "KEY_FILE_LOCATION";
    public static final String SERVICE_ACCOUNT_EMAIL = "SERVICE_ACCOUNT_EMAIL";
    public static String DELIMETER = "DELIMETER";
    public static String KEY = "key";
    public static String BUCKETNAME = "bucketName";
    public static String FILEEXTENSION = "fileExtension";
    public static String STORAGEPATH = "storagePath";
    public static String DOWNLOADOBJECTTYPE = "downloadObjectType";
    public static String ACCESSKEYID = "accesskeyid";
    public static String SECRETKEY = "secretkey";

    public static String GOOGLEANALYTIC = "GoogleAnalytic";
    public static String AMAZONS3 = "AmazonS3";

    public static String TLSV1 = "TLSv1";
    public static String TLSV11 = "TLSv1.1";
    public static String TLSV12 = "TLSv1.2";
    public static String SSLV3 = "SSLv3";
    public static String HTTP = "http";
    public static String HTTPS = "https";
    public static String OK_STRING = "200";

    public static final String SAMPLE = "Sample";
    public static final String APP_INST = "APP_INST";
    public static final String APP_ = "App_";
    public static final String FIELD_MAPPING = "FIELD-MAPPING";
    public static final String STATUS_DATASOURCE_VERSION = "StatusDataSourceVersion";
    public static final String STATUS_FIELDMAPPING_VERSION = "StatusFieldMappingVersion";
    public static final String STATUS_FILETYPE_MATCH = "StatusFileTypeMatch";

    public static final String INPUT_PARAMETERS = "INPUT_PARAMETERS";
    public static final String DATASOURCE = "DataSource";
    public static final String YES = "YES";

    public static final String FLOW = "Flow";
    public static final String TXT = "txt";

    public static final String BACK_SLASH_R = "\r";

    public static String DROPBOX = "DROPBOX";
    public static String FOLDER = "Folder";
    public static String FILE = "file";
    public static String DATASOURCETYPE = "dataSourceType";
    public static String NAME = "name";
    public static String FILEPATH = "filePath";
    public static String LENGTH = "length";
    public static String FOLDERNAME = "folderName";

    // Support for Excel xls file
    public static String EXCEL_FILE_EXTENSION_XLSX = "xlsx";
    public static String EXCEL_FILE_EXTENSION_XLS = "xls";

    public static class JavaProfling {
	public static String TABLE_NAME = "tableName";
	public static String COLUMN_NAME = "columnName";
	public static String LIMIT = "limit";
	public static String SCALE = "scale";
	public static String GROUPID = "groupId";
	public static String JOB_NAME = "jobName";

	// Scale Type
	public static String CONTINUOUS = "continuous";
	public static String CATEGORICAL = "categorical";
	public static String CONSTANT = "constant";
	public static String TEXT = "text";
	public static String DEFAULT = "default";
	public static String ERROR = "error";
    }

    public static String FACEBOOKPAGE = "FACEBOOKPAGE";
    public static String PAGENAME = "PageName";
    public static String PAGEDATATYPE = "PageDataType";
    public static String CONNECTIONNAME = "connectionName";

    public static String HACKYLIPADDRESS = "IPAddress";
    public static String HACKYL = "HECKYL";
    public static String IPURL = "ipUrl";
    public static String COMPANYDETAILS = "companyDetails";
    public static String ASSET = "asset";
    public static String ENTITYTYPE = "entityType";
    public static String ENTITYCODE = "entityCode";
    public static String INFO = "Info";
    public static String FIELDS = "fields";
    public static String FEED = "Feed";
    public static String FEEDVALUE = "/feed";
    public static String FBINFOHEADER = "PageName|id|name|birthday|website|fan_count|likes|picture|affiliation|personal_interests|link";
    public static String FBINFOPARAMETER = "id,name,birthday,website,fan_count,likes,picture,affiliation,personal_interests,link";
    public static String FBFEEDHEADER = "PageName|Type|Id|Message|Story|Timestamp";
    public static String LIMIT = "limit";
    public static String HACKYLCOMPANYHEADER = "Categorys|Channel|ClusterId|Description|EntityCode|EntityType|ImageUrl|IsInTitle|LastFetch|LastFetchTicks|Link|Name|Sentiment|Source|SourceMapId|Title|TitleCnt|UniqueGuid";
    public static String HACKYLELEMENTDATA = "Categorys|Channel|ClusterId|Description|EntityCode|EntityType|ImageUrl|IsInTitle|LastFetch|LastFetchTicks|Link|Name|Sentiment|Source|SourceMapId|Title|TitleCnt|UniqueGuid";
    public static String NEWSITEMS = "NewsItems";
    public static String HACKYLTESTLINK = "/GetLatestTrendingNews?asset=1&entitytype=ISIN&entitycode=INE009A01021&lft=0&sortby=0";
    public static String HACKYLURLPART1 = "/GetLatestTrendingNews?asset=";
    public static String HACKYLURLPART2 = "&entitytype=";
    public static String HACKYLURLPART3 = "&lft=0&sortby=";
    public static String HACKYLURLPART4 = "&entitycode=";
    public static String HACKYLURLPART5 = "http://";
    public static String HACKYLURLPART6 = "/Find1Svc/API/JSON/News.svc";

    public static String COMMON = "COMMON";
    public static String TEXT_FEATURES = "textFeatures";

    public static class ExternalConnector {
	public static String PROBE42 = "PROBE42";
	public static String COMPANYDETAILS = "companyDetails";
	public static String COMPANYBASEDATA = "companyBaseData";
	public static String COMPANYFINANCIALS = "CompanyFinancials";
	public static String COMPANYCOMPLIANCE = "CompanyCompliance";
	public static String HOLDINGSUBSIDIARY = "HoldingSubsidiary";
	public static String INDUSTRYSEGMENTS = "IndustrySegments";
	public static String CINNUMBER = "CINNumber";
	public static String PROBEAPIKEY = "ApiKey";
	public static String PROBEINPUTURL = "https://api.probe42.in/probe_lite/companies/";
	public static String X_API_KEY = "x-api-key";
	public static String X_API_VERSION = "x-api-version";
	public static String PROBE_VERSION = "1.2";
	public static String ACCEPT_TYPE = "application/json";
	public static String ACCEPT = "Accept";
	public static String DATA = "data";
	public static String AUTHORIZEDSIGNATORIES = "authorized-signatories";
	public static String AUTHORIZED_SIGNATORIES = "authorized_signatories";
	public static String CHARGES = "charges";
	public static String COMPLIANCE = "compliance";
	public static String FINANCIALS = "financials";
	public static String AUDITORS = "auditors";
	public static String FINANCIALDATASTATUS = "financial-datastatus";
	public static String BIFRHISTORY = "bifr-history";
	public static String CDRHISTORY = "cdr-history";
	public static String DEFAULTERLIST = "defaulter-list";
	public static String LEGALHISTORY = "legal-history";
	public static String CREDITRATINGS = "credit-ratings";
	public static String HOLDINGCOMPANIES = "holding-companies";
	public static String SUBSIDIARYCOMPANIES = "subsidiary-companies";
	public static String INDUSTRYSEGMENT = "industry-segments";
	public static String PROBURL = "https://api.probe42.in/probe_lite/companies?limit=25";

	public static String COMPANYDETAILSHEADER = "authorized_capital|cin|efiling_status|incorporation_date|last_agm_date|last_filing_date|legal_name|paid_up_capital|sum_of_charges|registered_address|classification|status|next_cin";
	public static String AUTHORIZEDSIGNATORIESHEADER = "pan|din|name|designation|date_of_birth|age|date_of_appointment|date_of_appointment_for_current_designation|date_of_cessation";
	public static String CHARGESHEADER = "holder_name|date|amount|type";
	public static String FINANCIALDATASTATUSHEADER = "last_fin_year_end|last_updated";
	public static String FINANCIALSHEADERS = "year|nature|stated_on|capital_wip|cash_and_bank_balances|investments|inventories|net_fixed_assets|other_assets|total_assets|trade_receivables|share_capital|reserves_and_surplus|others|long_term_borrowings|other_current_liabilities_and_provisions|other_long_term_liabilities_and_provisions|trade_payables|short_term_borrowings|total_equity|total_equity_and_liabilities|depreciation|income_tax|interest|operating_cost|operating_profit|other_income|profit_before_interest_and_tax|profit_before_tax|revenue|exceptional_items_before_tax|minority_interest_and_profit_from_associates_and_joint_ventures|profit_after_tax_but_before_exceptional_items_after_tax|profit_before_tax_and_exceptional_items_before_tax|profit_after_tax|pnl_version";
	public static String AUDITORSHEADERS = "auditor_name|year|auditor_firm_name";
	public static String BIFR_HISTORY_HEADER = "case_number|date|status";
	public static String CDR_HISTORY_HEADER = "date|description";
	public static String DEFAULTER_LIST_HEADER = "date|agency|bank|amount";
	public static String LEGAL_HISTORY_HEADER = "case_title|court|date";
	public static String CREDIT_RATINGS_HEADER = "rating_date|rating|rating_agency|type_of_loan|amount|currency";
	public static String HOLDING_SUBSIDIARY_COMPANIES_HEADER = "cin|legal_name|next_cin";
	public static String INDUSTRY_SEGMENTS_HEADER = "industry|segments";

	// For SAPERP
	public static String SAP_HOST_NAME = "HOSTNAME";
	public static String SAP_INSTANCE_NO = "NSTANCENO";
	public static String SAP_CLIENT = "CLIENT";
	public static String SAP_USER = "USER";
	public static String SAP_PASSWORD = "PASSWORD";
	public static String SAP_LANGUAGE = "LANGUAGE";

	public static String SAP_ERP = "SAPERP";
	public static String FUNCTION_MODULE = "functionModule";
	public static String TABLE_NAME = "tableName";
	public static String ROW_COUNT = "ROWCOUNT";
	public static String COLUMN_NAME = "columnName";
	public static String DATA_FILTER = "dataFilter";
	public static String QUERY_TABLE = "QUERY_TABLE";
	public static String DELIMITER = "DELIMITER";
	public static String FIELDS = "FIELDS";
	public static String FIELD_NAME = "FIELDNAME";
	public static String OPTIONS = "OPTIONS";
	public static String TEXT = "TEXT";
	public static String SAP_DATA = "DATA";
	public static String WA = "WA";
	public static String ROW_CONSTANT = "1";
	public static String NO_DATA = "NO_DATA";
	public static String NO_DATA_CONSTANT = "T";
	public static String NO_OF_SPLIT = "NOOFSPLIT";
	public static String SPLIT_ROW_COUNT = "SPLITROWCOUNT";
	public static String ROW_SKIPS = "ROWSKIPS";
	public static String FETCH_ALL_RECORDS = "0";

	// GoogleAnalytics
	public static String APPLICATION_NAME = "Google Analytics";
    }

    public static final String ALREADYSESSION = "ALREADYSESSION";

    // for nosql
    public static String HIVE_QUERY_DSID = "dsId";
    public static String ISNOSQL_QUERY = "isNoSql";

    // Decoupling constants
    public static class Decouple {
	public static final String DECOUPLE_EXTERNAL_TABLE_LOCATION = "/maxiq/ds/external_tables/";
	public static final String S_OPTION = "-s";
	public static final String Q_OPTION = "-q";
	public static final String D_OPTION = "-d";
	public static final String F_OPTION = "-f";
	public static final String U_OPTION = "-u";
	public static final String P_OPTION = "-p";
	public static final String C_OPTION = "-c";
	public static final String THRIFT_USERNAME = Cache.getProperty(CacheConstants.HIVE_SUPER_USER);
	public static final String THRIFT_PASSWORD = Cache.getProperty(CacheConstants.HIVE_SUPER_PASSWORD);

	public static class URLConstants {
	    // constants required for killing application in spark stand alone
	    public static final String CLUSTER_STAT_URL = "/json/";
	    public static final String APP_KILL_URL = "/app/kill/";
	}
    }

    public static final String APPEND_HEADER_FILE_NAME = "appendHeader";
    public static final String COMPONENT_TYPE = "componentType";
    public static final String OWNER_PROJECT_ID = "ownerProjectId";

    public static String TAB_MYAPPROVALS = "MyApprovals";
    public static String TAB_MYREQUESTS = "MyRequests";
    public static String NO = "NO";

    public static String PAGENUMBER = "pageNumber";
    public static String NUMBEROFRECORDSPERPAGE = "numberOfRecordsPerPage";
    public static String SEARCHCONTENT = "searchContent";
    public static String DATASOURCEID = "dataSourceId";
    public static String TABLE_NAME = "tableName";
}

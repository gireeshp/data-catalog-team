package com.augmentiq.constant.maxiq;

import java.util.HashSet;

import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.google.common.collect.Sets;

public class GlobalParams {
    public static final String MAXIQ_DEL = "";
    public static final String JOIN_SRC_RECORD_DEL = "";
    public static String MAXIQ_DATA_PATH = Cache.getProperty(CacheConstants.MAXIQ_DATA_PATH);
    public static String HADOOP_BASE_CONF_PATH = Cache.getProperty(CacheConstants.HADOOP_BASE_CONF_PATH);
    public static final String EQUAL_OPERATOR = "=";
    public static final String BACK_SLASH = "/";
    public static final String UNDER_SCORE = "_";
    public static final String MAXIQ_KEYWORD = "MaxIQ";
    public static final String COMMA = ",";
    public static final String COLON = ":";

    public static final String OUT = "OUT";
    public static final String ERR = "ERR";

    public static final String MAXIQ_STD_DT_FORMAT = "dd/MM/yyyy/HH/mm/ss";
    public static final String GLOBAL_PARAM_TIMESTAMP_FORMAT = Cache.getProperty(CacheConstants.GLOBAL_PARAM_TIMESTAMP_FORMAT);
    public static final String GLOBAL_PARAM_DATE_FORMAT = Cache.getProperty(CacheConstants.GLOBAL_PARAM_DATE_FORMAT);
    public static final String MAXIQ_STD_TS_FORMAT = Cache.getProperty(CacheConstants.MAXIQ_STD_TS_FORMAT);
    public static final String MAXIQ_STD_DF_TS_FORMAT = Cache.getProperty(CacheConstants.MAXIQ_STD_DF_TS_FORMAT);
    public static final String MAXIQ_STD_DF_DATE_FORMAT = Cache.getProperty(CacheConstants.MAXIQ_STD_DF_DATE_FORMAT);
    public static final String MAXIQ_STD_IST_FORMAT = "IST";
    public static final String ASTERISK = "*";

    public static interface ERRORS {
	public static final String INCORRECT_NO_OF_FIELDS = "Incorrect number of fields in input";
	public static final String RECORD_REJECTED = "Record rejected";
	public static final String FIELD_REJECTED = "Field rejected";
    }

    public static HashSet<String> keyWords = Sets.newHashSet("SQRT", "DATE", "STRING", "CHAR", "CHARACTER", "NUMERIC", "VARCHAR", "VARCHAR2", "SELECT", "FROM", "WHERE", "INSERT", "INTO", "UPDATE", "SET", "GROUP", "BY", "ORDER", "CREATE", "ALTER", "TRUNCATE", "DROP", "RENAME", "MODE", "MOD", "MIN", "MAX", "AVG", "AVERAGE", "LEN", "LENGTH", "COUNT", "RANK", "ROUND", "INT", "STRLEN", "SUBSTR", "TRIM", "REPLACE", "IS", "NOT", "NULL", "POWER", "DIVIDE", "SORT", "LPAD", "RPAD", "REPLACEALL", "DECODE",
	    "NVL", "MEAN", "MEDIAN", "COUNT", "SUM", "DENSE", "WORD", "IN", "LIKE", "TABLE", "DISTINCT", "LOAD", "JOIN", "FILTER", "TRANSPOSE", "EXEC", "SHOW", "IF", "DATABASE", "DESC", "ASC", "FOR", "ROW", "USE", "TIME", "DATABASES", "CAST", "FUNCTION", "CLUSTERED", "TO_DATE", "OVERWRITE", "WHILE", "FUNCTIONS", "STORED", "STAGE", "LCASE", "DAY", "SYS", "TERMINATED", "MONTH", "TYPE", "EXPLAIN", "EXTENDED", "IMPORT", "FULL", "USING", "REGEXP", "TINYINT", "DOUBLE", "COMMENT", "DISTRIBUTE",
	    "ARRAY", "SORTED", "LTRIM", "A", "CONCAT", "M", "KEY", "NUMBER", "ON", "PLANS", "RLIKE", "S", "LOCAL", "OF", "LIST", "FLOOR", "TABLESAMPLE", "UCASE", "PLAN", "OUT", "OR", "DESCRIBE", "LOCATION", "BUCKET", "INPATH", "LOWER", "FALSE", "RTRIM", "SIZE", "UPPER", "MODIFY", "INNER", "YEAR", "REGEXP_REPLACE", "BUCKETS", "TEXTFILE", "PARTITIONED", "TRANSFORM", "LINES", "FLOAT", "ABSTRACT", "AND", "PRIMITIVE", "GET_JSON_OBJECT", "AS", "SYNTAX", "BOOLEAN", "TREE", "PARTITION", "LEFT", "RAND",
	    "ALL", "COLUMN", "FROM_UNIXTIME", "COLLECTION", "ADD", "OUTER", "DELIMITED", "DATETIME", "DEFAULT", "RIGHT", "ITEMS", "STRUCT", "FIELDS", "SEMI", "TRUE", "LINE", "UNION", "SEQUENCEFILE", "COLUMNS", "BIGINT", "MAP", "CLUSTER", "DIRECTORY", "CEIL", "FORMAT", "KEYS", "DATA", "REDUCE", "SMALLINT", "CLUSTERSTATUS", "JAR", "ENABLE", "UNDO", "DIRECTORIES", "INPUTDRIVER", "DBPROPERTIES", "ROLES", "RECORDWRITER", "PRECEDING", "SERDEPROPERTIES", "ANALYZE", "EXPORT", "DISABLE", "SHARED",
	    "PRESERVE", "GROUPING", "DEFINED", "ELSE", "MATERIALIZED", "FILE", "REWRITE", "RESTRICT", "SERVER", "MSCK", "INTERSECT", "FETCH", "MORE", "READ", "NONE", "USER", "SKEWED", "SERDE", "LOCKS", "END", "UNLOCK", "UTCTIMESTAMP", "OPTION", "OWNER", "NOSCAN", "COMPACT", "SCHEMAS", "THEN", "NO_DROP", "PARTIALSCAN", "PLUS", "EXISTS", "PRETTY", "UNIQUEJOIN", "OUTPUTFORMAT", "TO", "VALUE_TYPE", "HAVING", "MINUS", "IGNORE", "HOUR", "READONLY", "CURRENT", "CHANGE", "SCHEMA", "UNSIGNED", "DECIMAL",
	    "WHEN", "INPUTFORMAT", "STREAMTABLE", "READS", "ROWS", "REVOKE", "LONG", "UNARCHIVE", "TOUCH", "BETWEEN", "FIRST", "EXTERNAL", "RECORDREADER", "ARCHIVE", "SETS", "COMPUTE", "TRIGGER", "CASE", "UNSET", "CASCADE", "REPAIR", "ELEM_TYPE", "LESS", "BOTH", "ADMIN", "PROTECTION", "OFFLINE", "SECOND", "VIEW", "ROLE", "FILEFORMAT", "ROLLUP", "DELETE", "FORMATTED", "TABLES", "URI", "IDXPROPERTIES", "HOLD_DDLTIME", "OUTPUTDRIVER", "SSL", "TRANSACTIONS", "CUBE", "STATISTICS", "ESCAPED",
	    "UNBOUNDED", "TEMPORARY", "CURSOR", "TIMESTAMP", "CURRENT_DATE", "OVER", "AUTHORIZATION", "LIMIT", "MAPJOIN", "DEFERRED", "PRINCIPALS", "EXCLUSIVE", "FOLLOWING", "UTC", "CROSS", "REBUILD", "INTERVAL", "LOCK", "INDEX", "CONTINUE", "PARTITIONS", "EXCHANGE", "COMPACTIONS", "RELOAD", "CURRENT_TIMESTAMP", "WITH", "GRANT", "PERCENT", "LATERAL", "VALUES", "RANGE", "INDEXES", "PURGE", "BEFORE", "DEPENDENCY", "AFTER", "PROCEDURE", "CONCATENATE", "SUB", "SHOW_DATABASE", "KEY_TYPE",
	    "UNIONTYPE", "TBLPROPERTIES", "MUL", "WINDOW", "LOGICAL", "BINARY", "MINUTE", "MACRO", "CONF", "DIV");
}

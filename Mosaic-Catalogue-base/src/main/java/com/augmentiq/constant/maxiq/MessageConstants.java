package com.augmentiq.constant.maxiq;

public interface MessageConstants {
    public static final String JOB_INSTANCEID = "jobInstanceId";
    public static final String APP_ID = "appId";
    public static final String PARENT_APP_ID = "parent_app_id";
    public static final String APP_NAME = "appName";
    public static final String APP_INSTANCE_ID = "appInstanceId";
    public static final String HADOOP_BASE_CONF_PATH = "hadoopBaseConfPath";
    public static final String HDFS_BLANK_PATH = "hdfsBlankPath";
    public static final String NODE_ID = "nodeId";
    public static final String NODE_INFO_ID = "ID";
    public static final String NODE_DESCRIPTION = "nodeDescription";
    public static final String NODE_LABEL = "nodeLabel";
    public static final String STAGE_LEVEL = "stageId";
    public static final String ROOT_ID = "root_id";
    public static final String RUN_MODE = "runMode";
    public static final String RUN_MODE_YARN = "HDFS";
    public static final String RUN_MODE_LOCAL = "yarn-cluster";
    public static final String NODES = "nodes";
    public static final String GROUP_ID = "groupId";

    public static final String CREATED_BY = "createdBy";
    public static final String WORKFLOW_RUN_BY = "workflowRunBy";
    public static final String STATUS = "status";
    public static final String EXECUTE_NODES = "execute_nodes";

    public static final String INPUTS = "inputs";
    public static final String OUTPUTS = "outputs";

    public static final String DATAFILE_ALIAS = "dfalias";
    public static final String REF_DATA_SOURCE_ID = "refdatasourceid";
    public static final String FILE_PATH = "filepath";

    public static final String PROCESS_TYPE = "processType";

    public static final String SUCCESSFUL = "successful";
    public static final String SUCCEEDED = "succeeded";
    public static final String FAIL = "fail";
    public static final String EXCEPTION = "exception";
    public static final String ERROR = "error";
    public static final String DSIDS = "dsids";
    public static final String DS_ID = "dsId";
    public static final String PATH = "path";

    public static final String CLEAN_COMMAND = "cleanCommand";
    public static final String CLEAN_HDFS_FOLDER = "clean_hdfs_folder";
    public static final String DELETE_HDFS_FOLDER_LIST = "delete_hdfs_folder_list";
    public static final String POLL_INTERVAL = "poll_interval";
    public static final String CHILD_NODES = "child_nodes";
    public static final String RCode = "RCode";
    public static final String IS_STREAM = "IS_STREAM";
    public static final String KEYNAME = "KeyName";
}
package com.augmentiq.constant.maxiq;

public class RequestConstants {
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String DELETE = "DELETE";
    public static final String OPTIONS = "OPTIONS";
    public static final String PUT = "PUT";
    public static final Integer RESPONE_CODE_200 = 200;
    public static final Integer RESPONE_CODE_204 = 204;
    public static final String APPLICATION_JSON = "application/json";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String ACCEPT = "Accept";
    public static final String AUTH = "Authorization";
    public static final String BASIC_AUTH = "Basic ";
    public static final String HTTP = "http";
    public static final String HTTPS = "https";

    public static final String ORIGIN = "Origin";
    public static final String MAX_AGE_0 = "max-age=0";
    public static final String CACHE_CONTROL = "Cache-Control";
    public static final String X_FORWARDED_PORT = "X-Forwarded-Port";
    public static final String X_FORWARDED_PROTO = "X-Forwarded-Proto";
    public static final String UPGRADE_INSECURE_REQUESTS = "Upgrade-Insecure-Requests";
    public static final String KEEP_ALIVE = "keep-alive";
    public static final String CONNECTION = "Connection";
    public static final String REFERER = "Referer";
    public static final String EN_US_EN_Q_0_5 = "en-US,en;q=0.5";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String HOST2 = "Host";
    public static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    public static final String TRUE = "true";
    public static final String TERMINATE = "terminate";
    public static final String ID = "id";
}

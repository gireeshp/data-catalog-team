package com.augmentiq.constant.maxiq;

public class UserManagementConstants {

    public static final String PASSWORD = "password";
    public static final String UPDATED_TS = "updatedTs";
    public static final String SUB_GROUPID_LIST = "subGroupIdList";
    public static final String SUB_GROUP_LIST = "subGroupList";

    public class GROUP_DOMAIN_CONSTANTS {

	public static final String GROUP_ID = "groupId";
	public static final String RANGER_GROUP_ID = "rangerGroupId";
    }
}

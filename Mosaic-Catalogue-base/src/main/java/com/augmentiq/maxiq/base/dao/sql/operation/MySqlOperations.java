package com.augmentiq.maxiq.base.dao.sql.operation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.StackTraceReader;
import com.augmentiq.maxiq.base.dao.sql.operation.hbase.HbaseUtility;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * Performs Mysql insert and fetch operations
 *
 * @author shiva Changed for MAX-523 By Balkrushna Patil on 02-Oct-2016
 */
public class MySqlOperations {

  private static final Logger logger = LoggerFactory.getLogger(MySqlOperations.class);

  /**
   * This method executes the sqlStmt by replacing the parameters in the map params
   *
   * @warn please make sure that map param should be of LinkedHashMap type
   * @param sqlStmt
   * @param params
   * @throws SystemException
   */
  public static void executeQuery(String sqlStmt, Map<String, Object> params)
      throws SystemException {

    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateSqlStatmentQueryData(
                connection.prepareStatement(sqlStmt), params)) {
      stmtment.executeUpdate();

    } catch (SystemException | SQLException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " << executeQuery() : {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << executeQuery() : ");
  }

  public static List<Map<String, Object>> executeQueryForResultSet(String sqlStmt, String[] params)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> executeQueryForResultSet()"
            + ParamUtils.getString(sqlStmt, params));

    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement(); ) {
      statement.execute(sqlStmt);
      ResultSet resultSet = statement.getResultSet();
      List<Map<String, Object>> list = HbaseUtility.getMapOfFieldsFromResultSet(resultSet, params);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << executeQueryForResultSet()"
              + ParamUtils.getString(list));
      return list;
    } catch (SQLException e) {

      logger.error(LoggerConstants.LOG_MAXIQWEB + " << executeQueryForResultSet() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
    }
    return null;
  }

  public static List<Map<String, Object>> executeQueryForResultSetPrepStatement(
      String sqlStmt, String[] params, Map<String, Object> objectMap) throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> executeQueryForResultSetPrepStatement()"
            + ParamUtils.getString(sqlStmt, params));

    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement statement =
            HbaseUtility.generateSqlStatmentQueryData(
                connection.prepareStatement(sqlStmt), objectMap);
        ResultSet resultSet = statement.executeQuery()) {

      List<Map<String, Object>> list = HbaseUtility.getMapOfFieldsFromResultSet(resultSet, params);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << executeQueryForResultSetPrepStatement()"
              + ParamUtils.getString(list));
      return list;
    } catch (SQLException e) {

      logger.error(
          LoggerConstants.LOG_MAXIQWEB + " << executeQueryForResultSetPrepStatement() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
    }
    return null;
  }

  /**
   * Saves the object into Mysql table.It uses the annotation {@link TableName} of the bean object
   * to determine the table name
   *
   * @param object
   * @throws SystemException
   * @throws SQLException
   */
  public static void insert(Object object) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> insert()" + ParamUtils.getString(object));

    if (null != object) {
      String tableName = HbaseUtility.getTableNameForObject(object);

      deleteRow(object);
      Connection connection = null;
      java.sql.PreparedStatement insertStatement = null;

      try {
        connection = MysqlConnection.getConnection();

        // Setting auto-commit false so that dependent queries are
        // commit at once.
        connection.setAutoCommit(false);
        String insertQuery = HbaseUtility.generateInsertQuery(object, tableName);
        PreparedStatement stmt = connection.prepareStatement(insertQuery);
        stmt = HbaseUtility.generateInsertQueryData(stmt, object);
        stmt.executeUpdate();
        connection.commit();

      } catch (Exception e) {
        logger.error(StackTraceReader.stringFromStackTrace(e));

        try {
          connection.rollback();
        } catch (Exception ex) {
          logger.error(StackTraceReader.stringFromStackTrace(ex));
        }

        logger.debug(
            LoggerConstants.LOG_MAXIQWEB + " : << insert()" + ParamUtils.getString(object));
        ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
      } finally {

        try {

          if (insertStatement != null) {
            insertStatement.close();
          }

          if (connection != null) {
            connection.close();
          }

        } catch (Exception e) {
          logger.debug(
              LoggerConstants.LOG_MAXIQWEB + " : << insert()" + ParamUtils.getString(object));
          ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
        }
      }
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << insert()" + ParamUtils.getString(object));
  }

  /**
   * Saves the object with primary key into Mysql table.It uses the annotation {@link TableName} of
   * the bean object to determine the table name
   *
   * @param object
   * @param primaryKeyAutoIncrement should be null
   * @return
   * @throws SystemException
   * @throws SQLException
   */
  public static Long insert(Object object, String primaryKeyAutoIncrement) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> insert()" + ParamUtils.getString(object));
    Long generatedKey = 0L;
    if (null != object) {
      String tableName = HbaseUtility.getTableNameForObject(object);

      Map<String, Object> map = null;

      deleteRow(object);

      try (Connection connection = MysqlConnection.getConnection();
          Statement statement = connection.createStatement(); ) {

        connection.setAutoCommit(false);

        map = HbaseUtility.getMapForBean(object);

        String insertStatement =
            HbaseUtility.generateInsertQueryForAutoIncrementedPrimaryKey(
                primaryKeyAutoIncrement, tableName, map);

        if (map.get(primaryKeyAutoIncrement) != null) {
          insert(object);
          return Long.parseLong(String.valueOf(map.get(primaryKeyAutoIncrement)));
        }

        logger.debug("@@@Insert : " + tableName + " Query " + insertStatement);

        java.sql.PreparedStatement prepareStatement =
            connection.prepareStatement(insertStatement, new String[] {primaryKeyAutoIncrement});
        HbaseUtility.generateInsertQueryDataWithPK(
            primaryKeyAutoIncrement, prepareStatement, object);

        prepareStatement.executeUpdate();

        connection.commit();

        ResultSet rs = prepareStatement.getGeneratedKeys();
        if (rs.next()) {
          generatedKey = rs.getLong(1);
        }

        connection.close();
      } catch (SQLException e) {
        logger.error(LoggerConstants.LOG_MAXIQWEB + " : << insert() {} ", e);
        ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
      }
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << insert()" + ParamUtils.getString(object));
    return generatedKey;
  }

  /**
   * Updates the object with primary key into Mysql table.It uses the annotation {@link TableName}
   * of the bean object to determine the table name
   *
   * @param object, query
   * @throws SystemException
   */
  public static void update(Object object, Map<String, Object> query) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> insert()" + ParamUtils.getString(object));

    if (null != object) {
      String updateStatement = HbaseUtility.generateUpdateQuery(object, query);

      try (Connection connection = MysqlConnection.getConnection();
          PreparedStatement stmt = connection.prepareStatement(updateStatement);
          PreparedStatement stmtment = HbaseUtility.generateSqlStatmentQueryData(stmt, query)) {
        stmtment.executeUpdate();
      } catch (SQLException e) {
        logger.error(LoggerConstants.LOG_MAXIQWEB + " : << update() {}", e);
        ExceptionsMessanger.throwException(new SystemException(), "ERR_017", e.getMessage());
      }
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << insert()" + ParamUtils.getString(object));
  }

  /**
   * This method is deprecated use executeQuery method instead
   *
   * @param sql
   * @param query
   * @throws SystemException
   */
  @Deprecated
  public static void updateQuery(String sql, Map<String, Object> query) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> updateQuery()" + ParamUtils.getString(sql));
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query)) {
      stmtment.executeUpdate();
    } catch (SQLException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " >> updateQuery() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
  }

  /**
   * Takes the argument class and filter conditions to fetch the data from the Mysql into list of
   * type input class. Pass the filter null if we do not want to put any conditions
   *
   * @param entityClass
   * @param filter
   * @return
   * @throws SystemException
   */
  public static <T> List<T> scanForQuery(Class<T> entityClass, Map<String, Object> query)
      throws SystemException {

    String sql = HbaseUtility.generateQuery(entityClass, query);

    List<T> list = null;
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtment.executeQuery()) {
      list = new ArrayList<>();

      while (resultSet.next()) {
        list.add(HbaseUtility.buildObject(entityClass, resultSet));
      }

    } catch (SQLException | ClassNotFoundException | IOException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + "scanForQuery {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }

    return list;
  }

  /**
   * Takes the argument class and filter conditions to fetch the data from the Mysql into list of
   * type input class. Pass the filter null if we do not want to put any conditions
   *
   * @param entityClass
   * @param filter
   * @return
   * @throws SystemException
   */
  public static <T> List<T> scan(Class<T> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> scan()" + ParamUtils.getString(entityClass, query));

    String sql = HbaseUtility.generateQuery(entityClass, query);

    List<T> scanWithSqlQuery = scanWithSqlQuery(entityClass, sql, query);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << scan()" + ParamUtils.getString(scanWithSqlQuery));

    return scanWithSqlQuery;
  }

  public static <T> List<T> scanWithSqlQuery(
      Class<T> entityClass, String sql, Map<String, Object> query) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> scanWithSqlQuery()"
            + ParamUtils.getString(entityClass, sql));

    List<T> list = null;

    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtment.executeQuery()) {
      list = new ArrayList<T>();
      while (resultSet.next()) {
        list.add(HbaseUtility.buildObject(entityClass, resultSet));
      }
    } catch (SQLException | ClassNotFoundException | IOException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " : << scanWithSqlQuery() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << scanWithSqlQuery()" + ParamUtils.getString(list));

    return list;
  }

  /**
   * Takes the argument class and Query (filter condition) to fetch the data from the Hbase into
   * object of input class. Pass the filter null if we do not want to put filter any conditions
   *
   * @param entityClass
   * @param query
   * @return
   * @throws SystemException
   */
  public static <T> T scanOneForQuery(Class<T> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> scanOneForQuery()"
            + ParamUtils.getString(entityClass, query));

    T buildObject = null;
    String sql = HbaseUtility.generateQuery(entityClass, query);
    logger.debug("scanOneForQuery : " + sql);
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtWithData =
            HbaseUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtWithData.executeQuery()) {

      while (resultSet.next()) {

        buildObject = HbaseUtility.buildObject(entityClass, resultSet);
      }

    } catch (SQLException | ClassNotFoundException | IOException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " : << scanOneForQuery() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << scanOneForQuery()"
            + ParamUtils.getString(buildObject));
    return buildObject;
  }

  /**
   * Takes the argument class and SQL Query (filter condition) to fetch the data from the Mysql into
   * object of input class. Pass the filter null if we do not want to put filter any conditions
   *
   * @param entityClass
   * @param query
   * @return
   * @throws SystemException
   */
  public static <T> T scanOneForSqlQuery(Class<T> entityClass, String query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> scanOneForQuery()"
            + ParamUtils.getString(entityClass, query));
    T buildObject = null;
    logger.debug("scanOneForSqlQuery : " + query);
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmt = connection.prepareStatement(query);
        ResultSet resultSet = stmt.executeQuery()) {
      while (resultSet.next()) {
        buildObject = HbaseUtility.buildObject(entityClass, resultSet);
      }

    } catch (SQLException | ClassNotFoundException | IOException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " : << scanOneForSqlQuery() {} ", e);

      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << scanOneForSqlQuery()"
            + ParamUtils.getString(buildObject));
    return buildObject;
  }

  public static <T> T scanOneForSqlQuerywithData(
      Class<T> entityClass, String sql, Map<String, Object> query) throws SystemException {
    T buildObject = null;
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtment.executeQuery()) {
      while (resultSet.next()) {
        buildObject = HbaseUtility.buildObject(entityClass, resultSet);
      }
    } catch (Exception e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " : << scanOneForSqlQuerywithData() {}", e);

      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    return buildObject;
  }

  public static <T> void deleteRecords(Object object, Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> :  deleteRecords()"
            + ParamUtils.getString(object, query));

    String sql = HbaseUtility.generateDeleteQuery(object.getClass(), query);

    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmt = connection.prepareStatement(sql);
        PreparedStatement stmtment = HbaseUtility.generateSqlStatmentQueryData(stmt, query)) {
      stmtment.executeUpdate();
    } catch (SQLException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " : << : deleteRecords() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << : deleteRecords()"
            + ParamUtils.getString(object, query));
  }

  public static <T> void deleteRow(Object object) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> deleteRow()" + ParamUtils.getString(object));

    if (null != object) {

      Map<String, Object> map = HbaseUtility.getMapForBean(object);
      Map<String, Object> query = new HashMap<String, Object>();
      String key = HbaseUtility.getIdAnnotationForClass(object.getClass());
      Object value = map.get(key);
      if (null != value) {
        query.put(key, value);
        String sql = HbaseUtility.generateDeleteQuery(object.getClass(), query);

        try (Connection connection = MysqlConnection.getConnection();
            PreparedStatement stmtment =
                HbaseUtility.generateSqlStatmentQueryData(
                    connection.prepareStatement(sql), query)) {
          stmtment.executeUpdate();
        } catch (SQLException e) {
          logger.error(LoggerConstants.LOG_MAXIQWEB + " : << : getDeleteQuery() {}", e);
          ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
        }
        logger.debug(
            LoggerConstants.LOG_MAXIQWEB + " << getDeleteQuery()" + ParamUtils.getString(sql));
      }
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << deleteRow()" + ParamUtils.getString(object));
  }

  public static <T> Long count(Class<T> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " >> count()" + ParamUtils.getString(entityClass, query));
    String sql = HbaseUtility.generateCountQuery(entityClass, query);
    Long counter = 0L;
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtment.executeQuery()) {
      while (resultSet.next()) {
        counter = resultSet.getLong(1);
      }

    } catch (SQLException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " << count() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << count()" + ParamUtils.getString(counter));
    return counter;
  }

  public static <T> List<String> distinctOnField(
      Class<T> entityClass, Map<String, Object> query, String distinctFieldName)
      throws SystemException, ClassNotFoundException, IOException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> distinctOnField()"
            + ParamUtils.getString(entityClass, query, distinctFieldName));

    String sql = HbaseUtility.generateDistinctQuery(entityClass, query, distinctFieldName);
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateSqlStatmentQueryData(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtment.executeQuery()) {
      List<String> listOfFieldsFromResultSet =
          HbaseUtility.getListOfFieldsFromResultSet(resultSet, "d");

      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " << distinctOnField()"
              + ParamUtils.getString(listOfFieldsFromResultSet));

      return listOfFieldsFromResultSet;
    } catch (SQLException e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + " << distinctOnField() {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    return null;
  }

  /**
   * Warn: Don't send any list in the map empty No filter will be applied in that case
   *
   * @param entityClass
   * @param query
   * @return
   * @throws SystemException
   * @throws SQLException
   * @throws IOException
   * @throws ClassNotFoundException
   */
  public static <T> List<T> getListPresentInArgumentList(
      Class<T> entityClass, Map<String, List<Object>> query) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> scan()" + ParamUtils.getString(entityClass, query));

    String sql = HbaseUtility.generateQueryForInCondition(entityClass, query);

    ArrayList<T> list = null;
    try (Connection connection = MysqlConnection.getConnection();
        PreparedStatement stmtment =
            HbaseUtility.generateQueryDataForInCondition(connection.prepareStatement(sql), query);
        ResultSet resultSet = stmtment.executeQuery()) {
      list = new ArrayList<T>();

      while (resultSet.next()) {
        list.add(HbaseUtility.buildObject(entityClass, resultSet));
      }
    } catch (Exception e) {
      logger.error(LoggerConstants.LOG_MAXIQWEB + ">> getListPresentInArgumentList {}", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    return list;
  }

  public static Long complexQueryCount(String complexQuery) throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> count()" + ParamUtils.getString(complexQuery));

    Long count = 0L;

    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(complexQuery)) {

      while (resultSet.next()) {
        count = resultSet.getLong(1);
      }

    } catch (SQLException e) {

      logger.error(LoggerConstants.LOG_MAXIQWEB + " << complexQueryCount() {} ", e);
      ExceptionsMessanger.throwException(new SystemException(), "ERR_018", e.getMessage());
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << count()" + ParamUtils.getString(count));
    return count;
  }
}

package com.augmentiq.maxiq.base.dao.sql.operation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.apps.setup.QueryConstants;
import com.augmentiq.maxiq.constant.cache.CacheConstants;

public class MysqlConnection {

  private static final Logger logger = LoggerFactory.getLogger(MysqlConnection.class);

  private static String jdbcDriver = Cache.getProperty(CacheConstants.JDBC_DRIVER);
  private static String dbAddress = Cache.getProperty(CacheConstants.DB_ADDRESS);
  private static String dbName = Cache.getProperty(CacheConstants.DB_NAME);
  private static String userName = Cache.getProperty(CacheConstants.USER_NAME);
  private static String password = Cache.getProperty(CacheConstants.PASSWORD);

  private static Connection cn = null;
  private static Long lastGivenAt = 0L;

  public static Connection getConnection() throws SystemException {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : >> getConnection()");
    Long currentTime = System.currentTimeMillis();

    if (cn == null || (currentTime - lastGivenAt) / 1000 > 30) {

      try {
        if (cn != null) cn.close();

        cn = null;
        lastGivenAt = currentTime;

        Class.forName(jdbcDriver);
        Connection connection =
            DriverManager.getConnection(
                dbAddress + dbName + "?zeroDateTimeBehavior=convertToNull", userName, password);
        logger.debug(
            LoggerConstants.LOG_MAXIQWEB,
            " : << getConnection()" + ParamUtils.getString(connection));
        return connection;
      } catch (ClassNotFoundException e) {
        logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
        logger.debug(LoggerConstants.LOG_MAXIQWEB, " : << getConnection()");
        e.printStackTrace();
        throw new SystemException(QueryConstants.DbExceptions.DRIVER_NOT_LOADED);
      } catch (SQLException e) {
        logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
        logger.debug(LoggerConstants.LOG_MAXIQWEB, " : << getConnection()");
        e.printStackTrace();
        throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
      } catch (Exception e) {
        logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
        logger.debug(LoggerConstants.LOG_MAXIQWEB, " : << getConnection()");
        e.printStackTrace();
        throw new SystemException(QueryConstants.DbExceptions.SQL_EXCEPTION);
      }
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB, " : << getConnection()" + ParamUtils.getString(cn));
    return cn;
  }
}

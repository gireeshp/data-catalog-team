package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import org.apache.commons.lang3.StringUtils;

public class DataTypeUtils {

  public static Long getLong(String value) {
    if (StringUtils.isBlank(value)) return 0L;

    return Long.parseLong(value);
  }

  public static Long getLong(String value, Long defaultValue) {
    if (StringUtils.isBlank(value)) return defaultValue;

    return Long.parseLong(value);
  }
}

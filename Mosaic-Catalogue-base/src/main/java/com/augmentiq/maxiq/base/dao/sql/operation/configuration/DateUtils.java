package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.augmentiq.constant.maxiq.GENERAL_CONSTANTS;
import com.augmentiq.constant.maxiq.GlobalParams;
import com.augmentiq.maxiq.constant.configuration.enums.FieldDataTypes;
import com.augmentiq.maxiq.constant.configuration.generic.GenericConstants;
import com.google.common.collect.Sets;

public class DateUtils {
  // private static Logger logger_ = LoggerFactory.getLogger(DateUtils.class);
  public static String DATE_FORMAT1 = "yyyy-MM-dd";
  public static String DATE_FORMAT2 = "dd/MM/yyyy";
  public static String DATE_FORMAT3 = "dd:MM:yyyy";
  public static String DATE_FORMAT4 = "ddMMyyyy";
  public static String DATE_FORMAT5 = "d MMMM, yyyy";
  public static String DATE_FORMAT6 = "dd-MMM-yyyy";
  public static String DATE_FORMAT7 = "MMdd";
  public static String DATE_FORMAT8 = "dd MMM yyyy hh:mm:SS a";
  public static String TIMESTAMP_FORMAT = "dd/MM/yyyy hh:mm:ss";
  public static String TIMESTAMP_FORMAT1 = "dd/MM/yyyy hh:mm:ss.SSS";
  public static String TIMESTAMP_FORMAT3 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
  public static String TIMESTAMP_FORMAT4 = "hhmmss";
  public static String sample = "yyyy-mm-dd";
  public static String TIMESTAMP_FORMAT_HIVE = "yyyy-MM-dd HH:mm:ss";

  public static HashSet<String> dateFormatsInHashSet =
      Sets.newHashSet(
          "M#dd#yyyy",
          "dd#MM#yyyy",
          "dd#M#yyyy",
          "dd#MMM#yyyy",
          "MMM#dd#yyyy",
          "yyyy#M#dd",
          "yy#dd#MM",
          "yyyy#dd#M",
          "yyyy#dd#MMM",
          "yyyy#MMM#dd",
          "yyyy#MM#dd",
          "dd#MM#yy",
          "dd#MMM#yy",
          "yy#MM#dd",
          "MM#dd#yyyy");

  private static List<String> amPmFormat =
      new ArrayList<String>() {
        {
          add("am");
          add("Am");
          add("AM");
          add("pm");
          add("Pm");
          add("PM");
        }
      };

  // private static SimpleDateFormat sdf =null;

  public static Date parseDate(String dateString) {
    // logger_.warn("<< parseDate(): [ dateString : {}]", dateString);

    SimpleDateFormat sdf = null;

    if (StringUtils.isNotBlank(dateString)) {
      try {
        if (StringUtils.contains(dateString, "-")) {
          sdf = new SimpleDateFormat(DATE_FORMAT1);
          Date parse = sdf.parse(dateString);
          // logger_.warn(">> parseDate(): [ return : {}]",
          // parse);
          return parse;
        } else if (StringUtils.contains(dateString, "/")) {
          sdf = new SimpleDateFormat(DATE_FORMAT2);
          Date parse = sdf.parse(dateString);
          // logger_.warn(">> parseDate(): [ return : {}]",
          // parse);
          return parse;
        } else if (StringUtils.contains(dateString, ":")) {
          sdf = new SimpleDateFormat(DATE_FORMAT3);
          Date parse = sdf.parse(dateString);
          // logger_.warn(">> parseDate(): [ return : {}]",
          // parse);
          return parse;
        } else if (StringUtils.contains(dateString, ",")) {
          return new SimpleDateFormat(DATE_FORMAT5, Locale.ENGLISH).parse(dateString);
        } else if (!StringUtils.contains(dateString, "-")
            && !StringUtils.contains(dateString, "/")
            && !StringUtils.contains(dateString, ":")
            && StringUtils.length(dateString) == 8) {
          sdf = new SimpleDateFormat(DATE_FORMAT4);
          Date parse = sdf.parse(dateString);
          // logger_.warn(">> parseDate(): [ return : {}]",
          // parse);
          return parse;
        }
      } catch (Exception e) {
        return null;
      }
    }
    return null;
  }

  public static String getCurrentDate() {
    // logger_.warn("<< getCurrentDate() ");
    SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE,dd MMMM yyyy hh:mm:ss");
    // logger_.warn(">> getCurrentDate() ");
    return dateFormat.format(new Date());
  }

  public static String getDate() {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd, MMM yyyy");
    return dateFormat.format(new Date());
  }

  public static String getFormattedDate(Date date, String format) {

    // logger_.warn("<< getFormattedDate() : [ date : {}, format : {} ]",
    // date, format);

    SimpleDateFormat sdf = null;

    try {
      if (StringUtils.isNotBlank(format) && date != null) {
        sdf = new SimpleDateFormat(format);

        String format2 = sdf.format(date);
        // logger_.warn(">> getFormattedDate() : [ return : {}]",
        // format2);
        return format2;
      }

      return null;
    } catch (Exception e) {
      return null;
    }
  }

  public static Date getDateFromString(String inputDtString, String format) {

    // logger_.warn(
    // "<< getDateFromString() : [ inputDtString : {}, format : {} ]",
    // inputDtString, format);

    SimpleDateFormat sdf = null;

    Date outputDt = null;

    try {
      sdf = new SimpleDateFormat(format);
      //			sdf.setTimeZone(TimeZone.getTimeZone(GlobalParams.MAXIQ_STD_IST_FORMAT));

      outputDt = sdf.parse(inputDtString);
      /*
       * String formatedDate = sdf.format(sdf.parse(inputDtString));
       *
       * if (StringUtils.equalsIgnoreCase(inputDtString, formatedDate)) {
       * outputDt = sdf.parse(inputDtString); }
       */
    } catch (NullPointerException e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    } catch (IllegalArgumentException e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    } catch (ParseException e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    }

    // logger_.warn(">> getDateFromString() : [ return : {} ]", outputDt);
    return outputDt;
  }

  public static String getFormatedTimestamp(String date) {

    // logger_.warn("<< getFormatedTimestamp() : [ date : {} ]", date);

    SimpleDateFormat sdf = null;

    sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    Date parse2 = null;
    try {
      if (StringUtils.isNotBlank(date)) {
        parse2 = sdf.parse(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        date = dateFormat.format(parse2);
      }
      // logger_.warn(">> getFormatedTimestamp() : [ return : {} ]",
      // date);
      return date;
    } catch (ParseException e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    }
  }

  public static String getFormatedDate(String date) {

    // logger_.warn("<< getFormatedTimestamp() : [ date : {} ]", date);

    SimpleDateFormat sdf = null;

    sdf = new SimpleDateFormat("dd MMMM yyyy");
    Date parse2 = null;
    try {
      if (StringUtils.isNotBlank(date)) {
        parse2 = sdf.parse(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        date = dateFormat.format(parse2);
      }
      // logger_.warn(">> getFormatedTimestamp() : [ return : {} ]",
      // date);
      return date;
    } catch (ParseException e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    }
  }

  public static Object getSQLFormattedDateAsString(java.util.Date date) {

    // logger_.warn("<< getSQLFormattedDateAsString() : [ date : {}]",
    // date);

    SimpleDateFormat sdf = null;

    try {
      if (date != null) {
        sdf = new SimpleDateFormat("dd-MMM-yyyy");
        String format = sdf.format(date);
        // logger_.warn(
        // ">> getSQLFormattedDateAsString() : [ return : {}]",
        // format);
        return format;
      }

    } catch (Exception e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    }
    return null;
  }

  public static java.sql.Date getJavaDateToSqlDate(java.util.Date date) {

    // logger_.warn("<< getJavaDateToSqlDate() : [ date : {}]", date);

    try {
      if (date != null) {

        java.sql.Date sqlDate = new java.sql.Date(date.getTime());

        // logger_.warn(">> getJavaDateToSqlDate() : [ return : {}]",
        // sqlDate);
        return sqlDate;
      }

    } catch (Exception e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    }
    return null;
  }

  public static java.sql.Timestamp getJavaDateToSqlTimestamp(java.util.Date date) {

    // logger_.warn("<< getJavaDateToSqlDate() : [ date : {}]", date);

    try {
      if (date != null) {

        java.sql.Timestamp sqlDate = new java.sql.Timestamp(date.getTime());

        // logger_.warn(">> getJavaDateToSqlDate() : [ return : {}]",
        // sqlDate);
        return sqlDate;
      }

    } catch (Exception e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    }
    return null;
  }

  public static String getConvertedString(String str) {

    // logger_.warn("<< getConvertedString() : [ str : {}]", str);

    try {
      if (StringUtils.isNotBlank(str)) {
        String replace = str.trim().replace("-", "").replace("/", "");
        String substring = StringUtils.substring(replace, 0, 9);

        // logger_.warn(">> getConvertedString() : [ return : {} ]",
        // substring);
        return substring;
      }
    } catch (Exception e) {
      // logger_.error("** " + e.getMessage(), e);
      return null;
    }

    return null;
  }

  public static String getDateTimeAsStringAndFormatIt(
      String requestDateTime_, String currentDateTimeFormat, String newDateTimeFormat) {

    if (StringUtils.isNotBlank(requestDateTime_)) {
      String newDateString = requestDateTime_.replace("-", "");
      // logger_.warn(
      // ">> getDateTimeAsStringAndFormatIt() : [ return : {}]",
      // newDateString);
      return newDateString;
    }
    return null;
  }

  public static String getDateAsStringAndFormatIt(
      String ageAsOn_, String currentDateFormat, String newDateFormat) {
    // logger_.warn(
    // "<< getDateAsStringAndFormatIt() : [ ageAsOn_ :{}, currentDateFormat
    // : {}, newDateFormat : {}]",
    // LogUtil.getObjectArray(ageAsOn_, currentDateFormat,
    // newDateFormat));
    if (StringUtils.isNotBlank(ageAsOn_)) {
      String newDateString = ageAsOn_.replace("-", "");
      // logger_.warn(">> getDateAsStringAndFormatIt() : [ return : {}]",
      // newDateString);
      return newDateString;
    }
    return null;
  }

  public static String changeFormat(String dateString, String currentFormat, String newFormat) {

    // logger_.warn(
    // "<< changeFormat() : [ dateString :{}, currentFormat : {}, newFormat
    // : {}]",
    // LogUtil.getObjectArray(dateString, currentFormat, newFormat));

    SimpleDateFormat sdf;
    try {
      sdf = new SimpleDateFormat(currentFormat);
      sdf.setLenient(false);
    } catch (Exception e) {
      // logger_.warn(">> changeFormat() : [ return : {}]", "null");
      return null;
    }

    Date convertedDate = null;

    try {
      convertedDate = sdf.parse(dateString);
    } catch (Exception e) {
      // logger_.warn(">> changeFormat() : [ return : {}]", "null");
      return null;
    }

    String tempString = sdf.format(convertedDate);

    if (!StringUtils.equals(tempString, dateString)) {
      // logger_.warn(">> changeFormat() : [ return : {}]", "null");
      return null;
    }

    SimpleDateFormat newSdf = null;
    try {
      newSdf = new SimpleDateFormat(newFormat);
    } catch (Exception e) {
    }

    String finalResultString = newSdf.format(convertedDate);

    // logger_.warn(">> changeFormat() : [ return : {}]",
    // finalResultString);
    return finalResultString;
  }

  public static String convert(String requestDateTime_, String format1_, String format2_) {

    SimpleDateFormat sdf = new SimpleDateFormat(format1_);
    String returnVal = null;

    if (StringUtils.isNotBlank(requestDateTime_)) {
      Date date = null;
      try {
        date = sdf.parse(requestDateTime_);
      } catch (ParseException e) {
        // System.out
        // .println("INVALID DATE FORMAT PROVIDED FOR PARSING DATE
        // OBJECT {DATE : "
        // + requestDateTime_
        // + " FORMAT1 : "
        // + format1_
        // + " FORMAT2 : " + format2_ + "}");
        return null;
      }
      try {
        sdf = new SimpleDateFormat(format2_);
        returnVal = sdf.format(date);
      } catch (Exception e) {
        // System.out
        // .println("INVALID DATE FORMAT PROVIDED FOR PARSING DATE
        // OBJECT {DATE : "
        // + requestDateTime_
        // + " FORMAT1 : "
        // + format1_
        // + " FORMAT2 : " + format2_ + "}");
        return null;
      }

      return returnVal;
    }

    return null;
  }

  public static String convertLongToDate(String longValue) {
    SimpleDateFormat sdf = null;

    try {
      Date date = new Date(Long.valueOf(longValue));
      if (StringUtils.isNotBlank(GlobalParams.MAXIQ_STD_DT_FORMAT) && date != null) {
        sdf = new SimpleDateFormat(GlobalParams.MAXIQ_STD_DT_FORMAT);
        String format2 = sdf.format(date);
        return format2;
      }
      return longValue;
    } catch (Exception e) {
      return longValue;
    }
  }

  private static List<SimpleDateFormat> dateFormats =
      new ArrayList<SimpleDateFormat>() {
        private static final long serialVersionUID = 1L;

        {
          List<String> dels = new ArrayList<String>();
          dels.add(".");
          dels.add("-");
          dels.add(":");
          dels.add("/");
          dels.add("\\");

          for (String del : dels) {
            for (String format : dateFormatsInHashSet) {
              add(new SimpleDateFormat(StringUtils.replace(format, "#", del)));
            }
          }
        }
      };

  private static List<SimpleDateFormat> timeStampFormats =
      new ArrayList<SimpleDateFormat>() {
        private static final long serialVersionUID = 1L;

        {
          List<String> dels = new ArrayList<String>();
          dels.add(".");
          dels.add("-");
          //			dels.add(":");
          dels.add("/");
          dels.add("\\");

          for (String del : dels) {
            for (String format : dateFormatsInHashSet) {
              add(new SimpleDateFormat(StringUtils.replace(format + " hh:mm:ss a", "#", del)));
              add(new SimpleDateFormat(StringUtils.replace(format + " HH:mm:ss", "#", del)));
              add(new SimpleDateFormat(StringUtils.replace(format + " hh:mm:ss.S a", "#", del)));
              add(new SimpleDateFormat(StringUtils.replace(format + " HH:mm:ss.S", "#", del)));
            }
          }
        }
      };

  public static Boolean checkIfFormatIsValid(String inputFormat, FieldDataTypes type) {
    HashSet<String> allFormats = new HashSet<String>();
    List<String> dels = new ArrayList<String>();
    dels.add(".");
    dels.add("-");
    dels.add(":");
    dels.add("/");
    dels.add("\\");

    if (StringUtils.isNotBlank(inputFormat)) {
      if (type.equals(FieldDataTypes.DATE)) {
        for (String del : dels) {
          for (String format : dateFormatsInHashSet) {
            allFormats.add(StringUtils.replace(format, "#", del));
          }
        }
        return allFormats.contains(inputFormat);
      } else if (type.equals(FieldDataTypes.TIMESTAMP)) {
        for (String del : dels) {
          if (del.equals(":")) {
            continue;
          }
          for (String format : dateFormatsInHashSet) {
            allFormats.add(StringUtils.replace(format + " hh:mm:ss a", "#", del));
            allFormats.add(StringUtils.replace(format + " HH:mm:ss", "#", del));
            allFormats.add(StringUtils.replace(format + " hh:mm:ss.S a", "#", del));
            allFormats.add(StringUtils.replace(format + " HH:mm:ss.S", "#", del));
          }
        }
        return allFormats.contains(inputFormat);
      }
    }
    return false;
  }

  /**
   * Convert String with various formats into java.util.Date
   *
   * @param input Date as a string
   * @return java.util.Date object if input string is parsed successfully else returns null
   */
  public static Date convertToDate(String input) {
    Date date = null;
    if (null == input) {
      return null;
    }
    for (SimpleDateFormat format : dateFormats) {
      try {
        format.setLenient(false);
        date = format.parse(input);
      } catch (ParseException e) {
        // Shhh.. try other formats
      }
      if (date != null) {
        break;
      }
    }

    return date;
  }

  public static boolean checkIfValidDate(String input) {
    Date date = null;
    if (null == input) {
      return false;
    }
    for (SimpleDateFormat format : dateFormats) {
      try {
        format.setLenient(false);
        date = format.parse(input);
      } catch (ParseException e) {
        // Shhh.. try other formats
      }
      if (date != null) {
        return true;
      }
    }

    return false;
  }

  public static boolean checkIfValidDate(String input, String dateFormat) {
    Date date = null;
    if (null == input) {
      return false;
    }
    try {
      DateFormat format = new SimpleDateFormat(dateFormat);
      format.setLenient(false);
      date = format.parse(input);
    } catch (ParseException e) {
      // Shhh.. try other formats
    }
    if (date != null) {
      return true;
    }

    return false;
  }

  /**
   * Check if the given string is any date or not. If it is a date, return the format
   *
   * @param input Date as a string
   * @return String Format of the date found in input
   */
  public static List<String> findMyDateFormat(String input) {

    List<String> suitableFormats = new ArrayList<String>();

    if (StringUtils.isNotBlank(input)) {

      int inputLength = input.length();
      suitableFormats = getSuitableFormats(input, suitableFormats, inputLength, dateFormats);
    }

    return suitableFormats;
  }

  public static List<String> findMyTimeStampFormat(String input) {

    List<String> suitableFormats = new ArrayList<String>();

    if (StringUtils.isNotBlank(input)) {
      int inputLength = input.length();
      for (String amPm : amPmFormat) {
        if (input.contains(amPm)) {
          inputLength--;
          break;
        }
      }

      suitableFormats = getSuitableFormats(input, suitableFormats, inputLength, timeStampFormats);
    }

    return suitableFormats;
  }

  public static List<String> findMyDateAndTSFormat(String input, FieldDataTypes type) {

    List<String> suitableFormats = new ArrayList<String>();

    if (StringUtils.isNotBlank(input) && null != type) {
      int inputLength = input.length();
      if (StringUtils.equalsIgnoreCase(FieldDataTypes.DATE.name(), type.name())) {
        suitableFormats = getSuitableFormats(input, suitableFormats, inputLength, dateFormats);
        return suitableFormats;
      } else if (StringUtils.equalsIgnoreCase(FieldDataTypes.TIMESTAMP.name(), type.name())) {
        for (String amPm : amPmFormat) {
          if (input.contains(amPm)) {
            inputLength--;
            break;
          }
        }
        suitableFormats = getSuitableFormats(input, suitableFormats, inputLength, timeStampFormats);
      }
    }
    return suitableFormats;
  }

  private static List<String> getSuitableFormats(
      String input,
      List<String> suitableFormats,
      int inputLength,
      List<SimpleDateFormat> reqFormat) {

    for (SimpleDateFormat format : reqFormat) {
      if (validateFormat(format, input)) {
        if (format.toPattern().length() == inputLength) {
          suitableFormats.add(format.toPattern());
        }
      }
    }
    return suitableFormats;
  }

  public static Boolean validateFormat(DateFormat format, String input) {
    try {

      format.setLenient(false);
      Date date = format.parse(input);

      return true;
    } catch (ParseException e) {
      return false;
    }
  }

  public static boolean formatChecker(String date, String format) {

    String formatedDate;
    SimpleDateFormat sdf = null;

    try {
      sdf = new SimpleDateFormat(format);
      formatedDate = sdf.format(sdf.parse(date));
      if (StringUtils.equalsIgnoreCase(date, formatedDate)) {
        return true;
      }

    } catch (ParseException e) {
      e.printStackTrace();
      return false;
    }

    return false;
  }

  public static Boolean checkIfDateOrTSFormatIsValid(String currentFormat, FieldDataTypes type) {
    if (null != currentFormat && null != type) {
      return DateUtils.checkIfFormatIsValid(currentFormat, type);
    }
    return false;
  }

  @Deprecated
  public static Boolean checkIfDateFormatIsValid(String currentFormat) {

    if (null != currentFormat) {
      try {
        if (DateUtils.checkIfFormatIsValid(currentFormat, FieldDataTypes.DATE)) {
          return true;
        }
        return false;
      } catch (Exception e) {
        return false;
      }
    }
    return false;
  }

  @Deprecated
  public static Boolean checkIfTimeStampFormatIsValid(String currentFormat) {

    if (null != currentFormat) {
      try {
        if (DateUtils.checkIfFormatIsValid(currentFormat, FieldDataTypes.TIMESTAMP)) {
          return true;
        }
        return false;
      } catch (Exception e) {
        return false;
      }
    }
    return false;
  }

  public static String longToDate(Long epoch) {

    if (null == epoch) {
      return null;
    }

    String dateString = null;
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT8);

    Date date = new Date(epoch);

    //		TimeZone timeZone = TimeZone.getTimeZone(GlobalParams.MAXIQ_STD_IST_FORMAT);

    //		sdf.setTimeZone(timeZone);

    dateString = sdf.format(date);

    return dateString;
  }

  public static String getDateyyyyMMdd() {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    return dateFormat.format(new Date());
  }

  public static Timestamp getCurrentTimestamp() {
    Date date = new Date();
    return (new Timestamp(date.getTime()));
  }

  public static Timestamp getTimeStampFromMilliseconds(long currentDateTime) {
    Date currentDate = new Date(currentDateTime);
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return Timestamp.valueOf(dateFormat.format(currentDate));
  }

  public static Timestamp convertStringToTimestamp(String str_date, String format) {
    try {
      DateFormat formatter;
      formatter = new SimpleDateFormat(format == null ? "yyyy-MM-dd HH:mm:ss" : format);
      //			formatter.setTimeZone(TimeZone.getTimeZone(GlobalParams.MAXIQ_STD_IST_FORMAT));
      // you can change format of date
      Date date = formatter.parse(str_date);
      java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

      return timeStampDate;
    } catch (ParseException e) {
      return null;
    }
  }

  public static String convertTimeStampToDateWithFormat(String timestamp, String format) {
    String convertedDateWithFormat = "";
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    Date date = null;
    try {
      date = sdf.parse(timestamp);
    } catch (ParseException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    DateFormat df = new SimpleDateFormat(format);
    convertedDateWithFormat = df.format(date);
    return convertedDateWithFormat;
  }

  public static Date convertStringToDateWithFormat(String input, String formatInString) {
    Date date = null;
    if (null == input) {
      return null;
    }
    if (null == formatInString) {
      formatInString = GlobalParams.MAXIQ_STD_DF_DATE_FORMAT;
    }
    SimpleDateFormat format = new SimpleDateFormat(formatInString);

    try {
      format.setLenient(false);
      date = format.parse(input);
    } catch (ParseException e) {
      date = null;
    }

    return date;
  }

  public static java.sql.Date gettingSampleDateFormatForSqlDateOrTimeStamp(Object inputValue) {

    try {
      if (null != inputValue) {
        String format = "";
        if (StringUtils.contains(inputValue.toString(), ":")) {
          format = GlobalParams.MAXIQ_STD_DF_TS_FORMAT;
        } else {
          format = GlobalParams.MAXIQ_STD_DF_DATE_FORMAT;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return new java.sql.Date(simpleDateFormat.parse(inputValue.toString()).getTime());
      } else {
        return null;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static Boolean checkIfFormatIsParsable(String format) {
    if (null != format && format.length() > 0) {
      try {
        SimpleDateFormat datetry = new SimpleDateFormat(format);
        Date d1 = new Date();
        String format2 = datetry.format(d1);
        return true;
      } catch (Exception e) {
        return false;
      }
    }
    return false;
  }

  public static String convertSparkFormatDateToGlobalParamDate(String paramType, String val)
      throws ParseException {
    if ((null == paramType || paramType.equalsIgnoreCase(GENERAL_CONSTANTS.NULL))
        || (null == val || val.equalsIgnoreCase(GENERAL_CONSTANTS.NULL))) {
      return null;
    }

    if (GenericConstants.APP_COMBINER_TYPES.TIMESTAMP.equalsIgnoreCase(paramType)) {
      SimpleDateFormat spf = new SimpleDateFormat(GlobalParams.MAXIQ_STD_DF_TS_FORMAT);
      Date newDate = spf.parse(val);
      spf = new SimpleDateFormat(GlobalParams.GLOBAL_PARAM_TIMESTAMP_FORMAT);
      val = spf.format(newDate);
    } else if (GenericConstants.APP_COMBINER_TYPES.DATE.equalsIgnoreCase(paramType)) {
      SimpleDateFormat spf = new SimpleDateFormat(GlobalParams.MAXIQ_STD_DF_DATE_FORMAT);
      Date newDate = spf.parse(val);
      spf = new SimpleDateFormat(GlobalParams.GLOBAL_PARAM_DATE_FORMAT);
      val = spf.format(newDate);
    }
    return val;
  }
}

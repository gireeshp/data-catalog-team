package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import java.text.DecimalFormat;
import java.text.ParseException;

/** Created by shivanand on 10/16/2015. */
public class DoubleUtils {
  public static String convertDoubleToString(Double doubleVal) {
    DecimalFormat decimalFormat = new DecimalFormat("#");
    decimalFormat.setMaximumFractionDigits(4);
    return decimalFormat.format(doubleVal);
  }

  public static String convertDoubleToString(String doubleVal) {
    return convertDoubleToString(Double.valueOf(doubleVal));
  }
}

package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * List related utilities will be present in the class.
 *
 * @author shiva
 */
public class ListUtils {
  /**
   * Making the constructor default to avoid initiating this class. The class only contains
   * utilities and should be accessed in the static way
   */
  private ListUtils() {}

  /**
   * Converts the Input list into the delimited string.
   *
   * @param list
   * @param delimter
   * @return
   */
  public static String convertListToString(List<String> list, String delimter) {
    String str = "";

    if (null != list && list.size() > 0) {
      for (String string : list) {
        str += string + delimter;
      }
    }
    return str;
  }

  /**
   * Takes the delimited string and the delimiter as the input and returns the List of strings
   *
   * @param input
   * @param delimiter
   * @return
   */
  public static List<String> convertStringToList(
      String input, String delimiter, String attachString) {
    List<String> list = new ArrayList<>();
    if (null != input) {
      String[] str = StringUtils.splitPreserveAllTokens(input, delimiter);

      putArrayIntoList(str, list, attachString);
    }
    return list;
  }

  /**
   * Will put the values of the String array into the list provided.
   *
   * @param str
   * @param list
   * @param attachString
   */
  public static void putArrayIntoList(String[] str, List<String> list, String attachString) {
    if (null != str && str.length > 0) {
      for (String secondString : str) {
        list.add(attachString + StringUtils.trim(secondString));
      }
    }
  }

  public static <T> List<T> createListOfType(T... t) {
    List<T> list = new ArrayList<>();
    for (T t2 : t) {
      list.add(t2);
    }
    return list;
  }

  /**
   * @param list
   * @param sourceClass
   * @param fieldName
   * @return
   * @throws NoSuchMethodException
   * @throws SecurityException
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   * @throws InvocationTargetException
   */
  public static <T, U> List<T> createListOfSpecifiedType(
      List<U> list, Class<U> sourceClass, Class<T> destinationClass, String fieldName)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
          IllegalArgumentException, InvocationTargetException {
    char[] charArray = fieldName.toCharArray();
    char charAt = charArray[0];
    char upperCase = Character.toUpperCase(charAt);
    charArray[0] = upperCase;
    fieldName = String.valueOf(charArray);
    Method method = sourceClass.getMethod("get" + fieldName);
    List<T> resultList = new LinkedList<T>();
    for (U u : list) {
      resultList.add((T) method.invoke(u));
    }
    return resultList;
  }
}

package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * Serialize and deserialize the object into JSON string
 *
 * @author shiva
 */
public class ObjectSerializationHandler {
  /*	private static final Logger logger = LoggerFactory
  			.getLogger(ObjectSerializationHandler.class);
  */
  /**
   * This will create the json string for the input object
   *
   * @param o
   * @return
   */
  public static String toString(Object o) {
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " : >> toString()" + ParamUtils.getString(o));
    */
    ObjectMapper mapper = new ObjectMapper();

    //		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,
    //				false);
    //

    try {
      String writeValueAsString = mapper.writeValueAsString(o);

      /*			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()" + ParamUtils.getString(writeValueAsString));
      */ return writeValueAsString;
    } catch (JsonGenerationException e) {
      /*			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()");
      */ e.printStackTrace();
    } catch (JsonMappingException e) {
      /*			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()");
      */ e.printStackTrace();
    } catch (IOException e) {
      /*			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()");
      */ e.printStackTrace();
    }
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " : << toString()");
    */ return "";
  }
  /**
   * This method is used to store date format correctly not in long format
   *
   * @param o
   * @return
   */
  public static String toStringForRunQuery(Object o) {
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " : >> toString()" + ParamUtils.getString(o));
    */
    ObjectMapper mapper = new ObjectMapper();

    mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    //				mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,
    //						false);
    //

    try {
      String writeValueAsString = mapper.writeValueAsString(o);

      /*			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()" + ParamUtils.getString(writeValueAsString));
      */ return writeValueAsString;
    } catch (JsonGenerationException e) {
      /*			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()");
      */ e.printStackTrace();
    } catch (JsonMappingException e) {
      /*			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()");
      */ e.printStackTrace();
    } catch (IOException e) {
      /*			logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toString()");
      */ e.printStackTrace();
    }
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " : << toString()");
    */ return "";
  }

  /** This will create the object back from the json string. */
  public static Object toObject(String str, Class<?> className) {
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " : >> toObject()" + ParamUtils.getString(str, className));
    */
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

    mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

    mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

    mapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);

    mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

    mapper.setSerializationInclusion(Include.NON_NULL);

    try {
      /*			logger.debug(LoggerConstants.LOG_MAXIQWEB
      					+ " : << toObject()",
      					mapper.readValue(str, className));
      */ return mapper.readValue(str, className);

    } catch (Exception e) {
      str = "\"" + str + "\"";
      try {

        /*				logger.debug(LoggerConstants.LOG_MAXIQWEB
        						+ " : << toObject()",
        						mapper.readValue(str, className));
        */ return mapper.readValue(str, className);
      } catch (Exception e1) {

        // System.out.println("STR: " + str + "\nCLASS: " + className);
        /*				logger.info(LoggerConstants.LOG_MAXIQWEB + "STR: " + str
        						+ "\nCLASS: " + className);
        				logger.warn(LoggerConstants.LOG_MAXIQWEB + e1);
        				logger.debug(LoggerConstants.LOG_MAXIQWEB
        						+ " : << toObject()");
        */
        //e1.printStackTrace();
      }
    }
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " : << toObject()");
    */ return null;
  }

  public static <T> List<T> toObjectList(String str, Class className, T typdef)
      throws JsonParseException, JsonMappingException, IOException {
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " >> toObjectList()" + ParamUtils.getString(str,className,typdef));
    */
    List<T> list = null;
    ObjectMapper mapper = new ObjectMapper();
    TypeFactory factory = TypeFactory.defaultInstance();

    list = mapper.readValue(str, factory.constructCollectionType(ArrayList.class, className));
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " << List<T> toObjectList()" + ParamUtils.getString(list));
    */ return list;
  }

  public static Map convertJsonToMap(String json) throws IOException {
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " >> convertJsonToMap()" + ParamUtils.getString(json));
    */
    Map<String, String> map = new HashMap<String, String>();
    ObjectMapper mapper = new ObjectMapper();

    mapper.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

    mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

    mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

    mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

    mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " << convertJsonToMap()" + ParamUtils.getString(json));
    */ return mapper.readValue(json, new TypeReference<HashMap>() {});
  }

  public static Map convertLoggerStringToMap(String loggerStringJson) {
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " >> convertLoggerStringToMap()" + ParamUtils.getString(loggerStringJson));
    */
    Map<String, String> loggerStingToJson =
        (Map<String, String>) ObjectSerializationHandler.toObject(loggerStringJson, Map.class);

    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB
    				+ " << convertLoggerStringToMap()" + ParamUtils.getString(loggerStingToJson));
    */ return loggerStingToJson;
  }

  public static String convertLoggerMapToString(Map<String, String> loggerJson) {
    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> convertLoggerMapToString()" + ParamUtils.getString(loggerJson));
     */
    String loggerJsonToString = ObjectSerializationHandler.toString(loggerJson);

    /*		logger.debug(LoggerConstants.LOG_MAXIQWEB + " << convertLoggerMapToString()" + ParamUtils.getString(loggerJsonToString));
     */ return loggerJsonToString;
  }
}

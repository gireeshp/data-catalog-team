package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * This class will generate Random Values
 *
 * @author shiva
 */
public class RandomUtility {

  public static String getRandomString(int length) {
    return RandomStringUtils.randomAlphabetic(length);
  }

  public static int getRandomInteger(int higherLimit) {
    Random random = new Random();
    return random.nextInt(higherLimit);
  }
}

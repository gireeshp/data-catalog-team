package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import java.util.Date;

public class ReverseTimeStamp {

  private static Date dt;

  static {
    dt = new Date(2050, 1, 1);
  }

  public static Long getCurrentTime() {
    return dt.getTime() - new Date().getTime();
  }
}

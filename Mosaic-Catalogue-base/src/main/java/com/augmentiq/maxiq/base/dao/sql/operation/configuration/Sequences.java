package com.augmentiq.maxiq.base.dao.sql.operation.configuration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.MySqlOperations;
import com.augmentiq.maxiq.base.dao.sql.operation.MysqlConnection;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;

public class Sequences {
  private static final Logger logger = LoggerFactory.getLogger(Sequences.class);

  public static final String UPLOAD_DIR_SEQ = "upload_dir_seq";
  public static final String EXECUTE_QUERIES = "execute_queries";

  public static final String CONFIGURATION = "configuration";
  public static final String AUTHORIZATION = "Authorization";
  public static final String BASIC = "Basic";
  public static final String ACCEPT = "application/*";
  public static final String ZEPPELIN_COMMON_PATH = "/api/notebook";

  public static final String HIVE_QUERY = "hiveQuery";
  public static final String MESSAGE_TRACKER = "MessageTracker";
  public static final String NOTEBOOKNAME = "notebookName";
  public static final String BODY = "body";
  public static final String NOTEBOOKJSON = "notebookJson";

  public static final String TIME_SERIES_COMPONENTS = "timeSeriesComponents";
  public static final String EXTERNALDATADETAILS = "externaldatadetails";

  public static final String TIME_SERIES_DATA = "timeSeriesData";
  public static final String DATA_PARAM_INSTANCE = "dataParamInstance";
  public static final String CONFIGURATIONVERSION = "configuration_version";
  public static final String DATA_REPOSITORY = "data_repository";
  public static final String DATA_SOURCE = "data_source";
  public static final String FIELD_MAPPING = "field_mapping";
  public static final String ADVANCED_VALIDATIONS = "advanced_validations";
  public static final String USERS = "users";
  public static final String USERS_LOGIN_HISTORY = "users_login_history";
  public static final String JOB_HANDLER = "job_handler";
  public static final String ERRORED_RECORDS = "errored_records";
  public static final String DATA_CLEANSER = "data_cleanser";

  public static final String DATA_REPO_INSTANCE = "datarepoinstance";
  public static final String TASK_DETAILS = "taskdetails";
  public static final String DATASOURCE_INSTANCE = "datasourceinstance";
  public static final String DATASOURCE_INSTANCE_HISTORY = "datasourceinstancehistory";
  public static final String DATASOURCE_SNAPSHOT_MASTER = "datasourcesnapshot_master";
  public static final String APP_GLOBAL_PARAM = "app_global_param";
  public static final String ZEPPELIN_CONFIGURE = "zeppelin_configure";
  public static final String MY_SOLUTIONS = "my_solutions";
  public static final String SOLUTION_OBJECTS = "solution_objects";
  public static final String DATA_REQUEST = "data_request";
  public static final String MY_COLLECTION = "my_collection";
  public static final String PERSONA_MASTER = "persona_master";
  public static final String APP_INSTANCE = "appinstance";
  public static final String JOB_INSTANCE = "jobinstance";
  public static final String JUPYTER = "jupyter";

  public static final String MAXIQ_FLOWS = "maxiq_flows";
  public static final String MAXIQ_APPS = "maxiq_apps";
  public static final String DATASOURCE_VIEW = "datasource_view";

  public static final String APP_OUTPUT_FILES = "app_output_files";
  public static final String PROJECTS = "projects";

  public static final String USER_OBJECT_MAPPING = "user_object_mapping";
  public static final String PROJECT_OBJECT_MAPPING = "project_object_mapping";
  public static final String USER_PROJECT_MAPPING = "user_project_mapping";
  public static final String GROUP_OBJECT_MAPPING = "group_object_mapping";
  public static final String GROUP_PROJECT_MAPPING = "group_project_mapping";
  public static final String SHARE_ACCESS_HISTORY = "share_access_history";

  public static final String SEQUENCE_TABLE = "sequence";
  public static final String SEQUENCE_FAMILY = "seq.table";
  public static final String SEQUENCE_ID = "seq.id";
  public static final String ID = "id";

  public static final String APPLICATION_USER = "application_user";
  public static final String MAXIQ_METADATA = "maxiq_metadata";

  public static final String MAXIQ_COMMONS = "maxiq_commons";

  public static final String APPVIEWINSTANCE = "appViewInstance";
  public static final String DATA_COLUMN = "data-column";
  public static final String CUSTOM_COMPONENT_INSTANCE = "customcomponentinstance";
  public static final String SOCIAL_APP_INSTANCE = "socialappinstance";
  public static final String INDEX_DEFINATIONS = "index_defination";
  public static final String SEARCH_QUERY_DEFINATION = "search_query_def";
  public static final String USER_GROUP = "usergroup";
  public static final String GROUP_LEVEL_USER = "group_level_user";

  public static final String USER_SUB_GROUP = "user_subgroup";
  public static final String QUEUE_MASTER = "queue_master";
  public static final String ACTIONS_MASTER = "actions_master";
  public static final String BLOCKED_URL_ACTION_MAPPINGS = "blocked_url_action_mappings";
  public static final String FLAVOUR_PERSONA_MAPPINGS = "flavour_persona_mappings";
  public static final String ROLE_MASTER = "role_master";
  public static final String ROLE_ACTION_MAPPINGS = "role_action_mappings";
  public static final String USER_GROUP_MAPPINGS = "user_group_mappings";
  public static final String USER_SUBGROUP_MAPPINGS = "user_subgroup_mappings";
  public static final String USER_ROLE_MAPPINGS = "user_role_mappings";
  public static final String USER_FEEDBACK = "user_feedback";
  public static final String SCHEDULED_APPLICATION_DETAILS = "scheduled_application_details";
  public static final String COMMUNICATION_DETAILS = "communication_details";
  public static final String FEEDBACK = "feedback";
  public static final String FEEDBACK_VOTING = "feedback_voting";
  public static final String BOOST = "boost";
  public static final String BOOST_MAPPING = "boost_mapping";
  public static final String BOOST_MAPPING_REDUCE = "boost_mapping_reduce";
  public static final String FILETYPEDS = "FileTypeDs";
  public static final String TRUE = "true";
  public static final String USER_CLIENT_MAPPING = "user_client_mapping";

  public static final String NAME = "name";
  public static final String EMAILID = "emailId";
  public static final String NEWPASSWORD = "newPassword";
  public static final String ENCODEDPASSWORD = "encodedPassword";
  
  public static final String HIS_PASSWORD = "his_password";
  public static final String OLDPASSWORD = "oldPassword";
  public static final String PASSWORD = "password";
  public static final String CONFIRMPASSWORD = "confirmPassword";
  public static final String SHA_HASH = "SHA-512";
  public static final String UTF_HASH = "UTF-8";
  public static final String RESULT = "RESULT";
  public static final String SOLUTION_ID = "solution_id";

  public static final String ALREADYVOTED = "alreadyVoted";
  public static final String NO_VOTE = "0";
  public static final String ACTIVITY_LOG = "activity_log";

  public static final String REQUEST = "REQUEST";
  public static final String MACHINE_LEARNING_MODELS = "machine_learning_models";
  public static final String GLOSSARY = "glossary_master";;

  public static final String LDAP_USER_GROUP_BINDING = "LdapUserGroupBinding";;

  public static final String TEMP_DATA = "tempData";

  public static final String NOTIFICATION = "notification";
  public static final String DATASOURCE_APPROVAL_MAPPER = "datasource_approval_mapper";
  public static final String DATASOURCE_SERVICE_APIMANAGER = "datasource_service_apimanager";
  public static final String CATEGORY_SUBCATEGORY = "category_subcategory_master";

  @Deprecated
  public static Long getNextId(String sequenceName) {
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " >> getNextId(String sequenceName)");

    Long nextValue = 0L;

    String sql = "select counter from sequence where tabename = '" + sequenceName + "'";

    try {
    	Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

      while (resultSet.next()) {
        nextValue = resultSet.getLong(1);
      }

      nextValue++;
      sql = "update sequence set counter=? where tabename=?";

      Map<String, Object> objectMap = new LinkedHashMap<String, Object>();
      objectMap.put("counter", nextValue);
      MySqlOperations.updateQuery(sql, objectMap);

    } catch (SQLException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getNextId(String sequenceName)");
      e.printStackTrace();
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getNextId(String sequenceName)");
      e.printStackTrace();
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getNextId(String sequenceName)");
    return nextValue;
  }

  public static Long getMaxId(String tableName, String columnName) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getMaxId()"
            + ParamUtils.getString(tableName, columnName));

    Long nextValue = 0L;

    String sql = "select max(" + columnName + ") from " + tableName;

    try (Connection connection = MysqlConnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql)) {

      while (resultSet.next()) {
        nextValue = resultSet.getLong(1);
      }

      nextValue++;

    } catch (SQLException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getMaxId(String sequenceName)");
      e.printStackTrace();
    } catch (SystemException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getMaxId(String sequenceName)");
      e.printStackTrace();
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " << getMaxId()" + ParamUtils.getString(nextValue));
    return nextValue;
  }
}

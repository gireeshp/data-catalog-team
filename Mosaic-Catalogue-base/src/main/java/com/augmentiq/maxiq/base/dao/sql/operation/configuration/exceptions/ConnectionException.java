package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions;

public class ConnectionException extends Exception {
  /** */
  private static final long serialVersionUID = 1L;

  public ConnectionException(String message, Throwable throwable) {
    super(message, throwable);
  }

  public ConnectionException(String message) {
    super(message);
  }
}

package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions;

public class EntityNameAlreadyExistsException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public EntityNameAlreadyExistsException() {
    super();
  }

  public EntityNameAlreadyExistsException(String msg) {
    super(msg);
  }

  public EntityNameAlreadyExistsException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

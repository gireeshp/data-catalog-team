package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions;

public class GroupNameAlreadyExistsException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public GroupNameAlreadyExistsException() {
    super();
  }

  public GroupNameAlreadyExistsException(String msg) {
    super(msg);
  }

  public GroupNameAlreadyExistsException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

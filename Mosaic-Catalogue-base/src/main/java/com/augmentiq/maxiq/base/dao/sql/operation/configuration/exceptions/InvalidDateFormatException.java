package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions;

public class InvalidDateFormatException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public InvalidDateFormatException() {
    super();
  }

  public InvalidDateFormatException(String msg) {
    super(msg);
  }

  public InvalidDateFormatException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

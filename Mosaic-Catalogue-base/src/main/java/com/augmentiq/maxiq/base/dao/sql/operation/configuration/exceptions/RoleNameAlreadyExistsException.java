package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions;

public class RoleNameAlreadyExistsException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public RoleNameAlreadyExistsException() {
    super();
  }

  public RoleNameAlreadyExistsException(String msg) {
    super(msg);
  }

  public RoleNameAlreadyExistsException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

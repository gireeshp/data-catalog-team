package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions;

public class SubgroupNameAlreadyExistsException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public SubgroupNameAlreadyExistsException() {
    super();
  }

  public SubgroupNameAlreadyExistsException(String msg) {
    super(msg);
  }

  public SubgroupNameAlreadyExistsException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

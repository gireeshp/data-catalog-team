package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions;

public class SystemException extends Exception {

  /** */
  private static final long serialVersionUID = 3700564391580133843L;

  public SystemException() {
    super();
  }

  public SystemException(String msg) {
    super(msg);
  }

  public SystemException(String msg, Throwable thr) {
    super(msg, thr);
  }
}

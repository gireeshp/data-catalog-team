package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler;

public class ExceptionMessages {
  public static final String CONF_FILE_NOT_FOUD = "Cannot find configuration file {0}";

  public static final String DATASKEYWORDS = "ERR_003";

  public static final String DATARKEYWORDS = "ERR_004";

  public static final String DATAAKEYWORDS = "ERR_005";

  public static final String DATANKEYWORDS = "ERR_006";

  public static final String KEYWORDS = "Field Name can not be a keyword :";

  public static final String BRACKET_MISSING = "ERR_007";

  public static final String HBASE_DB_SPRING_CONNECTION_FAILURE =
      "Unable to initialize SpringMongoFactory {0}. Error Message : {1}";

  public static final String HBASE_GENERIC_INSERTION_EXCEPTION =
      "Unable to insert Generic configuration into MongoDB. ParamType : {0} ParamId : {1}. Mongo ErrorMessage : {2}";

  public static final String HBASE_GENERIC_UPDATE_EXCEPTION =
      "Unable to update Generic configuration into MongoDB. ParamType : {0} ParamId : {1}. Mongo ErrorMessage : {2}";

  public static final String HBASE_GENERIC_FETCH_EXCEPTION = "ERR_008";

  public static final String DATA_SOURCE_IS_NOT_PRESENT = "ERR_009";

  public static final String FILE_READING_ERROR = "ERR_010";

  public static final String HIVE_TABLE_DROP_ERROR = "ERR_011";

  public static final String HIVE_TABLE_CREATE_ERROR = "ERR_12";

  public static final String HIVE_TABLE_DATA_LOAD_ERROR = "ERR_013";

  public static final String HIVE_TABLE_SELECT_ERROR = "ERR_014";

  public static final String TABLE_NAME_ANNOTATION_NOT_PRESENT = "ERR_015";

  public static final String ID_ANNOTATION_NOT_PRESENT = "ERR_016";

  public static final String HBASE_BEAN_CONVERTOR_ERROR = "ERR_017";

  public static final String HBASE_FETCH_ERROR = "ERR_018";

  public static final String DUPLICATE_NAME_EXISTS = "Same name already exist.";

  public static final String QUERY_ALREADY_EXISTS = "ERR_020";

  public static final String QUERY_SAVED = "Query saved successfully ";

  public static final String FIELDNAME_ALREADY_EXISTS = "Field by name {0} already exists";

  public static final String CUSTOM_COMPONENT_NAME_ERROR = "custom component is null or invalid";

  public static final String CUSTOM_COMPONENT_YAML_ZIP_ERROR = "Invalid yaml or zip file data";

  public static final String SOCIAL_APP_NAME_ERROR = "Social App is null or invalid";

  public static final String APP_NAME_ALREADY_EXISTS = "ERR_002";

  public static final String DATA_NAME_ALREADY_EXISTS = "ERR_022";

  public static final String USER_ALREADY_EXISTS = "User is already exists";

  public static final String SOMETHING_WRONG = "Something is going wrong";

  public static final String INVALID_EMAIL_PWD =
      "The email or password you entered is incorrect. You have  ";

  public static final String INVALID_EMAIL_PWD_COUNT_LEFT = " Attempt left ";

  public static final String USER_NOT_REGISTERED =
      "User is not registered with Maxiq. Please contact administrator.";

  public static final String LDAP_AUTH_FAILED = "LDAP Authentication failed.";

  public static final String INVALID_AUTH_METHOD =
      "Invalid authentication method. Please contact administrator.";

  public static final String AT_LEAST_ONE_MUST_BE_DEFAULT =
      "Boost not saved/updated. At least one boost must be default. Please make it default before saving.";

  public static final String BOOST_NOT_DELETED_BECAUSE_DEFAULT =
      "Boost not deleted. Default Boost can not be deleted. Please make it Non default before deletion.";

  public static final String RESET_PWD = "Please reset password.";

  public static final String INVALID_DATE_FORMAT = "Invalid date format {0}";

  public static final String READ_FILE_ERROR = "Unable to read file. Error {0}";

  public static final String SEARCH_FIELD_NOT_FOUND = "Search field {0} was not found in the map";

  public static final String APP_OUTPUT_NOT_PRESENT = "ERR_043";

  public static final String CUSTOM_INPUT_DUPLICATE = "Duplicate name exists in input fields";

  public static final String CUSTOM_OUTPUT_DUPLICATE = "Duplicate name exists in output fields";

  public static final String FIELDNAME_CANNOT_BE_BLANK = "{0} cannot be blank";

  public static final String REMOTE_FILE_PATH_INVALID = "Invalid Remote file path";

  public static final String SESSION_ALREADY_ACTIVE =
      "There is already another session active for same user.";

  public static final String LOGIN_MAX_TRY = "Maximum attempts exceeded. Account is locked.";
  public static final String USER_LOCK = "User is already locked.";

  public static final String DID_NOT_FIND_MATCHING_INST = "Didn't find matching instance";
  public static final String USER_NOT_ACTIVE = "User is not active";	
}

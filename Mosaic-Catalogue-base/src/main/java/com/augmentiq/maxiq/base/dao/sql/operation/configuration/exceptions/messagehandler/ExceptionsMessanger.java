package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.cache.support.Cache;

public class ExceptionsMessanger {
  private static final Logger logger = LoggerFactory.getLogger(ExceptionsMessanger.class);

  public static void throwException(Throwable throwable, String messageType, Object... objs)
      throws SystemException {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> throwException()"
            + ParamUtils.getString(throwable, messageType, objs));

    String errorMessage = ExceptionsMessanger.msg(Cache.getPropertyFromError(messageType), objs);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << throwException()"
            + ParamUtils.getString(throwable, messageType, objs));

    throw new SystemException(errorMessage, throwable);
  }

  public static String msg(String messageType, Object... objs) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> : msg()" + ParamUtils.getString(messageType, objs));

    MessageFormat format = new MessageFormat(messageType);

    String format2 = format.format(objs);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << String msg()" + ParamUtils.getString(format2));
    return format2;
  }
}

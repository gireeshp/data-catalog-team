package com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StackTraceReader {
  public static String stringFromStackTrace(Exception ex) {
    StringWriter errors = new StringWriter();
    ex.printStackTrace(new PrintWriter(errors));
    return errors.toString();
  }
}

package com.augmentiq.maxiq.base.dao.sql.operation.hbase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;

public class HbaseQueryConvertor {

  /**
   * Generates the Hbase filter for the given input Map. All the conditions are put into AND clause
   *
   * @param query
   * @return
   */
  private static final Logger logger = LoggerFactory.getLogger(HbaseQueryConvertor.class);

  public static FilterList getEqualsFilterQuery(Map<String, Object> query) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getEqualsFilterQuery()"
            + ParamUtils.getString(query));
    List<Filter> filters = new ArrayList<>();

    if (null != query) {
      for (Entry<String, Object> entry : query.entrySet()) {
        Filter fltr =
            new SingleColumnValueFilter(
                Bytes.toBytes(Sequences.DATA_COLUMN),
                Bytes.toBytes(entry.getKey()),
                CompareOp.EQUAL,
                HbaseUtility.serialize(entry.getValue()));

        filters.add(fltr);
      }
    }

    FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL, filters);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getEqualsFilterQuery()"
            + ParamUtils.getString(filter));
    return filter;
  }

  public static RowFilter getGreaterThanQuery(String value) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> getGreaterThanQuery()" + ParamUtils.getString(value));
    RowFilter filter = new RowFilter(CompareOp.GREATER, new BinaryComparator(Bytes.toBytes(value)));
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getGreaterThanQuery()"
            + ParamUtils.getString(filter));
    return filter;
  }

  public static FilterList createNullNotNullQuery(
      List<String> query, boolean nullQuery, Long baapJobInstId, String fieldName) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> createNullNotNullQuery()"
            + ParamUtils.getString(query, nullQuery, baapJobInstId, fieldName));

    List<Filter> filters = new ArrayList<Filter>();

    for (String entry : query) {
      if (nullQuery) {
        Filter fltr =
            new SingleColumnValueFilter(
                Bytes.toBytes(Sequences.DATA_COLUMN),
                Bytes.toBytes(entry),
                CompareOp.EQUAL,
                Bytes.toBytes("null"));
        filters.add(fltr);
      } else {
        Filter fltr =
            new SingleColumnValueFilter(
                Bytes.toBytes(Sequences.DATA_COLUMN),
                Bytes.toBytes(entry),
                CompareOp.NOT_EQUAL,
                Bytes.toBytes("null"));
        filters.add(fltr);
      }
    }

    Filter baap0Cond =
        new SingleColumnValueFilter(
            Bytes.toBytes(fieldName),
            Bytes.toBytes("1"),
            CompareOp.EQUAL,
            Bytes.toBytes(baapJobInstId.toString()));
    filters.add(baap0Cond);

    FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL, filters);
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getGreaterThanQuery()"
            + ParamUtils.getString(filter));
    return filter;
  }

  public static FilterList getCaseSenstiveStringMatch(Map<String, Object> query) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getCaseSenstiveStringMatch()"
            + ParamUtils.getString(query));
    List<Filter> filters = new ArrayList<>();

    if (null != query) {
      for (Entry<String, Object> entry : query.entrySet()) {
        Filter fltr =
            new SingleColumnValueFilter(
                Bytes.toBytes(Sequences.DATA_COLUMN),
                Bytes.toBytes(entry.getKey()),
                CompareOp.EQUAL,
                new SubstringComparator((String) entry.getValue()));
        filters.add(fltr);
      }
    }
    FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL, filters);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getCaseSenstiveStringMatch()"
            + ParamUtils.getString(filter));
    return filter;
  }
}

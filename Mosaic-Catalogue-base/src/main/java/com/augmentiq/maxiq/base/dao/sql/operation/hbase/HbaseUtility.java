package com.augmentiq.maxiq.base.dao.sql.operation.hbase;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.constant.maxiq.LoggerConstants;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ObjectSerializationHandler;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.ParamUtils;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.SystemException;
import com.augmentiq.maxiq.base.dao.sql.operation.configuration.exceptions.messagehandler.ExceptionsMessanger;
import com.augmentiq.maxiq.constant.configuration.enums.Id;
import com.augmentiq.maxiq.constant.configuration.enums.TableName;

/**
 * This contains the utilities required for Hbase processing
 *
 * @author shiva
 */
public class HbaseUtility {
  private static final Logger logger = LoggerFactory.getLogger(HbaseUtility.class);

  /**
   * Making the constructor private to avoid object creation as all the methods are static in this
   * class
   */
  private HbaseUtility() {}

  /**
   * Read the table name annotation and return the valid table name for the object
   *
   * @param object
   * @return
   * @throws SystemException
   */
  public static String getTableNameForObject(Object object) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getTableNameForObject()"
            + ParamUtils.getString(object));
    Class<?> entityClass = object.getClass();

    String tableNameForClass = getTableNameForClass(entityClass);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getTableNameForObject()"
            + ParamUtils.getString(tableNameForClass));
    return tableNameForClass;
  }

  /**
   * Return table name for class
   *
   * @param entityClass
   * @return
   * @throws SystemException
   */
  public static String getTableNameForClass(Class<?> entityClass) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getTableNameForClass()"
            + ParamUtils.getString(entityClass));

    TableName tableName = entityClass.getAnnotation(TableName.class);

    if (null != tableName) {
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB
              + " : << getTableNameForClass()"
              + ParamUtils.getString(entityClass));
      return tableName.tableName();
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getTableNameForClass()"
            + ParamUtils.getString(entityClass));

    ExceptionsMessanger.throwException(new SystemException(), "ERR_015", null);
    return null;
  }

  /**
   * Build object using Hbase Result object
   *
   * @param entityClass
   * @param result
   * @return
   * @throws InstantiationException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws NoSuchMethodException
   * @throws SecurityException
   * @throws IOException
   * @throws ClassNotFoundException
   * @throws SQLException
   * @throws NoSuchFieldException
   */
  @SuppressWarnings("unchecked")
  public static <T> T buildObject(Class<T> entityClass, ResultSet result)
      throws SQLException, ClassNotFoundException, IOException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> buildObject()"
            + ParamUtils.getString(entityClass, result));

    Map<String, Object> map = new HashMap<String, Object>();

    Field[] declaredFields = entityClass.getDeclaredFields();

    for (Field field : declaredFields) {

      String value = result.getString(field.getName());

      if (field.getType().equals(String.class)) {
        map.put(field.getName(), value);
      } else {
        Object val = deserialize(value, field.getType());

        map.put(field.getName(), val);
      }
    }

    Object bean = getObjectFromMap(map, entityClass);

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << buildObject()" + ParamUtils.getString((T) bean));

    return (T) bean;
  }

  public static List<String> getListOfFieldsFromResultSet(ResultSet result, String fieldName)
      throws SQLException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> getListOfFieldsFromResultSet()"
            + ParamUtils.getString(result, fieldName));

    List<String> res = new ArrayList<String>();

    while (result.next()) {
      String value = result.getString(fieldName);
      res.add(value);
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " << getListOfFieldsFromResultSet()"
            + ParamUtils.getString(res));
    return res;
  }

  @SuppressWarnings("unchecked")
  public static <T> T buildDistinctField(Class<T> entityClass, ResultSet result, String fieldName)
      throws SQLException, ClassNotFoundException, IOException {

    Map<String, Object> map = new HashMap<String, Object>();

    Field[] declaredFields = entityClass.getDeclaredFields();

    for (Field field : declaredFields) {

      if (field.getName().equals(fieldName)) {
        String value = result.getString(field.getName());
        if (field.getType().equals(String.class)) {
          map.put(field.getName(), value);
        } else {
          Object val = deserialize(value, field.getType());
          map.put(field.getName(), val);
        }
      }
    }
    Object bean = getObjectFromMap(map, entityClass);
    return (T) bean;
  }

  /**
   * Converts the object into bytes
   *
   * @param obj
   * @return
   */
  public static byte[] serialize(Object obj) {
    ObjectMapper mapper = new ObjectMapper();
    String value = "";
    try {
      value = mapper.writeValueAsString(obj);
    } catch (JsonGenerationException e) {
      e.printStackTrace();
    } catch (JsonMappingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return Bytes.toBytes(value);
  }

  /**
   * Builds the object of the Class type given with the Byte array
   *
   * @param bytes
   * @param classType
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  public static Object deserialize(String bytes, Class<?> classType) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> deserialize()"
            + ParamUtils.getString(bytes, classType));

    if (null != bytes && !StringUtils.equals("null", bytes)) {
      Object object = ObjectSerializationHandler.toObject(bytes, classType);
      logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << deserialize() : [return : {}]", object);

      return object;
    }
    logger.debug(LoggerConstants.LOG_MAXIQWEB + " : << deserialize()" + ParamUtils.getString(null));
    return null;
  }

  /**
   * Get Json map from Object
   *
   * @param object
   * @return
   */
  public static Map<String, Object> getMapForBean(Object object) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : >> : getMapForBean()" + ParamUtils.getString(object));

    Map<String, Object> hashmap = new HashMap<>();
    ObjectMapper mapper = new ObjectMapper();
    try {
      hashmap =
          mapper.readValue(
              ObjectSerializationHandler.toString(object),
              new TypeReference<Map<String, Object>>() {});
    } catch (JsonGenerationException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " : << : getMapForBean()" + ParamUtils.getString(null));
      e.printStackTrace();
    } catch (JsonMappingException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " : << : getMapForBean()" + ParamUtils.getString(null));
      e.printStackTrace();
    } catch (IOException e) {
      logger.warn(LoggerConstants.LOG_MAXIQWEB + e);
      logger.debug(
          LoggerConstants.LOG_MAXIQWEB + " : << : getMapForBean()" + ParamUtils.getString(null));
      e.printStackTrace();
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << : getMapForBean()" + ParamUtils.getString(hashmap));

    return hashmap;
  }

  /**
   * Get Bean for Object Map
   *
   * @param map
   * @param entityClass
   * @return
   * @throws ClassNotFoundException
   * @throws IOException
   */
  @SuppressWarnings("unchecked")
  private static <T> T getObjectFromMap(Map<String, Object> map, Class<T> entityClass)
      throws ClassNotFoundException, IOException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getObjectFromMap()"
            + ParamUtils.getString(map, entityClass));

    String json = ObjectSerializationHandler.toString(map);

    // logger.debug(LoggerConstants.LOG_MAXIQWEB+" : << getObjectFromMap()" +
    // ParamUtils.getString((T) ObjectSerializationHandler.toObject(json,
    // entityClass)));

    return (T) ObjectSerializationHandler.toObject(json, entityClass);
  }

  /**
   * Get Id annotations for the class
   *
   * @param cls
   * @return
   * @throws SystemException
   */
  public static String getIdAnnotationForClass(Class<?> cls) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> getIdAnnotationForClass()"
            + ParamUtils.getString(cls));

    for (Field field : cls.getDeclaredFields()) {

      String name = field.getName();
      Annotation[] annotations = field.getDeclaredAnnotations();

      if (null != annotations) {
        for (Annotation annotation : annotations) {
          if (annotation instanceof Id) {
            logger.debug(
                LoggerConstants.LOG_MAXIQWEB
                    + " : << getIdAnnotationForClass()"
                    + ParamUtils.getString(name));
            return name;
          }
        }
      }
    }

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << getIdAnnotationForClass()"
            + ParamUtils.getString(null));

    ExceptionsMessanger.throwException(new SystemException(), "ERR_016", null);
    return null;
  }

  public static String generateQuery(Class<?> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >>  generateQuery()"
            + ParamUtils.getString(entityClass, query));

    String tableName = HbaseUtility.getTableNameForClass(entityClass);

    String sql = "select * from " + tableName;
    if (null != query) {
      int counter = 0;
      for (Entry<String, Object> entry : query.entrySet()) {
        if (counter == 0) {
          sql += " where " + entry.getKey() + "=?";
        } else {
          sql += " and " + entry.getKey() + "=?";
        }
        counter++;
      }
    }

    // String sql = "select * from " + tableName + " where emailId = ?";

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : <<  generateQuery()" + ParamUtils.getString(sql));
    return sql;
  }

  public static PreparedStatement generateInsertQueryData(
      PreparedStatement statement, Object object) throws SystemException, SQLException {
    int counter = 1;

    Map<String, Object> map = null;

    map = HbaseUtility.getMapForBean(object);

    Set<String> set = map.keySet();

    for (String key : set) {

      Object obj = map.get(key);
      statement.setObject(
          counter,
          (obj instanceof String)
              ? obj + ""
              : (obj instanceof Enum)
                  ? String.valueOf(obj)
                  : ObjectSerializationHandler.toString(obj));
      counter++;
    }

    return statement;
  }

  public static PreparedStatement generateInsertQueryDataWithPK(
      String primaryKeyAutoIncrement, PreparedStatement statement, Object object)
      throws SystemException, SQLException {
    // if (null != query) {
    int counter = 1;

    Map<String, Object> map = null;

    map = HbaseUtility.getMapForBean(object);

    Set<String> set = map.keySet();

    for (String key : set) {
      if (!StringUtils.equals(key, primaryKeyAutoIncrement)) {
        Object obj = map.get(key);
        statement.setObject(
            counter,
            (obj instanceof String)
                ? obj + ""
                : (obj instanceof Enum)
                    ? String.valueOf(obj)
                    : ObjectSerializationHandler.toString(obj));
        counter++;
      }
    }
    return statement;
  }

  public static PreparedStatement generateQueryDataForInCondition(
      PreparedStatement statement, Map<String, List<Object>> query)
      throws SystemException, SQLException {

    if (null != query) {

      int counter = 1;
      for (Entry<String, List<Object>> entry : query.entrySet()) {
        List<Object> valuesInList = new ArrayList<Object>();
        valuesInList = entry.getValue();
        if (valuesInList.size() > 0) {
          for (int i = 0; i < valuesInList.size(); i++) {
            statement.setObject(counter, valuesInList.get(i));
            counter++;
          }
        }
      }
    }
    return statement;
  }

  public static PreparedStatement generateSqlStatmentQueryData(
      PreparedStatement statement, Map<String, Object> query) throws SystemException, SQLException {

    if (null != query && query.size() > 0) {
      int counter = 1;
      for (Entry<String, Object> entry : query.entrySet()) {
        statement.setObject(
            counter,
            (entry.getValue() instanceof String)
                ? entry.getValue() + ""
                : (entry.getValue() instanceof Enum)
                    ? String.valueOf(entry.getValue())
                    : ObjectSerializationHandler.toString(entry.getValue()));
        counter++;
      }
    }
    return statement;
  }

  public static String generateCountQuery(Class<?> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> generateCountQuery()"
            + ParamUtils.getString(entityClass, query));

    String tableName = HbaseUtility.getTableNameForClass(entityClass);

    String sql = "select count(1) from " + tableName;
    if (null != query) {
      int counter = 0;
      for (Entry<String, Object> entry : query.entrySet()) {
        if (counter == 0) {
          sql += " where " + entry.getKey() + "= ?";
        } else {
          sql += " and " + entry.getKey() + "= ?";
        }
        counter++;
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << generateCountQuery()" + ParamUtils.getString(sql));

    return sql;
  }

  public static String generateDeleteQuery(Class<?> entityClass, Map<String, Object> query)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> generateDeleteQuery()"
            + ParamUtils.getString(entityClass, query));

    String tableName = HbaseUtility.getTableNameForClass(entityClass);

    String sql = "delete from " + tableName;
    if (null != query) {
      int counter = 0;
      for (Entry<String, Object> entry : query.entrySet()) {
        if (counter == 0) {
          sql += " where " + entry.getKey() + "= ?";
        } else {
          sql += " and " + entry.getKey() + "= ?";
        }
        counter++;
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : << generateDeleteQuery()" + ParamUtils.getString(sql));
    return sql;
  }

  public static String generateDistinctQuery(
      Class<?> entityClass, Map<String, Object> query, String distinctOnFieldName)
      throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " >> generateDistinctQuery()"
            + ParamUtils.getString(entityClass, query, distinctOnFieldName));

    String tableName = HbaseUtility.getTableNameForClass(entityClass);

    String sql = "select distinct(" + distinctOnFieldName + ") d from " + tableName;
    if (null != query) {
      int counter = 0;
      for (Entry<String, Object> entry : query.entrySet()) {
        if (counter == 0) {
          sql += " where " + entry.getKey() + "=?";
        } else {
          sql += " and " + entry.getKey() + "=?";
        }
        counter++;
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " << generateDistinctQuery()" + ParamUtils.getString(sql));
    return sql;
  }

  /**
   * @param entityClass
   * @param query
   * @return select query based on in condition
   * @throws SystemException
   */
  public static String generateQueryForInCondition(
      Class<?> entityClass, Map<String, List<Object>> query) throws SystemException {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >>  generateQuery()"
            + ParamUtils.getString(entityClass, query));

    String tableName = HbaseUtility.getTableNameForClass(entityClass);

    String sql = "select * from " + tableName;
    if (null != query) {
      int counter = 0;
      for (Entry<String, List<Object>> entry : query.entrySet()) {
        if (counter == 0) {
          List<Object> valuesInList = new ArrayList<Object>();
          valuesInList = entry.getValue();
          if (valuesInList.size() > 0) {
            sql += " where " + entry.getKey() + " in(";
            for (Object obj : valuesInList) {
              sql += "?,";
            }
            sql = StringUtils.substring(sql, 0, sql.length() - 1);
            sql += ")";
          }
        } else {
          List<Object> valuesInList = new ArrayList<Object>();
          valuesInList = entry.getValue();
          if (valuesInList.size() > 0) {
            sql += " and " + entry.getKey() + " in(";
            for (Object obj : valuesInList) {
              sql += "?,";
            }
            sql = StringUtils.substring(sql, 0, sql.length() - 1);
            sql += ")";
          }
        }
        counter++;
      }
    }
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB + " : <<  generateQuery()" + ParamUtils.getString(sql));
    return sql;
  }

  public static List<Map<String, Object>> getMapOfFieldsFromResultSet(
      ResultSet resultSet, String[] params) throws SQLException {
    List<Map<String, Object>> list = new LinkedList<>();

    if (null == params || params.length == 0) {
      int columnCount = resultSet.getMetaData().getColumnCount();
      params = new String[columnCount];
      for (int i = 1; i <= columnCount; i++)
        params[i - 1] = resultSet.getMetaData().getColumnLabel(i);
    }

    while (resultSet.next()) {
      Map<String, Object> map = new HashMap<String, Object>();
      for (String key : params) {
        map.put(key, resultSet.getString(key));
      }

      list.add(map);
    }
    return list;
  }

  public static String generateInsertQuery(Object object, String tableName) {
    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : >> generateInsertQuery()"
            + ParamUtils.getString(object, tableName));

    Map<String, Object> map = null;

    map = HbaseUtility.getMapForBean(object);

    Set<String> set = map.keySet();

    String columns = "";
    String values = "";
    int i = 0;
    for (String key : set) {

      Object obj = map.get(key);
      String value = null;
      if (obj instanceof String) {
        value = obj + "";
      } else if (obj instanceof Enum) {
        value = String.valueOf(obj);
      } else {
        value = ObjectSerializationHandler.toString(map.get(key));
      }
      boolean skipBraces = false;
      if (obj instanceof Boolean) {
        skipBraces = true;
      } else {
        skipBraces = false;
      }
      value = StringUtils.replace(value, "'", "''");

      if (i == 0) {
        columns += key;
        if (skipBraces) values += "?";
        else values += "?";
      } else {
        columns += "," + key;
        if (skipBraces) values += ",?" /* + value */;
        else values += ",?" /* + value + "'" */;
      }
      i++;
    }

    String insertStatement =
        "insert into " + tableName + " (" + columns + ") values (" + values + ")";

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << generateInsertQuery()"
            + ParamUtils.getString(insertStatement));

    return insertStatement;
  }

  public static String generateInsertQueryForAutoIncrementedPrimaryKey(
      String primaryKeyAutoIncrement, String tableName, Map<String, Object> map) {

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << generateInsertQueryForAutoIncrementedPrimaryKey()"
            + ParamUtils.getString(primaryKeyAutoIncrement, tableName, map));

    Set<String> set = map.keySet();

    String columns = "";
    String values = "";
    int i = 0;
    for (String key : set) {

      if (!StringUtils.equals(key, primaryKeyAutoIncrement)) {

        String value = "?";

        if (i == 0) {
          columns += key;
          values += value;
        } else {
          columns += "," + key;
          values += "," + value;
        }
        i++;
      }
    }
    String insertStatement =
        "insert into " + tableName + " (" + columns + ") values (" + values + ")";

    logger.debug(
        LoggerConstants.LOG_MAXIQWEB
            + " : << generateInsertQueryForAutoIncrementedPrimaryKey()"
            + ParamUtils.getString(insertStatement));

    return insertStatement;
  }

  /**
   * @param object
   * @param query
   * @return
   * @throws SystemException
   */
  public static String generateUpdateQuery(Object object, Map<String, Object> query)
      throws SystemException {
    String tableName = HbaseUtility.getTableNameForObject(object);

    Map<String, Object> map = null;
    map = HbaseUtility.getMapForBean(object);
    Set<String> set = map.keySet();
    String keyValue = "";
    int i = 0;
    for (String key : set) {
      Object obj = map.get(key);
      String value = "";
      if (obj instanceof String) {
        value = obj + "";
      } else if (obj instanceof Enum) {
        value = String.valueOf(obj);
      } else {
        value = ObjectSerializationHandler.toString(map.get(key));
      }

      value = StringUtils.replace(value, "'", "''");

      if (!StringUtils.isEmpty(value) && !StringUtils.equalsIgnoreCase(value, "null")) {
        if (i == 0) {
          keyValue += "" + key + "=" + "'" + value + "'";
        } else {
          keyValue += ", " + key + "=" + "'" + value + "'";
        }
        i++;
      }
    }

    String updateStatement = "update " + tableName + " set " + keyValue + " where ";
    int j = 0;
    for (Map.Entry<String, Object> entry : query.entrySet()) {
      if (j == 0) {
        updateStatement += entry.getKey() + "= ?";
      } else {
        updateStatement += ", " + entry.getKey() + "= ?";
      }
    }
    return updateStatement;
  }
}

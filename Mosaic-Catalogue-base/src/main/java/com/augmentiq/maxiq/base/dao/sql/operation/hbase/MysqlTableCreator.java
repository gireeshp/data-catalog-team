package com.augmentiq.maxiq.base.dao.sql.operation.hbase;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;

@Deprecated
public class MysqlTableCreator {

  public static void createTable(String tableName, List<String> fields)
      throws MasterNotRunningException, ZooKeeperConnectionException, IOException {

    String sql = "create table " + tableName + " (";
    if (null != fields && fields.size() > 0) {
      for (String field : fields) {
        sql += field + " varchar(40), ";
      }
    }

    sql = sql.substring(0, sql.length() - 2);

    sql += ")";

    /*System.out.println(sql);*/

    //		try (Connection connection = MysqlConnection.getConnection();
    //				Statement statement = connection.createStatement();) {
    //			statement.executeUpdate(sql);
    //		} catch (Exception e) {
    //			e.printStackTrace();
    //		}

  }
}

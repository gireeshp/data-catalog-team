package com.augmentiq.maxiq.cache.support;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.augmentiq.maxiq.constant.cache.CacheConstants;

/**
 * *
 *
 * @author shiva This is Singleton Class which will load all the configuration from flat file during
 *     start up. It exposes two methods, one to check if the key exists and the other one to get the
 *     cache value.
 */
public class Cache {
  private static Properties config = new Properties();
  private static Properties error = new Properties();
  public static String MAXIQ_HOME = null;

  private static final Logger logger_ = LoggerFactory.getLogger(Cache.class);

  private static volatile Cache cache;

  static {
    String base = System.getenv().get(CacheConstants.MAXIQ_HOME);
    //		String base = "/Application/navinmaxiq";//TODO

    System.out.println("###MAXIQ_HOME" + base);

    if (StringUtils.isBlank(base)) {
      logger_.error("Base Path $MAXIQ_HOME {} not configured ", base);
    }

    MAXIQ_HOME = base;

    String basePath = base + "/conf/config.properties";
    String errorPath = base + "/conf/error_logs.properties";

    try (FileInputStream fis = FileUtils.openInputStream(new File(basePath));
        FileInputStream fisForError = FileUtils.openInputStream(new File(errorPath)); ) {

      config.load(fis);
      error.load(fisForError);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Cache getInstance() {
    if (null == cache) {
      synchronized (Cache.class) {
        if (null == cache) {
          cache = new Cache();
        }
      }
    }

    return cache;
  }

  /**
   * * Gets the cache value for the key
   *
   * @param key
   * @return
   */
  public static String getProperty(String key) {
    return config.getProperty(key);
  }

  public static String getPropertyFromError(String key) {
    return error.getProperty(key);
  }

  /**
   * * Checks if the key exists in the cache. Returns true if the key is present.
   *
   * @param key
   * @return
   */
  public static boolean containsKey(String key) {
    return Cache.config.containsKey(key);
  }

  public static void reloadFile(BufferedReader reader) throws IOException {
    config.load(reader);
  }

  public static void reloadErrorFile(BufferedReader reader) throws IOException {
    error.load(reader);
  }

  public static void reloadFile(List<String> list) throws IOException {
    StringBuilder buffer = new StringBuilder();
    for (String current : list) {
      buffer.append(current).append("\n");
    }

    BufferedReader br = new BufferedReader(new StringReader(buffer.toString()));
    config.load(br);
  }

  public static void reloadErrorFile(List<String> list) throws IOException {
    StringBuilder buffer = new StringBuilder();
    for (String current : list) {
      buffer.append(current).append("\n");
    }

    BufferedReader br = new BufferedReader(new StringReader(buffer.toString()));
    error.load(br);
  }
}

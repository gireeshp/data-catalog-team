package com.augmentiq.maxiq.cache.support;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.augmentiq.constant.maxiq.CHAR_CONSTANTS;

/**
 * This will be used to hold the configuration values
 *
 * @author shiva
 */
public class Properties extends Hashtable<String, String> {
  /** */
  private static final long serialVersionUID = 1L;

  public synchronized void load(List<String> list) throws IOException {
    if (null != list) {
      for (String line : list) {
        String[] splt = StringUtils.splitPreserveAllTokens(line, CHAR_CONSTANTS.EQUALS);

        if (null != splt && splt.length == 2) {
          put(splt[0], splt[1]);
        }
      }
    }
  }

  public String getProperty(String key) {
    String sval = super.get(key);
    return sval;
  }
}

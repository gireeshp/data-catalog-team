package com.augmentiq.maxiq.cache.support;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shivanand on 8/20/2015. The class will be used for in application storage or
 * communication of data.
 */
public class SharedVariables {
  private static Map<String, Object> sharedVariable = new HashMap<>();

  public static void set(String key, Object value) {
    sharedVariable.put(key, value);
  }

  public static String getString(String key) {
    return sharedVariable.get(key) + "";
  }

  public static Object get(String key) {
    return sharedVariable.get(key);
  }

  public static void remove(String key) {
    sharedVariable.remove(key);
  }
}

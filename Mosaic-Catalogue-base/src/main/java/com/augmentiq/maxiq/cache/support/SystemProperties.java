package com.augmentiq.maxiq.cache.support;

import com.augmentiq.maxiq.constant.cache.CacheConstants;

/** Created by shivanand on 8/6/2015. */
@Deprecated
public class SystemProperties {
  private static String confDir;

  public static String getConfDir() {
    if (null == confDir) {
      confDir =
          System.getenv().get(CacheConstants.MAXIQ_HOME) + CacheConstants.MAXIQ_CONFIGURATION_DIR;
    }
    return confDir;
  }

  public static void setConfDir(String confPath) {
    confDir = confPath;
  }
}

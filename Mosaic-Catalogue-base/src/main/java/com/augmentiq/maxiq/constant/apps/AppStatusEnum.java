package com.augmentiq.maxiq.constant.apps;

public enum AppStatusEnum {
  RUNNING,
  SUCCEEDED,
  active,
  inactive,
  FAILED,
  ABORTED
}

package com.augmentiq.maxiq.constant.apps;

public class ApplicationConstants {
  public static final String appPrefix = "App_";
  public static final String appId = "appId";
  public static final String appName = "appName";
  public static final String userId = "userId";
  public static final String userName = "userName";
  public static final String createDt = "createDt";
  public static final String status = "status";
  public static final String keyOf = "keyOf";
  public static final String groupName = "group_name";

  public static final String DATASOURCE = "datasource";
  public static final String NODE = "node";
  public static final String id = "id";
}

package com.augmentiq.maxiq.constant.apps.domain.enums;

public enum ProcessStatusEnum {
  stared,
  error,
  finished,
  running
}

package com.augmentiq.maxiq.constant.apps.setup;

import com.augmentiq.maxiq.base.dao.sql.operation.configuration.Sequences;
import com.augmentiq.maxiq.constant.configuration.enums.UserStatus;

public class QueryConstants {

	public static String DEFAULT_TIMESTAMP = "0000-00-00 00:00:00";
	public static Boolean IS_TRUE = true;
	public static Boolean IS_FALSE = false;
	public static final String SUCCESSFUL = "SUCCESSFUL";
	public static final String INACTIVE = "0";
	public static final String ACTIVE = "1";

	public static class GroupConfig {
		public static String FETCH_AVAILABLE_GROUPS = "select ug.groupId, mloa.appName, ug.groupName, ug.description, ug.queueMasterID, qm.queueName, CONCAT(A.firstName,' ',A.lastName) as insertedBy, ug.insertTs, CONCAT(B.firstName,' ',B.lastName) as updatedBy,ug.updateTs from usergroup ug left outer join queue_master qm on (ug.queueMasterID=qm.queueMasterId) left outer join application_user A on (A.unqUserId = ug.insertedBy) left outer join application_user B on (B.unqUserId = ug.updatedBy) left outer join masterListOfApplication mloa on (ug.appId = mloa.appId)";
		public static String FETCH_AVAILABLE_SUBGROUPS = "select usg.subGroupId, usg.subGroupName, usg.subGroupDescription,usg.usergroupId,usg.queueMasterID,mloa.appName,ug.groupName, (CASE WHEN CONCAT(A.firstName,' ',A.lastName) = 'MaxIQ Bot' THEN null ELSE CONCAT(A.firstName,' ',A.lastName) END) as  insertedBy, usg.insertTs, (CASE WHEN CONCAT(B.firstName,' ',B.lastName) = 'MaxIQ Bot' THEN null ELSE CONCAT(B.firstName,' ',B.lastName) END) as updatedBy, usg.updateTs, C.queueName as queueName from user_subgroup usg left outer join usergroup ug on (usg.usergroupId=ug.groupId) left outer join application_user A on (A.unqUserId = usg.insertedBy) left outer join application_user B on (B.unqUserId = usg.updatedBy) left outer join queue_master C on (C.queueMasterId = usg.queueMasterID) left outer join masterListOfApplication mloa on (ug.appid = mloa.appId)";
		public static String FETCH_AVAILABLE_QUEUES = "select * from queue_master";
		public static String FETCH_AVAILABLE_GROUPS_LIST = "select * from usergroup";
		public static String FETCH_RANGER_GROUPS_BY_MAXIQ_GROUPS = "select rangerGroupId from usergroup where groupId IN (%s)";
		public static String INSER_GROUP = "insert into usergroup(groupName,description,queueMasterID,insertedBy) select ?, ?,(select queueMasterID from queue_master where queueName=?) as queueMasterID,?";
		public static String FETCH_AVAILABLE_ACCESS_DATA = "select rm.unqRoleMasterId, rm.roleName, rm.roleDescription, group_concat(distinct ' ', acm.action) as actionsList, rm.createdBy, rm.createdTs, rm.updatedBy, rm.updatedTs from role_master rm left outer join role_action_mappings ram on ram.roleMasterId = rm.unqRoleMasterId left outer join actions_master acm on ram.actionId = acm.actionsMasterId group by rm.unqRoleMasterId";
		public static String FETCH_GROUP_USER_MAPPING_DATA = "select groupName,groupId from usergroup where groupId in (select groupId from user_group_mappings where userId = ?)";
	}

	public static class AccessConfig {
		public static String FETCH_AVAILABLE_ACCESS_DATA = "select rm.unqRoleMasterId, mloa.appName, rm.roleName, rm.roleDescription, group_concat(distinct ' ', acm.action) as actionsList, CONCAT(A.firstName,' ',A.lastName) as createdBy, rm.createdTs, CONCAT(B.firstName,' ',B.lastName) as updatedBy, rm.updatedTs from role_master rm left outer join role_action_mappings ram on ram.roleMasterId = rm.unqRoleMasterId left outer join actions_master acm on (ram.actionId = acm.actionsMasterId) left outer join application_user A on (A.unqUserId = rm.createdBy) left outer join application_user B on (B.unqUserId = rm.updatedBy) left outer join masterListOfApplication mloa on (rm.appId = mloa.appId) group by rm.unqRoleMasterId";
		public static String FETCH_AVAILABLE_ACTIONS = "select * from actions_master";
		public static String FETCH_MAXIQ_BASED_ACTION = "select * from actions_master where subsetOf =";
	}

	public static class UserManageMent {

		public static String FETCH_AVAILABLE_USERS = "select au.unqUserId, au.firstName, au.lastName, au.emailId, au.password, group_concat(distinct ' ', mloa.appName) as solutionNameList, au.status, au.updatedTs,au.userRangerId,au.rangerUserName,au.lockInd from application_user au left outer join userApplicationMapping uam on (au.unqUserId=uam.userId) left outer join masterListOfApplication mloa on (uam.appId=mloa.appId) where au.unqUserId > 0 and au.status != '"
				+ UserStatus.DELETED.name() + "' group by au.unqUserId";
		public static String FETCH_AVAILABLE_GROUPS = "select groupId, groupName,appId from usergroup where appId = ";
		public static String FETCH_AVAILABLE_SUBGROUPS = "select usg.subGroupId, CONCAT(usg.subGroupName,'_',ug.groupName) as subGroupName, usg.usergroupId, ug.groupName from user_subgroup usg left outer join usergroup ug on usg.usergroupId=ug.groupId where ug.appId = ";
		public static String FETCH_AVAILABLE_ROLES = "select unqRoleMasterId, roleName, appId from role_master where appId = ";
		public static String FETCH_GROUP_SUBGROUP_CONCAT = "select sg.subGroupId, concat(sg.subGroupName,'_',ug.groupName) as groupSubGroup  from user_subgroup sg left outer join usergroup ug on sg.usergroupId=ug.groupId group by sg.subGroupId";

		public static String FETCH_SELECTED_GROUPS = "select ug.groupId, ug.groupName,ug.appId from usergroup ug left outer join user_group_mappings ugm on (ug.groupId=ugm.groupId) where ug.appId = ";
		public static String FETCH_SELECTED_SUBGROUPS = "select usg.subGroupId, CONCAT(usg.subGroupName,'_',ug.groupName) as subGroupName, usg.usergroupId, ug.groupName from user_subgroup usg left outer join usergroup ug on usg.usergroupId=ug.groupId left outer join user_subgroup_mappings usm on usm.subgroupId=usg.subGroupId where ug.appId = ";
		public static String FETCH_SELECTED_ROLES = "select rm.unqRoleMasterId, rm.roleName, rm.appId from role_master rm left outer join user_role_mappings urm on rm.unqRoleMasterId=urm.roleId where rm.appId = ";
		public static String GET_ALL_DETAILS_OF_PERSONA = "select * from persona_master where persona_id in  (	select personaId from flavour_persona_mappings where flavourId in (  select flavourId from client where clientId in ( select clientId from user_client_mapping where unqUserId = ? )))";
		public static String GET_USER_PERSONA_MAPPING = "		select c.* from (select b.* from application_user a join user_persona b on a.unqUserId = b.unqUserId) as d join persona_master c on d.persona_id = c.persona_id "
				+ "		where d.unqUserId=selectedUserId and c.persona_id in (																													"
				+ "			select personaId from flavour_persona_mappings where flavourId in (  																						"
				+ "				select flavourId from client where clientId in ( 				 																						"
				+ "					select clientId from user_client_mapping where unqUserId =  selectedUserId 																						"
				+ "				)  																																						"
				+ "			)																																							"
				+ "		)																																								";

		public static final String UPDATE_USER_RANGER_USER_DETAILS = "update application_user set userRangerId=?,rangerUserName=? where unqUserId=?";
		public static String FETCH_FNAME_LNAME_OF_USER = "SELECT firstName, lastName FROM application_user where unqUserId = ";
		public static String ALLOCATE_USER_TO_CLIENT = "insert into user_client_mapping (unqUserId, clientId) select ?, clientId from user_client_mapping where unqUserId=?";
		public static String FETCH_LDAP_SPECIFIC_DETAILS_OF_USER = "select unqUserId, firstName, lastName, emailId, password, status from application_user where unqUserId > 0 ";
		public static String FETCH_AVAILABLE_USER_QUEUE = "select * from queue_master  where queueMasterId in (select distinct  queueMasterID from user_subgroup where subGroupId in (select distinct subgroupId from user_subgroup_mappings where userId = ?))";
		public static String UPDATE_LAST_USED_PERSONA_SQL = "update application_user set lastUsedPersonaId=(select persona_id from user_persona where unqUserId=? limit 1) where unqUserId=? and lastUsedPersonaId not in (select persona_id from user_persona where unqUserId=?)";
		public static final String FETCH_USER_OBJECT_MAPPING = "select * from user_object_mapping where userId= ? and objectId = ?";
		public static final String FETCH_GROUP_OBJECT_MAPPING = "select * from group_object_mapping where groupId= ? and objectId = ?";
		public static final String FETCH_PROJECTS_DETAILS = "select * from projects where createdBy = ? and id = ?";
		public static String FETCH_USER_ROLE_DETAILS = "select * from role_master where unqRoleMasterId in (select roleId from user_role_mappings where userId = ?)";
		public static String FETCH_USER_GROUP_DETAILS = "select * from usergroup g where g.groupId in (select ug.groupId from user_group_mappings ug where ug.userId = ?)";;
	}

	public static class DbExceptions {
		public static String DATA_BASE_EXCEPTION = "Error ocurred while performing database operations";
		public static String DRIVER_NOT_LOADED = "It looks like driver not loaded...";
		public static String SQL_EXCEPTION = "Error while connecting to mysql...";
		public static String SUBGROUP_ALREADY_EXIST = "Subgroup name already exists for given group, Please suggest another Subgroup name";
		public static String GROUP_ALREADY_EXIST = "Group name already exists, Please suggest another Group name";
		public static String ROLE_ALREADY_EXIST = "Role name already exists, Please suggest another Role name";
		public static String USER_ALREADY_EXISTS = "User already exists";
		public static String PASS_ENCRYPTION = "Error while encrypting password";
		public static String PASS_DECRYPTION = "Error while decrypting password";
	}

	public static class Parameters {
		public static final String RANGER_GROUP_ID = "rangerGroupId";
		public static final String GROUP_ID = "groupId";
		public static final String MODEL_TYPE = "modelType";
		public static final String GROUP_NAME = "groupName";
		public static final String APP_ID = "appId";
		public static final String DESCRIPTION = "description";
		public static final String SELECTED_USER_ID = "selectedUserId";
		public static final String CREATED_BY_USER_ID = "createdByUserId";
		public static final String NEW_USER_ID = "newUserId";
		public static final String DATA_SOURCE_NAME = "datasourcename";
		public static final String REPO_NAME = "repoName";
		public static final String INJECTION = "ingection";
		public static final String DATASOURCEID = "dataSourceId";
		public static final String ADVANCED_FIELD_VALIDATION_ID = "advancedFieldValidationId";
		public static final String DATASOURCE_LOAD_STRATEGY = "dataSourceLoadStrategy";
		public static final String FEATURE_EXTRACTION = "featureExtraction";
		public static final String FEATURE_EXTRACTION_ID = "featureExtractionId";
		public static final String CONFIG = "config";
		public static final String SOURCES = "sources";
		public static final String SOURCE_FEED_BACK = "sourceFeedback";
		public static final String RATING = "rating";
		public static final String VOTE = "vote";
		public static final String FEEDBACK_ID = "feedbackId";
		public static final String UPDATED_VALUE = "updatedValue";
		public static final String SNAP_SHOT_ID = "snapshotId";
		public static final String FROM_DATE = "fromDate";
		public static final String TO_DATE = "toDate";
		public static final String FIELD_MAPPING_VERSION_ID = "fieldMappingVersionId";
		public static final String INDICATOR = "indicator";
		public static final String KEY_OF = "keyOf";
		public static final String CONNECTOR_CONFIG = "connectorConfig";
		public static final String TRANSFORMATIONS = "transformations";
		public static final String FIELD_MAPPING = "fieldMapping";
		public static final String TAGS = "tags";
		public static final String RECORD_COMBINER = "recordCombiner";
		public static final String REPO_ID = "repoId";
		public static final String JOB_SCHEDULAR = "jobSchedular";
		public static final String JOB_INSTANCE_ID = "jobInstanceId";
		public static final String PROFILE_TYPE = "profileType";
		public static final String DR_ID = "drId";
		public static final String DS_ID = "dsId";
		public static final String NOTE_BOOK_ID = "notebookId";
		public static final String REQUIREMENT = "requirement";
		public static final String NEW_NOTEBOOK_NAME = "newNotebookName";
		public static final String OBJECT_ID = "objectId";
		public static final String OBJECT_TYPE = "objectType";
		public static final String APP_NAME = "appName";
		public static final String JOB_STATUS = "jobStatus";
		public static final String JOB_INST_ID = "jobInstId";
		public static final String COMMENT = "comment";
		public static final String TASK_TYPE = "taskType";
		public static final String SELECTED_JOB_LIMIT = "selectedJobLimit";
		public static final String JOB_NAME = "jobName";
		public static final String EMAIL = "email";
		public static final String ID = "id";
		public static final String INDEX_NAME = "indexName";
		public static final String INDEX_INPUT_TYPE = "indexInputType";
		public static final String INDEX_DEF = "indexDef";
		public static final String INDEX_ID = "indexId";
		public static final String PROCESS_DATA = "processData";
		public static final String PARENT_DATA_FILE = "parentDataFile";
		public static final String LOCATION = "location";
		public static final String PROPERTIES = "properties";
		public static final String DATA_REQUEST_DTO = "dataRequestDTO";
		public static final String IS_REQUESTED_FOR_GROUP = "isRequestedForGroup";
		public static final String REQUESTED_ID = "requestId";
		public static final String JUSTIFICATION = "justification";
		public static final String NODE_ID = "nodeId";
		public static final String APPLICATION_ID = "applicationId";
		public static final String BAAP_JOB_INST_ID = "baapInstId";
		public static final String INPUT_PARAM = "inputParam";
		public static final String OLD_APP_ID = "oldAppId";
		public static final String NEW_APP_NAME = "newAppName";
		public static final String SELECTED_BOOST_ID = "selectedBoostId";
		public static final String POLL_INTERVAL = "pollInterval";
		public static final String VALUES = "values";
		public static final String FLOW_TYPE = "flowType";
		public static final String OWNER_PROJECT_ID = "ownerProjectId";
		public static final String PARAM_1 = "param1";
		public static final String PARAM_2 = "param2";
		public static final String PARAM_3 = "param3";
		public static final String PARAM_4 = "param4";
		public static final String PARAM_5 = "param5";
		public static final String DATA_SOURCE_TYPE = "dataSourceType";
		public static final String PROJECT_ID = "projectId";
		public static final String MOSAIC_APPLICATION = "mosaicApplication";
		public static final String OTHER_APPLICATION = "otherApplication";
		public static final String ROLEACTIONS = "roleActions";
	}

	public static class constants {
		public static final String GROUP = "GROUP";
		public static final String SUBGROUP = "SUBGROUP";
		public static final String ROLES = "ROLES";
		public static final String SOLUTIONNAME = "SOLUTIONNAME";
		public static final String PERSONA_DETAILS = "PERSONA_DETAILS";
		public static final String SOLUTION_ID = "SOLUTIONID";
	}

	public static class GlobalSearch {

		public static final String FETCH_ALL_APPLICATION_USERS_NAME = "SELECT DISTINCT CONCAT(user.firstName,' ',user.lastName) as createdBy FROM application_user user inner join user_group_mappings usg on usg.userId = user.unqUserId where usg.groupId = ?";

		// isModified is column name in Database
		public static final String IS_MODIFIED = "isModified";
		public static final String IS_MODIFIED_TRUE = "1";
		public static final String IS_MODIFIED_FALSE = "0";

		// Select all datasource not contains flowlevel datasources
		public static final String QUERY_FOR_DATASORUCE = "select * from datasourceinstance";
		public static final String QUERY_FOR_MODIFIED_DATASORUCES = "select * from datasourceinstance where isModified='1' and appId='null'";

		public static final String UPDATE_ALL_APPS_OF_USER_IS_MODIEFIED = "update maxiq_apps set isModified = 1 where createdBy = ?";
		public static final String UPDATE_ALL_DATASOURCE_OF_USER_AS_MODIFIED = "update datasourceinstance set isModified = 1 where createdBy = ?";

		public static final String FETCH_ALL_DATASOURCE_FOR_OPERATIONAL = "select dsi.ingection, dsi.dataSourceName, dsi.appId, dsi.createdDate, dsi.updatedDate, dsi.fileType, dsi.compressionType, dsi.dataSourceStatus, dsi.totalRecords, dsi.size, dsi.dataSourceId, dsi.filePaths, dsi.latestFilepath, dsi.oldFilePath, dsi.dataRepoId, dsi.dataRepoName, dsi.dataSourceType, dsi.groupId, CONCAT(A.firstName,' ',A.lastName) as createdBy,CONCAT(B.firstName,' ',B.lastName) as lastRunBy from datasourceinstance dsi left outer join application_user A on (A.unqUserId = dsi.createdBy) left outer join application_user B on (B.unqUserId = dsi.lastRunBy) where dsi.appId='null'";
		public static final String QUERY_FOR_DATASORUCE_USERDETAILS = "select d.ownerProjectId,d.dataSourceId from datasourceinstance d where d.dataSourceName=";
	}

	public static class IndexConstants {
		public static final String INDEX_NAME = "indexName";
		public static final String ID = "id";
	}

	public static class GlossaryMaster {
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String SHORT_DESCRIPTION = "shortDescription";
		public static final String LONG_DESCRIPTION = "longDescription";
		public static final String CREATED_DATE = "createdDate";
		public static final String CREATED_BY = "createdBy";
		public static final String MODIFIED_BY = "modifiedBy";
		public static final String MODIFIED_DATE = "modifiedDate";
		public static final String GROUP_ID = "groupId";
		public static final String STATUS = "status";
		public static final String INACTIVE = "0";
		public static final String ACTIVE = "1";
	}

	public static class QUEUE_MASTER {
		public static final String QUEUE_MASTER_ID = "queueMasterId";
	}

	public static class Query {
		public static final String SELECT = " select ";
		public static final String SELECT_STAR_FROM = " select * from ";
		public static final String UPDATE = " update ";
		public static final String SET = " set ";
		public static final String WHERE = " where ";
		public static final String AND = " and ";
		public static final String OR = " or ";
		public static final String FROM = " from ";
		public static final String DELETE = " delete ";
		public static final String DELETE_FROM = " delete from ";
		public static final String ORDER_BY = " order by";
		public static final String DESC = " desc ";
		public static final String SINGLE_QUOTE = " ' ";
		public static final String LEFT_JOIN = " left join ";
		public static final String RIGHT_JOIN = " right join ";
		public static final String INNER_JOIN = " inner join ";
		public static final String ON = " on ";

	}

	public static class Project {
		public static final String ID = "id";
		public static final String PROJECT_NAME = "projectname";
		public static final String OWNER_PROJECT_ID = "ownerProjectId";
		public static final String PROJECT_ID = "projectId";
		public static final String DESCRIPTION = "description";
		public static final String TYPE = "type";
		public static final String INACTIVE = "INACTIVE";
		public static final String CREATED_BY = "createdBy";
		public static final String ACCESS_TYPE = "accessType";
	}

	public static class ShareAccesstMapping {
		public static final String ID = "id";
		public static final String USER_ID = "userId";
		public static final String GROUP_ID = "groupId";
		public static final String OBJECT_ID = "objectId";
		public static final String OBJECT_TYPE = "objectType";
		public static final String ACCESS_TYPE = "accessType";
	}

	public static class DataRequest {
		public static final String SHAREACCHIST_ID = "shareaccHist_Id";
		public static final String STATUS = "status";
		public static final String LAST_MODIFIED_BY = "last_modified_by";
		public static final String JUSTIFICATION = "justification";
		public static final String CREATED_DATE = "created_date";
		public static final String MODIFIED_DATE = "modified_date";
		public static final String GROUP_ID = "group_id";
		public static final String OBJECT_ID = "objectId";
		public static final String OBJECT_TYPE = "objectType";
		public static final String REQUESTED_USER_ID = "requested_user_id";
		public static final String USERID = "userId";
		public static final String ID = "id";
		public static final String GROUPID = "groupId";
		public static final String REQUESTED_BY = "requestedBy";
	}

	public static class Collection {
		public static final String ID = "id";
		public static final String USERID = "userId";
		public static final String OBJECT_ID = "objectId";
		public static final String OBJECT_TYPE = "objectType";
		public static final String STATUS = "status";
	}

	public static class GroupLevelUser {
		public static final String POLICY_STATUS = "policyStatus";
		public static final String CREATE_DATE = "createDate";
		public static final String GROUP_USER_PASSWORD = "groupUserPassword";
		public static final String GROUP_ID = Parameters.GROUP_ID;
	}

	public static class DataSourceInstance {
		public static final String CATEGORY = "category";
		public static final String SUBCATEGORY = "subCategory";
		public static final String DESCRIPTION = "description";
	}

	public static class Notification {
		public static final String ID = "id";
		public static final String USERID = "userId";
		public static final String OBJECTTYPE = "objectType";
		public static final String ACTIONID = "actionId";
		public static final String NOTIFICATIONTYPE = "notificationType";
	}

	public static class DatasourceServiceApimanager {
		public static final String ID = "id";
		public static final String DATASOURCE_ID = "dataSourceId";
		public static final String DATASOURCE_NAME = "dataSourceName";
		public static final String USER_ID = "userId";
		public static final String APIKEY = "apiKey";
		public static final String STATUS = "status";
		public static final String DELETED = "deleted";
		public static final String CREATED_DATE = "createdDate";
		public static final String MODIFIED_DATE = "modifiedDate";
		public static final String LEVEL = "level";
		public static final String CREATED_BY = "createdBy";
		public static final String COLUMN_NAME="COLUMN_NAME";
		public static final String QUERY = "query";
		public static final String FETCH_DATASOURCE_BY_USERID = "select * from datasourceinstance where dataSourceId NOT IN (select dataSourceId from datasource_service_apimanager where userId =?) and createdBy = ? ";
	}

	public static class CatSubCatMaster {
		public static final String ID = "id";
		public static final String CAT_OR_SUBCAT_NAME = "catOrSubCatName";
		public static final String PARENT_ID = "parentId";
		public static final String CREATED_DATE = "createdDate";
		public static final String CREATED_BY = "createdBy";
		public static final String MODIFIED_DATE = "modifiedDate";
		public static final String MODIFIED_BY = "modifiedBy";
		public static final String STATUS = "status";
	}

	public static class UserPersona {
		public static final String USER_ID = "userId";
		public static final String PERSONA_ID = "personaId";
	}

	public static class CategoryOrSubCategory {
		public static final String GET_ALL_DATASOURCES_BY_CATEGORY = QueryConstants.Query.SELECT_STAR_FROM
				+ Sequences.DATASOURCE_INSTANCE + " where category!='null' and category!='' and category=";
		public static final String GET_ALL_DATASOURCES_BY_SUB_CATEGORY = QueryConstants.Query.SELECT_STAR_FROM
				+ Sequences.DATASOURCE_INSTANCE + " where subCategory!='null' and subCategory!='' and subCategory=";
	}

	public static class Connection {
		public static final String CONNECTION_ID = "connectionId";
		public static final String CONNECTION_NAME = "conncetionName";
		public static final String SUBSOURCE_ID = "subSourceId";

	}
}

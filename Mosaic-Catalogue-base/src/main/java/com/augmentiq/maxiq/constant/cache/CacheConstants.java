package com.augmentiq.maxiq.constant.cache;

/***
 * Contains constants required to store and fetch cache objects.
 * 
 * @author shiva
 * 
 */
public class CacheConstants {
	public static final String MAXIQ_HOME = "MAXIQ_HOME";// TODO
	public static final String MAXIQ_CONFIGURATION_DIR = "/conf/";
	public static final String HIVE_OUTPUT_FOLDER = "HIVE_OUTPUT_FOLDER";
	public static final String SERVER_HOST_NAME = "SERVER_HOST_NAME";
	public static final String MONGO_SERVER_IP = "MONGO_SERVER_IP";
	public static final String MONGO_SERVER_PORT = "MONGO_SERVER_PORT";
	public static final String MONGO_CONFIG_DB = "MONGO_CONFIG_DB";
	public static final String MONGO_DB = "MONGO_DB";
	public static final String HIVE_CONNECTION_STRING = "HIVE_CONNECTION_STRING";
	public static final String HIVE_DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";
	public static final String ZOOKEEPER_IP = "ZOOKEEPER_IP";
	public static final String KAFKA_BROKER_IP_LIST = "KAFKA_BROKER_IP_LIST";
	public static final String SERVER_NAME = "SERVER_NAME";
	public static final String ZEPPELIN_SERVER_PATH = "ZEPPELIN_SERVER_PATH";
	// public static final String WEBSERVER_LOCAL_TEMP = "WEBSERVER_LOCAL_TEMP";
	public static final String UPLOAD_IP = "UPLOAD_IP";
	public static final String UPLOAD_USER = "UPLOAD_USER";
	public static final String UPLOAD_PWD = "UPLOAD_PWD";
	// public static final String UPLOAD_PATH = "UPLOAD_PATH";
	public static final String HBASE_HOSTNAME = "HBASE_HOSTNAME";
	public static final String JAR_PATH = "JAR_PATH";
	public static final String RESOURCE_MANAGER_ADDRESS = "RESOURCEMANAGER_ADDRESS";
	public static final String RANGER_ENABLE = "RANGER_ENABLE";
	public static final String SECURITY_ENABLE = "SECURITY_ENABLE";
	public static final String ENCRYPTION_ZONE_ENABLED = "ENCRYPTION_ZONE_ENABLED";

	public static final String KERBEROS_ADMIN_USER_NAME = "KERBEROS_ADMIN_USER_NAME";
	public static final String KERBEROS_ADMIN_USER_PASSWORD = "KERBEROS_ADMIN_USER_PASSWORD";

	public static final String HDFS_PATH_STARTS_WITH = "HDFS_PATH_STARTS_WITH";
	public static final String YARN_MASTER = "YARN_MASTER";
	public static final String JARS_UPLOAD_PATH = "JARS_UPLOAD_PATH";
	public static final String SCRIPT_PATH = "SCRIPT_PATH";
	public static final String HADOOP_SCRIPT_PATH = "HADOOP_SCRIPT_PATH";
	public static final String ELASTIC_SEARCH_SERVER = "ELASTIC_SEARCH_SERVER";
	public static final String ELASTIC_SEARCH_PORT = "ELASTIC_SEARCH_PORT";
	public static final String MAXIQ_DATA_PATH = "MAXIQ_DATA_PATH";
	public static final String HADOOP_BASE_CONF_PATH = "HADOOP_BASE_CONF_PATH";
	public static final String MAXIQ_SERVER_OUTPUT_DIR = "MAXIQ_SERVER_OUTPUT_DIR";
	public static final String PASS_EXP_DAYS = "PASS_EXP_DAYS";
	public static final String OUTPUT_FILE_TYPE = "OUTPUT_FILE_TYPE";
	public static final String PHOENIX_CONNECTION_URL = "PHOENIX_CONNECTION_URL";
	public static final String PHOENIX_JAR_PATH = "PHOENIX_JAR_PATH";
	public static final String HBASE_TABLE_STORAGE_PATH = "HBASE_TABLE_STORAGE_PATH";
	public static final String ZOOKEEPER_QUORUM = "ZOOKEEPER_QUORUM";
	public static final String ZOOKEEPER_CLIENT_PORT = "ZOOKEEPER_CLIENT_PORT";
	public static final String ZOOKEEPER_ZNODE_PARENT = "ZOOKEEPER_ZNODE_PARENT";
	public static final String NUM_EXECUTORS = "NUM_EXECUTORS";
	public static final String KIBANA_SERVER_PATH = "KIBANA_SERVER_PATH";
	public static final String XSRF_TOKEN = "XSRF_TOKEN";
	public static final String VISUALIZE_PATH = "VISUALIZE_PATH";
	public static final String ZEPPELIN_USERNAME = "ZEPPELIN_USERNAME";
	public static final String ZEPPELIN_PASSWORD = "ZEPPELIN_PASSWORD";
	public static final String RANGER_SERVER = "RANGER_SERVER";
	public static final String HIVE_QUERY_COUNT = "HIVE_QUERY_COUNT";
	public static final String WAIT_FOR_TIME_IN_SECONDS = "WAIT_FOR_TIME_IN_SECONDS";
	public static final String GLOBAL_PARAM_TIMESTAMP_FORMAT = "GLOBAL_PARAM_TIMESTAMP_FORMAT";
	public static final String GLOBAL_PARAM_DATE_FORMAT = "GLOBAL_PARAM_DATE_FORMAT";
	public static final String MAXIQ_STD_TS_FORMAT = "MAXIQ_STD_TS_FORMAT";
	public static final String MAXIQ_STD_DF_TS_FORMAT = "MAXIQ_STD_DF_TS_FORMAT";
	public static final String MAXIQ_STD_DF_DATE_FORMAT = "MAXIQ_STD_DF_DATE_FORMAT";
	public static final String MAXIQ_IMAGE_PATH = "MAXIQ_IMAGE_PATH";
	public static final String GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER = "GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER";
	public static final String GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER_PORT = "GLOBAL_SEARCH_ELASTIC_SEARCH_SERVER_PORT";
	public static final String GLOBAL_SEARCH_ELASTIC_SEARCH_CLUSTER_NAME = "GLOBAL_SEARCH_ELASTIC_SEARCH_CLUSTER_NAME";
	public static final String GLOBAL_SEARCH_ELASTIC_SEARCH_INDEX_NAME = "GLOBAL_SEARCH_ELASTIC_SEARCH_INDEX_NAME";
	public static final String LDAP_SERVER_PATH = "LDAP_SERVER_PATH";
	public static final String LDAP_CONTEXT_FACTORY = "LDAP_CONTEXT_FACTORY";
	public static final String LDAP_SECURITY_AUTH = "LDAP_SECURITY_AUTH";
	public static final String AUTH_METHOD = "AUTH_METHOD";
	public static final String AUTH_METHOD_LDAP = "LDAP";
	public static final String AUTH_METHOD_MAXIQ = "MAXIQ";
	public static final String LDAP_USERNAME = "LDAP_USERNAME";
	public static final String LDAP_PASSWORD = "LDAP_PASSWORD";
	public static final String OU = "OU";
	public static final String USERS = "USERS";
	public static final String DC = "DC";
	public static final String AGUMENT = "AGUMENT";
	public static final String COM = "COM";
	/* For Hive & Spark 1.5 table creation START */
	public static final String CREATE_TABLE_FROM_THREAD = "CREATE_TABLE_FROM_THREAD";
	public static final String THREAD_SLEEP_TIME = "THREAD_SLEEP_TIME";
	/* For Hive & Spark 1.5 table creation END */
	public static final String HIVE_SUPER_USER = "HIVE_SUPER_USER";
	public static final String HIVE_SUPER_PASSWORD = "HIVE_SUPER_USER_PASSWORD";
	public static final String APP_SLEEP_TIME = "APP_SLEEP_TIME";
	public static final String APPRUN_SCHED_URL = "APPRUN_SCHED_URL";
	public static final String APP_REQ_COOKIES = "APP_REQ_COOKIES";
	public static final String INSTALL_APP_ID = "INSTALL_APP_ID";
	public static final String IMAGE_PATH = "IMAGE_PATH";
	public static final String NOTEBOOK_IDS = "NOTEBOOK_IDS";
	public static final String R_SCRIPT_FOLDER = "R_SCRIPT_FOLDER";
	public static final String MYSQL_CONNECTOR_JAR_LOCATION = "MYSQL_CONNECTOR_JAR_LOCATION";
	public static final String DB_ADDRESS = "DB_ADDRESS";
	public static final String DB_NAME = "DB_NAME";
	public static final String USER_NAME = "USER_NAME";
	public static final String PASSWORD = "PASSWORD";
	/* For MarketPlace Configuration START */
	public static final String MARKETPLACE_SERVER_URL = "MARKETPLACE_SERVER_URL";
	public static final String CLIENT_NAME = "CLIENT_NAME";
	public static final String API_KEY = "API_KEY";
	public static final String MARKETPLACE_SERVER_READ_TIMEOUT = "MARKETPLACE_SERVER_READ_TIMEOUT";
	public static final String MARKETPLACE_SERVER_CONNECTION_TIMEOUT = "MARKETPLACE_SERVER_CONNECTION_TIMEOUT";
	public static final String MARKETPLACE_INDUSTRY_ID = "MARKETPLACE_INDUSTRY_ID";
	public static final String MARKETPLACE_ACCELERATOR_ID = "MARKETPLACE_ACCELERATOR_ID";
	public static final String MARKETPLACE_AUTOMATION_ID = "MARKETPLACE_AUTOMATION_ID";
	public static final String MARKETPLACE_ALGORITHMS_ID = "MARKETPLACE_ALGORITHMS_ID";
	public static final String MARKETPLACE_AI_ID = "MARKETPLACE_AI_ID";
	public static final String MARKETPLACE_INSIGHTS_ID = "MARKETPLACE_INSIGHTS_ID";
	/* For MarketPlace Configuration END */
	public static final String ZEPPELIN_SERVER_URL = "ZEPPELIN_SERVER_URL";
	public static final String RABBITMQ_HOST_NAME = "RABBITMQ_HOST_NAME";
	public static final String RABBITMQ_USER_NAME = "RABBITMQ_USER_NAME";
	public static final String RABBITMQ_PASSWORD = "RABBITMQ_PASSWORD";
	public static final String RABBITMQ_PORT = "RABBITMQ_PORT";
	/* Projects Configurations */
	public static final String PROJECTS_TYPES = "PROJECTS_TYPES";
	public static final String KAFKA_PROP = "KAFKA_PROP";
	public static final String KAFKA_CONSUMER_PROP = "KAFKA_CONSUMER_PROP";
	public static final String CONSUMER_TOPIC = "CONSUMER_TOPIC";
	public static final String PRODUCER_TOPIC = "PRODUCER_TOPIC";
	public static final String MODELS_MAP = "MODELS_MAP";
	public static final String MODELS_API_URL = "MODELS_API_URL";
	public static final String DATA_SOURCE_UTILITY_TIMEZONE = "DATA_SOURCE_UTILITY_TIMEZONE";
	public static final String DATASOURCE_UTILTIY_RUNTIME = "DATASOURCE_UTILTIY_RUNTIME";
	public static final String DATASOURCE_UTILITY_INSTANCESONDAYS_JOBID = "DATASOURCE_UTILITY_INSTANCESONDAYS_JOBID";
	public static final String DATASOURCE_UTILITY_JOBRUN_FLAG = "DATASOURCE_UTILITY_JOBRUN_FLAG";
	public static final String MODELS_KAFKA_LOOP_BREAKER = "MODELS_KAFKA_LOOP_BREAKER";

	/* Jupyter Properties start */
	public static final String JUPYTER_VISUALISE_SERVER = "JUPYTER_VISUALISE_SERVER";
	public static final String JUPYTER_SERVER = "JUPYTER_SERVER";
	public static final String JUPYTER_SERVER_PORT = "JUPYTER_SERVER_PORT";
	public static final String JUPYTER_SERVER_DATA_PATH = "JUPYTER_SERVER_DATA_PATH";
	public static final String JUPYTER_SERVER_TOKEN = "JUPYTER_SERVER_TOKEN";

	/* Jupyter Properties end */

	public static final String ADVANCED_STATS_PATH = "ADVANCED_STATS_PATH";

	/* ML Constants */
	public static final String CLASS_NOT_FOUND = "CLASS_NOT_FOUND";
	public static final String CLASS_NOT_FOUND_MSG = "This class is not available";
	/* For ML API START */
	public static final String ML_PACKAGES = "ML_PACKAGES";
	public static final String SPARK_MASTER = "SPARK_MASTER";
	public static final String SPARK_EXECUTOR_MEMORY = "SPARK_EXECUTOR_MEMORY";
	/* For ML API END */

	public static final String EXTERNAL_HBASE_CLUSTER = "EXTERNAL_HBASE_CLUSTER";

	// security changes
	public static final String JDBC_DRIVER = "JDBC_DRIVER";
	public static final String GROUP_MEMBER = "GROUP_MEMBER";
	public static final String LDAP_COMMON_USER_DN = "LDAP_COMMON_USER_DN";
	public static final String LDAP_COMMON_GROUP_DN = "LDAP_COMMON_GROUP_DN";
	public static final String KNOX_SSL_ENABLED = "KNOX_SSL_ENABLED";
	public static final String SSL_FILE_PATH = "SSL_FILE_PATH";
	public static final String CLUSTER_NAME = "CLUSTER_NAME";
	public static final String KERBEROS_REALM = "KERBEROS_REALM";
	public static final String KERBEROS_KEYTAB_LOCALTION = "KERBEROS_KEYTAB_LOCALTION";
	public static final String KNOX_DEFAULT_POLICY = "KNOX_DEFAULT_POLICY";
	public static final String KNOX_URL = "KNOX_URL";
	public static final String USE_GROUP_LEVEL_USER = "USE_GROUP_LEVEL_USER";
	public static final String USE_PASSWORD_HASHING = "USE_PASSWORD_HASHING";
	public static final String HDFS_KEYTAB_LOCATION = "HDFS_KEYTAB_LOCATION";
	public static final String USER_GUIDE_LINK = "USER_GUIDE_LINK";
	public static final String WHATS_NEW_RELEASE_NOTES = "WHATS_NEW_RELEASE_NOTES";

	/* Decoupling Properties */
	public static final String STORAGE_STRATEGY = "STORAGE_STRATEGY";
	public static final String STORAGE_PATH = "STORAGE_PATH";
	public static final String S3_FS = "S3";
	public static final String SPARK_HOST = "SPARK_HOST";
	public static final String SPARK_PORT = "SPARK_PORT";
	public static final String LOGIN_FAILURE_CNT = "LOGIN_FAILURE_CNT";
	public static final String LOGIN_FAILURE_CNT_RESET_TS = "LOGIN_FAILURE_CNT_RESET_TS";
	public static final String PUSH_TO_DS_COUNT_UPDTE_DELAY = "PUSH_TO_DS_COUNT_UPDTE_DELAY";
	// Schedule number of instance
	public static final String SCHEDULE_NO_OF_INSTANCE = "SCHEDULE_NO_OF_INSTANCE";
	public static final String DATASOURCE_EXPOSE_API_URL = "DATASOURCE_EXPOSE_API_URL";

	public static final String INTERMEDIATE_STORAGE_ENABLED = "INTERMEDIATE_STORAGE_ENABLED";
	public static final String INTERMEDIATE_STORAGE_CONNECTOR_NAME = "INTERMEDIATE_STORAGE_CONNECTOR_NAME";

	public static final String SPARK_CLUSTER_MODE = "SPARK_CLUSTER_MODE";
	// Cognos XML download
	public static final String COGNOS_XML_PATH = "COGNOS_XML_PATH";
}

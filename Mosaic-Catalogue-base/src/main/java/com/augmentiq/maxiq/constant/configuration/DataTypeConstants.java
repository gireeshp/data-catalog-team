package com.augmentiq.maxiq.constant.configuration;

public class DataTypeConstants {
  public static final String STRINGTYPE = "STRINGTYPE";
  public static final String DOUBLETYPE = "DOUBLETYPE";
  public static final String FLOATTYPE = "FLOATTYPE";
  public static final String INTEGERTYPE = "INTEGERTYPE";
  public static final String LONGTYPE = "LONGTYPE";
  public static final String SHORTTYPE = "SHORTTYPE";
  public static final String BYTETYPE = "BYTETYPE";
  public static final String BINARYTYPE = "BINARYTYPE";
  public static final String TIMESTAMPTYPE = "TIMESTAMPTYPE";
  public static final String DATETYPE = "DATETYPE";
  public static final String DECIMALTYPE = "DECIMALTYPE";
  public static final String DOT = ".";
  public static final String UNLIMITED = "UNLIMITED";
  public static final String BOOLEANTYPE = "BOOLEANTYPE";
  public static final String STRING = "STRING";
  public static final String VARCHAR = "VARCHAR";
  public static final String DATE = "DATE";
  public static final String BOOLEAN = "BOOLEAN";
  public static final String DOUBLE = "DOUBLE";
  public static final String LONG = "LONG";
  public static final String INTEGER = "INTEGER";
  public static final String TIMESTAMP = "TIMESTAMP";
  public static final String DECIMAL = "DECIMAL";
  public static final String BIGINT = "BIGINT";
}

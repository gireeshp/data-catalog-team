package com.augmentiq.maxiq.constant.configuration;

public enum InputParamTypes {
  INPUT,
  CALCULATED,
  FILE,
  SYSTEM,
  RCP
}

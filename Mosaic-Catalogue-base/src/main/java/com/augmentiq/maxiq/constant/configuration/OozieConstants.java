package com.augmentiq.maxiq.constant.configuration;

public interface OozieConstants {
  String CORD_JOB_ID = "COORDINATOR_JOBID";
  String COORD_APP_PATH_KEY = "COORDINATOR_APP_PATH";
  String WORKFLOW_APP_PATH_KEY = "WORKFLOW_APP_PATH";
  String START_TIME = "startTime";
  String END_TIME = "endTime";
  String FREQUENCY = "frequency";
  String OOZIE_JOB_NAME = "appName";
  String APP_PATH_KEY = "appPath";
  String JOB_MODE_KEY = "job.mode";
  String JOB_TYPE = "JOB_TYPE";
  String OOZIE_LIB_PATH = "OOZIE_LIB_PATH";
  String OOZIE_APP_NAME_KEY = "oozie.app.name";
  String OOZIE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm'Z'";
  String SCHEDULER_REF_ID = "";
  String CRON = "CRON";
  String JOBNAME = "JOB_NAME";
  String APP_ID = "APP_ID";
  String APPID = "appId";
  String GROUP_ID = "GROUP_ID";
  String GROUPID = "groupId";
  String USER_ID = "USER_ID";
  String USERID = "userId";
  String BASE_CONF_PATH = "BASE_CONF_PATH";
  String BASECONFPATH = "baseConfPath";
}

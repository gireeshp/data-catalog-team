package com.augmentiq.maxiq.constant.configuration.enums;

import java.util.HashMap;
import java.util.Map;

public enum AccessTypeEnum {
  //For DataSource/Index
  USE("USE", 1),
  LOAD("LOAD", 2),
  EDIT("EDIT", 3),
  LOAD_EDIT("LOAD_EDIT", 4),

  //For Project
  REVIEWER("REVIEWER", 5),
  CONTRIBUTOR("CONTRIBUTOR", 6),

  OWNER("OWNER", 0);

  private AccessTypeEnum(String name, Integer value) {
    this.name = name;
    this.value = value;
  }

  private String name;
  private Integer value;

  private static final Map<Integer, AccessTypeEnum> lookup = new HashMap<Integer, AccessTypeEnum>();

  static {
    for (AccessTypeEnum d : AccessTypeEnum.values()) {
      lookup.put(d.getValue(), d);
    }
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }

  public static AccessTypeEnum get(Integer id) {
    return lookup.get(id);
  }
}

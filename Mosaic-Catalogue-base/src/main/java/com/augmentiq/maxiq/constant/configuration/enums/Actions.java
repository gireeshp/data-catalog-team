package com.augmentiq.maxiq.constant.configuration.enums;

public enum Actions {
  CREATE,
  DELETE,
  UPDATE,
  REMOVE
}

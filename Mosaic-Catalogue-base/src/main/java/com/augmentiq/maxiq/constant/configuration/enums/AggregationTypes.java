package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 7/16/2015. */
public enum AggregationTypes {
  Minimum,
  Maximum,
  Sum,
  Average,
  Statistics,
  ExtendedStats,
  Count,
  Percentile,
  Cardinality,
  Terms
}

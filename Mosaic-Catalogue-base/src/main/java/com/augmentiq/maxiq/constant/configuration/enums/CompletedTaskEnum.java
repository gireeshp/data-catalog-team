package com.augmentiq.maxiq.constant.configuration.enums;

public enum CompletedTaskEnum {
  HOURS24,
  LAST3DAYS,
  LASTWEEK,
  ALL,
  AdavncedSearch
}

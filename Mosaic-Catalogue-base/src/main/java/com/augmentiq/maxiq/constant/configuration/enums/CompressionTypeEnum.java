package com.augmentiq.maxiq.constant.configuration.enums;

public enum CompressionTypeEnum {
  GZIP,
  SNAPPY,
  UNCOMPRESSED
}

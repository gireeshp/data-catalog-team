package com.augmentiq.maxiq.constant.configuration.enums;

public enum ConfigurationDataType {
  DATA_REPOSITORY,
  DATA_SOURCE,
  APP_NODE,
  WORK_FLOW_GRAPH,
  WORK_FLOW_PROCESS_INFO,
  OTHER_CONFIGURATIONS,
  CUSTOM_COMPONENT_INSTANCE,
  CONNECTION,
  EXPRESSION,
  STREAM_SOURCE
}

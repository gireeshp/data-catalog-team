package com.augmentiq.maxiq.constant.configuration.enums;

import java.io.Serializable;

public enum ConnectorType implements Serializable {
  ORACLE,
  DB2,
  MYSQL,
  POSTGRES,
  NETEZZA,
  SQL_SERVER,
  SFTP,
  FTP,
  HIVE,
  OTHERS,
  MONGODB;
}

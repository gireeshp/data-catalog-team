package com.augmentiq.maxiq.constant.configuration.enums;

public enum DataFilterType {
  VALUE_RANGE,
  STRING_CONTAINS,
  LIST_CONTAINS
}

package com.augmentiq.maxiq.constant.configuration.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Create for MAX-699 - My Collections & Request from users for datasource
 *
 * @author Ajinkya Marathe
 */
public enum DataRequestEnum {

  /** {@code -1 Request Rejected}. */
  REJECT("Reject", -1),

  REMOVE("Remove", 3),

  /** {@code 0 Request Pending}. */
  PENDING("Pending", 0),

  /** {@code 1 Request Accepted}. */
  ACCEPT("Accept", 1),

  /** {@code 2 NA}. */
  NA("NA", 2);

  private String name;
  private Integer value;

  // Reverse-lookup map for getting a day from an abbreviation
  private static final Map<Integer, DataRequestEnum> lookup =
      new HashMap<Integer, DataRequestEnum>();

  static {
    for (DataRequestEnum d : DataRequestEnum.values()) {
      lookup.put(d.getValue(), d);
    }
  }

  private DataRequestEnum(String name, Integer value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return name;
  }

  public Integer getValue() {
    return value;
  }

  public static DataRequestEnum get(Integer id) {
    return lookup.get(id);
  }

  /** Return a string representation of this status code. */
  @Override
  public String toString() {
    return Integer.toString(value);
  }
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum DataSchedularType {
  MINUTES,
  HOUR,
  DAY,
  WEEK,
  MONTH
}

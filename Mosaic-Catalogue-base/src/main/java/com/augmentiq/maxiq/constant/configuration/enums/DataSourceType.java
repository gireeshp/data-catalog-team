package com.augmentiq.maxiq.constant.configuration.enums;

public enum DataSourceType {
  EXTERNAL_DATA("EXTERNAL_DATA"),
  FILE_DATA_SOURCE("FILE_DATA_SOURCE"),
  RDBMS_DATA_SOURCE("RDBMS_DATA_SOURCE"),
  EXCEL_DATA_SOURCE("EXCEL_DATA_SOURCE"),
  CSV_DATA_SOURCE("CSV_DATA_SOURCE"),
  TSV_DATA_SOURCE("TSV_DATA_SOURCE"),
  FIXED_LENGTH_FORMAT("FIXED_LENGTH_FORMAT"),
  FTP_DATA("FTP_DATA"),
  PUSH_TO_DS("PUSH_TO_DS"),
  STREAM_DS("STREAM_DS"),
  REMOTE_DATA("REMOTE_DATA"),
  NOSQL("NOSQL"),
  RCP("RCP"),
  ORACLE("ORACLE"),
  MYSQL("MYSQL"),
  POSTGRESQL("POSTGRES"),
  SFTP("SFTP"),
  AMAZONS3("AMAZONS3"),
  HDFS("HDFS"),
  COGNOS_DATA_SOURCE("COGNOS_DATA_SOURCE"),
  BLOB_DATA_SOURCE("BLOB_DATA_SOURCE"),
  MONGODB("MONGODB");
	
	
  String dataSourceType;

  DataSourceType(String sourceType) {
    this.dataSourceType = sourceType;
  }

  public String getDataSourceType() {
    return dataSourceType;
  }
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum DataStatus {
  CREATED,
  ERRORED,
  RUNNING,
  WAITING,
  UPDATED
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum DataValidatorType {
  IN_VALUES,
  NOT_NULL,
  LENGTH_CHECK,
  DATE_FORMAT,
  LIST_CONTAINS,
  NOT_IN_VALUES
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum EvaluationCondition {
  ANY_OF_FIELD,
  CONCAT_ALL_FIELDS
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum FieldDataTypes {
	STRING, DATE, BOOLEAN, DOUBLE, LONG, INTEGER, TIMESTAMP, DECIMAL, ARRAY_OF_DOUBLE, ARRAY_OF_STRING,
	/* NUMERIC */

	/*
	 * Types introduced for NoSql
	 */
	MAP, ROWID, LIST, HASHMAP, LINKEDHASHMAP, ARRAYLIST, DOCUMENT, ARRAY, INT32, INT64, DECIMAL128, NULL, DATE_TIME,NUMBER, OBJECTID, BINARY;
}

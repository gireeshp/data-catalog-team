package com.augmentiq.maxiq.constant.configuration.enums;

public enum FileStoreObject {
  HDFS,
  HBASE,
  S3,
  BLOB,
  GOOGLE_CLOUD,
  SOURCE
}

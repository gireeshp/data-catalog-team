package com.augmentiq.maxiq.constant.configuration.enums;

public enum FileTypeEnum {
  TEXT,
  PARQUET,
  ANY,
  CSV,
  TSV
}

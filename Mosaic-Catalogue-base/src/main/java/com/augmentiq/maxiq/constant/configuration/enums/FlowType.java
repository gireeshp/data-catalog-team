package com.augmentiq.maxiq.constant.configuration.enums;

public enum FlowType {
  PERIODIC,
  STREAM,
  RCP
}

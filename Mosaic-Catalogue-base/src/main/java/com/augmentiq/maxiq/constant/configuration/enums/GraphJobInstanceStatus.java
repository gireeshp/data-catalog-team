package com.augmentiq.maxiq.constant.configuration.enums;

public enum GraphJobInstanceStatus {
  new_(0),
  inpro_(1),
  RUNNING(2),
  run_(3),
  apprun_(5),
  finished_(6),
  errorFinish_(7),
  aborted_(8),
  killed_(9);

  GraphJobInstanceStatus(int level) {}
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum HadoopJobState {
  RUNNING(1),
  SUCCEEDED(2),
  FAILED(3),
  PREP(4),
  ABORTED(5),
  DIDNOTRUN(6),
  PENDING(7);
  int value;

  HadoopJobState(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}

package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 8/5/15. */
public enum IncrementalUpdateTypes {
  APPEND,
  MERGE,
  DELETE
}

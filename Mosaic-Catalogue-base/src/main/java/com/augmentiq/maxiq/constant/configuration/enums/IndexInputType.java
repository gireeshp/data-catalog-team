package com.augmentiq.maxiq.constant.configuration.enums;

public enum IndexInputType {
  FLAT_FIELDS,
  JSON_INPUT
}

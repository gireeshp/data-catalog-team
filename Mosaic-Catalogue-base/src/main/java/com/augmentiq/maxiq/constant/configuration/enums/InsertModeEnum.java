package com.augmentiq.maxiq.constant.configuration.enums;

public enum InsertModeEnum {
  APPEND_ALL,
  REPLACE_ALL
}

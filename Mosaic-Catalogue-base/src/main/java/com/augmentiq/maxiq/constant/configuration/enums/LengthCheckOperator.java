package com.augmentiq.maxiq.constant.configuration.enums;

public enum LengthCheckOperator {
  EQUAL,
  NOT_EQUAL,
  GREATER_THEN,
  LESS_THEN,
  GREATER_THEN_EQUAL,
  LESS_THEN_EQUAL
}

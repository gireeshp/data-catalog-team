package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 8/4/2015. */
public enum LoadStategyInputType {
  FILEPATH,
  DATASOURCE,
  RDBMS
}

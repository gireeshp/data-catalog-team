package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 8/4/2015. */
public enum LoadStategyType {
  REPLACEALL,
  APPENDALL,
  UPDATE
}

package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 8/7/2015. */
public enum LoadStrategyMergeType {
  CUSTOM,
  REPLACELATEST
}

package com.augmentiq.maxiq.constant.configuration.enums;

import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;

public enum ModelsEnum {
  APP_ID("appId"),
  MODEL_TYPE("modelType"),
  MODEL_PATH("modelPath"),
  FEATURE_DATA("featureData"),
  REQUEST_ID("requestId"),
  CONSUMER_TOPIC(Cache.getProperty(CacheConstants.CONSUMER_TOPIC)),
  PRODUCER_TOPIC(Cache.getProperty(CacheConstants.PRODUCER_TOPIC));

  private String value;

  ModelsEnum(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }
}

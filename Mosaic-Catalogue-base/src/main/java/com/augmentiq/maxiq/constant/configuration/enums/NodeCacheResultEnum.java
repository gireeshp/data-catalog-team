package com.augmentiq.maxiq.constant.configuration.enums;

public enum NodeCacheResultEnum {
  MEMORY_ONLY,
  MEMORY_AND_DISK,
  MEMORY_ONLY_SER,
  MEMORY_AND_DISK_SER,
  DISK_ONLY
}

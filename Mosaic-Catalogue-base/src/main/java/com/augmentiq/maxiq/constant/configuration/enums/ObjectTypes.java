package com.augmentiq.maxiq.constant.configuration.enums;

public enum ObjectTypes {
  DATA_SOURCE,
  FLOW,
  NOTEBOOK,
  VISUALIZATION,
  PROJECT,
  INDEX
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum ProfilingEnum {
  MEDIAN,
  MAX_VALUE,
  MISSING_COUNT,
  MIN_VALUE,
  MEAN,
  STD_DEV,
  MODE,
  PERCENTILE_1,
  PERCENTILE_2,
  PERCENTILE_3,
  PERCENTILE_4,
  PERCENTILE_5,
  PERCENTILE_95,
  PERCENTILE_96,
  PERCENTILE_97,
  PERCENTILE_98,
  PERCENTILE_99,
  VARIANCE,
  PERCENTILE_100;

  public ProfilingEnum getNext() {
    return values()[(ordinal() + 1) % values().length];
  }
}

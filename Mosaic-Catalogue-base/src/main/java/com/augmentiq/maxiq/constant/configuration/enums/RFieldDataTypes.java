package com.augmentiq.maxiq.constant.configuration.enums;
/** @author Rushi created on 29-Mar-17 for MAX-720 */
public enum RFieldDataTypes {
  character,
  numeric,
  Date,
  logical
}

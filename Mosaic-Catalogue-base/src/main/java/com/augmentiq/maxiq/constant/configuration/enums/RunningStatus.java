package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 7/3/15. */
public enum RunningStatus {
  RUNNING,
  STOPPED,
  ERRORED,
  CREATED,
  INDEXED
}

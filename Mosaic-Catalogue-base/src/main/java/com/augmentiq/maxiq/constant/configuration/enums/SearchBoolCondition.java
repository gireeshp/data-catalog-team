package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 7/14/2015. */
public enum SearchBoolCondition {
  must,
  must_not,
  should
}

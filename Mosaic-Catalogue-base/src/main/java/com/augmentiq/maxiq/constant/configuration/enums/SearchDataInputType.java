package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 7/17/2015. */
public enum SearchDataInputType {
  DATASOURCE,
  APP_NODE
}

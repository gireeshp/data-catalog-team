package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by shivanand on 7/14/2015. */
public enum SearchQueryType {
  QUERY,
  FILTER
}

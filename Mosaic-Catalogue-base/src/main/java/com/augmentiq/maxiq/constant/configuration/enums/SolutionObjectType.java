package com.augmentiq.maxiq.constant.configuration.enums;

/** Created by Mayuri Narawade on 4/1/2016. */
public enum SolutionObjectType {
  Flow,
  Notebook,
  DataSource
}

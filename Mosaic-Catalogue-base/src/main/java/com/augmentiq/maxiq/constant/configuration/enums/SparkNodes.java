package com.augmentiq.maxiq.constant.configuration.enums;

public enum SparkNodes {
  node_rdbms,
  node_transpose,
  node_group_by,
  node_filters,
  node_sort,
  node_merge,
  node_join,
  node_co_locate,
  node_sampling,
  node_de_duplication,
  node_statistical_profiling,
  node_histogram,
  node_distinct,
  node_frequency_distribution,
  node_classification,
  node_regression,
  node_clustering,
  node_custom_processes,
  node_create_variables,
  node_feature_extraction,
  node_look_up,
  node_datarepository,
  node_hdfs_location_in,
  node_hdfs_location_out,
  test,
  node_Custom_Query,
  node_mJoin,
  node_appsNode,
  node_nosql,
  node_window,
  node_save_for_visualization,
  node_push_to_datasource,
  node_push_to_ds_profiling,
  node_push_to_ds_indexing,
  node_dimension_reduction,
  node_outlier_treatment,
  node_correlation,
  node_ml_classification_train,
  node_ml_regression_train,
  node_ml_clustering_train,
  node_ml_use,
  node_ml_text_data_cleansing,
  node_ml_feature_extraction,
  node_ml_normalize,
  node_delete_from_hbase
}

package com.augmentiq.maxiq.constant.configuration.enums;

public enum TransformationTypes {
  EQUALS,
  CONTAINS,
  OTHER
}

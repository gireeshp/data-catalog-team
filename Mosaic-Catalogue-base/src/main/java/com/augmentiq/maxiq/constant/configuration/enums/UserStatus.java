package com.augmentiq.maxiq.constant.configuration.enums;

public enum UserStatus {
  ACTIVE,
  INACTIVE,
  DELETED;
}

package com.augmentiq.maxiq.constant.configuration.enums.database.functions;

public enum DatabaseFunctionTypes {
  MATH(1),
  STRING(2),
  DATE(3),
  AGGREGATE(4),
  NUMERIC(5),
  FX(6);
  int value;

  DatabaseFunctionTypes(int value) {
    this.value = value;
  }

  // to get value (ex: 1,2,3,..)
  public int getValue() {
    return value;
  }

  // to get name (ex: HIVE,HBASE,..)
  public static String getName(int value) {
    for (DatabaseNames e : DatabaseNames.values()) {
      if (value == e.value) return e.name().toString();
    }
    return null;
  }
}

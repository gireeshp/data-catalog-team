package com.augmentiq.maxiq.constant.configuration.enums.database.functions;

public enum DatabaseNames {
  HIVE(1),
  HBASE(2),
  SPARK_SQL(3),
  SPARK_UDF(4);
  int value;

  DatabaseNames(int value) {
    this.value = value;
  }

  // to get value (ex: 1,2,3,..)
  public int getValue() {
    return value;
  }

  // to get name (ex: HIVE,HBASE,..)
  public static String getName(int value) {
    for (DatabaseNames e : DatabaseNames.values()) {
      if (value == e.value) return e.name().toString();
    }
    return null;
  }
}

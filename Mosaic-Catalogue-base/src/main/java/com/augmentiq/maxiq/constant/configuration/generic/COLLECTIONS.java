package com.augmentiq.maxiq.constant.configuration.generic;

public class COLLECTIONS {

  public static final String MAXIQ_RESOURCE = "MAXIQ_RESOURCE";

  public static class DATA_REPOSITORY {
    public static final String ID = "id";
    public static final String REPO_NAME = "repoName";
    public static final String DATA_SOURCES = "dataSources";
    public static final String COMBINER_MAP = "combinerMap";
    public static final String SCHEDULAR = "schedular";
    public static final String DATA_SOURCE_ID = "dataSourceId";
    public static final String FIELD_IDS = "fieldIds";
    public static final String COMBINER_MAP_STORE = "combinerMapStore";
    public static final String DATA_SOURCES_STORE = "dataSourcesStore";
  }

  public static class JOB_SCHEDULAR {
    public static final String MANUAL = "manual";
    public static final String REPEAT = "repeat";
    public static final String WEEK_DAYS = "weekDays";
    public static final String FETCH_TIME = "fetchTime";
    public static final String MONTH_DATES = "monthDates";

    public static final String TIME_HOUR = "timeHour";
    public static final String TIME_MINUTE = "timeMinute";
    public static final String TIME_SERIES = "timeSeries";
  }

  public static class FIELD_MAPPING {
    public static final String ID = "id";
    public static final String DATA_SOURCE_ID = "dataSourceId";
    public static final String FIELD_NAME = "fieldName";
    public static final String FIELD_DATA_TYPE = "fieldDataType";
    public static final String REPLACE_NULL_WITH = "replaceNullWith";
    public static final String SAMPLE_RECORD1 = "sampleRecord1";
    public static final String SAMPLE_RECORD2 = "sampleRecord2";
    public static final String SAMPLE_RECORD3 = "sampleRecord3";
    public static final String FORMAT = "format";
    public static final String DATA_VALIDATION = "dataValidations";
    public static final String DATA_FILTERS = "dataFilters";
    public static final String DATA_TRANSFORMERS = "dataTransformers";
    public static final String FIELD_POSITION = "fieldPosition";
  }

  public static class DATA_CLEANSER {
    public static final String ID = "id";
    public static final String DATA_SOURCE_ID = "dataSourceId";
    public static final String FROM_VALUE = "fromValue";
    public static final String TO_VALUE = "toValue";
    public static final String REGX_IND = "regxInd";
  }

  public static class DATA_VALIDATION {
    public static final String VALIDATOR = "validator";
    public static final String LENGTH_RANGE_FROM = "lengthRangeFrom";
    public static final String LENGTH_RANGE_TO = "lengthRangeTo";
  }

  public static class DATA_FILTER {
    public static final String DATA_FILTER_TYPE = "dataFilterType";
    public static final String FROM_VALUE = "fromValue";
    public static final String TO_VALUE = "toValue";
    public static final String MATCHING_STRING = "matchingString";
    public static final String MATCHING_LIST = "matchingList";
    public static final String EXACT_MATCH = "exactMatch";
    public static final String CASES_SENSITIVE = "caseSensitive";
  }

  public static class DATA_TRANSFORMERS {

    public static final String INPUT_FIELD_POS = "inputFieldsPos";
    public static final String FROM_VALUE = "fromValue";
    public static final String TO_VALUE = "toValue";
    public static final String REPLACE_ALL = "replaceAll";
    public static final String TRANSFORMATION_SERIES = "transforamtionSeries";
    public static final String TYPE = "type";

    public static final String NAME = "name";
    public static final String OUTFIELD_POS = "outputFieldsPos";
    public static final String OUT_DEL = "outDel";
    public static final String INFIELD_POS_FOR_API = "inFieldPosForApi";
    public static final String API_DOMAIN = "apiDomain";
  }

  public static class DATA_SOURCE {
    public static final String ID = "id";
    public static final String DATA_REPO_ID = "dataRepoId";
    public static final String DATA_SOURCE_TYPE = "dataSourceType";

    public static final String DATA_SOURCE_NAME = "dataSourceName";
    public static final String FIELD_MAPPINGS = "fieldMappings";
    public static final String FIELD_MAPPINGS_STORE = "fieldMappingsStore";
    public static final String HEADER_STRING = "headerString";
    public static final String DATA_SOURCE_IDENTIFIERS = "dsIndentifiers";
    public static final String ADVANCED_VALIDATIONS = "advancedValidations";
    public static final String ADVANCED_VALIDATIONS_STORE = "advancedValidationsStore";
    public static final String TRANSFORMATIONS = "transformations";
    public static final String DATA_CLEANSERS = "dataCleansers";
    public static final String DATA_CLEANSER_STORE = "dataCleansersStore";

    public static final String FILE_PATH_NAME = "filePathName";
    public static final String LOCAL_PATH_NAME = "localPathName";
    public static final String DELIMITER = "delimiter";
    public static final String CONTAINS_HEADER = "containsHeader";

    public static final String CONNECTION_CONFIG = "connectionConfig";
    public static final String DATA_SOURCE_URL = "dataSourceUrl";
    public static final String USER_NAME = "userName";
    public static final String PASSWORD = "password";
    public static final String DATA_BASE_TYPE = "dataBaseType";
    public static final String QUERY = "query";

    public interface CONNECTOR_CONFIG {
      public static String DB_USERNAME = "dbUserName";
      public static String DB_PASSWORD = "dbPassword";
      public static String DB_DRIVER_CLASS_NAME = "driverCLassName";
      public static String CONNECT_STRING = "connectString";
      public static String IP = "ip";
      public static String PORT = "port";
      public static String CONNECTOR_TYPE = "connectorType";
      public static String QUERY_TO_EXECUTE = "queryToExecute";
      public static String DB_NAME = "dbName";
    }
  }

  public static class ADVANCED_VALIDATIONS {
    public static final String ID = "id";
    public static final String FIELDS = "fields";
    public static final String FIELDS_STORE = "fieldsStore";
    public static final String EVAL_CONDITION = "evalCondition";
    public static final String DATA_FILTERS = "dataFilters";
    public static final String DATA_VALIDATIONS = "dataValidations";
  }

  public static class RECORD_COMBINER {
    public static final String LEFT_DS_ID = "leftDsId";
    public static final String RIGHT_DS_ID = "rightDsId";
    public static final String QUERY_STRING = "queryString";
    public static final String MAPPING_KEYS = "mappingKeys";

    public static final String LEFT_KEY = "leftKey";
    public static final String RIGHT_KEY = "rightKey";
  }

  public static class USERS {
    public static final String ID = "id";
    public static final String USER_FIRST_NAME = "userFirstName";
    public static final String USER_MIDDLE_NAME = "userMiddleName";
    public static final String USER_LAST_NAME = "userLastName";
    public static final String USERNAME = "userName";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String USER_CREATE_DATE_TIME = "userCreateDateTime";
    public static final String LAST_LOGIN_DATE_TIME = "lastLoginDateTime";
    public static final String PWD_CHANGE_DATE_TIME = "pwdChangeDateTime";
    public static final String LOCK_IND = "lockInd";
    public static final String DISABLE_IND = "disableInd";
    public static final String PWD_RST_IND = "pwdRstInd";
    public static final String STATUS = "status";
    public static final String LOGIN_HISTORY_ID = "userLoginHistoryId";
    public static final String FALSE_LOGIN_CNT = "falseLoginCnt";
    public static final String LOCK_DATE_TIME = "lockDateTime";
    public static final String DISABLE_DATE_TIME = "disableDateTime";
  }

  public static class USERS_LOGIN_HISTORY {
    public static final String ID = "id";
    public static final String USERID = "userId";
    public static final String USERNAME = "userName";
    public static final String IPADDRESS = "ipAddress";
    public static final String LOGIN_DATE_TIME = "loginDateTime";
    public static final String LOGOUT_DATE_TIME = "logoutDateTime";
  }

  public static class JOB_HANDLER {
    public static final String JOB_NAME = "jobName";
    public static final String JOB_ID = "jobId";
    public static final String JOB_STATUS = "jobStatus";
    public static final String JOB_TYPE = "jobType";
    public static final String JOB_INPUT = "jobInput";
    public static final String JOB_OUTPUT = "jobOutput";
    public static final String JOB_STARTED_TIME = "jobStartedTime";
    public static final String JOB_END_TIME = "jobEndTime";
  }

  public static class ERRORED_RECORDS {
    public static final String ERRORED_RECORD = "erroredRecord";
    public static final String ERRORED_TYPE = "erroredType";
    public static final String ERROR_FIELD = "errorField";
    public static final String JOB_ID = "jobId";
  }
}

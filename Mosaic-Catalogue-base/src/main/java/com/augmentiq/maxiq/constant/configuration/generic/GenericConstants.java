package com.augmentiq.maxiq.constant.configuration.generic;

public class GenericConstants {
  public static final String PARAM_ID = "paramId";
  public static final String PARAM_PARENT_ID = "paramParentId";
  public static final String PARAM_TYPE = "paramType";
  public static final String PARAM_VALUE = "paramValue";
  public static final String PARAM_NAME = "paramName";
  public static final String PARAM_STATUS = "paramStatus";

  public static final String APPEND = "Append";
  public static final String MERGE = "Merge";
  public static final String DELETE = "Delete";
  public static final String REPLACE = "Replace";

  //App Combiner types
  public static class APP_COMBINER_TYPES {
    public static final String NUMBER = "Number";
    public static final String STRING = "String";
    public static final String DATE = "Date";
    public static final String TIMESTAMP = "Timestamp";
    public static final String FILE = "File";
    public static final String INTEGER = "Integer";
    public static final String LONG = "Long";
    public static final String DOUBLE = "Double";
    public static final String DECIMAL = "Decimal";
  }

  public static class PARAM_TYPES {
    public static final String DBOBJECT = "dbObject";
    public static final String STRING = "string";
    public static final String LONG = "long";
    public static final String BOOLEAN = "boolean";
  }

  public static class PARAM_NAMES {
    public static final String DATA_SOURCE = "dataSource";
    public static final String DATA_REPO = "dataRepo";
    public static final String FIELD_MAPPING = "fieldMapping";
    public static final String ADVANCED_VALIDATIONS = "advancedValidation";
  }

  public static class PARAM_STATUS_VALUE {
    public static final Long ACTIVE = 1L;
    public static final Long DEACTIVATED = 2L;
  }

  public static class DATA_SOURCE {
    public static final String DATASOURCENAME = "datasourcename";
    public static final String REPONAME = "repoName";
    public static final String INGECTION = "ingection";
  }
}

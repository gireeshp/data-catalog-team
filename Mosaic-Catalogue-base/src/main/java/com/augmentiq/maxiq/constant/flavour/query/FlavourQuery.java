package com.augmentiq.maxiq.constant.flavour.query;

public class FlavourQuery {

  public static String PERSONAS_SQL =
      "		select * from flavour_persona_mappings where flavourId in ( "
          + "					select flavourId from client where clientId in ( 				"
          + "						select clientId from user_client_mapping where unqUserId =  loggedInUserId "
          + "					) "
          + "		) ";

  public static String ACTIONS_SQL =
      "select * from actions_master where actionsMasterId in ( "
          + "		select actionsMasterId from flavour_action_mappings where flavourId in ( "
          + "					select flavourId from client where clientId in ( 				"
          + "						select clientId from user_client_mapping where unqUserId =  loggedInUserId "
          + "					) "
          + "		) "
          + "	)";

  public static String BLOCKED_URL_SQL =
      "		select id, actionsMasterId, url from blocked_url_action_mappings where actionsMasterId not in ( "
          + "				select actionsMasterId from flavour_action_mappings where flavourId in (	 "
          + "					select flavourId from client where clientId in ( "
          + "						select clientId from user_client_mapping where unqUserId =  loggedInUserId "
          + "					) "
          + "				) "
          + "			) "
          + " union "
          + " select id, personaId as actionsMasterId, url from blocked_url_persona_mappings where personaId not in ( "
          + "			select personaId from flavour_persona_mappings where flavourId in ( "
          + "				select flavourId from client where clientId in (  "
          + "					select clientId from user_client_mapping where unqUserId =  loggedInUserId "
          + "				) "
          + "			)  	 "
          + ") ";
}

package com.augmentiq.maxiq.constant.globalsearch;

/*
 * Ajinkya Marathe
 * Required to maintain global search constants which are used in creation of index in elastic search
 */
public class GlobalSearchCosntants {

  // Indexing parameters
  // public static final String INDEX_NAME = "global_search1";
  public static final String INDEX_TYPE = "datasource_and_flow";

  // Document comman parameters
  public static final String DOCUMENT_ID = "ds_or_flow_id";
  public static final String DOCUMENT_NAME = "name";
  public static final String DOCUMENT_TYPE = "type";
  public static final String DOCUMENT_CREATED_BY = "created_by";
  public static final String DOCUMENT_CREATED_DATE = "created_date";
  public static final String DOCUMENT_GROUP_ID = "group_id";

  public static final String DOCUMENT_CATEGORY = "category";
  public static final String DOCUMENT_SUBCATEGORY = "sub_category";
  public static final String DOCUMENT_SOURCETYPE = "source_type";
  public static final String DOCUMENT_DATAATREST = "data_at_rest";
  public static final String DOCUMENT_LOADSTRATEGY = "load_strategy";
  public static final String DOCUMENT_DATAREPO = "data_repo";
  public static final String DOCUMENT_USERGROUP = "user_group";
  public static final String DOCUMENT_PROJECTSNAME = "projects_name";
  public static final String DOCUMENT_AVGRATING = "avg_rating";

  public static final String DOCUMENT_DATASOURCE_GLOBAL_INPUT_PARAMETERS =
      "datasource_global_input_parameters";
  public static final String DOCUMENT_FLOW_GLOBAL_INPUT_PARAMETERS = "flow_global_input_parameters";
  public static final String DOCUMENT_DATASOURCE_GLOBAL_INPUT_PARAMETER_VALUE = "value";
  public static final String DOCUMENT_FLOW_GLOBAL_INPUT_PARAMETER_TYPE = "input_parameter_type";

  // public static final String DOCUMENT_FIELD = "field";
  public static final String DOCUMENT_FIELD_NAME = "name";
  public static final String DOCUMENT_FIELD_TYPE = "type";
  public static final String DOCUMENT_FIELD_FORMAT = "format";
  public static final String DOCUMENT_FIELD_TAGS = "tags";
  public static final String DOCUMENT_FIELD_TAG_NAME = "tag_name";

  // Document datasource parameters
  public static final String DOCUMENT_DS_FIELDS = "ds_fields";
  public static final String DOCUMENT_DS_FIELPATH = "filepath";

  // RDBMS
  public static final String DOCUMENT_DS_QUERY = "query";
  public static final String DOCUMENT_DS_TYPE = "ds_type";
  public static final String DOCUMENT_DS_CONNECTOR_TYPE = "ds_connector_type";
  public static final String DOCUMENT_DS_CONFIG_CONNECTION_TYPE = "ds_config_connection_type";
  public static final String DOCUMENT_DS_PRIMARY_KEY = "ds_primary_key";
  public static final String DOCUMENT_DS_IP_ADDRESS = "ip_address";
  public static final String DOCUMENT_DS_DB_PORT = "database_port";
  public static final String DOCUMENT_DS_CONNECTION_NAME = "connection_name";
  public static final String DOCUMENT_DS_DB_USERNAME = "db_username";
  public static final String DOCUMENT_DS_DATABASE_NAME = "database_name";

  // DELIMITED FILE
  public static final String DOCUMENT_DS_DELIMITER = "ds_delimiter";

  // Document flow parameters
  public static final String DOCUMENT_FL_NODES = "nodes";
  public static final String DOCUMENT_FL_NODE_NAME = "node_name";
  public static final String DOCUMENT_FL_NODE_TYPE = "node_type";

  // Document flow Input
  public static final String DOCUMENT_FL_INPUT = "input";
  public static final String DOCUMENT_FL_INPUT_DATAFILES = "input_data_files";
  public static final String DOCUMENT_FL_INPUT_DATAFILE = "input_data_file";
  public static final String DOCUMENT_FL_INPUT_DATAFILE_IN_CONNECTOR =
      "input_data_file_in_connector";
  public static final String DOCUMENT_FL_IN_FIELDS = "node_in_fields";

  // Document flow Output
  public static final String DOCUMENT_FL_OUTPUT = "output";
  public static final String DOCUMENT_FL_OUTPUT_DATAFILES = "output_data_files";
  public static final String DOCUMENT_FL_OUTPUT_DATAFILE = "output_data_file";
  public static final String DOCUMENT_FL_OUTPUT_DATAFILE_OUT_CONNECTOR =
      "output_data_file_out_connector";
  public static final String DOCUMENT_FL_OUTPUT_FIELDS = "node_out_fields";

  // Document flow PROCESS
  public static final String DOCUMENT_FL_PROCESS = "process";
  public static final String DOCUMENT_FL_BASE_PROCESSES = "base_processes";
  public static final String DOCUMENT_FL_PROCESS_TYPE = "process_type";

  // PROCESS->Custom Query
  public static final String DOCUMENT_FL_PROCESS_CUSTOM_QUERY = "custom_query";
  public static final String DOCUMENT_FL_PROCESS_QUERY = "process_query";

  // PROCESS->Filter
  public static final String DOCUMENT_FL_PROCESS_FILTER = "filter";
  public static final String DOCUMENT_FL_PROCESS_FILTER_EXPRESSION = "process_filter_expression";

  // PROCESS->Join
  public static final String DOCUMENT_FL_PROCESS_JOIN = "process_join";
  public static final String DOCUMENT_FL_PROCESS_JOIN_TYPE = "process_join_type";
  public static final String DOCUMENT_FL_PROCESS_JOIN_LEFTDATASOURCE = "process_left_datasource";
  public static final String DOCUMENT_FL_PROCESS_JOIN_RIGHTDATASOURCE = "process_right_datasource";
  public static final String DOCUMENT_FL_PROCESS_JOIN_FILTER = "process_join_filter";
  public static final String DOCUMENT_FL_PROCESS_JOIN_FILTER_EXPRESSION =
      "process_join_filter_expression";
  public static final String DOCUMENT_FL_PROCESS_JOIN_CONDITIONS = "process_join_conditions";
  public static final String DOCUMENT_FL_PROCESS_JOIN_CONDITIONS_LEFT_FIELDS =
      "process_join_conditions_left_fields";
  public static final String DOCUMENT_FL_PROCESS_JOIN_CONDITIONS_RIGHT_FIELDS =
      "process_join_conditions_right_fields";

  // PROCESS->Group
  public static final String DOCUMENT_FL_PROCESS_GROUP = "process_group";
  public static final String DOCUMENT_FL_PROCESS_GROUP_FIELDS = "process_group_fields";
  // public static final String DOCUMENT_FL_PROCESS_GROUP_FIELD =
  // "process_group_field";
  public static final String DOCUMENT_FL_PROCESS_GROUP_DEFS = "process_group_defs";
  // public static final String DOCUMENT_FL_PROCESS_GROUP_DEF =
  // "process_group_def";
  public static final String DOCUMENT_FL_PROCESS_GROUP_DEF_FIELD = "process_group_def_field";
  public static final String DOCUMENT_FL_PROCESS_GROUP_PROCESS = "process_group_process";
  public static final String DOCUMENT_FL_PROCESS_GROUP_OUTPUT_VAR_FIELD_TYPE =
      "process_group_output_var_field_type";
  public static final String DOCUMENT_FL_PROCESS_GROUP_OUTPUT_VAR = "process_group_output_var";
  public static final String DOCUMENT_FL_PROCESS_GROUP_PROCESS_FILTER =
      "process_group_process_filter";
  public static final String DOCUMENT_FL_PROCESS_GROUP_PROCESS_FILTER_EXPRESSION =
      "process_group_process_filter_expression";

  // Process- Add Var MATHS
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_MATH = "process_add_var_math";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_MATH_OP_VAR_FLDTYPE =
      "process_add_var_math_op_var_field_type";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_MATH_OP_VAR =
      "process_add_var_math_op_var";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_MATH_EXPRESSION =
      "process_add_var_math_expression";

  // Process- Add Var Binning
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING = "process_add_var_binning";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_ON_FIELD =
      "process_add_var_binning_field";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_ON_VALUE =
      "process_add_var_binning_values";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_ON_VALUE_DATA_TYPE =
      "process_add_var_binning_data_type";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_RANGES =
      "process_add_var_binning_ranges";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_TYPE =
      "process_add_var_binning_type";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_ELSE_CONDITION =
      "process_add_var_binning_else";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_OUTPUT_VAR_FIELD_TYPE =
      "process_add_var_binning_output_var_type";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_BINNING_OUTPUT_VAR =
      "process_add_var_binning_output_var";

  // Process- Add Var SEGMENT
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_SEGMENT = "process_add_var_segment";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_SEGMENT_DATATYPE =
      "process_add_var_segment_datatype";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_SEGMENT_OUTPUT_VAR =
      "process_add_var_segment_output_var";
  public static final String DOCUMENT_FL_PROCESS_ADD_VAR_SEGMENT_EXPR =
      "process_add_var_segment_expr";

  // QueryBuilderContants
  public static final String QUERY_ALL = "_all";
  public static final String QUERY_FIELD_CREATED_BY = "datasource_and_flow.created_by";
  public static final String QUERY_FIELD_SOURCE_TYPE = "datasource_and_flow.source_type";
  public static final String QUERY_FIELD_DATA_AT_REST = "datasource_and_flow.data_at_rest";
  public static final String QUERY_FIELD_LOAD_STRATEGY = "datasource_and_flow.load_strategy";
  public static final String QUERY_FIELD_DATA_REPO = "datasource_and_flow.data_repo";
  public static final String QUERY_FIELD_USER_GROUP = "datasource_and_flow.user_group";
  public static final String QUERY_FIELD_PROJECTS_NAME = "datasource_and_flow.projects_name";
  public static final String QUERY_FIELD_AVG_RATING = "datasource_and_flow.avg_rating";
  public static final String QUERY_FIELD_CREATED_DATE = "datasource_and_flow.created_date";
  public static final String QUERY_FIELD_FLOW_DS_NAME = "datasource_and_flow.name";
  public static final String QUERY_FIELD_DS_QUERY = "datasource_and_flow.query";
  public static final String QUERY_FIELD_TYPE = "datasource_and_flow.type";
  public static final String QUERY_FIELD_FLOW_QUERY =
      "datasource_and_flow.nodes.process.base_processes.custom_query.process_query";
  public static final String QUERY_FIELD_GROUP_ID = "datasource_and_flow.group_id";
  public static final String QUERY_FIELD_DS_FIELDS_NAME = "ds_fields.name";
  public static final String QUERY_FIELD_FLOW_INPUT_FIELDS_NAME =
      "nodes.input.input_data_files.node_in_fields.name";
  public static final String QUERY_FIELD_FLOW_OUTPUT_FIELDS_NAME =
      "nodes.output.output_data_files.node_out_fields.name";
  public static final String QUERY_FIELD_TAGS = "datasource_and_flow.tags.tag_name";
  public static final String QUERY_FIELD_FIELD_LEVEL_TAGS =
      "datasource_and_flow.ds_fields.tags.tag_name";
  public static final String QUERY_SUB_CATEGORY = "datasource_and_flow.sub_category";
  public static final String QUERY_CATEGORY = "datasource_and_flow.category";

  // DateFormates
  public static final String DATE_FORMAT_FOR_QUERY = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  public static final String DATE_FORMAT_FOR_GSON_LOAD = "yyyy-MM-dd'T'HH:mm:ssX";

  // Messages
  public static final String SUCCESS = "success'";
  public static final String NO_RECORDS_FOUND = "No record found'";
  public static final String SOME_PROBLEM_IN_PROCESSING_RESPONSE =
      "Some problem in processing response";
}

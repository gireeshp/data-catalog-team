package com.augmentiq.maxiq.constant.log.logger.domains;

public enum LoggerMessaageConstants {
  INFO,
  DELETE,
  LOGGER_TYPE,
  LOGGER_MESSAGE,
  ERROR
}

package com.augmentiq.maxiq.constant.storage.strategy;

import com.augmentiq.maxiq.cache.support.Cache;
import com.augmentiq.maxiq.constant.cache.CacheConstants;
import com.augmentiq.maxiq.constant.configuration.enums.FileStoreObject;

public class FileStoreObjectStorageStrategy {
	
	 /**
	   * This function returns the FileStoreObject enum value after checking the type of storage
	   * strategy Mosaic is configured for, eg. HDFS, S3, BLOB
	   *
	   * @return FileStoreObject Enum
	   */
	  public static FileStoreObject getFileStoreObjectFromStorageStrategy() {
	    try {
	      if (Cache.getProperty(CacheConstants.STORAGE_STRATEGY).isEmpty()
	          || Cache.getProperty(CacheConstants.STORAGE_STRATEGY) == null)
	        return FileStoreObject.HDFS;
	      else return FileStoreObject.valueOf(Cache.getProperty(CacheConstants.STORAGE_STRATEGY));
	    } catch (Throwable e) {
	      return FileStoreObject.HDFS;
	    }
	  }

}

package com.augmentiq.maxiq.constant.workFlow.general;

import com.augmentiq.constant.maxiq.GlobalParams;

public class Constants {
	public static final String JOB_INSTANCES_COLLECTION = "job_instances";
	public static final String APPVIEWINSTANCE = "appViewInstance";
	public static final String JOB_NAME = "jobName";
	public static final String INDICATOR = "indicator";
	public static final String CONFIG = "config";
	public static final String JOB_INSTANCE_ID = "jobInstId";
	public static final String TYPE = "type";
	public static final String QUERY = "query";
	public static final String DELIMITER = "delimiter";
	public static final String ISHBASE = "isHbase";
	public static final String NULL = "null";
	public static final String FTPSFTP = "FTP/SFTP";
	public static final String RDBMS = "RDBMS";
	public static final String START_TIME = "startTime";
	public static final String END_TIME = "endTime";
	public static final String PARENT_JOB_INSTANCE_ID = "parentJobInstId";
	public static final String XSRF_TOKEN = "maxiq";
	public static final String LEVEL = "level";
	public static final String TOPIC = "topic";
	public static final String MESSAGE_PARAM = "messageParam";
	 public static final String DIST_FILE_PATH = GlobalParams.MAXIQ_DATA_PATH + "/distCache/";
	public static final String HYPHEN = "-";
	public static final String DR_ID = "drId";
	public static final String BAAP_JOB_INST_ID = "baapJobInstId";
	// add for workFlow Canvas 
	public static final String WORKFLOW_GRAPH = "workflow_graph";
	public static final String WORKFLOW_PROCESS_INFO = "workflow_process_info";
	public static final String CUSTOM_QUERY_FIELD_MAPPING = "customQueryFieldMapping";
	public static final String MAXIQ_APPS = "maxiq_apps";
	
	public static final String APP_GRAPH_NODES = "app_graph_nodes";
	
	public static final String APP_OUTPUT_FILES = "app_output_files";
	
	public static final String PROFILING_OUTPUT = "profiling_output";
	public static final String MAPRED_JOB_QUEUE_NAME = "mapred.job.queue.name";
	public static final String MAPREDUCE_JOB_QUEUENAME = "mapreduce.job.queuename";
	
	public static final String WORKFLOW_INSTANCE_TRACK = "m_track_workflowInstance";
	public static final String DS_ID = "dataSourceId";
	
	public static final String ACCESSKEY_ID = "accesskeyid";
	public static final String SECRETKEY = "secretkey";
	public static final String PHOENIX_DRIVER_NAME = "org.apache.phoenix.jdbc.PhoenixDriver";
	public static final String FIELDMAPPING_WARNING = "FIELDMAPPING_WARNING";
	public static final String STAGE1_DS_PATH = "/ds/stage1/";
	public static final String STAGE2_DS_PATH = "/ds/stage2/";
	public static final String apps = "/apps/";
	public static final String machinelearning = "/machinelearning/";
	public static final String models = "/models/";
	public static final String data = "/dataprep/";
	public static final String APP = "APP";
	public static final String DS = "DS";	
	public static final String REPO_ID = "repoId";
	public static final String IS_DS_LOAD_FIRST_TIME = "isDSLoadFirstTime";
	public static final String ID = "id";
	public static final String OUTPUT_HAS_HEADER = "hasHeader";
	public static final String UUID = "uuid";
	public static final String ACCEPTED_RECORDS="acceptedRecords";
	public static final String REJECTED_RECORDS="rejectedRecords";
	public static final String SPARK_RUN_LOCAL = "local";
	public static final String SPARK_RUN_YARN = "yarn-cluster";
	
	public static final String FALSE = "false";
    public static final String INDUSTRY = "Industry";
    public static final String ACCELERATORS = "Accelerators";
    public static final String AUTOMATION = "Automation";
    public static final String ALGORITHMS = "Algorithms";
    public static final String AI = "AI";
    public static final String INSIGHTS = "Insights";
    public static final String API = "api";
    public static final String CLIENT = "client";
    public static final String GET_SOLUTIONS_BY_CATEGORY_ID = "/solution/getSolutionsByCategoryId/";
    public static final String GET_SOLUTION_INTALLED_STATUS = "/installationDetails/getSolutionIntalledStatus";
    public static final String INSTALLATION_DETAILS = "/installationDetails/install";
    public static final String UNINSTALL_INSTALLATION_DETAILS = "/installationDetails/uninstall";
    public static final String GET_CATEGORIES_BY_TYPE_ID = "/category/getCategoriesByTypeId/";
    public static final String GET_IMAGE_BY_CATEGORY_ID = "/category/getImageByCategoryId/";
    public static final String INTERESTED_CREATE = "/interested/create";
    public static final String GET_USER_INTERESTED_STATUS = "/interested/getUserInterestedStatus/";
    public static final String GET_IMAGE_BY_SOLUTION_ID = "/solution/getImageBySolutionId";
    public static final String TRUE = "true";
	
	public static final String APP_NAME = "appname";
	
	public static final String TYPE_OF_SOURCE_DATASORUCE = "58a731726fcdb626f075ae29";
	public static final String TYPE_OF_SOURCE_NOTEBOOK = "58a731726fcdb626f075ae2a";
	public static final String TYPE_OF_SOURCE_FLOW = "58a731726fcdb626f075ae2b";
	
	public static final String SUCCESS = "success";
	public static final String FAIL = "fail";
	public static final String STREAM = "STREAM";
	public static final String INDEX_ID = "indexId";
	
    public static final String TOTAL_RECORDS_LOADED = "TOTAL_RECORDS_LOADED";
    public static final String TOTAL_RECORDS_MERGED = "TOTAL_RECORDS_MERGED";
    public static final String TOTAL_RECORDS_DELETED = "TOTAL_RECORDS_DELETED";
    public static final String TOTAL_RECORDS_ERRORED = "TOTAL_RECORDS_ERRORED";
    public static final String TOTAL_RECORDS = "TOTAL_RECORDS";
    public static final String TOTAL_RECORDS_ACCEPTED = "TOTAL_RECORDS_ACCEPTED";
    
    public static final String JUPYTER_NOTEBOOK_EXT = ".ipynb";
	public static final String NOTEBOOK_TYPE = "notebook";
	public static final String TOKEN = "token";
	public static final String BOOTSTRAP_SERVER = "bootstrap.servers";
	public static final String GROUP_ID = "group.id";
	public static final String KEY_DESERIALIZER = "key.deserializer";
	public static final String VALUE_DESERIALIZER = "value.deserializer";
	public static final String KAFKA_STRING_DESERIALIZER_CLASS = "org.apache.kafka.common.serialization.StringDeserializer";
	public static final String TEMP_GROUP_ID = "test-consumer-group";
	
	
	
    public static String TESTCONNECTION ="TEST_CONNECTION";
    public static String EXTERNALCONNECTOR ="EXTERNALCONNECTOR";
    public static String SALESFORCE ="SALESFORCE";
    public static String AMAZONS3 ="AMAZONS3";
    public static String FACEBOOKPAGE="FACEBOOKPAGE";
    public static String DROPBOX="DROPBOX";
    public static String HACKYL="HECKYL";
    public static final String GOOGLE_ANALYTICS = "GOOGLE_ANALYTICS";

    
    public static String USER_ID = "USER_ID";
    public static String RESOURCE_ID = "RESOURCE_ID";
    public static String POLICY_TYPE = "POLICY_TYPE";
    public static String PROBE42="PROBE42";
    public static String SAPERP ="SAPERP";
    public static String TEMP_UNIQUE_ID_VIEW = "TEMP_UNIQUE_ID_VIEW";
    public static String UNQ_USER_ID= "unqUserId";
    public static String STATUS = "status";
    
    //Default datetime values for flow and datasource
    public static final String GP_CURRENT_RUN_DATETIME = "gp_currentRunDateTime";
    public static final String GP_LAST_RUN_DATETIME = "gp_lastRunDateTime";
    public static final String GP_LAST_SUCCESSFULLY_RUN_DATETIME = "gp_lastSuccessfullyRunDateTime";
    
      
    public static final String MAPRED_JOB_NAME = "mapred.job.name";
    public static final String FIELD1 = "field1";
    public static final String FIELD3 = "field3";
    public static final String CSV = "csv";
    public static final String SPARK_TEMP_ALIAS = "SPARK_TEMP_ALIAS";
    public static final String UTF8 = "UTF-8";
    public static final String MOSAIC_AGENT = "agent";
    public static final String MOSAIC_INSTANCE_TYPE = "mosaic.instance.type";
    public static final String COMPONENT_ID = "componentId";
	public static final String DEFAULT = "DEFAULT";
}
    
